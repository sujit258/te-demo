<?php
class ControllerAccountMyvoucher extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
 
		$this->document->setTitle("My gift Vouchers");
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('Home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('Account'),
			'href' => $this->url->link('account/account', '', true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('My Vouchers'),
			'href' => $this->url->link('account/myvoucher', $url, true)
		);

		$this->load->model('setting/setting');
				
					
		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['myvouchers'] = array();

		$this->load->model('account/myvoucher');

		$myvoucher_total = $this->model_account_myvoucher->getTotalMyvouchers();

		$results = $this->model_account_myvoucher->Myvouchers(($page - 1) * 10, 10);

		
		foreach ($results as $result) {

			$data['myvouchers'][] = array(
				'voucher_id' => $result['voucher_id'], 
				'from_name'  => $result['from_name'],
				'from_email' => $result['from_email'],
				'code'       => $result['code'],
				'amount'     => $result['amount'],
				'is_send'    => $result['is_send'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
			
		}
        
        
        
		$pagination = new Pagination();
		$pagination->total = $myvoucher_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/myvoucher', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($myvoucher_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($myvoucher_total - 10)) ? $myvoucher_total : ((($page - 1) * 10) + 10), $myvoucher_total, ceil($myvoucher_total / 10));

		$data['continue'] = $this->url->link('account/account', '', true);
		$data['button_reorder'] = $this->language->get('button_reorder');
			$data['button_return'] = $this->language->get('button_return');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/myvoucher_list', $data));
	}
	
	
		public function sendmail() {
    		$this->language->load('account/voucher');
    
    		$this->load->model('account/myvoucher');
    
    		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
    			
    			$from_name = $this->request->post['from_name'];
    			$from_email = $this->request->post['from_email'];
    			$to_name = $this->request->post['to_name'];
    			$to_email = $this->request->post['to_email'];
    			$code = $this->request->post['code'];
    			$amount = $this->request->post['amount'];
    			$voucher_id = $this->request->post['voucher_id'];
    			
    			$data = array();

				$data['from_name'] = $from_name;
				$data['from_email'] = $from_email;
				$data['to_name'] = $to_name;
				$data['to_email'] = $to_email;
				$data['code'] = $code;
				$data['amount'] = $amount;
				$data['voucher_id'] = $voucher_id;
				
				
				$data['title'] = sprintf($this->language->get('text_subject'), $from_name);

				$data['text_greeting'] = sprintf($this->language->get('text_greeting'), $this->currency->format($amount, INR, 1.00000000));
				$data['text_from'] = sprintf($this->language->get('text_from'), $from_name);
				$data['text_message'] = $this->language->get('text_message1');
				$data['text_redeem'] = sprintf($this->language->get('text_redeem'), $code);
				$data['text_footer'] = $this->language->get('text_footer');
				
    			if($this->request->get['sendmail'] == 1) {
    			    
    			    $this->model_account_myvoucher->addVoucher($data);
    			    $from   = $from_name;
                    $sender = $from_email;
                    
    			       $mail = new Mail();
    			       $mail->protocol = $this->config->get('config_mail_protocol');
    			       $mail->parameter = $this->config->get('config_mail_parameter');
    			       $mail->hostname = $this->config->get('config_smtp_host');
    			       $mail->username = $this->config->get('config_smtp_username');
    			       $mail->password = $this->config->get('config_smtp_password');
    			       $mail->port = $this->config->get('config_smtp_port');
    			       $mail->timeout = $this->config->get('config_smtp_timeout');
    			        $mail->setTo($to_email);
        				$mail->setFrom($from_email);
        				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        				$mail->setSubject(html_entity_decode(sprintf($this->language->get('text_subject'), $from_name, $this->currency->format($amount, INR, 1.00000000)), ENT_QUOTES, 'UTF-8'));
        				$mail->setHtml($this->load->view('account/vouchermail', $data));
        				$mail->send();
    			    
    			       $this->data['mail_success'] = "Mail sent successfully!";
    			    
    			}
    
    		$this->data['success'] = "Mail sent successfully!";

    		} else {
    			$this->data['success'] = '';
    		}
    
	}
	 


}