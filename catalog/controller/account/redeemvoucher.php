<?php
class ControllerAccountRedeemvoucher extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
		    
		    $code = $this->request->get['code'];
		    
			$this->session->data['redirect'] = $this->url->link('account/redeemvoucher&code='.$code, '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
		
		
		 $this->load->model('extension/total/voucher');
		 
		 $voucher_info = $this->model_extension_total_voucher->getVoucher('QGqTzYeHHz');
		 print_r($voucher_info);
	    	
	    $this->load->model('extension/total/voucher'); 
	    $this->load->language('account/redeemvoucher');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/customer');
            
            $this->load->model('extension/total/voucher');
	    	
	       $voucher_query =  $this->model_extension_total_voucher->getVoucher($this->request->post['code']); 
	      
	        
    	   if($voucher_query){
    	        $this->model_account_customer->redeemvoucher($this->customer->getId(), $this->request->post['code'],$voucher_query['from_name'],$voucher_query['amount'],$voucher_query['theme']); 
    	   

    			$this->session->data['success'] = " Your Gift Voucher code has been successfully applied and is now redeemable as ". round($voucher_query['amount'],2) . "/- True Cash ";
    
    			// Add to activity log
    			if ($this->config->get('config_customer_activity')) {
    				$this->load->model('account/activity');
    
    				$activity_data = array(
    					'customer_id' => $this->customer->getId(),
    					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
    				);
    
    				$this->model_account_activity->addActivity('voucher_redeem', $activity_data);
    			}
    
    			//$this->response->redirect($this->url->link('account/reward', '', true));
    	   } 
    	   
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/redeemvoucher', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_code'] = $this->language->get('text_code');

		$data['entry_code'] = $this->language->get('entry_code');
		

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');
        
        if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$data['action'] = $this->url->link('account/redeemvoucher', '', true);

		if (isset($this->request->post['code'])) {
			$data['code'] = $this->request->post['code'];
		} else if($this->request->get['code']) {
		    $data['code'] = $this->request->get['code'];
		} else {    
			$data['code'] = '';
		}

	 	$data['back'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/redeemvoucher', $data));
	}

	protected function validate() {
	     
		$this->load->model('extension/total/voucher');
		
		$voucher_info = $this->model_extension_total_voucher->getVoucher($this->request->post['code']);
		
		if ($voucher_info) {
			 
			$this->session->data['successs'] = " You have successfully redeemed ". $voucher_info['amount'] . " into your true cash";
			
		}  else if (!$this->request->post['code']) {
			unset($this->session->data['voucher']);
			$json['error']['error'] = $this->language->get('voucher_blank');
			$this->error['code'] = "Please enter voucher code";
		} else {
			unset($this->session->data['voucher']);
			$this->error['code'] = "Voucher code is already used";
		}
		 

		return !$this->error;
	}
}