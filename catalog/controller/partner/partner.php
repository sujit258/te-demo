<?php
class ControllerPartnerPartner extends Controller {
	
	public function index() {
		
	    $this->load->language('partner/partner');

		$this->load->model('catalog/partner');
		
		$this->load->model('tool/image');
		
		$this->document->setTitle($this->language->get('heading_title'));
		$data['heading_title'] = $this->language->get('heading_title');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_title'),
			'href' => $this->url->link('partner/partner')
		);
		//text
		$data['text_title']       = $this->language->get('text_title');

		// enrty
		$data['entry_name']       = $this->language->get('entry_name');
		$data['entry_email']      = $this->language->get('entry_email');
		$data['entry_mobile']     = $this->language->get('entry_mobile');
		$data['entry_city']       = $this->language->get('entry_city');
		$data['entry_scope']      = $this->language->get('entry_scope');
		
		//Placeholder
		$data['tag_name']         = $this->language->get('tag_name');
		$data['tag_email']        = $this->language->get('tag_email');
		$data['tag_mobile']       = $this->language->get('tag_mobile');
		$data['tag_city']         = $this->language->get('tag_city');

		// Button
		$data['button_send']      = $this->language->get('button_send');
		$data['button_catalog']   = $this->language->get('button_catalog');

		
		
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

		
		$this->response->setOutput($this->load->view('partner/partner',$data));

	}
	
	public function write() {
		$this->load->language('partner/partner');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST')
		{
			if (!isset($json['error'])) 
			{
				
				$this->load->model('catalog/partner');
				
				if($this->request->post['cst_name'] != '' & $this->request->post['cst_email'] != '' & $this->request->post['cst_mobile'] != '' & $this->request->post['cst_city'] != '' & $this->request->post['cst_scope'] != '')
				{
				
				$email = $this->request->post['cst_email'];
				
				$mobile = $this->request->post['cst_mobile'];
				
				
				
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) 
				{
					if(preg_match('/^[0-9]{10}+$/', $mobile))
					{
						$this->model_catalog_partner->addPartner($this->request->post);

						$json['success'] = 'Your Response has been Recorded!';
					}
					else
					{
						$json['validate_mobile'] = 'Invalid Mobile No!';
					}
				}
				else
				{
					$json['validate_email'] = 'Invalid Email!';
				}
				
				}
				else
				{
					$json['not_success'] = 'Please Enter Valid Data!';
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>