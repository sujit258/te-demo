<?php
class ControllerRecyclesRecycles extends Controller {
	public function index() {
		$this->load->language('recycles/recycles');

		$this->load->model('catalog/recycles');
		
		$this->load->model('tool/image');
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
		$this->document->setTitle("Recycle Food Products Packets");
		
		//text
		$data['text_title']       = $this->language->get('text_title');

		// enrty
		$data['entry_name']       = $this->language->get('entry_name');
		$data['entry_email']      = $this->language->get('entry_email');
		$data['entry_mobile']     = $this->language->get('entry_mobile');
		$data['entry_address']    = $this->language->get('entry_address');
		$data['entry_no_of_packets']    = $this->language->get('entry_no_of_packets');
		$data['entry_order_id']   = $this->language->get('entry_order_id');
		$data['entry_pickup_date']       = $this->language->get('entry_pickup_date');
		$data['entry_pickup_slot']      = $this->language->get('entry_pickup_slot');
		
		//Placeholder
		$data['tag_name']         = $this->language->get('tag_name');
		$data['tag_email']        = $this->language->get('tag_email');
		$data['tag_mobile']       = $this->language->get('tag_mobile');		
		$data['tag_address']      = $this->language->get('tag_address');
		$data['tag_order_id']     = $this->language->get('tag_order_id');
		$data['tag_no_of_packets']= $this->language->get('tag_no_of_packets');

        $data['logged'] = $this->customer->isLogged();

		// Button
		$data['button_send']      = $this->language->get('button_send');
		$data['button_catalog']   = $this->language->get('button_catalog');// delete
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => 'Recycle',
			'href' => $this->url->link('recycles/recycles')
		);
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			unset($this->session->data['guest']);
		}
		// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} else {
			$this->data['password'] = '';
		}
		
		
		if ($this->customer->isLogged()) {
				$data['customer'] = true;				
		} else {
				$data['customer'] = false;
		}
			
		if($this->customer->getId()) {
			$data['cst_details'] = $this->model_catalog_recycles->getCustomer($this->customer->getId());				
		}
		$data['login'] = $this->url->link('account/login');
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('recycles/recycles',$data));

	}
	
	public function write() {
		$this->load->language('recycles/recycles');
		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST')	{
			if (!isset($json['error'])) {
			    $this->load->model('catalog/recycles');
			    if($this->request->post) {
				    $email = $this->request->post['cst_email'];
				    $mobile = $this->request->post['cst_mobile'];
				    $packets_count = $this->request->post['cst_no_of_packets'];
				    $pickup_date = $this->request->post['pickup_date'];
				    $pickup_slot = $this->request->post['pickup_slot'];
				
				    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
					    if(preg_match('/^[0-9]{10}+$/', $mobile)) {
						    if($packets_count != '' && $packets_count >= 10 ) {
						        if($pickup_date != '' && date('Y-m-d', strtotime($pickup_date)) > date('Y-m-d')) {
						            $this->model_catalog_recycles->addRecycles($this->customer->getId(), $this->request->post);
						            $json['success'] = 'Your Response has been Recorded!!';
						        } else {
						            $json['validate_pickup_date'] = 'Please Enter Valid Pickup Date!';
						        }
						    } else {							
							    $json['validate_nop'] = 'Minimum 10 Required!';
						    }
					    } else {
						    $json['validate_mobile'] = 'Invalid Mobile No!';
					    }
				    } else {
					    $json['validate_email'] = 'Invalid Email!';
				    }
			    } else {
					$json['not_success'] = 'Request Failed!';
			    }
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
	}
	
	protected function validate() {
	   $this->load->model('account/customer');

		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		if (!$this->error) {
			if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
				$this->error['warning'] = $this->language->get('error_login');

				$this->model_account_customer->addLoginAttempt($this->request->post['email']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
			}
		}

		return !$this->error;
	}
}
?>