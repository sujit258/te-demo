<?php
class ControllerExtensionShippingXshippingpro extends Controller {
    public function estimate_shipping() {
        $json=array();
        $this->load->model('extension/shipping/xshippingpro');
        $this->load->language('extension/shipping/xshippingpro');
        
        $xshippingpro_estimator =  $this->config->get('xshippingpro_estimator');
        $estimator_type = (isset($xshippingpro_estimator['type']) && $xshippingpro_estimator['type']) ? $xshippingpro_estimator['type'] : 'method';
        $address = array();
        if ($estimator_type == 'avail') {
            $address = array('only_address_rule' => true);
        }
        
        $json =  $this->model_extension_shipping_xshippingpro->getQuote($address);
        if ($estimator_type == 'avail') {
            if ($json) {
                $json = array();
                $json['message'] = $this->language->get('xshippingpro_available');
                $json['class'] = 'avail';
            } else {
                $json = array();
                $json['message'] = $this->language->get('xshippingpro_no_available');
                $json['class'] = 'no_avail';
            }
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
