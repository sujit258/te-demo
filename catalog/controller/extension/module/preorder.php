<?php  
class ControllerExtensionModulePreOrder extends Controller {
	private $data = array();

	public function index($setting) {
		$this->data['currenttemplate'] = $this->config->get('config_template');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['data']['preorder'] = str_replace('http', 'https', $this->config->get('preorder'));
		} else {
			$this->data['data']['preorder'] = $this->config->get('preorder');
		}
		
		if(!isset($this->data['data']['preorder']['CustomTitle'][$this->config->get('config_language')])){
			$this->data['data']['preorder']['CustomTitle'] = '';
		} else {
			$this->data['data']['preorder']['CustomTitle'] = $this->data['data']['preorder']['CustomTitle'][$this->config->get('config_language')];
		}
		 

		if (isset($this->request->get['product_id'])) {
			$this->data['product_id'] = $this->request->get['product_id'];
		}
		
		$this->data['preorder_Button'] = $this->data['data']['preorder']['ButtonName'][$this->config->get('config_language_id')];
		$this->data['button_cart']     = $this->language->get('button_cart');

		return $this->load->view('extension/module/preorder.tpl', $this->data); 
	}
	
	public function sendMails() {
		$this->load->model('setting/setting');
		$this->load->model('catalog/product');
		
		if (isset($this->request->post['store_id'])) {
			$store_id = $this->request->post['store_id'];	
		} else if (isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id'];
		} else {
			$store_id = 0;
		}
		
		$store_data = $this->getStore($store_id);
		
		$settings = $this->model_setting_setting->getSetting('preorder', $store_id);
		$settings = (isset($settings['preorder'])) ? $settings['preorder'] : array(); 

		if (isset($settings['Enabled']) && $settings['Enabled']=='yes') {
			$query = $this->db->query("SELECT super.*, product.name as product_name FROM `" . DB_PREFIX . "preorder` super 
				JOIN `" . DB_PREFIX . "product_description` product on super.product_id = product.product_id
				WHERE customer_notified=0 and language_id = " . (int)$this->config->get('config_language_id') . " and store_id = " . (int)$store_id . "
				ORDER BY `date_created` DESC");

			$counter = 0;
			$report = array();
			foreach ($query->rows as $row) {
				$send = false;
				$row['selected_options'] = unserialize($row['selected_options']);
				
				$product = $this->model_catalog_product->getProduct($row['product_id']);
				if (sizeof($row['selected_options']) == 0) {
					if ($product['quantity']>0) {
						$send = true;
					}
				} else {
					$product_options = $this->model_catalog_product->getProductOptions($row['product_id']);
					$matchCount = 0;
					$userCount = count($row['selected_options']);
					foreach ($row['selected_options'] as $row_options) {
						foreach ($product_options as $product_option) {
							foreach($product_option['product_option_value'] as $options) {
								if((($row_options['option_value_id'] == $options['option_value_id']) &&
								 ($row_options['product_option_value_id'] == $options['product_option_value_id']) &&
								 (($options['quantity'] > 0)))) {
									$send = true;
								}
							}
						}
					}
				}

				if ($send) {
					$this->sendEmailWhenAvailable($row,$store_data);
					$counter++;	
					$report[] = $row['customer_name'].' ('.$row['customer_email'].') - '.$row['product_name'];
				}
			}
			
			$result = "Cron was executed successfully! A total of <strong>".$counter."</strong> emails were sent to the customers.<br />";

			if ($counter>0 && sizeof($report)>0) {
				$result .= "<br />Here is a list with the notified customers:<br /><ul>";
				foreach ($report as $rep) {
					$result .= "<li>".$rep."</li>";	
				}
				$result .= "</ul>";
			}

			if (isset($settings['CronNotify']) && $settings['CronNotify']=='yes') {
				$mailToUser = new Mail($this->config->get('config_mail'));
				$mailToUser->setTo($this->config->get('config_email'));
				$mailToUser->setFrom($this->config->get('config_email'));
				$mailToUser->setSender($store_data['name']);
				$mailToUser->setSubject(html_entity_decode('preorder Sheduled Task', ENT_QUOTES, 'UTF-8'));
				$mailToUser->setHtml($result);
				$mailToUser->setText(html_entity_decode($result, ENT_QUOTES, 'UTF-8'));
				$mailToUser->send(); 
			} else {
				echo $result;	
			}
		}
	}
	
	public function sendEmailWhenAvailable($row,$store_data) {
		$this->load->model('catalog/product');	
		$this->load->model('tool/image');
		$this->load->model('setting/setting');
		
		$product_id = $row['product_id'];
		$product_info = $this->model_catalog_product->getProduct($product_id);
		if ($product_info['image']) { $image = $this->model_tool_image->resize($product_info['image'], 200, 200); } else { $image = false; }
			
		$preorder = $this->model_setting_setting->getSetting('preorder', $row['store_id']);
		$preorder = (isset($preorder['preorder'])) ? $preorder['preorder'] : array(); 
				
		if(!isset($preorder['EmailText'][$row['language']])){
			$EmailText = '';
			$EmailSubject = '';
		} else {
			$EmailText = $preorder['EmailText'][$row['language']];
			$EmailSubject = $preorder['EmailSubject'][$row['language']];
		}
	
		$string = html_entity_decode($EmailText);
		$patterns = array();
		$patterns[0] = '/{c_name}/';
		$patterns[1] = '/{p_name}/';
		$patterns[2] = '/{p_image}/';
		$patterns[3] = '/http:\/\/{p_link}/';
		$replacements = array();
		$replacements[0] = $row['customer_name'];
		$replacements[1] = "<a href='".$store_data['url']."index.php?route=product/product&product_id=".$product_id."' target='_blank'>".$product_info['name']."</a>";
		$replacements[2] = "<img src='".$image."' />";
		$replacements[3] = $store_data['url']."index.php?route=product/product&product_id=".$product_id;

		$text = preg_replace($patterns, $replacements, $string);
		
		$mailToUser = new Mail($this->config->get('config_mail'));
		$mailToUser->setTo($row['customer_email']);
		$mailToUser->setFrom($this->config->get('config_email'));
		$mailToUser->setSender($store_data['name']);
		$mailToUser->setSubject(html_entity_decode($EmailSubject, ENT_QUOTES, 'UTF-8'));
		$mailToUser->setHtml($text);
		$mailToUser->send(); 
		
	    $update_customers = $this->db->query("UPDATE `" . DB_PREFIX . "preorder` SET customer_notified=1 WHERE product_id = ".$product_id." and store_id = ". $row['store_id']);	
	}
	
	private function getStore($store_id) { 
		$this->load->model('setting/store');   
        if($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL();
        }
        return $store;
    }
	
	private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTP_SERVER;
        } else {
            $storeURL = HTTPS_SERVER;
        } 
        return $storeURL;
    }
}
?>