<?php
class ControllerExtensionModuleXtensionsCheckoutXCVC extends Controller {
	public $data = array();	

	public function __construct($registry) {
		parent::__construct($registry);
		$this->data = $this->load->language('checkout/checkout');
		$this->data = array_merge($this->data,$this->load->language($this->config->get('xtensions_language_path')));
		$this->data += $this->xtensions_checkout->getXtensionsData($this->config->get('config_store_id'), 'xtensions_best_checkout');
		$xconfig = $this->data['xconfig'];
		$options = $xconfig['options'];
		$this->data['display_rewards'] = isset($options['xcvc_view']) && isset($options['xcvc_view']) == 'modal' && isset($options['display_rewards']);
		$this->data['display_coupons'] = isset($options['xcvc_view']) && isset($options['xcvc_view']) == 'modal' && isset($options['display_coupons']);
		$this->data['display_vouchers'] = isset($options['xcvc_view']) && isset($options['xcvc_view']) == 'modal' && isset($options['display_vouchers']);
		$this->data['display_comments'] = isset($options['address_type']) && $options['address_type'] == 'block' && isset($options['display_comments']);
	}

	public function index() {
		$this->data['text_comments'] = $this->language->get('text_comments');
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['agree']   =  isset($this->session->data['agree'])?$this->session->data['agree']:'';
		$this->data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_checkout_id'), 'SSL'), $information_info['title'], $information_info['title']);
		
		
		$this->data['note'] = $this->session->data['note'];

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');
		
		if (isset($this->session->data['comment'])) {
			$this->data['comment'] = $this->session->data['comment'];
		} else {
			$this->data['comment'] = '';
		}
		$points = $this->customer->getRewardPoints();
		
		$points_total = 0;
		
		$min_points = $this->config->get('reward_point_min');
		$max_points = $this->config->get('reward_point_max');
		
		foreach ($this->cart->getProducts() as $product) {
			if ($product['points']) {
				$points_total += $product['points'];
			}
			
			$coupon_query = $this->db->query("SELECT c.code,c.type,c.discount,c.date_end, c.description FROM " . DB_PREFIX . "coupon c LEFT JOIN " . DB_PREFIX . "coupon_product cp ON (c.coupon_id = cp.coupon_id) WHERE (c.display_cart = 1 AND c.status = 1) AND ((c.date_start = '0000-00-00' OR c.date_start < NOW()) AND (c.date_end = '0000-00-00' OR c.date_end > NOW())) AND cp.product_id = '" . $product['product_id'] . "'");
			
			if($coupon_query->num_rows) {
				foreach($coupon_query->rows AS $coupon) {
					$this->data['coupons'][] = array(
						'coupon_code' => $coupon['code'],
						'discount'    => $coupon['discount'],
						'type'        => $coupon['type'],
						'date_end'    => $coupon['date_end'], 
						'description' => $coupon['description']
					);
				}
			}
			
		}
		$this->data['coupons'] = array_unique($this->data['coupons'], SORT_REGULAR);
		
		$common_coupon_query = $this->db->query("SELECT code,type,discount,date_end, description FROM " . DB_PREFIX . "coupon WHERE (display_cart = 1 AND status = 1) AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND common_coupon = 1");
		
		if($common_coupon_query->num_rows) {
			foreach($common_coupon_query->rows AS $common_coupon) {
				$this->data['coupons'][] = array(
					'coupon_code' => $common_coupon['code'], 
					'discount'    => $common_coupon['discount'],
					'type'        => $common_coupon['type'],
					'date_end'    => $common_coupon['date_end'],
					'description' => $common_coupon['description']
				);
			}
		}
		

        	$filter_data = array(
        		'filter_category_id' => 629,
        		'filter_filter'      => $filter,
        		'sort'               => $sort,
        		'order'              => $order,
        		'start'              => ($page - 1) * $limit,
        		'limit'              => 500
        	);

    		
    	    //$product_total = $this->model_catalog_product->getTotalProducts($filter_data);
    	    
            
            $cart_products = $this->cart->getProducts();
            
    		if($cart_products) {
    		    foreach($cart_products AS $c_products) {
    		        $data['cart_data'][$c_products['product_id']] = array(
    		            'cart_id'   => $c_products['cart_id'],
    		            'quantity'  => $c_products['quantity']
    		        );
    		    }
    		}
		    
		    if(array_key_exists($this->session->data['freehitcart'], $data['cart_data'])){
		        $this->data['freehitcart'] = $this->session->data['freehitcart'];
		    }
		    
		$filter_data = array(
        		'filter_category_id' => 624,
        		'filter_filter'      => $filter,
        		'sort'               => $sort,
        		'order'              => $order,
        		'start'              => ($page - 1) * $limit,
        		'limit'              => 500
        	);


		    $results = $this->model_catalog_product->getProducts($filter_data);
		     
		    
		    $data['tryproducts'][]  = array();
		    
			foreach ($results as $result) {
			    
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], 300, 300);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}
				
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}
				
				$base_option_special_price = $this->model_catalog_product->getFirstProductOptionValue($result['product_id']);
                
                $base_option_id = $this->model_catalog_product->getFirstProductOptionValueId($result['product_id']);
                 
                
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					if($base_option_special_price != 0) {
				        $special = $this->currency->format($this->tax->calculate($result['price'] + $base_option_special_price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				        $base_special = $result['price'] + $base_option_special_price;
				    } else {
					    $special = false;
				    }
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
                
				$options = NULL;
				if($result['product_type'] != 5) {
				    $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				} else {
				    $price = false;
				}
				 
			    
			    $name = html_entity_decode(substr($result['name'],0,55));
			    if(strlen($result['name']) > 55) {
			        $name .= "...";
			    }
			    
    		    $this->data['tryproducts'][] = array(
            		        'product_id'  => $result['product_id'],
            		        'thumb'       => $image,
             				'name'        => $name,
             				'options'     => $options,
            				'quantity'    => $result['quantity'],
            				'base_option_id' => $base_option_id,
            				'price'       => substr($price,3) > 0 ? $price : "",
            				'special'     => $special,
             				'tax'         => $tax,
            				'rating'      => $result['rating'],
            				'reviews'     => $result['reviews'] > 0 ? $result['reviews'] : 0,
            				'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
            			);

    		    
		}	

		$this->data['reward_status'] = ($points && $points_total && $this->config->get('reward_status'));
		if (isset($this->session->data['reward'])) {
			$rewardValidate = $this->validateReward($this->session->data['reward']);
			if ($rewardValidate) {
				$this->data['reward_value'] = $this->session->data['reward'] = $rewardValidate;
			} else {
				$this->data['reward_value'] = $this->session->data['reward'];
			}
		} else {
			$this->data['reward_value'] = '';
		}
		$this->language->load('extension/total/reward');
		$this->data['text_use_reward'] = sprintf($this->language->get('heading_title'), $points);
		
		$this->data['entry_reward'] = sprintf($this->language->get('entry_reward'), $points_total);
		
		$this->load->language($this->config->get('xtensions_language_path'));
		$this->data['coupon_apply_text'] = $this->language->get('coupon_apply_text');
		$this->data['coupon_text'] = $this->language->get('coupon_text');
		$this->data['coupon_placeholder'] = $this->language->get('coupon_placeholder');
		$this->data['voucher_apply_text'] = $this->language->get('voucher_apply_text');
		$this->data['voucher_text'] = $this->language->get('voucher_text');
		$this->data['voucher_placeholder'] = $this->language->get('voucher_placeholder');
		$this->data['reward_apply_text'] = $this->language->get('reward_apply_text');
		$this->data['reward_text'] = $this->language->get('reward_text');
		$this->data['comment_apply_text'] = $this->language->get('comment_apply_text');
		$this->data['comment_text'] = $this->language->get('comment_text');
		
		if (!empty($this->session->data['voucher'])) {
			$this->data['voucher_value'] = $this->session->data['voucher'];
		} else {
			$this->data['voucher_value'] = '';
		}
		
		if (!empty($this->session->data['coupon'])) {
			$this->data['coupon_value'] = $this->session->data['coupon'];
		} else {
			$this->data['coupon_value'] = '';
		}
		
		$this->template = $this->config->get('xtensions_view_path').'xcvc.tpl';
		return $this->xtensions_checkout->renderView($this);
	}

	public function xoptions() {
	    
	    if(isset($this->session->data['gift_packaging'])) {
	        $this->data['gift_packaging'] = $this->session->data['gift_packaging'];
	    } else {
	        $this->data['gift_packaging'] = 0;
	    }
	    
	    if (isset($this->session->data['note'])) {
			$this->data['note'] = $this->session->data['note'];
		} else {
			$this->data['note'] = '';
		}
		
		if(!$this->customer->isLogged()){
			$this->data['hasorders']  = 0;
		} else {
			$this->data['hasorders']  = $this->customer->hasorders();
		} 
		
		$this->data['misc_options'] = $misc_options = $this->data['xconfig']['options'];
		if (isset($misc_options['display_coupons']) || isset($misc_options['display_vouchers']) || isset($misc_options['display_rewards']) || isset($misc_options['display_comments'])) {
			$this->data['inline_view'] = isset($misc_options['xcvc_view']) && $misc_options['xcvc_view'] == 'inline' ? true : false;
			$this->data['display_rewards'] = $this->data['display_coupons'] = $this->data['display_vouchers'] = true;
			
			if (isset($misc_options['display_rewards'])) {
				if (isset($this->session->data['reward'])) {
					$rewardValidate = $this->validateReward($this->session->data['reward']);
					if ($rewardValidate) {
						$this->data['reward_value'] = $this->session->data['reward'] = $rewardValidate;
					} else {
						$this->data['reward_value'] = $this->session->data['reward'];
					}
				} else {
					$this->data['reward_value'] = '';
				}
				$points = $this->customer->getRewardPoints();
				
				$points_total = 0;
				$min_points = $this->config->get('reward_point_min');
				$max_points = $this->config->get('reward_point_max');
				
				foreach ($this->cart->getProducts() as $product) {
					if ($product['points']) {
						$points_total += $product['points'];
					}
					
					$coupon_query = $this->db->query("SELECT c.code, c.description FROM " . DB_PREFIX . "coupon c LEFT JOIN " . DB_PREFIX . "coupon_product cp ON (c.coupon_id = cp.coupon_id) WHERE (c.display_cart = 1 AND c.status = 1) AND ((c.date_start = '0000-00-00' OR c.date_start < NOW()) AND (c.date_end = '0000-00-00' OR c.date_end > NOW())) AND cp.product_id = '" . $product['product_id'] . "'");
					
					if($coupon_query->num_rows) {
						foreach($coupon_query->rows AS $coupon) {
							$this->data['coupons'][] = array(
								'coupon_code' => $coupon['code'], 
								'description' => $coupon['description']
							);
						}
					}
					
				}
				$this->data['coupons'] = array_unique($this->data['coupons'], SORT_REGULAR);
				
				$common_coupon_query = $this->db->query("SELECT code, description FROM " . DB_PREFIX . "coupon WHERE (display_cart = 1 AND status = 1) AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND common_coupon = 1");
				
				if($common_coupon_query->num_rows) {
					foreach($common_coupon_query->rows AS $common_coupon) {
						$this->data['coupons'][] = array(
							'coupon_code' => $common_coupon['code'], 
							'description' => $common_coupon['description']
						);
					}
				}
				
				
				
				$this->data['display_rewards'] = ($points && $points_total && $this->config->get('reward_status'));
				$this->data['text_use_reward'] = sprintf($this->language->get('text_rewards_label'), $points);
			} else {
				$this->data['display_rewards'] = false;
			}
			if (isset($misc_options['display_vouchers'])) {
				if (isset($this->session->data['voucher'])) {
					$this->data['voucher_value'] = $this->session->data['voucher'];
				} else {
					$this->data['voucher_value'] = '';
				}
			} else {
				$this->data['display_vouchers'] = false;
			}
			if (isset($misc_options['display_coupons'])) {
				if (!empty($this->session->data['coupon'])) {
					$this->data['coupon_value'] = $this->session->data['coupon'];
				} else {
					$this->data['coupon_value'] = '';
				}
			} else {
				$this->data['display_coupons'] = false;
			}
			$this->data['display_comments'] = isset($misc_options['address_type']) && $misc_options['address_type'] == 'block' && isset($misc_options['display_comments']);
			$this->template = $this->config->get('xtensions_view_path').'xoptions.tpl';
			return $this->xtensions_checkout->renderView($this);
		} else {
			return '';
		}
	}
    
    public function giftPackaging() {
	    $json = array();
        $this->session->data['gift_packaging'] = $this->request->get['gift_packaging'];
        $json['success'] = 'Success';
        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function xtotals() {
		$this->load->language($this->config->get('xtensions_language_path'));
		$this->data['text_totals'] = $this->language->get('text_totals');
		$this->language->load('checkout/checkout');
		$totals = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();
		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes' => &$taxes,
			'total' => &$total 
		);

		$this->load->model('extension/extension');
		$results = $this->model_extension_extension->getExtensions('total');
		$sort_order = array();
		
		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}
		
		array_multisort($sort_order, SORT_ASC, $results);
		
		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('extension/total/' . $result['code']);
				
				$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
			}
		}
		
		$sort_order = array();
		
		foreach ($totals as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}
		
		array_multisort($sort_order, SORT_ASC, $totals);
		$this->data['totals'] = array();
		foreach ($totals as $total) {
			$this->data['totals'][] = array(
				'title' => $total['title'],
				'text' => $this->currency->format($total['value'], $this->session->data['currency']),
				'code'  => $total['code']  
			);
		}
		$this->xtensions_checkout->setTotals($total['value']);
		$this->data['final_price'] = $this->currency->format($total['value'], $this->session->data['currency']);
		$this->template = $this->config->get('xtensions_view_path').'xtotals.tpl';
		return $this->xtensions_checkout->renderView($this);
	}

	public function validateCoupon() {
		$json = array();
		$this->load->model('extension/total/coupon');
		$coupon_info = $this->model_extension_total_coupon->getCoupon($this->request->post['coupon']);
		$this->load->language($this->config->get('xtensions_language_path'));
		
		if (isset($this->session->data['voucher'])) {
		    if($this->request->post['coupon'] == 'FREESHIPPING') {
		        $this->session->data['coupon'] = $this->request->post['coupon'];
		        $json['applied'] = $coupon_info['success_message'];
		    } else if($this->request->post['coupon'] == '') {
		        unset($this->session->data['coupon']);
			    $json['applied'] = $this->language->get('coupon_removed');
		    } else if(!empty($this->session->data['coupon']) || !empty($this->request->post['coupon'])) {
		        unset($this->session->data['coupon']);
			    $json['error']['error'] = "You have already applied Voucher";
		    }
		    /*unset($this->session->data['coupon']);
			$json['error']['error'] = "You have already applied Voucher";*/
		} else if ($coupon_info) {
			$this->session->data['coupon'] = $this->request->post['coupon'];
			$json['applied'] = $coupon_info['success_message'];
			
		} else if (!empty($this->session->data['coupon']) && !$this->request->post['coupon']) {
			unset($this->session->data['coupon']);
			$json['applied'] = $this->language->get('coupon_removed');
		} else if (!$this->request->post['coupon']) {
			unset($this->session->data['coupon']);
			$json['error']['error'] = $this->language->get('coupon_blank');
		} else {
			unset($this->session->data['coupon']);
			$json['error']['error'] = $this->language->get('coupon_invalid');
		}
		$child = $this->xtensions_checkout->getChildren(array($this->config->get('xtensions_controller_path').'xcvc/xtotals'));
		$json['xtotals'] = $child['xtotals'];
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function validateVoucher() {
		$json = array();
		$this->load->model('extension/total/voucher');
		$voucher_info = $this->model_extension_total_voucher->getVoucher($this->request->post['voucher']);
		$this->load->language($this->config->get('xtensions_language_path'));
		if ($voucher_info) {
		    if($this->session->data['coupon'] != 'FREESHIPPING') {
		        unset($this->session->data['coupon']);
		    }
			$this->session->data['voucher'] = $this->request->post['voucher'];
			$json['applied'] = $this->language->get('voucher_successful');
		} else if (!empty($this->session->data['voucher']) && !$this->request->post['voucher']) {
			unset($this->session->data['voucher']);
			$json['applied'] = $this->language->get('voucher_removed');
		} else if (!$this->request->post['voucher']) {
			unset($this->session->data['voucher']);
			$json['error']['error'] = $this->language->get('voucher_blank');
		} else {
			unset($this->session->data['voucher']);
			$json['error']['error'] = $this->language->get('voucher_invalid');
		}
		$child = $this->xtensions_checkout->getChildren(array($this->config->get('xtensions_controller_path').'xcvc/xtotals'));
		$json['xtotals'] = $child['xtotals'];
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function validateReward($reward = 0) {
		$json = array();
		$this->load->language($this->config->get('xtensions_language_path'));
		$points = $this->customer->getRewardPoints();
		
		$CustGroupId = $this->customer->getGroupId();
		
		$points_total = 0;
		
		if ($CustGroupId == 8){
			$min_points = $this->config->get('reward_point_min');
			$max_points = 10000;
		} else {
			$min_points = $this->config->get('reward_point_min');
			$max_points = $this->config->get('reward_point_max');
		}
		
		
		foreach ($this->cart->getProducts() as $product) {
			if ($product['points']) {
				$points_total += $product['points'];
			}
		}
		
		/*if (($this->request->post['reward']) > $points_total) {
		    
		    $this->request->post['reward'] = $points_total;
		}*/
		
		if (!$reward) {
			if (empty($this->request->post['reward']) && isset($this->session->data['reward'])) {
				unset($this->session->data['reward']);
				$json['applied'] = $this->language->get('reward_removed');
			} else if (empty($this->request->post['reward'])) {
				unset($this->session->data['reward']);
				$json['error']['error'] = $this->language->get('reward_blank');
			} else if ($this->request->post['reward'] > $points) {
				$json['error']['error'] = sprintf($this->language->get('reward_error_points'), $this->request->post['reward']);
			} else if ($this->request->post['reward'] < $min_points) {
				$json['error']['error'] = sprintf(($this->language->get('min_reward_error_points').$this->config->get('reward_point_min')), $this->request->post['reward']);
			} else if ($this->request->post['reward'] > $max_points) {
				$json['error']['error'] = sprintf(($this->language->get('max_reward_error_points').$this->config->get('reward_point_max')), $max_points);
			} else {
				$json['applied'] = $this->language->get('reward_successful');
				$this->session->data['reward'] = abs($this->request->post['reward']);
			}
			$child = $this->xtensions_checkout->getChildren(array($this->config->get('xtensions_controller_path').'xcvc/xtotals'));
			$json['xtotals'] = $child['xtotals'];
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		} else {
			if ($reward > $points_total) {
				return $points_total;
			} else {
				return $reward;
			}
		}
	}

	public function addComment() {
		$json = array();
		$this->load->language($this->config->get('xtensions_language_path'));
		$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		$json['applied'] = $this->language->get('comment_saved');
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>
