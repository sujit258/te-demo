<?php

class ControllerExtensionModuleJoseanMatiasPrecoCupom extends Controller {

    public function listview($product_info = array()) {
        $text = '';

        $product_price = $this->tax->calculate((isset($product_info['special']) ? $product_info['special'] : $product_info['price']), $product_info['tax_class_id'], $this->config->get('config_tax'));

        if ((float) $product_price > 0 && $this->config->get('joseanmatias_preco_cupom_status')) {

            $outofstock = true;
            $showlist = true;
            $product_single = (isset($product_info['product_single']) ? true : false);

            if (((int) $product_info['quantity'] <= 0 || (int) $product_info['quantity'] < (int) $product_info['minimum']) && !$this->config->get('joseanmatias_preco_cupom_outofstock')) {
                $outofstock = false;
            }

            if (!$product_single && !$this->config->get('joseanmatias_preco_cupom_showlist')) {
                $showlist = false;
            }

            if ($outofstock && $showlist) {
                $this->load->model('catalog/product');

                $coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1' AND default1 = '1'");

                $coupon_discount = 0.0;
                $coupon_code = '';

                if ($coupon_query->num_rows) {
                    foreach ($coupon_query->rows as $coupon) {
                        $status = true;

                        if ($coupon['total'] > $product_price) {
                            $status = false;
                        }

                        $coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int) $coupon['coupon_id'] . "'");

                        if ($coupon['uses_total'] > 0 && ($coupon_history_query->row['total'] >= $coupon['uses_total'])) {
                            $status = false;
                        }

                        if ($coupon['logged'] && !$this->customer->getId()) {
                            $status = false;
                        }

                        if ($this->customer->getId()) {
                            $coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int) $coupon['coupon_id'] . "' AND ch.customer_id = '" . (int) $this->customer->getId() . "'");

                            if ($coupon['uses_customer'] > 0 && ($coupon_history_query->row['total'] >= $coupon['uses_customer'])) {
                                $status = false;
                            }
                        }

                        // Products
                        $coupon_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_product` WHERE coupon_id = '" . (int) $coupon['coupon_id'] . "'");

                        if ($coupon_product_query->num_rows) {
                            $product_data = array();

                            foreach ($coupon_product_query->rows as $coupon_product) {
                                $product_data[] = $coupon_product['product_id'];
                            }

                            if (!in_array($product_info['product_id'], $product_data)) {
                                $status = false;
                            }
                        }

                        // Categories
                        $coupon_category_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_category` WHERE coupon_id = '" . (int) $coupon['coupon_id'] . "'");

                        if ($coupon_category_query->num_rows) {
                            $product_categories = $this->model_catalog_product->getCategories($product_info['product_id']);

                            foreach ($product_categories as $category) {
                                $category_data[] = $category['category_id'];
                            }

                            foreach ($coupon_category_query->rows as $coupon_category) {
                                $coupon_category_data[] = $coupon_category['category_id'];
                            }

                            $result_diff = array_diff($category_data, $coupon_category_data);

                            if (count($result_diff) == count($category_data)) {
                                $status = false;
                            }
                        }

                        if ($status) {
                            if ($coupon['type'] == 'P') {
                                $current_discount = (float) ($product_price * ((float) $coupon['discount'] / 100));
                            } else {
                                $current_discount = (float) $coupon['discount'];
                            }

                            if ($current_discount > $coupon_discount) {
                                $coupon_discount = $current_discount;
                                $coupon_code = $coupon['code'];
                            }
                        }
                    }
                }

                if ((int) $coupon_discount) {
                    if ($product_single) {
                        $text_mask = html_entity_decode($this->config->get('joseanmatias_preco_cupom_text_page'));
                    } else {
                        $text_mask = html_entity_decode($this->config->get('joseanmatias_preco_cupom_text_list'));
                    }

                    $text = str_replace(array('[CUPOM]', '[PRECO]'), array($coupon_code, $this->currency->format($product_price - $coupon_discount, $this->session->data['currency'])), $text_mask);
                }
            }
        }

        return $text;
    }

}

?>