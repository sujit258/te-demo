<?php
class ControllerExtensionModuleOcfeatureslider extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/ocfeatureslider');
		
        $this->document->addStyle('catalog/view/javascript/css/wow.css');
        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
        
        //$this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/wow.js');
        $this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/addtocart/notice_add.min.js');
        
		$this->load->model('catalog/product');
		$this->load->model('catalog/ocproductrotator');
		$this->load->model('tool/image');
		$this->load->model('catalog/recipe');

		$data = array();

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_title'] = $this->language->get('text_title');

		$lang_code = $this->session->data['language'];

		if(isset($setting['title']) && $setting['title']) {
			$data['title'] = $setting['title'][$lang_code]['title'];
		} else {
			$data['title'] = $this->language->get('heading_title');
		}
		
		$cover_image =  $this->model_tool_image->resize($setting['image'], 456, 723);

		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_new'] = $this->language->get('text_new');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$data['products'] = array();
		
		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}

		$new_filter_data = array(
				'sort'  => 'p.date_added',
				'order' => 'DESC',
				'start' => 0,
				'limit' => 10
		);

		$new_results = $this->model_catalog_product->getProducts($new_filter_data);

		if(isset($setting['rotator']) && $setting['rotator']) {
			$product_rotator_status = (int) $this->config->get('ocproductrotator_status');
		} else {
			$product_rotator_status = 0;
		}

		/* recipes */

		$category_info = $this->model_catalog_recipe->getRecipeCategories();
        if($category_info) {
            foreach($category_info AS $recipe_category) {
                $data['recipe_categories'][] = array(
                    'category_id'   => $recipe_category['category_id'],
                    'name'          => $recipe_category['name'],
                    //'image'         => $this->model_tool_image->resize($recipe_category['image'], 480, 576),
                    'href'          => $this->url->link('recipe/recipe/category', 'recipe_category_id=' . $recipe_category['category_id'])
                );
            }

        }
        

		$products = array_slice($setting['product'], 0, (int)$setting['limit']);

		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info['image']) {
				$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
			} else {
				$image = false;
			}
			
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}

				if($product_rotator_status == 1) {
					$product_id = $product_info['product_id'];
					$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($product_id);

					if($product_rotator_image) {
						$rotator_image = $this->model_tool_image->resize($product_rotator_image, $setting['width'], $setting['height']);
					} else {
						$rotator_image = false;
					}
				} else {
					$rotator_image = false;
				}

				$is_new = false;
				if ($new_results) {
					foreach($new_results as $new_r) {
						if($product_info['product_id'] == $new_r['product_id']) {
							$is_new = true;
						}
					}
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price_num = $this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
				} else {
					$price_num = false;
				}

				if ((float)$product_info['special']) {
					$special_num = $this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'));
				} else {
					$special_num = false;
				}

				$data['products'][] = array(
					'product_id'  => $product_info['product_id'],
					'is_new' => $is_new,
					'joseanmatias_preco_cupom' => $this->load->controller('extension/module/joseanmatias_preco_cupom/listview', $product_info),
					'thumb'       => $image,
					'rotator_image' => $rotator_image,
					'name'        => html_entity_decode(substr($product_info['name'],0,55)),
					'name_mobile' => html_entity_decode(substr($result['name'],0,35)),
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'price_num'       => $price_num,
					'special_num'     => $special_num,
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
				);
			}
		}
		
		$data['config_slide'] = array(
				'items' => $setting['item'],
				'image' => $cover_image,
				'autoplay' => $setting['autoplay'],
				'f_show_nextback' => $setting['shownextback'],
				'f_show_ctr' => $setting['shownav'],
				'f_speed' => $setting['speed'],
				'f_show_label' => $setting['showlabel'],
				'f_show_price' => $setting['showprice'],
				'f_show_des' => $setting['showdes'],
				'f_show_addtocart' => $setting['showaddtocart'],
				'f_rows' => $setting['rows']
		);
		
		$data['deals_href'] = "/all-deals";
		$deals =  $this->model_catalog_product->getActiveDeals();
		$data['total_deals'] =  $this->model_catalog_product->getDealsCount();
		if($deals) {
		    foreach($deals AS $deal) {
		        $deal_product = $this->model_catalog_product->getProduct($deal['product_id']);
		        
		        if ($deal_product['image']) {
					$image = $this->model_tool_image->resize($deal_product['image'], 400, 400);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 400, 400);
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$deal_product['special'] ? $deal_product['special'] : $deal_product['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $deal_product['rating'];
				} else {
					$rating = false;
				}
				
				$options = $this->model_catalog_product->getProductOptions($deal['product_id']);
				$option_id = $deal['option_id'] ? $deal['option_id'] : NULL;
				$option_value_id = $deal['option_value_id'] ? $deal['option_value_id'] : NULL;
				
				$deal_sku = array();
				if($option_id && $option_value_id) {
				    $product_option_id = $this->model_catalog_product->getProductOptionId($deal['product_id'], $option_id, $option_value_id);
				    foreach($options AS $option) {
				        if($option['product_option_id'] == $product_option_id && $option['option_id'] == $option_id) {
				            foreach($option['product_option_value'] AS $option_value) {
				                $price_with_option = 0;
				                if($option_value['option_value_id'] == $option_value_id) {
				                    $deal_sku = array(
				                        'product_option_id'       => $product_option_id,
				                        'option_id'               => $option_id,
				                        'option_value_id'         => $option_value_id,
				                        'product_option_value_id' => $option_value['product_option_value_id'],
				                        'name'                    => $option_value['name'],
				                        'type'                    => $option['type'],
				                        'required'                => $option['required'],
				                        'price'                   => $option_value['price'],
				                        'price_prefix'            => $option_value['price_prefix']
				                    );
				                    if($option_value['price'] > 0 && $option_value['price_prefix'] == "+") {
				                        $price_with_option = $deal_product['price'] + $option_value['price'];
				                    } else if($option_value['price'] > 0 && $option_value['price_prefix'] == "-") {
				                        $price_with_option = $deal_product['price'] - $option_value['price'];
				                    }
				                    break;
				                }
				            }
				        }
				    }
				} else {
				    $price_with_option = 0;
				}
				
				if($price_with_option > 0) {
				    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					    $price = $this->currency->format($this->tax->calculate($price_with_option, $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					    
					    $mrp = $this->currency->format($this->tax->calculate($price_with_option, $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				        $discount = 100 - ($deal['price'] / filter_var($mrp, FILTER_SANITIZE_NUMBER_INT) * 100);
			    	} else {
					    $price = false;
				    }
				} else {
				    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					    $price = $this->currency->format($this->tax->calculate($deal_product['price'], $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					    
					    $mrp = $this->currency->format($this->tax->calculate($deal_product['price'], $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				        $discount = 100 - ($deal['price'] / filter_var($mrp, FILTER_SANITIZE_NUMBER_INT) * 100);
			    	} else {
					    $price = false;
				    }
				}

				if($product_rotator_status == 1) {
					$product_id = $deal_product['product_id'];
					$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($product_id);

					if($product_rotator_image) {
						$rotator_image = $this->model_tool_image->resize($product_rotator_image, $setting['width'], $setting['height']);
					} else {
						$rotator_image = false;
					}
				} else {
					$rotator_image = false;
				}
				
		        $data['deals_data'][] = array(
		            'product_id'    => $deal['product_id'],
		            'thumb'         => $image,
					'rotator_image' => $rotator_image,
		            'name'          => $deal_product['name'],//strlen($deal_product['name']) > 25 ? substr($deal_product['name'],0,25) . "..." : $deal_product['name'],
		            'price'         => $price,
		            'deal_price'    => $deal['price'],
		            'discount'      => round($discount) . "% OFF",
		            'deal_coupon'   => $deal['coupon'],
		            'date_start'    => $deal['date_start'],
		            'date_end'      => $deal['date_end'],
		            'href'          => $this->url->link('product/product', 'product_id=' . $deal['product_id'])
		        );
		    }
		}
		
			$this->load->model('catalog/recipe');
		$latest_recipes = $this->model_catalog_recipe->getLatestRecipes(8);
		
		foreach($latest_recipes AS $recipe) {
		    $data['recipes'][] = array(
		        'title' => strlen($recipe['title']) > 40 ? substr($recipe['title'], 0, 40) . '...' : $recipe['title'],
		        'image' => $this->model_tool_image->resize($recipe['homepage_image'], 1100, 520),
		        //'image' => '/image/' . $recipe['image'],
                'href'  => $this->url->link('recipe/recipe/info', 'recipe_id='. $recipe['recipe_id'])
		    );
		}
		$data['view_more_img'] = $this->model_tool_image->resize('catalog/show-more.jpg', 1100, 520);
		
		$this->load->model('catalog/media');
		$latest_media = $this->model_catalog_media->getLatestMedia(8);
		foreach($latest_media AS $media) {
		    $data['media_article'][] = array(
		        'media_name' => $media['media_name'],
		        'publish_date' => $media['publish_date'],
		        'content' => $media['content'],
		        'image' => $this->model_tool_image->resize($media['image'], 1140, 600),
		        //'image' => '/image/' . $recipe['image'],
                'href'  => $media['url']
		    );
		}
		
		$this->load->model('mpblog/mpblogpost');
		
		$latestBlogposts = $this->model_mpblog_mpblogpost->getLatestMpBlogPosts(8);
		
		foreach($latestBlogposts AS $post) {
		    $data['blogPosts'][] = array(
		        'title' => strlen($post['name']) > 60 ? substr($post['name'], 0, 60) . '...' : $post['name'],
		        'image' => $this->model_tool_image->resize($post['image'], 1140, 600),
		        'href'  => $this->url->link('mpblog/blog', 'mpblogpost_id='. $post['mpblogpost_id'])
		    );
		}
		
		return $this->load->view('extension/module/ocfeatureslider', $data);
	}
}