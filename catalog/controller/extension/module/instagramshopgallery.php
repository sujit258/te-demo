<?php
class ControllerExtensionModuleInstagramShopGallery extends Controller
{
    private $data = array();
    private $module = array();

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->config->load('isenselabs/instagramshopgallery');
        $this->module = $this->config->get('instagramshopgallery');

        $this->load->model('setting/setting');
        $this->load->model($this->module['path']);
        $this->module['model'] = $this->{$this->module['model']};

        // Module setting
        $setting = $this->model_setting_setting->getSetting($this->module['code'], $this->config->get('config_store_id'));
        $this->module['setting'] = array_replace_recursive(
            $this->module['setting'],
            !empty($setting[$this->module['code'] . '_setting']) ? $setting[$this->module['code'] . '_setting'] : array()
        );

        // Template variables
        $this->data['store_id'] = $this->config->get('config_store_id');
        $this->data['lang_id']  = $this->config->get('config_language_id');
        $this->data['setting']  = $this->module['setting'];
        $this->data['module']   = array(
            'name'  => $this->module['name'],
            'path'  => $this->module['path']
        );

        $language_vars = $this->load->language($this->module['path'], $this->module['name']);
        
        //$this->data = array_replace_recursive($this->data,$language_vars[$this->module['name']]->all());
    }

    // Module
    public function index()
    {
        if (!$this->module['setting']['status'] || !$this->module['setting']['hashtag'] || !$this->module['setting']['module']['status']) {
            return;
        }

        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        $this->document->addStyle('catalog/view/javascript/' . $this->module['name'] . '/swiper/css/swiper.min.css');
        $this->document->addStyle('catalog/view/javascript/' . $this->module['name'] . '/swiper/css/opencart.css');
        $this->document->addStyle('catalog/view/theme/default/stylesheet/' . $this->module['name'] . '.css?v=' . $this->module['version']);

        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addScript('catalog/view/javascript/' . $this->module['name'] . '/swiper/js/swiper.jquery.js');

        $data = $this->data;

        $data['theme']      = $this->config->get('config_theme');
        $data['custom_css'] = trim(htmlspecialchars_decode($this->module['setting']['module']['custom_css']));

        if ($this->config->get('theme_default_directory') == 'journal2') {
            $data['theme'] = 'journal2';
            $this->document->addStyle('catalog/view/theme/default/stylesheet/' . $this->module['name'] . '_bootstrap.css?v=' . $this->module['version']);
        }

        return $this->load->view($this->module['path'] . '/module', $data);
    }

    public function page()
    {
        if (!$this->module['setting']['status'] || !$this->module['setting']['hashtag'] || !$this->module['setting']['page']['status']) {
            $this->response->redirect($this->url->link('common/home', '', true));
        }

        $data       = $this->data;
        $lang_id    = $this->config->get('config_language_id');
        $meta_title = $this->module['setting']['page']['meta_title'][$lang_id] ? $this->module['setting']['page']['meta_title'][$lang_id] : $this->module['setting']['page']['title'][$lang_id];

        $this->document->setTitle($meta_title);
        $this->document->setDescription($this->module['setting']['page']['meta_desc'][$lang_id]);
        $this->document->setKeywords($this->module['setting']['page']['meta_keyword'][$lang_id]);

        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        $this->document->addStyle('catalog/view/javascript/' . $this->module['name'] . '/swiper/css/swiper.min.css');
        $this->document->addStyle('catalog/view/javascript/' . $this->module['name'] . '/swiper/css/opencart.css');
        $this->document->addStyle('catalog/view/theme/default/stylesheet/' . $this->module['name'] . '.css?v=' . $this->module['version']);

        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addScript('catalog/view/javascript/' . $this->module['name'] . '/swiper/js/swiper.jquery.js');

        $page_url = HTTPS_SERVER . 'index.php?route=' . $this->module['path'] . '/page';
        if ($this->config->get('config_seo_url') && !empty($this->module['setting']['page']['seo_url'][$lang_id])) {
            $page_url = HTTPS_SERVER . $this->module['setting']['page']['seo_url'][$lang_id];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', '', true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->module['setting']['page']['title'][$lang_id],
            'href' => $page_url
        );

        // ======

        $data['theme']          = $this->config->get('config_theme');
        $data['banner']         = $this->module['setting']['page']['banner'] ? HTTPS_SERVER . 'image/' . $this->module['setting']['page']['banner'] : '';
        $data['custom_css']     = trim(htmlspecialchars_decode($this->module['setting']['page']['custom_css']));

        if ($this->config->get('theme_default_directory') == 'journal2') {
            $data['theme'] = 'journal2';
            $this->document->addStyle('catalog/view/theme/default/stylesheet/' . $this->module['name'] . '_bootstrap.css?v=' . $this->module['version']);
        }

        $data['column_left']    = $this->load->controller('common/column_left');
        $data['column_right']   = $this->load->controller('common/column_right');
        $data['content_top']    = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer']         = $this->load->controller('common/footer');
        $data['header']         = $this->load->controller('common/header');
        
        //print_r ($data);
        
        $this->response->setOutput($this->load->view($this->module['path'] . '/page', $data));
    }


    public function fetch()
    {
        $param = $this->request->post;
        $data  = array(
            'photos'  => array(
                'items' => array(),
                'page'  => array()
            ),
            'param'   => $param,
            'setting' => $this->module['setting'],
            'module'  => array(
                'name'  => $this->module['name'],
                'path'  => $this->module['path']
            )
        );

        if (!$this->module['setting']['status'] || !$this->module['setting'][$param['type']]['status']) {
            return;
        }

        switch ($this->module['setting'][$param['type']]['visibility']) {
            case 'all':
                $param['page']  = isset($param['page']) ? $param['page'] : '';
                $data['photos'] = $this->fetchInstagram($param['type'], $param['page']);
                break;

            case 'approve':
            case 'product':
            case 'both':
                $param['page']  = !empty($param['page']) ? $param['page'] : 1;
                $data['photos'] = $this->fetchDatabase($param['type'], (int)$param['page']);
                break;
        }

        // Incase instagram server is not responding
        if (empty($data['photos'])) {
            $this->module['setting'][$param['type']]['visibility'] = 'approve';
            $data['photos'] = $this->fetchDatabase($param['type'], (int)$param['page']);
        }

        // Append extra image
        if ($param['type'] == 'module' && $this->module['setting']['module']['extra_image'] && is_file(DIR_IMAGE . $this->module['setting']['module']['extra_image'])) {
            $this->load->model('tool/image');

            array_pop($data['photos']['items']);
            
            $data['photos']['items'][1000000] = array(
                'image_thumb' => $this->model_tool_image->resize($this->module['setting']['module']['extra_image'], 480, 480),
                'url'         => $this->module['setting']['module']['extra_link'],
                'is_extra'    => true
            );
        }
        
        $output = $this->load->view('extension/module/instagramshopgallery/items.tpl', $data);
        //$output = $data;
        
        if ($param['type'] == 'module') {
            $this->response->setOutput($this->load->view('extension/module/instagramshopgallery/items.tpl', $data));
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode(array(
                'output'      => $output,
                'page_info'   => $data['photos']['page'],
            )));
        }
    }

    public function modal()
    {
        $param = $this->request->get;
        $data  = $this->data;
        $data['products'] = array();
        $data['carousel'] = false;

        if (!$this->module['setting']['status'] || !$param) {
            return;
        }

        if (isset($param['shortcode'])) {
            $photoFeed     = array();
            $photoInfo     = $this->module['model']->getPhoto($param['shortcode']);
            $data['photo'] = $photoInfo ? $photoInfo : $param;

            try {
                $photoFeed = json_decode(file_get_contents('https://www.instagram.com/p/' . $param['shortcode'] . '/?__a=1'), true);
            } catch (Exception $e) {
                // silent
            }

            if (!empty($photoFeed)) {
                $photoData = $photoFeed['graphql']['shortcode_media'];
                $data['photo']['username'] = $photoData['owner']['username'];
                $data['photo']['fullname'] = $photoData['owner']['full_name'];

                // Latest photo data
                $data['photo']['like']    = $photoData['edge_media_preview_like']['count'];
                $data['photo']['comment'] = $photoData['edge_media_to_comment']['count'];
                $data['photo']['caption'] = isset($photoData['edge_media_to_caption']['edges'][0]['node']['text']) ? $photoData['edge_media_to_caption']['edges'][0]['node']['text'] : '';
            }

            $caption = str_replace(array('“', '”', "`", "'"), '', $data['photo']['caption']);
            $caption = preg_replace('~#[\w-]+~', '', $caption); // Remove hashtags in caption
            $data['photo']['caption'] = nl2br(htmlspecialchars_decode($caption));
            $data['photo']['date']    = $this->timeElapsedString('@'. $data['photo']['timestamp']);

            // Related products
            $image_size = 150;
            $products   = $this->module['model']->getRelatedProduct($param['shortcode']);

            if ($products) {
                $this->load->model('tool/image');
                $this->load->model('catalog/product');


                foreach ($products as $product) {
                    if ($product['image']) {
                        $image = $this->model_tool_image->resize($product['image'], $image_size, $image_size);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $image_size, $image_size);
                    }

                    $price = false;
                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    }

                    $special = false;
                    if ((float)$product['special']) {
                        $special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    }

                    $tax = false;
                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']);
                    }
                    
                    $personalized = 0;
    				$options = NULL;
    				if($product['product_id'] != 6438 && $product['product_id'] != 6345 && $product['product_id'] != 6449) {
    				    $options = $this->model_catalog_product->getProductOptions($product['product_id']);
    				} else {
    				    $personalized = 1;
    				}

                    $rating = false;
                    if ($this->config->get('config_review_status')) {
                        $rating = (int)$product['rating'];
                    }

                    $data['products'][] = array(
                        'product_id'  => $product['product_id'],
                        'thumb'       => $image,
                        'name'        => $product['name'],
                        'price'       => $price,
                        'quantity'    => $product['quantity'],
                        'special'     => $special,
                        'tax'         => $tax,
                        'minimum'     => $product['minimum'] > 0 ? $product['minimum'] : 1,
                        'rating'      => $rating,
                        'options'     => $options,
				    	'personalized' => $personalized,
                        'href'        => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                    );
                }
            }

            $data['carousel'] = count($products) > 1 ? true : false;

            $data['theme'] = $this->config->get('config_theme');
            if ($this->config->get('theme_default_directory') == 'journal2') {
                $data['theme'] = 'journal2';
            }

            $output = $this->load->view($this->module['path'] . '/modal', $data);

        } else {
            $output = '<div class="islip-p100">' . $data['error_general'] . '</div>';
        }

        $this->response->setOutput($output);
    }

    // Get photo from Instagram server
    private function fetchInstagram($type = 'module', $page = '', $photos = array(), $index = 100)
    {
        $limit  = $this->module['setting'][$type]['limit'];
        $photosPageInfo = array();

        try {
            @$photosFeed = json_decode(file_get_contents('https://www.instagram.com/explore/tags/'.$this->module['setting']['hashtag'].'/?__a=1&max_id=' . $page), true);
        } catch (Exception $e) {
            return $photos;
        }

        if (!empty($photosFeed)) {
            $photosMedia    = $photosFeed['graphql']['hashtag']['edge_hashtag_to_media'];
            $photosPageInfo = $photosMedia['page_info'];
            $photosItems    = $photosMedia['edges'];
            $photosCount    = count($photosItems);

            $x = 0;
            for ($i=0; $i < $photosCount; $i++) {
                if ($x == $limit) { break; }

                $caption = isset($photosItems[$i]['node']['edge_media_to_caption']['edges'][0]['node']['text']) ? $photosItems[$i]['node']['edge_media_to_caption']['edges'][0]['node']['text'] : '';

                $photos[$index + $i] = array(
                    'owner'          => $photosItems[$i]['node']['owner']['id'],
                    'image_original' => $photosItems[$i]['node']['display_url'],
                    'image_large'    => $photosItems[$i]['node']['thumbnail_src'], // 640 x 640
                    'image_thumb'    => $photosItems[$i]['node']['thumbnail_resources'][3]['src'], // 480 x 480
                    'caption'        => preg_replace('~#[\w-]+~', '', $caption), // Remove hashtags in caption
                    'shortcode'      => $photosItems[$i]['node']['shortcode'],
                    'timestamp'      => $photosItems[$i]['node']['taken_at_timestamp'],
                    'is_extra'       => false
                );

                $x++;
            }

            
            // Subrequest - until meet setting limit
            if (count($photos) < $limit) {
                $subrequest = $this->fetchInstagram($type, $photosPageInfo['end_cursor'], $photos, $index + 100);

                $photos = $subrequest['items'];
                $photosPageInfo = $subrequest['page'];
            }
        }

        return array(
            'items' => $photos,
            'page'  => $photosPageInfo
        );
    }

    // Get photo from database
    private function fetchDatabase($type = 'module', $page = 1)
    {
        $photos = array();
        $limit  = $this->module['setting'][$type]['limit'];

        switch ($this->module['setting'][$type]['visibility']) {
            case 'approve':
                $results = $this->module['model']->getPhotos($page, $limit, 'isg.shortcode, isg.image_thumb, isg.image_original', 'isg.approve = 1', '');
                break;

            case 'product':
                $results = $this->module['model']->getPhotos($page, $limit, 'isg.shortcode, isg.image_thumb, isg.image_original', '', 'HAVING related_product > 0');
                break;

            case 'both':
                $results = $this->module['model']->getPhotos($page, $limit, 'isg.shortcode, isg.image_thumb, isg.image_original', 'isg.approve = 1', 'HAVING related_product > 0');
                break;
        }

        $x = 0;
        foreach ($results as $key => $value) {
            if ($x == $limit) { break; }

            if (!empty($results[$key]['image_thumb']) && $this->isValidRemoteFile($results[$key]['image_thumb'])) {
                $photos[$key] = $results[$key];
                $photos[$key]['is_extra'] = false;

                $x++;
            }
        }

        $totalPhotos = $this->module['model']->getPhotosTotal();

        return array(
            'items' => $photos,
            'page'  => array(
                'has_next_page' => ($limit * $page) < $totalPhotos ? true : false,
                'end_cursor'    => $page + 1,
            )
        );
    }

    /**
     * Convert datetime to elapsed time lang
     * @link https://stackoverflow.com/a/18602474
     */
    private function timeElapsedString($datetime, $level = 2)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        $string = array_slice($string, 0, $level);

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    /**
     * Check if remote file response is 200
     */
    private function isValidRemoteFile($url)
    {
        $headers = get_headers($url);

        if (!empty($headers) && strpos($headers[0], '200')) {
            return true;
        }
        return false;
    }

    // Events
    // =====================================================

    // Custom page seo
    // catalog/view/common/menu/before
    public function navbar(&$route, &$data, &$output)
    {
        if ($this->module['setting']['status'] && $this->module['setting']['page']['status']) {
            $page_url = HTTPS_SERVER . 'index.php?route=' . $this->module['path'] . '/page';
            if ($this->config->get('config_seo_url') && !empty($this->module['setting']['page']['seo_url'][$this->config->get('config_language_id')])) {
                $page_url = HTTPS_SERVER . $this->module['setting']['page']['seo_url'][$this->config->get('config_language_id')];
            }

            $data['categories'][1000] = array(
                'name'      => $this->module['setting']['page']['title'][$this->config->get('config_language_id')],
                'href'      => $page_url,
                'column'    => 1,
                'children'  => array(),
            );
        }
    }

    // Custom page seo
    // catalog/controller/error/not_found/before
    public function seo(&$route, &$data)
    {
        if (isset($this->request->get['_route_']) && !empty($this->module['setting']['page']['seo_url']) && $this->request->get['_route_'] == $this->module['setting']['page']['seo_url'][$this->config->get('config_language_id')]) {
            $this->request->get['route'] = $this->module['path'] . '/page';
            return new Action($this->request->get['route']);
        }
    }
}
