<?php
class ControllerExtensionModuleOctabcategoryslider extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/octabcategoryslider');
		
		// Added to cartbookmark
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js');
		// Added to cart

		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$this->load->model('catalog/ocproductrotator');
		$this->load->model('tool/image');

		if (file_exists('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/opentheme/categorytabslider.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/opentheme/categorytabslider.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/opentheme/categorytabslider.css');
		}

		$data = array();

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_title'] = $this->language->get('text_title');


        $data['tracking_code'] =  $setting['tracking_code'];
        
		$lang_code = $this->session->data['language'];

		if(isset($setting['title']) && $setting['title']) {
			$data['title'] = $setting['title'][$lang_code]['title'];
		} else {
			$data['title'] = $this->language->get('heading_title');
		}
		
		$data['text_tax'] = $this->language->get('text_tax');

		if (empty($setting['limit'])) {
			$setting['limit'] = 10;
		}

		if(isset($setting['rotator']) && $setting['rotator']) {
			$product_rotator_status = (int) $this->config->get('ocproductrotator_status');
		} else {
			$product_rotator_status = 0;
		}

		if(isset($setting['thumbnail']) && $setting['thumbnail']) {
			$use_thumbnail = true;
		} else {
			$use_thumbnail = false;
		}

		$filter_data = array(
			'rotator_status' => $product_rotator_status,
			'sort'  => 'p.sort_order',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$arrayCates = $setting['product_category'];
		
		$arrayCateName = array();
		$arrayExisted = array();
		foreach ($arrayCates as $cate_id) {
			$categories_info = $this->model_catalog_category->getCategory($cate_id);
			$arrayExisted[] = $cate_id;
			if(isset($categories_info['name'])) {
				$category_title = $categories_info['name'];
				$arrayCateName[$cate_id]['name'] = $category_title;
			}
			if($use_thumbnail) {
				if(isset($categories_info['thumbnail_image'])) {
					$arrayCateName[$cate_id]['thumbnail_image'] = $this->model_tool_image->resize($categories_info['thumbnail_image'], 200, 200);
				} else {
					$arrayCateName[$cate_id]['thumbnail_image'] = false;
				}
				if(isset($categories_info['homethumb_image'])) {
					$arrayCateName[$cate_id]['homethumb_image'] = $this->model_tool_image->resize($categories_info['homethumb_image'], 370, 416);
				} else {
					$arrayCateName[$cate_id]['homethumb_image'] = false;
				}
			} else {
				$arrayCateName[$cate_id]['thumbnail_image'] = false;
				$arrayCateName[$cate_id]['homethumb_image'] = false;
			}
			
			//$arrayCateName[$cate_id]['bookmark'] = $categories_info['bookmark'];
			
			$arrayCateName[$cate_id]['bookmark'] = $this->url->link('product/category', 'path='.$cate_id);
	 
		}
		
		$productByCates = array();
		foreach($arrayCates as $cate_id) {
			if(in_array($cate_id,$arrayExisted)) {
				$filter_data['filter_category_id'] = $cate_id;
				$results = $this->getProductFromData($filter_data,$setting);
				$productByCates[$cate_id] = $results ;
			}			
		}
		
		$fproductByCates = array();
		foreach($arrayCates as $cate_id) {
			if(in_array($cate_id,$arrayExisted)) {
				$filter_data['filter_category_id'] = $cate_id;
				$result = $this->getFirstProductFromData($filter_data,$setting);
				$fproductByCates[$cate_id] = $result;
			}			
		}
		
		$productEFByCates = array();
		foreach($arrayCates as $cate_id) {
			if(in_array($cate_id,$arrayExisted)) {
				$filter_data['filter_category_id'] = $cate_id;
				$results = $this->getProductExceptFirstFromData($filter_data,$setting);
				$productEFByCates[$cate_id] = $results ;
			}			
		}
		
		$data['config_slide'] = array(
			'items' => $setting['item'],
			'tab_cate_autoplay' => $setting['autoplay'],
			'tab_cate_show_nextback' => $setting['shownextback'], 
			'tab_cate_show_ctr' => $setting['shownav'], 
			'tab_cate_speed_slide' => $setting['speed'],
			'tab_cate_show_price' => $setting['showprice'],
			'tab_cate_show_label' => $setting['showlabel'],
			'tab_cate_show_des' => $setting['showdes'],
			'tab_cate_show_addtocart' => $setting['showaddtocart'],
			'f_rows' => $setting['rows'],
            'tab_name' => $setting['name']
		);

		$alias = str_replace(' ','_',$setting['name']);
		$data['category_alias'] = $alias;
		$data['array_cates'] = $arrayCateName; 
		$data['tab_effect'] = 'wiggle';
		$data['category_products'] = $productByCates;
		$data['category_e_f_products'] = $productEFByCates;
		$data['f_products'] = $fproductByCates;

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_new'] = $this->language->get('text_new');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		
		if ($data['category_products']) {
			return $this->load->view('extension/module/octabcategoryslider', $data);
		}
	}

	public function getProductFromData($data= array(), $setting = array()) {

		if($data['rotator_status']) {
			$product_rotator_status = (int) $this->config->get('ocproductrotator_status');
		} else {
			$product_rotator_status = 0;
		}

		/* Get new product */
		$filter_data = array(
				'sort'  => 'p.sort_order',
				'order' => 'DESC',
				'start' => 0,
				'limit' => 10
		);

		$new_results = $this->model_catalog_product->getProducts($filter_data);
		/* End */
		
		$deal_data = array();
		$deals_filter_data = array(
		    'limit' => 100    
	    );
	    $active_deals = $this->model_catalog_product->getDeals($deals_filter_data);
        if($active_deals) {
            foreach($active_deals AS $deal) {
                $deal_data[$deal['product_id']] = $deal['coupon'];
            }
        }

		$results = $this->model_catalog_product->getcategoryProducts($data);
		
		$product_list = array();
		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
			} else {
				$image = false;
			}
						
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price = false;
			}
					
			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$special = false;
			}
			
			if ($this->config->get('config_review_status')) {
				$rating = $result['rating'];
			} else {
				$rating = false;
			}
			
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price_num = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
			} else {
				$price_num = false;
			}

			if ((float)$result['special']) {
				$special_num = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'));
			} else {
				$special_num = false;
			}

			$is_new = false;
			if ($new_results) {
				foreach($new_results as $new_r) {
					if($result['product_id'] == $new_r['product_id']) {
						$is_new = true;
					}
				}
			}

			if($product_rotator_status == 1) {
				$product_id = $result['product_id'];
				$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($result['product_id']);

				if($product_rotator_image) {
					$rotator_image = $this->model_tool_image->resize($product_rotator_image, $setting['width'], $setting['height']);
				} else {
					$rotator_image = false;
				}
			} else {
				//$rotator_image = false;
				$product_id = $result['product_id'];
				$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($result['product_id']);

				if($product_rotator_image) {
					$rotator_image = $this->model_tool_image->resize($product_rotator_image, $setting['width'], $setting['height']);
				} else {
					$rotator_image = false;
				}
			}
			
			//new
			$personalized = 0;
			$options = NULL;
			if($result['product_id'] != 6438 && $result['product_id'] != 6345 && $result['product_id'] != 6449) {
			    $options = $this->model_catalog_product->getProductOptions($result['product_id']);
			} else {
			    $personalized = 1;
			}
			
				
    		$name = html_entity_decode(substr($result['name'],0,55));
    		
			 if(strlen($result['name']) > 55) {
			        $name .= "...";
			 }
			 
			if(array_key_exists($result['product_id'],$deal_data) && $result['quantity'] > 0) {
		        $coupon = $deal_data[$result['product_id']];
		    } else {
		        $coupon = '';
		    }
		    
		    if($coupon){
    		        $coupon_query = $this->db->query("SELECT type,discount FROM " . DB_PREFIX . "coupon WHERE code = '" . $coupon . "'");
    		        $type     = $coupon_query->row['type'];
    		        $discount = $coupon_query->row['discount'];
    		        
    		        $product_price = $result['special'] ? ($result['special']) : ($result['price']);
    		        
    		        if ($type == 'P') {
                        $current_discount = (float) ($product_price * ((float) $discount / 100));
                    } else {
                        $current_discount = (float) $discount;
                    }
                    
                    $coupon_discount = $this->currency->format(((float)$product_price - $current_discount),$currency_code);
                    
                    $coupon_discount = $this->currency->format($this->tax->calculate(($product_price - $current_discount), $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
		        } else {
		            $coupon_discount = '';
		        } 

			$product_list[] = array(
				'product_id' => $result['product_id'],
                'is_new'      => $is_new,
				'thumb'   	 => $image,
				'rotator_image' => $rotator_image,
				'name'    	 => $name,
				'name_mobile' => html_entity_decode(substr($result['name'],0,35)),
				'coupon'      => $coupon,
				'discount'      => $discount + 2,
     			'coupon_discount'        => $coupon_discount,
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 80) . '..',
				'options'     => $options,
				'personalized' => $personalized,
				'quantity'     => $result['quantity'],
				'price'   	 => substr($price,3) > 0 ? $price : "",
				'special' 	 => $special,
			    //'joseanmatias_preco_cupom' => $this->load->controller('extension/module/joseanmatias_preco_cupom/listview', $result),
				'price_num'       => $price_num,
				'special_num'     => $special_num,
				'rating'     => $rating,
				'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
				'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
			);
		}
		
		return $product_list;
	}
	
	public function getFirstProductFromData($data= array(), $setting = array()) {

		if($data['rotator_status']) {
			$product_rotator_status = (int) $this->config->get('ocproductrotator_status');
		} else {
			$product_rotator_status = 0;
		}

		/* Get new product */
		$filter_data = array(
				'sort'  => 'p.sort_order',
				'order' => 'DESC',
				'start' => 0,
				'limit' => 10
		);

		$new_results = $this->model_catalog_product->getProducts($filter_data);
		/* End */

		$results = $this->model_catalog_product->getProducts($data);
		
		$f_product = array();
		$count = 0;
		
		foreach ($results as $result) {
			if($count > 0) break;
			
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
			} else {
				$image = false;
			}
						
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price = false;
			}
					
			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$special = false;
			}
			
			if ($this->config->get('config_review_status')) {
				$rating = $result['rating'];
			} else {
				$rating = false;
			}
			
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price_num = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
			} else {
				$price_num = false;
			}

			if ((float)$result['special']) {
				$special_num = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'));
			} else {
				$special_num = false;
			}

			$is_new = false;
			if ($new_results) {
				foreach($new_results as $new_r) {
					if($result['product_id'] == $new_r['product_id']) {
						$is_new = true;
					}
				}
			}

			if($product_rotator_status == 1) {
				$product_id = $result['product_id'];
				$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($product_id);

				if($product_rotator_image) {
					$rotator_image = $this->model_tool_image->resize($product_rotator_image, $setting['width'], $setting['height']);
				} else {
					$rotator_image = false;
				}
			} else {
				$rotator_image = false;
			}

			if($count == 0) {
				$f_product['product_id'] = $result['product_id'];
                $f_product['is_new']      = $is_new;
				$f_product['thumb']   	 = $image;
				$f_product['rotator_image'] = $rotator_image;
				$f_product['name']    	 = $result['name'];
				$f_product['description'] = utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..';
				$f_product['price']   	 = $price;
				
				$f_product['joseanmatias_preco_cupom']   	 =  $this->load->controller('extension/module/joseanmatias_preco_cupom/listview', $product_info);
				
				$f_product['special'] 	 = $special;
				$f_product['price_num']       = $price_num;
				$f_product['special_num']     = $special_num;
				$f_product['rating']     = $rating;
				$f_product['reviews']    = sprintf($this->language->get('text_reviews'), (int)$result['reviews']);
				$f_product['href']    	 = $this->url->link('product/product', 'product_id=' . $result['product_id']);
			}
			
			$count++;
		}
		
		return $f_product;
	}
	
	public function getProductExceptFirstFromData($data, $setting) {
		$productFromData = $this->getProductFromData($data, $setting);
		$firstProduct = $this->getFirstProductFromData($data, $setting);
		$productsExceptFList = array();
		
		foreach($productFromData as $product) {
			if($product['product_id'] == $firstProduct['product_id']) continue;
			
			$productsExceptFList[] = $product;
		}
		
		return $productsExceptFList;
	}
		
	
}