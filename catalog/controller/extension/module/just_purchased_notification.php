<?php
class ControllerExtensionModuleJustPurchasedNotification extends Controller {
	public function index($setting) {
		$this->document->addScript('catalog/view/javascript/jquery/bootstrap-notify.min.js');
		
		if (file_exists('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/just_purchased_notification.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/just_purchased_notification.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/just_purchased_notification.css');
		}
		
		if (file_exists('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/animate.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/animate.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/animate.css');
		}
		
		// get and process notifications
		$find = array(
			'{country}',
			'{zone}',
			'{city}',
			'{quantity}',
			'{product_with_link}'
		);
		
		$jpnl = $this->config->get('just_purchased_notification_localisation');
		$jpnl_current = $jpnl[$this->config->get('config_language_id')];
		
		$use_cache = $this->config->get('just_purchased_notification_cache');
		$shuffle = $this->config->get('just_purchased_notification_shuffle');
		
		if ($this->config->get('just_purchased_notification_hide_older')){
			$hide_older = (int)$this->config->get('just_purchased_notification_hide_older');
		} else {
			$hide_older = false;
		}
		
		$this->load->model('extension/module/just_purchased_notification');
		$this->load->model('tool/image');
		
		$notifications = $this->model_module_just_purchased_notification->getNotifications();
		
		if ($notifications) {
		
			if ($shuffle) {
				shuffle($notifications);
			}
			
			foreach($notifications as $notification) {
				$replace = array(
					'country' 			=> $notification['country'],
					'zone'    			=> $notification['zone'],
					'city'    			=> $notification['city'],
					'quantity'    	    => $notification['quantity'],
					'product_with_link' => $notification['product_name']
				);
				
				if ($notification['image']) {
					$image = $this->model_tool_image->resize($notification['image'], $this->config->get('just_purchased_notification_image_width'), $this->config->get('just_purchased_notification_image_height'));	
				} else {
					$image = $this->model_tool_image->resize('no_image.png', $this->config->get('just_purchased_notification_image_width'), $this->config->get('just_purchased_notification_image_height'));	
				}
				
				if ($use_cache) {
					$now = date('Y-m-d H:i:s');
					$date_added = $notification['date_added'];
					$seconds_ago = strtotime($now) - strtotime($date_added) - $notification['php_mysl_time_diff'];
					
				} else {
					$seconds_ago = $notification['time_ago'];
				}
				
				$show_time_ago = $this->config->get('just_purchased_notification_time_ago');
				
				if ($hide_older) {
					$older_seconds = $hide_older * 60 * 60;
					
					if ($seconds_ago > $older_seconds) {
						$show_time_ago = 0; // 0 = false
					}					
				} 
				
				$data['notifications'][] = array(
					'image'         => $image,
					'message'       => str_replace($find, $replace, $jpnl_current['message']),
					'time_ago'      => $this->timeAgoSecondsToText($seconds_ago),
					'product_href'  => $this->url->link('product/product', 'product_id=' . $notification['product_id']),
					'show_time_ago' => $show_time_ago
				);
			}
			
			// get ext settings
			$data['alert_type'] = $this->config->get('just_purchased_notification_alert_type');
			$data['placement_from'] = $this->config->get('just_purchased_notification_placement_from');
			$data['placement_align'] = $this->config->get('just_purchased_notification_placement_align');
			$data['allow_dismiss'] = $this->config->get('just_purchased_notification_allow_dismiss');
			$data['show_progressbar'] = $this->config->get('just_purchased_notification_show_progressbar');
			$data['animate_enter'] = $this->config->get('just_purchased_notification_animate_enter');
			$data['animate_exit'] = $this->config->get('just_purchased_notification_animate_exit');
			$data['zindex'] = $this->config->get('just_purchased_notification_zindex');
			$data['delay'] = $this->config->get('just_purchased_notification_delay') * 1000;
			$data['timeout'] = $this->config->get('just_purchased_notification_timeout') * 1000;
			
			// custom colors
			$data['background_color'] = $this->config->get('just_purchased_notification_background_color');
			$data['border_color'] = $this->config->get('just_purchased_notification_border_color');
			$data['text_color'] = $this->config->get('just_purchased_notification_text_color');
			$data['link_color'] = $this->config->get('just_purchased_notification_link_color');

			return $this->load->view('extension/module/just_purchased_notification.tpl', $data);
		}			
	}
	
	private function timeAgoSecondsToText($seconds) {
		$jpnl = $this->config->get('just_purchased_notification_localisation');
		$jpnl_current = $jpnl[$this->config->get('config_language_id')];
		
		$minutes = floor($seconds / 60);
		
		if ($minutes < 1) {
			$minutes = 1;
		}
		
		$hours   = floor($seconds / 60 / 60);
		$days    = floor($seconds / 60 / 60 / 24);
		
		if ( $minutes <= 59) {
			return str_replace("{time_counter}", $minutes, $jpnl_current['time_ago_minute']);
		} elseif ($hours <= 23) {
			return str_replace("{time_counter}", $hours, $jpnl_current['time_ago_hour']);
		} else {
			return str_replace("{time_counter}", $days, $jpnl_current['time_ago_day']);
		}
	}
}
?>