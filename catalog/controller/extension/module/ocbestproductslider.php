<?php
class ControllerExtensionModuleOcbestproductslider extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/ocbestproductslider');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data = array();

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_new'] = $this->language->get('text_new');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

        $data['tracking_code'] =  $setting['tracking_code'];
        
		$data['products'] = array();

		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}

		$lang_code = $this->session->data['language'];

		if(isset($setting['title']) && $setting['title']) {
			$data['title'] = $setting['title'][$lang_code]['title'];
		} else {
			$data['title'] = $this->language->get('heading_title');
		}

		$data['new_products'] = array();

		$new_filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 10
		);

		$new_results = $this->model_catalog_product->getProducts($new_filter_data);

		if(isset($setting['rotator']) && $setting['rotator']) {
			$product_rotator_status = (int) $this->config->get('ocproductrotator_status');
		} else {
			$product_rotator_status = 0;
		}
		
		$deal_data = array();
		$deals_filter_data = array(
		    'limit' => 100    
	    );
	    $active_deals = $this->model_catalog_product->getDeals($deals_filter_data);
        if($active_deals) {
            foreach($active_deals AS $deal) {
                $deal_data[$deal['product_id']] = $deal['coupon'];
            }
        }

		$results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				if($product_rotator_status == 1) {
					$this->load->model('catalog/ocproductrotator');
					
					$product_id = $result['product_id'];
					$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($product_id);

					if($product_rotator_image) {
						$rotator_image = $this->model_tool_image->resize($product_rotator_image, $setting['width'], $setting['height']);
					} else {
						$rotator_image = false;
					}
				} else {
					$rotator_image = false;
				}

				/* End */
				$is_new = false;
				if ($new_results) {
					foreach($new_results as $new_r) {
						if($result['product_id'] == $new_r['product_id']) {
							$is_new = true;
						}
					}
				}
				
					
    			//new
    			$personalized = 0;
    			$options = NULL;
    			if($result['product_id'] != 6438 && $result['product_id'] != 6345 && $result['product_id'] != 6449) {
    			    $options = $this->model_catalog_product->getProductOptions($result['product_id']);
    			} else {
    			    $personalized = 1;
    			}
    			
    			$name = html_entity_decode(substr($result['name'],0,55));
			    if(strlen($result['name']) > 55) {
			        $name .= "...";
			    }
			    
			    if(array_key_exists($result['product_id'],$deal_data) && $result['quantity'] > 0) {
		            $coupon = $deal_data[$result['product_id']];
		        } else {
		            $coupon = '';
		        }

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'is_new' => $is_new,
					'thumb'       => $image,
					'options'     => $options,
					'rotator_image' => $rotator_image,
					'name'        => $name,
					'name_mobile' => html_entity_decode(substr($result['name'],0,35)),
					'coupon'      => $coupon,
					'quantity'        => $result['quantity'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}
		}

		$data['config_slide'] = array(
			'items' => $setting['item'],
			'autoplay' => $setting['autoplay'],
			'f_show_nextback' => $setting['shownextback'],
			'f_show_ctr' => $setting['shownav'],
			'f_speed' => $setting['speed'],
			'f_show_price' => $setting['showprice'],
			'f_show_des' => $setting['showdes'],
			'f_show_label' => $setting['showlabel'],
			'f_show_addtocart' => $setting['showaddtocart'],
			'f_rows' => $setting['rows']
		);
		
		if($this->customer->getId() && $this->customer->getId() > 0) {
		        $this->load->model('account/order');
		        $order_products = $this->model_account_order->getShippedOrderProductsByCustomerId($this->customer->getId());
		        if($order_products) {
		            $deal_data = array();
		            $deals_filter_data = array(
		                'limit' => 100    
	                );
	                $active_deals = $this->model_catalog_product->getDeals($deals_filter_data);
                    if($active_deals) {
                        foreach($active_deals AS $deal) {
                            $deal_data[$deal['product_id']] = $deal['coupon'];
                        }
                    }
                    
		            foreach($order_products AS $order_product) {
		                $result = $this->model_catalog_product->getProduct($order_product['product_id']);
		                if ($result['image']) {
				        	$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				        } else {
				    	    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				        }

				        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				        	$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				        } else {
				        	$price = false;
				        }
    
				        if ((float)$result['special']) {
				        	$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				        } else {
    				    	$special = false;
				        }
    
				        if ($this->config->get('config_tax')) {
				        	$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				        } else {
    				    	$tax = false;
				        }
    
				        if ($this->config->get('config_review_status')) {
    				    	$rating = (int)$result['rating'];
				        } else {
    				    	$rating = false;
				        }
    				
				        $personalized = 0;
    				    $options = NULL;
				        if($result['product_id'] != 6438 && $result['product_id'] != 6345 && $result['product_id'] != 6449) {
    				        $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				        } else {
    				        $personalized = 1;
				        }
    				
				        $percent = 0;
    				    if($result['special'] > 0) {
				            $percent = round(100 - (($result['special'] / $result['price'])) * 100 ,0 ) > 0 ? sprintf($this->language->get('%s'), (round(100 - (($result['special'] / $result['price'])) * 100 ,0 ))) . '% OFF' : '';
    				    }
    				    if(array_key_exists($result['product_id'],$deal_data) && $result['quantity'] > 0) {
		                    $coupon = $deal_data[$result['product_id']];
		                } else {
		                    $coupon = '';
		                }
				        $data['order_history_products'][] = array(
				            'percent'       => $percent ? $percent : '',
    					    'product_id'    => $result['product_id'],
				    	    'thumb'         => $image,
     					    'name'          => html_entity_decode(substr($result['name'],0,55)),
     					    'name_mobile' => html_entity_decode(substr($result['name'],0,35)),
     					    'coupon'      => $coupon,
				    	    'description'   => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
    					    'options'       => $options,
				    	    'personalized'  => $personalized,
    					    'quantity'      => $result['quantity'],
				    	    'price'         => substr($price,3) > 0 ? $price : "",
    					    'price1'        => $this->load->controller('extension/module/joseanmatias_preco_cupom/listview', $result),
				    	    'special'       => $special,
     					    'tax'           => $tax,
				    	    'minimum'       => $result['minimum'] > 0 ? $result['minimum'] : 1,
    					    'rating'        => $result['rating'],
					        'href'          => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				        );
		            }
		        }
	        }

		return $this->load->view('extension/module/ocbestproductslider', $data);
	}
}