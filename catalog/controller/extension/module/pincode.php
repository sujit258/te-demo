<?php
class ControllerExtensionModulePincode extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/category');

		$data['heading_title'] = $this->language->get('heading_title');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/extension/module/pincode.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/extension/module/pincode.tpl', $data);
		} else {
			return $this->load->view('default/template/extension/module/pincode.tpl', $data);
		}
	}
	
	public function pinCheck() {
		$this->load->model('catalog/pincode');
		$pin = array();
			if(isset($_POST['pincode'])){
						$_SESSION['pin_check_status'] = "1";
						$pincode = addslashes($_POST['pincode']);
						$_SESSION['pincode'] = "<b>".addslashes($_POST['pincode'])."</b>";
						$pin = $this->model_catalog_pincode->getPin($pincode);
						$message_cod = $message_pre = $message_not = '';
						$message_cod = html_entity_decode($this->config->get('pincodesetting_cod_msg'), ENT_QUOTES, 'UTF-8');
						$message_pre = html_entity_decode($this->config->get('pincodesetting_prepaid_msg'), ENT_QUOTES, 'UTF-8'); 
						$message_not = html_entity_decode($this->config->get('pincodesetting_no_service_msg'), ENT_QUOTES, 'UTF-8'); 
						$text_color = $this->config->get('pincodesetting_text_color'); 
						
						if(!isset($message_cod) || $message_cod == ''){
							$message_cod  = "COD and Prepaid Service is Available At Your Location";
						}
						if(!isset($message_pre) || $message_pre == ''){
							$message_pre  = "Only Prepaid Service is Available At Your Location";
						}
						if(!isset($message_not) || $message_not == ''){
							$message_not  = "Service is not Available at your location yet";
						}
			
					if(isset($pin['id'])){
				$service = $pin['service_available'];
				if($service == '1'){
					echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px 3px;float: left;padding: 8px 0 0 22px;height: 35px;'> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform1()' value='CHANGE' id='button-change' class='button'/></form></p>";
					echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -73px;float: left;padding: 8px 0 0 22px;height: 35px;'><b>Status :</b> <font color = '".$text_color."'>".$message_cod."</font></p>";
					echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -73px;float: left;padding: 8px 0 0 22px;height: 35px;'><b>Expected Delivery :</b> <font color = '".$text_color."'>".$pin['delivery_time']."</font></p>";
						
					$_SESSION['pin_check_result'] = "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -73px;float: left;padding: 8px 0 0 22px;height: 35px;'> <b>Status : </b> <font color = '".$text_color."'>".$message_cod."</font></p>";
					$_SESSION['pin_check_delivery'] = "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -73px;float: left;padding: 8px 0 0 22px;height: 35px;'> <b> Expected Delivery :</b> <font color = '".$text_color."'>".$pin['delivery_time']."</font></p>";
				}
				else if($service == '0'){
					echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px 3px;float: left;padding: 8px 0 0 22px;height: 35px;'> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform1()' value='CHANGE' id='button-change' class='button'/></form></p>";
					echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -73px;float: left;padding: 8px 0 0 22px;height: 35px;'><b>Status :</b> <font color = '".$text_color."'>".$message_pre."</font></p>";
					echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -73px;float: left;padding: 8px 0 0 22px;height: 35px;'><b>Expected Delivery :</b> <font color = '".$text_color."'>".$pin['delivery_time']."</font></p>";
					
					$_SESSION['pin_check_result'] = "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -73px;float: left;padding: 8px 0 0 22px;height: 35px;'> <b>Status : </b> <font color = '".$text_color."'>".$message_pre."</font></p>";
					$_SESSION['pin_check_delivery'] = "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -73px;float: left;padding: 8px 0 0 22px;height: 35px;'> <b> Expected Delivery :</b> <font color = '".$text_color."'>".$pin['delivery_time']."</font></p>";
				}
				else{
					echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px 3px;float: left;padding: 8px 0 0 22px;height: 35px;'> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform1()' value='CHANGE' id='button-change' class='button'/></form></p>";
					echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -34px;float: left;padding: 8px 0 0 22px;height: 35px;'><b>Status </b></b><font color = '".$text_color."'>".$message_not."</font></p>";
						
					$_SESSION['pin_check_result'] = "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -34px;float: left;padding: 8px 0 0 22px;height: 35px;'><b>Status </b></b> <font color = '".$text_color."'>".$message_not."</font></p>";
					$_SESSION['pin_check_delivery'] = '';
				}
			}
			else{
				echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px 3px;float: left;padding: 8px 0 0 22px;height: 35px;'> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform1()' value='CHANGE' id='button-change' class='button'/></form></p>";
				echo "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -34px;float: left;padding: 8px 0 0 22px;height: 35px;'><b>Status  </b> <font color = '".$text_color."'>:".$message_not."</font></p>";
				
				$_SESSION['pin_check_result'] = "<p style='background:url(image/catalog/postcode/postcode_icons.jpg);  background-repeat: no-repeat;background-position: 3px -34px;float: left;padding: 8px 0 0 22px;height: 35px;'><b>Status </b></b><font color = '".$text_color."'>".$message_not."</font></p>";
				$_SESSION['pin_check_delivery'] = '';
			}
					}
	}
}