<?php
class ControllerExtensionModuleJossmsVerify extends Controller {
	public function index() {
		
		$this->language->load('extension/module/jossmsverify');
		$ll = array(
			"heading_title",
			"text_phone",
			"text_start",
			"text_verification_code",
			"text_provide_valid_number",
			"text_provide_valid_mobile_number",
			"text_invalid_pin","text_verify",
			"text_max_retries_exceeded",
			"text_please_wait",
			"text_please_wait_next",
			"text_resend",
			"text_send_success",
			"text_explain1",
			"text_explain_select_type",
			"text_explain_phone_call",
			"text_explain_sms",
			"text_explain_started",
			"text_explain_phone_call2",
			"text_explain_sms2",
			"text_explain_same_number",
			"text_connection_problem"
		);
		for ($i=0;$i<sizeof($ll);$i++) $data[$ll[$i]] = $this->language->get($ll[$i]);
		$retrytime = $this->config->get("max_retry");
		//$retrytime = 5;
		$data["text_please_wait_next"] = sprintf($data["text_please_wait_next"], $retrytime);
		$data['phone'] = "";
		$this->load->model('checkout/order');
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$data['phone'] = @preg_replace("/[^0-9]+/", "", $order_info["telephone"]);
		
		$data['pinsent'] = 0;
		if (isset($this->session->data['pin'])) if ($this->session->data['pin']) $data['pinsent'] = 1;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/extension/module/jossmsverify.tpl')) {
			//return $this->load->view($this->config->get('config_template') . '/template/extension/module/jossmsverify.tpl', $data);
			return $this->load->view('extension/module/jossmsverify', $data);
		} else {
			//return $this->load->view('default/template/extension/module/jossmsverify.tpl', $data);
			return $this->load->view('extension/module/jossmsverify', $data);
		}
	}
	public function start() {
		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$phone = $order_info['telephone'];
		if ($phone)  {
			$phone = @trim($phone);
			$phone = @preg_replace("/[^0-9]+/", "", $phone);
		}
		
		$result = "";
		$maxretries = $this->config->get("jossms_max_retry");
		
		if( !empty($maxretries) && $maxretries <> 0){
			if( isset($this->session->data['phone_number']) ){
				$sess_phone = $this->session->data['phone_number'];
			}else{
				$sess_phone = "";
			}
			
			if( $sess_phone != $phone && isset($this->session->data['phone_trials']) ){
				unset($this->session->data['phone_trials']);
			}
			
			if( isset($this->session->data['phone_trials']) && $this->session->data['phone_trials'] >= $maxretries && $sess_phone == $phone ){
				$result = 12;
				$phone = "";
			}
		}
		
		if ($phone)  {
			$this->load->model('libraries/jossms');		
			$gateway = $this->config->get('jossms_gateway');
			$pin = sprintf("%s", rand(1,9));
			$pin = "";
			$codedigits = $this->config->get('jossms_code_digit');
			for ($i=0;$i<$codedigits;$i++) {
				$pin .= rand(1,9);
			}
			$this->session->data['pin'] = $pin;
			$parsing = array ( '{code}' );
			$replace = array ( $pin );
			$pesan = str_replace( $parsing, $replace, $this->config->get('jossms_message_code_verification') );
			
			$query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$order_info['payment_country_id'] . "'");
			$isoc = $query->row['iso_code_2'];
			$phone_number = $this->model_libraries_jossms->getConvertPhonePrefix($phone,$isoc);
			$getresponse = $this->model_libraries_jossms->send_message($phone_number, $pesan, $gateway);
			$result = $getresponse;
			
			$this->session->data['phone_number'] = $phone;
			
			if (!isset($this->session->data['phone_trials'])) $this->session->data['phone_trials'] = 1;
			else
			$this->session->data['phone_trials']++;
		}
		echo $result;
	}
	public function confirm() {
		$pin = $this->request->post['pin'];
		if ($pin && $this->session->data['pin']==$pin) {
			/*
			if ($this->customer->isLogged()) {
				$groupid = $this->customer->getCustomerGroupId();
				$verifiedgroup = $this->config->get('phoneverification_verifiedgroup');
				$cid = $this->customer->getId();
				$phone = $this->session->data['phone'];
				if ($cid && $phone) {
					$this->db->query("UPDATE " . DB_PREFIX . "customer set telephone=$phone where customer_id=$cid");
				}
				if ($groupid && $verifiedgroup && $cid && $verifiedgroup>0)
					$this->db->query("UPDATE " . DB_PREFIX . "customer set customer_group_id=$verifiedgroup where customer_id=$cid");
			}
			*/
			$this->session->data['phone_verified'] = 1;
			echo "1";
		}
		else echo "2";
	}
}
?>
