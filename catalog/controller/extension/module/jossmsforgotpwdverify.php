<?php
class ControllerExtensionModuleJossmsForgotpwdVerify extends Controller {
	public function index() {
		
		$this->language->load('extension/module/jossmsforgotpwdverify');
		$ll = array(
			"heading_title",
			"text_phone",
			"text_help",
			"text_start",
			"text_verification_code",
			"text_provide_valid_number",
			"text_provide_valid_mobile_number",
			"text_different_number",
			"text_null_number",
			"text_invalid_pin","text_verify",
			"text_max_retries_exceeded",
			"text_please_wait",
			"text_please_wait_next",
			"text_resend",
			"text_send_success",
			"text_explain1",
			"text_explain_select_type",
			"text_explain_phone_call",
			"text_explain_sms",
			"text_explain_started",
			"text_explain_phone_call2",
			"text_explain_sms2",
			"text_explain_same_number",
			"text_connection_problem"
		);
		for ($i=0;$i<sizeof($ll);$i++) $data[$ll[$i]] = $this->language->get($ll[$i]);
		$retrytime = $this->config->get("jossms_max_retry");
		$data["text_please_wait_next"] = sprintf($this->data["text_please_wait_next"], $retrytime);
		$data['phone'] = "";
		$this->load->model('checkout/order');
		
		$data['pinsent'] = 0;
		//if (isset($this->session->data['forgotten_pin'])) if ($this->session->data['forgotten_pin']) $data['pinsent'] = 1;
		
		$verified = 1;
		if ($this->config->get('jossms_verify') == 1) {
			$verified = 0;
		}		
		$data['verified'] = $verified;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/extension/module/jossmsforgotpwdverify.tpl')) {
			//return $this->load->view($this->config->get('config_template') . '/template/extension/module/jossmsforgotpwdverify.tpl', $data);
			return $this->load->view('extension/module/jossmsforgotpwdverify', $data);
		} else {
			//return $this->load->view('default/template/extension/module/jossmsforgotpwdverify.tpl', $data);
			return $this->load->view('extension/module/jossmsforgotpwdverify', $data);
		}	
		
		//$this->render(); 
	}
	public function start() {
		$phone = $this->request->post['phone'];
		if ($phone)  {
			$phone = @trim($phone);
			$phone = @preg_replace("/[^0-9]+/", "", $phone);
		}
		
		$result = "";
		$maxretries = $this->config->get("jossms_max_retry");
		
		if( !empty($maxretries) && $maxretries <> 0){
			if( isset($this->session->data['forgotten_phone_number']) ){
				$sess_phone = $this->session->data['forgotten_phone_number'];
			}else{
				$sess_phone = "";
			}
			
			if( $sess_phone != $phone && isset($this->session->data['forgotten_phone_trials']) ){
				unset($this->session->data['forgotten_phone_trials']);
			}
			
			if( isset($this->session->data['forgotten_phone_trials']) && $this->session->data['forgotten_phone_trials'] >= $maxretries && $sess_phone == $phone ){
				$result = 12;
				$phone = "";
			}
		}
		
		if ($phone)  {
			$this->load->model('libraries/jossms');		
			$gateway = $this->config->get('jossms_gateway');
			$pin = sprintf("%s", rand(1,9));
			$pin = "";
			$codedigits = $this->config->get('jossms_code_digit');
			for ($i=0;$i<$codedigits;$i++) {
				$pin .= rand(1,9);
			}
			$this->session->data['forgotten_pin'] = $pin;
			$parsing = array ( '{code}' );
			$replace = array ( $pin );
			$pesan = str_replace( $parsing, $replace, $this->config->get('jossms_message_code_verification') );
			
			$getresponse = $this->model_libraries_jossms->send_message($phone, $pesan, $gateway);
			$result = $getresponse;
			
			$this->session->data['forgotten_phone_number'] = $phone;
			
			if (!isset($this->session->data['forgotten_phone_trials'])) $this->session->data['forgotten_phone_trials'] = 1;
			else
			$this->session->data['forgotten_phone_trials']++;
		}
		echo $result;
	}
	public function confirm() {
		$pin = $this->request->post['pin'];
		if ($pin && $this->session->data['forgotten_pin']==$pin) {
			$this->session->data['forgotten_phone_verified'] = 1;
			echo "1";
		}
		else echo "2";
		
	}
}
?>
