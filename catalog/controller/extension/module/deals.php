<?php
class ControllerExtensionModuleDeals extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/ocfeatureslider');
		
		
        $this->document->addStyle('catalog/view/javascript/css/wow.css');
        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        //$this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/wow.js');
        $this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        
        

		$this->load->model('catalog/product');
		$this->load->model('catalog/ocproductrotator');
		$this->load->model('tool/image');

		$data = array();

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_title'] = $this->language->get('text_title');

		$lang_code = $this->session->data['language'];

		if(isset($setting['title']) && $setting['title']) {
			$data['title'] = $setting['title'][$lang_code]['title'];
		} else {
			$data['title'] = $this->language->get('heading_title');
		}
		
		$cover_image =  $this->model_tool_image->resize($setting['image'], 456, 723);

		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_new'] = $this->language->get('text_new');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

	
		$data['deals_href'] = "/all-deals";
		$deals =  $this->model_catalog_product->getActiveDeals();
		$data['total_deals'] =  $this->model_catalog_product->getDealsCount();
		if($deals) {
		    foreach($deals AS $deal) {
		        $deal_product = $this->model_catalog_product->getProduct($deal['product_id']);
		        
		        if ($deal_product['image']) {
					$image = $this->model_tool_image->resize($deal_product['image'], 400, 400);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 400, 400);
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$deal_product['special'] ? $deal_product['special'] : $deal_product['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $deal_product['rating'];
				} else {
					$rating = false;
				}
				
				$options = $this->model_catalog_product->getProductOptions($deal['product_id']);
				$option_id = $deal['option_id'] ? $deal['option_id'] : NULL;
				$option_value_id = $deal['option_value_id'] ? $deal['option_value_id'] : NULL;
				
				$deal_sku = array();
				if($option_id && $option_value_id) {
				    $product_option_id = $this->model_catalog_product->getProductOptionId($deal['product_id'], $option_id, $option_value_id);
				    foreach($options AS $option) {
				            foreach($option['product_option_value'] AS $option_value) {
				                $price_with_option = 0;
				                if($option_value['option_value_id'] == $option_value_id) {
				                    $deal_sku = array(
				                        'product_option_id'       => $product_option_id,
				                        'option_id'               => $option_id,
				                        'option_value_id'         => $option_value_id,
				                        'product_option_value_id' => $option_value['product_option_value_id'],
				                        'name'                    => $option_value['name'],
				                        'quantity'                => $option_value['quantity'],
				                        'type'                    => $option['type'],
				                        'required'                => $option['required'],
				                        'price'                   => $option_value['price'],
				                        'price_prefix'            => $option_value['price_prefix']
				                    );
				                    if($option_value['price'] > 0 && $option_value['price_prefix'] == "+") {
				                        $price_with_option = $deal_product['price'] + $option_value['price'];
				                    } else if($option_value['price'] > 0 && $option_value['price_prefix'] == "-") {
				                        $price_with_option = $deal_product['price'] - $option_value['price'];
				                    }
				                    break;
				                }
				            }
				    }
				} else {
				    $price_with_option = 0;
				}
				
				if($price_with_option > 0) {
				    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					    $price = $this->currency->format($this->tax->calculate($price_with_option, $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					    
					    $mrp = $this->currency->format($this->tax->calculate($price_with_option, $deal_product['tax_class_id'], $this->config->get('config_tax')));
				        $discount = 100 - ($deal['price'] / filter_var($mrp, FILTER_SANITIZE_NUMBER_INT) * 100);
			    	} else {
					    $price = false;
				    }
				} else {
				    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					    $price = $this->currency->format($this->tax->calculate($deal_product['price'], $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					    
					    $mrp = $this->currency->format($this->tax->calculate($deal_product['price'], $deal_product['tax_class_id'], $this->config->get('config_tax')));
				        $discount = 100 - ($deal['price'] / filter_var($mrp, FILTER_SANITIZE_NUMBER_INT) * 100);
			    	} else {
					    $price = false;
				    }
				}

				if($product_rotator_status == 1) {
					$product_id = $deal_product['product_id'];
					$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($product_id);

					if($product_rotator_image) {
						$rotator_image = $this->model_tool_image->resize($product_rotator_image, $setting['width'], $setting['height']);
					} else {
						$rotator_image = false;
					}
				} else {
					$rotator_image = false;
				}
				
		        $data['deals_data'][] = array(
		            'product_id'    => $deal['product_id'],
		            'thumb'         => $image,
					'rotator_image' => $rotator_image,
		            'name'          => $deal_product['name'],//strlen($deal_product['name']) > 25 ? substr($deal_product['name'],0,25) . "..." : $deal_product['name'],
		            'quantity'      => $deal_product['quantity'],
		            'price'         => $price,
		            'deal_price'    => $deal['price'],
		            'discount'      => round($discount) . "% OFF",
		            'deal_coupon'   => $deal['coupon'],
		            'options'        => $deal_sku,
		            'date_start'    => $deal['date_start'],
		            'date_end'      => $deal['date_end'],
		            'href'          => $this->url->link('product/product', 'product_id=' . $deal['product_id'])
		        );
		    }
		}
		
		return $this->load->view('extension/module/deals', $data);
	}
}