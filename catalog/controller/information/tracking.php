<?php
class ControllerInformationTracking extends Controller {
     public function __construct($registry) {
    parent :: __construct($registry);
    /*$this->load->model('mpblog/mpbloginstall');
    $this->model_mpblog_mpbloginstall->install();*/

  }
  
    public function index() {
        $this->document->setTitle("Order Tracking");
        $data['heading_title'] = 'Order Tracking';
        $data['action']        = $this->url->link('information/tracking');
        
        
        $modeIs = $this->config->get("delhivery_lastmile_is_production");
        if($modeIs){
            $cl = $this->config->get("delhivery_lastmile_login_id");
            $token = $this->config->get("delhivery_lastmile_license_key");
            $apiurl = "https://track.delhivery.com/api/packages/json/?token=".$token." &verbose=2&waybill=";
        }else{
          $apiurl='http://test.delhivery.com/api/p/packing_slip';
        }
       
        
        if($cl && $token){
            $order_id='3463037227';
            $awb_no = $this->getAwbno($order_id);
                
            $gettrackingdetails =  $apiurl.$awb_no['awb'];  
            //echo $gettrackingdetails;  
            
            
            $modeIs = $this->config->get("delhivery_lastmile_is_production");
            if($modeIs){
                $apiurl='https://track.delhivery.com/api/packages/';
            }else{
                $apiurl='https://test.delhivery.com/api/packages/';
            }
            $cl = $this->config->get("delhivery_lastmile_login_id");
            $token = $this->config->get("delhivery_lastmile_license_key");
            if($apiurl && $token){
                
                $url = $apiurl.'json/?token='.$token.'&verbose=2&cl='.urlencode($cl).'&waybill='.$awb_no['awb'];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_ENCODING, '');
                curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: Token '.$token.''));
                $retValue = curl_exec($ch);
                $error_msg = '';
            if (curl_error($ch)) {
                 $error_msg = curl_error($ch);
            }
            $codes = json_decode($retValue);
            $errorLogData = "Request Url: ".$url." \r\nResponse from API: ".$retValue.", ".$error_msg; 
            $this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($codes));
            
            }
        }        
            
    }
    
    public function getAwbno($order_id) {
		$sql = "SELECT  * FROM " . DB_PREFIX . "delhivery_lastmile_awb Where orderid ='".$order_id."'";
		$query = $this->db->query($sql);

		return $query->row;
	}
    
    private function getAWBByOrderId($order_id) {
        $sql   = "SELECT o.awbno,o.courier_id,c.name,o.order_id FROM `" . DB_PREFIX . "order` o INNER JOIN `" . DB_PREFIX . "sw_couriers` c ON(o.courier_id = c.courier_id) where o.order_id ='" . addslashes($order_id)."' AND o.awbno != '0' AND o.awbno != '' ";
       
		$query = $this->db->query($sql);
        return $query->row;        
    }
    
    public function getAWBResults($username, $password, $carrier_id, $awbno, $order_id) {
        $url         = "https://shipway.in/api/getawbresult";
        $data_string = array(
            "username" 		=> $username,
            "password" 		=> $password,
            "carrier_id" 	=> $carrier_id,
            "awb" 			=> $awbno,
			"order_id"		=> $order_id
        );
		
        $data_string = json_encode($data_string);
        $curl        = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json'
        ));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }
}
?>