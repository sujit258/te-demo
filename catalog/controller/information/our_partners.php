<?php
class ControllerInformationOurPartners extends Controller
{
    public function index() {
        $data['heading_title'] = 'Our Partners';
        $this->document->setTitle('Our Partners');
        $data['action']        = $this->url->link('information/our_partners');
        
        $data['breadcrumbs'] = array();
        
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
            
        );
        
        $data['breadcrumbs'][] = array(
            'text' => 'Our Partners',
            'href' => $this->url->link('information/our_partners')
           
        );
		    
		    $data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
		    $this->response->setOutput($this->load->view('information/our_partners', $data));
    }
    

    public function certifications() {
        
        $data['heading_title'] = 'Our Certifications';
        $this->document->setTitle('Our Certifications');
        $data['action']        = $this->url->link('information/our_certifications');
        
        $data['breadcrumbs'] = array();
        
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
            
        );
        
        $data['breadcrumbs'][] = array(
            'text' => 'Our Certifications',
            'href' => $this->url->link('information/our_certifications')
           
        );
		    
		    $data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
		    $this->response->setOutput($this->load->view('information/certifications', $data));
    }
    
    
}?>