<?php
class ControllerRecipeRecipeInfo extends Controller {
	
	public function index() {
	    // Added to cart
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js'); 
		// Added to cart
		$this->load->language('recipe/recipe');

		$this->load->model('catalog/recipe');
		
		// SOF Recipe Related Product
		$this->load->model('tool/image');
		$data['products'] = array();
		$results = $this->model_catalog_recipe->getRecipeRelatedProduct($this->request->get['recipe_id']);
	
		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'],408,300);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png',408,300);
			}

			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price = false;
			}

			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$special = false;
			}

			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
			} else {
				$tax = false;
			}

			if ($this->config->get('config_review_status')) {
				$rating = (int)$result['rating'];
			} else {
				$rating = false;
			}

			$data['products'][] = array(
				'product_id'  => $result['product_id'],
				'thumb'       => $image,
 				'name'        => (substr($result['name'],0,40)),
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				'price'       => $price,
				'special'     => $special,
				'tax'         => $tax,
				'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				'rating'      => $rating,
				'status'        => $result['status'],
				'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
			);
		}
        // EOF Recipe Related Product
        
        // SOF Related Recipe
		$this->load->model('tool/image');
		$data['related_recipes'] = array();
		$related_recipe_results = $this->model_catalog_recipe->getRecipesRelatedRecipe($this->request->get['recipe_id']);
	
		foreach ($related_recipe_results as $rr_result) {
			if ($rr_result['image']) {
				$image = $this->model_tool_image->resize($rr_result['image'], 1100, 520);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png',269,127);
			}

			$data['related_recipes'][] = array(
				'recipe_id'  => $rr_result['recipe_id'],
				'thumb'       => $image,
 				'title'        => (substr($rr_result['title'],0,40)),
				'description' => utf8_substr(strip_tags(html_entity_decode($rr_result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				'href'        => $this->url->link('recipe/recipe/info', 'recipe_id=' . $rr_result['recipe_id'])
			);
		}
        // EOF Related Recipe

		if (isset($this->request->get['recipe_id'])) {
			$recipe_id = (int)$this->request->get['recipe_id'];
		} else {
			$recipe_id = "";
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_recipe'),
			'href' => $this->url->link('recipe/recipe')
		);
		
		$data['recipe_id'] = (int)$this->request->get['recipe_id'];
		
		$data['recipies'] = $this->model_catalog_recipe->getRecipe($recipe_id);
		
		$recipe_info = $this->model_catalog_recipe->getRecipe($recipe_id);

	    if ($recipe_info) {
			$this->document->setTitle($recipe_info['meta_title']);
			$this->document->setDescription($recipe_info['meta_description']);
			$this->document->setKeywords($recipe_info['meta_keyword']);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			$data['breadcrumbs'][] = array(
				'text' => $recipe_info['title'],
				'href' => $this->url->link('recipe/recipe/info', 'recipe_id=' . $this->request->get['recipe_id'] . $url)
			);
			
			// star-rating start
			$this->document->addStyle('catalog/view/javascript/star-rating/css/style.css');
			// star-rating end

			$data['heading_title'] 	= $recipe_info['title'];
			$data['title']			= $recipe_info['date_added'];
			$data['author'] 		= $recipe_info['author'];
			
			$data['title1'] 	= $recipe_info['title'];
			print_r($recipe_info['title']);
			
			$data['image'] 			= $this->model_tool_image->resize($recipe_info['homepage_image'], 1100, 520);
			
			
			$data['description']	=  html_entity_decode($recipe_info['description'], ENT_QUOTES, 'UTF-8');
			$data['directions']	=  html_entity_decode($recipe_info['directions'], ENT_QUOTES, 'UTF-8');
			$data['ingredients']	=  html_entity_decode($recipe_info['ingredients'], ENT_QUOTES, 'UTF-8');
					
			$data['text_empty'] 		= $this->language->get('text_empty');
			$data['text_recipe'] 		= $this->language->get('text_recipe');
			$data['text_sort'] 			= $this->language->get('text_sort');
			$data['button_continue'] 	= $this->language->get('button_continue');
			$data['text_description'] 	= $this->language->get('text_description');
			$data['text_directions'] 	= $this->language->get('text_directions');
			$data['text_ingredients']	= $this->language->get('text_ingredients');
			
			$data['text_related']		= $this->language->get('text_related');
    		$data['text_tax'] = $this->language->get('text_tax');
    		
    		// Buttons
    		$data['button_cart'] = $this->language->get('button_cart');
    		$data['button_wishlist'] = $this->language->get('button_wishlist');
    		$data['button_compare'] = $this->language->get('button_compare');

			
			// Start Riview code
			
			$data['entry_qty']                = $this->language->get('entry_qty');
			$data['entry_name']               = $this->language->get('entry_name');
			$data['entry_review']             = $this->language->get('entry_review');
			$data['entry_rating']             = $this->language->get('entry_rating');
			$data['entry_good']               = $this->language->get('entry_good');
			$data['entry_bad']  			  = $this->language->get('entry_bad');
			
			$data['text_write']            	  = $this->language->get('text_write');
			$data['text_reviews']             = $this->language->get('text_reviews');
			$data['text_no_reviews']  		  = $this->language->get('text_no_reviews');
			$data['text_note']  			  = $this->language->get('text_note');
			$data['text_loading']			  = $this->language->get('text_loading');
			
			
			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}
			
			$data['reviews'] = array();
			$data['reviews'] = $this->model_catalog_recipe->getReviewsByRecipeId($this->request->get['recipe_id'],($page - 1) * 5, 5);
			
			$review_total = $this->model_catalog_recipe->getTotalReviewsByRecipeId($this->request->get['recipe_id']);
			
			$data['r_total'] = $review_total;

    		$pagination = new Pagination();
    		$pagination->total = $review_total;
    		$pagination->page = $page;
    		$pagination->limit = 5;
    		$pagination->url = $this->url->link('recipe/recipe/info', 'recipe_id=' . $this->request->get['recipe_id'] . '&page={page}');

		    $data['pagination'] = $pagination->render();
		
		    $data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		// end review code
			

			// Image code *
			$this->load->model('tool/image');
			
			if ($recipe_info['homepage_image']) {
				$data['image'] = $this->model_tool_image->resize($recipe_info['homepage_image'], 1100, 520);
			} else {
				$data['image'] = '';
			}

			// End Image code
			
			
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['continue'] = $this->url->link('common/home');
			$data['category_link'] = $this->url->link('recipe/recipe');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('recipe/recipe_info', $data));
			
		}
		else
		{
			$url = '';

			if (isset($this->request->get['recipe_id'])) {
				$url .= '&recipe_id=' . $this->request->get['recipe_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('recipe/recipe/info', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
			
			

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['header'] = $this->load->controller('common/header');
			$data['footer'] = $this->load->controller('common/footer');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
		
	}
}