<?php
class ControllerRecipeRecipe extends Controller {
	
	public function index() {
		$this->load->language('recipe/recipe');

		$this->load->model('catalog/recipe');
		
		$this->load->model('tool/image');
		
		/*$this->model_tool_image->resize('catalog/ebc/masala-rolled-oats-kanda-poha-500gm.jpg', 800, 1007);*/
		/*$this->model_tool_image->resize('catalog/Banner/mobile/chocolate-granola-home-slider-mobile.jpg', 1081, 1701);*/
		
		$this->document->setTitle("Browse All True Elements Recipe Collection");
        $this->document->setDescription("Start your day with some delicious & healthy True Elements breakfast recipes. Also check some salad, smoothies & many more recipes.");
		$this->document->setKeywords("recipe,True Elements Recipe,Healthy recipes");

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_index'] = $this->language->get('text_index');
		$data['text_empty'] = $this->language->get('text_empty');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_search'] = $this->language->get('button_search');
		
		
		$data['place_holder']    = $this->language->get('place_holder');
		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_recipe'),
			'href' => $this->url->link('recipe/recipe')
		);
					
		if(isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$category_info = $this->model_catalog_recipe->getRecipeCategories();
        if($category_info) {
            foreach($category_info AS $recipe_category) {
                $data['recipe_categories'][] = array(
                    'category_id'   => $recipe_category['category_id'],
                    'name'          => $recipe_category['name'],
                    'image'         => $this->model_tool_image->resize($recipe_category['image'], 480, 576),
                    'href'          => $this->url->link('recipe/recipe/category', 'recipe_category_id=' . $recipe_category['category_id'])
                );
            }
        }
        
        $this->load->language('extension/module/filter');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['action'] = str_replace('&amp;', '&', $this->url->link('recipe/recipe', $url));

        if (isset($this->request->get['filter'])) {
			$data['filter_recipe'] = explode(',', $this->request->get['filter']);
		} else {
			$data['filter_recipe'] = array();
		}
		$data['filter_groups'] = array();

		$filter_groups = $this->model_catalog_recipe->getRecipeFilters();
		if ($filter_groups) {
			foreach ($filter_groups as $filter_group) {
				$childen_data = array();

				foreach ($filter_group['filter'] as $filter) {
					$filter_data = array(
						'filter_filter'      => $filter['filter_id']
					);
					$childen_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name'],
						'recipe_count'  => $this->model_catalog_recipe->getTotalRecipies($filter_data),
						'recipe_counts'  => $this->model_catalog_recipe->getTotalFilterRecipies($filter_data)
					);
				}
				

				$data['filter_groups'][] = array(
					'filter_group_id' => $filter_group['filter_group_id'],
					'name'            => $filter_group['name'],
					'filter'          => $childen_data
				);
			}
		}
		$filter_data = array(
		    'filter_filter' => $this->request->get['filter'],
			'start' => ($page - 1) * 24,
			'limit' => 24
		);
		
        $contest_winners = $this->model_catalog_recipe->getContestWinnersRecipes(10);
        
        if($contest_winners) {
            foreach($contest_winners AS $contest_winner) {
                $data['contest_winner'][] = array(
                    'name'          => $contest_winner['title'],
                    'author'        => $contest_winner['author'],
                    'contest_name'  => $contest_winner['contest_name'],
                    'image'         => $this->model_tool_image->resize($contest_winner['image'], 480, 576),
                    'href'          => $this->url->link('recipe/recipe/info', 'recipe_id=' . $contest_winner['recipe_id'])
                );
            }
        }
		$recipe_total = $this->model_catalog_recipe->getTotalFilterRecipies($filter_data);
		
		$results = $this->model_catalog_recipe->getRecipies($filter_data);
		
		$url = '';
		$pagination = new Pagination();
		$pagination->total = $recipe_total;
		$pagination->page = $page;
		$pagination->limit = 24;
		$pagination->url = $this->url->link('recipe/recipe',$url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		
				
		$data['list_results'] = sprintf($this->language->get('text_pagination'), ($recipe_total) ? (($page - 1) * 24) + 1 : 0, ((($page - 1) * 24) > ($recipe_total - 24)) ? $recipe_total : ((($page - 1) * 24) + 24), $recipe_total, ceil($recipe_total / 24));
		
		$data['recipies'] = array();		
		
		foreach($results as $result) {
				$data['recipies'][] = array(
				'recipe_id' => $result['recipe_id'],
				'title' => $result['title'],
				'author' => $result['author'],
				'date_added' => $result['date_added'],
				'prep_time'  => $result['prep_time'],
				'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'), 
				'href'        => $this->url->link('recipe/recipe/info', 'path=' . $this->request->get['path'] . '&recipe_id=' . $result['recipe_id'] . $url),
                'thumb'  => $this->model_tool_image->resize($result['image'], 1080, 1080),
				'image' => $this->model_tool_image->resize($result['image'], 1080, 1080)
			
			);
		}
						 	
		$data['continue'] = $this->url->link('common/home');
		
		$data['category_link'] = $this->url->link('recipe/recipe');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		

		$this->response->setOutput($this->load->view('recipe/recipe_listt', $data));
	}
	
	 public function tebundle() {
        $this->load->language('checkout/cart');
	    
        $json = array();
        $product_id = array();
        if (isset($this->request->post['vals'])) {
			foreach ($this->request->post['vals'] as $key => $pro_id) {
				$product_id[$key] = $pro_id;
			}
        } else {
            $product_id = array();
        }

        $this->load->model('catalog/product');

        $product_info = '';
        $product_options = '';
        if (!empty($product_id)) {
            foreach ($product_id as $key => $ids) {
			
            $product_info = $this->model_catalog_product->getProduct($key);
            
            $product_options = $this->model_catalog_product->getProductOptions($key);
				
           
           $default_options = [];
           
			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					foreach($product_option['product_option_value'] as $row => $val)
                    {
                        if(1 == $val['is_default'])
                        {
                            if('checkbox' == $product_option['type'])
                            {
                                $_default_val = [$val['product_option_value_id']];
                            }
                            else
                            {
                                $_default_val = (int)$val['product_option_value_id'];
                            }

                            $default_options[$product_option['product_option_id']] = $_default_val;
                        }
                    }

                    if(0 === count($default_options))
                    {
                        $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                    }
				}
			}
			
			if(count($default_options) > 0)
            {
                $filtered_default_options   = array_filter($default_options);
                $new_option                 = array_merge($option, $filtered_default_options);
                //$option                   = array_filter($new_option);
                $option                     = $default_options;
            }
			
                
                if($ids <= 0 && isset($ids)){
                    $quantity = 0;
                } elseif ($ids > $product_info['quantity']) {
                    $json['not_done'][$key]['error'][] = sprintf($this->language->get('error_quantity'),$product_info['name']	);
                } else {
                    $quantity = $ids;
                }

                $recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

                if ($recurrings) {
                    $recurring_ids = array();

                    foreach ($recurrings as $recurring) {
                        $recurring_ids[] = $recurring['recurring_id'];
                    }

                    if (!in_array($recurring_id, $recurring_ids)) {
                        $json['not_done'][$key]['error'][] = $this->language->get('error_recurring_required');
                    }
                }

                
                $profile_id = 0;
                if (isset($json) && empty($json['not_done'])) {
                    $this->cart->add($key, $quantity, $option, $profile_id);
                    $json['done'][] = $key;
                } elseif (!array_key_exists($key, $json['not_done']) && isset($json['not_done'])) {
                    $this->cart->add($key, $quantity, $option, $profile_id);
                    $json['done'][] = $key;
                }
            }
        }

        $product_data = '';
        
        if (isset($json['done']) && !empty($json['done'])) {
            $success = '';
            foreach ($json['done'] as $key => $val) {
                 $success .= ', '.$val['success'][0]; 
            }
            $success = trim($success,",");
            $product_data .= '<div class="success" style="display: none;">Success : '. $success . ' added to the shopping cart<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>';
        }
        if (isset($json['not_done']) && !empty($json['not_done'])) {
            foreach ($json['not_done'] as $key => $val) {
                $product_data .= '<div class="warning" style="display: none;">Error: ' . $val['error'][0] . '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>';
            }
        }

        $arr = array();
		if($product_data){
          $arr['product_data'] = $product_data;
		}
		$total = 0;
		$arr['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		// Added to cart	
		$arr['notice_add_layout'] = $this->config->get('notice_add_layout');
		$arr['notice_add_timeout'] = $this->config->get('notice_add_timeout');
		$arr['notice_add_status'] = $this->config->get('notice_add_status');	
		// Added to cart
		
		unset($this->session->data['shipping_method']);
        unset($this->session->data['shipping_methods']);
        unset($this->session->data['payment_method']);
        unset($this->session->data['payment_methods']);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($arr));
    }
    
	public function category() {
		$this->load->language('recipe/recipe');

		$this->load->model('catalog/recipe');
		
		$this->load->model('tool/image');
		
		if($this->request->get['recipe_category_id']) {
            $category_title = $this->model_catalog_recipe->getCategory($this->request->get['recipe_category_id']);
            $this->document->setTitle($category_title['meta_title']);
            $this->document->setDescription($category_title['meta_description']);
			$this->document->setKeywords($category_title['meta_keyword']);
        }
        else {
            $this->document->setTitle("Browse All True Elements Recipe Collection");
            $this->document->setDescription("Start your day with some delicious & healthy True Elements breakfast recipes. Also check some salad, smoothies & many more recipes.");
			$this->document->setKeywords("recipe,True Elements Recipe,Healthy recipes");
        }

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_index'] = $this->language->get('text_index');
		$data['text_empty'] = $this->language->get('text_empty');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_search'] = $this->language->get('button_search');
		
		
		$data['place_holder']   = $this->language->get('place_holder');
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_recipe'),
			'href' => $this->url->link('recipe/recipe')
		);
		
		if(isset($this->request->get['recipe_category_id'])) {
            $text_name = $this->model_catalog_recipe->getCategory($this->request->get['recipe_category_id']);
        }

        $data['breadcrumbs'][] = array(
            'text' => $text_name['name'],
            'href'     => $this->url->link('recipe/recipe/category', 'recipe_category_id=' . $this->request->get['recipe_category_id'])
        );
		
		if(isset($this->request->get['recipe_category_id'])) {
			$category_id = $this->request->get['recipe_category_id'];
			$data['category_id'] = $this->request->get['recipe_category_id'];
		}
		else {
			$category_id = '';
		}

		if(isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		}
		else {
			$page = 1;
		}
		
		$this->load->language('extension/module/filter');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['action'] = str_replace('&amp;', '&', $this->url->link('recipe/recipe/category', 'recipe_category_id=' . $category_id . $url));

        if (isset($this->request->get['filter'])) {
			$data['filter_recipe'] = explode(',', $this->request->get['filter']);
		} else {
			$data['filter_recipe'] = array();
		}
		$data['filter_groups'] = array();

		$filter_groups = $this->model_catalog_recipe->getRecipeFilters();
		if ($filter_groups) {
			foreach ($filter_groups as $filter_group) {
				$childen_data = array();

				foreach ($filter_group['filter'] as $filter) {
					$filter_data = array(
						'category_id'        => $category_id,
						'filter_filter'      => $filter['filter_id']
					);
					$childen_data[] = array(
						'filter_id'     => $filter['filter_id'],
						'name'          => $filter['name'],
						'recipe_count'  => $this->model_catalog_recipe->getTotalRecipies($filter_data),
						'recipe_counts'  => $this->model_catalog_recipe->getTotalFilterRecipies($filter_data)
						
					);
				}

				$data['filter_groups'][] = array(
					'filter_group_id' => $filter_group['filter_group_id'],
					'name'            => $filter_group['name'],
					'filter'          => $childen_data
				);
			}
		}
		
		$filter_data = array(
		    'filter_filter' => $this->request->get['filter'],
		    'category_id'   => $category_id,
			'start' => ($page - 1) * 12,
			'limit' => 12
		);
		
		$category_info = $this->model_catalog_recipe->getRecipeCategories();
        if($category_info) {
            foreach($category_info AS $recipe_category) {
                //if($recipe_category['category_id'] != $category_id) {
                    $data['recipe_categories'][] = array(
                        'category_id'   => $recipe_category['category_id'],
                        'name'          => $recipe_category['name'],
                        /*'image'         => $this->model_tool_image->resize($recipe_category['image'], 400, 400),*/
                        'image'         => $this->model_tool_image->resize($recipe_category['image'], 480, 576),
                        'href'          => $this->url->link('recipe/recipe/category', 'recipe_category_id=' . $recipe_category['category_id'])
                    );
                //}
            }
        }
        
        $contest_winners = $this->model_catalog_recipe->getContestWinnersRecipes(10);
        
        if($contest_winners) {
            foreach($contest_winners AS $contest_winner) {
                $data['contest_winner'][] = array(
                    'name'          => $contest_winner['title'],
                    'author'        => $contest_winner['author'],
                    'contest_name'  => $contest_winner['contest_name'],
                    'image'         => $this->model_tool_image->resize($contest_winner['image'], 480, 576),
                    'href'          => $this->url->link('recipe/recipe/info', 'recipe_id=' . $contest_winner['recipe_id'])
                );
            }
        }
		
		$recipe_total = $this->model_catalog_recipe->getTotalFilterRecipies($filter_data);
		
		$results = $this->model_catalog_recipe->getRecipies($filter_data);
		
		$url = '';
		$pagination = new Pagination();
		$pagination->total = $recipe_total;
		$pagination->page = $page;
		$pagination->limit = 12;
		$pagination->url = $this->url->link('recipe/recipe/category', 'recipe_category_id=' . $this->request->get['recipe_category_id']. $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		
				
		$data['list_results'] = sprintf($this->language->get('text_pagination'), ($recipe_total) ? (($page - 1) * 12) + 1 : 0, ((($page - 1) * 12) > ($recipe_total - 12)) ? $recipe_total : ((($page - 1) * 12) + 12), $recipe_total, ceil($recipe_total / 12));
		
			if ($page == 1) {
			    $this->document->addLink($this->url->link('recipe/recipe/category', 'recipe_category_id=' . $category_info['category_id'], true), 'canonical');
			} elseif ($page == 2) {
			    $this->document->addLink($this->url->link('recipe/recipe/category', 'recipe_category_id=' . $category_info['category_id'], true), 'prev');
			} else {
			    $this->document->addLink($this->url->link('recipe/recipe/category', 'recipe_category_id=' . $category_info['category_id'] . '&page='. ($page - 1), true), 'prev');
			}

			if (12 && ceil($recipe_total / 12) > $page) {
			    $this->document->addLink($this->url->link('recipe/recipe/category', 'recipe_category_id=' . $category_info['category_id'] . '&page='. ($page + 1), true), 'next');
			}
				
		foreach($results as $result) {
				$data['recipies'][] = array(
				'recipe_id' => $result['recipe_id'],
				'title' => $result['title'],
				'author' => $result['author'],
				'date_added' => $result['date_added'],
				'prep_time'  => $result['prep_time'],
				'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
				'href' => $this->url->link('recipe/recipe/info', 'recipe_id=' . $result['recipe_id']),
                'thumb'  => $this->model_tool_image->resize($result['image'], 1080, 1080),
				'image' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'))			
			);
		}	
		
						 	
		$data['continue'] = $this->url->link('common/home');
		
		$data['category_link'] = $this->url->link('recipe/recipe');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		

		$this->response->setOutput($this->load->view('recipe/recipe_listt', $data));
	}
		
	public function search() {
		$this->load->language('recipe/recipe');

		$this->load->model('catalog/recipe');
		
		$this->load->model('tool/image');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_index'] = $this->language->get('text_index');
		$data['text_empty'] = $this->language->get('text_empty');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_search'] = $this->language->get('button_search');
		
		
		$data['place_holder']    = $this->language->get('place_holder');
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_recipe'),
			'href' => $this->url->link('recipe/recipe')
		);
		
		$data['search_query'] = $this->request->get['search'];
		$search_keyword = $this->request->get['search'];
	
		$results = $this->model_catalog_recipe->getRecipiesList();
		
		// Pagination start
		
		// Pagination
		
		$data['recipies'] = array();		
		
		foreach($results as $result) {	
			if (preg_match('/'.$search_keyword.'/i', $result['title']))	{
				$data['recipies'][] = array(
				'recipe_id' => $result['recipe_id'],
				'title' => $result['title'],
				'author' => $result['author'],
				'date_added' => $result['date_added'],
				'prep_time'  => $result['prep_time'],
				'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
				'href' => $this->url->link('recipe/recipe/info', 'recipe_id=' . $result['recipe_id']),
                'thumb'  =>$this->model_tool_image->resize($result['image'], 1080, 1080),
				'image' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'))			
			);
			}		
		}
						 	
		$data['continue'] = $this->url->link('recipe/recipe');
		
		$data['category_link'] = $this->url->link('recipe/recipe');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		

		$this->response->setOutput($this->load->view('recipe/recipe_list', $data));
	}
	
	public function info() {
	    // Added to cart
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js'); 
		// Added to cart
		$this->load->language('recipe/recipe');

		$this->load->model('catalog/recipe');
		$this->load->model('catalog/product');
		$this->load->model('catalog/ocproductrotator');
		
		$deal_data = array();
        $deals_filter_data = array(
            'limit' => 100    
	    );
		$active_deals = $this->model_catalog_product->getDeals($deals_filter_data);
        if($active_deals) {
            foreach($active_deals AS $deal) {
                $deal_data[$deal['product_id']] = $deal['coupon'];
            }
        }
		
		// SOF Recipe Related Product
		$this->load->model('tool/image');
		$data['products'] = array();
		$results = $this->model_catalog_recipe->getRecipeRelatedProduct($this->request->get['recipe_id']);
	
		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'],640,806);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png',640,806);
			}
			
			$product_rotator_image = '';
		    $product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($result['product_id']);

			if($product_rotator_image) {
				$rotator_image = $this->model_tool_image->resize($product_rotator_image, 640, 806);
			} else {
				$rotator_image = false;
			}

			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price = false;
			}

			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$special = false;
			}

			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
			} else {
				$tax = false;
			}

			if ($this->config->get('config_review_status')) {
				$rating = (int)$result['rating'];
			} else {
				$rating = false;
			}
			
			$options = NULL;
			if($result['product_type'] != 5) {
			    $options = $this->model_catalog_product->getProductOptions($result['product_id']);
			} else {
			    $price = false;
			}
				
			if(array_key_exists($result['product_id'],$deal_data)) {
		        $coupon = $deal_data[$result['product_id']];
		    } else {
		        $coupon = '';
		    }
		    
		    if($result['quantity'] > 0 && !$in_stock_products) {
		        $in_stock_products = $result['product_id'];
		    } else if($result['quantity'] > 0) {
		        $in_stock_products .= "," . $result['product_id'];
		    }
		    
            if ($result['status'] == 1) {
		    	$data['products'][] = array(
				    'product_id'    => $result['product_id'],
				    'product_type'  => $result['product_type'],
				    'thumb'         => $image,
				    'rotator_image' => $rotator_image,
 				    'name'          => substr($result['name'],0,55),
 				    'name_mobile'   => substr($result['name'],0,38),
 				    'coupon'        => $coupon,
 					'options'       => $options,
 					'quantity'      => $result['quantity'],
				    'price'         => $price,
				    'special'       => $special,
				    'tax'           => $tax,
				    'minimum'       => $result['minimum'] > 0 ? $result['minimum'] : 1,
				    'rating'        => $rating,
				    'status'        => $result['status'],
				    'href'          => $this->url->link('product/product', 'product_id=' . $result['product_id'])
			    );
            }
		}
		if($in_stock_products) {
		    $data['in_stock_products'] = $in_stock_products;
		} else {
		    $data['in_stock_products'] = false;
		}
        // EOF Recipe Related Product
        
        // SOF Related Recipe
		$this->load->model('tool/image');
		$data['related_recipes'] = array();
		$related_recipe_results = $this->model_catalog_recipe->getRecipesRelatedRecipe($this->request->get['recipe_id']);
	
		foreach ($related_recipe_results as $rr_result) {
			if ($rr_result['homepage_image']) {
				$image = $this->model_tool_image->resize($rr_result['homepage_image'], 1100, 520);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png',269,127);
			}

			$data['related_recipes'][] = array(
				'recipe_id'  => $rr_result['recipe_id'],
				'thumb'       => $image,
 				'title'        => (substr($rr_result['title'],0,40)),
				'description' => utf8_substr(strip_tags(html_entity_decode($rr_result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				'href'        => $this->url->link('recipe/recipe/info', 'recipe_id=' . $rr_result['recipe_id'])
			);
		}
        // EOF Related Recipe

		if (isset($this->request->get['recipe_id'])) {
			$recipe_id = (int)$this->request->get['recipe_id'];
		} else {
			$recipe_id = "";
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_recipe'),
			'href' => $this->url->link('recipe/recipe')
		);
		
		$data['recipe_id'] = (int)$this->request->get['recipe_id'];
		
		$data['recipies'] = $this->model_catalog_recipe->getRecipe($recipe_id);
		
		$recipe_info = $this->model_catalog_recipe->getRecipe($recipe_id);

	    	if ($recipe_info) {
			$this->document->setTitle($recipe_info['meta_title']);
			$this->document->setDescription($recipe_info['meta_description']);
			$this->document->setKeywords($recipe_info['meta_keyword']);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			$data['breadcrumbs'][] = array(
				'text' => $recipe_info['title'],
				'href' => $this->url->link('recipe/recipe/info', 'recipe_id=' . $this->request->get['recipe_id'] . $url)
			);
			
			// star-rating start
			$this->document->addStyle('catalog/view/javascript/star-rating/css/style.css');
			// star-rating end

			$data['heading_title'] 	= $recipe_info['title'];
			$data['title']			= $recipe_info['title'];
			$data['author'] 		= $recipe_info['author'];
			
			$data['publish_date'] 	= $recipe_info['date_added'];
			
			$data['image'] 			= $recipe_info['image'];
			
			
			//Youtube Embed Video
			$videoId = '';
		    $origin = 'youtube';
		    $embedUrl = '';
		    if(!empty($recipe_info['youtube_url'])) {
			    $parts = parse_url($recipe_info['youtube_url']);

			    if(isset($parts['query'])) {
			    	parse_str($parts['query'], $query);
			    	if(isset($query['v'])) {
			    		$videoId = $query['v'];
			    	}
		    	}			
			    if(empty($videoId) && isset($parts['host']) && ($parts['host'] == 'youtu.be' || $parts['host'] == 'www.youtube.com' )) {
			    	$urlParts = explode('/', $parts['path']);
			    	// remove any empty arrays from trailing
			    	if (utf8_strlen(end($parts)) == 0) {
			    		array_pop($parts);
			    	}
			    	$videoId = end($urlParts); 
		 	    }

		 	    if(isset($parts['host']) && ($parts['host'] == 'youtu.be' || $parts['host'] == 'www.youtube.com' || $parts['host'] == 'youtube.com') ) {
		 	    	$origin = 'youtube';
		 	    }
		    }

		    if(!empty($videoId)) {
		    	if($origin == 'youtube') {
		    		$data['youtube_url'] = 'https://www.youtube-nocookie.com/embed/' . $videoId;
		    	}
		    }
			//EOF Youtube Embed Video
			
			
			$data['description']	=  html_entity_decode($recipe_info['description'], ENT_QUOTES, 'UTF-8');
			$data['directions']	=  html_entity_decode($recipe_info['directions'], ENT_QUOTES, 'UTF-8');
			$data['ingredients']	=  html_entity_decode($recipe_info['ingredients'], ENT_QUOTES, 'UTF-8');
					
			$data['text_empty'] 		= $this->language->get('text_empty');
			$data['text_recipe'] 		= $this->language->get('text_recipe');
			$data['text_sort'] 			= $this->language->get('text_sort');
			$data['button_continue'] 	= $this->language->get('button_continue');
			$data['text_description'] 	= $this->language->get('text_description');
			$data['text_directions'] 	= $this->language->get('text_directions');
			$data['text_ingredients']	= $this->language->get('text_ingredients');
			
			$data['text_related']		= $this->language->get('text_related');
    		$data['text_tax'] = $this->language->get('text_tax');
    		
    		// Buttons
    		$data['button_cart'] = $this->language->get('button_cart');
    		$data['button_wishlist'] = $this->language->get('button_wishlist');
    		$data['button_compare'] = $this->language->get('button_compare');

			
			// Start Riview code
			
			$data['entry_qty']                = $this->language->get('entry_qty');
			$data['entry_name']               = $this->language->get('entry_name');
			$data['entry_review']             = $this->language->get('entry_review');
			$data['entry_rating']             = $this->language->get('entry_rating');
			$data['entry_good']               = $this->language->get('entry_good');
			$data['entry_bad']  			  = $this->language->get('entry_bad');
			
			$data['text_write']            	  = $this->language->get('text_write');
			$data['text_reviews']             = $this->language->get('text_reviews');
			$data['text_no_reviews']  		  = $this->language->get('text_no_reviews');
			$data['text_note']  			  = $this->language->get('text_note');
			$data['text_loading']			  = $this->language->get('text_loading');
			
			
			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}
			
			$data['reviews'] = array();
			$data['reviews'] = $this->model_catalog_recipe->getReviewsByRecipeId($this->request->get['recipe_id'],($page - 1) * 5, 5);
			
			$review_total = $this->model_catalog_recipe->getTotalReviewsByRecipeId($this->request->get['recipe_id']);
			
			$data['r_total'] = $review_total;

    		$pagination = new Pagination();
    		$pagination->total = $review_total;
    		$pagination->page = $page;
    		$pagination->limit = 5;
    		$pagination->url = $this->url->link('recipe/recipe/info', 'recipe_id=' . $this->request->get['recipe_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();
		
		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		// end review code
			

			// Image code *
			$this->load->model('tool/image');
			
			if ($recipe_info['homepage_image']) {
				$data['image'] = $this->model_tool_image->resize($recipe_info['homepage_image'], 1100, 520);
			} else {
				$data['image'] = '';
			}

			// End Image code
			
			
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['continue'] = $this->url->link('common/home');
			$data['category_link'] = $this->url->link('recipe/recipe');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('recipe/recipe_info', $data));
			
		}
		else
		{
			$url = '';

			if (isset($this->request->get['recipe_id'])) {
				$url .= '&recipe_id=' . $this->request->get['recipe_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('recipe/recipe/info', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
			
			

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['header'] = $this->load->controller('common/header');
			$data['footer'] = $this->load->controller('common/footer');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
		
	}
	// Review Function

	public function write() {
		$this->load->language('recipe/recipe');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['review']) < 25) || (utf8_strlen($this->request->post['review']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			}

			// Captcha
			 if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					$json['error'] = $captcha;
				}
			} 

			if (!isset($json['error'])) {
				$this->load->model('catalog/recipe');

				$this->model_catalog_recipe->addReview($this->request->get['recipe_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	} 
	
}