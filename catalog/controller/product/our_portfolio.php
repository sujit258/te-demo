<?php
class ControllerProductOurPortfolio extends Controller {
	public function index() {
	    $this->load->language('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->document->setTitle("Our Products");
		$category_id = 572;
		
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => 'Our Products',
			'href' => $this->url->link('product/our_portfolio')
		);
		
		$data['path'] = "&path=" . $category_id;
		
		
		$data['top_banner'] = $this->url->link("product/product" . $data['path'] . "&product_id=6340");
		$data['oprte'] = $this->url->link("product/our_portfolio/breakfastReadyToEat");
		$data['oprtc'] = $this->url->link("product/our_portfolio/breakfastReadyToCook");
		$data['oprs'] = $this->url->link("product/our_portfolio/roastedSeeds");
		$data['opsb'] = $this->url->link("product/our_portfolio/seedsBerries");
		$data['oatmeal'] = $this->url->link("product/product" . $data['path'] . "&product_id=6423");
		$data['chatpati'] = $this->url->link("product/product" . $data['path'] . "&product_id=6411");
		$data['raw_flax'] = $this->url->link("product/product" . $data['path'] . "&product_id=5787");;
		$data['raw_sunflower'] = $this->url->link("product/product" . $data['path'] . "&product_id=4533");
		$data['raw_seeds'] = $this->url->link("product/category" . $data['path'] . "_527");

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('product/our_portfolio', $data));
	}
	
	public function breakfastReadyToEat() {
	    $this->load->language('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->document->setTitle("Breakfast - Ready to Eat");
		$category_id = 573;
		$base_path = "572_573";
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => 'Our Products',
			'href' => $this->url->link('product/our_portfolio')
		);
		
		$data['path_oatmeal'] = $base_path . "_579";
		$data['path_muesli'] = $base_path . "_577";
		$data['path_flakes'] = $base_path . "_578";
		
		$data['oatmeal'] = $this->url->link("product/category&path=" . $data['path_oatmeal']);
		$data['muesli'] = $this->url->link("product/category&path=" . $data['path_muesli']);
		$data['flakes'] = $this->url->link("product/category&path=" . $data['path_flakes']);
		$data['strawberry_muesli'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4073");
		$data['no_added_sugar_muesli'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6372");
		$data['fruit_nut_muesli'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4074");
		$data['multigrain_bix'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6431");
		$data['jowa_bix'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6432");
		$data['oatmeal'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6423");
		
		    
		$category_info = $this->model_catalog_category->getCategory($category_id);
		$data['breadcrumbs'][] = array(
			'text' => $category_info['name'],
			'href' => $this->url->link('product/our_portfolio/breakfastReadyToEat')
		);
		
		$data['category_oatmeal'] = 579;
		$data['category_muesli'] = 577;
		$data['category_flakes'] = 578;
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('product/ready_to_eat', $data));
	}
	
	public function breakfastReadyToCook() {
	    $this->load->language('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->document->setTitle("Breakfast - Ready to Cook");
		$category_id = 574;
		$base_path = "572_574";
		
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => 'Our Products',
			'href' => $this->url->link('product/our_portfolio')
		);
		
		$data['path_poha'] = $base_path . "_580";
		$data['path_grains'] = $base_path . "_581";
		$data['path_oats'] = $base_path . "_582";
		    
		$category_info = $this->model_catalog_category->getCategory($category_id);
		$data['breadcrumbs'][] = array(
			'text' => $category_info['name'],
			'href' => $this->url->link('product/our_portfolio/breakfastReadyToCook')
		);
		
		$data['category_poha'] = 580;
		$data['category_grains'] = 581;
		$data['category_oats'] = 582;
		
		$data['poha'] = $this->url->link("product/category&path=" . $data['path_poha']);
		$data['grains'] = $this->url->link("product/category&path=" . $data['path_grains']);
		$data['oats'] = $this->url->link("product/category&path=" . $data['path_oats']);
		$data['rolled_oats'] = $this->url->link("product/product&path=" . $base_path . "&product_id=5657");
		$data['oats_poha'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6436");
		$data['oat_bix'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6433");
		$data['quinoa'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4523");
		$data['all_oats'] = $this->url->link("product/category&path=" . $base_path . "_559");
		$data['super_grains'] = $this->url->link("product/category&path=" . $base_path . "_528");
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('product/ready_to_cook', $data));
	}
	
	public function seedsBerries() {
	    $this->load->language('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->document->setTitle("Seeds & Berries");
		$category_id = 575;
		$base_path = "572_575";
		
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => 'Our Products',
			'href' => $this->url->link('product/our_portfolio')
		);
		
		$category_info = $this->model_catalog_category->getCategory($category_id);
		$data['breadcrumbs'][] = array(
			'text' => $category_info['name'],
			'href' => $this->url->link('product/our_portfolio/seedsBerries')
		);
		
		$data['raw_flax'] = $this->url->link("product/product&path=" . $base_path . "&product_id=5787");
		$data['raw_chia'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4534");
		$data['cranberries'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4977");
		$data['blueberries'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4978");
		$data['raw_pumpkin'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4535");
		$data['raw_sunflower'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4533");
		$data['raw_seeds'] = $this->url->link("product/category&path=" . $base_path . "_527");
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('product/seeds_berries', $data));
	}
	
	public function roastedSeeds() {
	    $this->load->language('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->document->setTitle("Roasted Seeds");
		$category_id = 576;
		$base_path = "572_576";
		
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => 'Our Products',
			'href' => $this->url->link('product/our_portfolio')
		);
		
		$category_info = $this->model_catalog_category->getCategory($category_id);
		$data['breadcrumbs'][] = array(
			'text' => $category_info['name'],
			'href' => $this->url->link('product/our_portfolio/roastedSeeds')
		);
		
		$data['antioxidant'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6169");
		$data['super_seeds'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6340");
		$data['roasted_pumpkin'] = $this->url->link("product/product&path=" . $base_path . "&product_id=5534");
		$data['roasted_sunflower'] = $this->url->link("product/product&path=" . $base_path . "&product_id=4539");
		$data['protein_mix'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6376");
		$data['chilli_masala_pumpkin'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6413");
		$data['pumpkin_honey_cluster'] = $this->url->link("product/product&path=" . $base_path . "&product_id=6341");
		$data['pumpkin_cheesy_onion'] = $this->url->link("product/product&path=" . $base_path . "&product_id=5375");
		
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('product/roasted_seeds', $data));
	}
}
?>