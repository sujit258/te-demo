<?php
class ControllerProductOurProducts extends Controller {
	public function index() {
	    // Added to cart
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js');
		// Added to cart
		$this->document->setTitle("Buy Healthy Food Products Online in India | True Elements");
        
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Shop',
			'href' => $this->url->link('product/our_products')
		);

		$data['heading_title'] = $this->language->get('catalogue');
		
		
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');
		
		$this->load->model('catalog/ocproductrotator');
		
		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = 533;

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

			$category_info = $this->model_catalog_category->getCategory(533);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$category_id = 0;
		}
		$category_info = $this->model_catalog_category->getCategory(533);
		
		if ($category_info) {
		     $this->document->setTitle($category_info['meta_title']);
			 $this->document->setDescription($category_info['meta_description']);
			 
			 //Banners
			 //$data['slider_gifting_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-gifting-desktop.jpg', 1920, 320);
			 $data['slider_daily_dose_trail_mix_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-daily-dose-trail-mix-desktop.jpg', 1920, 320);
			 $data['slider_7_in_1_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-7-in-1-desktop.jpg', 1920, 320);
			 $data['slider_raw_honey_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-desktop-11.jpg', 1920, 320);
			 $data['slider_fruit_nut_muesli_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-fruit-nut-muesli-desktop.jpg', 1920, 320);
			 $data['slider_berries_mix_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-berries-mix-desktop.jpg', 1920, 320);
			 
			 $data['slider_crunchy_nuts_berries_muesli_desktop'] = $this->model_tool_image->resize('catalog/Banner/our-store-crunchy-nuts-berries-muesli-desktop.jpg', 1920, 320);
			 $data['chocolate_granola_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-chocolate-granola-desktop.jpg', 1920, 320);
			 $data['chocolate_granola_450gm_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-chocolate-granola-450gm-desktop.jpg', 1920, 320);
			 $data['rolled_oats_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-rolled-oats-desktop.jpg', 1920, 320);
			 $data['wheat_flakes_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-wheat-flakes-desktop.jpg', 1920, 320);
			 $data['chilli_masala_pumpkin_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-chilli-masala-pumpkin-desktop.jpg', 1920, 320);
			 $data['dried_blueberries_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-dried-blueberries-desktop.jpg', 1920, 320);
			 $data['seven_in_one_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-7in1-desktop.jpg', 1920, 320);
			 $data['crunchy_nuts_berries_muesli_desktop'] = $this->model_tool_image->resize('catalog/Banner/shopnow-crunchy-nuts-berries-muesli-desktop.jpg', 1920, 320);
			 
			 //$data['slider_gifting_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-gifting-mobile.jpg', 1920, 836);
			 $data['slider_daily_dose_trail_mix_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-daily-dose-trail-mix-mobile.jpg', 1920, 836);
			 $data['slider_7_in_1_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-7-in-1-mobile.jpg', 1920, 836);
			 $data['slider_raw_honey_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-mobile-11.jpg', 1920, 836);
			 $data['slider_fruit_nut_muesli_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-fruit-nut-muesli-mobile.jpg', 1920, 836);
			 $data['slider_berries_mix_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-slider-berries-mix-mobile.jpg', 1920, 836);
			 
			 $data['slider_crunchy_nuts_berries_muesli_mobile'] = $this->model_tool_image->resize('catalog/Banner/our-store-crunchy-nuts-berries-muesli-mobile.jpg', 1920, 836);
			 
			 $data['rolled_oats_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-rolled-oats-mobile.jpg', 1920, 836);
			 $data['chocolate_granola_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-chocolate-granola-mobile.jpg', 1920, 836);
			 $data['chocolate_granola_450gm_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-chocolate-granola-450gm-mobile.jpg', 1920, 836);
			 $data['wheat_flakes_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-wheat-flakes-mobile.jpg', 1920, 836);
			 $data['chilli_masala_pumpkin_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-chilli-masala-pumpkin-mobile.jpg', 1920, 836);
			 $data['dried_blueberries_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-dried-blueberries-mobile.jpg', 1920, 836);
			 $data['seven_in_one_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-7in1-mobile.jpg', 1920, 836);
			 $data['crunchy_nuts_berries_muesli_mobile'] = $this->model_tool_image->resize('catalog/Banner/shopnow-crunchy-nuts-berries-muesli-mobile.jpg', 1920, 836);
			 
			$deal_data = array();
		    $deals_filter_data = array(
		        'limit' => 100    
		    );
		    $active_deals = $this->model_catalog_product->getDeals($deals_filter_data);
            if($active_deals) {
                foreach($active_deals AS $deal) {
                    $deal_data[$deal['product_id']] = $deal['coupon'];
                }
            }
		    
		    $data['products'] = array();

			$filter_data = array(
				'filter_category_id' => 533,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => 500
			);

			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);
			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], 800, 1007);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}
				
				$product_rotator_image = '';
		        $product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($result['product_id']);

				if($product_rotator_image) {
					$rotator_image = $this->model_tool_image->resize($product_rotator_image, 800, 1007);
				} else {
					$rotator_image = false;
				}
				
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
                
				$options = NULL;
				if($result['product_type'] != 5) {
				    $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				} else {
				    $price = false;
				}
			    
			    $percent = 0;
				if($result['special'] > 0) {
				    $percent = round(100 - (($result['special'] / $result['price'])) * 100 ,0 ) > 0 ? sprintf($this->language->get('%s'), (round(100 - (($result['special'] / $result['price'])) * 100 ,0 ))) . '% OFF' : '';
				}
			    
			    $name = html_entity_decode(substr($result['name'],0,55));
			    if(strlen($result['name']) > 55) {
			        $name .= "...";
			    }
			    
			    if(array_key_exists($result['product_id'],$deal_data) && $result['quantity'] > 0) {
		            $coupon = $deal_data[$result['product_id']];
		        } else {
		            $coupon = '';
		        }
		        
		        if($coupon){
    		        $coupon_query = $this->db->query("SELECT type,discount FROM " . DB_PREFIX . "coupon WHERE code = '" . $coupon . "'");
    		        $type     = $coupon_query->row['type'];
    		        $discount = $coupon_query->row['discount'];
    		        
    		        $product_price = $result['special'] ? ($result['special']) : ($result['price']);
    		        
    		        if ($type == 'P') {
                        $current_discount = (float) ($product_price * ((float) $discount / 100));
                    } else {
                        $current_discount = (float) $discount;
                    }
                    
                    $coupon_discount = $this->currency->format(((float)$product_price - $current_discount),$currency_code);
                    
                    $coupon_discount = $this->currency->format($this->tax->calculate(($product_price - $current_discount), $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
		        } else {
		            $coupon_discount = '';
		        } 
			    
			    if($result['category_name'] == "Flakes &amp; Bix" || $result['category_name'] == "Flakes & Bix") {
			        $data['flakesbixproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Gift") {
			        $data['giftproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Gift Packs") {
			        $data['giftpacksproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Super saver packs") {
			        $data['sspproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Muesli") {
			        $data['muesliproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Granola") {
			        $data['granolaproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Oatmeal") {
			        $data['oatmealproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Seeds Mixes") {
			        $data['seedsmixesproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Oats") {
			        $data['oatsproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Supergrains") {
			        $data['supergrainsproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "On the Go Snacks") {
			        $data['otgsnacksproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Raw Seeds") {
			        $data['rawseedsproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Roasted Seeds") {
			        $data['roastedseedsproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Others") {
			        $data['othersproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Tea") {
			        $data['teaproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Will Come Back Soon") {
			        $data['wcbsproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    } else if($result['category_name'] == "Combo") {
			        $data['comboproducts'][] = array(
				        //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				        'percent'     => $percent ? $percent : '',
					    'product_id'  => $result['product_id'],
					    'thumb'       => $image,
					    'rotator_image' => $rotator_image,
 					    'name'        => $name,
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
 					    'options'     => $options,
					    'product_type' => $result['product_type'],
					    'category_name' =>$result['category_name'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    }
			}
		}

		//$data['column_left'] = $this-> getlayout();
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
        
        //print_r($data['column_left']);
        
		$this->response->setOutput($this->load->view('product/our_product', $data));
	}
	
    public function getlayout() {	
    		$this->load->model('design/layout');
    		$this->load->model('extension/module');
    		$data['modules'] = array();
    
    		$modules = $this->model_design_layout->getLayoutModules(3, 'column_left');
    
    		foreach ($modules as $module) {
    			$part = explode('.', $module['code']);
    
    			if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
    				$module_data = $this->load->controller('extension/module/' . $part[0]);
    
    				if ($module_data) {
    					$data['modules'][] = $module_data;
    				}
    			}
    
    			if (isset($part[1])) {
    				$setting_info = $this->model_extension_module->getModule($part[1]);
    
    				if ($setting_info && $setting_info['status']) {
    					$output = $this->load->controller('extension/module/' . $part[0], $setting_info);
    
    					if ($output) {
    						$data['modules'][] = $output;
    					}
    				}
    			}
    		}
    
    		return $this->load->view('common/column_left', $data);
    }	
}