<?php
class ControllerProductFathersDay extends Controller {
	public function index() {
	    // Added to cart
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js');
		// Added to cart 
        
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
	
 
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');
		
		$this->load->model('catalog/ocproductrotator');
		
		
    		$category_id = 624;	
    		$data['categories'] = array();

			$subcategories = $this->model_catalog_category->getCategories(624);
			
			$category_infos = $this->model_catalog_category->getCategory($category_id);
			 
			
			$this->document->setTitle($category_infos['meta_title']);
			$this->document->setDescription($category_infos['meta_description']);
			$this->document->setKeywords($category_infos['meta_keyword']);

			$data['heading_title'] = $category_infos['name'];
			
			$data['breadcrumbs'][] = array(
    			'text' => $category_infos['name'],
    			'href' => $this->url->link('product/fathers_day')
    		);
		
			//$data['shop_categoris'] = $this->model_catalog_category->getShopCategories();
		    $data['products']  = array();

        		$filter_data = array(
        			'filter_category_id' => 624,
        			'filter_filter'      => $filter,
        			'sort'               => $sort,
        			'order'              => $order,
        			'start'              => ($page - 1) * $limit,
        			'limit'              => 500
        		);
    		
    	    $product_total = $this->model_catalog_product->getTotalProducts($filter_data);
            
			$results = $this->model_catalog_product->getProducts($filter_data);
			 
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}
				
				if ($result['fathers_day_image']) {
					$fathers_day_image = $this->model_tool_image->resize($result['fathers_day_image'], 1500, 980); 
				} else {
					$fathers_day_image = 'https://harippa.co.in/image/cachewebp/catalog/New/te-labs/left-image-1500x980.webp'; 
				}
        
                if ($result['te_lab_product_image']) {
					$te_lab_product_image = $this->model_tool_image->resize($result['te_lab_product_image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$te_lab_product_image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} 

        
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$price11 = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
					$disc_per = $price11 * 35/100;
					$price11 = $price11 - $disc_per;
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}
				
				if($discount_per > 0) {
				    $special = $this->currency->format($this->tax->calculate(($result['price'] - ($result['price'] * $discount_per / 100)), $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				$personalized = 0;
				$options = NULL;
				if($result['product_id'] != 6438 && $result['product_id'] != 6345 && $result['product_id'] != 6449) {
				    $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				} else {
				    $personalized = 1;
				}
				
				$percent = 0;
				if($result['special'] > 0) {
				    $percent = round(100 - (($result['special'] / $result['price'])) * 100 ,0 ) > 0 ? sprintf($this->language->get('%s'), (round(100 - (($result['special'] / $result['price'])) * 100 ,0 ))) . '% OFF' : '';
				}
				
				if(array_key_exists($result['product_id'],$deal_data) && $result['quantity'] > 0) {
		            $coupon = $deal_data[$result['product_id']];
		        } else {
		            $coupon = '';
		        }
                
                if($coupon){
    		        $coupon_query = $this->db->query("SELECT type,discount FROM " . DB_PREFIX . "coupon WHERE code = '" . $coupon . "'");
    		        $type     = $coupon_query->row['type'];
    		        $discount = $coupon_query->row['discount'];
    		        
    		        $product_price = $result['special'] ? ($result['special']) : ($result['price']);
    		        
    		        if ($type == 'P') {
                        $current_discount = (float) ($product_price * ((float) $discount / 100));
                    } else {
                        $current_discount = (float) $discount;
                    }
                    
                    $coupon_discount = $this->currency->format(((float)$product_price - $current_discount),$currency_code);
                    
                    $coupon_discount = $this->currency->format($this->tax->calculate(($product_price - $current_discount), $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
		        } else {
		            $coupon_discount = '';
		        } 
		        
		        $this->load->model('catalog/review');
		        
		        $reviews = $this->model_catalog_review->getTopReviewByProductId($result['product_id'], ($page - 1) * 5, 5);

        		foreach ($reviews as $review) {
        			$data['reviews'][] = array(
        				'author'     => $review['author'],
        				'text'       => nl2br($review['text']),
        				'product_id'       => ($review['product_id']),
        				'rating'     => (int)$review['rating'],
        				'date_added' => date($this->language->get('date_format_short'), strtotime($review['date_added']))
        			);
        		}
        		
        		$max_per_discount = $this->model_catalog_product->getProductMaxDiscount($result['product_id']); 
        		
        		//$attribute = $this->model_catalog_product->getProductAttribute($result['product_id'],$category_id); 
        		
        		
		        if($result['product_id'] > 0) {
			    	$data['products'][] = array(
			    	    //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
			    	    'percent'     => $percent ? $percent : '',
			    	    'price11'     => $price11,
			    	    'percent'     => $max_per_discount ? $max_per_discount : '',
			    		'product_id'  => $result['product_id'],
			    		'category_name' => $result['category_name'],
			    		'jan'           => $result['jan'],
			    		'isbn'           => $result['isbn'],
			    		//'attribute'      => $attribute,
			    		'thumb'       => $image,
			    		'fathers_day_image'=> $fathers_day_image,
			    		'te_lab_product_image' => $te_lab_product_image, 
			    		'custom_image'=> $custom_image, 
			    		'custom_description'  => html_entity_decode($result['custom_description'], ENT_QUOTES, 'UTF-8'),
			    		'short_description'  => html_entity_decode($result['short'], ENT_QUOTES, 'UTF-8'),
 		    			'name'        => html_entity_decode(substr($result['name'],0,55) . ''),
 			    		'name_mobile' => html_entity_decode(substr($result['name'],0,38) . ''),
 					    'stock_status' => $result['quantity'] > 0 ? 'In' : 'Out',
 					    'sort_order' => $result['sort_order'],
 					    'discount'      => $max_per_discount,
 					    'coupon_discount'        => $max_per_discount,  
					    'category_name' =>$result['category_name'], 
					    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					    'options'     => $options,
					    'reviews'     => $data['reviews'],
					    'personalized'=> $personalized,
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'price1' => $this->load->controller('extension/module/joseanmatias_preco_cupom/listview', $result),
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
		        }
			}	 
			 
		 
		//$data['column_left'] = $this-> getlayout();
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
        
		$this->response->setOutput($this->load->view('product/fathers_day', $data));
	}
		
}