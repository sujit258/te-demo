<?php
class ControllerProductDeals extends Controller {
	public function index() {
		$this->load->model('catalog/product');
		$this->load->model('catalog/ocproductrotator');
		$this->load->model('tool/image');
		$this->load->language('product/deals');
		
		// Added to cart
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js'); 
		// Added to cart
        
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}
        
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/deals')
		);
		
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_coupon'] = $this->language->get('text_coupon');
		$data['text_quantity'] = $this->language->get('text_quantity');
		
		$filter_data = array(
		    'start'     => ($page - 1) * $limit,
			'limit'     => $limit    
		);
    
        $deals_total = $this->model_catalog_product->getDealsCount();
		$all_deals = $this->model_catalog_product->getDeals($filter_data);

		if ($all_deals) {
			$this->document->setTitle($this->language->get('heading_title'));

			$data['heading_title'] = $this->language->get('heading_title');

			foreach($all_deals AS $deal) {
		        $deal_product = $this->model_catalog_product->getProduct($deal['product_id']);
		        
		        if ($deal_product['image']) {
					$image = $this->model_tool_image->resize($deal_product['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}
				
				$coupon_discount = 0;
				$coupon_type = "";
				$result = $this->model_catalog_product->getCouponDiscount($deal['coupon']);
				if($result) {
				    $coupon_discount = $result['discount'];
				    $coupon_type = $result['type'];
				}
				
				$deal_price = 0;
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($deal_product['price'], $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$deal_product['special']) {
					$special = $this->currency->format($this->tax->calculate($deal_product['special'], $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$deal_price = $this->currency->format($this->tax->calculate($deal_product['special'] - ($deal_product['special'] * $coupon_discount / 100), $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
					$deal_price = $this->currency->format($this->tax->calculate($deal_product['price'] - ($deal_product['price'] * $coupon_discount / 100), $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$deal_product['rating'];
				} else {
					$rating = false;
				}

				$product_id = $deal_product['product_id'];
				$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($product_id);

				if($product_rotator_image) {
					$rotator_image = $this->model_tool_image->resize($product_rotator_image, $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$rotator_image = false;
				}
				
				$personalized = 0;
				$options = NULL;
				if($deal['product_id'] != 6438 && $deal['product_id'] != 6345 && $deal['product_id'] != 6449) {
				    $options = $this->model_catalog_product->getProductOptions($deal['product_id']);
				} else {
				    $personalized = 1;
				}
				/*$option_id = $deal['option_id'] ? $deal['option_id'] : NULL;
				$option_value_id = $deal['option_value_id'] ? $deal['option_value_id'] : NULL;
				
				$deal_sku = array();
				if($option_id && $option_value_id) {
				    $product_option_id = $this->model_catalog_product->getProductOptionId($deal['product_id'], $option_id, $option_value_id);
				    foreach($options AS $option) {
				        if($option['product_option_id'] == $product_option_id && $option['option_id'] == $option_id) {
				            foreach($option['product_option_value'] AS $option_value) {
				                if($option_value['option_value_id'] == $option_value_id) {
				                    $deal_sku = array(
				                        'product_option_id'       => $product_option_id,
				                        'option_id'               => $option_id,
				                        'option_value_id'         => $option_value_id,
				                        'product_option_value_id' => $option_value['product_option_value_id'],
				                        'name'                    => $option_value['name'],
				                        'type'                    => $option['type'],
				                        'required'                => $option['required'],
				                        'price'                   => $option_value['price'],
				                        'price_prefix'            => $option_value['price_prefix']
				                    );
				                    if($option_value['price'] > 0 && $option_value['price_prefix'] == "+") {
				                        $price_with_option = $deal_product['price'] + $option_value['price'];
				                    } else if($option_value['price'] > 0 && $option_value['price_prefix'] == "-") {
				                        $price_with_option = $deal_product['price'] - $option_value['price'];
				                    }
				                }
				            }
				        }
				    }
				}
				
				if($price_with_option > 0) {
				    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					    $price = $this->currency->format($this->tax->calculate($price_with_option, $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					    
					    $mrp = $this->currency->format($this->tax->calculate($price_with_option, $deal_product['tax_class_id'], $this->config->get('config_tax')));
				        $discount = 100 - ($deal['price'] / $mrp * 100);
			    	} else {
					    $price = false;
				    }
				} else {
				    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					    $price = $this->currency->format($this->tax->calculate($deal_product['price'], $deal_product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					    
					    $mrp = $this->currency->format($this->tax->calculate($deal_product['price'], $deal_product['tax_class_id'], $this->config->get('config_tax')));
				        $discount = 100 - ($deal['price'] / $mrp * 100);
			    	} else {
					    $price = false;
				    }
				}*/
		        
		        $data['deals_data'][] = array(
		            'product_id'    => $deal['product_id'],
		            'thumb'         => $image,
					'rotator_image' => $rotator_image,
					'name'          => html_entity_decode(substr($deal_product['name'],0,55)),
 					'name_mobile'   => html_entity_decode(substr($deal_product['name'],0,38)), $deal_product['name'],
		            'coupon'        => $deal['coupon'],
		            'coupon_discount' => $coupon_discount,
		            'coupon_type'   => $coupon_type,
		            'options'       => $options,
		            'personalized'  => $personalized,
		            'quantity'      => $deal_product['quantity'],
		            'price'         => $price,
		            'special'       => $special,
		            'deal_price'    => $deal_price,
		            'discount'      => $deal['discount'],
		            'rating'        => $rating,
		            'date_start'    => $deal['date_start'],
		            'date_end'      => $deal['date_end'],
		            'href'          => $this->url->link('product/product', 'product_id=' . $deal['product_id'])
		        );
		    }
		    
		    $url = '';
		    if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
		    $data['limits'] = array();

			$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/deals', 'limit=' . $value)
				);
			}
			
			$url = '';
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $deals_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/deals', $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($deals_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($deals_total - $limit)) ? $deals_total : ((($page - 1) * $limit) + $limit), $deals_total, ceil($deals_total / $limit));

			if ($page == 1) {
			    $this->document->addLink($this->url->link('product/deals'), 'canonical');
			} elseif ($page == 2) {
			    $this->document->addLink($this->url->link('product/deals'), 'prev');
			} else {
			    $this->document->addLink($this->url->link('product/deals', '&page='. ($page - 1), true), 'prev');
			}

			if ($limit && ceil($deals_total / $limit) > $page) {
			    $this->document->addLink($this->url->link('product/deals', '&page='. ($page + 1), true), 'next');
			}

			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('product/deals', $data));
		} else {

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/deals', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
}
?>