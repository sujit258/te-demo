<?php
class ControllerProductGiftHampers extends Controller {
	private $error = array();

	public function index() {
	    $category_id = 496;
	    $data['quantity'] = 1;
	    
	    if($this->session->data['is_invoice']){
		    print_r($this->session->data['is_invoice']);
		} else {
		    print_r("no personal Note");
		}
		
	    // Added to cart
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js'); 
		// Added to cart
	    
		$this->load->language('product/product');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('catalog/ocproductrotator');
        $this->load->model('tool/image');
        $this->load->language('account/voucher');
        
		$data['giftvoucher'] = $this->url->link('product/gift_hampers/giftvoucher', '', true);
                
        if ($this->customer->isLogged()) {
		     $data['from_email'] = $this->customer->getEmail();
		     $data['from_name'] = $this->customer->getFirstName() . ' '  . $this->customer->getLastName();
		} 
		        
		$this->load->model('extension/total/voucher_theme');
        $data['voucher_themes'] = $this->model_extension_total_voucher_theme->getVoucherThemes();
        
		$data['text_description'] = $this->language->get('text_description');
		$data['text_agree'] = $this->language->get('text_agree');

		$data['entry_to_name'] = $this->language->get('entry_to_name');
		$data['entry_to_email'] = $this->language->get('entry_to_email');
		$data['entry_from_name'] = $this->language->get('entry_from_name');
		$data['entry_from_email'] = $this->language->get('entry_from_email');
		$data['entry_theme'] = $this->language->get('entry_theme');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_amount'] = $this->language->get('entry_amount');

		$data['help_message'] = $this->language->get('help_message');
		$data['help_amount'] = sprintf($this->language->get('help_amount'), $this->currency->format($this->config->get('config_voucher_min'), $this->session->data['currency']), $this->currency->format($this->config->get('config_voucher_max'), $this->session->data['currency']));
		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
		}
		
		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

			$category_info = $this->model_catalog_category->getCategory($category_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		}
        
        $category_info = $this->model_catalog_category->getCategory($category_id);
        
        if ($category_info) {
            $this->document->setTitle($category_info['meta_title']);
            $this->document->setDescription($category_info['meta_description']);
            $data['products'] = array();

            $filter_data = array(
				'filter_category_id' => $category_id,
				'start'              => 0,
				'limit'              => 10
			);

			$results = $this->model_catalog_product->getProducts($filter_data);

		    if ($results) {
		        //SOF Gift Packs
		        $data['gift_products'] = array();

			    $filter_gift_data = array(
		    		'filter_category_id'    => 533,
			    	'filter_category_name'  => "Gift Packs",
		    		'start'                 => 0,
		    		'limit'                 => 4
		    	);
                $gift_results = $this->model_catalog_product->getProducts($filter_gift_data);
                
                $g_deal_data = array();
		        $g_deals_filter_data = array(
		            'limit' => 100    
		        );
		        $g_active_deals = $this->model_catalog_product->getDeals($g_deals_filter_data);
                if($g_active_deals) {
                    foreach($g_active_deals AS $g_deal) {
                        $g_deal_data[$g_deal['product_id']] = $g_deal['coupon'];
                    }
                }
                
		        foreach($gift_results AS $g_result) {
		            if ($g_result['image']) {
				        $g_thumb = $this->model_tool_image->resize($g_result['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'));
			        } else {
				        $g_thumb = '';
			        }
			        
			        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					    $g_price = $this->currency->format($this->tax->calculate($g_result['price'], $g_result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				    } else {
					    $g_price = false;
				    }

				    if ((float)$g_result['special']) {
				    	$g_special = $this->currency->format($this->tax->calculate($g_result['special'], $g_result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				    } else {
				    	$g_special = false;
				    }
			        
			        if ($this->config->get('config_tax')) {
				        $g_tax = $this->currency->format((float)$g_result['special'] ? $g_result['special'] : $g_result['price'], $this->session->data['currency']);
			        } else {
				        $g_tax = false;
			        }
			        
			        if ($this->config->get('config_review_status')) {
				    	$g_rating = (int)$result['rating'];
				    } else {
				    	$g_rating = false;
				    }
			        
			        $g_product_rotator_image = '';
		            $g_product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($g_result['product_id']);

				    if($g_product_rotator_image) {
					    $g_rotator_image = $this->model_tool_image->resize($g_product_rotator_image, 800, 1007);
				    } else {
					    $g_rotator_image = false;
				    }
				    
				    $g_percent = 0;
				    if($g_result['special'] > 0) {
				        $g_percent = round(100 - (($g_result['special'] / $g_result['price'])) * 100 ,0 ) > 0 ? sprintf($this->language->get('%s'), (round(100 - (($g_result['special'] / $g_result['price'])) * 100 ,0 ))) . '% OFF' : '';
				    }
				    
				    $g_name = html_entity_decode(substr($g_result['name'],0,55));
			        if(strlen($g_result['name']) > 55) {
			            $g_name .= "...";
			        }
			    
			        if(array_key_exists($g_result['product_id'],$g_deal_data) && $g_result['quantity'] > 0) {
		                $g_coupon = $g_deal_data[$g_result['product_id']];
		            } else {
		                $g_coupon = '';
		            }
		            
		            if($g_coupon){
    		            $g_coupon_query = $this->db->query("SELECT type,discount FROM " . DB_PREFIX . "coupon WHERE code = '" . $g_coupon . "'");
    		            $g_type     = $g_coupon_query->row['type'];
    		            $g_discount = $g_coupon_query->row['discount'];
    		        
    		            $g_product_price = $g_result['special'] ? ($g_result['special']) : ($g_result['price']);
    		        
    		            if ($g_type == 'P') {
                            $g_current_discount = (float) ($g_product_price * ((float) $g_discount / 100));
                        } else {
                            $g_current_discount = (float) $g_discount;
                        }
                    
                        $g_coupon_discount = $this->currency->format(((float)$g_product_price - $g_current_discount), $this->session->data['currency']);
                    
                        $g_coupon_discount = $this->currency->format($this->tax->calculate(($g_product_price - $g_current_discount), $g_result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
		            } else {
		                $g_coupon_discount = '';
		            }
			        
			        $g_options = NULL;
				    if($g_result['product_type'] != 5) {
				        $g_options = $this->model_catalog_product->getProductOptions($g_result['product_id']);
				    } else {
				        $g_personalized = 1;
				    }
			        
			        $data['gift_products'][] = array(
			            'percent'     => $g_percent ? $g_percent : '',
					    'product_id'  => $g_result['product_id'],
					    'thumb'       => $g_thumb,
					    'rotator_image' => $g_rotator_image,
 					    'name'        => $g_name,
 					    'coupon'      => $g_coupon,
 					    'discount'      => $g_discount + 2,
 					    'coupon_discount'        => $g_coupon_discount,
 					    'options'     => $g_options,
					    'personalized'=> $g_personalized,
					    'category_name' =>$g_result['category_name'],
					    'quantity'    => $g_result['quantity'],
					    'price'       => substr($g_price,3) > 0 ? $g_price : "",
					    'special'     => $g_special,
 					    'tax'         => $g_tax,
					    'minimum'     => $g_result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $g_result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $g_result['product_id'] . $url)
                    );
		        }
		        //EOF Gift Packs
			    $data['heading_title'] = $category_info['name'];

			    $data['text_select'] = $this->language->get('text_select');
			    $data['text_manufacturer'] = $this->language->get('text_manufacturer');
		    	$data['text_model'] = $this->language->get('text_model');
			    $data['text_reward'] = $this->language->get('text_reward');
			    $data['text_points'] = $this->language->get('text_points');
			    $data['text_stock'] = $this->language->get('text_stock');
			    $data['text_discount'] = $this->language->get('text_discount');
		    	$data['text_tax'] = $this->language->get('text_tax');
		    	$data['text_option'] = $this->language->get('text_option');
			    $data['text_write'] = $this->language->get('text_write');
			    $data['text_note'] = $this->language->get('text_note');
			    $data['text_tags'] = $this->language->get('text_tags');
		    	$data['text_loading'] = $this->language->get('text_loading');

			    $data['entry_qty'] = $this->language->get('entry_qty');
			    $data['entry_name'] = $this->language->get('entry_name');

		    	$data['button_cart'] = $this->language->get('button_cart');
			    $data['button_wishlist'] = $this->language->get('button_wishlist');

			    $data['category_id'] = (int)$category_id;
			    
			    foreach($results AS $result) {
			        if ($result['image']) {
				        $thumb = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'));
			        } else {
				        $thumb = '';
			        }
			        
			        if ($this->config->get('config_tax')) {
				        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
			        } else {
				        $tax = false;
			        }
			        
			        $product_rotator_image = '';
		            $product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($result['product_id']);

				    if($product_rotator_image) {
					    $rotator_image = $this->model_tool_image->resize($product_rotator_image, 220, 300);
				    } else {
					    $rotator_image = false;
				    }
			        
			        $data['options'] = array();
			        
				    $options = array();
			        foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
				        $product_option_value_data = array();

				        foreach ($option['product_option_value'] as $option_value) {
				        	if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
				        		if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
				        			$price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
				        			if($result['special'] > 0) {
				        			    if($option_value['option_special_price'] > 0) {
				        			        $option_price = $this->tax->calculate($result['special'] + $option_value['option_special_price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
				        			    } else {
				        			        $option_price = $this->tax->calculate($result['special'] + $option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
				        			    }
				        			} else {
				        			    if($option_value['option_special_price'] > 0) {
				        			        $option_price = $this->tax->calculate($result['price'] + $option_value['option_special_price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
				        			    } else {
				        			        $option_price = $this->tax->calculate($result['price'] + $option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
				        			    }
				        			}
						        } else {
						           	$price = false;
						           	if($result['special'] > 0) {
						           	    $option_price = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
						           	} else {
						           	    $option_price = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
						           	}
						        }
						        $option_mrp = $this->tax->calculate($result['price'] + $option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
						        $product_option_value_data[] = array(
							        'product_option_value_id' => $option_value['product_option_value_id'],
							        'option_value_id'         => $option_value['option_value_id'],
							        'name'                    => $option_value['name'],
							        'weight'                  => (int)$option_value['weight'],
							        'nutritional_value'       => $option_value['nutritional_value'],
							        'option_desc'             => $option_value['option_desc'],
							        'image'                   => $this->model_tool_image->resize($option_value['image'], 255, 355),
							        'price'                   => '0',
							        'option_price'            => $this->currency->format($option_price, $this->session->data['currency']),
							        'option_price_int'        => $option_price,
							        'option_mrp'              => $this->currency->format($option_mrp, $this->session->data['currency']),
							        'price_prefix'            => $option_value['price_prefix']
						        );
					        }
				        }

				        $options[] = array(
				        	'product_option_id'    => $option['product_option_id'],
				        	'product_option_value' => $product_option_value_data,
				        	'option_id'            => $option['option_id'],
				        	'name'                 => $option['name'],
				        	'type'                 => $option['type'],
				        	'value'                => $option['value'],
				        	'required'             => $option['required']
				        );
			        }
			        
			        $data['products'][] = array(
					    'product_id'    => $result['product_id'],
					    'thumb'         => $thumb,
					    'rotator_image' => $rotator_image,
 					    'name'          => $result['name'],
 					    'options'       => $options,
					    'quantity'      => $result['quantity'],
					    'special'       => $special,
 					    'tax'           => $tax,
					    'href'          => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
			    }

			    $data['column_left'] = $this->load->controller('common/column_left');
			    $data['column_right'] = $this->load->controller('common/column_right');
			    $data['content_top'] = $this->load->controller('common/content_top');
			    $data['content_bottom'] = $this->load->controller('common/content_bottom');
			    $data['footer'] = $this->load->controller('common/footer');
			    $data['header'] = $this->load->controller('common/header');
			    $this->response->setOutput($this->load->view('product/gift_hampers', $data));
		    } else {
			    $url = '';

			    if (isset($this->request->get['path'])) {
			    	$url .= '&path=' . $this->request->get['path'];
			    }

			    if (isset($this->request->get['filter'])) {
			    	$url .= '&filter=' . $this->request->get['filter'];
			    }

			    if (isset($this->request->get['manufacturer_id'])) {
			    	$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			    }

			    if (isset($this->request->get['search'])) {
			    	$url .= '&search=' . $this->request->get['search'];
			    }

    			if (isset($this->request->get['tag'])) {
    				$url .= '&tag=' . $this->request->get['tag'];
    			}

	    		if (isset($this->request->get['description'])) {
	    			$url .= '&description=' . $this->request->get['description'];
	    		}

	    		if (isset($this->request->get['category_id'])) {
	    			$url .= '&category_id=' . $this->request->get['category_id'];
	    		}

    			if (isset($this->request->get['sub_category'])) {
    				$url .= '&sub_category=' . $this->request->get['sub_category'];
    			}

    			if (isset($this->request->get['sort'])) {
    				$url .= '&sort=' . $this->request->get['sort'];
    			}

    			if (isset($this->request->get['order'])) {
    				$url .= '&order=' . $this->request->get['order'];
    			}

    			if (isset($this->request->get['page'])) {
    				$url .= '&page=' . $this->request->get['page'];
    			}

    			if (isset($this->request->get['limit'])) {
    				$url .= '&limit=' . $this->request->get['limit'];
    			}


    			$this->document->setTitle($this->language->get('text_error'));

    			$data['heading_title'] = $this->language->get('text_error');

    			$data['text_error'] = $this->language->get('text_error');

    			$data['button_continue'] = $this->language->get('button_continue');

    			$data['continue'] = $this->url->link('common/home');

    			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

    			$data['column_left'] = $this->load->controller('common/column_left');
    			$data['column_right'] = $this->load->controller('common/column_right');
    			$data['content_top'] = $this->load->controller('common/content_top');
    			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			    $data['footer'] = $this->load->controller('common/footer');
			    $data['header'] = $this->load->controller('common/header');

			    $this->response->setOutput($this->load->view('error/not_found', $data));
		    }
	    }
	}
	
		public function giftvoucher() {
	    /*if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/address', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}*/
		
		$this->load->language('account/voucher');

		$this->document->setTitle($this->language->get('heading_title'));

		if (!isset($this->session->data['vouchers'])) {
			$this->session->data['vouchers'] = array();
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->session->data['vouchers'][mt_rand()] = array(
				'description'      => sprintf($this->language->get('text_for'), $this->currency->format($this->request->post['amount'], $this->session->data['currency']), $this->request->post['to_name']),
				'to_name'          => $this->request->post['to_name'],
				'to_email'         => $this->request->post['to_email'],
				'from_name'        => $this->request->post['from_name'],
				'from_email'       => $this->request->post['from_email'],
				'voucher_theme_id' => $this->request->post['voucher_theme_id'],
				'message'          => $this->request->post['message'],
				'amount'           => $this->currency->convert($this->request->post['amount'], $this->session->data['currency'], $this->config->get('config_currency'))
			);

			$this->response->redirect($this->url->link('checkout/checkout'));
		}

		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['to_name'])) {
			$data['error_to_name'] = $this->error['to_name'];
		} else {
			$data['error_to_name'] = '';
		}

		if (isset($this->error['to_email'])) {
			$data['error_to_email'] = $this->error['to_email'];
		} else {
			$data['error_to_email'] = '';
		}

		if (isset($this->error['from_name'])) {
			$data['error_from_name'] = $this->error['from_name'];
		} else {
			$data['error_from_name'] = '';
		}

		if (isset($this->error['from_email'])) {
			$data['error_from_email'] = $this->error['from_email'];
		} else {
			$data['error_from_email'] = '';
		}

		if (isset($this->error['theme'])) {
			$data['error_theme'] = $this->error['theme'];
		} else {
			$data['error_theme'] = '';
		}

		if (isset($this->error['amount'])) {
			$data['error_amount'] = $this->error['amount'];
		} else {
			$data['error_amount'] = '';
		}

		$data['action'] = $this->url->link('account/voucher', '', true);

		if (isset($this->request->post['to_name'])) {
			$data['to_name'] = $this->request->post['to_name'];
		} else {
			$data['to_name'] = '';
		}

		if (isset($this->request->post['to_email'])) {
			$data['to_email'] = $this->request->post['to_email'];
		} else {
			$data['to_email'] = '';
		}

		if (isset($this->request->post['from_name'])) {
			$data['from_name'] = $this->request->post['from_name'];
		} elseif ($this->customer->isLogged()) {
			$data['from_name'] = $this->customer->getFirstName() . ' '  . $this->customer->getLastName();
		} else {
			$data['from_name'] = '';
		}

		if (isset($this->request->post['from_email'])) {
			$data['from_email'] = $this->request->post['from_email'];
		} elseif ($this->customer->isLogged()) {
			$data['from_email'] = $this->customer->getEmail();
		} else {
			$data['from_email'] = '';
		}

		$this->load->model('extension/total/voucher_theme');

		$data['voucher_themes'] = $this->model_extension_total_voucher_theme->getVoucherThemes();

		if (isset($this->request->post['voucher_theme_id'])) {
			$data['voucher_theme_id'] = $this->request->post['voucher_theme_id'];
		} else {
			$data['voucher_theme_id'] = '';
		}

		if (isset($this->request->post['message'])) {
			$data['message'] = $this->request->post['message'];
		} else {
			$data['message'] = '';
		}

		if (isset($this->request->post['amount'])) {
			$data['amount'] = $this->request->post['amount'];
		} else {
			$data['amount'] = $this->currency->format($this->config->get('config_voucher_min'), $this->config->get('config_currency'), false, false);
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('product/gift_hampers'));
	}
	
	

	protected function validate() {
		if ((utf8_strlen($this->request->post['to_name']) < 1) || (utf8_strlen($this->request->post['to_name']) > 64)) {
			$this->error['to_name'] = $this->language->get('error_to_name');
		}

		if ((utf8_strlen($this->request->post['to_email']) > 96) || !filter_var($this->request->post['to_email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['to_email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['from_name']) < 1) || (utf8_strlen($this->request->post['from_name']) > 64)) {
			$this->error['from_name'] = $this->language->get('error_from_name');
		}

		/*if ((utf8_strlen($this->request->post['from_email']) > 96) || !filter_var($this->request->post['from_email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['from_email'] = $this->language->get('error_email');
		}*/

		if (!isset($this->request->post['voucher_theme_id'])) {
			$this->error['theme'] = $this->language->get('error_theme');
		}

		if (($this->currency->convert($this->request->post['amount'], $this->session->data['currency'], $this->config->get('config_currency')) < $this->config->get('config_voucher_min')) || ($this->currency->convert($this->request->post['amount'], $this->session->data['currency'], $this->config->get('config_currency')) > $this->config->get('config_voucher_max'))) {
			$this->error['amount'] = sprintf($this->language->get('error_amount'), $this->currency->format($this->config->get('config_voucher_min'), $this->session->data['currency']), $this->currency->format($this->config->get('config_voucher_max'), $this->session->data['currency']));
		}

		if (!isset($this->request->post['agree'])) {
			$this->error['warning'] = $this->language->get('error_agree');
		}

		return !$this->error;
	}
}