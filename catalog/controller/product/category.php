<?php
class ControllerProductCategory extends Controller {
	public function index() {
	    // Added to cart
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
	    $this->document->addStyle('/catalog/view/theme/tt_presiden10/stylesheet/stylesheet.css');
	    $this->document->addStyle('catalog/view/theme/tt_presiden10/stylesheet/product-page.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js');
		// Added to cart
		
	    $this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {
			$this->document->setTitle($category_info['meta_title']);
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);

			$data['heading_title'] = $category_info['name'];
			$data['category_id'] = $category_id;

			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
			);
			
			$data['category_strip_desktop'] = $this->model_tool_image->resize('catalog/Banner/hp-brand-store-desktop.jpg', 1920, 100);
			$data['category_strip_mobile'] = $this->model_tool_image->resize('catalog/Banner/hp-brand-store-mobile.jpg', 1920, 400);

			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
			} else {
				$data['thumb'] = '';
			}
			
			// TE Labs
			
			$data['category_id'] = $category_id;
			
    		 /*if (!$this->customer->isLogged() && $category_id == '622') {
    			$this->session->data['redirect'] = $this->url->link('product/category&path=622', '', true);
    
    			$this->response->redirect($this->url->link('account/login', '', true));
    		}*/
    		 
		    $data['customer_group_id'] = $customer_group_id = $this->customer->getGroupId();
            $data['customer_id'] = $this->customer->getId();
            $data['is_logged'] = $this->customer->isLogged();
            
			if($category_id == '622'){
			    $data['top_image'] = $category_info['image'];
                $this->load->model('account/customer');
                $data['action'] = $this->url->link('product/category/applytelabs', '', true);
                
                $data['success'] = $this->session->data['success']; 
                $data['applied'] = $this->model_account_customer->getTotalTELabsCustomersByID($this->customer->getId());
            }
            
            if($category_id == '622' && $customer_group_id == 9) {
                $discounts = $this->config->get('total_customer_group_discount_customer_group_id');
                $data['discount_per'] = $discount_per = $discounts[$customer_group_id];
            } else {
                $data['discount_per'] = $discount_per = 0;
            }
            
            // TE Labs
            
			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['categories'] = array();

			$results = $this->model_catalog_category->getCategories($category_id);

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);

				$data['categories'][] = array(
					'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				);
			}
			
			$deal_data = array();
		    $deals_filter_data = array(
		        'limit' => 100    
		    );
		    $active_deals = $this->model_catalog_product->getDeals($deals_filter_data);
            if($active_deals) {
                foreach($active_deals AS $deal) {
                    $deal_data[$deal['product_id']] = $deal['coupon'];
                }
            }

			$data['products'] = array();

			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);

			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}
				
				if ($result['te_lab_product_image']) {
					$te_lab_product_image = $this->model_tool_image->resize($result['te_lab_product_image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$te_lab_product_image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}
				
				if ($result['te_lab_image']) {
					$te_lab_image = $this->model_tool_image->resize($result['te_lab_image'], 1500, 980);
				} else {
					$te_lab_image = '';
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$price11 = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
					$disc_per = $price11 * 35/100;
					$price11 = $price11 - $disc_per;
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}
				
				if($discount_per > 0) {
				    $special = $this->currency->format($this->tax->calculate(($result['price'] - ($result['price'] * $discount_per / 100)), $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				$options = NULL;
				if($result['product_type'] != 5) {
				    $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				} else {
					$price = false;
				}
				
				$percent = 0;
				if($result['special'] > 0) {
				    $percent = round(100 - (($result['special'] / $result['price'])) * 100 ,0 ) > 0 ? sprintf($this->language->get('%s'), (round(100 - (($result['special'] / $result['price'])) * 100 ,0 ))) . '% OFF' : '';
				}
				
				if(array_key_exists($result['product_id'],$deal_data) && $result['quantity'] > 0) {
		            $coupon = $deal_data[$result['product_id']];
		        } else {
		            $coupon = '';
		        }
                
                if($coupon){
    		        $coupon_query = $this->db->query("SELECT type,discount FROM " . DB_PREFIX . "coupon WHERE code = '" . $coupon . "'");
    		        $type     = $coupon_query->row['type'];
    		        $discount = $coupon_query->row['discount'];
    		        
    		        $product_price = $result['special'] ? ($result['special']) : ($result['price']);
    		        
    		        if ($type == 'P') {
                        $current_discount = (float) ($product_price * ((float) $discount / 100));
                    } else {
                        $current_discount = (float) $discount;
                    }
                    
                    $coupon_discount = $this->currency->format(((float)$product_price - $current_discount),$currency_code);
                    
                    $coupon_discount = $this->currency->format($this->tax->calculate(($product_price - $current_discount), $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
		        } else {
		            $coupon_discount = '';
		        } 
		        
		        $this->load->model('catalog/review');
		        
		        $results = $this->model_catalog_review->getTopReviewByProductId($result['product_id'], ($page - 1) * 5, 5);

        		foreach ($results as $result2) {
        			$data['reviews'][] = array(
        				'author'     => $result2['author'],
        				'text'       => nl2br($result2['text']),
        				'product_id'       => ($result2['product_id']),
        				'rating'     => (int)$result2['rating'],
        				'date_added' => date($this->language->get('date_format_short'), strtotime($result2['date_added']))
        			);
        		}
        		
		        if($result['product_id'] > 0) {
			    	$data['products'][] = array(
			    	    //'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
			    	    'percent'     => $percent ? $percent : '',
			    	    'price11'     => $price11,
			    		'product_id'  => $result['product_id'],
			    		'product_type' => $result['product_type'],
			    		'jan'           => $result['jan'],
			    		'thumb'       => $image,
			    		'te_lab_product_image' => $te_lab_product_image,
			    		'te_lab_image'=> $te_lab_image, 
			    		'short_description'  => html_entity_decode($result['short'], ENT_QUOTES, 'UTF-8'),
 		    			'name'        => html_entity_decode(substr($result['name'],0,55) . ''),
 			    		'name_mobile' => html_entity_decode(substr($result['name'],0,38) . ''),
 					    'stock_status' => $result['quantity'] > 0 ? 'In' : 'Out',
 					    'sort_order' => $result['sort_order'],
 					    'coupon'      => $coupon,
 					    'discount'      => $discount + 2,
 					    'coupon_discount'        => $coupon_discount,
					    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					    'options'     => $options,
					    'reviews'     => $data['reviews'],
					    'quantity'    => $result['quantity'],
					    'price'       => substr($price,3) > 0 ? $price : "",
					    'price1' => $this->load->controller('extension/module/joseanmatias_preco_cupom/listview', $result),
					    'special'     => $special,
 					    'tax'         => $tax,
					    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					    'rating'      => $result['rating'],
					    'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
		        }
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
			if ($page == 1) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], true), 'canonical');
			} elseif ($page == 2) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], true), 'prev');
			} else {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page - 1), true), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
			    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page + 1), true), 'next');
			}

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
 
			if($category_id == '622') {
			    $this->response->setOutput($this->load->view('product/te_labs', $data));
			} else if($category_info['is_custom'] == '1') {
			    $this->response->setOutput($this->load->view('product/custom_category', $data));
			} else {
			    $this->response->setOutput($this->load->view('product/category', $data));
			}
			
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
	
	public function applytelabs() {
	    $this->load->model('account/customer');
	    
	    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['customer_id'])) {
			$data['error_customer_id'] = $this->error['customer_id'];
		} else {
			$data['error_customer_id'] = '';
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_account_customer->applytelabs($this->request->post);

			$this->session->data['success'] = "You have successfully applied for TE Lab Program";

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
				);

				$this->model_account_activity->addActivity('te_labs_apply', $activity_data);
			}

			$this->response->redirect($this->url->link('product/category&path=622', '', true));
		}
	}
}