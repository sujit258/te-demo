<?php
class ControllerApiOrderinfo extends Controller {

	public function index() {
		$this->load->language('api/order');
 
        $json['orders'] = array();
         
        
		if (!$this->config->get('teapi_status') || !isset($this->request->get['api_key']) || $this->request->get['api_key'] != $this->config->get('teapi_api_key') || !isset($this->request->get['api_password']) || $this->request->get['api_password'] != $this->config->get('teapi_api_password')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
		    
			$this->load->model('checkout/order');
			
			$this->load->model('account/order'); 
			
			$json['orders'] = array();

    		$filter_data = array(
    			'filter_order_status'  => 23 
    		);
 
		    $results = $this->model_checkout_order->getOrders($filter_data);
            
            $order_products = array();
            
            foreach ($results as $result) {
		     
              $products = $this->model_checkout_order->getOrderProducts($result['order_id']);
                foreach ($products as $product) {
                     
                    $order_products[] = array(
                        'product_id' => $product['product_id'],
    					'name'       => $product['name'],
    					'model'      => $product['model'],
    					'option'     => $this->model_checkout_order->getOrderOptions($result['order_id'],$product['order_product_id']),
    					'quantity'   => $product['quantity'],
    					'price'      => $product['price'],
    					'total'      => $product['total'],
    					'reward'     => $product['reward']
                    );    
                    
                      
                }  
              $order_totals = $this->model_checkout_order->getOrderTotals ($result['order_id']);
              
			$json['orders'][] = array(
				'order_id'          => $result['order_id'],
				'telephone'         => $result['telephone'],
				'customer'          => $result['customer'],
			    'customer_id'       => $result['customer_id'],
                'order_products'    => $order_products, 
                'order_totals'      => $order_totals,
				'order_status'      => $result['order_status'] ? $result['order_status'] : $this->language->get('text_missing'),
				'order_status_id'   => $result['order_status_id'],
				'total'             => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
				'date_added'        =>  $result['date_added'],
				'date_modified'     => $result['date_modified'],
				'date_shipped'  => $result['date_shipped']
			);
		}
		
			/*$json['products'] = $this->model_account_order->getOrderProducts($order_id);

			if ($json['products']) {
				foreach ($json['products'] as $key => $product) {
						$json['products'][$key]['url'] = html_entity_decode($this->url->link('product/product', 'product_id=' . $product['product_id'], true));
				}
			}*/  
			 

			if (!$json['orders']) {
				$json['error'] = $this->language->get('error_not_found');
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function info() {
		$this->load->language('api/order');
 
        $json = array();
        
		if (!$this->config->get('teapi_status') || !isset($this->request->get['api_key']) || $this->request->get['api_key'] != $this->config->get('teapi_api_key') || !isset($this->request->get['api_password']) || $this->request->get['api_password'] != $this->config->get('teapi_api_password')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
		    
			$this->load->model('checkout/order');
			 
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}
			 
 
		    $order_info = $this->model_checkout_order->getOrder($order_id);
	            
    	   if ($order_info) {
    	       
    			$this->load->language('sale/order');
    
    			$json['order_id'] = $order_id;
    
    			$json['store_id'] = $order_info['store_id'];
    			$json['store_name'] = $order_info['store_name'];
    			 
    			
    			if ($order_info['store_id'] == 0) {
    				$json['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;
    			} else {
    				$json['store_url'] = $order_info['store_url'];
    			}
    
    			if ($order_info['invoice_no']) {
    				$json['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
    			} else {
    				$json['invoice_no'] = '';
    			}
                
                $json['ip'] = $order_info['ip'];
                $json['forwarded_ip'] = $order_info['forwarded_ip'];
                $json['user_agent'] = $order_info['user_agent'];
                $json['accept_language'] = $order_info['accept_language'];
               
                $json['date_added'] = $order_info['date_added'];
                $json['date_modified'] = $order_info['date_modified']; 
                $json['date_shipped'] = $order_info['date_shipped'];
    
    			$json['firstname'] = $order_info['firstname'];
    			$json['lastname'] = $order_info['lastname'];
     			$json['email'] = $order_info['email'];
    			$json['telephone'] = $order_info['telephone'];
    
    			$json['shipping_method'] = $order_info['shipping_method'];
    			$json['payment_method'] = $order_info['payment_method'];
    			
    			$json['payment_firstname'] = $order_info['payment_firstname'];
                $json['payment_lastname'] = $order_info['payment_lastname'];
                $json['payment_company'] = $order_info['payment_company'];
                $json['payment_company_id'] = $order_info['payment_company_id'];
                $json['payment_tax_id'] = $order_info['payment_tax_id'];
                $json['payment_address_1'] = $order_info['payment_address_1'];
                $json['payment_address_2'] = $order_info['payment_address_2'];
                $json['payment_city'] = $order_info['payment_city'];
                $json['payment_postcode'] = $order_info['payment_postcode'];
                $json['payment_zone'] = $order_info['payment_zone'];
                $json['payment_zone_code'] = $order_info['payment_zone_code'];
                $json['payment_country'] = $order_info['payment_country'];      
                $json['shipping_firstname'] = $order_info['shipping_firstname'];
                $json['shipping_lastname'] = $order_info['shipping_lastname'];
                $json['shipping_company'] = $order_info['shipping_company'];
                $json['shipping_address_1'] = $order_info['shipping_address_1'];
                $json['shipping_address_2'] = $order_info['shipping_address_2'];
                $json['shipping_city'] = $order_info['shipping_city'];
                $json['shipping_postcode'] = $order_info['shipping_postcode'];
                $json['shipping_zone'] = $order_info['shipping_zone'];
                $json['shipping_zone_code'] = $order_info['shipping_zone_code'];
                $json['shipping_country'] = $order_info['shipping_country'];
                
    			// Payment Address
    			if ($order_info['payment_address_format']) {
    				$format = $order_info['payment_address_format'];
    			} else {
    				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
    			}
    
    			$find = array(
    				'{firstname}',
    				'{lastname}',
    				'{company}',
    				'{address_1}',
    				'{address_2}',
    				'{city}',
    				'{postcode}',
    				'{zone}',
    				'{zone_code}',
    				'{country}'
    			);
    
    			$replace = array(
    				'firstname' => $order_info['payment_firstname'],
    				'lastname'  => $order_info['payment_lastname'],
    				'company'   => $order_info['payment_company'],
    				'address_1' => $order_info['payment_address_1'],
    				'address_2' => $order_info['payment_address_2'],
    				'city'      => $order_info['payment_city'],
    				'postcode'  => $order_info['payment_postcode'],
    				'zone'      => $order_info['payment_zone'],
    				'zone_code' => $order_info['payment_zone_code'],
    				'country'   => $order_info['payment_country']
    			);
    
    			$json['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
    
    			// Shipping Address
    			if ($order_info['shipping_address_format']) {
    				$format = $order_info['shipping_address_format'];
    			} else {
    				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
    			}
    
    			$find = array(
    				'{firstname}',
    				'{lastname}',
    				'{company}',
    				'{address_1}',
    				'{address_2}',
    				'{city}',
    				'{postcode}',
    				'{zone}',
    				'{zone_code}',
    				'{country}'
    			);
    
    			$replace = array(
    				'firstname' => $order_info['shipping_firstname'],
    				'lastname'  => $order_info['shipping_lastname'],
    				'company'   => $order_info['shipping_company'],
    				'address_1' => $order_info['shipping_address_1'],
    				'address_2' => $order_info['shipping_address_2'],
    				'city'      => $order_info['shipping_city'],
    				'postcode'  => $order_info['shipping_postcode'],
    				'zone'      => $order_info['shipping_zone'],
    				'zone_code' => $order_info['shipping_zone_code'],
    				'country'   => $order_info['shipping_country']
    			);
    
    			$json['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
            
    			// Uploaded files
    			$this->load->model('tool/upload');
    
    			$json['products'] = array();
    
    			$products = $this->model_checkout_order->getOrderProducts($order_id);
                
                $product_option_weight = 0;
    			foreach ($products as $product) {
    				$data['option_data'] = array();
    
    				$options = $this->model_checkout_order->getOrderOptions($order_id, $product['order_product_id']);
    
    				foreach ($options as $option) {
    				    
    				     $option_weight = substr($option['value'], 0, -1);
    				    
    				    $product_option_weight += $option_weight * $product['quantity'];
    				    
    					if ($option['type'] != 'file') {
    						$data['option_data'][] = array(
    							'name'  => $option['name'],
    							'hw_pid'  => $option['hw_pid'],
    							'value' => $option['value'],
    							'type'  => $option['type']
    						);
    					} else {
    						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
    
    						if ($upload_info) {
    							$data['option_data'][] = array(
    								'name'  => $option['name'],
    								'hw_pid'  => $option['hw_pid'],
    								'value' => $upload_info['name'],
    								'type'  => $option['type'],
    								'href'  => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], true)
    							);
    						}
    					}
    				}
    
    				$json['products'][] = array(
    					'order_product_id' => $product['order_product_id'],
    					'product_id'       => $product['product_id'],
    					'name'    	 	   => $product['name'],
    					'model'    		   => $product['model'],
    					'stock'		       => $product['stock'],
    					'quantity'		   => $product['quantity'],
    					'price'    		   => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
    					'sub-total'        => $this->currency->format($product['quantity']*$product['price'], $order_info['currency_code'], $order_info['currency_value']),
    					'total'    		   => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
    					'option'   		   => $data['option_data']
    				);
    			}
                
                
    			$json['vouchers'] = array();
    
    			$vouchers = $this->model_checkout_order->getOrderVouchers($order_id);
    
    			foreach ($vouchers as $voucher) {
    				$json['vouchers'][] = array(
    					'description' => $voucher['description'],
    					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
    					'href'        => $this->url->link('sale/voucher/edit', 'token=' . $this->session->data['token'] . '&voucher_id=' . $voucher['voucher_id'], true)
    				);
    			}
                
                
    			$json['totals'] = array();
    
    			$totals = $this->model_checkout_order->getOrderTotals($order_id);
    
    			foreach ($totals as $total) {
    				$json['totals'][] = array(
    					'title' => $total['title'],
    					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
    				);
    			}
    
    			$json['comment'] = nl2br($order_info['comment']);
    
    
    			// The URL we send API requests to
    			$json['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;
    			
    			
    		}  
		
	            
	        if (!$json) {
				$json['error'] = $this->language->get('error_not_found');
			}
		}
		

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
