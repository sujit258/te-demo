<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		
		$data['whatsapp_number'] = $this->config->get('config_whatsappid');
		$data['store_telephone'] = $this->config->get('config_telephone');
		$data['store_email'] = $this->config->get('config_email');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

 // BOF - Betaout Opencart mod
        $this->load->model('tool/betaout');
        $data['betaout_footer_text'] = $this->model_tool_betaout->getFooterText();
        // EOF - Betaout Opencart mod

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		return $this->load->view('common/footer', $data);
	}
	
	public function pinCheck() {
					$this->load->model('catalog/pincode');
					$pin = array();
					if(isset($_POST['pincode'])){
						$_SESSION['pin_check_status'] = "1";
						$pincode = addslashes($_POST['pincode']);
						$_SESSION['pincode'] = "<b>".addslashes($_POST['pincode'])."</b>";
						$pin = $this->model_catalog_pincode->getPin($pincode);
						$message_cod = $message_pre = $message_not = '';
						$message_cod = html_entity_decode($this->config->get('pincodesetting_cod_msg'), ENT_QUOTES, 'UTF-8');
						$message_pre = html_entity_decode($this->config->get('pincodesetting_prepaid_msg'), ENT_QUOTES, 'UTF-8'); 
						$message_not = html_entity_decode($this->config->get('pincodesetting_no_service_msg'), ENT_QUOTES, 'UTF-8'); 
						$text_color = $this->config->get('pincodesetting_text_color'); 
						
						if(!isset($message_cod) || $message_cod == ''){
							$message_cod  = "COD and Prepaid Service is Available At Your Location";
						}
						if(!isset($message_pre) || $message_pre == ''){
							$message_pre  = "Only Prepaid Service is Available At Your Location";
						}
						if(!isset($message_not) || $message_not == ''){
							$message_not  = "Service is not Available at your location yet";
						}
			
				if(isset($pin['id'])){
				$service = $pin['service_available'];
				$delivery_days = (int)filter_var($pin['delivery_time'], FILTER_SANITIZE_NUMBER_INT);
				$check_time = date("H");
				$day = "today";
				if($check_time >= 12) {
					$delivery_days += 1;
					$day = "tomorrow";
				}
				if($service == '1'){                    
                    echo "<span class='msg11'><i class='fa fa-map-marker'></i> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform2()' value='CHANGE' id='footer-button-change' class='button'  /></form></span><br/>";
                    echo "<span class='msg12'><b><i class='fa fa-check-square'></i> Get this by " . date("d M",strtotime('+' . $delivery_days . ' DAYS')) . " if you order by 12 PM " . $day . "</b></span>";
                    echo "<p class='codcheck'><i class='fa fa-check-square'></i> <b>Status :</b> <font color = '32CD32'>".$message_cod."</font></p><br/>";                   
                        
                    $_SESSION['pin_check_delivery'] ="<span class='msg12'><b><i class='fa fa-check-square'></i> Get this by " . date("d M",strtotime('+' . $delivery_days . ' DAYS')) . " if you order by 12 PM " . $day . "</b></span>";
                    $_SESSION['pin_check_result'] = "<p class='codcheck'><i class='fa fa-check-square'></i> <b>Status : </b> <font color = '32CD32'>".$message_cod."</font></p><br/>";                   
                }
                else if($service == '0'){
                    echo "<span class='msg11'><i class='fa fa-map-marker'></i> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform2()' value='CHANGE' id='footer-button-change' class='button'/></form></span><br/>";
                    echo "<span  class='msg12'><b><i class='fa fa-check-square'></i> Get this by " . date("d M",strtotime('+' . $delivery_days . ' DAYS')) . " if you order by 12 PM " . $day . "</b></span>";
                    echo "<p><i class='fa fa-check-square'></i> <b>Status :</b> <font color = '5EA4FF'>".$message_pre."</font></p><br/>";                   
                    
                    $_SESSION['pin_check_delivery'] = "<span  class='msg12'><b><i class='fa fa-check-square'></i> Get this by " . date("d M",strtotime('+' . $delivery_days . ' DAYS')) . " if you order by 12 PM " . $day . "</b></span>";
                    $_SESSION['pin_check_result'] = "<p><i class='fa fa-check-square'></i> <b>Status : </b> <font color = '5EA4FF'>".$message_pre."</font></p><br/>";                   
                }
                else{
                    echo "<span class='msg11'><i class='fa fa-map-marker'></i> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform2()' value='CHANGE' id='footer-button-change' class='button' /></form></span><br/>";
                    echo "<p><i class='fa fa-times'></i> <b>Status :</b></b><font color = 'FF1605'>".$message_not."</font></p><br/>";
                        
                    $_SESSION['pin_check_result'] = "<p><i class='fa fa-times'></i> <b>Status :</b></b> <font color = 'FF1605'>".$message_not."</font></p><br/>";
                    $_SESSION['pin_check_delivery'] = '';
                }
            }
            else{
                echo "<span class='msg11'><i class='fa fa-map-marker'></i> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform2()' value='CHANGE' id='footer-button-change' class='button'/></form></span><br/>";
                echo "<p><i class='fa fa-times'></i> <b>Status :</b> <font color = '".$text_color."'>".$message_not."</font></p><br/>";
                
                $_SESSION['pin_check_result'] = "<p><i class='fa fa-times'></i> <b>Status :</b></b><font color = '".$text_color."'>".$message_not."</font></p><br/>";
                $_SESSION['pin_check_delivery'] = '';
            }
                    }
    }
}
