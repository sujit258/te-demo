<?php
class ControllerCronBusinessReport extends Controller {
	public function index() {
	    $data = array();
	    $this->load->model('cron/business_report');
	    $current_date = date('Y-m-d', strtotime("-1 days"));
	    $date_start = date('Y-m-01', strtotime($current_date));
	    
	    $report = $this->model_cron_business_report->getDailyReport($current_date, $date_start);
	    
	    $custom_products_report = $this->model_cron_business_report->getCustomProductsData($current_date, $date_start);
	    
	    $payment_zone_report = $this->model_cron_business_report->getOrderCountByPaymentCode($current_date, $date_start);
	    
	    $order_with_comments_report = $this->model_cron_business_report->getOrderAndComment();
	    
	    $te_labs_data = $this->model_cron_business_report->getTeLabsData($current_date, $date_start);
	    
	    if(!empty($report)) {
	        $data['report'] = $report;
	        if(!empty($custom_products_report)) {
	            $data['custom_products_report'] = $custom_products_report;
	        }
	        if(!empty($payment_zone_report)) {
	            $data['payment_zone_report'] = $payment_zone_report;
	        }
	        if(!empty($order_with_comments_report)) {
	            $data['order_with_comments_report'] = $order_with_comments_report;
	        }
	        if(!empty($te_labs_data)) {
	            $data['te_labs_data'] = $te_labs_data;
	        }
	        
	        if(date('d') == 1) {
                $subject = 'TE Final Business Report ' . date('F Y',strtotime("-1 days"));
            } else {
	            $subject = 'TE Business Report ' . date("M d", strtotime($current_date));
            }
            if(isset($this->request->get['cron_call']) && $this->request->get['cron_call'] == 1) {
    		    $mail = new Mail();
			    $mail->protocol = $this->config->get('config_mail_protocol');
		        $mail->parameter = $this->config->get('config_mail_parameter');
			    $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			    $mail->smtp_username = $this->config->get('config_mail_smtp_username');
		        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
		        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
		        $mail->setFrom("sales@true-elements.com");
		        //$mail->setTo("shubham@true-elements.com");
		        if($this->request->get['sendMail'] == 5) {
		            $mail->setTo("puru@true-elements.com" . "," . "sreejith@true-elements.com" . "," . "shahnawaz@true-elements.com" . "," . "dhivni@true-elements.com" . "," . "nayana@true-elements.com" . "," . "shubham@true-elements.com" . "," . "sameer@true-elements.com" . "," . "raashi@true-elements.com" . "," . "sukumar@true-elements.com" . "," . "prachi@true-elements.com" . "," . "sujit@true-elements.com" . "," . "prajkta@true-elements.com" . "," . "rutuja@true-elements.com" . "," . "saee@true-elements.com" . "," . "sonakshi@true-elements.com" . "," . "riddhi@true-elements.com" . "," . "janhavi@true-elements.com");
		        } else {
		            $mail->setTo("tereport@true-elements.com");
		        }
		        $mail->setSender("True Elements");
		        $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		        $mail->setHtml($this->load->view('cron/business_report', $data));
		        $mail->send();
            }
            $this->response->setOutput($this->load->view('cron/business_report', $data));
	    }
	}
}
?>