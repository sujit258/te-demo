<?php
class ControllerCronDelayMailer extends Controller {
    public function index() {
        $order_query = $this->db->query("SELECT DISTINCT(email) FROM " . DB_PREFIX . "order WHERE (order_status_id = 3 OR order_status_id = 5) AND date_shipped BETWEEN '2020-07-13' AND '2020-07-20'");
	    
        if($order_query->num_rows) {
            foreach($order_query->rows AS $customer) {
                if(filter_var($customer['email'], FILTER_VALIDATE_EMAIL)) {
                    $mailer_data[] = array(
	                    'email'     => $customer['email']
	                );
	            }
	        }
	        print_r($mailer_data);
	        $subject = "Important Update - Temporary Delay in Shipping your order(s)";
	        
	        $html = '<p>Thank you so much for your recent purchase at True Elements.</p>';
	        
	        $html .= '<p>Due to COVID-19, Pune is under strict lockdown which is why courier partners are unable to pick your order. Although we have received your order, there will be a delay in delivering it.</p>';
	        
	        $html .= '<p>It would be great to get your support during this critical hour. We understand that it is causing inconvenience but your patience will really help us - we can assure you that we will do whatever it takes to quickly ship your order to your doorstep at the earliest, with utmost safety and precautions taken.</p>';
	        
	        $html .= '<p>In case of any queries, please feel free to reply to this email or call us at <b><a href="tel:8767120120">8767120120</a></b> or whatsapp us at <b><a href="https://api.whatsapp.com/send?phone=91 9321532959&text=Hi">9321532959</a></b></p>.';
	        
	        //$html .= '<p>In case of any queries, please feel free to reply to this email or call us at <b><a href="tel:8767120120">8767120120</a></b></p>.';
	        
	        if(!empty($mailer_data)) {
                $mail = new Mail();
		        $mail->protocol = $this->config->get('config_mail_protocol');
		        $mail->parameter = $this->config->get('config_mail_parameter');
		        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
		        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
		        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
    	    	$mail->setFrom("sales@true-elements.com");
		        $mail->setSender("True Elements");
		        $mail->setSubject($subject);
		        $i = 0;
	            foreach($mailer_data AS $mailer) {
		            $mail->setHtml($html);
		            if($this->request->get['cron_call'] == 1) {
		                if($i%50 == 0) {
		                    sleep(2);
		                }
		                $mail->setTo($mailer['email']);
		                $mail->send();
		                $i++;
		            }
	            }
	        }
	    }
	}
}
?>