<?php
class ControllerCronRebuyReminder extends Controller {
	public function index() {
	    $date_shipped = date('Y-m-d', strtotime("-30 DAYS"));
	    
	    $customer_order_query = $this->db->query("SELECT firstname, email, order_id FROM " . DB_PREFIX . "order WHERE (order_status_id = 3 OR order_status_id = 5) AND DATE(date_shipped) LIKE '" . $this->db->escape($date_shipped) . "'");
	    
	    if($customer_order_query->num_rows) {
	        foreach($customer_order_query->rows AS $customer_order) {
	            $order_product_query = $this->db->query("SELECT op.product_id, op.name FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id) WHERE p.status = 1 AND op.order_id = '" . (int)$customer_order['order_id'] . "'");
	            
	            if($order_product_query->num_rows) {
	                foreach($order_product_query->rows AS $order_product) {
	                    $order_products[] = array(
	                        'product_id'    => $order_product['product_id'],
	                        'name'          => $order_product['name']
	                    );
	                }
	                $mailer_data[] = array(
	                    'firstname' => $customer_order['firstname'],
	                    'email'     => $customer_order['email'],
	                    'products'  => $order_products
	                );
	                $order_products = NULL;
	            }
	        }
	        
	        if(!empty($mailer_data)) {
	            $mail = new Mail();
		        $mail->protocol = $this->config->get('config_mail_protocol');
		        $mail->parameter = $this->config->get('config_mail_parameter');
		        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
		        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
		        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
    	    	$mail->setFrom("sales@true-elements.com");
		        $mail->setSender("True Elements");
		        
	            foreach($mailer_data AS $mailer) {
	                $html = 'Hello ' . $mailer['firstname'] . ',<br/><br/>';
	                if(sizeof($mailer['products']) == 1) { 
	                    $subject = "Hey there! It's time you stock up on your favourite " . $mailer['products'][0]['name'] . " again!";
	                    
	                    $product_link = $this->url->link('product/product', 'product_id=' . $mailer['products'][0]['product_id']);
	                    $html .= "It was a pleasure in delivering to you your favourite True Elements <b><a href='" . $product_link . "'>" . $mailer['products'][0]['name'] . "</a></b>. We hope you liked the product as much as we liked serving you.<br/><br/>Before you run out of stock and feel empty, we are here to help you with your repeat purchase, so that you can stick to your favourites, all time :)";
	                } else {
	                    $subject = "Hey there! It's time you stock up on your favourite products again!";
	                    
	                    $html .= "It was a pleasure in delivering to you your favourites True Elements ";
	                    $i = 0;
	                    foreach($mailer['products'] AS $product) {
	                        $product_link = $this->url->link('product/product', 'product_id=' . $product['product_id']);
	                        if($i > 0) {
	                            $html .= ", <b><a href='" . $product_link . "'>" . $product['name'] . "</a></b>";
	                        } else {
	                            $html .= "<b><a href='" . $product_link . "'>" . $product['name'] . "</a></b>";
	                        }
	                        $i++;
	                    }
	                    
	                    $html .= ". We hope you liked the product as much as we liked serving you.<br/><br/>Before you run out of stock and feel empty, we are here to help you with your repeat purchase, so that you can stick to your favourites, all time :)";
	                }
	                if(!empty($mailer['email'])) {
	                    //$mail->setTo($mailer['email']);
	                    //$mail->setTo("shubham@true-elements.com");
		                $mail->setSubject($subject);
		                $mail->setHtml($html);
		                if($this->request->get['cron_call'] == 1) {
		                    $mail->send();
		                }
	                }
	                
	                echo "<b>Subject: </b>",$subject,"<br/>";
	                print_r($html);
	                echo "<br/>--------------------------------------------------<br/><br/>";
	            }
	        }
	    }
	}
}
?>