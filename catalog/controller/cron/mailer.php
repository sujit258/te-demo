<?php
class ControllerCronMailer extends Controller {
    public function index() {
        //$customer_query = $this->db->query("SELECT email FROM " . DB_PREFIX . "customer WHERE status = 1 AND customer_id BETWEEN 59001 AND 59700");
	    
        if($customer_query->num_rows) {
            foreach($customer_query->rows AS $customer) {
                if(filter_var($customer['email'], FILTER_VALIDATE_EMAIL)) {
                    $mailer_data[] = array(
	                    'email'     => $customer['email']
	                );
	            }
	        }
	        
	        $subject = "A bowl of this Muesli and you'll be free from Indigestion Problems";
	        
	        if(!empty($mailer_data)) {
                $mail = new Mail();
		        $mail->protocol = $this->config->get('config_mail_protocol');
		        $mail->parameter = $this->config->get('config_mail_parameter');
		        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
		        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
		        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
    	    	$mail->setFrom("sales@true-elements.com");
		        $mail->setSender("True Elements");
		        $mail->setSubject($subject);
		        $i = 0;
	            foreach($mailer_data AS $mailer) {
		            $mail->setHtml($this->load->view('cron/mailer'));
		            if($i%50 == 0) {
		                sleep(2);
		            }
		            if($this->request->get['cron_call'] == 1) {
		                $mail->setTo($mailer['email']);
		                $mail->send();
		                $i++;
		            }
	            }
	        }
	    }
	}
}
?>