<?php
class ControllerCronTruecashReminder extends Controller {
	public function index() {
	    $customer_query = $this->db->query("SELECT customer_id, firstname, email FROM " . DB_PREFIX . "customer WHERE status = 1 AND customer_group_id = 1 ORDER BY customer_id ASC");
	    
	    if($customer_query->num_rows) {
	        $i = 0;
	        foreach($customer_query->rows AS $customer) {
	            $truecash_query = $this->db->query("SELECT SUM(points) AS truecash FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . $customer['customer_id'] . "' AND NOW() <= DATE_ADD((DATE_ADD(date_added, INTERVAL 3 MONTH)), INTERVAL 1 DAY) GROUP BY customer_id");
	            
	            if($truecash_query->row['truecash'] >= 110) {
                    $truecash_data[] = array(
                        'firstname'     => $customer['firstname'],
                        'truecash'      => $truecash_query->row['truecash'],
                        'email'         => $customer['email']
                    );
	                $i++;
	            }
	        }
	    }
	    /*$html = '<table>';
	    foreach($truecash_data AS $tc) {
	        $html .= '<tr><td>' . $tc['email'] . $tc['truecash'] . '</td></tr>';
	    }
	    $html .= '</table>';
	    
	    print_r($html);*/
	    print_r(count($truecash_data));
	    return false;
	    $this->load->model('tool/image');
	    
	    //$new_launches_query = $this->db->query("SELECT p.product_id, pd.name, p.image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) WHERE p.status = 1 AND p.quantity > 0 AND p.product_type = 1 ORDER BY p.product_id DESC LIMIT 3");
	    
	    $new_launches_query = $this->db->query("SELECT p.product_id, pd.name, p.image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) WHERE p.status = 1 AND p.quantity > 0 AND p.product_type = 1 AND p.product_id = 6465");
	    
	    foreach($new_launches_query->rows AS $new) {
	        $url_alias_query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query LIKE 'product_id=" . $new['product_id'] . "' LIMIT 1");
	        
	        $new_launches[] = array(
                'name'      => strlen($new['name']) > 50 ? substr($new['name'], 0, 50) . '...' : $new['name'],
                'image'     => $this->model_tool_image->resize($new['image'], 400, 504),
                'href'      => "https://www.true-elements.com/true-elements-chocolate-granola?utm_source=mail&utm_medium=true-elements-tc-mail"//HTTPS_SERVER . $url_alias_query->row['keyword']
	        );
	    }
	    
	    $products_html = '<div>';
        foreach($new_launches AS $product) {
            $products_html .= '<div style="position: relative; display: inline-block; width: 100%;">';
            $products_html .= '<a href="https://www.true-elements.com/true-elements-chocolate-granola?utm_source=mail&utm_medium=true-elements-tc-mail"><img src="' . $product['image'] . '" style="width: 100%;"></a>';
            $products_html .= '<a href="https://www.true-elements.com/true-elements-chocolate-granola?utm_source=mail&utm_medium=true-elements-tc-mail"><h5 style="vertical-align: top; display: inline-block; min-height: 50px; font-size: 14px!important;">Indulge into the richness of Dark Chocolate Granola at FLAT 15% OFF</h5></a>';
            $products_html .= '</div>';
        }
        $products_html .= '</div>';
	    
	    $j = 0;
	    foreach($truecash_data AS $truecash) {
	        $subject = 'You have unused ' . $truecash['truecash'] . '/- Cash lying in your account';
	        
	        $html = '<html><head><title>Truecash Reminder</title><style> h4 ~ div div { display: inline-block; max-width: 27%;} @media(max-width: 768px) { h4 ~ div div { display: table!important; margin: auto; max-width: 80%!important; } } </style></head><body>';
	        $html .= 'Hello ' . $truecash['firstname'] . ',<br/><br/>';
	        $html .= 'Thank you for being a part of this story called True Elements and thank you for letting TRUE Elements be part of your story! It means a lot to us. Just to make it more TRUE-ly amazing journey for you in future, we have your True Cash rewards to cover up your spends and help you in saving BIG!<br/><br/>';
	        $html .= '<b>True Cash</b> is basically the reward points which you earn on every purchase which can then further be used to redeem on your next purchase.<br/><br/>';
	        $html .= 'Your current True Cash balance is <b>' . $truecash['truecash'] . '</b>, which is equal to <b>' . $truecash['truecash'] . '</b>/- in your account.<br/><br/>';
	        $html .= 'Redeem this True Cash while making the payment and earn another True Cash rewards on your very next purchase.<br/><br/>';
	        $html .="P.S.- You can earn True Cash on any of your orders with <b>NO PURCHASE LIMIT</b>.<br/><br/>Don't wait for long. As old True Cash will start expiring<br/><br/>";
	        $html .= '<a href="https://bit.ly/3gA7chP"><button class="btn" style="color: #fff; background: #f49a25; padding: 7px; border-radius: 5px; cursor: pointer!important; border: 1px solid #f49a25; font-weight: 500; letter-spacing: 0.6px;">Shop Now!</button></a>';
	        $html .= '<br/><h4 style="font-size: 15px!important;">Check out our New Launch:</h4>';
            $html .= $products_html;
            $html .= '</body></html>';
            
	        $mail = new Mail();
		    $mail->protocol = $this->config->get('config_mail_protocol');
		    $mail->parameter = $this->config->get('config_mail_parameter');
		    $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		    $mail->smtp_username = $this->config->get('config_mail_smtp_username');
		    $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		    $mail->smtp_port = $this->config->get('config_mail_smtp_port');
		    $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		    $mail->setTo($truecash['email']);
		    //$mail->setTo("shubham@true-elements.com");
	    	$mail->setFrom("sales@true-elements.com");
		    $mail->setSender("True Elements");
		    $mail->setSubject($subject);
		    $mail->setHtml($html);
		    if($this->request->get['cron_call'] == 1) {
		        if($j%50 == 0) {
	                sleep(2);
	            }
		        $mail->send();
		        $j++;
		    } else {
		        print_r($html);
		    }
	    }
	    /*foreach($truecash_data AS $truecash) {
	        echo $truecash['firstname']," => ",$truecash['truecash']," Email: ",$truecash['email'],"<br/>";
	    }*/
	}
}
?>