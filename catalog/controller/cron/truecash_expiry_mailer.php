<?php
class ControllerCronTruecashExpiryMailer extends Controller {
    public function index() {
        $customer_expiring_tc_query = $this->db->query("SELECT DISTINCT cr.customer_id, c.firstname, c.email, SUM(cr.points) AS expiring FROM " . DB_PREFIX . "customer_reward cr LEFT JOIN " . DB_PREFIX . "customer c USING(customer_id) WHERE cr.points > 0 AND DATE(NOW()) = DATE(DATE_SUB((DATE_ADD(cr.date_added, INTERVAL 3 MONTH)), INTERVAL 9 DAY)) GROUP BY cr.customer_id");
        if($customer_expiring_tc_query->num_rows) {
            foreach($customer_expiring_tc_query->rows AS $customer) {
                if(filter_var($customer['email'], FILTER_VALIDATE_EMAIL)) {
	                $customer_tc_balance_query = $this->db->query("SELECT SUM(points) AS balance FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer['customer_id'] . "' GROUP BY customer_id");
	                if($customer_tc_balance_query->row['balance'] > 0) {
	                    $data['mailer_data'][] = array(
	                        'email'     => $customer['email'],
	                        'expiring'  => $customer['expiring'],
	                        'balance'   => $customer_tc_balance_query->row['balance']
	                    );
	                    $mailer_data[] = array(
	                        'email'     => $customer['email']
	                    );
	                }
	            }
	        }
	        print_r($data['mailer_data']);
	        $subject = "True Cash expiring in 9 days. Hurry Up and Save Big!";
	        
	        if(!empty($mailer_data)) {
                $mail = new Mail();
		        $mail->protocol = $this->config->get('config_mail_protocol');
		        $mail->parameter = $this->config->get('config_mail_parameter');
		        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
		        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
		        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
    	    	$mail->setFrom("sales@true-elements.com");
		        $mail->setSender("True Elements");
		        $mail->setSubject($subject);
		        $i = 0;
	            foreach($mailer_data AS $mailer) {
		            $mail->setHtml($this->load->view('cron/truecash_expiry_mailer', $data));
		            if($i%50 == 0) {
		                sleep(2);
		            }
		            if($this->request->get['cron_call'] == 1) {
		                $mail->setTo("shubham@true-elements.com");
		                //$mail->send();
		                $i++;
		            }
	            }
	        }
	    }
    }
}
?>