<?php
class ControllerCheckoutSmartCart extends Controller {
	public function index() {
		
		
		if (!$this->customer->isLogged() && !$this->request->get['customer_id']) {
			$this->session->data['redirect'] = $this->url->link('checkout/smart_cart', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
		
		
		if (file_exists('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/opentheme/categorytabslider.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/opentheme/categorytabslider.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/opentheme/categorytabslider.css');
		}
		
		// Added to cartbookmark
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js');
		// Added to cart
		
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_new'] = $this->language->get('text_new');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->language('account/account');

		$this->document->setTitle('Smart basket');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Smart basket',
			'href' => $this->url->link('checkout/smart_cart', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 

		$data['heading_title'] = 'Smart basket';
		 
		$this->load->model('account/order');
		$this->load->model('catalog/product');
		$this->load->model('catalog/product');
		$this->load->model('account/wishlist');
        $this->load->model('account/myvoucher');
        $this->load->model('tool/image');
  
        $data['wishlist_count'] = $this->model_account_wishlist->getTotalWishlist();  
         
        
        $data['bestproducts'] = array();

		if (empty($setting['limit'])) {
			$setting['limit'] = 14;
		}

		$lang_code = $this->session->data['language'];

		if(isset($setting['title']) && $setting['title']) {
			$data['title'] = $setting['title'][$lang_code]['title'];
		} else {
			$data['title'] = $this->language->get('heading_title');
		}

		$data['new_products'] = array();

		$new_filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 10
		);

		$new_results = $this->model_catalog_product->getProducts($new_filter_data);

		if(isset($setting['rotator']) && $setting['rotator']) {
			$product_rotator_status = (int) $this->config->get('ocproductrotator_status');
		} else {
			$product_rotator_status = 0;
		}
		
		$deal_data = array();
		$deals_filter_data = array(
		    'limit' => 100    
	    );
	    $active_deals = $this->model_catalog_product->getDeals($deals_filter_data);
        if($active_deals) {
            foreach($active_deals AS $deal) {
                $deal_data[$deal['product_id']] = $deal['coupon'];
            }
        }

		$best_results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);

		if ($best_results) {
			foreach ($best_results as $best_result) {
				if ($best_result['image']) {
					$image = $this->model_tool_image->resize($best_result['image'], 320,403);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 320,403);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($best_result['price'], $best_result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}
				

				if ((float)$best_result['special']) {
					$special = $this->currency->format($this->tax->calculate($best_result['special'], $best_result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$best_result['special'] ? $best_result['special'] : $best_result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $best_result['rating'];
				} else {
					$rating = false;
				}

				if($product_rotator_status == 1) {
					$this->load->model('catalog/ocproductrotator');
					
					$product_id = $best_result['product_id'];
					$product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($product_id);

					if($product_rotator_image) {
						$rotator_image = $this->model_tool_image->resize($product_rotator_image, $setting['width'], $setting['height']);
					} else {
						$rotator_image = false;
					}
				} else {
					$rotator_image = false;
				}

				/* End */
				$is_new = false;
				if ($new_results) {
					foreach($new_results as $new_r) {
						if($best_result['product_id'] == $new_r['product_id']) {
							$is_new = true;
						}
					}
				}
				
					
    			//new
    			$personalized = 0;
    			$options = NULL;
    			if($best_result['product_id'] != 6438 && $best_result['product_id'] != 6345 && $best_result['product_id'] != 6449) {
    			    $options = $this->model_catalog_product->getProductOptions($best_result['product_id']);
    			} else {
    			    $personalized = 1;
    			}
    			
    			$name = html_entity_decode(substr($best_result['name'],0,55));
			    if(strlen($best_result['name']) > 55) {
			        $name .= "...";
			    }
			    
			    if(array_key_exists($best_result['product_id'],$deal_data) && $best_result['quantity'] > 0) {
		            $coupon = $deal_data[$best_result['product_id']];
		        } else {
		            $coupon = '';
		        }

				$data['bestproducts'][] = array(
					'product_id'  => $best_result['product_id'],
					'is_new' => $is_new,
					'thumb'       => $image,
					'options'     => $options,
					'rotator_image' => $rotator_image,
					'name'        => $name,
					'name_mobile' => html_entity_decode(substr($best_result['name'],0,35)),
					'coupon'      => $coupon,
					'quantity'        => $best_result['quantity'],
					'description' => utf8_substr(strip_tags(html_entity_decode($best_result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $best_result['product_id']),
				);
			}
		}
		
		$data['order_history_products'] = array();
		// Buy Again
		
		if($this->request->get['customer_id']){
		    $cusomer_id = $this->request->get['customer_id'];
		} else {
		    $cusomer_id = $this->customer->getId();
		}
		
		if($cusomer_id && $cusomer_id > 0) {
		        $this->load->model('account/order');
		        $order_products = $this->model_account_order->getShippedOrderProductsByCustomerId($cusomer_id);
		        
		        if($order_products) {
		            $deal_data = array();
		            $deals_filter_data = array(
		                'limit' => 100    
	                );
	                $active_deals = $this->model_catalog_product->getDeals($deals_filter_data);
                    if($active_deals) {
                        foreach($active_deals AS $deal) {
                            $deal_data[$deal['product_id']] = $deal['coupon'];
                        }
                    }
                    
		            foreach($order_products AS $order_product) {
		                $result = $this->model_catalog_product->getProduct($order_product['product_id']);
		                if ($result['image']) {
				        	$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				        } else {
				    	    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				        }

				        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				        	$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				        } else {
				        	$price = false;
				        }
				        
				        $base_option_special_price = $this->model_catalog_product->getFirstProductOptionValue($order_product['product_id']);
    
				        if ((float)$result['special']) {
				        	$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				        } else {
    				    	if($base_option_special_price != 0) {
        				        $special = $this->currency->format($this->tax->calculate($result['price'] + $base_option_special_price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        				    } else {
        					    $special = false;
        				    }
				        }
    
				        if ($this->config->get('config_tax')) {
				        	$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				        } else {
    				    	$tax = false;
				        }
    
				        if ($this->config->get('config_review_status')) {
    				    	$rating = (int)$result['rating'];
				        } else {
    				    	$rating = false;
				        }
    				
				        $personalized = 0;
    				    $options = NULL;
				        if($result['product_id'] != 6438 && $result['product_id'] != 6345 && $result['product_id'] != 6449) {
    				        $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				        } else {
    				        $personalized = 1;
				        }
    				
    				    if(array_key_exists($result['product_id'],$deal_data) && $result['quantity'] > 0) {
		                    $coupon = $deal_data[$result['product_id']];
		                } else {
		                    $coupon = '';
		                }
		                
        		        if($coupon){
            		        $coupon_query = $this->db->query("SELECT type,discount FROM " . DB_PREFIX . "coupon WHERE code = '" . $coupon . "' LIMIT 1");
            		        $type     = $coupon_query->row['type'];
            		        $discount = $coupon_query->row['discount'];
            		        
            		        if($base_option_special_price){
            		            $product_price = $base_special ? ($base_special) : ($result['price']);
            		        } else {
            		            $product_price = $result['special'] ? ($result['special']) : ($result['price']);
            		        }
            		        if ($type == 'P') {
                                $current_discount = (float) ($product_price * ((float) $discount / 100));
                            } else {
                                $current_discount = (float) $discount;
                            }
                            
                            $coupon_discount = $this->currency->format(((float)$product_price - $current_discount),$currency_code);
                            
                            $coupon_discount = $this->currency->format($this->tax->calculate(($product_price - $current_discount), $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        		        } else {
        		            $coupon_discount = '';
        		            $discount = 0;
        		        }
		                
		                $max_per_discount = $this->model_catalog_product->getProductMaxDiscount($result['product_id']);
        				if($max_per_discount > 2) {
        				    $max_per_discount = 'Upto <br/>' . $max_per_discount . '% Off';
        				} else if($max_per_discount == 2) {
        				    $max_per_discount = '2% Off';
        				} else {
        				    $max_per_discount = '';
				        }
				        
				        $data['order_history_products'][] = array(
				            'max_per_discount'       => $max_per_discount,
				            'discount'      => $discount > 0 ? round($discount) : '',
    					    'product_id'    => $result['product_id'],
				    	    'thumb'         => $image,
     					    'name'          => html_entity_decode(substr($result['name'],0,55)),
     					    'name_mobile' => html_entity_decode(substr($result['name'],0,35)),
     					    'coupon'      => $coupon,
				    	    'description'   => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
    					    'options'       => $options,
				    	    'personalized'  => $personalized,
    					    'quantity'      => $result['quantity'],
				    	    'price'         => substr($price,3) > 0 ? $price : "",
    					    'price1'        => $this->load->controller('extension/module/joseanmatias_preco_cupom/listview', $result),
				    	    'special'       => $special,
     					    'tax'           => $tax,
				    	    'minimum'       => $result['minimum'] > 0 ? $result['minimum'] : 1,
    					    'rating'        => $result['rating'],
					        'href'          => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				        );
		            }
		        }
	        }
	        
	        //print_r($data['order_history_products']);
	        
		$data['name']       = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['logout'] = $this->url->link('account/logout');
		$data['order'] = $this->url->link('account/order', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		
		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}		
		
		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('checkout/smart_cart', $data));
	}

}
