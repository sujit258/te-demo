<?php
class ControllerCheckoutCart extends Controller {
	public function index() {
		$this->load->language('checkout/cart');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('checkout/cart'),
			'text' => $this->language->get('heading_title')
		);
		
		if(isset($this->request->get['coupon']) && $this->request->get['coupon'] != '') {
		    $coupon = $this->request->get['coupon'];
		        
		    $this->load->model('extension/total/coupon');
            $coupon_info = $this->model_extension_total_coupon->getCoupon($coupon);
                
            if($coupon_info) {
		        $this->session->data['coupon'] = $coupon;
            }
        }
        
        $data['remaining'] = round(999 - $this->cart->getTotal());
        
		if(isset($this->request->get['cid']) && $this->request->get['cid'] > 0) {
		    $customer_id = $this->request->get['cid'];
		    if(isset($this->request->get['ep'])) {
		        foreach(unserialize(base64_decode(urldecode($this->request->get['ep']))) AS $email_products) {
		            $check_status_query = $this->db->query("SELECT status FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$email_products['pid'] . "' AND status = 1 LIMIT 1");
		            if($check_status_query->num_rows) {
		                $option_id = NULL;
		                $option = array();
		                
		                if(is_array($email_products['options'])) {
		                    foreach($email_products['options']['ovid'] AS $option_value_id) {
		                        if($option_value_id > 0) {
		                            $option_id_query = $this->db->query("SELECT option_id FROM " . DB_PREFIX . "option_value WHERE option_value_id = '" . (int)$option_value_id . "' LIMIT 1");
		                            $option_id = $option_id_query->row['option_id'];
		                            $product_options_query = $this->db->query("SELECT product_option_value_id, product_option_id FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$email_products['pid'] . "' AND option_id = '" . (int)$option_id . "' AND option_value_id = '" . (int)$option_value_id . "' LIMIT 1");
		                        }
		                    }
		                } else {
		                    $product_options_query = NULL;
		                }
		                
		                if($product_options_query->num_rows) {
	                        $option[$product_options_query->row['product_option_id']] = $product_options_query->row['product_option_value_id'];
	                    } else {
	                        $option = array();
	                    }
	                    $this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$customer_id . "' AND product_id = '" . (int)$email_products['pid'] . "' AND `option` LIKE '" . $this->db->escape(json_encode($option)) . "' LIMIT 1");
	                    
		                $quantity = 1;
		                $this->cart->add($email_products['pid'], $quantity, $option, '');
		            }
		        }
	        }
		}
		
		if(isset($this->request->get['product_id'])) {
		    $product_id = $this->request->get['product_id'];
		    
		    if(isset($this->request->get['option_id']) && isset($this->request->get['option_value_id'])) {
		        $option_id = $this->request->get['option_id'];
		        $option_value_id = $this->request->get['option_value_id'];
		    
		        $product_options_query = $this->db->query("SELECT product_option_value_id, product_option_id FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "' AND option_id = '" . (int)$option_id . "' AND option_value_id = '" . (int)$option_value_id . "' LIMIT 1");
		        
		        if($product_options_query->num_rows) {
		            $option[$product_options_query->row['product_option_id']] = $product_options_query->row['product_option_value_id'];
		        } else {
		            $option = array();
		        }
		    }
		    $quantity = 1;
		    $this->cart->add($product_id, $quantity, $option, '');
		    
		    if(isset($this->request->get['coupon']) && $this->request->get['coupon'] != '') {
		        $coupon = $this->request->get['coupon'];
		        
		        $this->load->model('extension/total/coupon');
                $coupon_info = $this->model_extension_total_coupon->getCoupon($coupon);
                
                if($coupon_info) {
		            $this->session->data['coupon'] = $coupon;
                }
		    }
		}

		if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_recurring_item'] = $this->language->get('text_recurring_item');
			$data['text_next'] = $this->language->get('text_next');
			$data['text_next_choice'] = $this->language->get('text_next_choice');

			$data['column_image'] = $this->language->get('column_image');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');

			$data['button_update'] = $this->language->get('button_update');
			$data['button_remove'] = $this->language->get('button_remove');
			$data['button_shopping'] = $this->language->get('button_shopping');
			$data['button_checkout'] = $this->language->get('button_checkout');
            
            $data['coupon'] = $this->session->data['coupon'];
            
			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$data['error_warning'] = $this->language->get('error_stock');
			} elseif (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$data['attention'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			$data['action'] = $this->url->link('checkout/cart/edit', '', true);

			if ($this->config->get('config_cart_weight')) {
				//$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
				$weight = $this->cart->getWeight();
				$data['weight'] = $weight/1000;
			} else {
				$data['weight'] = '';
			}

			$this->load->model('tool/image');
			$this->load->model('tool/upload');
			
			// Banners
			$data['top_banner_cart_desktop'] = $this->model_tool_image->resize('catalog/Banner/true-10-cart-desktop.jpg', 1920, 100);
			$data['top_banner_cart_mobile'] = $this->model_tool_image->resize('catalog/Banner/true-10-cart-mobile.jpg', 1920, 400);
			
			$data['cart_new_launch_desktop'] = $this->model_tool_image->resize('catalog/Banner/cart-new-launch-desktop.jpg', 1110, 810);
			$data['cart_new_launch_mobile'] = $this->model_tool_image->resize('catalog/Banner/cart-new-launch-mobile.jpg', 1320, 480);
			

			$data['products'] = array();

			$products = $this->cart->getProducts();

			foreach ($products as $product) {
				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
				} else {
					$image = '';
				}

				$option_data = array();
				$option_category_count = array();
                $seedsNuts = 0;
                $berriesFruits = 0;

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
						'option_category'   => $option['option_category']
					);
					if(html_entity_decode($option['option_category']) == 'Seeds & Nuts') {
					    $seedsNuts += 1;
					}
					if(html_entity_decode($option['option_category']) == 'Berries & Fruits') {
					    $berriesFruits += 1;
					}
				}
				$option_category_count[0]['name'] = 'Seeds & Nuts';
				$option_category_count[0]['value'] = $seedsNuts;
				$option_category_count[1]['name'] = 'Berries & Fruits';
				$option_category_count[1]['value'] = $berriesFruits;

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
					$reward_price = $this->tax->calculate($product['rewardprice'], $product['tax_class_id'], $this->config->get('config_tax'));
					
					$price = $this->currency->format($unit_price, $this->session->data['currency']);
					$te_total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
				} else {
					$price = false;
					$te_total = false;
				}

				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}
				
				$coupon_query = $this->db->query("SELECT c.code, c.description FROM " . DB_PREFIX . "coupon c LEFT JOIN " . DB_PREFIX . "coupon_product cp ON (c.coupon_id = cp.coupon_id) WHERE (c.display_cart = 1 AND c.status = 1) AND ((c.date_start = '0000-00-00' OR c.date_start < NOW()) AND (c.date_end = '0000-00-00' OR c.date_end > NOW())) AND cp.product_id = '" . $product['product_id'] . "'");
				
				if($coupon_query->num_rows) {
				    foreach($coupon_query->rows AS $coupon) {
				        $data['coupons'][] = array(
				            'coupon_code' => $coupon['code'], 
				            'description' => $coupon['description']
				        );
				    }
				}
				$data['coupons'] = array_unique($data['coupons'], SORT_REGULAR);
                
                $new_reward = round($product['reward1']*$reward_price/100);
                
				$data['products'][] = array(
					'cart_id'   => $product['cart_id'],
					'thumb'     => $image,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'option_category_count' => $option_category_count,
					'product_id' => $product['product_id'],
					'recurring' => $recurring,
					'quantity'  => $product['quantity'],
					'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					//'reward'    => $new_reward,
					'reward'    => ($new_reward ? sprintf($this->language->get('text_points'), $new_reward) : ''),
					'price'     => $price,
					'te_total'     => $te_total,
					'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
				);
			}
			
			$common_coupon_query = $this->db->query("SELECT code, description FROM " . DB_PREFIX . "coupon WHERE (display_cart = 1 AND status = 1) AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND common_coupon = 1");
			
			if($common_coupon_query->num_rows) {
			    foreach($common_coupon_query->rows AS $common_coupon) {
			        $data['coupons'][] = array(
			            'coupon_code' => $common_coupon['code'], 
			            'description' => $common_coupon['description']
			        );
			    }
			}
			
			
			
			$this->load->model('catalog/product');
			$data['sample_products'] = array();
				
			$sample_products = $this->model_catalog_product->getNewLaunchedProducts();
			
			if($sample_products) {
		        foreach($sample_products AS $product) {
		            $result = $this->model_catalog_product->getProduct($product['product_id']);
		            if ($result['image']) {
				    	$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				    } else {
				    	$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				    }

				    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				    	$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				    } else {
				    	$price = false;
				    }
    
				    if ((float)$result['special']) {
				    	$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				    } else {
    					$special = false;
				    }
    
				    if ($this->config->get('config_tax')) {
				    	$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				    } else {
    					$tax = false;
				    }
    
				    if ($this->config->get('config_review_status')) {
    					$rating = (int)$result['rating'];
				    } else {
    					$rating = false;
				    }
    				
				    $personalized = 0;
    				$options = NULL;
				    if($result['product_id'] != 6438 && $result['product_id'] != 6345 && $result['product_id'] != 6449) {
    				    $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				    } else {
    				    $personalized = 1;
				    }
    				
				    $percent = 0;
    				if($result['special'] > 0) {
				        $percent = round(100 - (($result['special'] / $result['price'])) * 100 ,0 ) > 0 ? sprintf($this->language->get('%s'), (round(100 - (($result['special'] / $result['price'])) * 100 ,0 ))) . '% OFF' : '';
    				}
				    $data['sample_products'][] = array(
				        'percent'       => $percent ? $percent : '',
    					'product_id'    => $result['product_id'],
				    	'thumb'         => $image,
     					'name'          => html_entity_decode(substr($result['name'],0,55) . ''),
     					'name_mobile'   => html_entity_decode(substr($result['name'],0,35) . ''),
    					'options'       => $options,
				    	'personalized'  => $personalized,
    					'quantity'      => $result['quantity'],
				    	'price'         => substr($price,3) > 0 ? $price : "",
    					'price1'        => $this->load->controller('extension/module/joseanmatias_preco_cupom/listview', $result),
				    	'special'       => $special,
     					'tax'           => $tax,
				    	'minimum'       => $result['minimum'] > 0 ? $result['minimum'] : 1,
    					'rating'        => $result['rating'],
					    'href'          => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				    );
		        }
		    }

			// Gift Voucher
			$data['vouchers'] = array();
		
		$this->load->model('extension/total/voucher_theme');
		
			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
				    
				    $voucher_image = $this->model_extension_total_voucher_theme->getVoucherThemeimage($voucher['voucher_theme_id']);
			           
			        $voucher_image = $this->model_tool_image->resize($voucher_image, 150, 150);
			        
					$data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
						'voucher_image'      => $voucher_image,
						'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)
					);
				}
			}
        
			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}

			$data['totals'] = array();

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
					'code'  => $total['code']
				);
				if($total['code'] == 'total') {
				    $data['free_delivery_value'] = 500 - round($total['value']);
				}
			}

			$data['continue'] = "/all-products";

			$data['checkout'] = $this->url->link('checkout/checkout', '', true);

			$this->load->model('extension/extension');

			$data['modules'] = array();
			
			$files = glob(DIR_APPLICATION . '/controller/extension/total/*.php');

			if ($files) {
				foreach ($files as $file) {
					$result = $this->load->controller('extension/total/' . basename($file, '.php'));
					
					if ($result) {
						$data['modules'][] = $result;
					}
				}
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('checkout/cart', $data));
		} else {
			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_error'] = $this->language->get('text_empty');
			$data['empty_cart'] = "image/catalog/images-08-19/banner/empty-cart.png";

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = "/all-products";
			
			$data['button_shopping'] = $this->language->get('button_shopping');

			unset($this->session->data['success']);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('checkout/cart', $data));
		}
	}

	public function add() {
		$this->load->language('checkout/cart');

		$json = array();
		
		
        
        if (isset($this->request->post['note'])) {
			$this->session->data['note'] = $this->request->post['note'];
		} else {
			$this->session->data['note'] =  0;
		}
		
		
		if (isset($this->request->post['is_invoice'])) {
			$this->session->data['is_invoice'] = $this->request->post['is_invoice'];
		} else {
			$this->session->data['is_invoice'] =  0;
		}
		
		
		
		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		if (isset($this->request->get['coupon'])) {
			$this->session->data['coupon'] = $this->request->get['coupon'];
		}
		
		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (isset($this->request->post['quantity']) && ((int)$this->request->post['quantity'] >= $product_info['minimum'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = $product_info['minimum'] ? $product_info['minimum'] : 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}

			if (!$json) {
				$this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id);
				
				                // BOF - Betaout Opencart mod
                $this->load->model('tool/betaout');
                                //$this->model_tool_betaout->track();
                $this->model_tool_betaout->trackEcommerceCartUpdate();
                // EOF - Betaout Opencart mod
                
                

				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));
			// Added to cart	
				$json['notice_add_layout'] = $this->config->get('notice_add_layout');
				$json['notice_add_timeout'] = $this->config->get('notice_add_timeout');
				$json['notice_add_status'] = $this->config->get('notice_add_status');
				$this->load->model('tool/image');				
				if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
				} else {
						$image = '';
				}					
				$json['href'] = $image;		
			// Added to cart			

				// Unset all shipping and payment methods
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);

				// Totals
				$this->load->model('extension/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
		
				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('extension/total/' . $result['code']);

							// We have to put the totals in an array so that they pass by reference.
							$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
						}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);
				}

				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function edit() {
		$this->load->language('checkout/cart');

		$json = array();

		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
				
				            // BOF - Betaout Opencart mod
                            $this->load->model('tool/betaout');
                            $this->model_tool_betaout->track();
            // EOF - Betaout Opencart mod
            
            
			}

			$this->session->data['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			$this->response->redirect($this->url->link('checkout/cart'));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function ajaxupdatecart() {
		$this->load->language('checkout/cart');
		$json = array();

		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
				// BOF - Betaout Opencart mod
                $this->load->model('tool/betaout');
                $this->model_tool_betaout->track();
                // EOF - Betaout Opencart mod
			}

			$this->session->data['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);
			
			$data['coupon'] = $this->session->data['coupon'] ? $this->session->data['coupon'] : '';

			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$data['error_warning'] = $this->language->get('error_stock');
			} elseif (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$data['attention'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			$data['action'] = $this->url->link('checkout/cart/edit', '', true);

			if ($this->config->get('config_cart_weight')) {
				//$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
				$weight = $this->cart->getWeight();
				$data['weight'] = $weight/1000;
			} else {
				$data['weight'] = '';
			}
            
            $data['remaining'] = round(999 - $this->cart->getTotal());
            
			$this->load->model('tool/image');
			$this->load->model('tool/upload');

			$data['products'] = array();

			$products = $this->cart->getProducts();

			foreach ($products as $product) {
				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
				} else {
					$image = '';
				}

				$option_data = array();
				$option_category_count = array();
                $seedsNuts = 0;
                $berriesFruits = 0;

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
						'option_category'   => $option['option_category']
					);
					if(html_entity_decode($option['option_category']) == 'Seeds & Nuts') {
					    $seedsNuts += 1;
					}
					if(html_entity_decode($option['option_category']) == 'Berries & Fruits') {
					    $berriesFruits += 1;
					}
				}
				$option_category_count[0]['name'] = 'Seeds & Nuts';
				$option_category_count[0]['value'] = $seedsNuts;
				$option_category_count[1]['name'] = 'Berries & Fruits';
				$option_category_count[1]['value'] = $berriesFruits;

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
					
					$price = $this->currency->format($unit_price, $this->session->data['currency']);
					$te_total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
				} else {
					$price = false;
					$te_total = false;
				}

				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}
				
				$coupon_query = $this->db->query("SELECT c.code, c.description FROM " . DB_PREFIX . "coupon c LEFT JOIN " . DB_PREFIX . "coupon_product cp ON (c.coupon_id = cp.coupon_id) WHERE (c.display_cart = 1 AND c.status = 1) AND ((c.date_start = '0000-00-00' OR c.date_start < NOW()) AND (c.date_end = '0000-00-00' OR c.date_end > NOW())) AND cp.product_id = '" . $product['product_id'] . "'");
				
				if($coupon_query->num_rows) {
				    foreach($coupon_query->rows AS $coupon) {
				        $data['coupons'][] = array(
				            'coupon_code' => $coupon['code'], 
				            'description' => $coupon['description']
				        );
				    }
				}
				$data['coupons'] = array_unique($data['coupons'], SORT_REGULAR);
				
				$new_reward = round($product['reward1']*$unit_price/100);

				$data['products'][] = array(
					'cart_id'   => $product['cart_id'],
					'thumb'     => $image,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'option_category_count' => $option_category_count,
					'product_id' => $product['product_id'],
					'recurring' => $recurring,
					'quantity'  => $product['quantity'],
					'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					//'reward' => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
					'reward'    => ($new_reward ? sprintf($this->language->get('text_points'), $new_reward) : ''),
					'price'     => $price,
					'te_total'     => $te_total,
					'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
				);
			}
			
			$common_coupon_query = $this->db->query("SELECT code, description FROM " . DB_PREFIX . "coupon WHERE (display_cart = 1 AND status = 1) AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND common_coupon = 1");
			
			if($common_coupon_query->num_rows) {
			    foreach($common_coupon_query->rows AS $common_coupon) {
			        $data['coupons'][] = array(
			            'coupon_code' => $common_coupon['code'], 
			            'description' => $common_coupon['description']
			        );
			    }
			}

			// Gift Voucher
			$data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
						'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)
					);
				}
			}

			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}

			$data['totals'] = array();

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
					'code'  => $total['code']
				);
				if($total['code'] == 'total') {
				    $data['free_delivery_value'] = 500 - round($total['value']);
				}
			}
			
			$data['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));

			$data['continue'] = "/all-products";

			$data['checkout'] = $this->url->link('checkout/checkout', '', true);

			$this->load->model('extension/extension');

			$data['modules'] = array();
			
			$files = glob(DIR_APPLICATION . '/controller/extension/total/*.php');

			if ($files) {
				foreach ($files as $file) {
					$result = $this->load->controller('extension/total/' . basename($file, '.php'));
					
					if ($result) {
						$data['modules'][] = $result;
					}
				}
			}
		}
		$data['text_error'] = $this->language->get('text_empty');
		$data['button_shopping'] = $this->language->get('button_shopping');
		$data['continue'] = "/all-products";
		$data['empty_cart'] = "image/catalog/images-08-19/banner/empty-cart.png";

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function remove() {
		$this->load->language('checkout/cart');

		$json = array();

		// Remove
		if (isset($this->request->post['key'])) {
			$this->cart->remove($this->request->post['key']);

            // BOF - Betaout Opencart mod
            $this->load->model('tool/betaout');
            $this->model_tool_betaout->trackEcommerceCartUpdate();
            // EOF - Betaout Opencart mod
            
            
			unset($this->session->data['vouchers'][$this->request->post['key']]);

			$json['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);

						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}

			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}