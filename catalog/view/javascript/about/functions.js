;(function($, window, document, undefined) {
	var $win = $(window);
	var $doc = $(document);
	var $slider = [];
	var counters = [];

	var classes = {
		owlWrapper: 'owl-carousel'
	};
	
	$doc.ready(function() {
		$slider['main'] = $('.slider-main');
		$slider['products'] = $('.slider-products');

		initSliders();
		listInstagramDelay();

		//Select
		$( 'select' ).select2({
			minimumResultsForSearch: -1
		});

		//is touch device
		isTouchDevice();

		if( isTouchDevice() === true ) {
			$('body').removeClass('no-touch');
		}
		else {
			$('body').addClass('no-touch');
		}

		// Mobile navigation
		$('.nav-trigger').on('click', function(e){
			e.preventDefault();
			$(this).toggleClass('active');
			$('body').toggleClass('nav-shown');
		});

		$('.nav').on('touchstart click', 'a', function(e) {
			var $subMenu = $(this).next('ul');
			var $parentLi = $(this).parent();

			$('.nav').on('touchstart click', 'a', function(e) {
				var $subMenu = $(this).next('ul');
				var $parentLi = $(this).parent();

				if ( $win.width() < 1023 ) {
					if ( $subMenu.length && !$subMenu.is(':visible') ) {
						e.preventDefault();

						$subMenu.slideDown(), 1000;

					$subMenu.closest('li').siblings().find('ul').slideUp(), 1000;
					} 
				} else {
					if ( $subMenu.length && !$parentLi.hasClass('show-submenu') ) {
						e.preventDefault();

						$(this).parent('li').siblings().removeClass('show-submenu');

						$parentLi.addClass('show-submenu');
					} 
				}
			});
		});

		$win.on('scroll resize load', function(){
			var $sectionIngredients = $('.main > section');
			var $listCookies = $('.list-cookies li');

			elementInViewPort($sectionIngredients);
			elementInViewPort($listCookies);
		});

		// Smooth scroll
		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);
					$(this).parent().addClass('current').siblings().removeClass('current');
				return false;
			  }
			}
		});

		// Sticky footer
		 $win.on('load resize', function(){
			var $main = $('.main');
			var winHeight = $win.height();
			var headerHeight = $('.header').outerHeight();
			var footerHeight = $('.footer').outerHeight();

			$main.css({
				minHeight: winHeight - headerHeight - footerHeight
			});

			checkHeightListInstagram();
		});

		 //Tabs
		 $('.tab .tab__head').on('click', 'a', function(e){
			e.preventDefault();
			var questionElement = $(this).parents('.tab');
			var answerElement   = questionElement.find('.tab__body');
			$('.tab .tab__body').not(answerElement).slideUp().closest('.tab').removeClass('current');
			answerElement.slideToggle().closest('.tab').toggleClass('current');
		});

		var $cartTotal = "$0";
		var $cartCount = 0;

		if (document.cookie.length == 0) {
			return false;
		} else {
			var cartCookie = Cookies.get('cart');
			var cartData = cartCookie.replace('contents=', '');
			var cartObject = JSON.parse(cartData);

			var $cartTotal = '$' + cartObject.subtotal;
			var $cartCount = cartObject.itemcount;
		}

		var cartCount = document.querySelector('.c-cart__count');
		var cartTotal = document.querySelector('.c-cart__total');

		cartCount.innerText = $cartCount;
		cartTotal.innerText = $cartTotal;

		console.log('Cart total: ' + $cartTotal);
		console.log('Cart count ' + $cartCount);

	});

	$win.on('load', function() {
		initCounters();

		$('[data-percent], [data-number], .list-entries > ul').viewportChecker({
			callbackFunction: function(elem, action){
				if (elem.is('[data-percent]')) {
					animateCircle(elem);
				}

				if (elem.is('[data-number]')) {
					playCounter(elem.attr('id'));
				}

				if (elem.is('.list-entries > ul')) {
					elem.addClass('visible');
				}
			}
		});
	});

	function isTouchDevice(){
		return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
	}

	function initSliders() {
		//Slider Main
		if ( $slider['main'].length ) {
			$slider['main'].find('.' + classes.owlWrapper).owlCarousel({
				loop:true,
				items: 1,
				autoplay: true,
				autoplayTimeout: 8000
			});
		}

		if($slider['products'].length) {
			$slider['products'].find('.' + classes.owlWrapper).owlCarousel({
				loop:true,
				items: 3,
				autoplay: false,
				autoplayTimeout: 3000,
				center: true,
				nav: true,
				responsive: {
					767 : {
						items: 2,
						center: false
					},
					1130 : {
						items: 3		
					}
				}
			});
		}
	};

	function checkHeightListInstagram() {
		var $container = $('.list-instagram')

		if($container.length) {
			var _innerHeight = $container.find('>ul>li').eq(0).innerHeight();
			var _innerHeightLarge = $container.find('>ul>li.list__large').eq(0).innerHeight();

			if(_innerHeightLarge > _innerHeight) {
				$container.find('>ul>li').css('min-height', _innerHeightLarge);
			}
		}
	};

	function animateCircle($elem) {
		$elem.circliful({
			fontColor: '#ff4338',
			foregroundBorderWidth: 7,
			backgroundBorderWidth: 7,
			percentageTextSize: 33,
			foregroundColor: "#ef483e",
			backgroundColor: "#d0f0ea",
			percentageY: 113,
			percentageX: 103,
			textAdditionalCss: 'font-weight: 700;',
			animationStep: 3
		});
	};

	function initCounters() {
		var _counterId = 1;

		$('[data-number]').each( function() {
			var $this = $(this);
			var _duration = $this.data('duration') != undefined ? $this.data('duration') : 2;
			var _value = $this.data('number');
			var _startNumber = $this.data('start') != undefined ? $this.data('start') : 0;
			var _decimals = $this.data('number') % 1 != 0 ; 

			$this.attr('id', 'counter-' + _counterId);

			var counter = new CountUp($this.attr('id'), _startNumber, _value, _decimals, _duration, { useEasing : false, });

			counters.push([counter, $this.attr('id')]);

			_counterId += 1;
		});
	};

	function playCounter(_elemId) {
		for (var i = counters.length - 1; i >= 0; i--) {
			if (counters[i][1] === _elemId) {
				counters[i][0].start();
				$('#' + _elemId).addClass('active');
				break;
			}
		};
	};

	function elementInViewPort(elem) {
		if(elem.length){
			elem.each( function(i){
				if(!$(this).hasClass('animate')) {
					var bottom_of_object = $(this).offset().top - ($win.height() / 1.5);
					var scrollTop = $win.scrollTop();

					bottom_of_object = (bottom_of_object > 0) ? bottom_of_object : 0;

					if(scrollTop >= bottom_of_object) {
						$(this).addClass('animate');
					}
				}
			});
		}
	}

	function listInstagramDelay() {
		var $container = $('.list-instagram li');

		if($container.length) {
			var delay = 0.09;
			$container.each(function(i) {
				$(this).css('transition-delay', (delay * i) + 's');
			});
		}
	}
})(jQuery, window, document);
