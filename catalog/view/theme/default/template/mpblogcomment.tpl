
<?php if ($comments) { ?>

<?php foreach ($comments as $comment) { ?>

<div class="mp-comments col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-padding">
  <div class="col-lg-1 col-xs-2 blog-user-icon blog-padding"><div class="img-circle mp-avatar"><i class="fa fa-user"></i></div></div>
   <div class="col-lg-6 col-xs-4 blog-padding"><h4 class="mp-commentauthor"><?php echo $comment['author']; ?></h4></div>
   <div class="col-lg-5 col-xs-6 blog-date pull-right blog-padding"><h5 class="blog-comment-date"><i class="fa fa-calendar"></i><?php echo $comment['date_added']; ?></h5></div>
  </div>

 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-comment-text blog-padding">
   <p><?php echo $comment['text']; ?></p>
 </div>
</div>
	 

<?php } ?>
<?php if($show_read_more) { ?>
    <button id="comment-button" class="btn btn-default">Click to see more</button>
<?php } ?>

<div id="comment-pagination" class="text-right <?php if($show_read_more) { echo 'hidden'; } ?>"><?php echo $pagination; ?></div>

<p><?php echo $comment_total; ?></p>

<?php } else { ?>

<p class="commentt"><?php echo $text_no_comments; ?></p>

<?php } ?>
<script type="text/javascript">
    $('#comment-button').click(function() {
        $('#comment-pagination').removeClass('hidden');
        $('#comment-button').remove();
        $('#comment-pagination .pagination li:nth-child(2) a').click();
    });
</script>