<?php echo $header; ?>
<link href="catalog/view/javascript/addtocart/noty.css" type="text/css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/addtocart/notice_add.min.js" type="text/javascript"></script>
<style>
    .quantity {
        height: 35px!important;
        width: 60%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .price-new {
        display: inline-block!important;
        font-size: 15px!important;
        color: #f49a25!important;
        text-align: left!important;
        font-weight: 700!important;
        margin: 0!important;
    }
    .price-old {
        display: inline-block!important;
        color: #5c5c5c!important;
        font-size: 12px!important;
        margin-left: 5px!important;
        text-decoration: line-through!important;
    }
    .options {
        min-height: 45px;
        padding-right: 21px;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    .options .radio input {
        display: none !important;
    }
    .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .breadcrumb {
        margin-bottom: 15px!important;
    }
</style>
<ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      
          
        <div class="description">
            <a href="/our_products">
                <img src="/image/catalog/Banner/hp-brand-store-desktop.jpg" style="" class="img-responsive hidden-xs hidden-sm">
            </a>
        
            <a href="/our_products">
                <img src="/image/catalog/Banner/hp-brand-store-mobile.jpg" style="" class="img-responsive hidden-md hidden-xl hidden-lg">
            </a>
        </div>
<div class="container isl-instagramphotos islip-page th-isl" id="container">
  

  <div class="row">
    <?php echo $column_left; ?>

    <?php if ($column_left and $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
    <?php } ?>

    <div id="content" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>

      <h1 class=" "><?php echo $setting['page']['title']['lang_id']; ?></h1>

      <div class="clearfix">
        <!--<?php if ($banner) { ?>
          <?php if ($setting['page']['banner_link']) { ?>
            <a href="<?php echo $setting['page']['banner_link']; ?>" target="_blank">
              <img src="<?php echo $banner; ?>" alt="<?php echo $setting['page']['title']['lang_id']; ?>" class="img-responsive">
            </a>
          <?php } else { ?>
            <img src="<?php echo $banner; ?>" alt="<?php echo $setting['page']['title']['lang_id']; ?>" class="img-responsive">
          <?php } ?>
        <?php } ?>-->
    

        <div class="islip-gutter-5px clearfix js-islip-page-container"></div>
        <div class="container">
            <div class="waiting"></div>
        </div>    
        <div class="js-photos-notification"></div>
        <div class="text-center">
          <a class="btn btn-default btn-info btn-sm js-fetch-more" data-isl-fetch>Load more</a>
        </div>
      </div>

<?php if (!empty($custom_css) ) { ?>
  <?php echo $custom_css; ?>
</style>
<?php } ?>
<script>
$(document).ready(function()
{
  // Get photos
  fetchPhotos();
  $('[data-isl-fetch]').on('click', function() {
    fetchPhotos();
  });

  // On addCart
  $('body').on('click', '[data-isg-add-cart]', function() {
    var product_id = $(this).data('isg-add-cart');

    if (product_id) {
      <?php if ($theme == 'journal2') { ?>
        addToCart(product_id);
      <?php } else { ?>
        cart.add(product_id);
      <?php } ?>

      setTimeout(function() {
        $.magnificPopup.close();
      }, 1000);
    }
  });
});

function fetchPhotos() {
  var page = $('[data-isl-fetch]').data('isl-fetch');

  $.ajax({
    url: 'index.php?route=product/insta_shop/fetch&_='+ new Date().getTime(),
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: {
      type :"page",
      page : page
    },
    beforeSend: function() {
        $('.js-photos-notification').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i> Please wait..</div>');
    },
    success: function(data) {
        
      $('.js-photos-notification').html('');
      $('.islip-page .js-islip-page-container').append(data['output']);

      if (data['page_info']['has_next_page']) {
        $('.js-fetch-more').fadeIn();
        $('.js-fetch-more').data('isl-fetch', data['page_info']['end_cursor']);
      } else {
        $('.js-fetch-more').fadeOut();
      }

      photoGallery();
    }
  });
}

function photoGallery() {
  $('.js-islip-page-container').magnificPopup({
    delegate: '.isl-photo-gallery',
    type: 'ajax',
    gallery: {
      enabled: true,
      tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
    },
    showCloseBtn: true,
    callbacks: {
      open: function() {
        $('html').addClass('mfpip-open');
      },
      close: function() {
        $('html').removeClass('mfpip-open');
      }
    }
  });
}
</script>
      <?php echo $content_bottom; ?>
    </div>

    <?php echo $column_right; ?></div>
</div>

<?php echo $footer; ?>