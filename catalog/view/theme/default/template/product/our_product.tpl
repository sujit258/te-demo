<style type="text/css">
@import url("https://fonts.googleapis.com/css?family=Amatic+SC");
body {
  margin: 0px;
  padding: 0px;
  font-family: "gothamBold";
  overflow-x: hidden;
}

.msg11 {
    font-size: 15px;
}
.msg12 {
    font-size: 15px;
    margin-top: 4px;
}


.qty-container {
    width: 50%;
}
.breadcrumb {
    margin-bottom: 15px!important;
}

.category-title {
    font-size: 35px;
}
.sew {
  width: 100%;
}

.banner-text-lg {
    font-size: 6em!important;
}

.banner-text-md {
    font-size: 4em!important;
}

h1 {
  font-size: 6em;
  font-weight: bold;
}

h3 {
  font-size: 4em;
}
.intro {
  position: absolute;
  text-align: center;
  top: 280px;
  left: 470px;
}

.space {
  height: 80px;
}

h2 {
  font-weight: bold;
}

.space1 {
  padding: 0;
  margin: 0;
  background-color: #f49a25;
  height: 50px;
  text-align: center;
  font-size: 2em;
}

  .product-image {
      /*margin: auto;
      display: table;*/
      padding: 5%;
  }
  h2 {
      text-align: center;
  }
  
  .product-name-text{
        font-size: 16px;
        text-align: center;
        font-weight: 700;
    }
    .product-name-text a {
        color: #000;
    }
    .options {
        padding: 0px!important;
        min-height: 55px!important;
    }
    .product-name-text {
        min-height: 40px;
        text-align: left;
    }
    .product-options {
        text-align: center;
        float: left;
        background: #fff!important;
        width: 90%;
        color: #000;
    }
    .button-product-cart {
        float: right!important;
        font-size: 13px!important;
        background: #f49a25;
        color: #fff;
    }
    .button-personalize {
        font-size: 13px!important;
        float: right !important;
        background: #f49a25;
        color: #fff;
        margin-top: 0px;
    }
    .price-label {
        padding: 5px 0px !important;
        min-height: 38px!important;
        width: 100%!important;
    }
    .price {
        margin-bottom: 0px!important;
        min-height: 22px!important;
    }
    .quantity {
        height: 35px!important;
        width: 45%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    .oos {
        background: #777!important;
        border-radius: 4px!important;
        float: right!important;
        font-size: 13px!important;
        color: #fff;
    }
    .oos:hover {
        cursor: not-allowed!important;
        color: #fff!important;
    }
    .button-product-cart:focus, .button-personalize:focus {
        color: #fff!important;
    }
    .button-product-cart:hover, .button-personalize:hover {
        color: #fff!important;
        background: #000!important;
    }
    .center {
        text-align: center;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 23px !important;
        padding: 9px 10px !important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .category-title {
        margin: 70px 0 20px 0;
    }
    .main-row {
        margin-right: 0px!important;
        margin-left: 0px!important;
    }
    .price-new {
        display: inline-block!important;
        font-size: 15px!important;
        color: #f49a25!important;
        text-align: left!important;
        font-weight: 700!important;
        margin: 0!important;
    }
    .price-old {
        display: inline-block!important;
        color: #5c5c5c!important;
        font-size: 12px!important;
        margin-left: 5px!important;
        text-decoration: line-through!important;
    }
    .deals-overlay {
        right: 0px;
        top: 10px;
        position: absolute;
        background:#f49a25;
        color:#fff;
        padding: 4px;
        font-weight: 600;
        width: 25%;
        text-align: center;
        z-index: 1;
    }
    .top-banner {
        margin-top: 0!important;
        margin-bottom: 25px!important;
    }
    .top-banner img {
        width: 100%;
    }
    .top-image img {
        width: 100%;
    }
    .top-banners {
        margin-bottom: 25px;
    }
    .readyToEatBanner, .readyToCookBanner, .seedsMixesBanner, .rawAndroastedSeedsBanner {
        cursor: pointer;
    }
    #readyToEatBreakfast, #seedsMixes, #readyToCookBreakfast, #rawAndRoastedSeeds, #others, #tea, #wcbs, #combo {
        padding-top: 15px;
    }
    .sub-category {
        font-size: 25px;
        margin-top: 25px;
        font-weight: bold;
    }
    .price, .coupon {
        padding-left: 0px!important;
        padding-right: 0px!important;
    }
    .coupon {
        min-height: 20px!important;
        text-align: left;
        font-size: 12px!important;
        margin-bottom: 0!important;
    }
    .options .radio input {
        display: none!important;
    }
    .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px!important;
    }
    .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0!important;
        text-align: center;
        min-width: 52px;
    }
    .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    .product-container {
        padding: 0!important;
    }
@media(min-width: 1000px) {
    .mobile {
        min-height: 570px!important;
    }
    .youtubeVideo {
        padding: 0 30px!important;
    }
}
@media (max-width: 1000px){
    .youtubeVideo {
        padding: 0!important;
    }
    .qty-container {
        width: 70%!important;
    }
    .coupon {
        text-align: left!important;
    }
    .intro {
        position: absolute;
        text-align: center;
        top: 15%;
        left: 22%;
    }
    .button-personalize {
        font-size: 14px!important;
        margin-top: 0px!important;
    }
    .product-name-text {
        min-height: 40px!important;
        font-size: 14px!important;
    }
    .banner-text-lg {
        font-size: 3em!important;
    }
    .banner-text-md {
        font-size: 1.5em!important;
    }
    .category-title {
        margin: 40px 0 20px 0!important;
        font-size: 28px!important;
    }
    .mobile{
        margin-left: 0px;
        margin-bottom: 10px;
        min-height: 470px!important;
        padding: 7px!important;
    } 
    .container {
        width: 100%!important;
    }
    .product-name-text .select{
        height: 90px;
        font-size: 20px;
        text-align: center; 
        font-family: roboto, 'Oswald', sans-serif;
    }
    .deals-overlay {
        width: 35%!important;
    }
    .readyToEatBanner, .readyToCookBanner, .seedsMixesBanner, .rawAndroastedSeedsBanner {
        margin-top: 10px;
    }
    .quantity-container {
        width: 60%!important;
    }
    .radio label span {
        font-size: 12px!important;
    }
    .options .radio {
        margin: 0px 0px 5px -2px!important;
    }
    .options {
        min-height: 55px!important;
    }
    .category-name h4 {
        line-height: 15px;
        font-size: 12px!important;
        padding: 8px 1px!important;
        margin-top: 0!important;
    }
    .category-name, .bookmark-heading {
        padding-left: 5px!important;
        padding-right: 5px!important;
    }
}
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .category-name h4 {
        border: 1px solid #c6c6c6!important;
        border-radius: 4px;
        padding: 14px;
        margin-top: 5px!important;
        font-size: 12px!important;
        font-weight: bold;
        text-align: center;
    }
    .category-name h4:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
        cursor: pointer!important;
    }
    .bookmark-heading {
        font-weight: 600;
    }
    
    .deals-discount {
        border-radius: 50%;
        right: 0px;
        top: 5%;
        position: absolute;
        background: #f49a25;
        text-align: center;
        z-index: 5;
        width: 50px;
    }
    
    .deals-discount h5 {
        color: #fff;
        font-weight: 600;
        padding: 1px;
    }
    
    .price-coupon{
        font-weight: bold;
        padding-right: 5px;
    }
  </style>
  <?php echo $header; ?>
  <ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
  </ul>

<div class="container-fluid">
    <div class="row top-banner hidden-xs hidden-sm">
        <div class="category-product-carousel w3-content w3-section">
            <a href="/index.php?route=checkout/cart&product_id=6481&option_id=1&option_value_id=80&coupon=launch20"><img alt="True Elements" class="mySlides" src="<?php echo $slider_daily_dose_trail_mix_desktop; ?>" class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=6422&option_id=1&option_value_id=80&coupon=BERRIES15"><img alt="True Elements" class="mySlides" src="<?php echo $slider_berries_mix_desktop; ?>" class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=6451&option_id=1&option_value_id=80"><img alt="True Elements" class="mySlides" src="<?php echo $slider_7_in_1_desktop; ?>" class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=5057&option_id=1&option_value_id=113"><img alt="True Elements" class="mySlides" src="<?php echo $slider_raw_honey_desktop; ?>"  class="img-responsive"></a>
		</div>

        <script type="text/javascript">
            $(document).ready(function() {
                $(".category-product-carousel").owlCarousel({
                    autoPlay: true,
                    items : 1,
                    slideSpeed : 1000,
                    navigation : true,
                    pagination : true,
                    controls: true,
                    stopOnHover : false,
                    itemsDesktop : [1199,1],
                    itemsDesktopSmall : [991,1],
                    itemsTablet: [700,1],
                    itemsMobile : [400,1],
                });
            });
        </script>
    </div>
    <div class="row top-banner hidden-md hidden-xl hidden-lg">
        <div class="category-product-mobile-carousel w3-content w3-section">
            <a href="/index.php?route=checkout/cart&product_id=6481&option_id=1&option_value_id=80&coupon=launch20"><img alt="True Elements" class="mySlides" src="<?php echo $slider_daily_dose_trail_mix_mobile; ?>"  class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=6422&option_id=1&option_value_id=80&coupon=BERRIES15"><img alt="True Elements" class="mySlides" src="<?php echo $slider_berries_mix_mobile; ?>"  class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=6451&option_id=1&option_value_id=80"><img alt="True Elements" class="mySlides" src="<?php echo $slider_7_in_1_mobile; ?>"  class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=5057&option_id=1&option_value_id=113"><img alt="True Elements" class="mySlides" src="<?php echo $slider_raw_honey_mobile; ?>"  class="img-responsive"></a>
		</div>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".category-product-mobile-carousel").owlCarousel({
                    autoPlay: true,
                    items : 1,
                    slideSpeed : 1000,
                    navigation : true,
                    pagination : true,
                    controls: true,
                    stopOnHover : false,
                    itemsDesktop : [1199,1],
                    itemsDesktopSmall : [991,1],
                    itemsTablet: [700,1],
                    itemsMobile : [400,1],
                });
            });
        </script>
    </div>
      
<!-- Postcode -->
<!--br>
<center>    
      <?php ?>
       <div style="overflow: none;">
              <form method = "POST" id = "pincheck1" <?php if(isset($_SESSION['pincode'])){ echo "style='display:none;'";}?>>
               
                <span style="font-size: 15px;">Check time to deliver to your pincode</span>&nbsp;&nbsp;&nbsp;<input placeholder="Enter Delivery Pincode" type="text" id="pin1" style="padding: 5px;background-color: #f5f5f5;">
                <input type = "button"  onClick = "getdata1()" value="Check" id="button-check" class="button" style="background: #000 !important;border-radius: 0px !important;padding: 7px 12px 7px 12px;"/>
                <div id="show_message" style="display:none; color: red;">Enter Delivery Pincode</div>
              </form>
              <div id="msg1" >
                <?php
                if(isset($_SESSION['pincode'])){
                echo "<div id='msg' ><span><i class='fa fa-map-marker'></i> Pincode : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display:inline;'><input type = 'button'  onclick = 'showform1()' value='Change' id='button-change' class='button'/></form></span><br/>";
                    if(isset($_SESSION['pin_check_delivery']) &&        $_SESSION['pin_check_delivery'] != ''){
                      echo $_SESSION['pin_check_delivery'];
                    }
                    echo $_SESSION['pin_check_result'];
                    echo"</font></font></div>";
                }
            ?>
          </div>
        </div-->          
    <?php ?>
        <!--- POSTCODE --->
        
        <br /><br /></center>
        
                
 
        
    <div class="container hidden-xs hidden-sm">
        <h4 class="col-md-12 bookmark-heading">Breakfast</h4>
        <div class="col-md-2 category-name">
		    <h4 class="oatsBookmark">Oats</h4>
		</div>
		<div class="col-md-2 category-name">
		    <h4 class="supergrainsBookmark">Supergrains</h4>
		</div>
        <div class="col-md-2 category-name">
		    <h4 class="granolaBookmark">Granola</h4>
		</div>
        <div class="col-md-2 category-name">
		    <h4 class="muesliBookmark">Muesli</h4>
		</div>
        <div class="col-md-2 category-name">
		    <h4 class="flakesBixBookmark">Flakes</h4>
		</div>
        <div class="col-md-2 category-name">
		    <h4 class="oatmealBookmark">Oatmeal</h4>
		</div>
		<!--div class="col-md-2 category-name">
		    <h4 class="teaBookmark">Tea</h4>
		</div-->
		
		<h4 class="col-md-12 bookmark-heading">Snacks</h4>
		<div class="col-md-2 category-name">
		    <h4 class="seedsMixesBookmark">Seeds Mixes</h4>
		</div>
		<div class="col-md-2 category-name">
		    <h4 class="otgBookmark">On the Go</h4>
		</div>
		<div class="col-md-2 category-name">
		    <h4 class="rawSeedsBookmark">Healthy Seeds</h4>
		</div>
		<div class="col-md-2 category-name">
		    <h4 class="roastedSeedsBookmark">Roasted Seeds</h4>
		</div>
		<div class="col-md-2 category-name">
		    <h4 class="othersBookmark">For your Sweet Tooth</h4>
		</div>
		
		<h4 class="col-md-12 comboBookmark">Value Packs</h4>
		<div class="col-md-2 category-name">
		    <h4 class="giftspackBookmark">Gift Packs</h4>
		</div>
		<div class="col-md-2 category-name">
		    <h4 class="sspBookmark">Super Saver Packs</h4>
		</div>
		
		
		<!--h4 class="col-md-12 bookmark-heading">Others</h4>
        <div class="col-md-2 category-name">
		    <h4 class="teaBookmark">Tea</h4>
		</div> 
		<div class="col-md-2 category-name">
		    <h4 class="giftBookmark">Gift Certificates</h4>
		</div>
        <div class="col-md-2 category-name">
		    <h4 class="wcbsBookmark">Be Right Back</h4>
		</div-->
    </div>
    <div class="hidden-md hidden-xl hidden-lg">
        <h4 class="col-xs-12 col-md-12 bookmark-heading">Breakfast</h4>
        <div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="oatsBookmark">Oats</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="supergrainsBookmark">Supergrains</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="granolaBookmark">Granola</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="muesliBookmark">Muesli</h4>
		</div>
        <div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="flakesBixBookmark">Flakes</h4>
		</div>
        <div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="oatmealBookmark">Oatmeal</h4>
		</div>
		<!--div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="teaBookmark">Tea</h4>
		</div-->
		<h4 class="col-xs-12 col-md-12 bookmark-heading">Snacks</h4>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="seedsMixesBookmark">Seeds Mixes</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="otgBookmark">On The Go</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="rawSeedsBookmark">Healthy Seeds</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="roastedSeedsBookmark">Roasted Seeds</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="othersBookmark">For Sweet Tooth</h4>
		</div>
		
		<h4 class="col-xs-12 col-md-12 bookmark-heading">Value Packs</h4>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="giftspackBookmark">Gift Packs</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="sspBookmark">Super Saver Packs</h4>
		</div>
		
		<!--h4 class="col-xs-12 col-md-12 bookmark-heading">Others</h4>
        <div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="teaBookmark">Tea</h4>
		</div>
		<div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="giftBookmark">Gift Certificates</h4>
		</div>
        <div class="col-sm-2 col-xs-4 category-name">
		    <h4 class="wcbsBookmark">Be Right Back</h4>
		</div-->
    </div>
</div>
    <!-- <div class="container youtubeVideo">
        <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/RDW_2dOi9SE?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

<div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?> -->

<input type="hidden" name="product_id" value="" />
<div class="row main-row" id="readyToCookBreakfast">
    <div class="container">
        <?php if ($oatsproducts || $supergrainsproducts) { ?>
            <h3 class="center category-title readyToCookSection">Ready to Cook Breakfast</h3>
            <?php if($oatsproducts) { ?>
                <a class="hidden-xs hidden-sm" href="/index.php?route=checkout/cart&product_id=5657&option_id=1&option_value_id=120&coupon=ROLLED20"><img alt="True Elements" class="img-responsive" src="<?php echo $rolled_oats_desktop; ?>" style="margin-top: 25px;"></a>
                <a class="hidden-md hidden-xl hidden-lg" href="/index.php?route=checkout/cart&product_id=5657&option_id=1&option_value_id=120&coupon=ROLLED20"><img alt="True Elements" class="img-responsive" src="<?php echo $rolled_oats_mobile; ?>"></a>
            <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="oats">
                <h4 class="sub-category">Oats</h4>
                <hr/>
                <?php foreach ($oatsproducts as $oats) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($oats['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($oats['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $oats['href']; ?>">
                            <div class="pro_img">
                                <?php if($oats['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $oats['rotator_image']; ?>" alt="<?php echo $oats['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $oats['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $oats['href']; ?>"><?php echo $oats['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($oats['options']) { ?>
    	                    <div class="options options-<?php echo $oats['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($oats['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $oats['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $oats['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $oats['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $oats['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $oats['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $oats['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($oats['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($oats['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$oats['special']) { ?>
                                                <span class="price-new price-new-<?php echo $oats['product_id']; ?>"><?php echo $oats['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $oats['product_id']; ?>"><?php echo $oats['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $oats['product_id']; ?>"><?php echo $oats['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($oats['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $oats['product_id']; ?>"><b> <?php echo $oats['coupon_discount']; ?> </b></span><span> <?php echo $oats['coupon'] ? 'Using code: <b>' . $oats['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $oats['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $oats['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $oats['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $oats['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $oats['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $oats['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $oats['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $oats['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $oats['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($oats['quantity'] > 0) { ?>
						        <a href="<?php echo $oats['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
            <?php if($supergrainsproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="supergrains">
                <h4 class="sub-category">Supergrains</h4>
                <hr/>
                <?php foreach ($supergrainsproducts as $supergrains) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($supergrains['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($supergrains['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $supergrains['href']; ?>">
                            <div class="pro_img">
                                <?php if($supergrains['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $supergrains['rotator_image']; ?>" alt="<?php echo $supergrains['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $supergrains['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $supergrains['href']; ?>"><?php echo $supergrains['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($supergrains['options']) { ?>
    	                    <div class="options options-<?php echo $supergrains['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($supergrains['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $supergrains['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $supergrains['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $supergrains['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $supergrains['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $supergrains['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $supergrains['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($supergrains['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($supergrains['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$supergrains['special']) { ?>
                                                <span class="price-new price-new-<?php echo $supergrains['product_id']; ?>"><?php echo $supergrains['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $supergrains['product_id']; ?>"><?php echo $supergrains['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $supergrains['product_id']; ?>"><?php echo $supergrains['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($supergrains['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $supergrains['product_id']; ?>"><b> <?php echo $supergrains['coupon_discount']; ?> </b></span><span> <?php echo $supergrains['coupon'] ? 'Using code: <b>' . $supergrains['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $supergrains['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $supergrains['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $supergrains['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $supergrains['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $supergrains['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $supergrains['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $supergrains['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $supergrains['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $supergrains['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($supergrains['quantity'] > 0) { ?>
						        <a href="<?php echo $supergrains['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
        <?php } ?>
    </div>
</div>

<div class="row main-row" id="readyToEatBreakfast">
    <div class="container">
        <?php if ($flakesbixproducts || $muesliproducts || $granolaproducts || $oatmealproducts) { ?>
            <h3 class="center category-title readyToEatSection">Ready to Eat Breakfast</h3>
            <?php if($granolaproducts) { ?>
                <a class="hidden-xs hidden-sm" href="/index.php?route=checkout/cart&product_id=6465&option_id=1&option_value_id=363&coupon=granola15"><img alt="True Elements" class="img-responsive" src="<?php echo $chocolate_granola_450gm_desktop; ?>" style="margin-top: 25px;"></a>
                <a class="hidden-md hidden-xl hidden-lg" href="/index.php?route=checkout/cart&product_id=6465&option_id=1&option_value_id=363&coupon=granola15"><img alt="True Elements" class="img-responsive" src="<?php echo $chocolate_granola_450gm_mobile; ?>"></a>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="granola">
                <h4 class="sub-category">Granola</h4>
                <hr/>
                <?php foreach ($granolaproducts as $granola) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($granola['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($granola['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $granola['href']; ?>">
                            <div class="pro_img">
                                <?php if($granola['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $granola['rotator_image']; ?>" alt="<?php echo $granola['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $granola['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $granola['href']; ?>"><?php echo $granola['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($granola['options']) { ?>
    	                    <div class="options options-<?php echo $granola['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($granola['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $granola['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $granola['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $granola['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $granola['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $granola['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $granola['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($granola['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($granola['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$granola['special']) { ?>
                                                <span class="price-new price-new-<?php echo $granola['product_id']; ?>"><?php echo $granola['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $granola['product_id']; ?>"><?php echo $granola['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $granola['product_id']; ?>"><?php echo $granola['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($granola['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $granola['product_id']; ?>"><b> <?php echo $granola['coupon_discount']; ?> </b></span><span> <?php echo $granola['coupon'] ? 'Using code: <b>' . $granola['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $granola['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $granola['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $granola['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $granola['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $granola['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $granola['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $granola['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $granola['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $granola['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($granola['quantity'] > 0) { ?>
						        <a href="<?php echo $granola['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
            <?php if($muesliproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="muesli">
                    <a class="hidden-xs hidden-sm" href="/index.php?route=checkout/cart&product_id=6462&option_id=1&option_value_id=31&coupon=muesli15"><img alt="True Elements" class="img-responsive" src="<?php echo $crunchy_nuts_berries_muesli_desktop; ?>" style="margin-top: 25px;"></a>
                    <a class="hidden-md hidden-xl hidden-lg" href="/index.php?route=checkout/cart&product_id=6462&option_id=1&option_value_id=31&coupon=muesli15"><img alt="True Elements" class="img-responsive" src="<?php echo $crunchy_nuts_berries_muesli_mobile; ?>"></a>
                <h4 class="sub-category">Muesli</h4>
                <hr/>
                <?php foreach ($muesliproducts as $muesli) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($muesli['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($muesli['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $muesli['href']; ?>">
                            <div class="pro_img">
                                <?php if($muesli['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $muesli['rotator_image']; ?>" alt="<?php echo $muesli['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $muesli['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $muesli['href']; ?>"><?php echo $muesli['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($muesli['options']) { ?>
    	                    <div class="options options-<?php echo $muesli['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($muesli['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $muesli['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $muesli['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $muesli['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $muesli['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $muesli['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $muesli['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($muesli['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($muesli['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$muesli['special']) { ?>
                                                <span class="price-new price-new-<?php echo $muesli['product_id']; ?>"><?php echo $muesli['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $muesli['product_id']; ?>"><?php echo $muesli['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $muesli['product_id']; ?>"><?php echo $muesli['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($muesli['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $muesli['product_id']; ?>"><b> <?php echo $muesli['coupon_discount']; ?> </b></span><span> <?php echo $muesli['coupon'] ? 'Using code: <b>' . $muesli['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $muesli['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $muesli['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $muesli['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $muesli['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $muesli['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $muesli['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $muesli['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $muesli['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $muesli['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($muesli['quantity'] > 0) { ?>
						        <a href="<?php echo $muesli['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
            <?php if($flakesbixproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="flakesBix">
                    <a class="hidden-xs hidden-sm" href="/index.php?route=checkout/cart&product_id=6458&option_id=1&option_value_id=114"><img alt="True Elements" class="img-responsive" src="<?php echo $wheat_flakes_desktop; ?>" style="margin-top: 25px;"></a>
                    <a class="hidden-md hidden-xl hidden-lg" href="/index.php?route=checkout/cart&product_id=6458&option_id=1&option_value_id=114"><img alt="True Elements" class="img-responsive" src="<?php echo $wheat_flakes_mobile; ?>"></a>
                <h4 class="sub-category">Wholegrain High-Fibre Flakes</h4>
                <hr/>
                <?php foreach ($flakesbixproducts as $flakesbix) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($flakesbix['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($flakesbix['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $flakesbix['href']; ?>">
                            <div class="pro_img">
                                <?php if($flakesbix['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $flakesbix['rotator_image']; ?>" alt="<?php echo $flakesbix['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $flakesbix['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $flakesbix['href']; ?>"><?php echo $flakesbix['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($flakesbix['options']) { ?>
    	                    <div class="options options-<?php echo $flakesbix['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($flakesbix['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $flakesbix['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $flakesbix['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $flakesbix['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $flakesbix['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $flakesbix['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $flakesbix['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($flakesbix['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($flakesbix['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$flakesbix['special']) { ?>
                                                <span class="price-new price-new-<?php echo $flakesbix['product_id']; ?>"><?php echo $flakesbix['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $flakesbix['product_id']; ?>"><?php echo $flakesbix['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $flakesbix['product_id']; ?>"><?php echo $flakesbix['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($flakesbix['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $flakesbix['product_id']; ?>"><b> <?php echo $flakesbix['coupon_discount']; ?> </b></span><span> <?php echo $flakesbix['coupon'] ? 'Using code: <b>' . $flakesbix['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $flakesbix['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $flakesbix['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $flakesbix['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $flakesbix['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $flakesbix['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $flakesbix['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $flakesbix['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $flakesbix['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $flakesbix['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($flakesbix['quantity'] > 0) { ?>
						        <a href="<?php echo $flakesbix['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	            </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
            <?php if($oatmealproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="oatmeal">
                <h4 class="sub-category">Oatmeal</h4>
                <hr/>
                <?php foreach ($oatmealproducts as $oatmeal) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($oatmeal['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($oatmeal['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $oatmeal['href']; ?>">
                            <div class="pro_img">
                                <?php if($oatmeal['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $oatmeal['rotator_image']; ?>" alt="<?php echo $oatmeal['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $oatmeal['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $oatmeal['href']; ?>"><?php echo $oatmeal['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($oatmeal['options']) { ?>
    	                    <div class="options options-<?php echo $oatmeal['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($oatmeal['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $oatmeal['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $oatmeal['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $oatmeal['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $oatmeal['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $oatmeal['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $oatmeal['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($oatmeal['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($oatmeal['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$oatmeal['special']) { ?>
                                                <span class="price-new price-new-<?php echo $oatmeal['product_id']; ?>"><?php echo $oatmeal['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $oatmeal['product_id']; ?>"><?php echo $oatmeal['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $oatmeal['product_id']; ?>"><?php echo $oatmeal['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($oatmeal['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $oatmeal['product_id']; ?>"><b> <?php echo $oatmeal['coupon_discount']; ?> </b></span><span> <?php echo $oatmeal['coupon'] ? 'Using code: <b>' . $oatmeal['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $oatmeal['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $oatmeal['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $oatmeal['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $oatmeal['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $oatmeal['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $oatmeal['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $oatmeal['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $oatmeal['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $oatmeal['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($oatmeal['quantity'] > 0) { ?>
						        <a href="<?php echo $oatmeal['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
        <?php } ?>
    </div>
</div>

<div class="row main-row">
    <div class="container">
        <?php if ($seedsmixesproducts) { ?>
            <h3 class="center category-title seedsMixesSection">Seeds Mixes</h3>
            <?php if($seedsmixesproducts) { ?>
                <a class="hidden-xs hidden-sm" href="/index.php?route=checkout/cart&product_id=6451&option_id=1&option_value_id=80&coupon=seedsmix"><img alt="True Elements" class="img-responsive" src="<?php echo $seven_in_one_desktop; ?>" style="margin-top: 25px;"></a>
                <a class="hidden-md hidden-xl hidden-lg" href="/index.php?route=checkout/cart&product_id=6451&option_id=1&option_value_id=80&coupon=seedsmix"><img alt="True Elements" class="img-responsive" src="<?php echo $seven_in_one_mobile; ?>"></a>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="seedsMixes">
                <h4 class="sub-category">Seeds Mixes</h4>
                <hr/>
                <?php foreach ($seedsmixesproducts as $seedsmixes) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($seedsmixes['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($seedsmixes['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $seedsmixes['href']; ?>">
                            <div class="pro_img">
                                <?php if($seedsmixes['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $seedsmixes['rotator_image']; ?>" alt="<?php echo $seedsmixes['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $seedsmixes['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $seedsmixes['href']; ?>"><?php echo $seedsmixes['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($seedsmixes['options']) { ?>
    	                    <div class="options options-<?php echo $seedsmixes['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($seedsmixes['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $seedsmixes['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $seedsmixes['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $seedsmixes['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $seedsmixes['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $seedsmixes['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $seedsmixes['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($seedsmixes['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($seedsmixes['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$seedsmixes['special']) { ?>
                                                <span class="price-new price-new-<?php echo $seedsmixes['product_id']; ?>"><?php echo $seedsmixes['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $seedsmixes['product_id']; ?>"><?php echo $seedsmixes['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $seedsmixes['product_id']; ?>"><?php echo $seedsmixes['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($seedsmixes['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $seedsmixes['product_id']; ?>"><b> <?php echo $seedsmixes['coupon_discount']; ?> </b></span><span> <?php echo $seedsmixes['coupon'] ? 'Using code: <b>' . $seedsmixes['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $seedsmixes['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $seedsmixes['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $seedsmixes['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $seedsmixes['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $seedsmixes['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $seedsmixes['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $seedsmixes['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $seedsmixes['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $seedsmixes['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($seedsmixes['quantity'] > 0) { ?>
						        <a href="<?php echo $seedsmixes['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
        <?php } ?>
    </div>
</div>

<div class="row main-row" id="roastedSnacks">
    <div class="container">
        <?php if ($otgsnacksproducts) { ?>
            <h3 class="center category-title roastedSnacksSection">Roasted Snacks</h3>
            
            <?php if($otgsnacksproducts) { ?>
            <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="onTheGoSnacks">
                <a class="hidden-xs hidden-sm" href="/index.php?route=checkout/cart&product_id=6413&option_id=1&option_value_id=79"><img alt="True Elements" class="img-responsive" src="<?php echo $chilli_masala_pumpkin_desktop; ?>" style="margin-top: 25px;"></a>
                <a class="hidden-md hidden-xl hidden-lg" href="/index.php?route=checkout/cart&product_id=6413&option_id=1&option_value_id=79"><img alt="True Elements" class="img-responsive" src="<?php echo $chilli_masala_pumpkin_mobile; ?>"></a>
                <h4 class="sub-category">On the Go Snacks</h4>
                <hr/>
                <?php foreach ($otgsnacksproducts as $otgsnacks) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($otgsnacks['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($otgsnacks['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $otgsnacks['href']; ?>">
                            <div class="pro_img">
                                <?php if($otgsnacks['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $otgsnacks['rotator_image']; ?>" alt="<?php echo $otgsnacks['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $otgsnacks['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $otgsnacks['href']; ?>"><?php echo $otgsnacks['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($otgsnacks['options']) { ?>
    	                    <div class="options options-<?php echo $otgsnacks['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($otgsnacks['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $otgsnacks['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $otgsnacks['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $otgsnacks['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $otgsnacks['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $otgsnacks['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $otgsnacks['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($otgsnacks['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($otgsnacks['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$otgsnacks['special']) { ?>
                                                <span class="price-new price-new-<?php echo $otgsnacks['product_id']; ?>"><?php echo $otgsnacks['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $otgsnacks['product_id']; ?>"><?php echo $otgsnacks['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $otgsnacks['product_id']; ?>"><?php echo $otgsnacks['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($otgsnacks['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $otgsnacks['product_id']; ?>"><b> <?php echo $otgsnacks['coupon_discount']; ?> </b></span><span> <?php echo $otgsnacks['coupon'] ? 'Using code: <b>' . $otgsnacks['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $otgsnacks['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $otgsnacks['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $otgsnacks['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $otgsnacks['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $otgsnacks['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $otgsnacks['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $otgsnacks['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $otgsnacks['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $otgsnacks['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($otgsnacks['quantity'] > 0) { ?>
						        <a href="<?php echo $otgsnacks['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
        <?php } ?>
    </div>
</div>


<div class="row main-row" id="rawAndRoastedSeeds">
    <div class="container">
        <?php if ($rawseedsproducts || $roastedseedsproducts) { ?>
            <h3 class="center category-title rawAndRoastedSeedsSection">Raw & Roasted Seeds</h3>
            <?php if($rawseedsproducts) { ?>
            <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="rawSeeds">
                <h4 class="sub-category">Healthy Seeds</h4>
                <hr/>
                <?php foreach ($rawseedsproducts as $rawseeds) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($rawseeds['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($rawseeds['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $rawseeds['href']; ?>">
                            <div class="pro_img">
                                <?php if($rawseeds['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $rawseeds['rotator_image']; ?>" alt="<?php echo $rawseeds['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $rawseeds['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $rawseeds['href']; ?>"><?php echo $rawseeds['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($rawseeds['options']) { ?>
    	                    <div class="options options-<?php echo $rawseeds['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($rawseeds['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $rawseeds['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $rawseeds['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $rawseeds['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $rawseeds['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $rawseeds['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $rawseeds['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($rawseeds['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($rawseeds['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$rawseeds['special']) { ?>
                                                <span class="price-new price-new-<?php echo $rawseeds['product_id']; ?>"><?php echo $rawseeds['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $rawseeds['product_id']; ?>"><?php echo $rawseeds['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $rawseeds['product_id']; ?>"><?php echo $rawseeds['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($rawseeds['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $rawseeds['product_id']; ?>"><b> <?php echo $rawseeds['coupon_discount']; ?> </b></span><span> <?php echo $rawseeds['coupon'] ? 'Using code: <b>' . $rawseeds['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $rawseeds['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $rawseeds['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $rawseeds['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $rawseeds['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $rawseeds['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $rawseeds['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $rawseeds['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $rawseeds['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $rawseeds['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($rawseeds['quantity'] > 0) { ?>
						        <a href="<?php echo $rawseeds['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
            <?php if($roastedseedsproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="roastedSeeds">
                <h4 class="sub-category">Roasted Seeds</h4>
                <hr/>
                <?php foreach ($roastedseedsproducts as $roastedseeds) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($roastedseeds['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($roastedseeds['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $roastedseeds['href']; ?>">
                            <div class="pro_img">
                                <?php if($roastedseeds['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $roastedseeds['rotator_image']; ?>" alt="<?php echo $roastedseeds['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $roastedseeds['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $roastedseeds['href']; ?>"><?php echo $roastedseeds['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($roastedseeds['options']) { ?>
    	                    <div class="options options-<?php echo $roastedseeds['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($roastedseeds['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $roastedseeds['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $roastedseeds['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $roastedseeds['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $roastedseeds['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $roastedseeds['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $roastedseeds['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($roastedseeds['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($roastedseeds['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$roastedseeds['special']) { ?>
                                                <span class="price-new price-new-<?php echo $roastedseeds['product_id']; ?>"><?php echo $roastedseeds['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $roastedseeds['product_id']; ?>"><?php echo $roastedseeds['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $roastedseeds['product_id']; ?>"><?php echo $roastedseeds['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($roastedseeds['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $roastedseeds['product_id']; ?>"><b> <?php echo $roastedseeds['coupon_discount']; ?> </b></span><span> <?php echo $roastedseeds['coupon'] ? 'Using code: <b>' . $roastedseeds['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $roastedseeds['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $roastedseeds['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $roastedseeds['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $roastedseeds['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $roastedseeds['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $roastedseeds['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $roastedseeds['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $roastedseeds['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $roastedseeds['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($roastedseeds['quantity'] > 0) { ?>
						        <a href="<?php echo $roastedseeds['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
        <?php } ?>
    </div>
</div>


<div class="row main-row">
    <div class="container">
        <?php if ($othersproducts) { ?>
            <h3 class="center category-title othersSection">For Your Sweet Tooth</h3>
            <!--a class="hidden-xs hidden-sm" href="/index.php?route=checkout/cart&product_id=6469&option_id=1&option_value_id=115"><img alt="True Elements" class="img-responsive" src="<?php echo $dried_blueberries_desktop; ?>" style="margin-top: 25px;"></a>
            <a class="hidden-md hidden-xl hidden-lg" href="/index.php?route=checkout/cart&product_id=6469&option_id=1&option_value_id=115"><img alt="True Elements" class="img-responsive" src="<?php echo $dried_blueberries_mobile; ?>"></a-->
            <?php if($othersproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="others">
                <h4 class="sub-category">For Your Sweet Tooth</h4>
                <hr/>
                <?php foreach ($othersproducts as $others) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($others['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($others['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $others['href']; ?>">
                            <div class="pro_img">
                                <?php if($others['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $others['rotator_image']; ?>" alt="<?php echo $others['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $others['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $others['href']; ?>"><?php echo $others['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($others['options']) { ?>
    	                    <div class="options options-<?php echo $others['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($others['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $others['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $others['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $others['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $others['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $others['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $others['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($others['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($others['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$others['special']) { ?>
                                                <span class="price-new price-new-<?php echo $others['product_id']; ?>"><?php echo $others['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $others['product_id']; ?>"><?php echo $others['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $others['product_id']; ?>"><?php echo $others['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($others['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $others['product_id']; ?>"><b> <?php echo $others['coupon_discount']; ?> </b></span><span> <?php echo $others['coupon'] ? 'Using code: <b>' . $others['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $others['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $others['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $others['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $others['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $others['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $others['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $others['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $others['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $others['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($others['quantity'] > 0) { ?>
						        <a href="<?php echo $others['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
        <?php } ?>
    </div>
</div>

<div class="row main-row">
    <div class="container">
        <?php if ($teaproducts) { ?>
            <h3 class="center category-title teaSection">Tea</h3>
            <?php if($teaproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="tea">
                <h4 class="sub-category">Tea</h4>
                <hr/>
                <?php foreach ($teaproducts as $tea) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($tea['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($tea['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $tea['href']; ?>">
                            <div class="pro_img">
                                <?php if($tea['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $tea['rotator_image']; ?>" alt="<?php echo $tea['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $tea['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $tea['href']; ?>"><?php echo $tea['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($tea['options']) { ?>
    	                    <div class="options options-<?php echo $tea['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($tea['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $tea['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $tea['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $tea['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $tea['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $tea['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $tea['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($tea['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($tea['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$tea['special']) { ?>
                                                <span class="price-new price-new-<?php echo $tea['product_id']; ?>"><?php echo $tea['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $tea['product_id']; ?>"><?php echo $tea['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $tea['product_id']; ?>"><?php echo $tea['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($tea['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $tea['product_id']; ?>"><b> <?php echo $tea['coupon_discount']; ?> </b></span><span> <?php echo $tea['coupon'] ? 'Using code: <b>' . $tea['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $tea['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $tea['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $tea['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $tea['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $tea['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $tea['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $tea['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $tea['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $tea['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($tea['quantity'] > 0) { ?>
						        <a href="<?php echo $tea['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>

<div class="row main-row">
    <div class="container">
        <?php if ($giftproducts) { ?>
            <h3 class="center category-title teaSection">Gift Certificates</h3>
            <?php if($giftproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="gift">
                <h4 class="sub-category">Gift Certificates</h4>
                <hr/>
                <?php foreach ($giftproducts as $gift) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($gift['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($gift['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $gift['href']; ?>">
                            <div class="pro_img">
                                <?php if($gift['rotator_image']){ ?>
									<img alt="True Elements" class="image2 product-image" src="<?php echo $gift['rotator_image']; ?>" alt="<?php echo $gift['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $gift['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $gift['href']; ?>"><?php echo $gift['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($gift['options']) { ?>
    	                    <div class="options options-<?php echo $gift['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($gift['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $gift['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $gift['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $gift['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $gift['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $gift['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $gift['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($gift['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($gift['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$gift['special']) { ?>
                                                <span class="price-new price-new-<?php echo $gift['product_id']; ?>"><?php echo $gift['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $gift['product_id']; ?>"><?php echo $gift['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $gift['product_id']; ?>"><?php echo $gift['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($gift['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $gift['product_id']; ?>"><b> <?php echo $gift['coupon_discount']; ?> </b></span><span> <?php echo $gift['coupon'] ? 'Using code: <b>' . $gift['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $gift['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $gift['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $gift['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $gift['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $gift['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $gift['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $gift['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $gift['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $gift['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($gift['quantity'] > 0) { ?>
						        <a href="<?php echo $gift['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            
        <?php } ?>
    </div>
</div>

<div class="row main-row">
    <div class="container">
        <?php if ($giftpacksproducts || $sspproducts) { ?>
            <h3 class="center category-title comboSection">Value Packs</h3>
            
            <?php if($giftpacksproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="giftpacks">
                <h4 class="sub-category">Gift Packs</h4>
                <hr/>
                <?php foreach ($giftpacksproducts as $giftpacksproduct) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($giftpacksproduct['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($giftpacksproduct['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $giftpacksproduct['href']; ?>">
                            <div class="pro_img">
                                <?php if($giftpacksproduct['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $giftpacksproduct['rotator_image']; ?>" alt="<?php echo $giftpacksproduct['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $giftpacksproduct['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $giftpacksproduct['href']; ?>"><?php echo $giftpacksproduct['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($giftpacksproduct['options']) { ?>
    	                    <div class="options options-<?php echo $giftpacksproduct['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($giftpacksproduct['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $giftpacksproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $giftpacksproduct['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $giftpacksproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $giftpacksproduct['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $giftpacksproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $giftpacksproduct['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($giftpacksproduct['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($giftpacksproduct['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$giftpacksproduct['special']) { ?>
                                                <span class="price-new price-new-<?php echo $giftpacksproduct['product_id']; ?>"><?php echo $giftpacksproduct['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $giftpacksproduct['product_id']; ?>"><?php echo $giftpacksproduct['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $giftpacksproduct['product_id']; ?>"><?php echo $giftpacksproduct['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($giftpacksproduct['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $giftpacksproduct['product_id']; ?>"><b> <?php echo $giftpacksproduct['coupon_discount']; ?> </b></span><span> <?php echo $giftpacksproduct['coupon'] ? 'Using code: <b>' . $giftpacksproduct['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $giftpacksproduct['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $giftpacksproduct['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $giftpacksproduct['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $giftpacksproduct['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $giftpacksproduct['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $giftpacksproduct['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $giftpacksproduct['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $giftpacksproduct['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $giftpacksproduct['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($giftpacksproduct['quantity'] > 0) { ?>
						        <a href="<?php echo $giftpacksproduct['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
            <?php if($sspproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="ssp">
                <h4 class="sub-category">Super Saver Packs</h4>
                <hr/>
                <?php foreach ($sspproducts as $sspproduct) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($sspproduct['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($sspproduct['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $sspproduct['href']; ?>">
                            <div class="pro_img">
                                <?php if($sspproduct['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $sspproduct['rotator_image']; ?>" alt="<?php echo $sspproduct['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $sspproduct['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $sspproduct['href']; ?>"><?php echo $sspproduct['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($sspproduct['options']) { ?>
    	                    <div class="options options-<?php echo $sspproduct['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($sspproduct['options'] AS $option) { ?>
    	                            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $sspproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $sspproduct['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $sspproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $sspproduct['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $sspproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $sspproduct['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($sspproduct['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($sspproduct['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$sspproduct['special']) { ?>
                                                <span class="price-new price-new-<?php echo $sspproduct['product_id']; ?>"><?php echo $sspproduct['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $sspproduct['product_id']; ?>"><?php echo $sspproduct['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $sspproduct['product_id']; ?>"><?php echo $sspproduct['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($sspproduct['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $sspproduct['product_id']; ?>"><b> <?php echo $sspproduct['coupon_discount']; ?> </b></span><span> <?php echo $sspproduct['coupon'] ? 'Using code: <b>' . $sspproduct['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $sspproduct['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $sspproduct['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $sspproduct['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $sspproduct['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $sspproduct['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $sspproduct['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $sspproduct['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $sspproduct['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $sspproduct['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($sspproduct['quantity'] > 0) { ?>
						        <a href="<?php echo $sspproduct['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>

<!--<div class="row main-row">
    <div class="container">
        <?php if ($wcbsproducts) { ?>
            <h3 class="center category-title wcbsSection">Will Come Back Soon</h3>
            <?php if($wcbsproducts) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="wcbs">
                <h4 class="sub-category">Be Right Back</h4>
                <hr/>
                <?php foreach ($wcbsproducts as $wcbs) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					    <?php if($wcbs['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($wcbs['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $wcbs['href']; ?>">
                            <div class="pro_img">
                                <?php if($wcbs['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $wcbs['rotator_image']; ?>" alt="<?php echo $wcbs['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $wcbs['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $wcbs['href']; ?>"><?php echo $wcbs['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($wcbs['options']) { ?>
    	                    <div class="options options-<?php echo $wcbs['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($wcbs['options'] AS $option) { ?>
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $wcbs['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $wcbs['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $wcbs['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $wcbs['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($wcbs['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($wcbs['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$wcbs['special']) { ?>
                                                <span class="price-new price-new-<?php echo $wcbs['product_id']; ?>"><?php echo $wcbs['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $wcbs['product_id']; ?>"><?php echo $wcbs['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $wcbs['product_id']; ?>"><?php echo $wcbs['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($wcbs['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $wcbs['product_id']; ?>"><b> <?php echo $wcbs['coupon_discount']; ?> </b></span><span> <?php echo $wcbs['coupon'] ? 'Using code: <b>' . $wcbs['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $wcbs['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $wcbs['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $wcbs['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $wcbs['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $wcbs['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $wcbs['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $wcbs['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $wcbs['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $wcbs['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($wcbs['quantity'] > 0) { ?>
						        <a href="<?php echo $wcbs['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>-->

<!--/div></div-->
<!-- main close -->
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left" ><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center">' + json['success'] + '</div></div>';
                    
                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('.price-new-' + product_id).html(json.new_price.special);
					$('.price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.coupon) {
					    $('.price-coupon-' + product_id).html(json.new_price.coupon); 
					}
					
					if(json.new_price.special) {
					    $('.price-new-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('.price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
        
        /*$(function() {
            let isMobile = window.matchMedia("only screen and (max-width: 1000px)").matches;

            if (isMobile) {
                $(".readyToEatBanner").click(function() {
                    $("html, body").animate({
                        scrollTop: $(".readyToEatSection").offset().top-20
                    }, 1000);
                });
                $(".readyToCookBanner").click(function() {
                    $("html, body").animate({
                        scrollTop: $(".readyToCookSection").offset().top-20
                    }, 1000);
                });
                $(".seedsMixesBanner").click(function() {
                    $("html, body").animate({
                        scrollTop: $(".seedsMixesSection").offset().top-20
                    }, 1000);
                });
                $(".rawAndroastedSeedsBanner").click(function() {
                    $("html, body").animate({
                        scrollTop: $(".rawAndroastedSeedsSection").offset().top-20
                    }, 1000);
                });
                
            } else {
                $(".readyToEatBanner").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 100;
                    } else {
                        var offsetValue = 210;
                    }
                    $("html, body").animate({
                        scrollTop: $(".readyToEatSection").offset().top - offsetValue
                    }, 1000);
                });
                $(".readyToCookBanner").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 100;
                    } else {
                        var offsetValue = 210;
                    }
                    $("html, body").animate({
                        scrollTop: $(".readyToCookSection").offset().top - offsetValue
                    }, 1000);
                });
                $(".seedsMixesBanner").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 100;
                    } else {
                        var offsetValue = 210;
                    }
                    $("html, body").animate({
                        scrollTop: $(".seedsMixesSection").offset().top - offsetValue
                    }, 1000);
                });
                $(".rawAndroastedSeedsBanner").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 100;
                    } else {
                        var offsetValue = 210;
                    }
                    $("html, body").animate({
                        scrollTop: $(".rawAndroastedSeedsSection").offset().top - offsetValue
                    }, 1000);
                });
            }
        });*/
        $(function() {
            let isMobile = window.matchMedia("only screen and (max-width: 1000px)").matches;

            if (isMobile) {
                $(".giftBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#gift").offset().top-100
                    }, 1000);
                });
                
                $(".teaBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#tea").offset().top-100
                    }, 1000);
                });
                
                $(".comboBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#combo").offset().top-100
                    }, 1000);
                });
                
                $(".giftspackBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#giftpacks").offset().top-100
                    }, 1000);
                });
                
                $(".sspBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#ssp").offset().top-100
                    }, 1000);
                });
                
                $(".wcbsBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#wcbs").offset().top-100
                    }, 1000);
                });
                
                $(".flakesBixBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#flakesBix").offset().top-100
                    }, 1000);
                });
                $(".muesliBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#muesli").offset().top-100
                    }, 1000);
                });
                $(".granolaBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#granola").offset().top-100
                    }, 1000);
                });
                $(".oatmealBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#oatmeal").offset().top-100
                    }, 1000);
                });
                $(".oatsBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#oats").offset().top-100
                    }, 1000);
                });
                $(".supergrainsBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#supergrains").offset().top-100
                    }, 1000);
                });
                $(".seedsMixesBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#seedsMixes").offset().top-100
                    }, 1000);
                });
                $(".otgBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#onTheGoSnacks").offset().top-100
                    }, 1000);
                });
                $(".rawSeedsBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#rawSeeds").offset().top-100
                    }, 1000);
                });
                $(".roastedSeedsBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#roastedSeeds").offset().top-100
                    }, 1000);
                });
                $(".teaBookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#tea").offset().top-100
                    }, 1000);
                });
            } else {
                $(".giftBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#gift").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".teaBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#tea").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".comboBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#combo").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".giftspackBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#giftpacks").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".sspBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#ssp").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".comboBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#combo").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".wcbsBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#wcbs").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".flakesBixBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#flakesBix").offset().top - offsetValue
                    }, 1000);
                });
                $(".muesliBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#muesli").offset().top - offsetValue
                    }, 1000);
                });
                $(".granolaBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#granola").offset().top - offsetValue
                    }, 1000);
                });
                $(".oatmealBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#oatmeal").offset().top - offsetValue
                    }, 1000);
                });
                $(".oatsBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#oats").offset().top - offsetValue
                    }, 1000);
                });
                $(".supergrainsBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#supergrains").offset().top - offsetValue
                    }, 1000);
                });
                $(".seedsMixesBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#seedsMixes").offset().top - offsetValue
                    }, 1000);
                });
                $(".otgBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#onTheGoSnacks").offset().top - offsetValue
                    }, 1000);
                });
                $(".rawSeedsBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#rawSeeds").offset().top - offsetValue
                    }, 1000);
                });
                $(".roastedSeedsBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#roastedSeeds").offset().top - offsetValue
                    }, 1000);
                });
                $(".othersBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#others").offset().top - offsetValue
                    }, 1000);
                });
                $(".teaBookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#tea").offset().top - offsetValue
                    }, 1000);
                });
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.options .radio input[type="radio"]').click(function() {
        $('.options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>

  <!--- POSTCODE --->     
  <style>
    #msg1 p{margin:0;}
    #msg1{color:#000}
    #msg1 .fa-2x{font-size:2em!important;}
  </style>
  <script type="text/javascript">
    $('#pincheck1').submit(function (e) {
     e.preventDefault();
     getdata1();
   });
    function getdata1(){
      var pin = $("#pin1").val();
      if(pin != ''){
        $.ajax({
          type: "POST",
          url: "?route=product/product/pinCheck",
          data: {pincode: pin},
          dataType : "text"
        }).done(function( result )
        {
          $("#msg1").show();
          $("#msg1").html( result );
          $("#pincheck1").hide();
          $("#show_message").hide();
          
        });
      }
      else{
        $("#show_message").show();
      }
    }
    function showform1(){
      $("#msg1").hide();
      $("#pincheck1").show();
    }
  </script>
  <!--- POSTCODE --->
<?php echo $footer; ?>