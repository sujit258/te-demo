<style>
* {
  box-sizing: border-box;
}
.html{
  overflow: auto;
}

.myInput {
  background-image: url('/css/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 50%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}


#myTable {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

#myTable th, td {
    text-align: left;
    padding: 16px;
}

.breadcrumb-container1 {

    padding: 30px 0;
    text-align: center;
    margin-bottom: 50px;
    background: #f1f1f1;
    border-top: 1px solid #d8d8d8;
    display: inline-block;
    width: 100%;
    height: 17%;

}


</style>
<?php echo $header; ?>
<div class="breadcrumb-container1">

	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
</div>
<div class="container">
    	<center><h3 ><?php echo $heading_title; ?></h3></center><br>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	<?php if ($thumb || $description) { ?>
		<div class="category-info">
			<?php if ($thumb) { ?>
				<div class="category-img"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
			<?php } ?>
			<?php if ($description) { ?>
			<div class="category-des"><?php echo $description; ?></div>
			<?php } ?>
		</div>
    <?php } ?>
    <?php if ($categories) { ?>
		<h3><?php echo $text_refine; ?></h3>
		<?php if (count($categories) <= 5) { ?>
		<div class="row">
			<div class="col-sm-3">
			  <ul>
				<?php foreach ($categories as $category) { ?>
				<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
				<?php } ?>
			  </ul>
			</div>
		</div>
		<?php } else { ?>
		<div class="row">
			<?php foreach (array_chunk($categories, ceil(count($categories)/4)) as $categories) { ?>
			<div class="col-sm-3">
			  <ul>
				<?php foreach ($categories as $category) { ?>
				<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
				<?php } ?>
			  </ul>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	<?php } ?>
	<div class="custom-category">
	<?php if ($products) { ?>
<center>
      <input type="text" id="myInput" class="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
</center>
<br>
  <div class="row">

          <div id="content" style="min-height: 200px;" >  
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <table id="myTable" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th style="width: 108px;">Image</th>
                  <th style="width: 450px;">Product Name</th>
                  <th>Product price</th>
                  <th> Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($products as $product) { ?>
                <tr style="height: 20px;">
                  <td class="text-center" style="width: 12px;"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive"/></a></td>
                  <td class="text-center"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
               </td>
               <td>
                  	<div class="price-label">
							<div class="box-price">
							   
								<?php if ($product['price']) { ?>
									<p class="price">
									  <?php if (!$product['special']) { ?>
									  <?php echo $product['price']; ?>
									  <?php } else { ?>
									  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
									  	
									  <?php } ?><?php } ?>
									
							</div>
						</div>
                  </td> 
                 <td>	
                 
                 <div class="cart">
									<button class="button btn-cart" type="button" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><span><span><?php echo $button_cart; ?></span></span></button>
								</div>
                 
                 </td>              
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
  </div>
 
    <?php } ?>
	</div>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<script>
  function myFunction() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[1];
      if (td) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }       
    }
  }
</script>


<?php echo $footer; ?>
