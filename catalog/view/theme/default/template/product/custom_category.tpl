<style>
.reviews1 {
    /*background: #fff;*/
    padding: 15px 20px;
    margin: 10px 0;
    border: 1px solid #ddd;
    display: flex;
    width: 688px;
}
.img-circle {
    border-radius: 50%;
}
.reviews-avatar {
    background: #f49a25;
    color: #fff;
    height: 50px;
    width: 50px;
    text-align: center;
    display: table;
    float: left;
}
.p0 {
    padding-left: 0;
    padding-right: 0;
}
.reviews1 .reviews-wrap {
    margin-left: 30px;
}
.reviews-author1 {
    display: inline;
}
.reviews-avatar i {
    font-size: 25px;
    display: table-cell;
    vertical-align: middle;
}
.reviews-wrap .fa {
    color: #f49a25;
}
.individual-rating1 .fa {
    height: 15px;
}
.fa-calendar {
    color: #000 !important;
    margin-right: 10px !important;
}

@media(min-width:768px) {
    .individual-rating1 {
        margin-top: 5px;
    }
    .reviews-text1 {
        margin-top: 15px;
    }
    .reviews-date1 {
        float: right;
    }
}
body {
    font-size: 15px;
}
 .disc1 li {
    margin: 0px;
    list-style-type: disc !important;
}
.product-description {
    margin-top: 5px;
    margin-bottom: 5px;
    font-size: 15px;
}
 body {
    font-size: 15px;
}
.product-banner, .product-1, .product-2 {
    border: 2px solid #d5dbdb;
    background: rgba(135, 149, 150, 0.095);
}
.product-1, .product-2 {
    margin-top: 0px!important;
    min-height: 640px;
    max-height: 580px;
    height: 100%;
}
.product-text {
    height: 100%;
    min-height: 375px;
    padding-right: 15px;
}
.product-banner, .product-data {
    min-height: 640px!important;
}
.product-banner {
    background-repeat: no-repeat;
    background-size: 101%, 100%;
}
.overlay {
    min-height: 190px;
    background: rgba(236, 238, 239, 1);
    padding: 15px;
    margin: 15px;
    width: 49%;
    color: #002f36;
}
body h4 {
    font-weight: 500;
    font-size: 17px;
    line-height: 22px;
    color: #002f36;
}
body h1 {
    font-weight: 700;
    color: #002f36;
    font-size: 25px;
}
.product-image img {
    margin-top: 10%;
}
.product-image {
    min-height: 375px;
    padding-left: 0px;
}
.top-banner {
    margin-top: 0!important;
    margin-bottom: 25px!important;
}
.top-banner img {
    width: 100%;
}
.breadcrumb {
    margin-bottom: 20px!important;
}
.top-image img {
    width: 100%;
}
.carousel-control span {
    font-size: 30px;
}
/*.top-banners {
    margin-bottom: 25px;
}*/
.content-vertical-center {
    margin-top: 15%;
    margin-left: 15px;
}
.buttons .btn-block {
    padding: 10px 16px!important;
}
.buttons .btn-block:active, .buttons .btn-block:focus {
    background: #f49a25!important;
}
@media(max-width:1000px) {
    .top-msg h2 {
        font-size: 17px!important;
    }
    .product-banner {
        min-height: 329px !important;
        background-size: 100%, 100%!important;
    }
    body h4 {
        font-size: 15px!important;
    }
    body h1 {
        font-size: 17px!important;
    }
    .overlay {
        min-height: 160px;
        width: 70%;
    }
    .product-image img {
        margin-top: 30%;
    }
    .product-image {
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .product-1, .product-2 {
        max-height: 495px;
        min-height: 320px;
        height: auto!important;
    }
    .product-data {
        min-height: 1px !important;
    }
    .product-text, .product-image {
        min-height: 270px;
    }
    .product-text {
        height: auto!important;
    }
    .buttons .btn-block {
        width: 38%!important;
        padding: 7px 16px;
    }
    .content-vertical-center {
        margin-top: 0!important;
        margin-left: 0!important;
    }
    #cart #cart-total {
        top: -5px;
    }
}
    #cart #cart-total {
        top: -6px;
    }
    
</style>

<style>
    .options {
        min-height: 55px;
    }
    .product-options {
        margin-top: 10px;
        text-align: center;
        float: left;
        background: #fff!important;
    }
    
    .des-container {
        margin-top: 2%;
    }
    
    @media(max-width: 1000px) {
        .radio label span {
            font-size: 12px!important;
        }
        .des-container .options .radio {
            margin: 0px 0px 5px -2px!important;
        }
        .options {
            min-height: 55px!important;
        }
        
        .buttons .btn-block { 
            margin-top: -5%;
            margin-bottom: 4%;
        }
    }
    .des-container {
        padding: 0!important;
    }
    @media(min-width: 1000px) {
        .des-container {
            min-height: 200px;
        }
    }
    
    
    .des-container .options .radio input {
        display: none !important;
    }
    .des-container .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .des-container .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    .des-container .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    
    .image1, .image2 {
        padding: 5%!important;
    }
     @media(min-width:741px){ 
        .options {
            min-height: 55px !important;
        }
   }
   
   @media(max-width:740px){
        .options {
            min-height: 55px !important;
        }
        .reviews1{
            display: none;
            width: 294px;
        }
   } 
   
   .des-container {
        min-height: 48px !important;
    }
    
    .te-lab-desc {
        font-size: 15px;
        line-height: 20px;
        text-align: justify; 
    }
</style> 

<?php echo $header; ?> 

<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>


<div class="container-fluid">
    <div class="row top-banner hidden-xs hidden-sm">
        <img src="/image/catalog/New/jowar-flakes-desktop.jpg" class="img-responsive" />
    </div>
    <div class="row top-banner hidden-md hidden-xl hidden-lg">
        <img src="/image/catalog/New/lab/top-banner-mobile.jpg" class="img-responsive" />
    </div>
    
    <div class="row top-banners">
	    <div class="col-md-12 col-xs-12 col-sm-12 top-image" style="background: #f7f7f7;padding-bottom: 12px;">
	        
	        <div class="top-msg" >
                <h2 class="text-center" style=" font-size: 22px;line-height: 1.3;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </b>
                </h2>
            </div>

	    </div>
	     
    </div>
    <?php if ($products) { ?>
    <input type="hidden" name="product_id" value="" />
    <?php $count=0; ?>
    <?php foreach ($products as $product) { ?>
    <br>
    <?php $remainder = $count % 2; ?>
     
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <?php if ($remainder == 0) { ?> 
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('<?php echo $product['custom_image']; ?>');">
                </div> 
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <a href="<?php echo $product['href']; ?>"  target="_blank"><h1><?php echo $product['name']; ?></h1></a> 
                            <h4><?php echo $product['short_description']; ?></h1></h4>
                            <h1><?php if (!$product['special']) { ?>
                                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } else { ?>
								        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
						                <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } ?></h1>
								  
								
                            <!-- options -->
                            <div class="des-container">
                            <?php $is_optionqty = 1; ?>
						<?php if($product['options']) { ?>
						    <div class="options options-<?php echo $product['product_id']; ?>">
						        <?php foreach($product['options'] AS $option) { ?>
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if($option_value['is_default'] == 1) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked-option'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
                                        <?php } ?>
						        <?php } ?> 
						    </div>
						<?php } else { ?>
						    <div class="options <?php if($category_id == 586) { echo 'hidden-md hidden-xl hidden-lg hidden-xs hidden-sm';} ?>"></div>
						    <?php $is_optionqty = 0; ?>
						<?php } ?>
						
                        </div>
                            <!-- options -->
                            
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a onclick ="addtocart(<?php echo $product['product_id']; ?>)" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Add to Cart</a>
                            </div> 
                            <?php foreach($product['reviews'] AS $review) { ?>
                            <?php if ($product['product_id'] ==  $review['product_id']) { ?>        
                            <div class="review" style="margin-top: 28%;"> 
                              <div class="reviews1">
                                <div class="img-circle reviews-avatar"><i class="fa fa-user"></i></div>
                                <div class="reviews-wrap col-md-12 col-xs-12 col-sm-12 p0">
                                  <h4 class="reviews-author1"><?php echo $review['author']; ?></h4>
                                  <h4 class="reviews-date1 hidden-xs hidden-sm"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
                                  <div class="individual-rating1">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                      <?php if ($review['rating'] < $i) { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } else { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } ?>
                                      <?php } ?>
                                  </div>
                                  <h4 class="reviews-date1 hidden-md hidden-xl hidden-lg"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
                                  <p class="reviews-text1"><?php echo $review['text']; ?></p>
                                </div>
                              </div>
                            </div>
                            <?php } ?>
                           <?php } ?>  
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <a href="<?php echo $product['href']; ?>"  target="_blank"><img src="<?php echo $product['thumb']; ?>" class="img-responsive" /></a>
                    </div>
                    
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a onclick ="addtocart(<?php echo $product['product_id']; ?>)" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Add to Cart</a>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <a href="<?php echo $product['href']; ?>" target="_blank"><h1><?php echo $product['name']; ?></h1></a>
                            <h4><?php echo $product['short_description']; ?></h1></h4>
                            <h1><?php if (!$product['special']) { ?>
                                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } else { ?>
								        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
						                <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } ?></h1>
								  
                            <!-- options -->
                            <div class="des-container">
                            <?php $is_optionqty = 1; ?>
						<?php if($product['options']) { ?>
						    <div class="options options-<?php echo $product['product_id']; ?>">
						        <?php foreach($product['options'] AS $option) { ?>
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
						        <?php } ?> 
						    </div>
						<?php } else { ?>
						    <div class="options <?php if($category_id == 586) { echo 'hidden-md hidden-xl hidden-lg hidden-xs hidden-sm';} ?>"></div>
						    <?php $is_optionqty = 0; ?>
						<?php } ?>
                        </div>
                            <!-- options -->
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a onclick ="addtocart(<?php echo $product['product_id']; ?>)" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Add to Cart</a>
                            </div>
                            <?php foreach($product['reviews'] AS $review) { ?>
                            <?php if ($product['product_id'] ==  $review['product_id']) { ?>        
                            <div class="review" style="margin-top: 28%;"> 
                              <div class="reviews1">
                                <div class="img-circle reviews-avatar"><i class="fa fa-user"></i></div>
                                <div class="reviews-wrap col-md-12 col-xs-12 col-sm-12 p0">
                                  <h4 class="reviews-author1"><?php echo $review['author']; ?></h4>
                                  <h4 class="reviews-date1 hidden-xs hidden-sm"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
                                  <div class="individual-rating1">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                      <?php if ($review['rating'] < $i) { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } else { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } ?>
                                      <?php } ?>
                                  </div>
                                  <h4 class="reviews-date1 hidden-md hidden-xl hidden-lg"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
                                  <p class="reviews-text1"><?php echo $review['text']; ?></p>
                                </div>
                              </div>
                            </div>
                            <?php } ?>
                           <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <a href="<?php echo $product['href']; ?>"  target="_blank"><img src="<?php echo $product['thumb']; ?>" class="img-responsive" /></a>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a onclick ="addtocart(<?php echo $product['product_id']; ?>)" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Add to Cart</a>
                    </div>
                </div>
            </div>
          <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('<?php echo $product['custom_image']; ?>');">
                </div> 
          <?php } ?>  	
        </div>
    </div>
    <?php $count++; ?>
    <?php } ?>
    <?php } ?>
     
</div>

<script type="text/javascript"> 
      $(document).ready( function() {
        $('.alert-success').delay(1000).fadeOut();
      });
</script>
    
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        var discount_per = '<?php echo $discount_per; ?>';
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
				    if(discount_per > 0) {
                        $('.price-new-' + product_id).html('Rs.'+ Math.round(json.new_price.price1 - (json.new_price.price1 * discount_per / 100)));
                        $('.price-old-' + product_id).html(json.new_price.price);
				    } else {
				        if(json.new_price.special) {
					        $('.price-new-' + product_id).html(json.new_price.special);
					        $('.price-old-' + product_id).html(json.new_price.price);
					    } else {
					        $('.price-new-' + product_id).html(json.new_price.price);
					    }
				    }
					
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.des-container .options .radio input[type="radio"]').click(function() {
        $('.des-container .options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
<?php echo $footer; ?>