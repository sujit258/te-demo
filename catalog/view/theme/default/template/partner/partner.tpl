<?php echo $header; ?>
<style>
input {
   width: 82%;
}
h3 {
    font-size:17px;
}
p {
    font-size:15px;
    margin-top: 10px;
}
.panel-title {
    font-weight: 600;
    margin-top:5px!important;
    margin-bottom: 5px!important;
}
.panel-title > a:before {
	float: right !important;
	font-family: FontAwesome;
	content:"\f068";
	padding-right: 5px;
}
.panel-title > a.collapsed:before {
	float: right !important;
	content:"\f067";
}
.panel-title > a:hover, .panel-title > a:active, .panel-title > a:focus {
	text-decoration:none;
}
.bottom-image {
    margin: 15px 0 15px 0;
}
.select-css {
    display: block;
    line-height: 1.3;
    padding: .6em 1.4em .5em .8em;
    width: 100%;
    max-width: 100%; 
    box-sizing: border-box;
    margin: 0;
    border: 1px solid #aaa;
    box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
}
.container-fluid {
    background: #fafbfd;
}
h3 {
    color: #000;
    font-size: 17px;
    font-weight: 700;
}
.form-control {
    background: #fff;
    border: 1px solid #f2f2f2;
    border-radius: 3px!important;
    padding: 20px 12px!important;
    color: #000!important;
}
.form-control:focus {
    border-color: #80bdff!important;
    outline: 0!important;
    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25)!important;
}
.button {
    background: #f49a25!important;
    border-radius: 4px!important;
    height: 42px!important;
    width: 100px!important;
    font-weight: 600!important;
    font-size: 16px!important;
}
.button:hover {
    background: #000!important;
}
.panel-body {
    border: none!important;
}
.form-group {
    margin-top: 20px!important;
}
.form-group select {
    padding: 0!important;
    height: 42px!important;
}
.fa-exclamation-circle {
    margin-right: 10px;
}
.alert {
    font-size: 15px!important;
    padding: 10px!important;
}
.alert-danger {
    color: #a94442!important;
}
.alert-success {
    color: #a94442!important;
}
.panel-title {
    text-transform: none!important;
    font-size: 15px!important;
}
h4 {
    font-size:15px!important;
    font-weight: 600!important;
    line-height: 20px!important;
}
.panel-body p, #form-partner p {
    text-align: justify!important;
}
@media screen and (max-width: 800px) {
    .breadcrumb {
        margin-bottom: 15px!important;
    }
}
.panel-heading {
    background: #fff!important;
}
.pink {
    color: #feac00;
}
.page-heading {
    font-size: 33px !important;
    margin-left: 15px!important;
} 
.footer-breaking {
    display: none;
}
.top-banner {
    width: 100%;
}
</style>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" itemprop="description" content="We’re looking forward to appointing of Distributors / Dealerships for our entire range of high-quality Snacks and Supplements like Roasted Seeds, Quinoa, Tea," />
<meta name="keywords" itemprop="keywords" content="partner with us,become distributor,become retailer" />
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

  <ul class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><span><?php echo $breadcrumb['text']; ?></span></a></li>
  <?php } ?>
</ul>
<div class="w3-content w3-section">
  <img class="img-responsive top-banner" src="image/catalog/images-08-19/banner/banner-partner.jpg">
</div>
<div class="container-fluid">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

	<div class="container">
		<div class="row">
		    <br/><br/>
		  <h1 class="page-heading"><span class="pink">Partner </span>With Us</h1>
		<div class="col-md-8">	         
			<!-- Offline -->
		<div class="buttons clearfix"> 
		<br>
	    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-top: -24px;">
		<div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Want to become Offline partner ?</a>
		</h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
			<h4>Become a Retailer</h4>
			<p>
			Do you own a store which sells healthy and natural foods? We’re looking for retailers to stock
			our range of high-quality Snacks and Supplements like Roasted Seeds, Quinoa, Tea, Honey, etc.
			If you are interested, please fill in the following details and we will get in touch with you.
			</p>
			<h4>Become a Distributor</h4>
			<p>
			Help us spread!<br><br>
			We’re looking forward to appointing of Distributors / Dealerships for our entire range of high-quality Snacks and Supplements like Roasted Seeds, Quinoa, Tea, Honey, etc. If you are interested in Distributorship / Dealerships please fill in the following details and we will get in touch with you.
			</p>
			</p>
			<h4>Minimum Requirements</h4>
			<p>
			1. GST Certificate for the region you are planning to work.<br>
			2. Good Understanding of FMCG Industry in your respective geography.<br>
			3. At least 1-2 years of distributorship in FMCG preferred.
			</p>
			</div>
        </div>
    </div>
	
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
    <h4 class="panel-title">
    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Want to become Online partner ?</a>
    </h4>
	</div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
    <div class="panel-body">
	<h4>What is Affiliate Marketing ?</h4>
				<p>
				Affiliate marketing is basically performance-based marketing, whereby affiliates/partners promote a merchant’s product/service and get remunerated for every sale, visit or subscription sent to the merchant. It relies on a relationship between a merchant and you, the affiliate/publisher, as well as a relationship between you and consumers. When you promote the merchant’s product or service offerings (on your blog, website, social media feed, etc.) and a consumer purchases a product/service from the brand based on your promotion, the merchant pays you a revenue share of the sale that you helped generate.
				</p>
				<h4>How do I get started ?</h4>
				<p>
				We recommend that you learn as much as possible about what it means to be an affiliate. Fortunately, there is a lot of information out there to help educate new affiliates, including podcasts, blogs, webinars, research reports and much more. Although you do not need training, we do have the power to approve or reject affiliate applications. We have to be more selective about who we choose to accept into the program. Often, we will not approve sites that are sexually explicit, violent, violate international property laws, advocate discrimination, promote radical religious or political views, or advocate or promote any illegal activities. You can go ahead and fill the initial <a href = "index.php?route=affiliate/login">Sign Up application</a> to get started.
				</p>
				<h4>Is there a cost associated with signing up for the program ?</h4>
				<p>
				There is no cost for signing up as a partner to promote our brand through the affiliate program. You can sign up for as many programs as you would like across any affiliate network at no cost to you.
				</p>
				<h4>Do I need to have an established website to get started ?</h4>
				<p>
				Not necessarily. While it is possible to register a site that is not live, it’s important to understand that we look to partner with affiliates who have an established blog/website with a good network of readers/visitors. We would also want to work with affiliates who have a track record of providing high-quality content to their audiences. Therefore, we strongly recommend having an established blog/website that’s receiving good traffic before you apply to the program. Alternatively, if you don’t have an established site, but do have an established social media following, we will accept and allow you to post affiliate links on social media pages you own or operate. You can go ahead and fill the initial <a href = "index.php?route=affiliate/login">Sign Up application</a> to get started.
				</p><br>
		</div>
		</div>
	   </div>
	</div>
</div>
		  <!-- End Online -->
	
		<br>
		</div>
		
		   
			<div class="col-md-4 col-sm-12 col-xs-12">
              <div class="caption" >
			   <div style = "background-color:#ffffff; padding: 20px;margin-bottom: 20px;">
			   <form id="form-partner">
			  <div id="success"></div><div id="not_success"></div>
			    <h3>Fill This Form</h3>
			    <div class="form-group required">
				<label><?php echo $entry_name; ?></label>
                  <input type="text" class="form-control" name="cst_name">
                  </div>
                
                <div class="form-group required">
                <label><?php echo $entry_email; ?></label>
                  <input type="text" class="form-control" name="cst_email">
                  </div>
                
                
                <div id="validate_email" class="form-group required">
                <label><?php echo $entry_mobile; ?></label>
                  <input type="text" class="form-control" name="cst_mobile">
                  </div>
    
                <div id="validate_mobile" class="form-group required">
                <label><?php echo $entry_city; ?></label>
                <input type="text" class="form-control" name="cst_city">
                </div>
                
                <div class="form-group">
			    <label for="cst_scope"><?php echo $entry_scope; ?></label>
				 <select name="cst_scope" class="form-control" id="cst_scope">
				 <option value="" disabled selected>Choose:</option>
				 <option value="For my own retail Store / Chain">For my Own Retail Store/Chain</option>
				 <option value="Bulk Order">Bulk Order</option>
				 <option value="Distributorship">Distributorship</option>
				 <option value="Franchise">Franchise</option>
				 </select>
				 </div>
				 <div class="buttons clearfix">                
                    <button type="button" id="button-send" class="button">Submit</button>										
				 </div>
				 <p>If you are looking to partner as a Distributor, please mention the area for which you would like to partner with us (Part of city / City / State).</p>
				 </form>
			</div>
		   </div>
	  </div>		   
	</div>
	</div>
	</div>
    <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<div class="container">
    <div class="row">
	    <!--div class="col-md-4 bottom-image"><a href="/breakfast-cereals"><img src="/image/breakfast-cereals-small-banner.jpg" alt="" sizes="(max-width: 388px) 100vw, 388px" width="388" height="254"></a></div>
	
	    <div class="col-md-4 bottom-image"><a href="/nuts-berries"><img src="/image/nuts-berries-small-banner.jpg" alt="" sizes="(max-width: 388px) 100vw, 388px" width="388" height="254"></a></div>
	
	    <div class="col-md-4 bottom-image"><a href="/seeds-grains"><img src="/image/seeds-grain-small-bannner.jpg" alt="" sizes="(max-width: 388px) 100vw, 388px" width="388" height="254"></a></div-->
	    <div class="col-md-4 col-xs-12 col-sm-12 bottom-image"><a href="/breakfast-ready-to-eat-food"><img src="/image/catalog/images-08-19/banner/partner-small-banner-1.jpg"></a></div>
	    <div class="col-md-4 col-xs-12 col-sm-12 bottom-image"><a href="/breakfast-ready-to-cook-food"><img src="/image/catalog/images-08-19/banner/partner-small-banner-2.jpg"></a></div>
	    <div class="col-md-4 col-xs-12 col-sm-12 bottom-image"><a href="/roasted-seeds-collection"><img src="/image/catalog/images-08-19/banner/partner-small-banner-3.jpg"></a></div>
	    <!--div class="col-md-3 col-xs-12 col-sm-12 bottom-image"><a href="/seeds-and-berries-collection"><img src="/image/catalog/images-08-19/banner/partner-small-banner-4.jpg"></a></div-->
    </div>
</div>

<script type="text/javascript">


$('#button-send').on('click', function() {
	$.ajax({
		url: 'index.php?route=partner/partner/write',
		type: 'post',
		dataType: 'json',
		data: $("#form-partner").serialize(),
		complete: function() {
			$('#button-send').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#success').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><span>' + json['error'] + '</span></div>');
			}
			
			if (json['validate_email']) {
				$('#validate_email').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><span>' + json['validate_email'] + '</span></div>');
			}
			
			if (json['validate_mobile']) {
				$('#validate_mobile').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><span>' + json['validate_mobile'] + '</span></div>');
			}
			
			if (json['not_success']) {
				$('#not_success').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><span>' + json['not_success'] + '</span></div>');
			}	

			if (json['success']) {
				$('#success').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i><span>' + json['success'] + '</span></div>');				

				$('input[name=\'cst_name\']').val('');
				$('input[name=\'cst_email\']').val('');
				$('input[name=\'cst_mobile\']').val('');
				$('input[name=\'cst_city\']').val('');
				$('input[name=\'cst_scope\']').val('');
			}
		}
	});
});

</script>
</div>
<?php echo $footer; ?>