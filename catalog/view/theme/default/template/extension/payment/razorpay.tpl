<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var razorpay_options = {
    key: "<?php echo $key_id; ?>",
    amount: "<?php echo $total; ?>",
    name: "<?php echo $name; ?>",
    description: "Order # <?php echo $merchant_order_id; ?>",
    netbanking: true,
    currency: "<?php echo $currency_code; ?>",
    prefill: {
      name:"<?php echo $card_holder_name; ?>",
      email: "<?php echo $email; ?>",
      contact: "<?php echo $phone; ?>"
    },
    notes: {
      opencart_order_id: "<?php echo $merchant_order_id; ?>"
    },
    handler: function (transaction) {
        document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
        document.getElementById('razorpay-form').submit();
    }
  };
  var razorpay_submit_btn, razorpay_instance;

  function razorpaySubmit(el){
    if(typeof Razorpay == 'undefined'){
      setTimeout(razorpaySubmit, 200);
      if(!razorpay_submit_btn && el){
        razorpay_submit_btn = el;
        el.disabled = true;
        el.value = 'Please wait...';  
      }
    } else {
      if(!razorpay_instance){
        razorpay_instance = new Razorpay(razorpay_options);
        if(razorpay_submit_btn){
          razorpay_submit_btn.disabled = false;
          razorpay_submit_btn.value = "<?php echo $button_confirm; ?>";
        }
      }
      razorpay_instance.open();
    }
  }

</script>
 <style>
        .buttons{
            margin-bottom: 8%;
        }    
        .btn-primary {
            background: #f49a25;border: 0;font-weight: 600;font-size: 14px;padding: 10px 16px;text-transform: none;color: #fff;-webkit-transition: ease-in-out .3s all;-moz-transition: ease-in-out .3s all;transition: ease-in-out .3s all; 
        }    
        .btn-primary:hover, .btn-primary:focus, .btn-primary.focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
            color: #fff;
            background-color: #000;
            border-color: #000;
        }

    </style>
    
<form name="razorpay-form" id="razorpay-form" action="<?php echo $return_url; ?>" method="POST">
  <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
  <input type="hidden" name="merchant_order_id" id="merchant_order_id" value="<?php echo $merchant_order_id ?>"/>
</form>
<div class="buttons">
  <div class="pull-left">
    <input type="submit" onclick="razorpaySubmit(this);" value="<?php echo $button_confirm; ?>" class="btn btn-primary" />
  </div>
</div>