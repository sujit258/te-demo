<div class="all-list">
  <h1><?php echo $heading_title; ?></h1>
    <?php foreach($mpblogposts as $mpblogpost) { ?>
    <div class="mpblog-layout">
      <div class=" row">
          <div class="image col-md-4 col-sm-12 xl-33 md-33 sm-100">
            <?php if($mpblogpost['showImage']) { ?>
            <a href="<?php echo $mpblogpost['href']; ?>"><img src="<?php echo $mpblogpost['thumb']; ?>" alt="<?php echo $mpblogpost['name']; ?>" title="<?php echo $mpblogpost['name']; ?>" class="img-responsive" /></a>
            <?php } else { ?>
            <div class="video-container"><iframe width="<?php echo $mpblogpost['width']; ?>" height="<?php echo $mpblogpost['height']; ?>" src="<?php echo $mpblogpost['iframeVideo']; ?>" frameborder="0" allowfullscreen></iframe></div>
            <?php } ?>
          </div>
          <div class="caption col-md-8 col-sm-12 xl-66 md-66 sm-100">
            <h4><a href="<?php echo $mpblogpost['href']; ?>"><?php echo (strlen($mpblogpost['name']) > 40) ? substr($mpblogpost['name'], 0, 40) : $mpblogpost['name']; ?></a></h4>
            <p><?php if($show_sdescription && false) { echo (strlen($mpblogpost['sdescription']) > 70) ? substr($mpblogpost['sdescription'],0, 70) : $mpblogpost['sdescription']; } ?> </p>
          </div>
      </div>
    </div>
    <?php } ?>
</div>