<table cellspacing="0" cellpadding="0" border="0" style="width:100%;table-layout:fixed;">
    <tbody>
        <tr>
            <td align="center" width="100%" style="font-family:Verdana,sans-serif;font-size:16px;line-height:1.4;font-weight:normal;">
                <table cellspacing="0" cellpadding="0" border="0" style="width:100%;table-layout:fixed;">
                    <tbody>
                        <tr>
                            <td align="center" width="100%" style="font-family:Verdana,sans-serif;font-size:16px;line-height:1.4;font-weight:normal; padding:15px 0;letter-spacing:0;text-align:center;">
                                <!--[if mso]>
                                  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{reviewmail_link_href}" style="height:36px;v-text-anchor:middle;width:160px;" arcsize="0%" strokecolor="#229ac8" fillcolor="#229ac8">
                                    <w:anchorlock/>
                                    <center style="color:#ffffff;font-family:Verdana,sans-serif;font-size:16px;">Leave a review</center>
                                  </v:roundrect>
                                <![endif]-->
                                <a href="{reviewmail_link_href}" style="background-color:#229ac8;color:#ffffff;display:inline-block;font-family:Verdana,sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:160px;-webkit-text-size-adjust:none;mso-hide:all;margin:0;">Leave a review</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>