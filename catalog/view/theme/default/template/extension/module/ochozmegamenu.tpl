
<div class="nav-container mt-lg-3 mt-md-5 mt-sm-5 mt-4">
   <button class="navbar-toggler navbtn ml-n5 ml-md-n5 mt-1 mt-sm-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" style="margin-left: -67px">
        <span class="navbar-toggler-icon"></span>
    </button>
 
 <div class="collapse navbar-collapse justify-content-center" id="navbarCollapse">
  <div class="px-4 d-lg-none">
      <a  class="img-fluid pull-left py-4" href="<?php echo $home; ?>"><img src="/image/catalog/brands/te-logo.svg" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" style="width: 80px;"/></a>           
   
    <button class="navbar-toggler nav-close pull-right mt-5" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <div class="close-icon">✖</div>
    </button>
  </div>
	  <div id="pt_custommenu" class="pt_custommenu mt-lg-0">
	 <ul id="pt_menu<?php echo $header['headermenu_id'] ?>" class="text-left navbar-nav pt_menu <?php echo $class; ?> mt-lg-1">
        <?php foreach($headermenu as $header){?>   
        <?php if($header['sub_title']){?>
          <li class="nav-item dropdown">
               <a class="nav-link px-lg-4 px-4" href="#" role="button" data-toggle="collapse" aria-expanded="false" data-target="#dropdown-collapse"><span class="dropdown-toggle"><?php echo $header['title'] ?></span></a>
    
           <?php } else { ?>
           <li class= "nav-item">
             <a class="nav-link px-lg-4 px-4" href="#"><span><?php echo $header['title'] ?></span></a> 
           </li>     
          <?php  }?>
        <?php if($header['sub_title']){?>                 
            <ul id="dropdown-collapse" class="dropdown-content dropdown-menu collapse text-left px-3" style="font-size: 11px;">
        <?php foreach($header['sub_title'] as $subtitle){ ?>
          <li class="nav-item display">
             <a class="nav-link dropdown-item" href="#"><span><?php echo $subtitle['title'];?></span></a>
          </li>
      <?php  }?>
     </ul> 
    <?php  }?>
    </li>
   <?php  }?>
   </ul>
   </div>
 </div>
</div>
<script type="text/javascript">
//<![CDATA[
	var body_class = $('body').attr('class'); 
	if(body_class == 'common-home') {
		$('#pt_menu_home').addClass('act');
	}
	
var CUSTOMMENU_POPUP_EFFECT = <?php echo $effect; ?>;
var CUSTOMMENU_POPUP_TOP_OFFSET = <?php echo $top_offset ; ?>

//]]>
</script>
<script>
$(document).click(function (event) {
if ($(event.target).parents(".navbar-collapse").length < 1) {
    var clickover = $(event.target);
    var $navbar = $(".navbar-collapse");               
    var _opened = $navbar.hasClass("show");
    var $overlay = $(".screen-overlay");
    if (_opened === true && !clickover.hasClass("navbar-toggler")) {      
        $navbar.collapse('hide');
        $overlay.removeClass("show");
    }
  }
});
</script>