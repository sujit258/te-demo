<?php if ($alert_type == 'custom') { ?>
<style type="text/css">
.alert-jpn.alert-custom {
	background: <?php echo $background_color; ?> !important;
	border-color: <?php echo $border_color; ?> !important;
	color: <?php echo $text_color; ?> !important;
}
</style>
<?php } ?> 

<script type="text/javascript"><!--
var jpn_notifications = <?php echo json_encode($notifications); ?>;
var jpn_total_notifications = jpn_notifications.length;
var jpn_show_index = 0;
	
$(document).ready(function(){
	$.notifyDefaults({
		position: 'fixed',
		type: '<?php echo $alert_type; ?>',
		<?php if ($allow_dismiss) { ?>
		allow_dismiss: true,
		<?php } else { ?>
		allow_dismiss: false,
		<?php } ?>
		<?php if ($show_progressbar) { ?>
		showProgressbar: true,
		<?php } else { ?>
		showProgressbar: false,
		<?php } ?>	
		placement: {
			from: '<?php echo $placement_from; ?>',
			align: '<?php echo $placement_align; ?>'
		},		
		z_index: <?php echo $zindex; ?>,
		delay: <?php echo $delay; ?>,
		url_target: '_self',
		mouse_over: 'pause',
		animate: {
			enter: 'animated <?php echo $animate_enter; ?>',
			exit: 'animated <?php echo $animate_exit; ?>'
		},
		icon_type: 'image',
		template: '<div data-notify="container" class="col-md-3 col-sm-4 col-xs-11 alert-jpn alert-{0}" role="alert">' +
				'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
				'<span data-notify="icon"></span> ' +
				'<span data-notify="message">{2}</span>' +
				'<span data-notify="title">{1}</span> ' +
				'<div class="progress" data-notify="progressbar">' +
					'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
				'</div>' +
				'<a href="{3}" target="{4}" data-notify="url"></a>' +
			'</div>' 
	});
	
	showNotification(jpn_show_index);
});

function showNotification(show_index){
	$.notify({
		icon: jpn_notifications[show_index]['image'],
		message: jpn_notifications[show_index]['message'],
		title: (jpn_notifications[show_index]['show_time_ago'] != 0) ? jpn_notifications[show_index]['time_ago'] : '',
		url: jpn_notifications[show_index]['product_href']
	},{
		onClosed: function() {
			if (show_index < jpn_total_notifications - 1) {
				setTimeout(showNotification, <?php echo $timeout; ?>, ++show_index);	
			}	
		}
	});
}
--></script>