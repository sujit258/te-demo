<?php if ($title) { ?>
<h3><?php echo $title; ?></h3>
<?php } ?>
<div id="tltslideshow<?php echo $slideshow; ?>" class="owl-carousel" style="opacity: 1;">
<?php foreach ($slides as $slide) { ?>
    <div class="tltslide hidden-xs hidden-sm">
    <?php if ($slide['link']) { ?>
    <a href="<?php echo $slide['link']; ?>">
        <?php if ($slide['image']) { ?>
            <img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['title']; ?>" class="img-responsive" />
        <?php } else { ?>
            <div style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; margin: 0; padding: 0;">&nbsp;</div>
        <?php } ?>
        <?php if ($slide['textbox']) { ?>
        <?php if ($slide['override']) { ?>
        <div class="<?php echo $slide['css']; ?>" style="background: <?php echo $slide['background']; ?>; opacity: <?php echo $slide['opacity']; ?>;">
        <?php } else { ?>
        <div class="<?php echo $slide['css']; ?>">
        <?php } ?>
        <?php if ($slide['use_html']) { ?>
            <?php echo $slide['html']; ?>
        <?php } else { ?>
            <h1><?php echo $slide['header']; ?></h1>
            <span><?php echo $slide['description']; ?></span>
        <?php } ?>
        </div>
        <?php } ?>
    </a>
    <?php } else { ?>
        <?php if ($slide['image']) { ?>
            <img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['title']; ?>" class="img-responsive" />
        <?php } else { ?>
            <div style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; margin: 0; padding: 0;">&nbsp;</div>
        <?php } ?>
        <?php if ($slide['textbox']) { ?>
        <?php if ($slide['override']) { ?>
        <div class="<?php echo $slide['css']; ?>" style="background: <?php echo $slide['background']; ?>; opacity: <?php echo $slide['opacity']; ?>;">
        <?php } else { ?>
        <div class="<?php echo $slide['css']; ?>">
        <?php } ?>
        <?php if ($slide['use_html']) { ?>
            <?php echo $slide['html']; ?>
        <?php } else { ?>
            <h1><?php echo $slide['header']; ?></h1>
            <span><?php echo $slide['description']; ?></span>
        <?php } ?>
        </div>
        <?php } ?>
    <?php } ?>
    </div>
 <?php } ?>
</div>   
    <div id="tltslideshow1<?php echo $slideshow; ?>" class="owl-carousel" style="opacity: 1;">
    <?php foreach ($slides as $slide) { ?>
    <div class="tltslide hidden-md hidden-lg hidden-xlg">
    <?php if ($slide['link']) { ?>
    <a href="<?php echo $slide['link']; ?>">
        <?php if ($slide['image']) { ?>
            <img src="<?php echo $slide['mobileimage']; ?>" alt="<?php echo $slide['title']; ?>" class="img-responsive" />
        <?php } else { ?>
            <div style="width: <?php echo $mobilewidth; ?>px; height: <?php echo $mobileheight; ?>px; margin: 0; padding: 0;">&nbsp;</div>
        <?php } ?>
        <?php if ($slide['textbox']) { ?>
        <?php if ($slide['override']) { ?>
        <div class="<?php echo $slide['css']; ?>" style="background: <?php echo $slide['background']; ?>; opacity: <?php echo $slide['opacity']; ?>;">
        <?php } else { ?>
        <div class="<?php echo $slide['css']; ?>">
        <?php } ?>
        <?php if ($slide['use_html']) { ?>
            <?php echo $slide['html']; ?>
        <?php } else { ?>
            <h1><?php echo $slide['header']; ?></h1>
            <span><?php echo $slide['description']; ?></span>
        <?php } ?>
        </div>
        <?php } ?>
    </a>
    <?php } else { ?>
        <?php if ($slide['mobileimage']) { ?>
            <img src="<?php echo $slide['mobileimage']; ?>" alt="<?php echo $slide['title']; ?>" class="img-responsive" />
        <?php } else { ?>
            <div style="width: <?php echo $mobilewidth; ?>px; height: <?php echo $mobileheight; ?>px; margin: 0; padding: 0;">&nbsp;</div>
        <?php } ?>
        <?php if ($slide['textbox']) { ?>
        <?php if ($slide['override']) { ?>
        <div class="<?php echo $slide['css']; ?>" style="background: <?php echo $slide['background']; ?>; opacity: <?php echo $slide['opacity']; ?>;">
        <?php } else { ?>
        <div class="<?php echo $slide['css']; ?>">
        <?php } ?>
        <?php if ($slide['use_html']) { ?>
            <?php echo $slide['html']; ?>
        <?php } else { ?>
            <h1><?php echo $slide['header']; ?></h1>
            <span><?php echo $slide['description']; ?></span>
        <?php } ?>
        </div>
        <?php } ?>
    <?php } ?>
    </div>
<?php } ?>
</div>
                    

<script type="text/javascript">
if ($(window).width() > 780) {
    var owl = $("#tltslideshow<?php echo $slideshow; ?>");
    owl.owlCarousel({
        autoPlay: true,
        items : 1,
        slideSpeed : 1000,
        navigation : true,
        pagination : true,
        controls: true,
        stopOnHover : false,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [991,1],
        itemsTablet: [700,1],
        itemsMobile : [400,1],
    });

}

if ($(window).width() < 779) {
      
    var owl = $("#tltslideshow1<?php echo $slideshow; ?>");
    owl.owlCarousel({
        autoPlay: true,
        items : 1,
        slideSpeed : 1000,
        navigation : true,
        pagination : true,
        controls: true,
        stopOnHover : false,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [991,1],
        itemsTablet: [700,1],
        itemsMobile : [400,1],
    });

}

</script>