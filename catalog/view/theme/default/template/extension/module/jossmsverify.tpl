<div class="warning" style="display:none" id="ovwarn"></div>
<div class="success" style="display:none" id="sendsuccess"></div>
<h3><?php echo $heading_title?>: </h3>
<div class="buttons" id="jstep1">
<?php $the_text = sprintf($text_explain1, $phone); echo "<h4>$the_text</h4>";?>
<br />

<div class="left">
<a id="button-startver" class="btn btn-default"><span><?php echo $text_start?></span></a>
</div>
</div>
<div id="jstep2" style="display:none" class="buttons">
<?echo "<h4>$text_explain_started</h4>";?>
<div id="type1" style="display:none"><h4><?echo $text_explain_phone_call2;?></h4></div>
<div id="type2" style="display:none"><h4><?echo $text_explain_sms2;?></h4></div>

<?echo "$text_verification_code";?> <input type="text" name="pin" id="pin"><br><br>
  <div class="right"> <a id="button-confirm" class="btn btn-default"><span><?php echo $text_verify;?></span></a> <a id="button-startver2" class="btn btn-default"><span><?php echo $text_resend;?></span></a></div>
</div>
<script type="text/javascript"><!--
var pinsent = '<?php echo $pinsent?>';
if (pinsent=='1') {
		
		$("#jstep1").hide();
		$("#jstep2").show();
		$("#type1").hide();
		$("#type2").hide();
}
$('#button-confirm').bind('click', function() {
	$.ajax({ 
		type: 'POST',
		data: 'pin=' + $('#pin').val(),
		url: 'index.php?route=extension/module/jossmsverify/confirm',
		success: function(data) {				
				if (data==1) {
					$("#ovwarn").hide();
					$("#sendsuccess").hide();
					$.ajax({
						url: 'index.php?route=checkout/confirm',
						dataType: 'html',
						success: function(html) {
							
							$('#spayment-method-content').html(html);	
						
							<?php if ($payment_methods) { ?>
							<?php foreach ($payment_methods as $payment_method) { ?>					
							$('#t<?php echo $payment_method['code'];?>').removeClass('disabled');
							<?php  }?>
							<?php  }?>
							hideBar();
							$('#pLoader').hide();													
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
				else {
				$("#sendsuccess").hide();
				$("#ovwarn").html("<?php echo $text_invalid_pin?>");
				$("#ovwarn").show();
				}
		}		
	});
});
$('#button-startver2').bind('click', function() {
	$("#ovwarn").hide();
	$("#sendsuccess").hide();
	$("#jstep2").hide();
	$("#jstep1").show();
});
var wait = 0;
$('#button-startver').bind('click', function() {

if (!wait) {
wait = 1;
	$.ajax({ 
		type: 'POST',
		data: 'phone='+$('#phone').val() + "&svtype=" + $("input[name='svtype']:checked").val(),
		url: 'index.php?route=extension/module/jossmsverify/start',
		success: function(data) {
		wait = 0;
			switch (data) {
			case "5":
				$("#sendsuccess").hide();
				$("#ovwarn").html("<?php echo $text_provide_valid_mobile_number;?>");
				$("#ovwarn").show();
			break;
			case "2":
			case "Success":
					$("#sendsuccess").html("<?php echo $text_send_success?>");
					$("#sendsuccess").show();
					$("#ovwarn").hide();
					$("#jstep1").hide();
					$("#jstep2").show();	
					$("#type1").hide();
					$("#type2").hide();
					$("#type"+data).show();
			break;
			case "15":
				$("#sendsuccess").hide();
				$("#ovwarn").html("<?php echo $text_explain_same_number;?>");
				$("#ovwarn").show();
			break;
			case "14":
				$("#sendsuccess").hide();
				$("#ovwarn").html("<?php echo $text_please_wait_next;?>");
				$("#ovwarn").show();
			break;
			case "12":
				$("#sendsuccess").hide();
				$("#ovwarn").html("<?php echo $text_max_retries_exceeded;?>");
				$("#ovwarn").show();
			break;
			case "16":
				$("#sendsuccess").hide();
				$("#ovwarn").html("<?php echo $text_connection_problem;?>");
				$("#ovwarn").show();
			break;
			default:
				$("#sendsuccess").hide();
				$("#ovwarn").html("<?php echo $text_provide_valid_number;?>");
				$("#ovwarn").show();
				
				}
		}		
	});
	}
	else alert('<?php echo $text_please_wait;?>');
});
//--></script> 