<?php if($force_redirect){ ?>
<script type="text/javascript">
location = '<?php echo $force_redirect; ?>';
</script>
<?php } else { ?>
</script>

</script><link href="catalog/view/javascript/addtocart/noty.css" type="text/css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/addtocart/checkout_notice_add.min.js" type="text/javascript"></script>   

 <style> 
    
    @media screen and (max-width: 500px) {
        
        #product-6422{
            display:none;
        }
    }
    
    .options {
        min-height: 45px;
    }
        
    .pluss, .minuss {
         float: right;
         font-size: 25px;
    }
    .minuss { display: none;}
    .questions:checked ~ .pluss { display: none; } 
    .questions:checked ~ .minuss { display: block; }
     .quantity {
        margin-left: 25%;
        margin-right: 25%;
    }
    
    .quantity1 {
        height: 35px!important;
        width: 60%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }	.btn-plus, .btn-minus {
  	    background: #fff;
        border: 1px solid #ddd;
  	}
  	.btn-plus:hover, .btn-minus:hover, .btn-plus:focus, .btn-minus:focus  {
  	    background: #f49a25;
  	    color: #fff;
        border: 1px solid #f49a25;
  	}
  	.quantity .btn-block {
  	    display: flex;
  	}
  	.top-banner{
  	    margin-bottom: 18px;
  	}
  		.quantity1 {
        height: 35px!important;
        width: 45%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    .price-new {
        display: inline-block!important;
        font-size: 15px!important;
        color: #f49a25!important;
        text-align: left!important;
        font-weight: 700!important;
        margin: 0!important;
    }
    .price-old {
        display: inline-block!important;
        color: #5c5c5c!important;
        font-size: 12px!important;
        margin-left: 5px!important;
        text-decoration: line-through!important;
    }
    .image1 {
        display: table;
        margin: auto;
    }
    .cart-msg h4 {
        text-align: center;
        font-weight: 600;
        line-height: 20px;
    }
    .des-container {
        padding: 0!important;
    }
    .product-name {
        padding-right: 0!important;
        padding-top: 5px;
        font-size: 16px!important;
        min-height: 50px;
        font-weight: 700;
    }
    .price-label {
        width: 100%!important;
        padding-left: 0!important;
    }
    .module-title{
        margin-bottom: 20px;
        text-align: left;
        margin-bottom: 50px;
        margin-top: 10px;
        font-size: 17px !important;
        font-weight: 600;
        position: relative;
        z-index: 1;
    }
    .button-cart-mobile {
        float: right;
        padding: 7px 10px!important;
    }
    .product-options {
        background: #fff;
    }
    .quantity-container {
        width: 54%;
    }
    .button-cart {
        font-size: 13px !important;
        padding: 8px 10px !important;
        float: right!important;
    }
    .try-new-image {
        padding-left: 0!important;
    }
    .try-new-image img {
        width: 100%;
    }
    .des-container .options .radio input {
        display: none !important;
    }
    .des-container .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .des-container .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    .des-container .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    .options {
        min-height: 55px!important;
    }
    .oos {
        background: #777 !important;
        border-radius: 4px !important;
        float: right !important;
        font-size: 13px !important;
        padding: 8px 10px !important;
    }
    .button-cart {
        float: right!important;
        font-size: 13px!important;
        padding: 8px 10px!important;
    }
    .button-personalize {
        font-size: 13px!important;
        padding: 8px 10px!important;
        float: right!important;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 13px!important;
        padding: 8px 10px!important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .quantity-new-launch {
        height: 35px!important;
        width: 50%;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .related-product, .new-launch-container {
        padding-left: 7px!important;
        padding-right: 7px!important;
    }
    .product-name {
         font-size: 16px !important;
    }
    .button-cart {
        color: #fff !important;
    }
    
    .des-container .options .radio {
        margin: 0px 3px 5px 0px;
    }
     
    .new-launch-container {
        min-height: 365px!important;
    }
    
    .button {
        background: #f49a25;
        border: 0;
        font-weight: 600;
        font-size: 11px;
        padding: 6px 10px;
        text-transform: none;
        color: #fff;
        -webkit-transition: ease-in-out .3s all;
        -moz-transition: ease-in-out .3s all;
        transition: ease-in-out .3s all;
    }
    .item-inner .image2 {
        position: absolute;
        opacity: 0;
    }
    .item-inner:hover .image2 {
        opacity: 1;
        z-index: 0;
        -moz-transition: all 0.3s ease-in-out 0s;
        transition: all 0.3s ease-in-out;
    }
    
    .deals-discount {
        border-radius: 50%;
        right: 16%;
        top: -7%;
        position: absolute;
        background: #f49a25;
        text-align: center;
        z-index: 5;
        width: 50px;
    }
    
    .deals-discount h5 {
        color: #fff;
        font-weight: 600;
        padding: 1px;
    }
    
    .price-coupon{
        font-weight: bold;
    }  
</style>  


<?php echo $modal_form; ?>
<div id="addressPage" class="container">
<div class="row equal">
<div id="existingAddress" class="col-md-9 animated fadeIn">
<ul class="hidden nav nav-pills nav-wizard hidden-xs">
        	<li class="locked"><a><?php echo $text_checkout_step_1; ?><i class="fa fa-lock"></i></a><div class="nav-arrow"></div></li>
        	<li class="active"><div class="nav-wedge"></div><a><?php echo $text_checkout_step_2; ?></a><div class="nav-arrow"></div></li>
        	<li class="locked"><div class="nav-wedge"></div><a><?php echo $text_checkout_step_3; ?><i class="fa fa-lock"></i></a></li>
    	</ul>
<div id="existingAddressPanel" <?php echo $addresses || ($address_modal && !$addresses)?'':'style="display:none;"'?>>
<?php if($addresses && $shipping_required && !$xshipping_method) { ?>
	<div class="alert alert-danger noshipping margintop20 marginbottom0"><?php echo $shipping_error; ?></div>
<?php } ?>
<?php if($address_block || !$addresses){ ?>
<?php if(!$shipping_required || $same_shipping || !$addresses){ ?>
	<input type="hidden" name="xshipping_address_check" value="1"  />
<?php } else { ?>
	<div class="checkbox is_checkbox info-back-checkout text-center">
		<label for="shipping_check">
		<input type="checkbox" class="input-checkbox" name="xshipping_address_check" value="1" id="shipping_check" <?php echo ($same_address ?'checked="checked"':''); ?> /><?php echo $entry_shipping; ?></label>
	</div>
<?php }?>
<?php if($addresses) { ?>
	<div class="row">
		<div class="col-md-12 ">              
		    <h4 style="cursor: pointer;" class="pull-right">
		    <?php if($address_modal){ ?>
		    	<a data-toggle="modal" data-target="#addressModal"><i class="fa fa-plus"></i>&nbsp;<?php echo $text_address_new;?></a>
			<?php } else { ?>
				<a onclick="toggleElement('#existingAddressPanel, #addressfields');$('.col-md-3.lborder').addClass('xblur');"><i class="fa fa-plus"></i>&nbsp;<?php echo $text_address_new;?></a>
			<?php } ?>
		    </h4>
		</div>
	</div>
<?php } ?>
	<div class="clearfix"></div>
<?php if ($addresses) { ?>
              <div class="row">
                <?php foreach ($addresses as $address) { ?>
                <div class="col-sm-6 col-md-4 text-center">
                  <div class="panel panel-default">                   
                   <div class="panel-heading <?php echo ($address['address_id'] == $address_id)?'selected':''?>">
                    <div class="radio">                        
                        <label for="p-<?php echo $address['address_id']; ?>">
                        <input id="p-<?php echo $address['address_id']; ?>" <?php echo ($address['address_id'] == $address_id)?'checked="checked"':''?> type="radio" name="address_id" value="<?php echo $address['address_id']; ?>" />  
                          <span class="padd"><?php echo (($shipping_required && $same_shipping)? $text_psaddress:$text_paddress); ?></span>                         
                        </label>                        
                    </div>
                    </div>                                       
                    <div class="panel-body highlight">
                    <p class="text-justified"><?php echo $address['address']; ?></p>
                    <?php echo ($address['default']?$entry_default:'<span>'.$address['delete']).'</span>';?>
                    <span><?php echo $address['edit']; ?></span>
                    <div class="clearfix"></div>  
                    </div>
                    <?php if($shipping_required && !$same_shipping){ ?>
                    <div <?php echo ($same_address ?'style="display:none;"':''); ?> class="panel-footer <?php echo ($address['address_id'] == $shipping_address_id)?'selected':''?>"">
                    <div class="radio">                    
                        <label for="s-<?php echo $address['address_id']; ?>">
                      <input id="s-<?php echo $address['address_id']; ?>" <?php echo ($address['address_id'] == $shipping_address_id)?'checked="checked"':''?> type="radio" name="saddress_id" value="<?php echo $address['address_id']; ?>" />      
                          <?php echo $text_saddress;?>                         
                        </label>
                    </div>                  
                    </div>                     
                    <?php } ?>                   
                  </div>
                </div>
                <div class="clearfix visible-xs">
                </div>
                <?php } ?>                
              </div>
            <?php } else{ ?>
              <div class="row">                
                <div class="col-md-12">                                                       
                    <div style="background: transparent;" class="jumbotron">                                   
                    <h3 class="text-center"><a style="cursor: pointer;" data-toggle="modal" data-target="#addressModal"><span style="line-height: 170px;" class="glyphicon glyphicon-plus"></span><?php echo $text_new_address_heading; ?></a></h3>
                  </div>
                </div>
              <div class="clearfix visible-xs">
                </div>
              </div>
            <?php } ?>
            </div>
            <?php } else { ?>
              <?php if ($addresses) { ?>
    			<div class="row">       
    			<?php if(!$shipping_required || $same_shipping || !$addresses){ ?>
              <input type="hidden" name="xshipping_address_check" value="1"  />
            <?php }else{ ?>
              <div class="col-md-12">	
               <div class="checkbox is_checkbox info-back-checkout text-center">
              	<label for="shipping_check">
                <input type="checkbox" class="input-checkbox" name="xshipping_address_check" value="1" id="shipping_check" <?php echo ($same_address ?'checked="checked"':''); ?> />
        		<?php echo $entry_shipping; ?>
              	</label>
           	   </div>
           	   </div>
            <?php }?>        	              
                <div class="payment-address-panel <?php echo (!$shipping_required || ($shipping_required && $same_address))?'col-md-12':'col-md-6' ?>">
                  <div class="panel-address">                   
                   <div class="panel-address-heading address-type"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;<span class="padd"><?php echo (($shipping_required && $same_address)? $text_psaddress:$text_paddress); ?></span>
                   <?php if(count($addresses)>0){ ?>
                   <small class="pull-right">
                   <?php if($address_modal){ ?>
                   <a data-toggle="modal" data-target="#addressModal"><i class="fa fa-plus"></i>&nbsp;<?php echo $text_address_new; ?></a>
                   <?php } else { ?>
                   <a onclick="toggleElement('#existingAddressPanel, #addressfields');$('.col-md-3.lborder').addClass('xblur');"><i class="fa fa-plus"></i>&nbsp;<?php echo $text_address_new; ?></a>
                   <?php } ?>
                   </small>
                   <?php } ?>
                   </div>                    
                    <div class="panel-address-body payment-address" >
                    	<div class="text-justified">
                    	<div class="address-list">
                    	<?php $count = 1;?>                    	
                    	<?php foreach ($addresses as $address) { ?>
                    	<?php $count++; ?>
                    	<label class="address-label <?php echo($address['address_id'] == $address_id) ? 'selected':''; ?> " sort="<?php echo($address['address_id'] == $address_id) ? 1:$count; ?>">
                    	<input class="input-radio" type="radio" name="address_id" value="<?php echo $address['address_id']; ?>" <?php echo($address['address_id'] == $address_id)?'checked="checked"':''; ?> />
                    	<span class="address-string" title="<?php echo $address['linear_address']; ?>"><?php echo $address['linear_address']; ?></span>
                    	<span class="add_delete">
                    		<span class="pull-left"><?php echo ($address['default']?$entry_default:$address['delete']);?></span>
                    		<span class="pull-right"><?php echo $address['edit']; ?></span>  
                    	</span>
                    	</label>
                    	<?php } ?>
                    	</div>                  	
                    </div>
                  </div>
                </div>
                </div>
                <?php if($shipping_required && !$same_shipping){ ?>
                <div class="col-md-6 shipping-address-panel" <?php echo ($same_address ?'style="display:none;"':''); ?> >
                  <div class="panel-address">                   
                   <div class="panel-address-heading"><i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp;<?php echo $text_saddress; ?>                   
                   <small class="pull-right">
                   <?php if($address_modal){ ?>
                   <a data-toggle="modal" data-target="#addressModal"><i class="fa fa-plus"></i>&nbsp;<?php echo $text_address_new; ?></a>
                   <?php } else { ?>
                   <a onclick="toggleElement('#existingAddressPanel, #addressfields');$('.col-md-3.lborder').addClass('xblur');"><i class="fa fa-plus"></i>&nbsp;<?php echo $text_address_new; ?></a>
                   <?php } ?>                   
                   </small>                   
                   </div>                    
                    <div class="panel-address-body shipping-address">
                    	<div class="text-justified" >                    	
                    	<div class="address-list">
                    	<?php $count = 1;?>                    	
                    	<?php foreach ($addresses as $address) { ?>
                    	<?php $count++; ?>
                    	<label class="address-label <?php echo($address['address_id'] == $shipping_address_id)?'selected':''; ?>" sort="<?php echo($address['address_id'] == $shipping_address_id) ? 1:$count; ?>">
                    	<input class="input-radio" data-formatted-address="<?php echo $address['address']; ?>" type="radio" name="saddress_id" value="<?php echo $address['address_id']; ?>" <?php echo($address['address_id'] == $shipping_address_id)?'checked="checked"':''; ?> />
                    	<span class="address-string" title="<?php echo $address['linear_address']; ?>"><?php echo $address['linear_address']; ?></span>
                    	<span class="add_delete">
                    		<span class="pull-left"><?php echo ($address['default']?$entry_default:$address['delete']);?></span>
                    		<span class="pull-right"><?php echo $address['edit']; ?></span>  
                    	</span>
                    	</label>
                    	<?php } ?>
                    	</div>
                    	</div>                    	
                    </div>
                  </div>
                </div>
                <?php } ?>                            
               <div class="clearfix"></div>                
              </div>
            <?php } ?>
            <?php if($shipping_required || $display_comments){ ?>
            <div class="row">
            	<?php if($shipping_required){ ?>
                <div class="<?php echo $display_comments?'col-md-6':'col-md-12'; ?>">
                	<div id="shipping_method" class="container_panel">                	              
        			<div class="heading"><i class="fa fa-truck" aria-hidden="true"></i>&nbsp;<?php echo $text_shipping_method; ?></div>
          			<div class="shipping-table">
          			<?php if($addresses && $shipping_required && !$xshipping_method) { ?>
					<span class="xerror noshipping marginbottom0"><?php echo $shipping_error; ?></span>
					<?php } else { ?>
					<?php echo $xshipping_method; ?>
					<?php } ?>
					</div>
          			</div>
                </div>
                <?php } ?>
                <?php if ($addresses && $display_comments) { ?>
                <div class="<?php echo $shipping_required?'col-md-6':'col-md-12'; ?>">
                	<div id="comment">
                		<div id="order-comment" class="container_panel" >              
        					<div class="heading"><i class="fa fa-comment"></i>&nbsp;<?php echo $comment_text; ?></div>
        					<textarea name="comment" class="form-control" rows="3" placeholder="<?php echo $comment_placeholder_text; ?>"><?php echo $comment; ?></textarea>
        				</div>
        			</div>
                 </div>
                <?php } ?> 
             </div> 
	     <div class="clearfix"></div>
	     <?php } ?>
	     <?php if ($addresses) { ?>
	     <?php if($isMobile){ ?>  
	      	<div id="options" class="animated1 zoomIn1"><?php echo $xoptions;?></div>
	      	<div class="clearfix"></div>
      	     <?php } ?>
	     <?php if(!isset($error_stock_warning) && !isset($error_minimum_warning)){ ?>   
       		<div class="row margintop20">
       			<div id="agree-panel" <?php echo ($addresses && $shipping_required && !$xshipping_method)?'style="display:none;"':''; ?> class="col-md-12 agree-panel">
       			<?php if($text_agree){?>
       				<div id="agreeText" class="col-md-6 agreeText">
          			<label class="pointer checkbox is_checkbox"><input type="checkbox" class="input-checkbox" name="agree" value="1" checked="checked"><span><?php echo $text_agree;?></span></label>          			
    				</div>
    			<?php }?>
    			<div class="<?php echo $text_agree?'col-md-6 large-pull-right paddingright0':'col-md-12 nopadding';?>">
    				<div class="progress progress-continue-disabled" id="progress-continue-disabled" <?php echo ($text_agree && !$agree?'style="display: none"':'style="display: none"');?> >
                    	<div data-trigger="focus" tabindex="0" data-placement="top" data-toggle="popover" title="<?php echo $info_title;?>" data-content="<?php echo $agree_content;?>" class="progress-bar" role="progressbar" id="button-payment-disabled" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100">
                      		<span><?php echo $text_payment_continue;?><i class="fa fa-lock"></i></span>
                    	</div>
                	</div>
        			<div class="progress progress-continue" id="progress-continue" <?php echo ($text_agree && !$agree?'style="display: block"':'');?> >
                    	<div class="progress-bar progress-bar-success" onclick="callPaymentFromAddress();" role="progressbar" id="button-payment" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                      		<span><?php echo $text_payment_continue;?></span>
                    	</div>
                 	</div>
                 </div>
        		</div>
        		</div>
	        <?php } else{ ?>
		        <?php if(isset($error_stock_warning)) { ?>
					<div class="alert alert-danger noshipping margintop20 marginbottom0"><?php echo $error_stock_warning; ?></div>
				<?php } ?>
				<?php if(isset($error_minimum_warning)) { ?>
					<div class="alert alert-danger noshipping margintop20 marginbottom0"><?php echo $error_minimum_warning; ?></div>
				<?php } ?>
	        <?php } ?>		
	      <?php } ?>               
             </div>
            <?php } ?>
            <div id="addressfields" <?php echo !$addresses && !$address_modal?'':'style="display:none;"'?> class="inlineaddress margintop20  zoomIn"><?php echo $inline_form; ?></div> 
      		<div id="editAddressFields" style="display: none;" class="inlineaddress margintop20 animated1 zoomIn1"></div>
      		<!-- sample products -->
      		<!-- sample products -->
      		
     <?php if ($sample_products) { ?>
      	<input type="hidden" name="product_id" value="" />
        <div class="related-product-container quickview-product" style="padding-bottom: 10%;">
            <div class="module-title"><span>Try our New Launches</span><hr/></div>
            <div class="row">
                <div class="related-product quickview-added qv-wtext col-md-12 col-sm-12 col-xs-12">
                   
                    <?php foreach ($sample_products as $product) { ?>
                        <div class="col-md-4 col-xs-6 col-sm-6 new-launch-container" id="product-<?php echo $product['product_id']; ?>">
                            <div class="product-thumb item-inner">
                                <?php if($product['coupon_discount']){ ?> 
    				                <div class="deals-discount"><h5><?php echo round($product['discount']); ?> %<br>Off</h5></div>
    			                <?php } ?>
			                <div class="images-container">
                                    <a href="<?php echo $product['href']; ?>">
                                        <?php if ($product['thumb']) { ?>
                                            <?php if($product['rotator_image']){ ?>
                                                <img class="image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" style="max-width: 70% !important; margin-left: 15%!important;"/>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <img src="image/cache/no_image-100x100.png" alt="<?php echo $product['name']; ?>" />
                                        <?php } ?>
                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="image1" style="max-width: 70% !important;"/>
                                    </a>
                                </div>
                                <div class="des-container">
                                    <div class="name-wishlist">
                                        <h2 class="product-name hidden-xs hidden-sm"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                                        <h2 class="product-name hidden-md hidden-lg hidden-xl"><a href="<?php echo $product['href']; ?>"><?php echo $product['name_mobile']; ?></a></h2>
                                    </div>
                                	<?php $is_optionqty = 1; ?>
				                    <?php if($product['options']) { ?>
				                        <div class="options options-<?php echo $product['product_id']; ?>">
				                            <?php foreach($product['options'] AS $option) { ?>
                                                <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <?php if($option_value['quantity'] > 0) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="checked-option">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'enabled'; } ?> checked/>
                                                            <?php } else { ?>
                                                                <label>
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'enabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                                
					                        <?php } ?>
					                    </div>
				                    <?php } else { ?>
				                   	    <div class="options"></div>
				                           <?php $is_optionqty = 0; ?>
				                    <?php } ?>
				                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
				                        <div class="box-price">
				                            <?php if ($product['price']) { ?>
				                                <p class="price">
				                                    <?php if (!$product['special']) { ?>
                                                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
				                   	                <?php } else { ?>
				                   	                    <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
				                   	                    <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
				                   	                <?php } ?>
				                                </p>
				                                <?php if ($product['coupon_discount']) { ?>
            			    	            <span class="price-coupon price-coupon-<?php echo $product['product_id']; ?>"><b> <?php echo $product['coupon_discount']; ?> </b></span><span> <?php echo $product['coupon'] ? 'Using code: <b>' . $product['coupon'] . '</b>' : ''; ?></span>
            			    	    <?php } ?>
				                            <?php } ?>
				                        </div>
			                        </div>
				                    <?php if($product['personalized'] == 0) { ?>
			                           <?php if($is_optionqty == 1 && $product['quantity'] > 0) { ?>
						        <div class="quantity-container">
						            <input type="button" value="-" class="minus-button" onclick="minus1(<?php echo $product['product_id']; ?>)" />
						            <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity-new-launch" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>)" />
						            <input type="button" value="&#43;" class="plus-button" onclick="plus1(<?php echo $product['product_id']; ?>)" />
						        </div>
						        <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-sm hidden-xs" onclick ="addtocart(<?php echo $product['product_id']; ?>)">Add to Cart</button>
						        <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
				                    <?php } else { ?>
				                        <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
				                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
      		
      		
      		<!-- sample products -->
      		<!-- sample products -->
            </div>
<div class="col-md-3 lborder<?php echo (!$addresses)?' xblur':''; ?>">  
<div id="sLoader" class="loader"></div>  
      <?php if($address_block && $shipping_required){ ?>
      <?php if($addresses){ ?>
      	<div id="shipping_method" class="panel panel-green1 clearfix animated1 zoomIn1">
		<div class="panel-heading"><i class="fa fa-truck" aria-hidden="true"></i>&nbsp;<?php echo $text_shipping_method; ?></div>
		<div class="shipping-table">
		<?php if($shipping_required && !$xshipping_method) { ?>
		<span class="xerror noshipping marginbottom0"><?php echo $shipping_error; ?></span>
		<?php } else { ?>
		<?php echo $xshipping_method; ?>
		<?php } ?>
		</div>
		</div>
		<div class="clearfix"></div>
		<?php } ?>		
		<?php } ?>
		<?php if($addresses && $address_block && $isMobile){ ?>  
	      	<div id="options" class="animated1 zoomIn1"><?php echo $xoptions;?></div>
	      	<div class="clearfix"></div>
      	<?php } ?>
      	<?php if(!$shipping_required && !$isMobile){ ?>
      	<div id="xcart" class="animated1 zoomIn1"><?php echo $xcart; ?></div>
      	<div class="clearfix"></div>
      	<?php } ?>
       <?php if ($addresses && $address_block) { ?> 
       <?php if(!isset($error_stock_warning) && !isset($error_minimum_warning)){ ?>      
       			<div id="agree-panel" <?php echo ($addresses && $shipping_required && !$xshipping_method)?'style="display:none;"':''; ?> class="agree-panel animated1 zoomIn1">
       			<?php if($text_agree){?>
       				<div id="agreeText" class="agreeText agreeTextalternate text-center">
          				<label class="pointer checkbox is_checkbox"><input type="checkbox" class="input-checkbox" name="agree" value="1" <?php echo ($agree?'checked="checked"':'');?>><span><?php echo $text_agree;?></span></label>
    				</div>       				
    			<?php }?>    			
    			<div class="progress progress-continue-disabled" id="progress-continue-disabled" <?php echo ($text_agree && !$agree?'style="display: block"':'style="display: none"');?> >
                    	<div data-trigger="focus" tabindex="0" data-placement="top" data-toggle="popover" title="<?php echo $info_title;?>" data-content="<?php echo $agree_content;?>" class="progress-bar" role="progressbar" id="button-payment-disabled" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100">
                      		<span><?php echo $text_payment_continue;?><i class="fa fa-lock"></i></span>
                    	</div>
                </div>
        		<div class="progress progress-continue" id="progress-continue" <?php echo ($text_agree && !$agree?'style="display: none"':'');?> >
                  	<div class="progress-bar progress-bar-success" onclick="callPaymentFromAddress();" role="progressbar" id="button-payment" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  		<span><?php echo $text_payment_continue;?></span>
                   	</div>
                	</div>
                 </div>  
       <?php } else{ ?>
	        <?php if(isset($error_stock_warning)) { ?>
				<div class="alert alert-danger noshipping margintop20 marginbottom0"><?php echo $error_stock_warning; ?></div>
			<?php } ?>
			<?php if(isset($error_minimum_warning)) { ?>
				<div class="alert alert-danger noshipping margintop20 marginbottom0"><?php echo $error_minimum_warning; ?></div>
			<?php } ?>
        <?php } ?>	    		

      <?php } ?> 
      <?php if($shipping_required || $isMobile){ ?>
      <div id="xcart" class="animated1 zoomIn1"><?php echo $xcart; ?></div>      
      <div class="clearfix"></div>
      <?php } ?>      
	  <?php if(!$isMobile) { ?>  
      <div id="options" class="animated1 zoomIn1"><?php echo $xoptions;?></div>
      <div class="clearfix"></div>
      <?php } ?>      
	  <div id="totals" class="animated1 zoomIn1"><?php echo $xtotals; ?></div>
	  <div class="clearfix"></div>
    </div>
  </div>
<?php if($mask){ ?>
<script type="text/javascript" src="catalog/view/javascript/xtensions/stylesheet/bs/js/jquery.maskedinput.min.js"></script>
 <script type="text/javascript">
 jQuery(function($){
 <?php foreach ($mask as $key=>$value){ ?>	      
 $("#address_fields #<?php echo $key; ?>").mask("<?php echo $value; ?>");	      
 <?php }?>
 });
 </script>
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/xtensions/stylesheet/bs/js/xcustom.js"></script>
<script type="text/javascript">
address_block = <?php echo $address_block?'true':'false'; ?>;
<?php if (!$addresses) { ?>
$(document).ready(function() {
    $('#addressModal').modal('show');
});
<?php } else { ?>
$(document).ready(function() {
	$('body').removeClass('modal-open');
});
<?php } ?>
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});
$('.date').datetimepicker({
  pickTime: false
});
$('.time').datetimepicker({
  pickDate: false
});
$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});
</script>
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt=""></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    
                    location.reload(); 
                    
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                    ajaxcall();
                    window.location.reload()
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus1(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus1(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('.price-new-' + product_id).html(json.new_price.special);
					$('.price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.coupon) {
					    $('.price-coupon-' + product_id).html(json.new_price.coupon); 
					}
					
					if(json.new_price.special) {
					    $('.price-new-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('.price-new-' + product_id).html(json.new_price.price);
					}
				if(json.new_price.quantity < 1) {
				    
				        $('#button-cart-'+ product_id).html('<?php echo "Preorder"; ?>');
						 $('#button-cart-'+ product_id).addClass('preorder');
                    } else {
                        $('#button-cart-'+ product_id).html('<?php echo "Add to Cart"; ?>');
					    $('#button-cart-'+ product_id).removeClass('preorder');
                    }	
				}
				
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.des-container .options .radio input[type="radio"]').click(function() {
        $('.des-container .options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>


<?php } ?>