<?php echo $header; ?>
<style>
    h4,h5 {
        text-align:center;
    }

    .card{
      background-color: #f1f1f1;
  }

</style>

<ul class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  </li>
  <?php } ?>
</ul>
    <div class="container" style="background-color:#fafbfd; width:auto; margin-top:-35px;">
<div class="container" style="background-color:#fff;">
  <div class="row" style="margin-top: 36px;">
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <div class="col-md-3" style="margin-bottom: 11px;">
        <div class="card row-md-3" style="display: block; text-align: center;">
            <span class="glyphicon glyphicon-user" style="font-size: 150px;"></span>
            <div class="card-body">
                <h5 class="card-title">
                  <b>Welcome : <?php  echo $name; ?></b>
              </h5>
              <p class="card-text"><?php if (isset($email)) { echo $email; } ?></p>

              <ul class="list-inline">
                <li>
                    <div class="btn">
                        <a href="<?php echo $edit; ?>" data-toggle="tooltip" title="Edit Profile"><i class="fa fa-pencil-square-o" style="color:#000;" aria-hidden="true"></i></a>
                    </div>
                    <div class="btn">
                        <a href="<?php echo $logout; ?>" data-toggle="tooltip" title="logout"><i class="fa fa-sign-out" style="color:#000;" aria-hidden="true"></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row-md-9">
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>
    </div>
</div>

<div id="content" class="col-md-9"><?php echo $content_top; ?>
    <div class="col-md-12">
      <div class="row">     
         
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset id="address">          
          <?php foreach ($fields['address'] as $html){ ?>
          <?php echo $html; ?>
          <?php } ?>          
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>
            <div class="col-sm-10">
              <?php if ($default) { ?>
              <label class="radio-inline">
                <input type="radio" name="default" value="1" checked="checked" />
                <?php echo $text_yes; ?></label>
              <label class="radio-inline">
                <input type="radio" name="default" value="0" />
                <?php echo $text_no; ?></label>
              <?php } else { ?>
              <label class="radio-inline">
                <input type="radio" name="default" value="1" />
                <?php echo $text_yes; ?></label>
              <label class="radio-inline">
                <input type="radio" name="default" value="0" checked="checked" />
                <?php echo $text_no; ?></label>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
          </div>
        </div>
      </form>
        
    </div>
</div>

<?php echo $content_bottom; ?>

</div>
<div class="col-sm-12">
  <div class="col-sm-6 text-left"></div>
  <div class="col-sm-6 text-right"></div>
</div>
</div></div></div>
<?php if($mask){ ?>
<script type="text/javascript" src="catalog/view/javascript/xtensions/stylesheet/bs/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript">
 jQuery(function($){
 <?php foreach ($mask as $key=>$value){ ?>        
 $("#<?php echo $key; ?>").mask("<?php echo $value; ?>");         
 <?php }?>
 });
 </script>
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/xtensions/stylesheet/bs/js/xcustom.js"></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

    $('#form-upload input[name=\'file\']').trigger('click');

    if (typeof timer != 'undefined') {
        clearInterval(timer);
    }
    timer = setInterval(function() {
        if ($('#form-upload input[name=\'file\']').val() != '') {
            clearInterval(timer);
        
            $.ajax({
                url: 'index.php?route=tool/upload',
                type: 'post',
                dataType: 'json',
                data: new FormData($('#form-upload')[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(node).button('loading');
                },
                complete: function() {
                    $(node).button('reset');
                },
                success: function(json) {
                    $(node).parent().find('.text-danger').remove();
                    
                    if (json['error']) {
                        $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                    }
    
                    if (json['success']) {
                        alert(json['success']);
    
                        $(node).parent().find('input').val(json['code']);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
    pickTime: false
});

$('.datetime').datetimepicker({
    pickDate: true,
    pickTime: true
});

$('.time').datetimepicker({
    pickDate: false
});
//--></script>
<script type="text/javascript"><!--
$('<?php echo ($display_country?'select':'input');?>[name=\'country_id\']').on('change', function() {
    $.ajax({
        url: 'index.php?route=account/account/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
            $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
        },
        complete: function() {
            $('.fa-spin').remove();
        },
        success: function(json) {
            if (json['postcode_required'] == '1') {
                $('input[name=\'postcode\']').parent().parent().addClass('required');
            } else {
                $('input[name=\'postcode\']').parent().parent().removeClass('required');
            }
    
            html = '<option value=""><?php echo $text_select; ?></option>';
    
            if (json['zone'] && json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';
    
                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                        html += ' selected="selected"';
                    }
    
                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }
    
            $('select[name=\'zone_id\']').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$('<?php echo ($display_country?'select':'input');?>[name=\'country_id\']').trigger('change');
//--></script>
<style type="text/css">
label.control-label span:after {
    font-family: FontAwesome;
    color: #1E91CF;
    content: "\f059";
    margin-left: 4px;
}
</style>
<?php echo $footer; ?>