<div class="panel panel-green1 totalspanel">              
<div class="panel-heading"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;<?php echo $text_totals;?> - <?php echo $final_price; ?><br>
<small>- Coupon discounted amount is excluding GST</small>
</div>
<div id="xtotals-content">        
<table class="table">
<tbody>
    <style>#popup_totals_sub_total,#popup_totals_total_customer_group_discount,#popup_totals_salescombo, #popup_totals_tax { display: none; }</style>
<?php foreach ($totals as $total) { ?>
<tr id="popup_totals_<?php echo $total['code']; ?>"><td><?php echo $total['title']; ?>:</td><td class="text-right"><?php echo $total['text']; ?></td></tr>
<?php } ?>            
</tbody>            
</table>
</div>
</div>