<?php if($inline_view){ ?>
<?php if($display_coupons || $display_vouchers || $display_rewards || $display_comments){?>

<style>
       .centerplease { margin: 0 auto; max-width: 270px; font-size: 40px; } 
        .question { color:#FFF; background: #f49a25; padding: 10px 10px 10px 25px; width:100%; cursor: pointer;border-radius: 7px; } 
        .answers {margin-top: -32px; padding: 0px 15px; margin: 5px 0; height: 0; overflow: hidden; z-index: -1; opacity: 0; -webkit-transition: .7s ease; -moz-transition: .7s ease; -o-transition: .7s ease; transition: .7s ease; } 
        .questions:checked ~ .answers{height: auto;opacity: 1;padding: 0px;padding-top: 15px;} 
        #question1{display:none;}
        
        .button{background: #f49a25;border: 0;font-weight: 600;font-size: 11px;padding: 6px 10px;text-transform: none;color: #fff;-webkit-transition: ease-in-out .3s all;-moz-transition: ease-in-out .3s all;transition: ease-in-out .3s all; }
    /*.applyNow {
        background: #f49a25 !important;
        border: 0;
        font-weight: 600;
        font-size: 11px;
        padding: 6px 10px !important;
        text-transform: none;
        color: #fff;
        -webkit-transition: ease-in-out .3s all;
        -moz-transition: ease-in-out .3s all;
        transition: ease-in-out .3s all;
    }*/
    
    .discount-coupons{
        margin-top: 1%;
    }
    
    .panel-title {
        font-size: 13px;
    }    
    
 
    .btn-cvc-small {
        background-color: #fff !important;
        color: #526cd0 !important;
        border: 1px solid #526cd0 !important;
        font-size: 12px;
        padding: 4px 10px;
    }
    
    #options .table > tbody > tr > td {
        vertical-align: middle;
        font-size: 14px;
    }
    
    #note{
        width: 265px;
        height: 90px;
        border: 3px solid #cccccc;
        padding: 5px;
        font-family: Tahoma, sans-serif;
        background-image: url(bg.gif);
        background-position: bottom right;
        background-repeat: no-repeat;
        margin-bottom: 19px;
    }

</style>
  
<div class="col-md-12 paddingright0 paddingleft0">
<div class="panel panel-green1 couponpanel">
<div class="panel-heading"><i class="fa fa-tags" aria-hidden="true"></i>&nbsp;<?php echo $text_options;?></div>
<div id="xoptions-content">
  
    <?php if($display_coupons){?>
     
    
    <div id="xoptions-content">
        <label class="pointer checkbox is_checkbox gift"><input type="checkbox" class="input-checkbox" name="gift_packaging" value="1" onchange="gift_packaging();" <?php if($gift_packaging == 1) { echo 'checked'; } ?>><span><b>Add gift options&emsp;</b><i class="fa fa-gift" aria-hidden="true" style="font-size: 25px; color: gold; vertical-align: middle;" title="Add gift options"></i></span></label>
        
        <div id="p-note">
            <textarea placeholder="Personal Note!" cols="30" rows="5" name="note" id="note"> <?php echo $note;?></textarea>
        </div>
        
        <table class="table">
            <tbody>
                <tr>          
                   	<td>Coupon</td>
        			<td class="text-right"><a class="btn btn-cvc-small" data-toggle="modal" data-target="#couponModal"><?php if($coupon_value){?> Edit Coupon<?php } else { ?>Select  Coupon <?php } ?></a></td>
                </tr>
            </tbody>            
        </table>
    </div> 
    
    
  <!--form role="form" id="form_add_coupon" action="#" method="post" enctype="multipart/form-data">
  <div id="coupon-panel" class="cvc-panel coupon-panel">
  		<div class="input-group group">
     		<input name="coupon" type="text" id="cpn" value="<?php echo $coupon_value;?>" class="inputMaterial" />
     		<label><?php echo $coupon_apply_text;?>&nbsp;<i class="fa fa-scissors" aria-hidden="true"></i></label>
      		<div class="input-group-addon"><button id="couponbtn1" type="submit"><i class="cvcapply fa fa-check"></i></button></div>
    </div>
  </div>
  </form-->
  <?php } ?>

            
  <?php if($display_vouchers){?>
  
  <div id="xoptions-content">
        <table class="table">
            <tbody>
                <tr>          
                   	<td>Gift Voucher</td>
        			<td class="text-right"><a class="btn btn-cvc-small" data-toggle="modal" data-target="#voucherModal"><?php if($voucher_value){?> Edit Voucher<?php } else { ?>Use Voucher <?php } ?></a></td>
                </tr>
            </tbody>            
        </table>
    </div>
  <!--form role="form" id="form_add_voucher" action="#" method="post" enctype="multipart/form-data">
  <div id="voucher-panel" class="cvc-panel voucher-panel">
  		<div class="input-group group">
     		<input name="voucher" value="<?php echo $voucher_value;?>" type="text" class="inputMaterial"  />
     		<label><?php echo $voucher_apply_text;?>&nbsp;<i class="fa fa-tag" aria-hidden="true"></i></label>
      		<div class="input-group-addon"><button id="voucherbtn1" type="submit"><i class="cvcapply fa fa-check"></i></button></div>
    </div>
  </div>
  </form-->
  <?php } ?> 
  
  <?php if($display_rewards){?>
  <form role="form" id="form_add_reward" action="#" method="post" enctype="multipart/form-data">
  <div id="reward-panel" class="cvc-panel reward-panel">
  		<?php if($hasorders > 0){?>
  		    <div class="input-group group filledalways">
     		    <input name="reward" value="<?php echo $reward_value;?>" type="text" placeholder="Use True Cash" class="numeric inputMaterial" />
     		    <label><?php echo $text_use_reward;?></label>
      		    <div class="input-group-addon"><button id="rewardbtn1" type="submit"><i class="cvcapply fa fa-check"></i></button></div>
            </div>
        <?php } else { ?>
                <div class="input-group group filledalways">
     		    <input type="text" placeholder="Not eligible for first order" class="numeric inputMaterial" style="padding: 0px 0px 0px 14px;" disabled/> 
      		    <div class="input-group-addon" style="background: #dddddd;border: #000;"><button type="submit"><i class="cvcapply fa fa-check"></i></button></div>
            </div>
        <?php } ?>
  </div>
  </form>
  <?php } ?> 
  
  <?php if($display_comments){ ?>
  <div id="comment-panel" class="form-group cvc-panel">
  	<div class="row">
		<span class="col-xs-6"><?php echo $comment_text;?></span>
		<div class="col-xs-6 text-right"><a data-toggle="modal" data-target="#commentModal"><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;<?php echo $comment_apply_text1;?></a></div>
	</div>           
</div>
<?php } ?>

 

<div class="hidden-xs hidden-sm"  style="display: block;margin-top: 6%;">
    <div class="progress progress-continue-disabled" id="progress-disabled" <?php echo ($text_agree && !$agree?'style="display: none"':'style="display: block"');?> >
        <div data-trigger="focus" tabindex="0" data-placement="top" data-toggle="popover" title="<?php echo $info_title;?>" data-content="<?php echo $agree_content;?>" class="progress-bar" role="progressbar" id="button-payment-disabled" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%;text-align: center;">
            <span><?php echo $text_payment_continue;?><i class="fa fa-lock"></i></span>
        </div>
    </div>
    <div class="progress progress-continue" id="progress-continue1" <?php echo ($text_agree && !$agree?'style="display: block"':'');?> >
        <div class="progress-bar progress-bar-success" onclick="callPaymentFromAddress();" role="progressbar" id="button-payment" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%;text-align: center;background-color: #f49a25;color: #fff;">
            <span><?php echo $text_payment_continue;?></span>
        </div>
    </div>
</div>

</div>
</div>
</div>
<?php } ?>
<?php } else { ?>
<?php if($display_coupons || $display_vouchers || $display_rewards || $display_comments){?>
<div class="col-md-12 paddingright0 paddingleft0">
<div class="panel panel-green1 couponpanel">
        <div class="panel-heading"><i class="fa fa-tags" aria-hidden="true"></i>&nbsp;<?php echo $text_options;?></div>
		<div id="xoptions-content">
          <table class="table">
          <tbody>
          	<?php if($display_coupons){?>
          	<tr>
           	<td><?php echo $coupon_text;?></td>
			<td class="text-right"><span style="cursor: pointer;"><a data-toggle="modal" data-target="#couponModal"><?php echo $coupon_apply_text;?></a></span></td>
            </tr>
            <?php } ?>
            <?php if($display_vouchers){?>
            <tr>          
           	<td><?php echo $voucher_text;?></td>
			<td class="text-right"><span style="cursor: pointer;"><a data-toggle="modal" data-target="#voucherModal"><?php echo $voucher_apply_text;?></a></span></td>
            </tr>
            <?php } ?>
            <?php if($display_rewards){?>
          	<tr>
           	<td><?php echo $reward_text;?></td>
			<td class="text-right"><span style="cursor: pointer;"><a data-toggle="modal" data-target="#rewardModal"><?php echo $reward_apply_text;?></a></span></td>
            </tr>
            <?php } ?>           
            <?php if($display_comments){ ?>
            <tr>
           	<td><?php echo $comment_text;?></td>
			<td class="text-right"><span style="cursor: pointer;"><a data-toggle="modal" data-target="#commentModal"><?php echo $comment_apply_text1;?></a></span></td>
            </tr>
            <?php }?>
            </tbody>            
            </table>
            </div>
</div>
</div>
<?php } ?>
<?php } ?>

<script type="text/javascript"><!-- 
    function gift_packaging() {
        if($('.gift input').is(':checked')) {
            var gift_packaging = 1;
        } else {
            var gift_packaging = 0;
        }
        $.ajax({
            url: 'index.php?route=extension/module/xtensions/checkout/xcvc/giftPackaging&gift_packaging=' + gift_packaging,
            dataType: 'json'
        });
    }
    
    $('#button-payment').removeClass('progress-bar-striped active');
    $('.question').click(function() {
        if($('#question1').is(":checked")) {
            $('.pluss').show();
            $('.minuss').hide();
        } else {
            $('.pluss').hide();
            $('.minuss').show();
        }
    }); 
    
    $(document).on('click','.applyNow', function() {
      var coupon_code = $(this).data('coupon_code');
      $('#coupon-panel input[type=\'text\']').val(coupon_code);
      $('#couponbtn1').trigger('click');
      $('#cpn').parent().addClass("filled");
    });
    
    $(document).on('click','#applyNow', function() {
      $('#coupon-panel input[type=\'text\']').val(coupons[coupon_id]['code']);
      $('#cpn').parent().addClass("filled");
      $('#couponbtn1').trigger('click');
    });
</script>
