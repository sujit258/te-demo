<style type="text/css">
  .coupons-list {
    margin-top: 20px;
  }
  .couponblock {
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-size: 12px;
    margin-bottom: 10px;
    cursor: pointer;
  }
  .radio, .checkbox {
    position: relative;
    display: block;
    margin-top: 10px;
    margin-bottom: 10px;
  }
  label.couponlabel {
    padding-left: 0;
  }
  label {
    font-size: 14px;
    font-weight: normal;
  }
  .inner_div {
    border: .5px dashed #aaa;
    padding: 9px;
  }
  .you_save {
    padding-top: 8px;
  }
  .text-right {
    text-align: right;
  }

  .coupon-highlight {
    background: #f4fdfb;
    border: 1px solid #b9f0e5 !important;
  }
  .coupon-highlight .inner_div {
    color: #46BA97;
    border: .5px solid #46BA97 !important;
    background-color: transparent;
  }
  .hidden{
    display:none;
  }
  .showonsuccess {
    padding: 30px 0;
    text-align: center;
    display: block;
  }
  .text-success-coupon {
    text-align: center;
    color: #46BA97;
    font-size: 20px;
    margin-bottom: 20px;
    line-height: 28px;
  } 
  .btnwidth200 {
    width: 120px;
    margin: 0 10px;
  }
  
  .btn-coupon, .btn-coupon:hover {
    border: 1px solid #46BA97 !important;
    border-radius: 5px !important;
    color: #fff !important;
    background-color: #46BA97;
  }
  .btn-coupon:hover {
    box-shadow: 0 1px 2px #ddd;
  }
  .load-complete {
    -webkit-animation: none;
    animation: none;
    border-color: #46BA97;
    transition: border 500ms ease-out;
  }
  .circle-loader {
    margin-bottom: 20px;
    border: 1px solid rgba(0,0,0,0.2);
    border-top-color: rgba(0, 0, 0, 0.2);
    border-right-color: rgba(0, 0, 0, 0.2);
    border-bottom-color: rgba(0, 0, 0, 0.2);
    border-left-color: rgba(0, 0, 0, 0.2);
    border-left-color: #46BA97;
    animation: loader-spin 1.2s infinite linear;
    position: relative;
    display: inline-block;
    vertical-align: top;
    border-radius: 50%;
    width: 7em;
    height: 7em;
  }

  .circle-loader {
    margin-bottom: 20px;
    border: 1px solid rgba(0,0,0,0.2);
    border-top-color: rgba(0, 0, 0, 0.2);
    border-right-color: rgba(0, 0, 0, 0.2);
    border-bottom-color: rgba(0, 0, 0, 0.2);
    border-left-color: rgba(0, 0, 0, 0.2);
    border-left-color: #46BA97;
    animation: loader-spin 1.2s infinite linear;
    position: relative;
    display: inline-block;
    vertical-align: top;
    border-radius: 50%;
    width: 7em;
    height: 7em;
  }

  .checkmark.draw::after {
    animation-duration: 800ms;
    animation-timing-function: ease;
    animation-name: checkmark;
    transform: scaleX(-1) rotate(135deg);
  }
  .checkmark::after {
    opacity: 1;
    height: 3.5em;
    width: 1.75em;
    transform-origin: left top;
    border-right: 3px solid #46BA97;
    border-top: 3px solid #46BA97;
    content: '';
    left: 1.75em;
    top: 3.5em;
    position: absolute;
  }
</style>

<?php if($display_coupons){ ?>
  <div id="couponModal" class="modal fixed"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" id="form_add_coupon" action="#" method="post" enctype="multipart/form-data">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
            <h4 class="modal-title"><?php echo $coupon_apply_text;?></h4>
          </div>

          <div class="showonsuccess" style="display: none;">
            <div class="circle-loader load-complete">
             <div class="checkmark draw" style="display: block;"></div>
           </div><div class="text-success-coupon">Coupon has been applied!</div>
           <div class="text-center"><a data-dismiss="modal" class="btn btn-default btnwidth200">Close</a><a onclick="goback();" class="btn btn-coupon btnwidth200">Show Coupons</a></div>
         </div>

         <div class="hideonsuccess modal-body">
    <!--<div class="form-group">
    <label><?php echo $coupon_text;?></label>
    <input type="text" class="form-control" name="coupon" value="<?php echo $coupon_value;?>" placeholder="<?php echo $coupon_placeholder;?>">
    </div>
    <button id="couponbtn" class="btn btn-success"><?php echo $coupon_apply_text;?></button>-->
    
    <div id="coupon-panel" class="cvc-panel coupon-panel">
      <div class="input-group group coupontext">
        <input name="coupon" type="text" value="<?php echo $coupon_value;?>" class="inputMaterial" />
        <label><?php echo $coupon_apply_text;?>&nbsp;<i class="fa fa-scissors" aria-hidden="true"></i></label>
        <div class="input-group-addon"><button id="couponbtn" type="submit" style="background-color: #f49a25;border: 1px solid #f49a25;"><i class="cvcapply fa fa-check"></i></button></div>
      </div>
      
      <div class="coupons-list">
        <h4>Available Coupons</h4>
        <?php if($coupons) { ?>

          <?php foreach($coupons AS $coupon) { ?>
            <?php if($coupon['coupon_code'] == $coupon_value) { ?>
              <div class="couponblock applyNow coupon-highlight"  data-coupon_code="<?php echo $coupon['coupon_code']; ?>">
              <?php } else { ?>        
                <div class="couponblock applyNow"  data-coupon_code="<?php echo $coupon['coupon_code']; ?>">
                <?php } ?> 
                <div class="row">
                  <div class="col-xs-6">
                    <div class="radio"> 
                      <label for="coupon-<?php echo $coupon['coupon_code']; ?>" class="couponlabel">
                        <span class="inner_div">
                          <input class="input-radio hidden" id="coupon-<?php echo $coupon['coupon_code']; ?>" type="radio" name="radiocoupon" value="<?php echo $coupon['coupon_code']; ?>">
                          <?php echo $coupon['coupon_code']; ?>
                        </span>                           
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="text-right you_save">

                      <div class="msg_you_saved"><b>You Will get  
                        <span class="couponamountsaved">
                          <?php if($coupon['type'] == 'P'){?> <?php echo round($coupon['discount']); ?> % Off<?php } else { ?>Flat Rs.<?php echo round($coupon['discount']); ?> Off<?php } ?>
                        </span></b>
                      </div>
                      <!--div class="msg_free_shipping_right text-right">Free Shipping</div-->

                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="coupon_msg coupon_msg_total"><?php echo $coupon['description']; ?></div> 
                    <div class="coupon_msg coupon_msg_validity">Valid till <?php echo date("d/m/Y", strtotime($coupon['date_end'])); ?>
                  </div>
                </div>
              </div>
            </div> 
          <?php } ?>    
        <?php } ?>
      </div>

    </div>

  </div>
</div>  
</form>   
</div>
</div>
</div>
<?php } ?>
<!-- try me modal -->


<style type="text/css">
  #trymeModal .bounce {
    animation: bounce 2s ease infinite;
  }
  #trymeModal .tryme-img{
    width: 100px;
    position: absolute;
    top: -5%;
    left: 38%;
    border-radius: 53px;
    transform: translatey(0px);
    
  }
  @keyframes bounce {
    70% { transform:translateY(0%); }
    80% { transform:translateY(-15%); }
    90% { transform:translateY(0%); }
    95% { transform:translateY(-7%); }
    97% { transform:translateY(0%); }
    99% { transform:translateY(-3%); }
    100% { transform:translateY(0); }
  }

  #trymeModal .modal-content{
    background: #EDEDED;
    width: 85%;
  }

  #trymeModal .price-new{
    float: right;
  }

  #trymeModal .options{
    min-height: 0 !important;
    margin: 10px 0px;
  }

  #trymeModal .coupon-block{
    font-size: 15px;
  }

   #trymeModal .modal-header{
    border-bottom: 0 !important;
   }

  #trymeModal .price-old {
    font-size: 14px !important;
  }

  #trymeModal .coupons-list{
    display: inline-block;
  }

  .trymeModal-title {
    text-align: center;
    margin: 15px 0;
    color: #000;
    font-size: 17px;
    font-weight: 700;
  }

  .trymeModal-img{
   margin: 8px 0;
 }

 .trymeModal-details{
  border-left: 1px solid #eee;
}

@media screen and (min-width: 300px) and (max-width: 480px){
  #trymeModal .modal-content{
    width: 100%;
  }

  #trymeModal .tryme-img{
    width: 95px;
  }

  #trymeModal .modal-dialog{
    top: 50px !important;
  }
}

@media screen and (min-width: 500px) and (max-width: 900px){
    #trymeModal .modal-dialog{
    top: -15% !important;
  }
}
</style>
<?php if (!$freehitcart) { ?>
<div id="trymeModal" class="modal fixed"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" id="form_add_coupon" action="#" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
          <img class="tryme-img bounce" src="https://harippa.co.in/demo/image/catalog/2021-07-01.jpg">
        </div>
        <div class="hideonsuccess modal-body">
          <div id="coupon-panel" class="cvc-panel coupon-panel">
            <div class="input-group group coupontext" style="display:none">
              <input name="coupon" type="text" value="<?php echo $coupon_value;?>" class="inputMaterial" />
              <label><?php echo $coupon_apply_text;?>&nbsp;<i class="fa fa-scissors" aria-hidden="true"></i></label>
              <div class="input-group-addon"><button id="couponbtn" type="submit" style="background-color: #f49a25;border: 1px solid #f49a25;"><i class="cvcapply fa fa-check"></i></button></div>
            </div>

            <div class="coupons-list">
              <h4 class="trymeModal-title">Please Click On Block</h4>
              <!-- <h4>Available Products</h4> -->
              <?php if($tryproducts) { ?>
                <input type="hidden" name="product_id" value="" />
                <?php foreach($tryproducts AS $tryproduct) { ?>
                  <?php if($tryproduct['product_id'] == $freehitcart) { ?>
                    <div class="couponblock coupon-highlight"  onclick ="addtocart(<?php echo $tryproduct['product_id']; ?>)">
                    <?php } else { ?>        
                      <div class="couponblock"  onclick ="addtocart(<?php echo $tryproduct['product_id']; ?>)">
                      <?php } ?> 
                      <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 trymeModal-img">
                          <img class="img-responsive" src="<?php echo $tryproduct['thumb']; ?>">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 trymeModal-details">
                          <div class="you_save">

                            <div class="msg_you_saved"><b><span class="couponamountsaved"> <?php echo $tryproduct['name']; ?> </span></b><br>
                              <div class="des-container">
                                <?php $is_optionqty = 1; ?>
                                <?php if($tryproduct['options']) { ?>
                                  <div class="options options-<?php echo $tryproduct['product_id']; ?>">
                                    <?php foreach($tryproduct['options'] AS $option) { ?>
                                      <?php $countoption = count($option['product_option_value']); ?>
                                      <?php $price_tooltip_radio = 1; ?>
                                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <?php if($option_value['quantity'] > 0) { ?>
                                        <?php if($option_value['product_option_value_id'] == $tryproduct['base_option_id']) { ?>
                                          <div class="radio">
                                              <label class="checked-option">
                                                <input type="radio" class="option-<?php echo $tryproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $tryproduct['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'enabled'; } ?> checked/>
                                       
                                                <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                  <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                  <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                  <span><?php echo $option_value['name']; ?></span>
                                                <?php } ?>
                                              </label>
                                            </div>
                                          <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                      <?php } ?>
                                    </div>
                                  <?php } else { ?>
                                    <div class="options"></div>
                                    <?php $is_optionqty = 0; ?>
                                  <?php } ?>
                                  <?php if ($tryproduct['price']) { ?>
                                    <p class="price">
                                      <span class="price-new price-new-<?php echo $tryproduct['product_id']; ?>">Rs.00</span>
                                      <?php if (!$tryproduct['special']) { ?>
                                        <span class="price-old price-old-<?php echo $tryproduct['product_id']; ?>"><?php echo $tryproduct['price']; ?></span>
                                      <?php } else { ?>
                                        <span class="price-old price-old-<?php echo $tryproduct['product_id']; ?>"><?php echo $tryproduct['special']; ?></span>
                                        <span class="price-old price-old-<?php echo $tryproduct['product_id']; ?>"><?php echo $tryproduct['price']; ?></span>
                                      <?php } ?>
                                    </p>
                                  <?php } ?>
                                </div>
                              </div>
                              <!--div class="msg_free_shipping_right text-right">Free Shipping</div-->
                            </div>
                          </div>
                        </div>
                      </div>
                    
                  <?php } ?> 
                <?php } ?>    
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php } ?>

<?php if($display_vouchers){ ?>
  <div id="voucherModal" class="modal fixed"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" id="form_add_voucher" action="#" method="post" enctype="multipart/form-data">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
            <h4 class="modal-title">Gift Voucher</h4>
          </div>
          <div class="modal-body">
           <div id="voucher-panel" class="cvc-panel voucher-panel">
            <div class="input-group group"> 
              <input name="voucher" value="<?php echo $voucher_value;?>" type="text" class="inputMaterial"  />
              <label>Enter Gift Voucher Code&nbsp;<i class="fa fa-tag" aria-hidden="true"></i></label>
              <div class="input-group-addon"><button id="voucherbtn" type="submit" style="background-color: #f49a25;border: 1px solid #f49a25;"><i class="cvcapply fa fa-check"></i></button></div>
            </div>
          </div>

        </div>  
      </form>   
    </div>
  </div>
</div>
<?php } ?>
<?php if($display_rewards){ ?>
  <div id="rewardModal" class="modal fixed"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form  role="form" id="form_add_reward" action="#" method="post" enctype="multipart/form-data">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
            <h4 class="modal-title"><?php echo $reward_apply_text;?></h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label><?php echo $text_use_reward;?></label>
              <input type="text" class="form-control" name="reward" value="<?php echo $reward_value;?>" placeholder="<?php echo $entry_reward;?>">
            </div>
            <button id="rewardbtn" class="btn btn-success"><?php echo $reward_apply_text;?></button>
          </div>    
        </form> 
      </div>
    </div>
  </div>
<?php } ?>
<?php if($display_comments){ ?>
  <div id="commentModal" class="modal fixed"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" id="form_add_comment" action="#" method="post" enctype="multipart/form-data">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
            <h4 class="modal-title"><?php echo $comment_apply_text;?></h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label><?php echo $comment_text;?></label>
              <textarea rows="8" cols="40" class="form-control" name="comment" placeholder="<?php echo $comment_apply_text;?>.."><?php echo $comment;?></textarea>        
            </div>
            <button id="commentbtn" class="btn btn-success"><?php echo $comment_apply_text;?></button>
          </div> 
        </form>
      </div>
    </div>
  </div>
<?php } ?>

<script type="text/javascript"><!--

  $(document).on('click','.applyNow', function() {
    var coupon_code = $(this).data('coupon_code');
    $('#coupon-panel input[type=\'text\']').val(coupon_code);
    $('#couponbtn').trigger('click');
    $(this).find('input:radio[name=radiocoupon]').prop('checked',true);
    $('#couponModal .coupon-highlight').removeClass('coupon-highlight'); 
    $('#couponModal .coupontext').addClass('filled');
    $("#couponModal input[name=radiocoupon]:checked").closest('.couponblock').addClass('coupon-highlight');
  });
  
  $(document).on('click','#applyNow', function() {
    $('#coupon-panel input[type=\'text\']').val(coupons[coupon_id]['code']);
    $('#couponbtn').trigger('click');
  });
  
  function goback(){
   $('#couponModal .hideonsuccess').show();
   $('#couponModal .showonsuccess, #couponModal .checkmark').hide();
   $('#couponModal .circle-loader').removeClass('load-complete'); 
 }

</script>
