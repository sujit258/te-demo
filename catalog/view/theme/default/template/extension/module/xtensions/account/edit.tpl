<?php echo $header; ?>
<style>
    h4,h5 {
        text-align:center;
    }

    .card{
      background-color: #f1f1f1;
  }

</style>

<ul class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  </li>
  <?php } ?>
</ul>
    <div class="container" style="background-color:#fafbfd; width:auto; margin-top:-35px;">
<div class="container" style="background-color:#fff;">
  <div class="row" style="margin-top: 36px;">
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <div class="col-md-3" style="margin-bottom: 11px;">
        <div class="card row-md-3" style="display: block; text-align: center;">
            <span class="glyphicon glyphicon-user" style="font-size: 150px;"></span>
            <div class="card-body">
                <h5 class="card-title">
                  <b>Welcome : <?php  echo $name; ?></b>
              </h5>
              <p class="card-text"><?php if (isset($email)) { echo $email; } ?></p>

              <ul class="list-inline">
                <li>
                    <div class="btn">
                        <a href="<?php echo $edit; ?>" data-toggle="tooltip" title="Edit Profile"><i class="fa fa-pencil-square-o" style="color:#000;" aria-hidden="true"></i></a>
                    </div>
                    <div class="btn">
                        <a href="<?php echo $logout; ?>" data-toggle="tooltip" title="logout"><i class="fa fa-sign-out" style="color:#000;" aria-hidden="true"></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row-md-9">
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>
    </div>
</div>

<div id="content" class="well col-md-9" style="background-color: #fff !important;"><?php echo $content_top; ?>
    <div class="col-md-12">
      <div class="row">     
         
          <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
              <h1><?php echo $heading_title; ?></h1>
              <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">        
                  <fieldset id="account">
                      <legend><?php echo $text_your_details; ?></legend>
                      <?php foreach ($fields['account'] as $html){ ?>
                      <?php echo $html; ?>
                      <?php } ?>
                  </fieldset>
                  <div class="buttons clearfix">
                      <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                      <div class="pull-right">
                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
        
    </div>
</div>

<?php echo $content_bottom; ?>

</div>
<div class="col-sm-12">
  <div class="col-sm-6 text-left"></div>
  <div class="col-sm-6 text-right"></div>
</div>
</div></div></div>
<?php if($mask){ ?>
<script type="text/javascript" src="catalog/view/javascript/xtensions/stylesheet/bs/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript">
   jQuery(function($){
       <?php foreach ($mask as $key=>$value){ ?>        
           $("#<?php echo $key; ?>").mask("<?php echo $value; ?>");         
           <?php }?>
       });
   </script>
   <?php } ?>
   <script type="text/javascript" src="catalog/view/javascript/xtensions/stylesheet/bs/js/xcustom.js"></script>
   <script type="text/javascript"><!--
    $('button[id^=\'button-custom-field\']').on('click', function() {
        var node = this;
        
        $('#form-upload').remove();
        
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);
                
                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',       
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,     
                    beforeSend: function() {
                        $(node).button('loading');
                    },
                    complete: function() {
                        $(node).button('reset');            
                    },      
                    success: function(json) {
                        $(node).parent().find('.text-danger').remove();
                        
                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }
                        
                        if (json['success']) {
                            alert(json['success']);
                            
                            $(node).parent().find('input').val(json['code']);
                        }
                    },          
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script> 
    <script type="text/javascript"><!--
        $('.date').datetimepicker({
            pickTime: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });

        $('.time').datetimepicker({
            pickDate: false
        });
        //--></script> 
        <style type="text/css">
            label.control-label span:after {
                font-family: FontAwesome;
                color: #1E91CF;
                content: "\f059";
                margin-left: 4px;
            }
        </style>
        <?php echo $footer; ?>