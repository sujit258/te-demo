<div id="blogsearch">
	<h1><?php echo $text_search; ?></h1>	
	<div  class="input-group">
	  <input type="text" name="blogsearch" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-md" data-url="<?php echo $url; ?>" />
	  <span class="input-group-btn">
	    <button type="button" class="btn btn-primary btn-md"><i class="fa fa-search"></i></button>
	  </span>
	</div>
</div>