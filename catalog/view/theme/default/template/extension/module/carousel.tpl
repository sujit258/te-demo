<div class="container">
    	<div class="new-title">
        <h2><p class="te">True Talk Of Our Customers</p></h2>
    </div>
    <div class="row"><div class="col-sm-4 col-md-4"></div>
<div class="title-line col-sm-4 col-md-4" style="margin-top: -10px;margin-bottom: 20px;">
          <div class="tl-1" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
          <div class="tl-2" style="display: inline-block;height: 13px;margin: 0 5px;position: relative;top: 5px;width: 13px;border: 1px solid #ebc131;border-radius: 50px;"></div>
          <div class="tl-3" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
</div>
<div class="col-sm-4 col-md-4"></div></div>
  <section class="review_part section_padding">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-end">
                <!--div class="col-lg-5 col-xl-4">
                    <div class="tour_pack_text">
                       
                        <p style="color:#000 !important;">Don't just take it from us, let our family of TRUE Customers tell us about their TRUE Stories</p>
                    </div>
                </div-->
                <div class="col-lg-12 col-sm-12">
                    <div id="review_part_cotent" class="review_part_cotent owl-carousel owl-loaded owl-drag">
                     	<?php foreach ($banners as $banner) { ?>   
                        <div class="single_review_part">
                         <a href="<?php echo $banner['link']; ?>">  <img src="<?php echo $banner['image']; ?>" alt="Millet Granola" class="img-responsive"></a>
                        </div>
                    <?php } ?>    
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
var owl = $('#review_part_cotent');
owl.owlCarousel({
      items: 4,
      slideSpeed : 1000,
      loop: true,
      dots: false,
      autoPlay: true,
      //rewindNav: false,
      margin: 40,
      autoplayHoverPause: true,
      autoplayTimeout: 5000,
      nav: true,
      //navText: ['<span class="flaticon-left-arrow"></span>','<span class="flaticon-arrow-pointing-to-right"></span>'],
      responsive: {
        0: {
          nav: false,
          items: 1
        },
        575: {
          nav: false,
          items: 1
        },
        991: {
          nav: true,
          items: 1
        },
        1200: {
          nav: true,
          items: 1
        },
      }
    });

</script>	