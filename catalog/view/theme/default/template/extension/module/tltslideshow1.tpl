<style type="text/css">
    #myCarousel .carousel-indicators .active{
        background: #000;
        border-color: #000;
    }

    #myCarousel .carousel-indicators li{
        background: transparent;
        border-color: #000;
    }

    #myCarousel .glyphicon-chevron-right::before{
        content: "\276F" !important;
        color: #000;
        font-size: 40px;
    }

    #myCarousel .glyphicon-chevron-left::before{
        content: "\276E" !important;
        color: #000;
        font-size: 40px;
    }

  #myCarousel .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next, 
   {
    margin-left: -47% !important;
  }

   #myCarousel .carousel-control .glyphicon-chevron-left, .carousel-control .icon-prev{
    margin-right: -44% !important;
   }

   #myCarousel  .carousel-control
   {
    opacity: 1;
   }

  .carousel.carousel-fade .item {
    -webkit-transition: opacity 10s ease-out;
    -moz-transition: opacity 10s ease-out;
    -ms-transition: opacity 10s ease-out;
    -o-transition: opacity 10s ease-out;
    transition: opacity 10s ease-out;
    transition-duration: 10s;
    opacity:0;
}

.carousel.carousel-fade .item.active {
    opacity: 1 !important;
}

</style>

<?php if ($title) { ?>
<h3><?php echo $title; ?></h3>
<?php } ?>
<div id="tltslideshow<?php echo $slideshow; ?>" class="owl-carousel hidden">
<?php foreach ($slides as $slide) { ?>
    <div class="tltslide">
    <?php if ($slide['link']) { ?>
    <a href="<?php echo $slide['link']; ?>">
        <?php if ($slide['image']) { ?>
            <img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['title']; ?>" class="img-responsive" />
        <?php } else { ?>
            <div style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; margin: 0; padding: 0;">&nbsp;</div>
        <?php } ?>
        <?php if ($slide['textbox']) { ?>
        <?php if ($slide['override']) { ?>
        <div class="<?php echo $slide['css']; ?>" style="background: <?php echo $slide['background']; ?>; opacity: <?php echo $slide['opacity']; ?>;">
        <?php } else { ?>
        <div class="<?php echo $slide['css']; ?>">
        <?php } ?>
        <?php if ($slide['use_html']) { ?>
            <?php echo $slide['html']; ?>
        <?php } else { ?>
            <h1><?php echo $slide['header']; ?></h1>
            <span><?php echo $slide['description']; ?></span>
        <?php } ?>
        </div>
        <?php } ?>
    </a>
    <?php } else { ?>
        <?php if ($slide['image']) { ?>
            <img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['title']; ?>" class="img-responsive" />
        <?php } else { ?>
            <div style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; margin: 0; padding: 0;">&nbsp;</div>
        <?php } ?>
        <?php if ($slide['textbox']) { ?>
        <?php if ($slide['override']) { ?>
        <div class="<?php echo $slide['css']; ?>" style="background: <?php echo $slide['background']; ?>; opacity: <?php echo $slide['opacity']; ?>;">
        <?php } else { ?>
        <div class="<?php echo $slide['css']; ?>">
        <?php } ?>
        <?php if ($slide['use_html']) { ?>
            <?php echo $slide['html']; ?>
        <?php } else { ?>
            <h1><?php echo $slide['header']; ?></h1>
            <span><?php echo $slide['description']; ?></span>
        <?php } ?>
        </div>
        <?php } ?>
    <?php } ?>
    </div>
 <?php } ?>
</div>   
</div>  
<div class="" > 

<link href="catalog/view/theme/tt_presiden10/stylesheet/mobile/mobile-style.css" rel="stylesheet">

<link href="catalog/view/theme/tt_presiden10/stylesheet/mobile/media.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.min.js"></script>

<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="4000">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
     <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php $i = 0; ?>
    <?php foreach ($slides as $slide) { ?>
    <div class="item <?php if($i==0){ echo 'active';} ?>">
      <img class="hidden-xl hidden-lg hidden-sm hidden-md" src="<?php echo $slide['mobileimage']; ?>" alt="image">
       <img class="hidden-xs" src="<?php echo $slide['image']; ?>" alt="image">
    </div>
    <?php $i++; ?>
 <?php } ?>
</div>
 <!-- Left and right controls -->
  <a class="left carousel-control hidden-xs" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control hidden-xs" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<style type="text/css">
    .carousel-control {
        background-image: none !important;
    }
</style>
<script type="text/javascript">
if ($(window).width() > 780) {
    var owl = $("#tltslideshow<?php echo $slideshow; ?>");
    owl.owlCarousel({
        items:1,
        loop:true,
        autoplay:true,
        pagination: true, 
        autoplayTimeout:5000,
        navigation: true,
        controls: true
    });

}


/*if ($(window).width() < 779) {
      
    var owl = $("#tltslideshow1<?php echo $slideshow; ?>");
    owl.owlCarousel({
        items:1,
        loop:true,
        autoplay:true,
        pagination: true, 
        autoplayTimeout:5000,
        navigation: true,
        controls: false
    });

}*/

</script>
<script type="text/javascript">
  $(".carousel").swipe({
    excludedElements: "input, select, textarea, .noSwipe",
    swipeLeft: function() {
      $(this).carousel('next');
    },
    swipeRight: function() {
      $(this).carousel('prev');
    },
    allowPageScroll: 'vertical'
  });
</script>