<style>
    .has-option {
        border: none!important;
        box-shadow: none!important;
    }
  
    body {
        font-size: 15px !important;
    }
    
    .disc1 ul li {
        margin: 0px;
        list-style-type: disc !important;
    }
  
    .options .radio input {
        display: none !important;
    }
    
    .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    
    .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    
    .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    
    .radio label span {
        display: block;
        font-size: 13px;
    }
    
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .old-price{
        color:#000;
    }
     
    .options {
        min-height: 55px;
    }
    
    .product-options {
        margin-top: 10px;
        text-align: center;
        float: left;
        background: #fff!important;
    }
    
    .price-label {
        padding: 8px 0px !important;
        min-height: 37px;
        width: 100% !important;
    }
    
    
    @media(max-width: 1000px) {
        
        .radio label span {
            font-size: 12px !important;
        }
        
        .des-container .options .radio {
            margin: 0px 0px 5px -2px !important;
        }
        
        .options {
            min-height: 55px !important;
        }
        
        .category-products {
            padding: 0!important;
        }
        
        .product-container {
            padding: 7px !important;
        }
    }
    
    @media(min-width: 800px) {
        .des-container {
            height: 200px;
        }
    }
    
    .item-inner .des-container {
        padding:10px !important;
    }
        
    .oos {
        background: #777!important;
        border-radius: 4px !important;
        font-size: 13px !important;
        padding: 8px 10px !important;
    }
    
    .oos:hover {
        cursor: not-allowed!important;
    }
    
    .des-container .options .radio input {
        display: none !important;
    }
    
    .des-container .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    
    .des-container .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    
    .des-container .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    
    .radio label span {
        display: block;
        font-size: 13px;
    }
    
    .price-label {
        padding: 8px 0px !important;
        min-height: 37px;
        width: 100% !important;
    }
    
    .price {
        padding-left: 0px !important;
        padding-right: 0px !important;
        font-size: 15px;
        color: #f49a25;
        text-align: left;
        font-weight: 700;
        margin: 0;
    }
     
    .special-price .price {
        color: #f49a25;
    }
    
    .old-price {
        display: inline-block;
        margin: 0;
        color: #000;
    }
    
    .old-price .price {
        color: #5c5c5c;
        font-size: 12px;
        margin-left: 5px;
        text-decoration: line-through;
    }
    
    .price-box {
        display: block ruby;
    }
    
    .cart-edit-modal {
        padding: 0 40px 0 50px;
    }
    
    @media(max-width: 800px) {
         
        .cart-edit-modal {
            padding: 0;
        }
        
        .des-container {
            padding: 21px !important;
        }
    }
    
    .update-option { 
        background: #f49a25;
        border: 0;
        font-weight: 600;
        font-size: 16px;
        padding: 7px 16px;
        text-transform: none;
        color: #fff;
        -webkit-transition: ease-in-out .3s all;
        -moz-transition: ease-in-out .3s all;
        transition: ease-in-out .3s all;
    }
    
</style>
<div id="digitcart-edit-cart-options" class="des-container edit-cart">
    <div class="row" style="margin-top: 13px;">
        <div class="col-md-2 col-xs-4 col-sm-4">
            <div class="text-center"> 
                <img src="<?php echo $image; ?>" alt="Gluten Free Rolled Oats - Protein Rich Oats" title="Gluten Free Rolled Oats - Protein Rich Oats" class="img-thumbnail"> 
            </div>
        </div>                        
        <div class="col-md-10 col-xs-8 col-sm-8">
            <div class="price-rating">
                <div class="price-label">
                    <div class="box-price">
                        <?php if ($price) { ?>
                            <?php if (!$special) { ?>
                                <div class="price-box box-regular">
                                    <span class="regular-price">
                                    <span class="price-new-<?php echo $product_id; ?> price"><?php echo $price; ?></span>
                                    </span>
                                </div>
                            <?php } else { ?>
                                <div class="price-box box-special">
                                    <p class="special-price"><span class="price-new-<?php echo $product_id; ?> price"><?php echo $special; ?></span></p>
                                    <p class="price-old-<?php echo $product_id; ?> old-price"><span class="price"><?php echo $price; ?></span></p>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php $is_optionqty = 1; ?>
    	<?php if ($options) { ?>
            <div class="options options-<?php echo $product_id; ?>">
                <?php foreach($options AS $option) { ?>
                    <?php $countoption = count($option['product_option_value']); ?>
                        <?php $price_tooltip_radio = 1; ?>
                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                <div class="radio">
                                    <?php if($countoption == 1) { ?>
                                        <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                        <input type="radio" class="option-<?php echo $product_id; ?>" onchange="price_with_options_ajax_call1(<?php echo $product_id; ?>,'digitcart-edit-cart-options')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                    <?php } else { ?>
                                        <?php if($option_value['product_option_value_id'] == $cart_option) { ?>
                                            <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                            <input type="radio" class="option-<?php echo $product_id; ?>" onchange="price_with_options_ajax_call1(<?php echo $product_id; ?>,'digitcart-edit-cart-options')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                        <?php } else { ?>
                                            <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                            <input type="radio" class="option-<?php echo $product_id; ?>" onchange="price_with_options_ajax_call1(<?php echo $product_id; ?>,'digitcart-edit-cart-options')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                        <?php } ?>        
                                    <?php } ?>
                                        <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                            <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                        <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                        <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?></span>
                                        <?php } ?>
                                    </label>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?> 
                
                <div class="pull-right">
                    <button class="btn btn-primary update-option" id="editCartOptionsButton" type="button" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-refresh"></i> <?php echo $button_update; ?></button>
                </div>
                
        </div>            
    </div>
	<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
	<input type="hidden" name="cart_id" value="<?php echo $cart_id; ?>" />
	
</div>

<script type="text/javascript">
    function price_with_options_ajax_call1(product_id, id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('#'+id+' .checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
				   
					$('#'+id+' .price-new-' + product_id).html(json.new_price.special);
					$('#'+id+' .price-old-' + product_id).html('<span class="price">' + json.new_price.price + '</span>');
					
					if(json.new_price.coupon) {
					     $('#'+id+' .price-new-' + product_id).html(json.new_price.coupon);
					}
					
					if(json.new_price.special) {
					    $('#'+id+' .price-new-' + product_id).html(json.new_price.special);
					    $('#'+id+' .price-old-' + product_id).html('<span class="price">' + json.new_price.price + '</span>');
					} else {
					    $('#'+id+' .price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>


 <script type="text/javascript"><!--
  $('#digitcart-edit-cart-options input[type="radio"], #digitcart-edit-cart-options input[type="checkbox"]').click(function() {
    $('#digitcart-edit-cart-options input[type="radio"], #digitcart-edit-cart-options input[type="checkbox"]').each(function() {
      $(this).parents("label").toggleClass('checked-option', this.checked);
    });
  });
  //--></script>