<!--

<?php
	$count = 0;
	$rows = $slide['rows'];
	if(!$rows) { $rows = 1; }
?>


<div class="testimonial-container">
	<div class="container">
		<div class="module-title block-title">
			<h2 style="color:#000000"><?php echo $title; ?></h2>
		</div>
	
			<div id="slides">
				<?php foreach($testimonials as $testimonial) { ?>
					<?php  if($count % $rows == 0 ) { echo '<div class="row_items">'; } $count++; ?>
					<div class="testimonial-inner">
					    	<div class="testimonial-content">
					    <div class="testimonial-image">
							    <img src="<?php echo $testimonial['image'];?>" alt="<?php echo $testimonial['customer_name'];?>">
							</div>
							</div>
					<div style="margin: 4px 46px 0px 35px;">
							<p style="color:#000000;"><?php echo substr($testimonial['content'],0,600)."..."; ?></p>
						</div>
						   <center>
						
								<span style="color:#000000" class="testimonial-author"><?php echo $testimonial['customer_name']; ?></span>
							
						</center>
						
					</div>
					
					<?php if($count % $rows == 0 || $count == count($testimonials)): ?>
					</div>
					<?php endif; ?>
				<?php  } ?>
		
		</div>
	</div>
</div>
<script type="text/javascript">
    $("#slides").owlCarousel({
		autoPlay : <?php if($slide['auto']) { echo 'true' ;} else { echo 'false'; } ?>,
		items : <?php echo $slide['items'] ?>,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
		slideSpeed : <?php echo $slide['speed']; ?>,
		paginationSpeed : <?php echo $slide['speed']; ?>,
		rewindSpeed : <?php echo $slide['speed']; ?>,
		navigation : <?php if($slide['navigation']) { echo 'true' ;} else { echo 'false'; } ?>,
		pagination : <?php if($slide['pagination']) { echo 'true' ;} else { echo 'false'; } ?>
    });
</script> -->

<div class="container">
  <section class="review_part section_padding">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-end">
                <div class="col-lg-5 col-xl-4">
                    <div class="tour_pack_text">
                        <h2>Our <span class="pink">Partners</span></h2>
                        <p>Which cattle fruitful he fly visi won not let above lesser stars fly form wonder every let third form two air seas after us said day won lso together midst two female she</p>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div id="review_part_cotent" class="review_part_cotent owl-carousel owl-loaded owl-drag">
                        <div class="single_review_part">
                            <img src="image/img/DocsApp-logo.png" alt="">
                            <div class="tour_pack_content">
                                <p>Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Docsapp</h4>
                            </div>
                        </div>
                        <div class="single_review_part">
                            <img src="image/img/ssg.png" alt="">
                            <div class="tour_pack_content">
                                <p> Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Step Set Go</h4>
                            </div>
                        </div>
                       <div class="single_review_part">
                            <img src="image/img/DocsApp-logo.png" alt="">
                            <div class="tour_pack_content">
                                <p>Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Docsapp</h4>
                            </div>
                        </div>
                        <div class="single_review_part">
                            <img src="image/img/ssg.png" alt="">
                            <div class="tour_pack_content">
                                <p> Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Step Set Go</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
var owl = $('#review_part_cotent');
owl.owlCarousel({
      items: 1,
      loop: true,
      dots: false,
      autoplay: true,
      margin: 40,
      autoplayHoverPause: true,
      autoplayTimeout: 5000,
      nav: true,
      navText: ['<span class="flaticon-left-arrow"></span>','<span class="flaticon-arrow-pointing-to-right"></span>'],
      responsive: {
        0: {
          nav: false,
          items: 1
        },
        575: {
          nav: false,
          items: 1
        },
        991: {
          nav: true,
          items: 1
        },
        1200: {
          nav: true,
          items: 1
        },
      }
    });

</script>	