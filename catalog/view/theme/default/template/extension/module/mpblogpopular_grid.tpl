<div class="mp-grid-layout mp-blog <?php echo $themeclass; ?>">
  <h1><?php echo $heading_title; ?></h1>
  <div class="row">
    <?php foreach($mpblogposts as $mpblogpost) { ?>
    <div class="mpblog-layout col-lg-4 col-md-4 col-sm-6 col-xs-12 xs-100 sm-50 md-50 lg-33 xl-33">
      <div class="inner-layout">
        <div class="image">
          <?php if($mpblogpost['showImage']) { ?>
          <div class="video-container"><a href="<?php echo $mpblogpost['href']; ?>"><img src="<?php echo $mpblogpost['thumb']; ?>" alt="<?php echo $mpblogpost['name']; ?>" title="<?php echo $mpblogpost['name']; ?>" class="img-responsive" /></a></div>
          <?php } else { ?>
          <div class="video-container"><iframe src="<?php echo $mpblogpost['iframeVideo']; ?>" frameborder="0" allowfullscreen></iframe></div>
          <?php } ?>
          <?php if($show_viewed) { ?>
            <div class="view" title="<?php echo $mpblogpost['viewed']; ?>">
              <i class="fa fa-eye"></i>
              <?php echo $mpblogpost['viewed']; ?>
            </div>
          <?php } ?>
        </div>
        <div class="caption">
          <h4><a href="<?php echo $mpblogpost['href']; ?>"><?php echo $mpblogpost['name']; ?></a></h4>
            <ul class="dar clearfix list-inline mpblogpost-id-<?php echo $mpblogpost['mpblogpost_id']; ?>">
                    <li class="col-xs-6 col-sm-6 xl-50">
                    <?php if($show_date) { ?>
                      <i class="fa fa-calendar"></i>
                      <?php if(!empty($mpblogpost['date_availableurl'])) {  ?>
                      <a href="<?php echo $mpblogpost['date_availableurl']; ?>"><?php echo $mpblogpost['date_available']; ?></a>
                      <?php } else { ?>
                      <?php echo $mpblogpost['date_available']; ?>
                      <?php } ?>
                    <?php } ?>
                    </li>
                    <?php if($show_author) { ?>
                    <li title="<?php echo $mpblogpost['author']; ?>" class="author col-xs-6 col-sm-6 xl-50">
                      <i class="fa fa-user"></i>                    
                      <?php if(!empty($mpblogpost['authorurl'])) { ?>
                      <a href="<?php echo $mpblogpost['authorurl']; ?>">
                        <?php echo $mpblogpost['author']; ?>
                      </a>
                      <?php } else { ?>
                      <?php echo $mpblogpost['author']; ?>
                      <?php } ?>
                    </li>
                    <?php } ?>
                    <?php if($show_rating) { ?>
                    <li class="col-sm-12 col-xs-12 xl-100" title="<?php echo $mpblogpost['rating']; ?>">
                      <div class="rating small-rating text-center">
                        <div class="rating-icons-container text-center">
                        <div class="rating-icons-wrap"><?php 
                         for ($i = 1; $i <= 5; $i++) { 
                          if ($mpblogpost['rating'] < $i) {
                          $rdecimal = 'EMPTY_STAR';
                          if(ceil ($mpblogpost['rating']) == $i) {
                            $rates = explode('.', $mpblogpost['rating']);
                            if(!empty($rates[1]) ) {
                              if((float)"0.$rates[1]" < 0.6000) {
                                $rdecimal = 'HALF_STAR';
                              }
                              if((float)"0.$rates[1]" > 0.6000) {
                                $rdecimal = 'FULL_STAR';
                              }
                            }
                          }
                        if($rdecimal=='HALF_STAR') { ?>
                          <div class="rating-icons half">
                            <span class="red"></span>  
                            <span class="grey"></span>  
                            <i class="fa fa-star"></i>
                          </div>
                        <?php } if($rdecimal=='FULL_STAR') { ?>
                          <div class="rating-icons full">
                            <span class="red"></span>  
                            <span class="grey"></span>  
                            <i class="fa fa-star"></i>
                          </div>
                        <?php } if($rdecimal=='EMPTY_STAR') { ?>
                          <div class="rating-icons">
                            <span class="red"></span>  
                            <span class="grey"></span>  
                            <i class="fa fa-star"></i>
                          </div>
                        <?php } } else { ?>
                        <div class="rating-icons full">
                          <span class="red"></span>  
                          <span class="grey"></span>  
                          <i class="fa fa-star"></i>
                        </div>
                        <?php } ?>
                        <?php } ?>
                        </div>
                        </div>
                      </div>
                    </li>
                    <?php } ?>
                  </ul>
            <?php if($show_sdescription) { ?><p class="desc"><?php echo $mpblogpost['sdescription']; ?> <?php if(!empty($mpblogpost['sdescription']) && $show_readmore) { ?> <a href="<?php echo $mpblogpost['href'];?>">  <?php echo $text_readmore; ?> </a> <?php } ?></p> <?php } ?> 
            <ul class="list-inline dar cmt-wsh clearfix">
              <?php if($show_wishlist) { ?>
              <li title="<?php echo $mpblogpost['wishlist']; ?>">
                <span class="mpbloglike <?php if($mpblogpost['isLikeByMe']) { echo 'liked'; } ?>" data-id="<?php echo $mpblogpost['mpblogpost_id']; ?>">
                <i class="fa fa-heart <?php if(!$mpblogpost['isLikeByMe']) { echo 'fa-heart-o'; } ?>"></i>
                <span><?php echo $mpblogpost['wishlist']; ?></span>
                </span>
              </li>
            <?php } ?>
            <?php if($show_comments) { ?>
              <li class="pull-right" title="<?php echo $mpblogpost['comments']; ?>">
                <i class="fa <?php if($mpblogpost['comments']) { echo 'fa-comments'; } else { echo 'fa-comments-o'; } ?>"></i>
                <?php echo $mpblogpost['comments'].' '. $text_comment; ?>
              </li>
              <?php } ?>
            </ul>
            <?php if($show_tag && $mpblogpost['tag']) { ?>
              <ul class="list-inline blog-tags">
                <li>
                  <i class="fa fa-tags"></i>
                  <?php foreach($mpblogpost['tag'] as $rtag) { ?>
                  <?php if(!empty($rtag['href'])) { ?>
                  <a href="<?php echo $rtag['href']; ?>"><?php echo $rtag['tag']; ?></a>
                  <?php } else { ?>
                  <?php echo $rtag['tag']; ?>
                  <?php } ?>
                  ,
                  <?php } ?>
                </li>
              </ul>
            <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
</div>