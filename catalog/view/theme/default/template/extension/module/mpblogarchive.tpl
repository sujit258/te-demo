<div class="mp-list-group">
<h1><?php echo $heading_title; ?></h1>
  <?php foreach($hrefs as $href) { ?>
   <a href="<?php echo $href['href']; ?>" class="list-group-item <?php if($href['selected']) { echo 'active'; }?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $href['title']; ?></a>
   <?php } ?>
</div>