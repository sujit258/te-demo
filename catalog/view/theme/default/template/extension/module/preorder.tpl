<?php if($data['preorder']['Enabled'] == 'yes'): ?>

    <script>
        $(document).ready( function() { 
            checkQuantityP();
            $('input[name*=option], select[name*=option]').on('change', function() {
                checkQuantityP();
            });
        });
        
        // Journal filetrs Compatibility
        $(document).ajaxComplete(function( event,request, settings ) {
            var current_url = settings.url;
            
            if (current_url.indexOf("journal2_super_filter/products") > 1) {
                product_id = null;
                checkQuantityP();
            }

        });
    
        var checkQuantityP = function () { 
            var product_ids=[];
            var product_id;
            if($('[onclick^="cart.add"]').length > 0 ) {
                for (i = 0; i < $('[onclick^="cart.add"]').length; i++) { 
                    onclick_string = $('[onclick^="cart.add"]')[i]['attributes']['onclick']['value'];
                    split_onclick_string = onclick_string.split("'");
                    product_id=split_onclick_string[1];
                    product_ids.push(product_id);
                }
                setTimeout(function(){
                    checkPreorderedProducts(product_ids);
                }, 500);

            } else if ($('[onclick^="addToCart"]').length > 0 ) {
                for (i = 0; i < $('[onclick^="addToCart"]').length; i++) { 
                    onclick_string = $('[onclick^="addToCart"]')[i]['attributes']['onclick']['value'];
                    split_onclick_string = onclick_string.split("'");
                    product_id=split_onclick_string[1];
                    product_ids.push(product_id);
                }
                setTimeout(function(){
                    checkPreorderedProductsJournal(product_ids);
                }, 500);

            }

            var query = $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea').serialize();

            $.ajax({
                url: 'index.php?route=checkout/cart/checkQuantityPO',
                type: 'post',
                data: query,
                dataType: 'json',
                success: function(json) {
                    if(json['PO']) {
                        $('#button-cart').html('<?php echo $preorder_Button; ?>');
                        $('#button-cart').val('<?php echo $preorder_Button; ?>');
                        $('#button-cart span').text('<?php echo $preorder_Button; ?>');
                        $('#button-cart').parent().parent().removeClass('outofstock');
						 $('#button-cart').addClass('preorder');
                    } else {
                        $('#button-cart').button('reset');
						$('#button-cart').html('<?php echo $button_cart; ?>');
                        $('#button-cart').val('<?php echo $button_cart; ?>');
						$('#button-cart span').text('<?php echo $button_cart; ?>');
					    $('#button-cart').removeClass('preorder');
                    }
                }
            });
        }
        
        var checkPreorderedProducts = function(product_ids) {
            $.ajax({
                url: 'index.php?route=checkout/cart/checkQuantityPO',
                type: 'post',
                data: {product_id:product_ids},
                dataType: 'json',
                success: function(json) {
                    if(json['PO']) {
                            for(product_id=0; product_id < json['PO'].length; product_id++) {
                                $('[onclick^="cart.add(\''+json['PO'][product_id]+'\'"]').children('span').html('<?php echo $preorder_Button; ?>');
                                $('[onclick^="cart.add(\''+json['PO'][product_id]+'\'"]').parent().removeClass('outofstock');
                            } 
                    } 
                }
            });
        }
        
        var checkPreorderedProductsJournal = function(product_ids) {
            $.ajax({
                url: 'index.php?route=checkout/cart/checkQuantityPO',
                type: 'post',
                data: {product_id:product_ids},
                dataType: 'json',
                success: function(json) {
                    if(json['PO']){
                        for(product_id=0; product_id < json['PO'].length; product_id++) {
                            $('[onclick^="addToCart(\''+json['PO'][product_id]+'\'"]').children('span').html('<?php echo $preorder_Button; ?>');
                            $('[onclick^="addToCart(\''+json['PO'][product_id]+'\'"]').attr('data-hint','<?php echo $preorder_Button; ?>');
                            $('[onclick^="addToCart(\''+json['PO'][product_id]+'\'"]').parent().removeClass('outofstock');
                        } 
                    } 
                }
            });
        }
        
    </script>
<?php endif; ?>