<div class="mp-list-group">
<h1><?php echo $heading_title; ?></h1>
  <?php foreach ($mpblogcategories as $mpblogcategory) { ?>
  <?php if ($mpblogcategory['mpblogcategory_id'] == $mpblogcategory_id) { ?>
  <a href="<?php echo $mpblogcategory['href']; ?>" class="list-group-item active"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $mpblogcategory['name']; ?></a>
  <?php if ($mpblogcategory['children']) { ?>
  <?php foreach ($mpblogcategory['children'] as $child) { ?>
  <?php if ($child['mpblogcategory_id'] == $child_id) { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } else { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } ?>
  <?php } ?>
  <?php } ?>
  <?php } else { ?>
  <a href="<?php echo $mpblogcategory['href']; ?>" class="list-group-item"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $mpblogcategory['name']; ?></a>
  <?php } ?>
  <?php } ?>
</div>
