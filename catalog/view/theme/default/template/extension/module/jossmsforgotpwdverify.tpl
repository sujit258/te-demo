<fieldset>
	<div id="step0">
		
	  <legend><?php echo $heading_title; ?></legend>
	  <div class="warning" style="display:none" id="ovwarn"></div>
		<div class="success" style="display:none" id="sendsuccess"></div>
		
		<div class="buttons" id="step1">
		  <div class="form-group required">
		    <label class="col-sm-2 control-label" for="phone">
					<?php echo $text_phone?>:
		    </label>
		    <div class="col-sm-10">
		      <input type="text" name="phone" value="<?php echo $phone; ?>" placeholder="<?php echo $text_phone; ?>" id="phone" class="form-control" />
		      <?php $the_text = sprintf($text_explain1, $phone); echo "$the_text";?>
		      <div>
					<a id="button-startver" class="btn btn-primary"><span><?php echo $text_start?></span></a>
					</div>
		    </div>
		  </div>
	  </div>
	  
	  <div id="step2" style="display:none" class="buttons">
		  <div class="form-group">
		    <label class="col-sm-2 control-label" for="pin">
					<?php echo $text_verification_code?>:
		    </label>
		    <div class="col-sm-10">
		      <input type="text" name="pin" value="<?php echo $phone; ?>" placeholder="<?php echo $text_verification_code; ?>" id="pin" class="form-control" />
		      <?php echo $text_explain_started;?>
		      <div>
					<div> <a id="button-confirm" class="btn btn-primary"><span><?php echo $text_verify;?></span></a> <a id="button-startver2" class="btn btn-warning"><span><?php echo $text_resend;?></span></a></div>
					</div>
		    </div>
		  </div>
	  </div>
	</div>
</fieldset>

<script type="text/javascript"><!--
	var verified = '<?php echo $verified?>';
	if (verified != '1') {
		$("#continue").hide();
	}
	
var pinsent = '<?php echo $pinsent?>';
if (pinsent=='1') {
		
		$("#step1").hide();
		$("#step2").show();
		$("#type1").hide();
		$("#type2").hide();
}
$('#button-confirm').bind('click', function() {
	$.ajax({ 
		type: 'POST',
		data: 'pin=' + $('#pin').val(),
		url: 'index.php?route=extension/module/jossmsforgotpwdverify/confirm',
		success: function(data) {
				if (data==1) {
					$("#ovwarn").hide();
					$("#sendsuccess").hide();
					$("#step2").hide();
					$("#step0").hide();
					$("#continue").show();
				}else if (data==3) {
				$("#sendsuccess").hide();
				$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_null_number?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
				$("#ovwarn").show();
				}
				else {
				$("#sendsuccess").hide();
				$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_invalid_pin?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
				$("#ovwarn").show();
				}
		}		
	});
});
$('#button-startver2').bind('click', function() {
	$("#ovwarn").hide();
	$("#sendsuccess").hide();
	$("#step2").hide();
	$("#step1").show();
});
var wait = 0;
$('#button-startver').bind('click', function() {

	if (!wait) {
		wait = 1;
		$.ajax({ 
			type: 'POST',
			data: 'phone='+$('#phone').val(),
			url: 'index.php?route=extension/module/jossmsforgotpwdverify/start',
			success: function(data) {
			wait = 0;
				switch (data) {
				case "1":
					$("#sendsuccess").hide();
					$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_different_number;?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
					$("#ovwarn").show();
				break;
				case "5":
					$("#sendsuccess").hide();
					$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_provide_valid_mobile_number;?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
					$("#ovwarn").show();
				break;
				case "2":
				case "Success":
						$("#sendsuccess").html("<div class='alert alert-success'><i class='fa fa-exclamation-circle'></i> <?php echo $text_send_success?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
						$("#sendsuccess").show();
						$("#ovwarn").hide();
						$("#step1").hide();
						$("#step2").show();	
						$("#type1").hide();
						$("#type2").hide();
						$("#type"+data).show();
				break;
				case "15":
					$("#sendsuccess").hide();
					$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_explain_same_number;?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
					$("#ovwarn").show();
				break;
				case "14":
					$("#sendsuccess").hide();
					$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_please_wait_next;?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
					$("#ovwarn").show();
				break;
				case "12":
					$("#sendsuccess").hide();
					$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_max_retries_exceeded;?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
					$("#ovwarn").show();
				break;
				case "16":
					$("#sendsuccess").hide();
					$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_connection_problem;?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
					$("#ovwarn").show();
				break;
				default:
					$("#sendsuccess").hide();
					$("#ovwarn").html("<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> <?php echo $text_provide_valid_number;?><button type='button' class='close' data-dismiss='alert'>&times;</button></div>");
					$("#ovwarn").show();
					
				}
			}		
		});
	}
	else alert('<?php echo $text_please_wait;?>');
});
//--></script> 