 
<div class="islip-modal-dialog th-isl-<?php echo $theme; ?>">
  <div class="row">

    <div class="col-md-7 uk-text-center">
      <a href="https://www.instagram.com/p/<?php echo $photo['shortcode']; ?>/" target="_blank">
        <img src="<?php echo $photo['image_thumb']; ?>" style="padding:10%" class="img-responsive">
      </a>
    </div>

    <div class="col-md-5">

      <div class="islip-modal-info">
        <h3 class="photo-ig-username">
          <i class="fa fa-instagram"></i>
          <a href="https://www.instagram.com/<?php echo $photo['username']; ?>" target="_blank"><?php echo $photo['username']; ?></a>
        </h3>
        <p class="photo-ig-info">
          <span style="margin-right:15px"><?php echo $photo['date']; ?></span>
          <?php if (!empty($photo['like'])) { ?>
            <span style="margin-right:10px"><i class="fa fa-heart"></i> <?php echo $photo['like']; ?> <?php echo $text_likes; ?></span>
            <span style="margin-right:10px"><i class="fa fa-comment"></i> <?php echo $photo['comment']; ?> <?php echo $text_comments; ?></span>
          <?php } ?>
        </p>

        <div class="photo-ig-caption caption-truncate">
          <p><?php echo $photo['caption']; ?></p>
          <div class="photo-ig-caption-overlay">
            <span class="photo-ig-caption-more label label-primary">More</span>
            <span class="photo-ig-caption-less label label-success">Less</span>
          </div>
        </div>

        <?php if ($products) { ?>
        <input type="hidden" name="product_id" value="" />
          <hr>
          <div class="swiper-viewport islip-related-product">
            <div id="islip-swiper-<?php echo $photo['shortcode']; ?>" class="islip-gutter-5px swiper-container">

              <div class="<?php echo $carousel ? 'swiper-wrapper' : ''; ?>">
              <?php foreach ($products as $product) { ?>
                  <div class="<?php echo $carousel ? 'swiper-slide' : 'col-xs-12'; ?>">
                    <div class="product-thumb" style="padding-bottom: 35px;">
                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                            <div class="image">
                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                      </div></div> 
                    <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                      
                      <div class="text-center">
                        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                        <div class="price-label <?php if($product['price'] == '') { echo  'hidden-sm hidden-xs hidden-sm'; } ?>">
						    <div class="box-price">
						        <?php if ($product['price']) { ?>
						            <p class="price">
						                <?php if (!$product['special']) { ?>
                                            <?php if ($product['joseanmatias_preco_cupom']) { ?>
                                                <strike class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price'],"  ",$product['joseanmatias_preco_cupom']; ?></strike>
                                            <?php } else { ?>
                                               <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
                                            <?php } ?>
						                <?php } else { ?>
						                    <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
						                    <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
						                <?php } ?>
						            </p>
						        <?php } ?>
						    </div>
				        </div>
                      </div>   
                      </div>
                      <br>
                      <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                      <?php $is_optionqty = 1; ?>
						<?php if($product['options']) { ?>
						    <div class="options options-<?php echo $product['product_id']; ?>">
						        <?php foreach($product['options'] AS $option) { ?>
						            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                         <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                                <?php $is_optionqty = 0; ?>
                                            
                                        <?php } ?>
                                    </select-->
                                    
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
						        <?php } ?>
						    </div>
						<?php } else { ?>
						    <?php $is_optionqty = 0; ?>
						<?php } ?>
						</div>
						 <br>
                        
    	                <?php if($product['personalized'] == 0) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	               
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $product['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="width: 50%;margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $product['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $product['product_id']; ?>)" />
    	                        </div>&nbsp;&nbsp;&nbsp;
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $product['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $product['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $product['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
    	                <?php } ?>
                    </div>
                  </div>
               <?php } ?>
              </div>

            </div>

            <?php if ($carousel) { ?>
              <div class="swiper-pagination islip-swiper-<?php echo $photo['shortcode']; ?>"></div>
              <div class="swiper-pager">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            <?php } ?>
          </div>
        <?php } ?>
      </div>

    </div>
  </div>

<script>
  var captionHeight = $('.photo-ig-caption p').height();

  if (captionHeight < 100) {
    $('.photo-ig-caption').removeClass('caption-truncate');
  }
  $('.photo-ig-caption-overlay .label').on('click', function() {
    $('.photo-ig-caption').toggleClass('caption-expand');
  });

<?php if ($carousel) { ?>
  $('#islip-swiper-<?php echo $photo['shortcode']; ?>').swiper({
    loop: true,
    mode: 'horizontal',
    slidesPerView: 1,
    centeredSlides: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    pagination: '.islip-swiper-<?php echo $photo['shortcode']; ?>',
    paginationClickable: true,
  });
<?php } ?>
</script>


<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('.price-new-' + product_id).html(json.new_price.special);
					$('.price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.special) {
					    $('.price-new-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('.price-new-' + product_id).html(json.new_price.price);
					}
					/*if(json.new_price.quantity < 1) {
				        $('#button-cart-'+ product_id).html('<?php echo "Preorder"; ?>');
						 $('#button-cart-'+ product_id).addClass('preorder');
                    } else {
                        $('#button-cart-'+ product_id).html('<?php echo "Add to Cart"; ?>');
					    $('#button-cart-'+ product_id).removeClass('preorder');
                    }*/
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
</script>

<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    alert("Error");
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt=""></div><div  style="text-align: center" >' + json['success'] + '</div></div>';
                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript"><!--
    $('.options .radio input[type="radio"]').click(function() {
        $('.options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
</div>