<div class="isl-instagramphotos islip-module th-isl-<?php echo $theme; ?>">
  <?php if ($theme == 'journal2') { ?>
    <h3 class="box-heading"><?php echo $setting['module']['title']['lang_id']; ?></h3>
  <?php } else { ?>
    <h3><?php echo $setting['module']['title']['lang_id']; ?></h3>
  <?php } ?>

  <div class="islip-gutter-remove clearfix js-islip-container"></div>

<?php if (!empty($custom_css)) { ?>
<style>
<?php echo $custom_css; ?>
</style>
<?php } ?>
<script>
$(document).ready(function()
{
  // Get photos
  $('.islip-module .js-islip-container').load('index.php?route=<?php echo $module['path']; ?>/fetch&_='+ new Date().getTime(), {"type":"module"}, function() {
    photoGallery();
  });

  // On addCart
  $('body').on('click', '[data-isg-add-cart]', function() {
    var product_id = $(this).data('isg-add-cart');

    if (product_id) {
      <?php if ($theme == 'journal2') { ?>
        addToCart(product_id);
      <?php } else { ?>
        cart.add(product_id);
      <?php } ?>

      setTimeout(function() {
        $.magnificPopup.close();
      }, 1000);
    }
  });
});

function photoGallery() {
  $('.js-islip-container').magnificPopup({
    delegate: '.isl-photo-gallery',
    type: 'ajax',
    gallery: {
      enabled: true,
      tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
    },
    showCloseBtn: true,
    callbacks: {
      open: function() {
        $('html').addClass('mfpip-open');
      },
      close: function() {
        $('html').removeClass('mfpip-open');
      }
    }
  });
}
</script>
</div>
