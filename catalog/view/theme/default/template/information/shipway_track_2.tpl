<?php echo $header; ?>
<style>
#shipway_track_wrapper{width:60%; margin: 0 auto;}
#content table{width:100%}
#shipway_track_wrapper td{font-family: sans-serif!important;}
#shipway_track_wrapper fieldset legend + table tbody tr td input{width: 100%; padding: 4px;}

#shipway_track_wrapper fieldset{border: 1px solid #2eade0 !important;padding: 20px;margin :0 auto !important;}
#shipway_track_wrapper legend{border-bottom:none!important;width:90px!important; margin-bottom:0!important}
#shipway_track_wrapper .track_table{border-collapse : collapse;margin: 20px auto;}
#shipway_track_wrapper .track_table th{padding:5px; text-align:center; color:#ffffff; font-size: 14px;}
#shipway_track_wrapper .track_table td{text-align:center;}
#shipway_track_wrapper .track_table tr.head{background-color:#2386AD;}
#shipway_track_wrapper .track_table .rhead{border:1px solid #2eade0; padding:5px;}
#shipway_track_wrapper .shp_table{width:500px;}

@media (max-width:767px){	
	fieldset{padding:5px;}
	#shipway_track_wrapper{width:98%;}
}
</style>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<div class="row"><?php echo $column_left; ?>
		<div id="content"><?php echo $content_top; ?>
			<h1 align="center">Track Your Shipment Status</h1>
			<div id="shipway_track_wrapper" >
			<form action="<?php echo $action; ?>" id="form" method="post" style="margin:0 auto;" >
				<fieldset>
					<legend>Track Now</legend>
						<table>
							<tbody>
								<tr>
									<th width="70">ORDER ID :</th><td><input type="text" name="order_id"  value="<?php if(isset($order_id)){ echo $order_id;} ?>" /></td>
									
									<td><input type="submit" value="Track"  name="btnSubmit" class="btn btn-primary btn-block"/></td>
								</tr>
							</tbody>
						</table>
				</fieldset>
			</form>
			<?php if(isset($status_result)){ ?>
			<table class="track_table">
			<?php if(isset($awbno)) { ?>	
				<tr class="head">
					<th>Tracking ID</th>
					<th>Carrier Name</th>					
				</tr>
				<tr>
					<td class="rhead"><?php if(isset($awbno)){ echo $awbno;} ?></td>
					<td class="rhead"><?php if(isset($carrier_name)){ echo $carrier_name;} ?></td>
				</tr>
				<?php } ?>	
				<tr>
					<td colspan="2">
					<div id="status_container" style="text-align:center;"></div>
					<?php echo $status_result; ?>				
					</td>
				</tr>
			</table>		
			<?php } ?>							
		  <?php echo $content_bottom; ?>
		  </div>
		<?php echo $column_right; ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>