<?php echo $header; ?>
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/shipway_default.css" />
<style>
    .btn-primary:active, .btn-primary:focus, .btn-primary:active:focus {
        background: #f49a25;
    }
</style>

	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<div class="container">
	<div class="row"><?php echo $column_left; ?>
		<div id="content" class="onj_content"><?php echo $content_top; ?>
			<h1 align="center">Track Your Shipment Status</h1>
			<div id="shipway_track_wrapper" >
			<form action="<?php echo $action; ?>" id="form" method="post" style="margin:0 auto;" >
				<fieldset>
					<legend>Track now</legend>
						<table>
							<tbody>
								<tr>
								<?php if(isset($orderid)){ ?>
									<th width="70">ORDER ID :</th><td><input type="text" name="order_id"  value="<?php if(isset($orderid)){ echo $orderid;} ?>" /></td>
								<?php } else { ?>
								    	<th width="70">ORDER ID :</th><td><input type="text" name="order_id"  value="<?php if(isset($order_id)){ echo $order_id;} ?>" /></td>
								<?php } ?>	
																
									<td><input type="submit" value="Track"  name="btnSubmit" id="btnSubmit" class="btn btn-primary btn-block"/></td>
								</tr>
							</tbody>
						</table>
				</fieldset>
			</form>
			<?php if(isset($status_result)){ ?>
			<table class="track_table">
			<?php if(isset($awbno)) { ?>	
				<tr class="head">
					<th>Tracking ID</th>
					<th>Carrier Name</th>					
				</tr>
				<tr>
					<td class="rhead"><?php if(isset($awbno)){ echo $awbno;} ?></td>
					<td class="rhead"><?php if(isset($carrier_name)){ echo $carrier_name;} ?></td>
				</tr>
				<?php } ?>	
				<tr>
					<td colspan="2">
					<div id="status_container" style="text-align:center;"></div>
					<?php echo $status_result; ?>				
					</td>
				</tr>
			</table>		
			<?php } ?>							
		  <?php echo $content_bottom; ?>
		  </div>
		<?php echo $column_right; ?>
		</div>
	</div>
</div>

<?php if(isset($orderid)){ ?>
	<script>
        $(function() {
            $('#btnSubmit').click();
        });
    </script>
<?php } ?>
<?php echo $footer; ?>