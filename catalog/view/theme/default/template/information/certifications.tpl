<?php echo $header; ?>



<style>
    .pink {
    color: #feac00;
}
	.certifications .te-certificate {
    margin-bottom: 20px;
    float: left;
    width: 100%;
    border-bottom: 1px solid #efefef;
}
.certifications .te-certificate .te-certificate-left {
    float: left;
    width: 20%;
    padding-right: 2%;
}
.lazyloaded {
    opacity: 1;
    transition: opacity .3s;
}
.certifications .te-certificate .te-certificate-right {
    float: left;
    width: 80%;
}
.te-certificate-right h3, .te-certificate-right h3 a {
    color: #f49a25 ;
    font-weight: 800;
    font-size: 18px;
}

.certifications .te-certificate p {
    font-size: 13px;
    margin-bottom: 20px;
    text-align: justify;
}
.pink {
		color: #feac00;
	}
h2{
    font-size: 30px;
}	
</style>


<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
</ul>
	
<div class="container mt40">
    <h2><span class="pink">Our </span> Certifications</h2>
	<br><br>
<div id="primary" class="content-area col-md-12">
	<main id="main" class="site-main">
		<article id="post-1934" class="post-1934 page type-page status-publish hentry">
			<div class="entry-content">
				<div class="certifications">
					<h4>Following are the renowned certifications our products bear.</h4>
					
					<div class="te-certificate">
						<div class="col-md-3">
							<a href="https://www.fssai.gov.in/" target="_blank" rel="noopener noreferrer">
								<img class="lazyloaded" src="image/img/FSSAI_logo.png" data-src="https://www.unived.in/wp-content/uploads/2018/02/peta.jpg" alt=""></a>
							</div>
							<div class="col-md-9">
								<h3><a href="https://www.fssai.gov.in/" target="_blank" rel="noopener noreferrer">FSSAI:</a></h3><p>Food Safety & Standards Authority of India (FSSAI) is a certification established under the Ministry of Health & Family Welfare, Govt. of India in order to supervise the food safety & regulation in India. This certification is responsible for protecting and promoting public health through regulation & supervision of food safety.</p><p class="CCmComment">
									
								<!--	<img class=" ls-is-cached lazyloaded" src="https://www.unived.in/wp-content/uploads/2018/02/b1.jpg" data-src="https://www.unived.in/wp-content/uploads/2018/02/b1.jpg" alt=""> PETA India is happy to associate with Unived Healthcare Products. Apart from being a vegan company against animal testing, Unived Health Care Products is the first nutritional supplements company to be added to PETA India’s cruelty free list. 
									
									<img class=" ls-is-cached lazyloaded" src="https://www.unived.in/wp-content/uploads/2018/02/b2.jpg" data-src="https://www.unived.in/wp-content/uploads/2018/02/b2.jpg" alt=""> 
									<a class="CCmView" href="http://www.petaindia.com/" target="_blank" rel="noopener noreferrer">– Peta India</a> -->
								</p>
							</div>
						</div>
                       
                    <div class="te-certificate">
						<div class="col-md-3">
							<a href="https://wholegrainscouncil.org/" target="_blank" rel="noopener noreferrer">
								<img class="lazyloaded" src="image/img/whole-grain.jpg" data-src="image/img/whole-grain.jpg" alt=""></a>
							</div>
							<div class="col-md-9">
								<h3><a href="https://wholegrainscouncil.org/" target="_blank" rel="noopener noreferrer">Whole Grains Council</a></h3><p>All over the world, people are recommending more of consuming whole foods, around 3 or more servings of whole grains per day. But the lack of appropriate certifications hamper their decision making process in terms of whole grains. The Whole Grain Stamp plays a key role in consumer buying decisions. It assures that the grains used in the products contain a full serving or more of whole grain in each labeled serving & that ALL grain is whole grain.</p><p class="CCmComment">
								</p>
							</div>
						</div>
						
					  <div class="te-certificate">
						<div class="col-md-3">
							<a href="http://www.haccpindia.org/" target="_blank" rel="noopener noreferrer">
								<img class="lazyloaded" src="image/img/haccp-logo.png" data-src="image/img/haccp-logo.png" alt=""></a>
							</div>
							<div class="col-md-9">
								<h3><a href="http://www.haccpindia.org/" target="_blank" rel="noopener noreferrer">HACCP</a></h3><p>Hazard Analysis Critical Control Point (HACCP) is an internationally accepted technique which prevents microbiological, chemical & physical contamination across the food supply chain. This technique identifies the risks by establishing critical control points, sets critical limits & ensures proper control measures are validated, verified and monitored before implementation. Being certified to HACCP enhances our ability to protect our brand and its private labels, promote consumer confidence and & confirm to regulatory and market requirements.</p><p class="CCmComment">
								</p>
							</div>
						</div>
                      
                     <div class="te-certificate">
						<div class="col-md-3">
							<a href="https://www.iso.org/home.html" target="_blank" rel="noopener noreferrer">
								<img class="lazyloaded" src="" alt=""></a>
							</div>
							<div class="col-md-9">
								<h3><a href="https://www.iso.org/home.html" target="_blank" rel="noopener noreferrer">International Standards Organization</a></h3><p>Being certified to International Standards Organisation (ISO) 22000, a Food Safety Management System assures consumers that the company has a secure food safety management in place. It provides a confidence in the customer regarding the brand or the company’s product. The consequences of unsafe food can be serious and ISO’s food safety management standards help organisations identify and control food safety hazards.</p><p class="CCmComment">
								</p>
							</div>
						</div>

					</div>
				</div>
			</article>
		</main>
	</div>	
</div> 

<!--<div class="container">
  <section class="review_part section_padding">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-end">
                <div class="col-lg-5 col-xl-4">
                    <div class="tour_pack_text">
                        <h2>Our <span class="pink">Partners</span></h2>
                        <p>Which cattle fruitful he fly visi won not let above lesser stars fly form wonder every let third form two air seas after us said day won lso together midst two female she</p>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div id="review_part_cotent" class="review_part_cotent owl-carousel owl-loaded owl-drag">
                        <div class="single_review_part">
                            <img src="image/img/female-icon.jpg" alt="">
                            <div class="tour_pack_content">
                                <p>Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Docsapp</h4>
                            </div>
                        </div>
                        <div class="single_review_part">
                            <img src="image/img/male-icon.jpg" alt="">
                            <div class="tour_pack_content">
                                <p> Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Step Set Go</h4>
                            </div>
                        </div>
                       <div class="single_review_part">
                            <img src="image/img/female-icon.jpg" alt="">
                            <div class="tour_pack_content">
                                <p>Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Docsapp</h4>
                            </div>
                        </div>
                        <div class="single_review_part">
                            <img src="image/img/male-icon.jpg" alt="">
                            <div class="tour_pack_content">
                                <p> Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Step Set Go</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
-->
<script type="text/javascript">
var owl = $('#review_part_cotent');
owl.owlCarousel({
      items: 1,
      loop: true,
      dots: false,
      autoplay: true,
      margin: 40,
      autoplayHoverPause: true,
      autoplayTimeout: 5000,
      nav: true,
      navText: ['<span class="flaticon-left-arrow"></span>','<span class="flaticon-arrow-pointing-to-right"></span>'],
      responsive: {
        0: {
          nav: false,
          items: 1
        },
        575: {
          nav: false,
          items: 1
        },
        991: {
          nav: true,
          items: 1
        },
        1200: {
          nav: true,
          items: 1
        },
      }
    });

</script>	

<?php echo $footer; ?>