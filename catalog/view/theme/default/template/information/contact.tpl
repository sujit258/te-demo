<?php echo $header; ?>
<style type="text/css">
    .pink{
        color: #feac00;
    }

    .mt-5{
       margin-top: 3rem !important;
        } 
    .pr-lg-5{
        padding-right: 3rem !important;
    }
    p {
    font-size: 1em;
    color: #777;
    line-height: 1.8em;
}
</style>
<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="container">
    <div class="row">
   <h1><span class="pink">Contact</span> Us</h1>
     <p class="mt-5 pr-lg-5">Accumsan orci faucibus id eu lorem semper. Eu ac iaculis ac nunc nisi lorem vulputate lorem neque cubilia ac in adipiscing in curae lobortis tortor primis integer massa adipiscing id nisi accumsan pellentesque commodo blandit enim arcu non at amet id arcu magna. Accumsan orci faucibus id eu lorem semper nunc nisi lorem vulputate lorem neque cubilia.</p>
    </div>
 </div>    
    <!-- Contact -->
    <section class="about-info py-5 px-lg-5">
        <div class="content-w3ls-inn px-lg-5">
            <div class="container py-md-5 py-3">
                <div class="px-lg-5">
                   

                    <div class="contact-hny-form mt-lg-5 mt-3">
                        <h3 class="title-hny mb-lg-5 mb-3">
                            Drop a Line
                        </h3>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="w3lName"><?php echo $entry_name; ?></label>
                                        <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control">
                                        <?php if ($error_name) { ?>
                                            <div class="text-danger"><?php echo $error_name; ?></div>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="w3lSender"><?php echo $entry_email; ?></label>
                                        <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control">
                                        <?php if ($error_email) { ?>
                                            <div class="text-danger"><?php echo $error_email; ?></div>
                                        <?php } ?>
                                    </div>
                                  
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="w3lSubject"><?php echo $entry_enquiry; ?></label>
                                        <textarea name="enquiry" id="input-enquiry"><?php echo $enquiry; ?>
                                        </textarea>
                                        <?php if ($error_enquiry) { ?>
                                            <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group mx-auto mt-3">
                                    <button type="submit" class="btn btn-default morebtn more black con-submit"><?php echo $button_submit; ?></button>
                                </div>
                            </div>

                        </form>
                    </div>



                    <div class="map-w3pvt mt-5">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d423286.27404345275!2d-118.69191921441556!3d34.02016130939095!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c75ddc27da13%3A0xe22fdf6f254608f4!2sLos+Angeles%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1522474296007" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //Contact -->

    <!-- /news-letter -->
 
    <!-- //news-letter -->

    <!-- footer -->
   <?php echo $footer; ?>
