<?php echo $header; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 


<style type="text/css">

    a:focus, a:hover {
    text-decoration: none !important;
    }

	.partners {
		background: #fff!important;
		height: 384px;
	}
	.partners img {
		display: block;
		width: 50%;
		margin-left: auto;
        margin-right: auto;
        padding-top: 10px;
	}
	
	.partners{ box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.2) !important; }
	
	.partners:hover{ box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.3) !important; }
	
	.partner-name{text-align: center;padding-left: 14px;padding-bottom: 3px;padding-right: 14px; margin-top: 0!important;}
	
	.desc-container {
		padding: 10px 10px 3px 0px;
	}
	
	.desc, .desc2 {
		padding: 0 14px 0 14px;line-height: 20px;
		text-align: justify;
	}
	.desc {
	    font-size: 13px;
	}
	.desc2 {
	    font-size: 11px;
	}

	.title1{text-align:center;color: #000000!important;font-size:15px;font-weight:600;}

	.pink {
		color: #feac00;
	}
	
	.title1:hover { color: #f49a25!important;text-decoration: none;}
	@media(max-width:1000px) {
	    .partners {
	        height: 400px!important;
	    }
	}
</style> 
<script src="//rawgithub.com/ashleydw/lightbox/master/dist/ekko-lightbox.js"></script>

	<!--<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>-->
<div class="container mt40">


	<h2><span class="pink">Our </span> Partners</h2>
	<br><br>

	<div class="row">
		<!--<div class="col-md-3" >
			<div class="partners" >
				<img src="image/img/Docsapp.jpg" title="DocsApp" alt="DocsApp" />
				<br/>
				<div class="desc-container">
					<h4 class="partner-name"><a href="https://www.docsapp.in/" class="title1" target="_blank">DocsApp</a></h4>
					<p class="desc">On purchase of products worth Rs 499/- or more,<br><br></p>
                    <p class="desc2">Get a FREE online doctor consulation worth Rs 500/- on DocsApp<br><span class="hidden-xs hidden-sm"><br></span>
                    <span style="color: gray;font-size:8px;">T&C: Offer valid till 1st Nov'19.</span></p>
				</div>
			</div><br /><br />
		</div>-->
		
		<div class="col-md-3" >
			<div class="partners" >
				<img src="image/img/ssg.jpg" title="Step Set Go" alt="Step Set Go" />
				<br/>
				<div class="desc-container">
					<h4 class="partner-name"><a href="https://www.stepsetgo.com/" class="title1" target="_blank">Step Set Go</a></h4>
					<p class="desc">Get FLAT 30% OFF on True Elements Crispy Muesli Combo by redeeming the coins in Step Set Go App.<br><span class="hidden-md hidden-lg hidden-xl"><br></span></p>
					<p class="desc2">To avail this offer, log in to Step Set Go App now!<br><br>
                    <span style="color: gray;font-size:8px;">T&C: Offer valid till 15th Dec'19.</span></p>
				</div>
			</div><br /><br />
		</div>
		
		
	</div>
	
	
	
	
<!--section class="row"> 
<article class="col-xs-12 col-sm-3 col-md-3"> 
<div class="panel panel-default">
                <div class="panel-body">
                    <a href="https://www.stepsetgo.com/" target="new"> 
                    <img src="image/img/ssg.png" alt=""> </a>
                </div>
                <div class="panel-footer">
                    <h4><a href="https://www.stepsetgo.com/" target="_blank">Step Set Go</a></h4>  
                    <span class="pull-right"> </span>
                </div>
            </div> 
    </article> 
<article class="col-xs-12 col-sm-3 col-md-3"> 
<div class="panel panel-default">
                <div class="panel-body">
                    <a href="https://www.stepsetgo.com/" target="new"> 
                    <img src="image/img/ssg.png" alt=""> </a>
                </div>
                <div class="panel-footer">
                    <h4><a href="https://www.stepsetgo.com/" target="_blank">Step Set Go</a></h4>  
                    <span class="pull-right"> </span>
                </div>
</div> 
</article>
<article class="col-xs-12 col-sm-3 col-md-3"> 
<div class="panel panel-default">
                <div class="panel-body">
                    <a href="https://www.docsapp.in/" target="new"> 
                    <img src="image/img/DocsApp-logo.png" alt=""> </a>
                </div>
                <div class="panel-footer">
                    <h4><a href="https://www.docsapp.in/" target="_blank">Docsapp</a></h4>  
                    <span class="pull-right"> </span>
                </div>
            </div> 
    </article> 
<article class="col-xs-12 col-sm-3 col-md-3"> 
<div class="panel panel-default">
                <div class="panel-body">
                    <a href="https://www.stepsetgo.com/" target="new"> 
                    <img src="image/img/ssg.png" alt=""> </a>
                </div>
                <div class="panel-footer">
                    <h4><a href="https://www.stepsetgo.com/" target="_blank">Step Set Go</a></h4>  
                    <span class="pull-right"> </span>
                </div>
</div> 
</article>
</section-->

<br/>
<!-- <div class="panel panel-default">
                <div class="panel-heading">
                    Our
                </div>
</div>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
 <div class="carousel-inner">
      <div class="item active">
        <img src="/image/img/review-1.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="/image/img/review-2.jpg" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="/image/img/review-1.jpg" style="width:100%;">
      </div>
    </div>
    
</div>  -->
</div> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script type="text/javascript"> 
	$(document).ready(function() { 
		$('i.glyphicon-thumbs-up, i.glyphicon-thumbs-down').click(function(){ 
			var $this = $(this), c = $this.data('count');
			if (!c) c = 0; c++; $this.data('count',c);
			$('#'+this.id+'-bs3').html(c); }); 
		$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) { event.preventDefault(); $(this).ekkoLightbox(); 
			
		});
	}); 
</script>

<?php echo $footer; ?>