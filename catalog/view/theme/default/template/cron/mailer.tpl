<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="https://www.true-elements.com/catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <style>
            .para, .para2 {
                text-align: center;
                margin-top: 10px;
                margin-bottom: 10px;
            }
            
            @media only screen and (max-width:800px) {
                .cta-img, .para, .para2 {
                    width: 90%;
                    padding-left: 5%;
                    padding-right: 5%;
                }
                .para {
                    font-size: 15px;
                }
                .para2 {
                    font-size: 14px;
                }
            }
            @media only screen and (min-width:801px) {
                .cta-img {
                    width: 60%;
                }
                .cta-img-link {
                    padding-left: 20%;
                    padding-right: 20%;
                }
                .para, .para2 {
                    width: 60%;
                    margin-left: 20%;
                    margin-right: 20%;
                }
                
                .para {
                    font-size: 22px;
                }
                .para2 {
                    font-size: 20px;
                }
            }
        </style>
    </head>
    <body>
        <p class="para">Is constipation or indigestion problem putting you in a state of worry every morning?</p>
	                
        <p class="para">Stress no more, because we have a simple fix to this problem.</p>
	                
        <a class="cta-img-link" href="https://bit.ly/32tslqw"><img src="https://www.true-elements.com/image/mult-muesli-mailer-shop-now.jpg" class="img-responsive cta-img" style="padding: 20px 15px;"></a>
	                
        <p class="para2">Including about <b>40% more</b> wholegrain Millets than any other Muesli, True Elements Multigrain Diet Muesli is one of the most favourable, <b>weight-loss-friendly breakfast cereal</b> which can be ready to consume in less than 5 minutes.</p>
                    
        <p class="para2">Stacked with soluble Dietary Fibre to solve your indigestion problems as well as shrink your waistline, this <b>Zero Added Sugar</b> variant of Multigrain Diet Muesli is the perfect pick for you!</p>
                    
        <p class="para2">Shop this Muesli at FLAT 10% OFF by using code TRUE10</p>
        <br/>            
        <p class="para"><a href="https://bit.ly/32tslqw" style="padding: 15px 50px; border-radius: 50px; background: #40a2ca; color:#fff; font-weight: bold; text-decoration: none;" class="para">Shop Now</a></p><br/><br/>
                    
    </body>
</html>