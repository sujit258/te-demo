<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <title><?php echo $title; ?></title>
	    <style>
    	    /*@media only screen and (max-width:800px){
    	        #content {
                    width: 120%;
                }
            }*/
            table {
                border-collapse: collapse;
                float: left;
                margin: 10px;
            }
            thead, tfoot {
                background: rgb(143,194,255);
                font-weight: bold;
                text-align: center;
            }
            .first-child {
                background: rgb(196,217,238);
                padding-right: 10px;
                padding-left: 10px;
                font-weight: bold;
            }
            td {
                text-align: center;
                border: 1px solid #000;
                padding: 5px;
            }
            .status_wise_sale, .te_labs_personalized_products_report, .payment_zone_report, .order_comments_report, .buttons {
                width: 100%;
                display: inline-block;
            }
            .buttons {
                margin: 20px 0;
                text-align: center;
            }
            .buttons a {
                padding: 10px;
                background: #f49a25;
                color: #fff;
                font-weight: bold;
                border-radius: 5px;
                text-decoration: none;
                font-size: 15px;
                letter-spacing: 0.5px;
            }
	    </style>
    </head>
    <body>
        <div id="content">
            <div class="status_wise_sale">
                <table>
                    <thead>
                        <tr>
                            <td colspan="7">True Elements - Operations</td>
                        </tr>
                        <tr>
                            <td><?php echo date("M d", strtotime("-1 DAYS")); ?></td>
                            <td>MTD</td>
                            <td>MTD TC Used</td>
                            <td>Orders Count</td>
                            <td>Today</td>
                            <td>Today TC Used</td>
                            <td>Orders Count Today</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($report AS $data) { ?>
			                <tr>
			                    <td class="first-child"><?php echo $data['status']; ?></td>
			                    <td><?php echo round($data['total_sale']); ?></td>
			                    <td><?php echo round($data['total_tc']); ?></td>
			                    <td><?php echo $data['total_orders']; ?></td>
			                    <td><?php echo round($data['total_sale_today']); ?></td>
			                    <td><?php echo round($data['total_tc_today']); ?></td>
			                    <td><?php echo $data['total_orders_today']; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="te_labs_personalized_products_report">
                <?php if(!empty($te_labs_data)) {
                    $total_te_labs_sales_today = 0;
                    $total_te_labs_orders_today = 0;
                    $total_te_labs_sales = 0;
                    $total_te_labs_orders = 0;
                ?>
                    <table class="te_labs_products">
                        <thead>
                            <tr>
                                <td colspan="5">TE Labs</td>
                            </tr>
                            <tr>
                                <td>Product Name</td>
                                <td>Today Sales</td>
                                <td>Today Orders Count</td>
                                <td>MTD Sales</td>
                                <td>MTD Order Count</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($te_labs_data AS $data) { ?>
                                <?php
                                    $total_te_labs_sales_today += $data['total_sales_today'];
                                    $total_te_labs_orders_today += $data['total_orders_today'];
                                    $total_te_labs_sales += $data['total_sales'];
                                    $total_te_labs_orders += $data['total_orders'];
                                ?>
                                <tr>
                                    <td class="first-child"><?php echo $data['name']; ?></td>
                                    <td><?php echo $data['total_sales_today']; ?></td>
                                    <td><?php echo $data['total_orders_today']; ?></td>
                                    <td><?php echo $data['total_sales']; ?></td>
                                    <td><?php echo $data['total_orders']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td><?php echo $total_te_labs_sales_today; ?></td>
                                <td><?php echo $total_te_labs_orders_today; ?></td>
                                <td><?php echo $total_te_labs_sales; ?></td>
                                <td><?php echo $total_te_labs_orders; ?></td>
                            </tr>
                        </tfoot>
                    </table>
                <?php } ?>
                
                <?php if(!empty($custom_products_report)) {
                    $total_custom_sales_today = 0;
                    $total_custom_orders_today = 0;
                    $total_custom_sales = 0;
                    $total_custom_orders = 0;
                ?>
                    <table class="personalized_products">
                        <thead>
                            <tr>
                                <td colspan="5">Personalized Products</td>
                            </tr>
                            <tr>
                                <td>Product Name</td>
                                <td>Today Sales</td>
                                <td>Today Orders Count</td>
                                <td>MTD Sales</td>
                                <td>MTD Order Count</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($custom_products_report AS $data) { ?>
                                <?php
                                    $total_custom_sales_today += $data['total_sales_today'];
                                    $total_custom_orders_today += $data['total_orders_today'];
                                    $total_custom_sales += $data['total_sales'];
                                    $total_custom_orders += $data['total_orders'];
                                ?>
                                <tr>
                                    <td class="first-child"><?php echo $data['name']; ?></td>
                                    <td><?php echo $data['total_sales_today']; ?></td>
                                    <td><?php echo $data['total_orders_today']; ?></td>
                                    <td><?php echo $data['total_sales']; ?></td>
                                    <td><?php echo $data['total_orders']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td><?php echo $total_custom_sales_today; ?></td>
                                <td><?php echo $total_custom_orders_today; ?></td>
                                <td><?php echo $total_custom_sales; ?></td>
                                <td><?php echo $total_custom_orders; ?></td>
                            </tr>
                        </tfoot>
                    </table>
                <?php } ?>
            </div>
                  
            <div class="payment_zone_report">  
                <?php if(!empty($payment_zone_report)) {
                    $total_payment_zone_razorpay = 0;
                    $total_payment_zone_cod = 0;
                    $total_payment_zone_payu = 0;
                    $total_payment_zone_paytm = 0;
                ?>
                    <table>
                        <thead>
                            <tr>
                                <td colspan="5">Orders Count By Payment Type</td>
                            </tr>
                            <tr>
                                <td><?php echo date("M d", strtotime("-1 DAYS")); ?></td>
                                <td>Razorpay</td>
                                <td>COD</td>
                                <td>Payu</td>
                                <td>Paytm</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($payment_zone_report AS $data) { ?>
                                <?php
                                    $total_payment_zone_razorpay += $data['razorpay_total_orders'];
                                    $total_payment_zone_cod += $data['cod_total_orders'];
                                    $total_payment_zone_payu += $data['payu_total_orders'];
                                    $total_payment_zone_paytm += $data['paytm_total_orders'];
                                ?>
                                <tr>
                                    <td class="first-child"><?php echo $data['status']; ?></td>
                                    <td><?php echo $data['razorpay_total_orders']; ?></td>
                                    <td><?php echo $data['cod_total_orders']; ?></td>
                                    <td><?php echo $data['payu_total_orders']; ?></td>
                                    <td><?php echo $data['paytm_total_orders']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <td>Total</td>
                            <td><?php echo $total_payment_zone_razorpay; ?></td>
                            <td><?php echo $total_payment_zone_cod; ?></td>
                            <td><?php echo $total_payment_zone_payu; ?></td>
                            <td><?php echo $total_payment_zone_paytm; ?></td>
                        </tfoot>
                    </table>
                <?php } ?>
            </div>
            
            <div class="order_comments_report">
                <?php if(!empty($order_with_comments_report)) { ?>
                    <table>
                        <thead>
                            <tr>
                                <td colspan="5">Order Comments by Customers</td>
                            </tr>
                            <tr>
                                <td>Order Id</td>
                                <td>Date Added</td>
                                <td>Comment</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($order_with_comments_report AS $data) { ?>
                                <?php if($data['name'] == '' || $data['name'] == NULL) {
	                                $status = 'Missing Order';
	                            } else {
	                                $status = $data['name'];
	                            } ?>
		                        <tr>
		                            <td class="first-child"><?php echo $data['order_id']; ?></td>
		                            <td><?php echo date('d-m-Y', strtotime($data['date_added'])); ?></td>
		                            <td><?php echo $data['comment']; ?></td>
		                            <td><?php echo $status; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
            
            <div class="buttons">
                <a href="https://docs.google.com/spreadsheets/d/1x6Fpd5_W7eizk0IRUBvonEXxw9M9G6Gd1DeI-wEwwKU/edit?pli=1#gid=1193424113">TE Labs Daily Report</a>
            </div>
        </div>
    </body>
</html>