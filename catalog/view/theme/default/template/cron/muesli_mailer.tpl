<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="https://www.true-elements.com/catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <style>
            .bottom-image {
                width: 100%;
                text-align: center;
            }
            .section-heading {
                text-align: center;
            }
            
            @media only screen and (max-width:800px) {
                .top-img {
                    width: 90%;
                    margin-left: 5%;
                }
                .special-feature {
                    width: 48%;
                    display: inline-block;
                    margin-right: 1%;
                    margin-bottom: 15px;
                }
                .section-heading {
                    font-size: 15px;
                }
            }
            @media only screen and (min-width:801px) {
                .top-img {
                    width: 60%;
                    margin-left: 20%;
                }
                .special-feature {
                    width: 23%;
                    display: inline-block;
                    margin-right: 0.66%;
                }
                .section-heading {
                    font-size: 20px;
                }
            }
        </style>
    </head>
    <body>
        <div class="top-img"><a href="https://bit.ly/31QyP1T"><img class="img-responsive" style="width: 100%;" src="https://www.trueelements.co.in/image/catalog/mailer/muesli-mailer.gif"></a></div>
        <div style="width: 100%;">
            <h2 class="section-heading">Special Features of True Elements Wholegrain Muesli</h2>
            <div class="special-feature">
                <a href="https://bit.ly/31QyP1T"><img class="img-responsive" src="https://www.trueelements.co.in/image/catalog/mailer/high-fibre.jpg" style="width: 100%;" alt="True Elements"></a>
                <p>Our Muesli is rich in Dietary Fibre which promotes weight loss and also regulates the digestive system, thus avoiding the problem of constipation.</p>
            </div>
            <div class="special-feature">
                <a href="https://bit.ly/31QyP1T"><img class="img-responsive" src="https://www.trueelements.co.in/image/catalog/mailer/no-added-sugar.jpg" style="width: 100%;" alt="True Elements"></a>
                <p>All ranges of our Muesli are made with Zero Added Sugar, hence can be easily consumed by sugar-conscious people as well as Diabetics.</p>
            </div>
            <div class="special-feature">
                <a href="https://bit.ly/31QyP1T"><img class="img-responsive" src="https://www.trueelements.co.in/image/catalog/mailer/real-fruits.jpg" style="width: 100%;" alt="True Elements"></a>
                <p>Unlike other brands who add sugar-coated dehydrated Fruits, Our Muesli is enhanced with Freeze-dried Real Fruits with NOT a single layer of Sugar on it.</p>
            </div>
            <div class="special-feature">
                <a href="https://bit.ly/31QyP1T"><img class="img-responsive" src="https://www.trueelements.co.in/image/catalog/mailer/wholegrains.jpg" style="width: 100%;" alt="True Elements"></a>
                <p>Our Muesli is made with 100% wholegrains such as Rolled Oats, Wheat Flakes, Jowar Flakes and Bajra Flakes, all of which are helpful in keeping the Heart healthy.</p>
            </div>
        </div>
        <h2 class="section-heading">How to relish your breakfast with True Elements Wholegrain Muesli?</h2>
        <div class="bottom-image"><a href="https://bit.ly/31QyP1T"><img class="img-responsive" src="https://www.trueelements.co.in/image/catalog/mailer/muesli-how-to-use.jpg"></a></div>
        
        <a href="https://www.true-elements.com"><img style="width: 70px;" src="https://www.true-elements.com/image/catalog/brands/png-logo.png" /></a>
		<br/>
		<a href="tel:8767120120">8767120120</a><br/><a href="mailto:care@true-elements.com">care@true-elements.com</a>
    </body>
</html>