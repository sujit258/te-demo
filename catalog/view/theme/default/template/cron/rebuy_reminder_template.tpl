<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!-- NAME: SELL PRODUCTS -->
        <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Are you about to finish the goodies from True Elements?</title>
        
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage,.mcnRetinaImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	/*
	@tab Page
	@section Heading 1
	@style heading 1
	*/
		h1{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:40px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 2
	@style heading 2
	*/
		h2{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:34px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 3
	@style heading 3
	*/
		h3{
			/*@editable*/color:#444444;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:22px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 4
	@style heading 4
	*/
		h4{
			/*@editable*/color:#949494;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:125%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Container Style
	*/
		#templateHeader{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:50% 50%;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:1px none ;
			/*@editable*/border-bottom:1px none ;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Header
	@section Header Interior Style
	*/
		.headerContainer{
			/*@editable*/background-color:#fffcfc;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Header
	@section Header Text
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/color:#757575;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Link
	*/
		.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
			/*@editable*/color:#007C89;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section Body Container Style
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Body
	@section Body Interior Style
	*/
		.bodyContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Body
	@section Body Text
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/color:#757575;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section Body Link
	*/
		.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
			/*@editable*/color:#007C89;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Footer
	@section Footer Style
	*/
		#templateFooter{
			/*@editable*/background-color:#333333;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:5px;
			/*@editable*/padding-bottom:5px;
		}
	/*
	@tab Footer
	@section Footer Interior Style
	*/
		.footerContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Footer
	@section Footer Text
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@section Footer Link
	*/
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}

}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}

}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnRetinaImage{
			max-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
			max-width:100% !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:30px !important;
			/*@editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:26px !important;
			/*@editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}

}
    @media only screen and (max-width: 480px){
        .mcnTextContent .mcnTextContent {
            width: 30%!important;
            float: left!important;
            padding: 5px!important;
            line-height: 100%!important;
        }
    }
    @media only screen and (max-width: 480px){
        .mcnButtonContent {
            font-size: 14px!important;
        }
    }
    @media only screen and (max-width: 480px){
        #mcnRecycleBlock .mcnButtonContent .mcnButton {
            padding: 18px 3px!important;
        }
    }
    @media only screen and (max-width: 480px){
        #repurchase-banner .mcnImageSrcMobile {
            display: block!important;
        }
    }
    @media only screen and (max-width: 480px){
        #repurchase-banner .mcnImageSrcDesktop {
            display: none!important;
        }
    }
</style></head>
    <body>
        <!--*|IF:MC_PREVIEW_TEXT|*-->
        <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></span><!--<![endif]-->
        <!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top" id="templateHeader" data-template-container>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="headerContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:0px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                
                                    <a href="https://www.true-elements.com?utm_source=email&utm_medium=tesystem&utm_campaign=logo" title="True Elements" class="" target="_blank">
                                        <img align="center" alt="" src="<?php echo $top_logo; ?>" width="88.625" style="max-width:709px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnRetinaImage">
                                    </a>
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #000000;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #222222;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;">
                                        <span style="color:#ffffff"><span style="font-size:20px;">Hi <?php echo trim($mail_data['firstname']); ?>,</span><br><span style="font-size:18px;">Reordering your favorites is just a few clicks away!</span></span>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-right:18px;">
                        
                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width:100% !important;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 15px 5px 0 5px;color: #222222;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;">
                                        <div style="text-align: center;"><span style="font-size:16px">
<span style="text-align:center">Here is the list of your last purchase. You can choose which all to buy again with a single click.<br>
<hr/>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
    <thead>
        <tr>
            <th style="text-align: center; width: 15%; padding: 5px;"></th>
            <th style="text-align: center; width: 70%; padding: 5px;">Product</th>
            <th style="text-align: center; width: 15%; padding: 5px;">Qty</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($mail_data['products'] as $product) { ?>
            <tr>
                <td style="text-align: center; padding: 5px;">
                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="display:block;" height="60" width="60"></a>
                </td>
                <td style="text-align: left; padding: 5px; line-height: 120%;">
                    <a href="<?php echo $product['href']; ?>" style="text-decoration: none; color: #000;"><?php echo $product['name']; ?></a><br/>
                    <small>- <?php echo $product['option_name'], '&nbsp;', $product['option_value']; ?></small>
                </td>
                <td style="text-align: center; padding: 5px;">x <?php echo $product['quantity']; ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<hr/>
</span></span><br>
</div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 4px;background-color: #000000;">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 18px;">
                                <a class="mcnButton " title="Yes I want to Restock" href="<?php echo $mail_data['email_quick_order']; ?>" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Yes I want to Restock</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                        
                            <div style="text-align: center;"><span style="font-size:14px"><span style="color:red;">Surprise: Pre-loaded 12% discount for you!</span></span><br>
<span style="color:#A9A9A9"><span style="font-size:12px">Click on the above button to place your order.</span></span></div>

                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 4px;background-color: #000000;">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 18px;">
                                <a class="mcnButton " title="Show me all my previously ordered Stuff" href="<?php echo $smart_cart_href; ?>" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Show me all my previously ordered Stuff</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #000000;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #EFEFEF;font-family: Helvetica;font-size: 18px;font-weight: normal;text-align: center;">
                                        <span style="color:#FFFFFF">Hand-picked New Launches for you!</span>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px;color: #222222;font-size: 24px;font-style: italic;font-weight: normal;text-align: center;">
                        
                            
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCodeBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                <style>
  /* Three image containers (use 25% for four, and 50% for two, etc) */
.column {
  float: left!important;
  width: 33.33%!important;
  padding: 5px!important;
}

/* Clear floats after image containers */
.row::after {
  content: "";
  clear: both;
  display: table;
}
.row {
  display: flex;
}

.column {
  flex: 33.33%!important;
  padding: 5px!important;
}
button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 10px 22px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  border-radius:5px;
}
.off{
    text-align: center;
}

img {
    border-radius: 5px;
}
 </style>
 <div class="row mcnTextContent">
     <?php foreach($new_launch_data as $new_launch) { ?>
        <div class="column mcnTextContent" style="width: 33.33%; float: left!important;">
	        <a href="<?php echo $new_launch['href']; ?>"><img class="mcnImage" src="<?php echo $new_launch['image']; ?>" alt="<?php echo $new_launch['name']; ?>" style="width:100%;"></a>
		    <a style="text-decoration: none;" href="<?php echo $new_launch['href']; ?>"><h5 style="text-align: center; color: #000;"><?php echo $new_launch['name']; ?></h5></a>
        </div>
     <?php } ?>
</div>
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 10px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px none #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateBody" data-template-container>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="bodyContainer">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                                                	<tbody class="mcnBoxedTextBlockOuter">
                                                        <tr>
                                                            <td valign="top" class="mcnBoxedTextBlockInner">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                                                                    <tbody><tr>
                                                                        
                                                                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                                                                        
                                                                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #000000;">
                                                                                <tbody><tr>
                                                                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #EFEFEF;font-family: Helvetica;font-size: 18px;font-weight: normal;text-align: center;">
                                                                                        <span style="color:#FFFFFF">Recycle & get Rewarded!</span>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                        
                            <div style="text-align: center;"><span style="font-size:14px"><span style="color:#000000">Did you know that the plastic you use could help provide fuel to few households?</span><br/><span style="color:#000000">Return the packets to us and make a difference in their lives! And get rewarded too!</span></span><br>
<span style="color:#A9A9A9"></span></div>

                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;" id="mcnRecycleBlock">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; width: 49%;">
                                <a class="mcnButton" title="Know More about Recycle" href="https://youtu.be/M3nADg8gHCQ" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF; padding: 18px 15px; border-radius: 4px;background-color: #000000;">Know More about Recycle</a>
                            </td>
                            <td style="width: 1%;">&nbsp;</td>
                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; width: 49%;">
                                <a class="mcnButton" title="Yes I want to Recycle" href="https://bit.ly/3yd3Cnu" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF; padding: 18px 15px; border-radius: 4px;background-color: #000000;">Yes I want to Recycle</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

</td>
                                        </tr>
                                    </table>
                                    <table id="repurchase-banner" border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                        <tbody class="mcnImageBlockOuter">
                                                <tr>
                                                    <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                            <tbody><tr>
                                                                <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                                                    
                                                                        <a href="https://bit.ly/3uPf4mM" title="" class="mcnImageSrcDesktop" target="_blank">
                                                                            <img align="center" alt="" src="<?php echo $repurchase_banner; ?>" width="80%" style="max-width:970px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                                                        </a>
                                                                        <a href="https://bit.ly/3uPf4mM" title="" class="mcnImageSrcMobile" target="_blank" style="display: none;">
                                                                            <img align="center" alt="" src="<?php echo $repurchase_banner_mobile; ?>" width="80%" style="max-width:970px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                                                        </a>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                        </tbody>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateFooter" data-template-container>
                                    
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="footerContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
                                                <tbody class="mcnFollowBlockOuter">
                                                    <tr>
                                                        <td align="center" valign="top" class="mcnFollowBlockInner">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
                                                <tbody><tr>
                                                    <td align="center" style="padding-left:9px;padding-right:9px;">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                                                            <tbody><tr>
                                                                <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                        <tbody><tr>
                                                                            <td align="center" valign="top">
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                            <tbody><tr>
                                                                                                <td valign="top" style="padding-right:10px; padding-bottom:5px;" class="mcnFollowContentItemContainer">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                        <tbody><tr>
                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                    <tbody><tr>
                                                                                                                        
                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                <a href="https://www.facebook.com/trueelements.in" target="_blank"><img src="https://www.true-elements.com/image/icons/social/mailer-footer-facebook.png" alt="Facebook" style="display:block;" height="24" width="24" class=""></a>
                                                                                                                            </td>
                                                                                                                        
                                                                                                                        
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                            <tbody><tr>
                                                                                                <td valign="top" style="padding-right:10px; padding-bottom:5px;" class="mcnFollowContentItemContainer">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                        <tbody><tr>
                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                    <tbody><tr>
                                                                                                                        
                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                <a href="https://twitter.com/trueelements" target="_blank"><img src="https://www.true-elements.com/image/icons/social/mailer-footer-twitter.png" alt="Twitter" style="display:block;" height="24" width="24" class=""></a>
                                                                                                                            </td>
                                                                                                                        
                                                                                                                        
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                            <tbody><tr>
                                                                                                <td valign="top" style="padding-right:10px; padding-bottom:5px;" class="mcnFollowContentItemContainer">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                        <tbody><tr>
                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                    <tbody><tr>
                                                                                                                        
                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                <a href="https://www.instagram.com/true.elements/" target="_blank"><img src="https://www.true-elements.com/image/icons/social/mailer-footer-instagram.png" alt="Link" style="display:block;" height="24" width="24" class=""></a>
                                                                                                                            </td>
                                                                                                                        
                                                                                                                        
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                            <tbody><tr>
                                                                                                <td valign="top" style="padding-right:10px; padding-bottom:5px;" class="mcnFollowContentItemContainer">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                        <tbody><tr>
                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                    <tbody><tr>
                                                                                                                        
                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                <a href="https://www.youtube.com/channel/UC1T1ylFuw3CnZ4COia18tvA" target="_blank"><img src="https://www.true-elements.com/image/icons/social/mailer-footer-youtube.png" alt="YouTube" style="display:block;" height="24" width="24" class=""></a>
                                                                                                                            </td>
                                                                                                                        
                                                                                                                        
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                            <tbody><tr>
                                                                                                <td valign="top" style="padding-right:10px; padding-bottom:5px;" class="mcnFollowContentItemContainer">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                        <tbody><tr>
                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                    <tbody><tr>
                                                                                                                        
                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                <a href="https://www.linkedin.com/company/true-elements-te/?viewAsMember=true" target="_blank"><img src="https://www.true-elements.com/image/icons/social/mailer-footer-linkedin.png" alt="LinkedIn" style="display:block;" height="24" width="24" class=""></a>
                                                                                                                            </td>
                                                                                                                        
                                                                                                                        
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                    
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                            <tbody><tr>
                                                                                                <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                        <tbody><tr>
                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                    <tbody><tr>
                                                                                                                        
                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                <a href="https://in.pinterest.com/trueelements/" target="_blank"><img src="https://www.true-elements.com/image/icons/social/mailer-footer-pinterest.png" alt="Pinterest" style="display:block;" height="24" width="24" class=""></a>
                                                                                                                            </td>
                                                                                                                        
                                                                                                                        
                                                                                                                    </tr>
                                                                                                                </tbody></table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                            
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                                <tbody class="mcnTextBlockOuter">
                                                    <tr>
                                                        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                                                                        <strong>True Elements</strong><br>
                                                                            What you see above is part of our world that is Made of Truth – Not a world full of lies, jargon or False Promises. Where what is written on the pack is what's in it, and vice versa.<br>
                                                                            Food ranging from ‘Nashta’ to ‘Tiffin’, from Breakfast to Snacks.<br/><br/>
                                                                            Email: <?php echo $store_email; ?><br/>
                                                                            Telephone: <a href="tel:<?php echo $store_telephone; ?>"><?php echo $store_telephone; ?></a><br/>
                                                                            For Whatsapp Order: <?php echo $store_whatsapp; ?><br/><br/>
                                                                            <a href="https://play.google.com/store/apps/details?id=com.trueelements.india&hl=en&gl=US" target="_blank">
                                            								    <img class="googleplay-original" alt="googleplay-logo" src="https://www.true-elements.com/image/data/googleplay1.png" width="140" height="50">
                                        								    </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table></td>
                                        </tr>
                                    </table>
                                    
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
        <img src="https://www.true-elements.com/index.php?route=cron/test&customer_id=<?php echo $mail_data['customer_id']; ?>" style="display:none" />
    </body>
</html>