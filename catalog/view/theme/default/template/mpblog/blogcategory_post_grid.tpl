<script src="https://www.true-elements.com/catalog/view/javascript/multislider.min.js" type="text/javascript"></script>
<script src="https://www.true-elements.com/catalog/view/javascript/jquery.detect_swipe.js"></script>
<style>
    .mpblogpost-layout {
        margin-bottom: 25px;
    }
    .caption {
        padding-bottom: 40px!important;
    }
    .caption h2 {
        text-align: center;
        min-height: 75px;
    }
    .caption h2 a {
        font-size: 17.5px!important;
        text-align: center;
        line-height: 25px;
        color: #000;
    }
    .caption h2 a:hover {
        color: #f49a25;
    }
    .other-data {
        border-top: 2.5px dotted #000;
        border-bottom: 2.5px dotted #000;
        text-align: center;
        padding: 10px;
        font-size: 14px;
    }
    .read-more {
        position: absolute;
        bottom: 0%;
        left: 35%;
        width: 30%;
        right: 35%;
    }
    .read-more:hover h3 {
        background: #000;
    }
    .read-more h3 {
        text-align: center;
        padding: 11px 0;
        background: #f49a25;
        color: #fff;
        font-weight: 600;
        border-radius: 4px;
    }
    .other-data span {
        font-size: 25px;
        vertical-align: top;
    }
.category-title {
    text-align: center;
    font-weight: bold;
    padding: 5px;
}
.category-title:hover {
    color: #f49a25;
}
.category-image {
    padding: 0 15%;
    margin-left: auto;
    margin-right: auto;
    display: table;
}
@media(max-width: 1000px) {
    #category-slider .MS-controls button {
        top: 30%!important;
    }
    .category-title {
        font-size: 14px!important;
    }
}
#category-slider {
    position: relative;
    margin-bottom: 20px
}
#category-slider .MS-content {
  margin: 15px 5%;
  overflow: hidden;
  white-space: nowrap;
}
#category-slider .MS-content .item {
  display: inline-block;
  height: 100%;
  overflow: hidden;
  position: relative;
  vertical-align: top;
  width: 25%;
}
@media (max-width: 1200px) {
  #category-slider .MS-content .item {
    width: 25%;
  }
}
@media (max-width: 992px) {
  #category-slider .MS-content .item {
    width: 33.3333%;
  }
}
@media (max-width: 767px) {
  #category-slider .MS-content .item {
    width: 50%;
  }
}
#category-slider .MS-controls button {
  position: absolute;
  border: none;
  background: transparent;
  font-size: 30px;
  outline: 0;
  top: 35%;
}
#category-slider .MS-controls button:hover {
  cursor: pointer;
  color: #f49a25;
}
#category-slider .MS-controls .MS-left {
  left: 0px;
}
@media (max-width: 992px) {
  #category-slider .MS-controls .MS-left {
    left: -2px;
  }
}
#category-slider .MS-controls .MS-right {
  right: 0px;
}
@media (max-width: 992px) {
  #category-slider .MS-controls .MS-right {
    right: -2px;
  }
}
.MS-controls .fa {
    color: #000;
}
.MS-controls .fa:hover {
    color: #f49a25;
}
</style>
<?php if($mpblog_categories) { ?>
    <div id="category-slider">
        <div class="MS-content">
            <?php foreach($mpblog_categories AS $category) { ?>
                <div class="item">
                    <a href="<?php echo $category['href']; ?>">
                        <img alt="True Elements" class="img-responsive category-image" src="<?php echo $category['image']; ?>">
                        <h4 class="category-title"><?php echo $category['name']; ?></h4>
                    </a>
                </div>
            <?php } ?>
        </div>
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
        </div>
    </div>
<?php } ?>
<?php if ($mpblogposts) { ?>
<div class="mp-grid-layout">
<div class="<?php if($themename !== 'journal2')  { echo 'row'; } if($themename == 'journal2') { echo 'xl-100'; } ?>">
	<?php foreach ($mpblogposts as $mpblogpost) { 
  $cols = isset($mpblog_category_design[0]) ? (12/$mpblog_category_design[0]) : 4;
  $jcols = 50;
  if($themename == 'journal2') {
    switch($cols) {
      case 12:
        $jcols = 100;
      break;
      case 6:
        $jcols = 50;
      break;
      case 4:
        $jcols = 33;
      break;
      case 3:
        $jcols = 25;
      break;
      case 2:
        $jcols = 16;
      break;        
      default:
      $jcols = 50;
    }
  }
   ?>
		<div class="mpblogpost-layout mpblogpost-grid col-lg-<?php echo $cols; ?> col-md-<?php echo $cols; ?> col-sm-6 col-xs-12 xs-100 sm-100 md-50 lg-<?php echo $jcols; ?> xl-<?php echo $jcols; ?>">
      <div class="inner-layout">
        <div class="image">
          <?php if($mpblogpost['showImage']) { ?>
          <div class="video-container"><a href="<?php echo $mpblogpost['href']; ?>"><img src="<?php echo $mpblogpost['thumb']; ?>" alt="<?php echo $mpblogpost['name']; ?>" title="<?php echo $mpblogpost['name']; ?>" class="img-responsive" /></a></div>
          <?php } else { ?>
          <div class="video-container"><iframe src="<?php echo $mpblogpost['iframeVideo']; ?>" frameborder="0" allowfullscreen></iframe></div>
          <?php } ?>
          <!--<?php if($show_viewed) { ?>
            <div class="view" title="<?php echo $mpblogpost['viewed']; ?>">
              <i class="fa fa-eye"></i>
              <?php echo $mpblogpost['viewed']; ?>
            </div>
          <?php } ?>-->
        </div>
        <div class="caption">
          <h2><a href="<?php echo $mpblogpost['href']; ?>"><?php echo $mpblogpost['name']; ?></a></h2>
            <ul class="dar clearfix list-inline mpblogpost-id-<?php echo $mpblogpost['mpblogpost_id']; ?>">
                    <!--<li class="col-sm-6 xl-50" title="<?php echo $mpblogpost['date_available']; ?>">
                    <?php if($show_date) { ?>
                      <i class="fa fa-calendar"></i>
                      <?php if(!empty($mpblogpost['date_availableurl'])) {  ?>
                      <a href="<?php echo $mpblogpost['date_availableurl']; ?>"><?php echo $mpblogpost['date_available']; ?></a>
                      <?php } else { ?>
                      <?php echo $mpblogpost['date_available']; ?>
                      <?php } ?>
                    <?php } ?>
                    </li>
                    <?php if($show_author) { ?>
                    <li title="<?php echo $mpblogpost['author']; ?>" class="author col-sm-6 xl-50">
                      <i class="fa fa-user"></i>                    
                      <?php if(!empty($mpblogpost['authorurl'])) { ?>
                      <a href="<?php echo $mpblogpost['authorurl']; ?>">
                        <?php echo $mpblogpost['author']; ?>
                      </a>
                      <?php } else { ?>
                      <?php echo $mpblogpost['author']; ?>
                      <?php } ?>
                    </li>
                    <?php } ?>-->
                    <?php if($show_rating) { ?>
                    <li class="col-sm-12 col-xs-12 xl-100" title="<?php echo $mpblogpost['rating']; ?>">
                      <div class="small-rating rating text-center">
                        <div class="rating-icons-container text-center">
                        <div class="rating-icons-wrap"><?php 
                         for ($i = 1; $i <= 5; $i++) { 
                          if ($mpblogpost['rating'] < $i) {
                          $rdecimal = 'EMPTY_STAR';
                          if(ceil ($mpblogpost['rating']) == $i) {
                            $rates = explode('.', $mpblogpost['rating']);
                            if(!empty($rates[1]) ) {
                              if((float)"0.$rates[1]" < 0.6000) {
                                $rdecimal = 'HALF_STAR';
                              }
                              if((float)"0.$rates[1]" > 0.6000) {
                                $rdecimal = 'FULL_STAR';
                              }
                            }
                          }
                        if($rdecimal=='HALF_STAR') { ?>
                          <div class="rating-icons half">
                            <span class="red"></span>  
                            <span class="grey"></span>  
                            <i class="fa fa-star"></i>
                          </div>
                        <?php } if($rdecimal=='FULL_STAR') { ?>
                          <div class="rating-icons full">
                            <span class="red"></span>  
                            <span class="grey"></span>  
                            <i class="fa fa-star"></i>
                          </div>
                        <?php } if($rdecimal=='EMPTY_STAR') { ?>
                          <div class="rating-icons">
                            <span class="red"></span>  
                            <span class="grey"></span>  
                            <i class="fa fa-star"></i>
                          </div>
                        <?php } } else { ?>
                        <div class="rating-icons full">
                          <span class="red"></span>  
                          <span class="grey"></span>  
                          <i class="fa fa-star"></i>
                        </div>
                        <?php } ?>
                        <?php } ?>
                        </div>
                        </div>
                      </div>
                    </li>
                    <?php } ?>
                  </ul>
            <?php if($show_sdescription) { ?><p class="desc"><?php echo $mpblogpost['sdescription']; ?><!--<?php if(!empty($mpblogpost['sdescription']) && $show_readmore) { ?> <a href="<?php echo $mpblogpost['href'];?>">  <?php echo $text_readmore; ?> </a> <?php } ?>--></p><?php } ?> 
            <div class="other-data"><?php echo $mpblogpost['date_available'],"<span>&nbsp;|&nbsp;</span>", $mpblogpost['viewed']," <i class='fa fa-eye' style='color: #000'></i>","<span>&nbsp;|&nbsp;</span>","By ",$mpblogpost['author']; ?>
	        </div>
            <!--<ul class="list-inline dar clearfix cmt-wsh">
              <?php if($show_wishlist) { ?>
              <li title="<?php echo $mpblogpost['wishlist']; ?>">
                <span class="mpbloglike <?php if($mpblogpost['isLikeByMe']) { echo 'liked'; } ?>" data-id="<?php echo $mpblogpost['mpblogpost_id']; ?>">
                <i class="fa fa-heart <?php if(!$mpblogpost['isLikeByMe']) { echo 'fa-heart-o'; } ?>"></i>
                <span><?php echo $mpblogpost['wishlist']; ?></span>
                </span>
              </li>
            <?php } ?>
            <?php if($show_comments) { ?>
              <li class="pull-right" title="<?php echo $mpblogpost['comments']; ?>">
                <i class="fa <?php if($mpblogpost['comments']) { echo 'fa-comments'; } else { echo 'fa-comments-o'; } ?>"></i>
                <?php echo $mpblogpost['comments'].' '. $text_comment; ?>
              </li>
              <?php } ?>
            </ul>-->
            <?php if($show_tag && $mpblogpost['tag']) { ?>
              <ul class="list-inline blog-tags">
                <li>
                  <i class="fa fa-tags"></i>
                  <?php foreach($mpblogpost['tag'] as $rtag) { ?>
                  <?php if(!empty($rtag['href'])) { ?>
                  <a href="<?php echo $rtag['href']; ?>"><?php echo $rtag['tag']; ?></a>
                  <?php } else { ?>
                  <?php echo $rtag['tag']; ?>
                  <?php } ?>
                  ,
                  <?php } ?>
                </li>
              </ul>
            <?php } ?>
            <a href="<?php echo $mpblogpost['href'];?>" class="read-more"><h3><?php echo $text_readmore; ?></h3></a>
        </div>
      </div>
    </div>

	<?php } ?>
</div>
</div>
<div class="<?php if($themename !== 'journal2')  { echo 'row'; } if($themename == 'journal2') { echo 'xl-100'; } ?>">
	<div class="col-sm-6 xl-50 text-left">&nbsp;<?php echo $pagination; ?></div>
	<div class="col-sm-6 xl-50 text-right"><?php echo $results; ?></div>
</div>
<?php } ?>
<script type="text/javascript">
$('#category-slider').multislider({interval: 5000});
$("#category-slider").on('swipeleft',  function(){
    $('.MS-right').click();
});
$("#category-slider").on('swiperight',  function(){
    $('.MS-left').click();
});
</script>