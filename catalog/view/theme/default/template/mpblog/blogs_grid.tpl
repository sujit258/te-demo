<?php echo $header; ?>
<script src="https://www.true-elements.com/catalog/view/javascript/multislider.min.js" type="text/javascript"></script>
<script src="https://www.true-elements.com/catalog/view/javascript/jquery.detect_swipe.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/masonry/3.1.5/masonry.pkgd.min.js"></script>
<style>
    .inner-layout{
        box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.05) !important;
    }
    
     .inner-layout:hover{
        box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
    }
    
    @media(max-width:590px){
        .breadcrumb {
            	font-size: 15px;
            	color: #f49a25;
            	margin-bottom: 39px;
            	padding: 29px 0;
            	text-align:center;
            	text-transform: capitalize;
            	border-radius:0;
            	background-size: 100% 100%;
            	background-repeat: none;
        }
    }

    @media(min-width:591px){
        
        .breadcrumb {
        	font-size: 15px;
        	color: #f49a25;
        	margin-bottom: 39px;
        	padding: 29px 0;
        	text-align:center;
        	text-transform: capitalize;
        	border-radius:0;
        	background-size: 100% 100%;
        	background-repeat: none;
        }
    }
    
    #container-fluid, .pagination {
        background: #fafbfd !important;
    }
    
    .footer-breaking {
        margin: 0!important;
    }
    #content row {
        margin-top: 0px!important;
    }
    .mp-grid-layout .inner-layout .caption h4 a:hover {
        color: #f49a25!important;
    }
    .pink {
        color: #feac00;
    }
    .page-heading {
        font-size: 33px !important;
    }
    .mpblog-layout {
        margin-bottom: 25px;
    }
    .caption {
        padding-bottom: 40px!important;
    }
    .caption h2 {
        text-align: center;
        min-height: 75px;
    }
    .caption h2 a {
        font-size: 17.5px!important;
        text-align: center;
        line-height: 25px;
        color: #000;
    }
    .caption h2 a:hover {
        color: #f49a25;
    }
    .other-data {
        border-top: 2.5px dotted #000;
        border-bottom: 2.5px dotted #000;
        text-align: center;
        padding: 10px;
        font-size: 14px;
    }
    .read-more {
        position: absolute;
        bottom: 0%;
        left: 35%;
        width: 30%;
        right: 35%;
    }
    .read-more:hover h3 {
        background: #000;
    }
    .read-more h3 {
        text-align: center;
        padding: 11px 0;
        background: #f49a25;
        color: #fff;
        font-weight: 600;
        border-radius: 4px;
    }
    .other-data span {
        font-size: 25px;
        vertical-align: top;
    }
    #mpblogtenetsubcates .tenet-container {
        margin-bottom: 25px;
        padding-left: 7px!important;
        padding-right: 7px!important;
    }
    .img-tenet {
        min-width: 1px!important;
        display: table;
        margin: auto;
        width: 44%;
        margin-bottom: 20px;
    }
    .text-tenet {
        text-align: center;
        font-weight: 600;
    }
    @media(max-width: 1000px) {
        .img-tenet {
            width: 60%!important;
        }
    }
.category-title {
    text-align: center;
    font-weight: bold;
    padding: 5px;
}
.category-title:hover {
    color: #f49a25;
}
.category-image {
    padding: 0 15%;
    margin-left: auto;
    margin-right: auto;
    display: table;
}
@media(max-width: 1000px) {
    #category-slider .MS-controls button {
        top: 30%!important;
    }
    .category-title {
        font-size: 14px!important;
    }
}
#category-slider {
    position: relative;
    margin-bottom: 20px
}
#category-slider .MS-content {
  margin: 15px 5%;
  overflow: hidden;
  white-space: nowrap;
}
#category-slider .MS-content .item {
  display: inline-block;
  height: 100%;
  overflow: hidden;
  position: relative;
  vertical-align: top;
  width: 25%;
}
@media (max-width: 1200px) {
  #category-slider .MS-content .item {
    width: 25%;
  }
}
@media (max-width: 992px) {
  #category-slider .MS-content .item {
    width: 33.3333%;
  }
}
@media (max-width: 767px) {
  #category-slider .MS-content .item {
    width: 50%;
  }
}
#category-slider .MS-controls button {
  position: absolute;
  border: none;
  background: transparent;
  font-size: 30px;
  outline: 0;
  top: 35%;
}
#category-slider .MS-controls button:hover {
  cursor: pointer;
  color: #f49a25;
}
#category-slider .MS-controls .MS-left {
  left: 0px;
}
@media (max-width: 992px) {
  #category-slider .MS-controls .MS-left {
    left: -2px;
  }
}
#category-slider .MS-controls .MS-right {
  right: 0px;
}
@media (max-width: 992px) {
  #category-slider .MS-controls .MS-right {
    right: -2px;
  }
}
.MS-controls .fa {
    color: #000;
}
.MS-controls .fa:hover {
    color: #f49a25;
}
</style>
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
<div id="container-fluid">
<div id="container" class="container j-container <?php echo $journal_class; ?>">

	<div class="row"><?php echo $column_left; ?>
		<?php if($theme_name == 'journal2') { ?>
			<?php echo $column_right; ?>
		<?php } ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<div class="mp-grid-layout mp-blog"><br /><br />
				<h1 class="page-heading"><span class="pink">Blogs</span></h1>
				<?php if ($mpblog_tenets_categories) { ?>
			    <h2 style="padding-bottom: 15px;">Read stories about</h2>
			    <div class="mpblog-subcate">
			    	<div class="<?php if($themename !== 'journal2')  { echo 'row'; } ?>">
			    		<div class="col-md-12 col-xs-12 col-sm-12">
			    			<div id="mpblogtenetsubcates">
			    				<?php foreach ($mpblog_tenets_categories as $tenets) { ?>
			    				    <div class="col-md-3 col-xs-6 col-sm-3 tenet-container">
			    				        <a href="<?php echo $tenets['href']; ?>">
			    					        <?php if($tenets['image']) { ?>
			    					            <img class="img-responsive img-tenet" src="<?php echo $tenets['image']; ?>" alt="<?php echo $tenets['name']; ?>" title="<?php echo $tenets['name']; ?>" />
			    					        <?php } ?>
			    					        <h4 class="text-tenet"><?php echo $tenets['name']; ?> </h4>
			    					    </a>
			    				    </div>
			    				<?php } ?>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			<?php } ?>
				<?php if($mpblog_categories) { ?>
                    <div id="category-slider">
                        <div class="MS-content">
                            <?php foreach($mpblog_categories AS $category) { ?>
                                <div class="item">
                                    <a href="<?php echo $category['href']; ?>">
                                        <img alt="True Elements" class="img-responsive category-image" src="<?php echo $category['image']; ?>">
                                        <h4 class="category-title"><?php echo $category['name']; ?></h4>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="MS-controls">
                            <button class="MS-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                            <button class="MS-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                <?php } ?>
				<?php if ($mpblogposts) { ?>
					<div class="row test">
						<?php foreach($mpblogposts as $mpblogpost) { 
						$cols = isset($mpblog_blog_design[0]) ? (12/$mpblog_blog_design[0]) : 4; 
						$jcols = 50;
						if($theme_name == 'journal2') {
						switch($cols) {
							case 12:
								$jcols = 100;
							break;
							case 6:
								$jcols = 50;
							break;
							case 4:
								$jcols = 33;
							break;
							case 3:
								$jcols = 25;
							break;
							case 2:
								$jcols = 16;
							break;							
							default:
								$jcols = 50;
						}
						}

						?> 
							<div class="test1 mpblog-layout mpblogpost-grid col-lg-<?php echo $cols; ?> col-md-<?php echo $cols; ?> col-sm-6 col-xs-12 xl-<?php echo $jcols; ?> lg-<?php echo $jcols; ?> md-50 sm-100 xs-100">
							<div class="inner-layout">
								<div class="image">
									<?php if($mpblogpost['showImage']) { ?>
									<div class="video-container"><a href="<?php echo $mpblogpost['href']; ?>"><img src="<?php echo $mpblogpost['thumb']; ?>" alt="<?php echo $mpblogpost['name']; ?>" title="<?php echo $mpblogpost['name']; ?>" class="img-responsive" /></a></div>
									<?php } else { ?>
									<div class="video-container"><iframe width="<?php echo $mpblogpost['width']; ?>" height="<?php echo $mpblogpost['height']; ?>" src="<?php echo $mpblogpost['iframeVideo']; ?>" frameborder="0" allowfullscreen></iframe></div>
									<?php } ?>
									<!--<?php if($show_viewed) { ?>
										<div class="view" title="<?php echo $mpblogpost['viewed']; ?>">
											<i class="fa fa-eye"></i>
											<?php echo $mpblogpost['viewed']; ?>
										</div>
									<?php } ?> -->
								</div>
								<div>
								<div class="caption">
									<h2><a href="<?php echo $mpblogpost['href']; ?>"><?php echo $mpblogpost['name']; ?></a></h2>
									<ul class="dar clearfix list-inline mpblogpost-id-<?php echo $mpblogpost['mpblogpost_id']; ?>">
									
										<?php if($show_rating) { ?>
											<li class="col-sm-12 col-xs-12 xl-100" title="<?php echo $mpblogpost['rating']; ?>">
												<div class="rating small-rating text-center">
													<div class="rating-icons-container text-center">
														<div class="rating-icons-wrap"><?php 
														 for ($i = 1; $i <= 5; $i++) { 
															if ($mpblogpost['rating'] < $i) {
															$rdecimal = 'EMPTY_STAR';
															if(ceil ($mpblogpost['rating']) == $i) {
																$rates = explode('.', $mpblogpost['rating']);
																if(!empty($rates[1]) ) {
																	if((float)"0.$rates[1]" < 0.6000) {
																		$rdecimal = 'HALF_STAR';
																	}
																	if((float)"0.$rates[1]" > 0.6000) {
																		$rdecimal = 'FULL_STAR';
																	}
																}
															}
														if($rdecimal=='HALF_STAR') { ?>
															<div class="rating-icons half">
																<span class="red"></span>  
																<span class="grey"></span>  
																<i class="fa fa-star"></i>
															</div>
														<?php } if($rdecimal=='FULL_STAR') { ?>
															<div class="rating-icons full">
																<span class="red"></span>  
																<span class="grey"></span>  
																<i class="fa fa-star"></i>
															</div>
														<?php } if($rdecimal=='EMPTY_STAR') { ?>
															<div class="rating-icons">
																<span class="red"></span>  
																<span class="grey"></span>  
																<i class="fa fa-star"></i>
															</div>
														<?php } } else { ?>
														<div class="rating-icons full">
															<span class="red"></span>  
															<span class="grey"></span>  
															<i class="fa fa-star"></i>
														</div>
														<?php } ?>
														<?php } ?>
														</div>
													</div>
												</div>
											</li>
										<?php } ?>
									</ul>
									<?php if($show_sdescription) { ?><p class="desc"><?php echo $mpblogpost['sdescription']; ?><!-- <?php if(!empty($mpblogpost['sdescription']) && $show_readmore) { ?> <a href="<?php echo $mpblogpost['href'];?>" style="font-weight: 600;">  <?php echo $text_readmore; ?> </a> <?php } ?> --></p><?php } ?>
							            <div class="other-data"><?php echo $mpblogpost['date_available'],"<span>&nbsp;|&nbsp;</span>", $mpblogpost['viewed']," <i class='fa fa-eye' style='color: #000'></i>","<span>&nbsp;|&nbsp;</span>","By ",$mpblogpost['author']; ?>
							            </div>
										<?php if($show_tag && $mpblogpost['tag']) { ?>
										<ul class="list-inline blog-tags">
											<li>
												<i class="fa fa-tags"></i>
												<?php foreach($mpblogpost['tag'] as $rtag) { ?>
												<?php if(!empty($rtag['href'])) { ?>
				                <a href="<?php echo $rtag['href']; ?>"><?php echo $rtag['tag']; ?></a>
				                <?php } else { ?>
				                <?php echo $rtag['tag']; ?>
				                <?php } ?>
												,
												<?php } ?>
											</li>
										</ul>
										<?php } ?>
										<a href="<?php echo $mpblogpost['href'];?>" class="read-more"><h3><?php echo $text_readmore; ?></h3></a>
									</div>
								</div>
							</div>
							</div>
						<?php } ?>
					</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12"><center><?php echo $pagination; ?></center></div>
				 
			</div>
			<?php } else { ?>
			<div class="row">
			<div class="col-sm-12 xl-100">
				<p><?php echo $text_empty; ?></p>
				<div class="buttons text-right">
					<div><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
				</div>
			</div>
			</div>
			<?php } ?>
			<?php echo $content_bottom; ?></div>
			<?php if($theme_name != 'journal2') { ?>
	        	<?php echo $column_right; ?>
	      	<?php } ?></div>
</div>
</div>
<script type="text/javascript">
$('#category-slider').multislider({interval: 5000});
$("#category-slider").on('swipeleft',  function(){
    $('.MS-right').click();
});
$("#category-slider").on('swiperight',  function(){
    $('.MS-left').click();
});
/*$(function(){
    var m = new Masonry($('.test').get()[0], {
        gutter: 50,
        horizontalOrder: true,
        originLeft: true,
        itemSelector: ".test1"
    });
});*/
</script>
<?php echo $footer; ?>