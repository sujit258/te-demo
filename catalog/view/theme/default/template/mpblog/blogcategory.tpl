<?php echo $header; ?>
<style>
    #container-fluid, .pagination {
        background: #fafbfd !important;
    }
    .mpblog-cate-wrap {
        padding-left: 0!important;
    }
    .mpblog-cate-wrap h2 {
        font-size: 21px!important;
        font-family: roboto, 'Oswald', sans-serif;
        margin-bottom: 0!important;
    }
    #mpblogtenetsubcates .tenet-container {
        margin-bottom: 25px;
        padding-left: 7px!important;
        padding-right: 7px!important;
    }
    .pd-0 {
        padding-left: 0!important;
        padding-right: 0!important;
    }
    .img-tenet {
        min-width: 1px!important;
        display: table;
        margin: auto;
        width: 44%;
        margin-bottom: 20px;
    }
    .text-tenet {
        text-align: center;
        font-weight: 600;
    }
    @media(max-width: 1000px) {
        .img-tenet {
            width: 60%!important;
        }
    }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div id="container-fluid" class="mp-blog j-container <?php echo $themeclass; ?>">
	<div class="container"><?php echo $column_left; ?>
		<?php if($themename == 'journal2') { ?>
			<?php echo $column_right; ?>
		<?php } ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?> pd-0"><?php echo $content_top; ?>	
			<div class="mpblog-cate-wrap">		
				<h2><?php echo $heading_title; ?></h2>
				<?php if ($thumb || $description) { ?>
					<div class="<?php if($themename !== 'journal2')  { echo 'row'; } if($themename == 'journal2') { echo 'journal-box'; } ?>">
					<!--<?php $rowno = 12; if ($thumb) {  $rowno = 8; ?>
						<div class="col-sm-4 xl-33 sm-100"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
					<?php } ?>-->
					<?php if ($description) { ?>
						<div class="col-sm-12"><?php echo $description; ?></div>
					<?php } ?>
					</div>
				<?php } ?>
			</div>
			<?php if ($mpblog_tenets_categories) { ?>
			    <h2 style="padding-bottom: 15px;">Read stories about</h2>
			    <div class="mpblog-subcate">
			    	<div class="<?php if($themename !== 'journal2')  { echo 'row'; } ?>">
			    		<div class="col-md-12 col-xs-12 col-sm-12">
			    			<div id="mpblogtenetsubcates">
			    				<?php foreach ($mpblog_tenets_categories as $tenets) { ?>
			    				    <div class="col-md-3 col-xs-6 col-sm-3 tenet-container">
			    				        <a href="<?php echo $tenets['href']; ?>">
			    					        <?php if($tenets['image']) { ?>
			    					            <img class="img-responsive img-tenet" src="<?php echo $tenets['image']; ?>" alt="<?php echo $tenets['name']; ?>" title="<?php echo $tenets['name']; ?>" />
			    					        <?php } ?>
			    					        <h4 class="text-tenet"><?php echo $tenets['name']; ?> </h4>
			    					    </a>
			    				    </div>
			    				<?php } ?>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			<?php } ?>
			
			<?php if ($mpblogcategories) { ?>
			<div class="mpblog-subcate">
				<h3><?php echo $text_categories; ?></h3>
				<div class="<?php if($themename !== 'journal2')  { echo 'row'; } ?>">
					<div class="col-sm-12 xl-100">
						<ul class="list list-inline owl-carousel" id="mpblogsubcates">
							<?php foreach ($mpblogcategories as $mpblogcategory) { ?>
							<li class="item"><a href="<?php echo $mpblogcategory['href']; ?>">
								<?php if($mpblogcategory['image']) { ?>
								<img class="img-responsive" src="<?php echo $mpblogcategory['image']; ?>" alt="<?php echo $mpblogcategory['name']; ?>" title="<?php echo $mpblogcategory['name']; ?>" /> <?php } ?>
								<h4><?php echo $mpblogcategory['name']; ?> </h4>
								</a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php echo $view_posts; ?>
			<?php if (!$mpblogcategories && !$mpblogposts) { ?>
			<div class="row">
				<div class="col-sm-12 xl-100">
					<p><?php echo $text_empty; ?></p>
					<div class="buttons text-right">
						<div><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php echo $content_bottom; ?></div>
			<?php if($themename != 'journal2') { ?>
	        	<?php echo $column_right; ?>
	      	<?php } ?>
	    </div>
<script type="text/javascript"><!--
$('#mpblogsubcates').owlCarousel({
	items: 6,
	autoPlay: 3000,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: false
});
--></script></div>
<?php echo $footer; ?>