<?php if ($mpblogposts) { ?>
<hr/>
<div class="">
	<?php foreach($mpblogposts as $mpblogpost) { ?>
	<div class="mpblogpost-layout mpblogpost-list">
		<div class="row">
			<div class="col-sm-4 xl-33 sm-100">
				<div class="image">
				<?php if($mpblogpost['showImage']) { ?>
				<a href="<?php echo $mpblogpost['href']; ?>"><img src="<?php echo $mpblogpost['thumb']; ?>" alt="<?php echo $mpblogpost['name']; ?>" title="<?php echo $mpblogpost['name']; ?>" class="img-responsive" /></a>
				<?php } else { ?>
				<div class="video-container"><iframe width="<?php echo $mpblogpost['width']; ?>" height="<?php echo $mpblogpost['height']; ?>" src="<?php echo $mpblogpost['iframeVideo']; ?>" frameborder="0" allowfullscreen></iframe></div>
				<?php } ?>
				<ul class="list-inline dar hidden-xs">
					<?php if($show_author) { ?>
					<li title="<?php echo $mpblogpost['author']; ?>">
						<i class="fa fa-user"></i>                    
						<?php if(!empty($mpblogpost['authorurl'])) { ?>
						<a href="<?php echo $mpblogpost['authorurl']; ?>">
							<?php echo $mpblogpost['author']; ?>
						</a>
						<?php } else { ?>
						<?php echo $mpblogpost['author']; ?>
						<?php } ?>
					</li>
					<?php } ?>
				</ul>  
				<?php if($show_viewed) { ?>
					<div class="view" title="<?php echo $mpblogpost['viewed']; ?>">
						<i class="fa fa-eye"></i>
						<?php echo $mpblogpost['viewed']; ?>
					</div>
				<?php } ?>
			</div>
			</div>
			<div class="col-sm-8 xl-66 sm-100">
				<div class="caption">
					<h4><a href="<?php echo $mpblogpost['href']; ?>"><?php echo $mpblogpost['name']; ?></a></h4>
					<ul class="list-inline dar mpblogpost-id-<?php echo $mpblogpost['mpblogpost_id']; ?>">
						<?php if($show_date) { ?>
						<li title="<?php echo $mpblogpost['date_available']; ?>">
							<i class="fa fa-calendar"></i>
							<?php if(!empty($mpblogpost['date_availableurl'])) {  ?>
							<a href="<?php echo $mpblogpost['date_availableurl']; ?>"><?php echo $mpblogpost['date_available']; ?></a>
							<?php } else { ?>
							<?php echo $mpblogpost['date_available']; ?>
							<?php } ?>
						</li>
						<?php } ?>
						<?php if($show_rating) { ?>
							<li title="<?php echo $mpblogpost['rating']; ?>">
								<div class="rating small-rating">
									<div class="rating-icons-container text-center">
									<div class="rating-icons-wrap"><?php 
									 for ($i = 1; $i <= 5; $i++) { 
										if ($mpblogpost['rating'] < $i) {
										$rdecimal = 'EMPTY_STAR';
										if(ceil ($mpblogpost['rating']) == $i) {
											$rates = explode('.', $mpblogpost['rating']);
											if(!empty($rates[1]) ) {
												if((float)"0.$rates[1]" < 0.6000) {
													$rdecimal = 'HALF_STAR';
												}
												if((float)"0.$rates[1]" > 0.6000) {
													$rdecimal = 'FULL_STAR';
												}
											}
										}
									if($rdecimal=='HALF_STAR') { ?>
										<div class="rating-icons half">
											<span class="red"></span>  
											<span class="grey"></span>  
											<i class="fa fa-star"></i>
										</div>
									<?php } if($rdecimal=='FULL_STAR') { ?>
										<div class="rating-icons full">
											<span class="red"></span>  
											<span class="grey"></span>  
											<i class="fa fa-star"></i>
										</div>
									<?php } if($rdecimal=='EMPTY_STAR') { ?>
										<div class="rating-icons">
											<span class="red"></span>  
											<span class="grey"></span>  
											<i class="fa fa-star"></i>
										</div>
									<?php } } else { ?>
									<div class="rating-icons full">
										<span class="red"></span>  
										<span class="grey"></span>  
										<i class="fa fa-star"></i>
									</div>
									<?php } ?>
									<?php } ?>
									</div>
									</div>
								</div>
							</li>
							<?php } ?>
					</ul>
					<?php if($show_sdescription) { ?> <p class="desc"> <?php echo $mpblogpost['sdescription1'];?> <?php if(!empty($mpblogpost['sdescription1']) && $show_readmore) { ?> <a href="<?php echo $mpblogpost['href'];?>">  <?php echo $text_readmore; ?> </a> <?php } ?> </p> <?php } ?>
					<ul class="list-inline dar">
						<?php if($show_wishlist) { ?>
						<li title="<?php echo $mpblogpost['wishlist']; ?>">
							<span class="mpbloglike <?php if($mpblogpost['isLikeByMe']) { echo 'liked'; } ?>" data-id="<?php echo $mpblogpost['mpblogpost_id']; ?>">
							<i class="fa fa-heart <?php if(!$mpblogpost['isLikeByMe']) { echo 'fa-heart-o'; } ?>"></i>
							<span><?php echo $mpblogpost['wishlist']; ?></span>
							</span>
						</li>
						<?php } ?>					 
						<?php if($show_comments) { ?>
						<li title="<?php echo $mpblogpost['comments']; ?>">
							<i class="fa <?php if($mpblogpost['comments']) { echo 'fa-comments'; } else { echo 'fa-comments-o'; } ?>"></i>
							<?php echo $mpblogpost['comments'].' '. $text_comment; ?>
						</li>
						<?php } ?>
					</ul>
					<?php if($show_tag && $mpblogpost['tag']) { ?>
					<ul class="list-inline blog-tags">
						<li>
							<i class="fa fa-tags"></i>
							<?php foreach($mpblogpost['tag'] as $rtag) { ?>
							<?php if(!empty($rtag['href'])) { ?>
				              <a href="<?php echo $rtag['href']; ?>"><?php echo $rtag['tag']; ?></a>
				              <?php } else { ?>
				              <?php echo $rtag['tag']; ?>
				              <?php } ?>
								,
							<?php } ?>
						</li>
					</ul>
					<?php } ?>
				</div>
			</div>
		</div>  
	</div>
	<?php } ?>
</div>
<div class="row">
	<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
	<div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
<?php } ?>