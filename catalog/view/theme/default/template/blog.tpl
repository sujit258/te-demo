<style>
  #ac-wrapper {
    position: fixed;
    top: 0;
    left: 124px;
    width: 100%;
    height: 100%;
    z-index: 1001;
  }
  .sub-description a,ol li a {
       color: #f49a25 !important;
    }
  button {
    float: right !important;
    font-size: 13px !important;
    background: #f49a25;
    color: #fff;
  }

  @media screen and (min-width: 900px){
    .capcha .form-control{
      width: 60% !important;
    }
  }
/*.popup{
width: 369px;
height: 399px;
background: transparent;
position: relative;
top: 457px;left: -118px;
}
*/
.capcha{
  padding: 0 !important;
}
.close1 { 
  position: absolute; 
  top: -2px; 
  right: 2px; 
  transition: all 200ms;
  font-size: 39px; 
  font-weight: bold; 
  text-decoration: none; 
  color: #333; }
  .close1:hover { 
    color: orange; }

    @media(max-width:590px){
      
      .mobile { 
        width: 369px;
        height: 399px;
        background: transparent;
        position: relative;
        top: 193px;
        left: -124px;
      }
      
      .close1 { 
        position: absolute; 
        top: -2px; 
        right: 56px; 
        transition: all 200ms;
        font-size: 39px; 
        font-weight: bold; 
        text-decoration: none; 
        color: #333; }
        .close1:hover { 
          color: orange; }
        }

        .description h3 {
          color: #000000 !important;
          /*font-family: roboto, 'Oswald', sans-serif !important;*/
          line-height: 1.1 !important;
          font-size: 17px !important;
          margin-top: 15px !important;
          font-weight: 600!important;
        }
        .description h4 {
          margin-top: 20px !important;
          line-height: 26px !important;
          font-weight: 600;
          font-size: 18px;
          text-align: left;
          direction: inline-block;

        }
        .description .sub-description {
          color: #000 !important;
          text-align: justify !important;
          line-height: 23px !important;
        }
        .description {
          width: 100%!important;
        }
        body, #comment p, .help-block {
          font-size: 15px;
        }
        .card__title {
          line-height: 1.5!important;
        }
        .card__title a{
          color: #000!important;
        }
        .card__title a:hover {
          color: #f49a25!important;
          cursor: pointer;
        }
        .owl-carousel:hover .owl-buttons div.owl-next {
          top: 120px!important;
        }
        .owl-carousel:hover .owl-buttons div.owl-prev {
          top: 120px!important;
        }
        .mp-blog .blog-info {
        /*  margin-left: -15px!important;*/
          padding: 15px 0px 15px 0px;
          margin: 5px 0px 5px 0px !important;

        }
        #section-comment {
         /* margin-left: -15px!important;*/
          padding: 0px!important;
        }
        #section-comment label {
          line-height: 28px!important;
        }
        .module-title {
          margin-bottom: 25px !important;
        }
        #section-comment .form-control {
          border: 1px solid #ccc!important;
        }
        .disc1 li {
          list-style-type: disc;
        }
        ol li {
          list-style-type: decimal!important;
          text-align: justify!important;
        }
        ol {
          margin-left: -25px!important;
        }
        ul li {
          text-align: justify!important;
        }
        .sub-image {
          text-align: center!important;
        }
        .sub-image img{
          width: 70%!important;
        }
        .full-image img {
          width: 100vw!important;
        }
        #comments-container {
          padding-left: 0px!important;
          padding-right: 0px!important;
        }
        .fa-calendar {
          color: #000 !important;
        }
        h5 {
          font-size: 15px!important;
          color: #000!important;
        }
        .fa-exclamation-circle {
          margin-right: 10px;
        }
        .alert {
          font-size: 15px!important;
          padding: 10px!important;
        }
        .alert-danger {
          color: #a94442!important;
        }
        .alert-success {
          color: #a94442!important;
        }
        .form-control {
          background: #fff;
          border: 1px solid #f2f2f2;
          border-radius: 3px!important;
          padding: 20px 12px!important;
          color: #000!important;
        }
        .form-control:focus {
          border-color: #80bdff!important;
          outline: 0!important;
          box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25)!important;
        }
        .btn-primary:active:focus {
          background: #f49a25!important;
        }
        @media(max-width:800px) {
          .capcha {
            width: 100vw;   
          }
        }
        #atic_twitter, .at3winsvc_twitter {
            display: none !important;
        }
        .addthis_toolbox {
            bottom: 0%!important;
        }
        @media(max-width: 1000px) {
            .addthis_toolbox {
                bottom: 10%!important;
            }
            .social-share-container {
                min-height: 35px!important;
            }
        }
        

.msg11 {
    font-size: 15px;
}
.msg12 {
    font-size: 15px;
    margin-top: 4px;
}


.qty-container {
    width: 50%;
}
.breadcrumb {
    margin-bottom: 15px!important;
}

.category-title {
    font-size: 35px;
}
.sew {
  width: 100%;
}

.banner-text-lg {
    font-size: 6em!important;
}

.banner-text-md {
    font-size: 4em!important;
}

h1 {
  font-size: 6em;
  font-weight: bold;
}

h3 {
  font-size: 4em;
}
.intro {
  position: absolute;
  text-align: center;
  top: 280px;
  left: 470px;
}

.space {
  height: 80px;
}

h2 {
  font-weight: bold;
}

.space1 {
  padding: 0;
  margin: 0;
  background-color: #f49a25;
  height: 50px;
  text-align: center;
  font-size: 2em;
}

  .product-image {
      /*margin: auto;
      display: table;*/
      padding: 5%;
  }
  h2 {
      text-align: center;
  }
  
  .product-name-text{
        font-size: 16px;
        text-align: center;
        font-weight: 700;
    }
    .product-name-text a {
        color: #000;
    }
    .options {
        padding: 0px!important;
        min-height: 55px!important;
    }
    .product-name-text {
        min-height: 40px;
        text-align: left;
    }
    .product-options {
        text-align: center;
        float: left;
        background: #fff!important;
        width: 90%;
        color: #000;
    }
    .button-product-cart {
        float: right!important;
        font-size: 13px!important;
        background: #f49a25;
        color: #fff;
    }
    .button-personalize {
        font-size: 13px!important;
        float: right !important;
        background: #f49a25;
        color: #fff;
        margin-top: 0px;
    }
    .price-label {
        padding: 5px 0px !important;
        min-height: 38px!important;
        width: 100%!important;
    }
    .price {
        margin-bottom: 0px!important;
        min-height: 22px!important;
    }
    .quantity {
        height: 35px!important;
        width: 45%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    .oos {
        background: #777!important;
        border-radius: 4px!important;
        float: right!important;
        font-size: 13px!important;
        color: #fff;
    }
    .oos:hover {
        cursor: not-allowed!important;
        color: #fff!important;
    }
    .button-product-cart:focus, .button-personalize:focus {
        color: #fff!important;
    }
    .button-product-cart:hover, .button-personalize:hover {
        color: #fff!important;
        background: #000!important;
    }
    .center {
        text-align: center;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 23px !important;
        padding: 9px 10px !important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .category-title {
        margin: 70px 0 20px 0;
    }
    .main-row {
        margin-right: 0px!important;
        margin-left: 0px!important;
    }
    .price-new {
        display: inline-block!important;
        font-size: 15px!important;
        color: #f49a25!important;
        text-align: left!important;
        font-weight: 700!important;
        margin: 0!important;
    }
    .price-old {
        display: inline-block!important;
        color: #5c5c5c!important;
        font-size: 12px!important;
        margin-left: 5px!important;
        text-decoration: line-through!important;
    }
    .deals-overlay {
        right: 0px;
        top: 10px;
        position: absolute;
        background:#f49a25;
        color:#fff;
        padding: 4px;
        font-weight: 600;
        width: 25%;
        text-align: center;
        z-index: 1;
    }
    .top-banner {
        margin-top: 0!important;
        margin-bottom: 25px!important;
    }
    .top-banner img {
        width: 100%;
    }
    .top-image img {
        width: 100%;
    }
    .top-banners {
        margin-bottom: 25px;
    }
    .readyToEatBanner, .readyToCookBanner, .seedsMixesBanner, .rawAndroastedSeedsBanner {
        cursor: pointer;
    }
    #readyToEatBreakfast, #seedsMixes, #readyToCookBreakfast, #rawAndRoastedSeeds, #others, #tea, #wcbs, #combo {
        padding-top: 15px;
    }
    .sub-category {
        font-size: 25px;
        margin-top: 25px;
        font-weight: bold;
    }
    .price, .coupon {
        padding-left: 0px!important;
        padding-right: 0px!important;
    }
    .coupon {
        min-height: 20px!important;
        text-align: left;
        font-size: 12px!important;
        margin-bottom: 0!important;
    }
    .options .radio input {
        display: none!important;
    }
    .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px!important;
    }
    .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0!important;
        text-align: center;
        min-width: 52px;
    }
    .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    .product-container {
        padding: 0!important;
    }
@media(min-width: 1000px) {
    .mobile {
        min-height: 570px!important;
    }
}
@media (max-width: 1000px){
    .qty-container {
        width: 70%!important;
    }
    .coupon {
        text-align: left!important;
    }
    .intro {
        position: absolute;
        text-align: center;
        top: 15%;
        left: 22%;
    }
    .button-personalize {
        font-size: 14px!important;
        margin-top: 0px!important;
    }
    .product-name-text {
        min-height: 40px!important;
        font-size: 14px!important;
    }
    .banner-text-lg {
        font-size: 3em!important;
    }
    .banner-text-md {
        font-size: 1.5em!important;
    }
    .category-title {
        margin: 40px 0 20px 0!important;
        font-size: 28px!important;
    }
    .mobile{
        margin-left: 0px;
        margin-bottom: 10px;
        min-height: 470px!important;
        padding: 7px!important;
    } 
    .container {
        width: 100%!important;
    }
    .product-name-text .select{
        height: 90px;
        font-size: 20px;
        text-align: center; 
        /*font-family: roboto, 'Oswald', sans-serif;*/
    }
    .deals-overlay {
        width: 35%!important;
    }
    .readyToEatBanner, .readyToCookBanner, .seedsMixesBanner, .rawAndroastedSeedsBanner {
        margin-top: 10px;
    }
    .quantity-container {
        width: 60%!important;
    }
    .radio label span {
        font-size: 12px!important;
    }
    .options .radio {
        margin: 0px 0px 5px -2px!important;
    }
    .options {
        min-height: 55px!important;
    }
    .category-name h4 {
        line-height: 15px;
        font-size: 12px!important;
        padding: 8px 1px!important;
        margin-top: 0!important;
    }
    .category-name, .bookmark-heading {
        padding-left: 5px!important;
        padding-right: 5px!important;
    }
}
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .category-name h4 {
        border: 1px solid #c6c6c6!important;
        border-radius: 4px;
        padding: 14px;
        margin-top: 5px!important;
        font-size: 12px!important;
        font-weight: bold;
        text-align: center;
    }
    .category-name h4:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
        cursor: pointer!important;
    }
    .bookmark-heading {
        font-weight: 600;
    }
    
    .deals-discount {
        border-radius: 50%;
        right: 0px;
        top: 5%;
        position: absolute;
        background: #f49a25;
        text-align: center;
        z-index: 5;
        width: 50px;
    }
    
    .deals-discount h5 {
        color: #fff;
        font-weight: 600;
        padding: 1px;
    }
    
    .price-coupon{
        font-weight: bold;
        padding-right: 5px;
    }  
</style>
<!--<div onClick="PopUp('hide')" >
<div id="ac-wrapper" style='display:none'>
    <div class="popup mobile">
        <a class="close1">×</a>
        <center>
            <a href="https://www.gympik.com/" target="_new"><img src="/catalog/view/theme/default/image/gympik-popup.jpg"></a>
          
        </center>
    </div>
  </div>-->
  <?php echo $header; ?>
  <!-- Add Next and Previous Blog Link stats-->
  <div class="mpblognavs <?php echo $themeclass; ?> sujit">
    <?php if($nextprev['prev']['href']) { ?>
    <div class="mpblognav mpblognavs-nextlink">
      <a href="<?php echo $nextprev['prev']['href']; ?>">
        <span class="mpblognavs-image">
          <img style="<?php if ($nextprev['prev']['posttype']) { ?> width: <?php echo $nextprev['prev']['width ']; ?>px; height: <?php echo $nextprev['prev']['height']; ?>px; <?php } ?>" src="<?php echo $nextprev['prev']['image']; ?>" alt="<?php echo $nextprev['prev']['name']; ?>"/>
        </span>
        <span class="mpblognavs-icon">
          <i class="fa fa-arrow-left"></i>
        </span>
        <div class="clearfix"></div>
        <?php if($nextprev['prev']['name']) { ?>
        <span class="mpblognavs-title">
          <h4><?php echo $nextprev['prev']['name']; ?></h4>
        </span>
        <?php } ?>  
      </a>
    </div>
    <?php } ?>
    <?php if($nextprev['next']['href']) { ?>
    <div class="mpblognav mpblognavs-prevlink">
      <a href="<?php echo $nextprev['next']['href']; ?>">
        <span class="mpblognavs-icon">
          <i class="fa fa-arrow-right"></i>
        </span>
        <span class="mpblognavs-image">
          <img style="<?php if ($nextprev['next']['posttype']) { ?> width: <?php echo $nextprev['next']['width ']; ?>px; height: <?php echo $nextprev['next']['height']; ?>px; <?php } ?>" src="<?php echo $nextprev['next']['image']; ?>" alt="<?php echo $nextprev['next']['name']; ?>"/>
        </span>
        <div class="clearfix"></div>
        <?php if($nextprev['next']['name']) { ?>
        <span class="mpblognavs-title">
          <h4><?php echo $nextprev['next']['name']; ?></h4>
        </span>
        <?php } ?>
      </a>
    </div>
    <?php } ?>
  </div>
  <!-- Add Next and Previous Blog Link ends-->

  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="container" style="width: auto;overflow: hidden;background: #fafbfd;"><div id="container" class="container mp-blog j-container <?php echo $themeclass; ?>" style="background-color: #fff;">
    <?php echo $content_top; ?>
    <div class="row"><?php echo $column_left; ?>
      <?php if($themename == 'journal2') { ?>
      <?php echo $column_right; ?>
      <?php } ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-md-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-md-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-md-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>">
    <!-- <h1 style="font-size: 20px !important;"><?php echo $heading_title; ?></h1> -->



        <div class="row">
          <div class="col-lg-12 xl-100 sm-100">
            <div class="blog-details">
              
             <!--  <?php if ($showImage) { ?>
              <?php if ($thumb && $blog_image) { ?>
              <ul class="thumbnails">
                <?php if($blog_image_popup) { ?>
                <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="img-responsive" /></a></li>
                <?php } else { ?>
                <li><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="img-responsive" /></li>
                <?php } ?>
              </ul>
              <?php } ?>
              <?php } else { ?>
              <div class="video-container">
                <iframe width="<?php echo $width; ?>" height="<?php echo $height; ?>" src="<?php echo $iframeVideo; ?>" frameborder="0" allowfullscreen></iframe>
              </div>
              <?php } ?> -->
              
             
              <br />
              <style type="text/css">

                @media screen and (max-width: 490px) and (min-width: 300px){
                  /*.blog-container .jumbotron{
                    min-height: 200px !important;
                  }
*/
                  /*.blog-description{
                    bottom: 40px !important;
                  }*/
                }

                  @media screen and (max-width: 900px) and (min-width: 500px){
                  .col-sm-12{
                   width: 100% !important;
                  }

                  .blog-info{
                    text-align: left !important;
                  }
                }

                .blog-container .jumbotron{
                  /*background: url('https://harippa.co.in/demo/image/catalog/blog/vegan-plant-based.jpeg') no-repeat;*/
                 /* background-size: cover;*/
                  padding: 0 !important;
                  background: none !important;
                  /*min-height: 420px;*/
                  margin-bottom: 0 !important;
                 }
                
               

                .blog-description .blog-content {
                  border-radius: 0 !important;
                  background: #fff !important;
                  display: inline-block;
                  
                }
               
                .blog-padding{
                    padding: 0!important;
                  }

                  .blog-content h1{
                    padding: 15px 0px
                  }

                  @media screen and (min-width: 500px){
                  .blog-description{
                  position: relative;
                  bottom: 22px;
                  text-align: center;
                }

                .blog-description .blog-content {
                     width: 96%;
                  }

                  .blog-description .blog-content{
                    padding: 8px;
                  }
                  .blog-content-img:nth-child(even)  {
                      background: #faebcc !important;
                    }

                    .blog-img{
                    padding: 8px 8px 8px 0px !important;
                  }
                }
                
               @media screen and (max-width: 900px){
                .blog-content h1{
                  padding: 8px 0px !important;
                }

                .blog-info{
                  padding: 2px 0px 10px 0px !important;
                }
               }

               @media screen and (min-width: 900px){
                .blog-info{
                  text-align: right;
                }
               }

              </style>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-container blog-padding">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-padding">
                 <?php if ($showImage) { ?>
                <?php if ($thumb && $blog_image) { ?>
                 <ul class="thumbnails">
                <?php if($blog_image_popup) { ?>
                <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="img-responsive" /></a></li>
                </ul>
                 <?php } else { ?>
                <div class="jumbotron">
                  <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="img-responsive" />
                </div>
                <?php } ?>
                <?php } ?>
                <?php } else { ?>
                <div class="video-container">
                  <iframe width="<?php echo $width; ?>" height="<?php echo $height; ?>" src="<?php echo $iframeVideo; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
              <?php } ?>
               </div>


              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-description blog-padding">
                <div class="blog-content">
                  <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 blog-padding"><h1><?php echo $heading_title; ?></h1></div>
                  <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 blog-padding">
                    
                   <ul class="blog-info list-inline mpblogpost-id-<?php echo $mpblogpost_id; ?>">
            <?php if($show_date) { ?>
            <li  class="blog-padding" title="<?php echo $date_available; ?>"><i class="fa fa-calendar"></i> <?php if(!empty($date_availableurl)) { ?> <a href="<?php echo $date_availableurl; ?>"> <?php echo $date_available; ?></a> <?php } else { ?> <?php echo $date_available; ?><?php } ?></li><!-- <span>|</span>-->
            <?php } ?>
          <!--<?php if($show_author) { ?>
            <li title="<?php echo $author; ?>"><i class="fa fa-user"></i> <?php if(!empty($authorurl)) { ?> <a href="<?php echo $authorurl; ?>"> By <?php echo $author; ?> </a> <?php } else { ?> <?php echo $author; ?> <?php } ?></li> <span class="hidden-xs">|</span>
            <?php } ?>-->
            <?php if($show_comments) { ?>
            <li class="hidden-xs" title="<?php echo $comments; ?>"><i class="fa <?php if($comments) { echo 'fa-comments'; } else { echo 'fa-comments-o'; } ?> "></i> <?php echo $comments .' '. $text_comment; ?></li>  
            <?php } ?>
            
            
          </ul>
                </div>
                  <div class="description"><?php echo $description; ?></div>
                </div>
              </div>
              </div>
              
              <!-- AddThis Button BEGIN -->
              <div class="col-md-12 col-xs-12 col-sm-12 social-share-container">
                <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
              </div>
              <!-- AddThis Button END -->
              <div class="row">
                <?php if($show_tag && $tags) { ?>
                <div class="col-sm-8 col-xs-12 xl-75 sm-100 tags" style="margin-bottom: 15px;">
                  <ul class="list-unstyled">
                    <li>
                      <div class="icon-tag"><i class="fa fa-tags"></i>  <?php echo $text_tags; ?></div>
                      <?php foreach($tags as $rtag) { ?>
                      <?php if(!empty($rtag['href'])) { ?>
                      <a href="<?php echo $rtag['href']; ?>"><?php echo $rtag['tag']; ?></a>
                      <?php } else { ?>
                      <span><?php echo $rtag['tag']; ?></span>
                      <?php } ?>
                      <?php } ?>
                    </li>
                  </ul>
                </div>              
                <?php } ?>


              </div>
              
              
            </div>
          </div>   
        </div>
      
        <br />
        <hr>
        <div class="row"><div class="container">
          <div class="module-title"><span>Add your Perspective</span></div>
          <div class="col-md-8 col-xs-12" id="section-comment">
            <?php $class='col-md-12 xl-100'; if($allow_comment && $allow_rating ) { $class='col-md-6 col-xs-11 xl-50 sm-100'; } ?>
            <?php if($allow_comment) { ?>
            <div class="<?php echo $class; ?> col-xs-12 capcha">
              <form class="form-horizontal" id="form-comment">
               
                <?php if ($comment_guest) { ?>
                <div class="form-group required">
                  <div class="col-md-12 xl-100 sm-100">
                    <label class="control-label" for="input-name">Your Name</label>
                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-md-12 xl-100 sm-100">
                    <label class="control-label" for="input-comment">Comment</label>
                    <textarea name="text" rows="5" id="input-comment" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-left">
                    <button type="button" id="button-comment" class="button btn btn-primary"><?php echo $button_add_comment; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <div class="inner-rating">
                  <div class="login-text"><?php echo $text_login_comment; ?></div>
                </div>
                <?php } ?>
              </form>
            </div>
            <?php } ?>
            <?php if($allow_rating) { ?>
            <div class="<?php echo $class; ?> rating-wrap">
              <h1><i class="fa fa-thumbs-up"></i> <?php echo $heading_give_rating; ?></h1>
              <div class="inner-rating">
                <form class="form-horizontal" id="form-rating">
                  <?php if ($rating_guest) { ?>
                  <div id="rating"></div>
                  <div class="form-group required">
                    <div class="col-md-12 xl-100 sm-100">

                      <div class="rating-container">
                        <input type="hidden" name="rating" id="input-rating" class="rating hide" value="" data-error="<?php echo $error_rating; ?>" />
                        <div class="rating-icons-wrap">
                          <?php for($i = 1; $i <= 5; $i++) { ?>
                          <div class="rating-icons" data-value="<?php echo $i; ?>">
                            <span class="red"></span>  
                            <span class="grey"></span>  
                            <i class="fa fa-star"></i>
                          </div>
                          <?php } ?>
                        </div>
                      </div>

                      <button class="btn btn-primary" type="button" id="button-rating"><?php echo $button_give_rating; ?></button>
                    </div>
                  </div>
                  <?php } else { ?>
                  <?php echo $text_login_rating; ?>
                  <?php } ?>
                </form>
              </div>
            </div>
            <?php } ?>
            
            
          </div><div class="col-xs-1"></div>
          
          <?php if($show_comments) { ?>
          <div class="col-md-12 xl-100 sm-100 col-xs-12" id="comments-container">
            
            <div id="comment"></div>
          </div>
          <?php } ?>
          
        </div></div>
       
        
        <input type="hidden" name="product_id" />
        <?php if ($products) { ?>
        <br/>
        <hr>
        <div class="module-title"><span>Recommended Products for you</span></div>
        <div id="prodcarouselblog" class="owl-carousel">
          <?php foreach ($products as $product) { ?>
          <div class="blog-post col-md-12 col-xs-12 col-sm-12">
            <div class="row">
              <div class="col-md-12 col-xs-12 carousel-container">
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" width="100%"/></a><br />
                
                <div class="des-container">
						<h2 class="product-name hidden-xs hidden-sm"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
						<h2 class="product-name hidden-md hidden-xl hidden-lg"><a href="<?php echo $product['href']; ?>"><?php echo $product['name_mobile']; ?></a></h2>
						<?php $is_optionqty = 1; ?>
						<?php if($product['options']) { ?>
						    <div class="options options-<?php echo $product['product_id']; ?>">
						        <?php foreach($product['options'] AS $option) { ?>
						            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
						        <?php } ?>
						    </div>
						<?php } else { ?>
						    <div class="options <?php if($category_id == 586) { echo 'hidden-md hidden-xl hidden-lg hidden-xs hidden-sm';} ?>"></div>
						    <?php $is_optionqty = 0; ?>
						<?php } ?>

						<div class="price-label <?php if($category_id == 586) { echo 'hidden-xs hidden-sm hidden-md hidden-xl hidden-lg'; } else { echo 'col-md-12 col-xs-12 col-sm-12'; } ?>">
							<div class="box-price">
								<?php if ($product['price']) { ?>
								<p class="price col-md-12 col-xs-12 col-sm-12">
								  <?php if (!$product['special']) { ?>
                                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } else { ?>
								        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
						                <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } ?>
								</p>
								<?php if ($product['coupon_discount']) { ?>
            			    	            <span class="price-coupon price-coupon-<?php echo $product['product_id']; ?>"><b> <?php echo $product['coupon_discount']; ?> </b></span><span> <?php echo $product['coupon'] ? 'Using code: <b>' . $product['coupon'] . '</b>' : ''; ?></span>
            			    	    <?php } ?>	
								<?php } ?>
							</div>
						</div>
						
						<?php if($product['personalized'] == 0) { ?>
						    <?php if($is_optionqty != 1 && $product['quantity'] > 0) { ?>
						        <div class="quantity-container" style="width: 50%;">
						            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $product['product_id']; ?>)" />
						            <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>)" />
						            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $product['product_id']; ?>)" />
						        </div>
						        <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-sm hidden-xs" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><?php echo $button_cart; ?></button>
						        <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
						<?php } else { ?>
						    <p class="coupon col-md-12 col-xs-12 col-sm-12 pull-right"></p>
						    <?php if($product['quantity'] > 0) { ?>
						        <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
						<?php } ?>
					</div>
    	                
              </div>
            </div>
            
          </div>
          <?php } ?>   
        </div> 
        <?php } ?>
        

        
        <?php if ($categories) { ?>
        <h3 class="box-heading"><?php echo $text_related_categories; ?></h3>
        <hr/>
        <div class="row">
          <?php $i = 0; ?>
          <?php foreach ($categories as $category) { ?>
          <?php if ($column_left && $column_right) { ?>
          <?php $class = 'col-xs-12 col-sm-6 xl-50 sm-100'; ?>
          <?php } elseif ($column_left || $column_right) { ?>
          <?php $class = 'col-xs-12 col-md-4 xl-33 sm-100'; ?>
          <?php } else { ?>
          <?php $class = 'col-xs-12 col-sm-3 xl-25 sm-100'; ?>
          <?php } ?>
          <div class="product-grid-item <?php echo $class; ?>">
            <div class="product-thumb transition product-wrapper">
              <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
              <div class="product-details">
                <div class="caption">
                  <h4 class="name"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
                  <p><?php echo $category['description']; ?></p>
                </div>
              </div>
            </div>
          </div>
          <?php if (($column_left && $column_right) && (($i+1) % 2 == 0)) { ?>
          <div class="clearfix visible-md visible-sm"></div>
          <?php } elseif (($column_left || $column_right) && (($i+1) % 3 == 0)) { ?>
          <div class="clearfix visible-md"></div>
          <?php } elseif (($i+1) % 4 == 0) { ?>
          <div class="clearfix visible-md"></div>
          <?php } ?>
          <?php $i++; ?>
          <?php } ?>
        </div>
        <?php } ?>

        <?php echo $content_bottom; ?></div>
        <?php if($themename != 'journal2') { ?>
        <?php echo $column_right; ?>
        <?php } ?>
      </div>
    </div></div>
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('.price-new-' + product_id).html(json.new_price.special);
					$('.price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.coupon) {
					    $('.price-coupon-' + product_id).html(json.new_price.coupon); 
					}
					
					if(json.new_price.special) {
					    $('.price-new-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('.price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.des-container .options .radio input[type="radio"]').click(function() {
        $('.des-container .options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
<script type="text/javascript"><!--
      $('#prodcarouselblog').owlCarousel({
        items: 4,
        // autoPlay: 6000,
        navigation: true,
        pagination: false
    });
</script>

<script type="text/javascript"><!--
  $('#carouselblog').owlCarousel({
        items: 3,
        // autoPlay: 6000,
        navigation: true,
        pagination: false
    });
--></script> 

<script type="text/javascript"><!--
  $('#comment').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#comment').fadeOut('slow');

    $('#comment').load(this.href);

    $('#comment').fadeIn('slow');
  });

  $('#comment').load('index.php?route=mpblog/blog/comment&mpblogpost_id=<?php echo $mpblogpost_id; ?>');

  $('#button-comment').on('click', function() {
    $.ajax({
      url: 'index.php?route=mpblog/blog/writeComment&mpblogpost_id=<?php echo $mpblogpost_id; ?>',
      type: 'post',
      dataType: 'json',
      data: $("#form-comment").serialize(),
      complete: function() {
        $('#button-comment').button('reset');
      },
      success: function(json) {
        $('.alert-success, .alert-danger').remove();

        if (json['error']) {
          $('#form-comment').before('<div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
        }

        if (json['success']) {
          $('#form-comment').before('<div class="alert alert-success success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
          $('input[name=\'name\']').val('');
          $('#input-comment').val('');
          $('#form-comment input[type="text"]').val('');

        // load latest comments
        $('#comment').fadeOut('slow');
        $('#comment').load('index.php?route=mpblog/blog/comment&mpblogpost_id=<?php echo $mpblogpost_id; ?>');
        $('#comment').fadeIn('slow');

      }
    }
  });
  });

   

  $(document).ready(function() {
    
    <?php if($blog_image_popup) { ?>
      $.each($('.thumbnails'), function() {
        $(this).magnificPopup({
          type:'image',
          delegate: 'a',
          gallery: {
            enabled:true
          }
        });
      });
      <?php } ?>
      $('#comments a:first').tab('show');
      
    });
    //--></script>


<script type="text/javascript">
      function PopUp(hideOrshow) {
        if (hideOrshow == 'hide') document.getElementById('ac-wrapper').style.display = "none";
        else document.getElementById('ac-wrapper').removeAttribute('style');
      }
      window.onload = function () {
        setTimeout(function () {
          PopUp('show');
        }, 5000);
      }


      var modal = document.getElementById('ac-wrapper');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
</script>



<?php echo $footer; ?>
