<?php echo $header; ?>
<style type="text/css">
     @media screen and (min-width: 1000px){
      .prep-time:nth-child(1),.prep-time:nth-child(2){
        border-right: 1px solid #ccc;
    }
    .recipe_icon, .recipe-icon, .recipe-icon-1{
        margin: 0 12px;
    }
}
 .recipe-content{
        padding-top: 20px;
 }

.buttons, .add-products{
    margin: 0 15px;
}
.ingredients-title{
    margin: 10px 0;
}
.recipe-owl-carousel .owl-wrapper-outer .owl-item img{
    width: 80px;
    margin: 0 20px;
}
.recipe-owl-carousel .owl-wrapper-outer .owl-item p{
    text-align: center;
   font-size: 14px;
  font-weight: 500;
}
.recipe-owl-carousel .owl-buttons div{
    background: none !important;
    width: 10px !important;
    height: 10px !important;
}
.recipe-owl-carousel .owl-buttons div:hover::before{
    color: #252525 !important;
}

.padding-0{
        padding: 0;
    }
.directions{
    margin-bottom: 30px;
    background: #fbfbfb;
}
.directions ul{
    padding: 0;
    margin: 15px 0;
    text-align: justify;
}
.directions ul li span{
  padding-left: 8px;
}
.directions ul li{
 line-height: 1.7;
}
.directions ul li:before{
content: '\2022';
color: #f49a25;
font-size: 17px;
}

.icons{
   margin: 10px 0;
}
.prep-time h5{
        font-weight: 600;
        color: #f49a25;
        font-size: 15px;
        float: left;
        font-weight: 700;
    }`

    .prep-time{
       margin: 12px 0;
       text-align: right;
       display: inline-block;
       text-align: left;
   }
   .recipe-icon{
    width: 35px;
    float: left;
   }

   .recipe_icon{
    width: 30px;
    float: left;
   }

   .recipe-icon-1{
    width: 33px;
    float: left;
   }
   .prep-time span{
    float: left;
    margin: 8px;
    color: grey;
    font-size: 12px;
    font-weight: 700;
}
.prep-time span i.far{
  font-size: 19px;
  color: #f49a25;
  margin: 8px 0;  
}
@media screen and (min-width: 300px) and (max-width: 500px){
/*.recipe-owl-carousel .owl-wrapper-outer .owl-item img{
    width: 60px !important;
}*/
 .icons{
        padding: 0;
        height: 39px;
        margin: 10px 0 0 0;
  }
.recipe-des{
   padding: 0; 
}
.recipe_icon{
 width: 20px !important;
}
.recipe-icon, .recipe-icon-1{
    width: 24px !important;
}
.prep-time:nth-child(2), .prep-time:nth-child(3){
    border-left: 1px solid #ccc;
}

.recipe-icon, .recipe-icon-1{
    margin: 0px 5px !important;
}
.prep-time span{
margin: 3px !important;
}
.recipe_icon{
    width: 20px !important;
}
.recipe-icon, .recipe-icon-1{
    width: 24px !important;
}
}
</style>
<style>
    .page-header-recipe {
        border-bottom: 1px solid #eee;
        margin-top: 3px;
        margin-bottom: 7px;
    }
    .product-image {
        padding: 5%;
    }
    @media(max-width: 1000px) {
        .carousel-container {
            margin: 0 8px !important;
            width: 90%important;
        }
    }
    #content1 h1{
        border: medium none;
        font-size: 21px;
        font-weight: 500;
    }
    .carousel-container {
        padding: 0!important;
        margin: 1px 15px;
        width: 95%!important;
    }
    .description1 {
        font-size: 15px;
        text-align: justify;
        font-family: roboto, 'Oswald', sans-serif;
    }
    ol {
        margin: 0 0 0 -42px;
    }
    .description1 li {
        list-style: decimal;
        font-size: 15px;
        font-family: roboto, 'Oswald', sans-serif;
        font-weight: 700;
    }
    .description1 li span {
        font-size: 15px !important;
        font-family: roboto, 'Oswald', sans-serif;
        font-weight: 400 !important;
    }
    .ingredients {
        line-height: 19pt;
    }
    .blog-post center {
        margin: 10px 0;
    }
    .module-title {
        margin-bottom: 10px!important;
    }
    #prodcarousel4 {
        margin-bottom: 10px!important;

    }
    .model {
        top: 145px;
    }
    .popup-heading {
        font-family: SackersGothicStd;
        font-size: 27px;
        font-weight: 900;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: normal;
        text-align: center;
        color: #454545;
    }
    .popup-content {
        width: 558px;
        height: 78px;
        font-family: CircularStd;
        font-size: 20px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.67;
        letter-spacing: 1.2px;
        text-align: center;
        color: #000;
    }
    .modal-body {
        top: -13px;
        background: url(https://www.true-elements.com/image/img/bundle_cart_image_1.png);
        background-repeat: repeat;
        background-size: auto;
        background-size: 748px 299px;
        background-repeat: no-repeat;
        height: 280px;
        padding: -22px;
        right: 66px;
    }
    .modal-dialog{
        background:#FFF;
        top: 81px;
    }
    .modal-header {
        border-bottom: 0;
    }
    .noty_theme__mint.noty_type__success {
        color: #f49a25!important;
    }
    .noty_theme__mint.noty_bar .noty_body {
        padding: 20px!important;
    }
    .breadcrumb {
        margin-bottom: 0!important;
    }
    .addthis_toolbox {
        bottom: 0%!important;
    }
    .product-name-text{
        font-size: 16px;
        text-align: center;
        font-weight: 700;
    }
    .product-name-text a {
        color: #000;
    }
    .options {
        padding: 0px!important;
        min-height: 55px!important;
    }
    .product-name-text {
        min-height: 40px;
        text-align: left;
    }
    .product-options {
        text-align: center;
        float: left;
        background: #fff!important;
        width: 90%;
        color: #000;
    }
    .button-product-cart {
        float: right!important;
        font-size: 13px!important;
        background: #f49a25;
        color: #fff;
    }
    .button-personalize {
        font-size: 13px!important;
        float: right !important;
        background: #f49a25;
        color: #fff;
        margin-top: 0px;
    }
    .price-label {
        padding: 5px 0px !important;
        min-height: 38px!important;
    }
    .price {
        margin-bottom: 0px!important;
        min-height: 22px!important;
    }
    .quantity {
        height: 35px!important;
        width: 45%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    .oos {
        background: #777!important;
        border-radius: 4px!important;
        float: right!important;
        font-size: 13px!important;
        color: #fff;
    }
    .oos:hover {
        cursor: not-allowed!important;
        color: #fff!important;
    }
    .button-product-cart:focus, .button-personalize:focus {
        color: #fff!important;
    }
    .button-product-cart:hover, .button-personalize:hover {
        color: #fff!important;
        background: #000!important;
    }
    .center {
        text-align: center;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 23px !important;
        padding: 1px 10px !important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .category-title {
        margin: 70px 0 20px 0;
    }
    .main-row {
        margin-right: 0px!important;
        margin-left: 0px!important;
    }
    .price-new {
        display: inline-block!important;
        font-size: 15px!important;
        color: #f49a25!important;
        text-align: left!important;
        font-weight: 700!important;
        margin: 0!important;
    }
    .price-old {
        display: inline-block!important;
        color: #5c5c5c!important;
        font-size: 12px!important;
        margin-left: 5px!important;
        text-decoration: line-through!important;
    }
    .price, .coupon {
        padding-left: 0px!important;
        padding-right: 0px!important;
    }
    .coupon {
        min-height: 20px!important;
        text-align: left;
        font-size: 12px!important;
        margin-bottom: 0!important;
    }
    .options .radio input {
        display: none!important;
    }
    .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px!important;
    }
    .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0!important;
        text-align: center;
        min-width: 52px;
    }
    .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .price-label {
        width: 100%!important;
    }
    .qty-container {
        width: 50%;
    }
    .thumb-video iframe {
        width: 100%;
    }
    @media(max-width: 1000px) {
        .mobile {
            padding-left: 7px!important;
            padding-right: 7px!important;
        }
        .addthis_toolbox {
            bottom: 10%!important;
        }
        .social-share-container {
            min-height: 35px!important;
        }
        .qty-container {
            width: 70%!important;
        }
        .coupon {
            text-align: left!important;
        }
        .button-personalize {
            font-size: 14px!important;
            margin-top: 0px!important;
        }
        .product-name-text {
            min-height: 40px!important;
            font-size: 14px!important;
        }
        .quantity-container {
            width: 60%!important;
        }
        .radio label span {
            font-size: 12px!important;
        }
        .options .radio {
            margin: 0px 0px 5px -2px!important;
        }
        .options {
            min-height: 80px!important;
        }
        .thumb-video iframe {
            height: 100%;
        }
    }
</style>

<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><span><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
</ul>

<div style="background-color: #fafbfd;width: auto; overflow: hidden;">
     <div class="container" style="padding-right:0!important; padding-left:0!important;background: #fff;">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-md-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-md-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-md-12'; ?>
        <?php } ?>

         <div id="content">
            <?php echo $content_top; ?>
               <!--Recipe Image -->
               <div class="recipe-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="thumb-video col-lg-5 col-md-5 col-sm-5 col-xs-12 padding-0">
                    <?php if($youtube_url) { ?>
                        <iframe width="1140" height="600" src="<?php echo $youtube_url; ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"  frameborder="0" allowfullscreen></iframe>
                    <?php } else if($image) { ?>
                        <img class="img-responsive" style="width:100%;" src="<?php echo $image; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                    <?php } ?>
        
                    <div class="icons col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
                        <div class="prep-time col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                            <img class="recipe_icon" src="https://harippa.co.in/demo/image/catalog/recipes/clock.png">
                            <span class="clock-icon-span">15 minutes</span>
                        </div>
                        <div class="prep-time col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                            <img class="recipe-icon" src="https://harippa.co.in/demo/image/catalog/recipes/serves.png">
                            <span>2 people</span>
                        </div>
                        <div class="prep-time col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-0">
                            <img class="recipe-icon-1" src="https://harippa.co.in/demo/image/catalog/recipes/steps.png">
                            <span>6 steps</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 recipe-des">
                    <div id="content1" class="page-header-recipe col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
                      <h1><?php echo $heading_title; ?></h1><span>
                     </div>

                    <?php if ($recipies) { ?>
                        <div class="description1 col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
                            <?php echo $description; ?>
                        </div>
                    <?php } else { ?>
                     <p><?php echo $text_empty; ?></p>
                     <div class="buttons">
                        <div class="pull-right">
                            <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
                        </div>
                    </div>
                <?php } ?>
             
             <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ingredients-title padding-0">Ingredients</h3>
             <div class="recipe-owl-carousel owl-theme col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0">
                <div class="item"><img src="https://harippa.co.in/demo/image/catalog/recipe/banana.jpg"><p>Banana</p></div>
                <div class="item"><img src="https://harippa.co.in/demo/image/catalog/recipe/oatsbowl.jpg"><p>Rolled Oats</p></div>
                <div class="item"><img src="https://harippa.co.in/demo/image/catalog/recipe/honey.jpg"><p>Honey</p></div>
                <div class="item"><img src="https://harippa.co.in/demo/image/catalog/recipe/almonds.jpg"><p>Almonds</p></div>
                <div class="item"><img src="https://harippa.co.in/demo/image/catalog/recipe/dates.jpg"><p>Dates</p></div>
                <div class="item"><img src="https://harippa.co.in/demo/image/catalog/recipe/flax-seeds.jpg"><p>Flax Seeds</p></div>
                <div class="item"><img src="https://harippa.co.in/demo/image/catalog/recipe/milk.jpg"><p>Milk</p></div>
            </div>
        </div>
        </div>
        <!-- Directions-->
        <?php if ($recipies) { ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 directions">
               <h3><?php echo $text_directions; ?></h3>
               <?php echo $directions; ?>  
           </div>
       <?php } ?>

       <!--related products-->
       <?php if ($products) { ?>
        <input type="hidden" name="product_id" value="" />
        <div class="module-title col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <span>Products Used in this Recipe</span>
            <hr>
        </div>
        <div class="pull-right add-products">
            <?php if($in_stock_products) { ?>
                <div class="buttons hidden-sm hidden-xs" style="text-align: center;">
                    <a id="checkall_button"  onclick="submittocart('<?php echo $in_stock_products; ?>')" type="button" class="hidden-sm hidden-xs btn btn-primary btn-lg btn-block" style="width: 100%;display:inline-block;">Add Products to Cart</a>
                </div>
                <a id="checkall_button"  onclick="submittocart('<?php echo $in_stock_products; ?>')" type="button" class="hidden-md hidden-lg hidden-xlg btn button button-cart-mobile"><i class="fa fa-shopping-cart" style="color:#fff;"></i></a>
            <?php } else { ?>
                <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
                <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
            <?php } ?>
        </div>
        <div id="prodcarousel4" class="owl-carousel col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php foreach ($products as $product) { ?>
                <div class="col-md-12 col-sm-12 col-xs-12 mobile">
                    <div class="product-thumb item-inner">
                        <div class="images-container">
                            <a href="<?php echo $product['href']; ?>">
                                <div class="pro_img">
                                    <?php if($product['rotator_image']){ ?>
                                        <img class="image2 product-image" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" />
                                    <?php } ?>
                                    <img alt="True Elements" class="product-image" src="<?php echo $product['thumb']; ?>">
                                </div>
                            </a>
                        </div>
                        <p class="product-name-text hidden-xs hidden-sm"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>
                        <p class="product-name-text hidden-md hidden-xl hidden-lg"><a href="<?php echo $product['href']; ?>"><?php echo $product['name_mobile']; ?></a></p>

                        <?php $is_optionqty = 1; ?>
                        <?php if($product['options']) { ?>
                            <div class="options options-<?php echo $product['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
                                <?php foreach($product['options'] AS $option) { ?>
                                    <?php $countoption = count($option['product_option_value']); ?>
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                        <div class="radio">
                                            <?php if($countoption == 1) { ?>
                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                <?php } else { ?>
                                                    <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                        <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                    <?php } ?>
                                                    <?php if($option_value['price']) { ?>
                                                        <span><?php echo $option_value['name']; ?></span>
                                                    <?php } ?>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } else { ?>
                                <div class="options col-md-12 col-xs-12 col-sm-12"></div>
                                <?php $is_optionqty = 0; ?>
                            <?php } ?>

                            <?php if($product['product_type'] != 5) { ?>
                                <div class="price-label col-md-12 col-xs-12 col-sm-12">
                                    <div class="box-price">
                                        <?php if ($product['price']) { ?>
                                            <p class="price">
                                                <?php if (!$product['special']) { ?>
                                                    <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
                                                <?php } else { ?>
                                                    <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
                                                    <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
                                                <?php } ?>
                                            </p>
                                            <p class="coupon col-md-12 col-xs-12 col-sm-12 pull-right"><?php echo $product['coupon'] ? 'Coupon: <b>' . $product['coupon'] . '</b>' : ''; ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php if($is_optionqty != 1 && $product['quantity'] > 0) { ?>
                                    <div class="qty-container" style="margin-top: 10px;">
                                        <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $product['product_id']; ?>)" />
                                        <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>)" />
                                        <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $product['product_id']; ?>)" />
                                    </div>
                                    <button type="button" class="btn button button-product-cart cart-<?php echo $product['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $product['product_id']; ?>)">Add to Cart</button>
                                    <button type="button" class="btn button button-product-cart cart-<?php echo $product['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
                                <?php } else { ?>
                                    <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
                                    <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="price-label col-md-12 col-xs-12 col-sm-12">
                                    <div class="box-price">
                                        <p class="price"></p>
                                    </div>
                                </div>
                                <?php if($product['quantity'] > 0) { ?>
                                    <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
                                <?php } else { ?>
                                    <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
                                    <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>   
            </div> 
        <?php } ?>
        <!--related recipes-->
        <?php if ($related_recipes) { ?>
            <br/>
            <div class="module-title col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <span>Browse more Recipes</span>
                <hr/>
            </div>
            <div id="recipecarousel" class="owl-carousel col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php foreach ($related_recipes as $related_recipe) { ?>
                    <div class="blog-post">
                        <div class="carousel-container">
                            <a href="<?php echo $related_recipe['href']; ?>"><img src="<?php echo $related_recipe['thumb']; ?>" alt="<?php echo $related_recipe['title']; ?>" title="<?php echo $related_recipe['title']; ?>" width="100%"/></a><br />
                            <center><a style="color: #000; font-size: 15px;" href="<?php echo $related_recipe['href']; ?>"><?php echo $related_recipe['title']; ?></a></center> 
                        </div>
                    </div>
                <?php } ?>   
            </div> 
        <?php } ?>
    </div><!--content end-->
     </div>
</div>
<script type="text/javascript">
    var owl = $('.recipe-owl-carousel');
    owl.owlCarousel({
        rewind:true,
        margin:10,
        navigation:true,
        dots: false,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [991,3],
        itemsTablet: [700,3],
        itemsMobile : [480,3]
    });
</script>
<script type="text/javascript">
$('#prodcarousel4').owlCarousel({
    autoPlay: true,
    items : 4,
    slideSpeed : 1000,
    navigation : true,
    paginationNumbers : true,
    pagination : false,
    stopOnHover : false,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [991,3],
    itemsTablet: [700,2],
    itemsMobile : [480,2]
});
</script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('#recipecarousel').owlCarousel({
        autoPlay: true,
        items : 4,
        slideSpeed : 1000,
        navigation : true,
        paginationNumbers : true,
        pagination : false,
        stopOnHover : false,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [991,3],
        itemsTablet: [700,2],
        itemsMobile : [480,2]
    });
});
</script>
<script type="text/javascript">
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();
    $('#review').fadeOut('slow');
    $('#review').load(this.href);
    $('#review').fadeIn('slow');
});

//$('#review').load('index.php?route=recipe/recipe/review&recipe_id=<?php echo $recipe_id; ?>');

$('#button-review').on('click', function() {
    $.ajax({
        url: 'index.php?route=recipe/recipe/write&recipe_id=<?php echo $recipe_id; ?>',
        type: 'post',
        dataType: 'json',
        data: $("#form-review").serialize(),
        beforeSend: function() {
            $('#button-review').button('loading');
        },
        complete: function() {
            $('#button-review').button('reset');
        },
        success: function(json) {
            $('.alert-success, .alert-danger').remove();
            if (json['error']) {
                $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
            }
            if (json['success']) {
                $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                $('input[name=\'name\']').val('');
                $('textarea[name=\'text\']').val('');
                $('input[name=\'rating\']:checked').prop('checked', false);
            }
        }
    });
});
</script>
<script type="text/javascript">                            
function checkall(me) { 
    if(me.checked) { // check select status
        $('.finicart-tool #checkall').each(function() { //loop through each checkbox
            this.checked = true;  //select all checkboxes with class "checkbox1"               
        });
    } else {
        $('.finicart-tool #checkall').each(function() { //loop through each checkbox
            this.checked = false; //deselect all checkboxes with class "checkbox1"                       
        });         
    }                               
}
</script>
<script type="text/javascript">
    function submittocart(pids) {
        var products = pids.split(',');
        var option_not_checked = 0;
        for (i = 0; i < products.length; i++) {
            addtocart(products[i]);
        }/*
        setTimeout(function(){
            if($("#prodcarousel4 div").hasClass('text-danger')) {
                alert("Please select weight to proceed.");
            }
        }, 1000);*/
    }
</script>
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left" ><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center">' + json['success'] + '</div></div>';
                    
                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        $(".text-danger." + product_id).remove();
        $.ajax({
            type: 'POST',
            url: 'index.php?route=product/live_options/index&product_id=' + product_id,
            data: $('.checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json.success) {
                    $('.price-new-' + product_id).html(json.new_price.special);
                    $('.price-old-' + product_id).html(json.new_price.price);
                    
                    if(json.new_price.special) {
                        $('.price-new-' + product_id).html(json.new_price.special);
                        $('.price-old-' + product_id).html(json.new_price.price);
                    } else {
                        $('.price-new-' + product_id).html(json.new_price.price);
                    }
                }
            },
            error: function(error) {
                console.log('error: '+error);
            }
        });
    }
</script>
<script type="text/javascript"><!--
    $('.options .radio input[type="radio"]').click(function() {
        $('.options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
<?php echo $footer; ?>