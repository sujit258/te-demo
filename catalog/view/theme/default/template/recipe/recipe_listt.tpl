<?php echo $header; ?>
<script src="catalog/view/javascript/multislider.min.js" type="text/javascript"></script>
<script src = "catalog/view/javascript/jquery.detect_swipe.js"></script>
<!--script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/masonry/3.1.5/masonry.pkgd.min.js"></script-->
<style>
    a:focus { color: #000; }
    .desc,.read-more-button,.caption{
       display:none; 
    }
    
    .desc{ padding: 0 14px 0 14px;font-size: 15px; line-height: 20px; min-height: 120px; text-align: justify;}
    
   .recipe-name{padding-left: 14px;padding-bottom: 3px;padding-right: 14px; margin-top: 0!important; min-height: 45px;}
   .desc-container {
       padding: 10px 10px 3px 0px;
   }
    .title1{text-align:center;color: #000000!important;font-size:18px;font-weight:600;}
    
    .title1:hover { color: #f49a25!important;}
    
    h1{ text-align: left;font-size: 20px !important;font-family: roboto,'Oswald', sans-serif;}
    
    .container-fluid, .pagination {
        background: #fafbfd!important;
    }

    .breadcrumb {
        margin-bottom: 0!important;
    }
    .footer-breaking {
        margin: 0!important;
    }
    .pink {
        color: #feac00;
    }
    .page-heading {
        font-size: 33px !important;
    }
    .input-group-addon {
        padding: 0!important;
    }
    #button-search {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
        padding: 5px 17px;
    }
    #input-search {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
        height: 35px;
    }
    .category-title {
        text-align: center;
        font-weight: bold;
        padding: 5px;
        margin-left: 8px;
        margin-right: 8px;
        color: #fff;
        background: #000;
        font-size: 17px;
    }

@media(max-width: 1000px) {
    .recipes-grid {
        padding-left: 0;
        padding-right: 0;
    }
    .recipe-container {
        min-height: 400px!important;
    }
    .recipes-grid {
        min-height: 400px!important;
    }
}

.recipe-container {
    padding: 5px;
    border-radius: 12px;
    background: #fff;
    min-height: 220px;
    -webkit-transition: all 0.4s ease;
    transition: all 0.4s ease;
}
.recipe-container:hover {
    box-shadow: 0px 12px 28px 0px rgba(151,160,204,0.3);
    -webkit-transform: translateY(-8px);
    -moz-transform: translateY(-8px);
    -ms-transform: translateY(-8px);
    -o-transform: translateY(-8px);
    transform: translateY(-8px);
}
.recipe-image-container {
    margin: 10px 13px 0 13px;
    border-radius: 10px;
    margin-bottom: 20px;
}
.recipe-image-container img {
    border-radius: 10px;
    -webkit-transition: all 0.6s ease;
    transition: all 0.6s ease;
}
.recipe-image-container img:hover {
    -webkit-transform: scale(1.1);
    -moz-transform: scale(1.1);
    -ms-transform: scale(1.1);
    -o-transform: scale(1.1);
    transform: scale(1.1);
}

.cat-hover-text {
    position: absolute;
    bottom: 5%;
    text-align: center;
    width: 100%;
    -webkit-transition: all 0.6s ease;
    transition: all 0.6s ease;
}
#category-slider {
    position: relative;
    margin-bottom: 20px;
    margin-top: 15px;
    padding-left: 0!important;
    padding-right: 0!important;
}
#category-slider .item {
    display: inline-block;
    height: 100%;
    overflow: hidden;
    position: relative;
    vertical-align: top;
    width: 100%;
    -webkit-transition: all 0.6s ease;
    transition: all 0.6s ease;
}
.category-image {
    margin-left: auto;
    margin-right: auto;
    display: table;
    border-radius: 15px;
    -webkit-transition: all 0.6s ease;
    transition: all 0.6s ease;
}
#category-slider .item:hover .category-image, #category-slider .item:hover .category-title, .winner-item:hover {
    -webkit-transform: scale(1.05);
    -moz-transform: scale(1.05);
    -ms-transform: scale(1.05);
    -o-transform: scale(1.05);
    transform: scale(1.05);
}
#category-slider .item:hover .cat-hover-text {
    -webkit-transform: translateY(-10px);
    -moz-transform: translateY(-10px);
    -ms-transform: translateY(-10px);
    -o-transform: translateY(-10px);
    transform: translateY(-10px);
}
#category-slider .item-image {
    padding: 8px;
}
.recipes-grid {
    margin-bottom: 30px;
    min-height: 230px;
}
.owl-carousel .owl-buttons div.owl-prev, .owl-carousel .owl-buttons div.owl-next {
    top: 50%!important;
}
.winner-item {
    -webkit-transition: all 0.6s ease;
    transition: all 0.6s ease;
    padding: 10px;
}
.contest-hover {
    top: 4.5%;
    right: 10px;
    position: absolute;
    background: #f49a25;
    text-align: center;
    z-index: 1;
    color: #fff;
}
.contest-author {
    /*bottom: 5%;
    position: absolute;
    background: #f49a25;*/
    background: #000;
    text-align: center;
    z-index: 1;
    color: #fff;
    font-size: 18px;
}
.contest-author div {
    padding: 4px 6px;
}
.contest-hover div {
    padding: 2px 6px;
}
.read-more {
        text-align: left;
        color: #000;
        border-bottom: 2px solid #000;
        padding-bottom: 3px;
        margin-left: 10px;
    }
    .read-more:hover {
        color: #f49a25;
        border-bottom: 2px solid #f49a25;
        
    }
    .read-more-button {
        margin-bottom: 10px;
    }
.pl10 {
    padding-left: 10px!important;
}
.pr10 {
    padding-right: 10px!important;
}
.caption {
    min-height: 30px;
}
/*.test1:nth-child(2n) .desc {
    min-height: 250px;
}*/
.cat-list {
    padding-left: 0;
    font-size: 15px;
}
.cat-list li a {
    color: #000;
}
.cat-list li.current_cat {
    cursor: pointer;
    color: #f49a25;
    margin-left: 10px;
    border-left: 2px solid #f49a25;
    padding-left: 10px!important;
}
.cat-list li {
    padding: 3px;
}
.list-group-item {
    color: #000!important;
    font-size: 16px;
}
a.list-group-item {
    font-weight: 600;
    background-color: #f5f5f5;
}
div.list-group-item {
    padding-left: 10px;
    padding-right: 10px;
}
.button-filter {
    color:#fff!important;
    padding: 7px 10px;
    font-size: 14px;
}
.filter_search {
    margin-bottom: 10px;
    background: #fff;
}
.filter-group .checkbox label {
    width: 90%;
}
.filter-group .checkbox label:hover, .cat-list li a:hover {
    color: #f49a25;
}
.recipe_count {
    width: 10%;
    text-align: center;
    border: 1px solid #fcfcfc;
    background: #ccc;
    font-size: 12px;
    border-radius: 4px;
}
.recipes-grid {
    padding-left: 7px!important;
    padding-right: 7px!important;
}
.panel-footer {
    background: #fff!important;
    padding: 5px 10px!important;
    border-top-width: 1px!important;
}
#mobile-filter {
    transform: translate3d(-120%, 0, 0);
    -webkit-transform: translate3d(-120%, 0, 0);
    transition: all 0.6s;
    -webkit-transition: all 0.6s;
    position: fixed;
    top: 30px;
    overflow: auto;
    box-shadow: 0 0 20px;
    -webkit-box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 1);
    -moz-box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 1);
    background: #fff;
    z-index: 999;
    border-radius: 4px
}
#mobile-filter.opened {
    -webkit-transform: translate3d(0%, 0, 0);
    transform: translate3d(0%, 0, 0);
}
#mobile-filter .panel {
    margin-top: 10px;
    margin-bottom: 10px;
}
#mobile-filter .cat-list li {
    font-size: 14px!important; 
}
#mobile-filter a.list-group-item {
    font-size: 15px!important;
    padding-top: 5px!important;
    padding-bottom: 5px!important;
}
#mobile-filter .filter-group .checkbox label {
    font-size: 14px!important;   
}
#show-filter {
    display: block;
    position: fixed;
    cursor: pointer;
    z-index: 9999;
}
@media(min-width: 1000px) {
    #show-filter, #mobile-filter {
        display: none;
    }
}
#show-filter.opened {
    display: none;
}
#show-filter i:first-child {
    position: fixed;
    top: 30%;
    left: 0;
    font-size: 2rem;
    width: 5rem;
    height: 5rem;
    line-height: 5rem;
    cursor: pointer;
    text-align: center;
    z-index: 120;
    border-top-right-radius: 3.5rem;
    border-bottom-right-radius: 3.5rem;
    -webkit-box-shadow: 0 0 0.5rem rgba(0,0,0,.17);
    box-shadow: 0 0 0.5rem rgba(0,0,0,.17);
    background-color: #000;
}
.filter-close {
    min-height: 25px!important;
}
.filter-close span {
    padding: 7px 0!important;
}
.filter-close span i {
    color: #000!important;
    font-size: 20px!important;
}

#filters{
    
    position: -webkit-sticky;
    position: sticky;
    top: 81px;
    margin-top: -6px;
}
.recipe-panel .list-group {
    height: 600px;
    overflow-y: auto;
    scrollbar-width: thin;
}
</style>

  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
    <?php } ?>
  </ul>
  <div id="show-filter">
      <i class="fa fa-filter"></i>
  </div>
<div class="container-fluid">
    <div class="container" style="padding-left: 0!important; padding-right: 0!important;">
        <h1 class="page-heading"><span class="pink">Recipes </span>Collection</h1>
            <?php if($recipe_categories) { ?>
                <div id="category-slider">
                        <?php foreach($recipe_categories AS $category) { ?>
                            <div class="item">
                                <a href="<?php echo $category['href']; ?>">
                                    <div class="item-image"><img class="img-responsive category-image" src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>"></div>
                                    <div class="cat-hover-text"><h4 class="category-title"><?php echo $category['name']; ?></h4></div>
                                </a>
                            </div>
                        <?php } ?>
                </div>
            <?php } ?>
            
            <!-- <?php if($contest_winner) { ?>
                <h3>Contest Winners</h3>
                <div id="contest-slider">
                    <?php foreach($contest_winner AS $winner) { ?>
                        <div class="winner-item">
                            <a href="<?php echo $winner['href']; ?>">
                                <div class="item-image">
                                    <div class="contest-hover"><div><?php echo $winner['contest_name']; ?></div></div>
                                    <img class="img-responsive winner-image" src="<?php echo $winner['image']; ?>" alt="<?php echo $winner['name']; ?>">
                                    <div class="contest-author"><div><?php echo $winner['author']; ?></div></div>
                                </div>
                               <!--h4 class="winner-title"><?php echo $winner['name']; ?></h4>
                               <h5>- By <?php echo $winner['author']; ?></h5>
                           </a>
                       </div>
                   <?php } ?>
                </div>
            <?php } ?> -->
</div>
    
<div class="container">

  <div class="row" style="margin-top: 25px;"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="filters" class="col-md-3 hidden-xs hidden-sm">
        <div class="panel panel-default recipe-panel">
            <div class="list-group">
                <?php foreach ($filter_groups as $filter_group) { ?>
                    <a class="list-group-item"><?php echo $filter_group['name']; ?></a>
                    <div class="list-group-item">
                        <input class="form-control filter_search" type="text" placeholder="Search">
                        <div id="filter-group<?php echo $filter_group['filter_group_id']; ?>" class="filter-group">
                            <?php foreach ($filter_group['filter'] as $filter) { ?>
                                <?php if($filter['recipe_counts'] > 0) { ?>
                                    <div class="checkbox">
                                        <label>
                                            <?php if (in_array($filter['filter_id'], $filter_recipe)) { ?>
                                                <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" checked="checked" />
                                                <?php echo $filter['name']; ?>
                                            <?php } else { ?>
                                                <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" />
                                                <?php echo $filter['name']; ?>
                                            <?php } ?>
                                        </label>
                                        <span class="recipe_count pull-right"><?php echo $filter['recipe_counts']; ?></span>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-footer text-right">
                <button type="button" class="btn button button-filter"><?php echo $button_filter; ?></button>
            </div>
        </div>
    </div>
    
    <div id="mobile-filter" class="col-xs-9 col-sm-6 hidden-md hidden-xl hidden-lg">
        <div class="filter-close">
            <span class="pull-right"><i class="fa fa-close"></i></span>
        </div>
        <div class="panel panel-default recipe-panel">
            <div class="list-group">
                <!--<?php if($recipe_categories) { ?>
                    <a class="list-group-item">Category</a>
                    <div class="list-group-item" id="cat-filter">
                        <div id="filter-group<?php echo $category_id; ?>">
                            <ul class="cat-list">
                                <?php foreach ($recipe_categories AS $category) { ?>
                                    <?php if($category['category_id'] == $category_id) { ?>
                                        <li class="current_cat"><?php echo $category['name']; ?></li>
                                    <?php } else { ?>
                                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>-->
                <?php foreach ($filter_groups as $filter_group) { ?>
                    <a class="list-group-item"><?php echo $filter_group['name']; ?></a>
                    <div class="list-group-item">
                        <input class="form-control filter_search" type="text" placeholder="Search">
                        <div id="filter-group<?php echo $filter_group['filter_group_id']; ?>" class="filter-group">
                            <?php foreach ($filter_group['filter'] as $filter) { ?>
                                <?php if($filter['recipe_counts'] > 0) { ?>
                                    <div class="checkbox">
                                        <label>
                                            <?php if (in_array($filter['filter_id'], $filter_recipe)) { ?>
                                                <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" checked="checked" />
                                                <?php echo $filter['name']; ?>
                                            <?php } else { ?>
                                                <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" />
                                                <?php echo $filter['name']; ?>
                                            <?php } ?>
                                        </label>
                                        <span class="recipe_count pull-right"><?php echo $filter['recipe_counts']; ?></span>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-footer text-right">
                <button type="button" class="btn button button-filter"><?php echo $button_filter; ?></button>
            </div>
        </div>
    </div>
    <div id="content" class="col-md-9 col-xs-12 col-sm-12"><?php echo $content_top; ?>
     
      <!--<div class="row">
      <div class="col-md-12 col-xs-12">-->
            <!--form class="col-md-12" style="padding: 20px 0;">
                <div class="col-md-offset-3 col-md-6 input-group">
                    <input type="text" name="search" placeholder="<?php echo $place_holder; ?>" value="<?php echo $search_query; ?>" id="input-search" class="form-control" />
                    <span class="input-group-addon">
                        <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary form-control" />
                    </span>
                </div>
            </form-->
            
            
            
            
            <!-- Search Code -->    
            <!--<div class="row">
                <div class="col-sm-4">
                  <input type="text" name="search" placeholder="<?php echo $place_holder; ?>" id="input-search" class="form-control" />
                </div>
               <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />    
              </div>-->
            <!-- Search Code -->
     <!-- </div>
    </div>--> 
    
<?php if (!empty($recipies)) { ?>
 <div class="row">
   <div class="test">
   <?php foreach ($recipies as $recipe) { ?>          
     <div class="col-md-4 col-xs-12 col-sm-6 recipes-grid test1">
        <div class="recipes recipe-container">
            <?php if($recipe['image']) { ?>
                <div class="recipe-image-container"><a href="<?php echo $recipe['href']; ?>"><img src="<?php echo $recipe['thumb']; ?>" title="<?php echo $recipe['title']; ?>" alt="<?php echo $recipe['title']; ?>" /></a></div>
            <?php }  ?>
            <div class="desc-container">
                <h4 class="recipe-name"><a href="<?php echo $recipe['href']; ?>" class="title1"><?php echo strlen($recipe['title']) > 30 ? substr($recipe['title'],0,30) . '...' : $recipe['title']; ?></a></h4>
                <p class="desc"><?php echo substr($recipe['description'],0,150); ?></p>
                <div class="read-more-button">
				    <a href="<?php echo $recipe['href'];?>" class="read-more">Read more</a>
				</div>
				<div class="caption">
				    <div class="col-md-5 col-xs-4 col-sm-4 pl10"><i style="color: #000;" class="fa fa-calendar"></i> <?php echo $recipe['date_added'] ? date('M d', strtotime($recipe['date_added'])) : 'Apr 14'; ?></div>
				    <div class="col-md-2 col-xs-2 col-sm-2 "></div>
				    <div class="col-md-5 col-xs-6 col-sm-6 pr10" style="text-align: right;"><?php if($recipe['prep_time'] > 0) { ?><i class="fa fa-clock-o" style="color: #000;"></i> <?php echo $recipe['prep_time']; ?> min<?php } ?></div>
				</div>
            </div>
        </div>
    </div>
  <?php } ?>
  </div>
 </div>
    
<?php } else { ?>
      <p style="font-size: 15px;"><?php echo $text_empty; ?></p>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <div class="col-md-12 col-xs-12 col-sm-12">
         <center><?php echo $pagination; ?></center>
       </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<script type="text/javascript"><!--
$('#input-search').on('keypress',function(e) {
    if(e.which == 13) {
        $('#button-search').trigger("click");
        return false; 
    }
});

$('#button-search').bind('click', function() {
  url = 'index.php?route=recipe/recipe/search';

  var search = $('#content input[name=\'search\']').prop('value');

  if (search) {
    url += '&search=' + encodeURIComponent(search);
  }
  location = url;
});

--></script>
<script type="text/javascript">

$("#category-slider").owlCarousel({
	autoPlay: true,
	items : 5,
	slideSpeed : 1000,
	navigation : true,
	paginationNumbers : true,
	pagination : false,
	stopOnHover : false,
	itemsDesktop : [1199,3],
	itemsDesktopSmall : [991,3],
	itemsTablet: [700,2],
	itemsMobile : [480,2]
});

$("#contest-slider").owlCarousel({
	autoPlay: true,
	items : 3,
	slideSpeed : 1000,
	navigation : true,
	paginationNumbers : true,
	pagination : false,
	stopOnHover : false,
	itemsDesktop : [1199,3],
	itemsDesktopSmall : [991,3],
	itemsTablet: [700,2],
	itemsMobile : [480,1]
});
/*$(function(){
    var m = new Masonry($('.test').get()[0], {
        //gutter: 0,
        horizontalOrder: true,
        originLeft: true,
        percentPosition: true,
        itemSelector: ".test1"
    });
});*/
$('#filters .button-filter').click(function() {
	filter = [];

	$('#filters input[name^=\'filter\']:checked').each(function(element) {
		filter.push(this.value);
	});

	location = '<?php echo $action; ?>&filter=' + filter.join(',');
});

$('#mobile-filter .button-filter').click(function() {
	filter = [];

	$('#mobile-filter input[name^=\'filter\']:checked').each(function(element) {
		filter.push(this.value);
	});

	location = '<?php echo $action; ?>&filter=' + filter.join(',');
});

$(document).ready(function() {
    $(".filter_search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        var checkboxes = $(this).next().attr("id");
        $("#" + checkboxes + " .checkbox").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
$('#show-filter').click(function() {
    if ($(this).hasClass('opened')) {
        $(this).removeClass('opened');
        $("#mobile-filter").removeClass('opened');
    } else {
        $(this).addClass('opened');
        $("#mobile-filter").addClass('opened');
    }
});
$('.filter-close span').click(function() {
    if($('#show-filter').hasClass('opened')) {
        $('#show-filter').removeClass('opened');
        $("#mobile-filter").removeClass('opened');
    }
});
</script>
<?php echo $footer; ?>