<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($thumb || $description) { ?>
      
          <div class="row">
            <?php if ($thumb) { ?>
            <div class="col-md-12"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
            <?php } ?>
            <?php if ($description) { ?>
            <!-- <p><?php echo $description; ?></p> !-->
            <?php } ?>
          </div>
     <br>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
       
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
     <?php if ($articles) { ?>
    
     <div class="row">
        <div class="col-sm-12 col-md-12 ">
             <?php foreach ($articles as $article) { ?>
           
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
               <a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['original']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" style="height: 250px; width:100%"/></a>
                
                <div class="caption">
                    <h3><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h3>
                    <hr>
                    <p><?php echo $article['preview']; ?></p><hr>
                    <p><a href="<?php echo $article['href']; ?>" class="btn btn-primary" role="button">View</a></p>

                    <?php if ($article['attributes']) { ?>
                      <h5><?php echo $text_attributes;?></h5>
                      <?php foreach ($article['attributes'] as $attribute_group) { ?>
                        <?php foreach ($attribute_group['attribute'] as $attribute_item) { ?>
                            <b><?php echo $attribute_item['name'];?>:</b> <?php echo $attribute_item['text'];?><br />
                        <?php } ?>
                      <?php } ?>
                    <?php } ?>
                </div>
              </div>
            </div>
            
            
            <?php } ?>
          </div>
      </div>
      
      
      
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$articles) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>