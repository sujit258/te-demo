<?php echo $header; ?>
<!--Google Login Script-->
<script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      auth2 = gapi.auth2.init({
        client_id: '688467726932-ftoln2tngeqru6ahv73sqh2rlg1aeq8k.apps.googleusercontent.com'
      });
      attachSignin(document.getElementById('customBtn1'));
    });
  };

  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
              var profile = googleUser.getBasicProfile();
          var id = profile.getId(); // Do not send to your backend! Use an ID token instead.
          var name = profile.getName();
          var first_name = profile.getGivenName();
          var last_name = profile.getFamilyName();
          var imageUrl = profile.getImageUrl();
          var email = profile.getEmail(); // This is null if the 'email' scope is not present.
          document.getElementById('name').innerHTML = 'Please Wait...!';
           googleUser.disconnect();
          $.ajax({
            url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
            data : 'email_address='+email+'&fname='+first_name+'&lname='+last_name+'&login_via=google',
            type : 'post',
            success: function(result) {
                if(result == 'success'){
                    window.location.reload();
                    //location.href = "/index.php?route=account/account";
                } else if(result == 'registered'){
                   window.location.reload();
                   //location.href = "/index.php?route=account/edit/savedetails";
                } else{
                    $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
                }
                
            }
          });
        }, function(error) {
            $('<div class="alert alert-danger">There was a problem with the authorisation or user may have closed the popup.</div>').insertBefore($('div.error'));
        });
  }
  </script>
<!--Facebook Login Script-->
  <script>

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '587428728308321',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v3.0' // use graph api version 3.1
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.async=true;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
  // Facebook login with JavaScript SDK
function xfbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {		
            getFbUserData();
        } else {
        	autherror();
        }
    }, {scope: 'email'});
}

  function testAPI() { 
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function(response) {
      if(response.email == 'undefined')
          $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please check your email id associated with your facebook account.</div>').insertBefore($('div.customAlert'));
      else{
        document.getElementById('name').innerHTML ='Please Wait...!';

        $.ajax({
            url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
            data : 'email_address='+response.email+'&fname='+response.first_name+'&lname='+response.last_name+'&login_via=facebook',
            type : 'post',
            success: function(result) {
                if(result == 'success'){
                    window.location.reload();
                } else if(result == 'registered'){
                    window.location.reload();
                } else{
                    $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
                }
                
            }
        });
      }
    });
  }
</script>

<!--Linkedin Login Script-->
<script type="text/javascript">
  function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);
        
    }
    
    function getProfileData() {
        IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(onSuccess).error(onError);
    }

function onSuccess(data){
var user = data.values[0];
        var first_name = user.firstName;
        var last_name = user.lastName;
        var email = user.emailAddress;
        document.getElementById('name').innerHTML = 'Please Wait...!';
        $.ajax({
            url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
            data : 'email_address='+email+'&fname='+first_name+'&lname='+last_name+'&login_via=linkedin',
            type : 'post',
            success: function(result) {
                if(result == 'success'){
                    window.location.reload();
                } else if(result == 'registered'){
                    window.location.reload();
                } else{
                    $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
                }     
            }
        });

    }

    function onError(error) {
        console.log(error);
    }

</script>

<style>
    .login-button-container {
        margin: 10px auto;
        display: table;
        table-layout: fixed;
        width: 100%;
    }
    .login-button-container .social-container {
        min-width: 133px;
    }
    
    .login-button-container .social-container .social-container-box {
        border: 1px solid #bfc0c6;
        background-color: #fff;
        border-radius: 5px;
        text-align: left;
        padding: 14px 14px 0 0;
        height: 50px;
        cursor: pointer;
        max-width: 150px;
        margin: 0 auto;
    }
    .login-button {
        display: block;
        margin: 0 auto;
        width: 100%;
        max-width: 186px;
        padding-left: 50px;
        position: relative;
        font-weight: 500;
    }
    .login-button-container .login-gplus-logo {
        background-position: -298px 0 !important;
        width: 23px;
        top: -2px;
    }
    .login-button-container .login-fb-logo, .login-button-container .login-gplus-logo, .login-button-container .login-linkedin-logo {
        height: 29px;
        position: absolute;
        left: 15px;
    }
    .login-button-container .header-sprite {
        background: url(https://www.true-elements.com/catalog/view/javascript/xtensions/image/social.png) no-repeat 0 0;
            background-position-x: 0px;
            background-position-y: 0px;
            background-size: auto;
        background-size: 336px 48px;
    }
    .group {
        position: relative;
        margin-bottom: 15px;
    }
    .group .inputMaterial {
        font-size: 13px;
        display: block;
        width: 100%;
        height: 42px;
        box-shadow: none;
        -webkit-box-shadow: none;
        color: #333333;
        padding: 16px 15px 4px;
        border-radius: 4px;
        border: 1px solid  #dddddd;
    }
    
    .group label {
        color: #cecbe2;
        font-size: 12px;
        font-weight: 600;
        position: absolute;
        pointer-events: none;
        left: 5px;
        top: 12px;
        transition: 0.2s ease all;
        -moz-transition: 0.2s ease all;
        -webkit-transition: 0.2s ease all;
        z-index: 22;
        padding: 0px 10px;
    }
    .group.focus label {
      top: -8px !important;
      background: #ffffff;
    }
    .group.focus label span.required:after {
      display: inline-block;
    }
    .group.focus select.inputMaterial {
      padding-top: 18px;
    }
    .group.focus label {
      color: #96858d;
    }
    *:focus,
    a:focus {
      outline: none;
    }
    .group.filled label {
        top: -8px !important;
        background: #fff;
        color: #96858d;
    }

    .loginform{
        width: 83%;
        padding-left: 40px;
    }
   .modal-dialog {
        top: 200px;
        width: 23%;
    }
    
    input[type="text"].form-control,input[type="password"].form-control{
            width: 50%;
        }
         input[type="text"].form-text{
            width: 100%;
        }
        
       .login-info-text{
            margin-left: -47%;
        } 
        
        .social-container{
            margin-left: -47%;
        }
        .login-heading{
            margin-left: 8%;
        }
    @media only screen and (max-width: 600px) {

        .modal-dialog {
            width: 95%;
        }  
        .social-container{
            margin-left: -3%;
        }
        
        .login-heading{
            margin-left: 19%;
        }
        input[type="text"].form-control,input[type="password"].form-control{
            width: 100%;
        }
        .login-info-text{
            margin-left:-4px;
        }
        
    }    
	@import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css");
	.panel-title > a:before {
		float: right !important;
		font-family: FontAwesome;
		content:"\f068";
		padding-right: 5px;
	}
	.panel-title > a.collapsed:before {
		float: right !important;
		content:"\f067";
	}
	.panel-title > a:hover, 
	.panel-title > a:active, 
	.panel-title > a:focus  {
		text-decoration:none;
	}
	
	.form-group input {
	    border: 1px solid #f2f2f2!important;
	}	
        .container-fluid {
        background: #fafbfd;
    }
    h3 {
        color: #000;
        font-size: 17px;
        font-weight: 700;
    }
    
    .form-control {
        color: #000!important;
        background: #fff;
        border: 1px solid #f2f2f2;
        border-radius: 3px!important;
        padding: 20px 12px!important;
    }
    .form-control:focus {
        border-color: #80bdff!important;
        outline: 0!important;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25)!important;
    }
    .button {
        background: #f49a25;
        border-radius: 4px!important;
        height: 42px!important;
        width: 100px!important;
        font-weight: 600!important;
        font-size: 16px!important;
    }
    .button:hover {
        background: #000;
        transition: .5s ease-in;
        -webkit-transition: .5s ease-in;
        -moz-transition: .5s ease-in;
        -o-transition: .5s ease-in;
        -ms-transition: .5s ease-in;
    }
    .panel-body {
        border: none!important;
    }
    .form-group {
        margin-top: 20px!important;
    }
    .fa-exclamation-circle {
        margin-right: 10px;
    }
    .alert {
        font-size: 15px!important;
    }
    .alert-danger {
        color: #a94442!important;
    }
    .alert-success {
        color: #a94442!important;
    }
    .panel-title {
        text-transform: none!important;
        font-size: 15px!important;
    }
    h4 {
        font-size:15px!important;
        font-weight: 600!important;
        line-height: 20px!important;
    }
    .panel-body p, #form-partner p {
        text-align: justify!important;
    }
    @media screen and (max-width: 800px) {
        .breadcrumb {
            margin-bottom: 15px!important;
        }
    }
    .breadcrumb {
        margin-bottom: 20px!important;
    }
    .top_banner img {
        cursor: pointer;
        width: 100%!important;
    }
    .footer-breaking {
        margin: 0px!important;
    }
    button[disabled] {
        cursor: not-allowed!important;
    }
    .btn-oos {
        background: #777!important;
    }
    .panel-heading {
        background: #fff!important;
    }
    .text-danger {
        font-size: 13px !important;
    }
    .pink{
        color: #feac00;
    }
    .page-heading {
        font-size: 33px !important;
    }
</style>

    <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
     <?php if (!$logged) { ?>
  
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
         <div class="error"> </div>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">X</button>
          <span class="modal-title"><p class="login-heading"><strong>LOGIN USING SOCIAL ACCOUNT</strong></p></span>
        </div>
        <div class="modal-body">
           
        <div class="container login-container">
            <div class="row">
                <div class="col-md-6 login-form-1">
                    
                    <div class="login-button-container">
                        <div class="social-container">
                        <div class="social-container-box" id="customBtn1">
                        <span class="login-google login-button"><span class="header-sprite login-gplus-logo"></span>GOOGLE</a>
                        </div>
                        </div>
                    </div>
                    <p class="login-info-text text-center hide_guest">- OR USING EMAIL -</p>
             
                    <form action="index.php?route=recycles/recycles" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" placeholder="Your Email *" value=""/>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Your Password *" value="" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" value="Login" />
                        </div>
                        
                <div id="name"></div> 
                    </form>
                </div>
            </div>
        </div>
	
        </div>
      
    </div>

  </div>
</div>
<?php } ?> 
<div class="top_banner">
  <img alt="True Elements" class="hidden-xs hidden-sm" src="/image/catalog/images-08-19/banner/recycle-banner-desktop.jpg"/>
  <img alt="True Elements" class="hidden-md hidden-xl hidden-lg" src="/image/catalog/images-08-19/banner/recycle-banner-mobile.jpg"/>
</div>
<div class="container-fluid">
<div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    <br/><br/>
	<div class="container">
	<div class="row">	
	<div class="col-md-8">
	<h1 class="page-heading"><span class="pink">Recycle</span>, and be Rewarded</h1>
	
		
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="font-size: 15px;">Our commitment towards saving the Mother Earth:</a>
		</h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
		<p style="text-align:justify; font-size:15px;" >
		     As part of our strong commitment towards being True to Nature, and True to You, we strongly believe to make the planet Earth, a safer, cleaner and beautiful place to live in. We are in an attempt to recycle all of our used packets, and we would love to include YOU in the process! It is our belief that we, as humans, are equally important to be responsible enough and do our part in keeping our Mother Earth, colourful and clean.
			</p>
			</div>
        </div>
    </div>
	
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
    <h4 class="panel-title">
    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"style="font-size: 15px;">How it works:</a>
    </h4>
	</div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
    <div class="panel-body">
	        <p style="text-align:justify; font-size:15px;">Participating in this program is completely free and very easy.<br/>
	        1. Send back to us, 10 empty & clean True Elements product packets in a box for recycling.<br/>
	        2. You will be gifted with super-beneficial True Cash, redeemable on your next purchase.</p>
	<br>
	<h4>Come, let us join our hands together and save our Mother Earth.</h4>
		</div>
		</div>
	   </div>
	</div>

	<br>			
	</div>
		
	<?php if ($customer) { ?>
		<div class="col-md-4 col-sm-12 col-xs-12">
            <div class="jumbotron" style="background-color:#ffffff;padding: 20px;">
			   <form id="form-recycles">			  
                <h3>Fill This Form</h3>
                 <div class="form-group">
                     <label><?php echo $tag_name; ?></label>
                <input type="text" class="form-control form-text" value ="<?php echo $cst_details['firstname']," ",$cst_details['lastname']; ?>"name="cst_name">
                </div>
                
                 <div class="form-group">
                     <label><?php echo $tag_email; ?></label>
                      <input type="text" class="form-control form-text" value ="<?php echo $cst_details['email']; ?>"name="cst_email">
                      <div id="validate_email"></div>
                </div>
                
                <div class="form-group">
                    <label><?php echo $tag_mobile; ?></label>
                      <input type="text" class="form-control form-text" value ="<?php echo $cst_details['telephone']; ?>"name="cst_mobile">
                      <div id="validate_mobile"></div>
                </div>
                
				  <div class="form-group">
				      <?php $company = $cst_details['company'] ? $cst_details['company'] . ", " : ''; ?>
				      <label><?php echo $tag_address; ?></label>
                      <input type="text" class="form-control form-text" value ="<?php echo $company,$cst_details['address_1'],", ",$cst_details['address_2'],", ",$cst_details['city'],", ",$cst_details['postcode']; ?>"name="cst_address">
                      <div id="validate_mobile"></div>
                </div>
                
                 <div class="form-group">
                     <label><?php echo $entry_no_of_packets; ?></label>
                      <input type="text" class="form-control form-text" placeholder="<?php echo $entry_no_of_packets; ?>" name="cst_no_of_packets">
                      <div id="validate_nop"></div>
                </div>
                
                <div class="form-group">
                    <label><?php echo $entry_pickup_date; ?></label>
                    <input type="text" class="form-control form-text date" placeholder="yyyy-mm-dd" name="pickup_date">
                    <div id="validate_pickup_date"></div>
                </div>
                
                <div class="form-group">
                    <label for="pickup_slot"><?php echo $entry_pickup_slot; ?></label>
                    <select name="pickup_slot" class="form-control form-text" id="pickup_slot" style="padding: 5px !important;">
                        <option value="1" selected>9AM - 1PM</option>
                        <option value="2">1PM - 4PM</option>
                        <option value="3">4PM - 7PM</option>
                    </select>
                </div>
                		 
				 <div class="buttons clearfix">                
                 <button type="button" id="button-send" class="button">Submit</button>										
				 </div>
				</div>
				<div id="success"></div>
				<div id="not_success"></div>
				 
			</form>
			</div>
		    </div>	
			<?php } else { ?>
			<div class="col-md-4 col-sm-12 col-xs-12">
            <div class="caption" >
			   <div class="jumbotron" style = "background-color:#ffffff;padding: 20px;">
			   <form id="form-recycles">
				 <h3>Please <a href="#myModal" class="pink" role="button" data-toggle="modal">Login</a>  To Fill Form</h3>
				 
				 <div class="form-group">
				 <label><?php echo $tag_name; ?></label>
                 <input type="text" name="cst_name" class="form-control form-text" disabled/>
                 </div>
                 
                 <div class="form-group">
                 <label><?php echo $tag_email; ?></label>
				 <input type="text" name="cst_email" class="form-control form-text" disabled/><div id="validate_email"></div>
				 </div>
				 
				 <div class="form-group">
				 <label><?php echo $tag_mobile; ?></label>
				 <input type="text" name="cst_mobile" class="form-control form-text" disabled/><div id="validate_mobile"></div>
				 </div>
				 
				 <div class="form-group">
				 <label><?php echo $tag_address; ?></label>
				 <input type="text" name="cst_address" class="form-control form-text" disabled/>
				 </div>
				 
				 <div class="form-group">
				 <label><?php echo $entry_no_of_packets; ?></label>
				 <input type="text" name="cst_no_of_packets" class="form-control form-text" min="0" disabled/>
				 </div>
				 
				 <div class="form-group">
                    <label><?php echo $entry_pickup_date; ?></label>
                    <input type="text" class="form-control form-text" name="pickup_date" disabled>
                </div>
                
                <div class="form-group">
                    <label><?php echo $entry_pickup_slot; ?></label>
                    <select name="pickup_slot" class="form-control form-text" style="padding: 5px !important;" disabled>
                        <option value="1" selected>9AM - 1PM</option>
                        <option value="2">1PM - 4PM</option>
                        <option value="3">4PM - 7PM</option>
                    </select>
                </div>
				 
				 <div class="buttons clearfix">
                 <button type="button" class="button btn-oos" disabled/><?php echo $button_send; ?></button>			
				 </div>
				<div id="success"></div>
				<div id="not_success"></div>
				 </div>
			</form>
			</div>
		    </div>	
			<?php } ?>
	</div>		   
	</div>
	</div></div><hr>	
    <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
             
<script>startApp();</script>

<script type="text/javascript">
 $(window).load(function(){        
   $('#myModal').modal('show');
    });
    </script>
<script type="text/javascript">


$('#button-send').on('click', function() {
	$.ajax({
		url: 'index.php?route=recycles/recycles/write',
		type: 'post',
		dataType: 'json',
		data: $("#form-recycles").serialize(),
		complete: function() {
			$('#button-send').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#success').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['validate_email']) {
				$('#validate_email').html('<div class="text-danger">' + json['validate_email'] + '</div>');
			}
			
			if (json['validate_mobile']) {
				$('#validate_mobile').html('<div class="text-danger">' + json['validate_mobile'] + '</div>');
			}
			
			if (json['validate_nop']) {
				$('#validate_nop').html('<div class="text-danger">' + json['validate_nop'] + '</div>');
			}
			
			if (json['validate_pickup_date']) {
				$('#validate_pickup_date').html('<div class="text-danger">' + json['validate_pickup_date'] + '</div>');
			}
			
			if (json['not_success']) {
				$('#not_success').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['not_success'] + '</div>');
			}	

			if (json['success']) {
				$('#success').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				$('input[name=\'cst_name\']').val('');
				$('input[name=\'cst_email\']').val('');
				$('input[name=\'cst_mobile\']').val('');
				$('input[name=\'cst_address\']').val('');
				$('input[name=\'cst_no_of_packets\']').val('');
				$('input[name=\'validate_pickup_date\']').val('');
				
                $('#validate_email').html('');
                $('#validate_mobile').html('');
                $('#validate_nop').html('');
                $('#validate_pickup_date').html('');
                $('#not_success').html('');
			}
		}
	});
});

</script>

<script>
$(document).ready(function(){
    $("#myModal").modal({keyboard: true});
  
});
</script>

<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 5000); // Change image every 2 seconds
}
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd', minDate: 1});
});
</script> 
<?php echo $footer; ?>