<?php echo $header; ?>
<style>
body {
    font-size: 15px;
}
.product-banner, .product-1, .product-2 {
    border: 2px solid #d5dbdb;
    background: rgba(135, 149, 150, 0.095);
}
.product-1, .product-2 {
    margin-top: 0px!important;
    min-height: 375px;
    max-height: 375px;
    height: 100%;
}
.product-text {
    height: 100%;
    min-height: 375px;
    padding-right: 15px;
}
.product-banner, .product-data {
    min-height: 750px!important;
}
.product-banner {
    background-repeat: no-repeat;
    background-size: 101%, 100%;
}
.overlay {
    min-height: 190px;
    background: rgba(236, 238, 239, 1);
    padding: 15px;
    margin: 15px;
    width: 49%;
    color: #002f36;
}
body h4 {
    font-weight: 500;
    font-size: 17px;
    line-height: 22px;
    color: #002f36;
}
body h1 {
    font-weight: 700;
    color: #002f36;
    font-size: 25px;
}
.product-image img {
    margin-top: 10%;
}
.product-image {
    min-height: 375px;
    padding-left: 0px;
}
.top-banner {
    margin-top: 0!important;
    margin-bottom: 25px!important;
}
.top-banner img {
    width: 100%;
}
.breadcrumb {
    margin-bottom: 20px!important;
}
.top-image img {
    width: 100%;
}
.carousel-control span {
    font-size: 30px;
}
.top-banners {
    margin-bottom: 25px;
}
.content-vertical-center {
    margin-top: 15%;
    margin-left: 15px;
}
.buttons .buttons .buttons .buttons .btn-block {
    padding: 10px 16px!important;
}
.buttons .btn-block:active, .buttons .btn-block:focus {
    background: #f49a25!important;
}
@media(max-width:1000px) {
    .product-banner {
        min-height: 330px!important;
        background-size: 100%, 100%!important;
    }
    body h4 {
        font-size: 15px!important;
    }
    body h1 {
        font-size: 17px!important;
    }
    .overlay {
        min-height: 160px;
        width: 70%;
    }
    .product-image img {
        margin-top: 30%;
    }
    .product-image {
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .product-1, .product-2 {
        max-height: 320px;
        min-height: 320px;
    }
    .product-data {
        min-height: 1px !important;
    }
    .top-banner img {
        height: 140px!important;
    }
    .product-text, .product-image {
        min-height: 270px;
    }
    .buttons .buttons .buttons .buttons .btn-block {
        width: 35%!important;
        padding: 7px 16px;
    }
    .content-vertical-center {
        margin-top: 0!important;
        margin-left: 0!important;
    }
    #cart #cart-total {
        top: -5px;
    }
}
#cart #cart-total {
    top: -6px;
}
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container-fluid">
    <div class="row top-banner hidden-xs hidden-sm">
        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-top-banner.jpg" class="img-responsive" />
    </div>
    <div class="row top-banner hidden-md hidden-xl hidden-lg">
        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-top-banner-mobile.jpg" class="img-responsive" />
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <a href="<?php echo $raw_seeds; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section1.jpg');">
                </div>
            </a>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Raw Flax Seeds, 500gm</h1>
                            <h4>Flax seeds, also known as Alsi, are small, shiny and flat in appearance that can be added to any of your foods. These are a tiny storehouse of vital nutrients.</h4>
                            <h1>Rs 194 <small><strike>Rs 200</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $raw_flax; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section1-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $raw_flax; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section1-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Raw Chia Seeds, 500gm</h1>
                            <h4>Chia seeds are richest source of calcium which can help in improving the mineral bone density and strengthens the muscles.</h4>
                            <h1>Rs 441 <small><strike>Rs 450</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $raw_chia; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $raw_chia; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Dried Cranberries (Whole), 125 gm</h1>
                            <h4>Dried Whole Cranberries are naturally sun-dried, with no addition of chemical preservatives, flavours or colours.</h4>
                            <h1>Rs 166 <small><strike>Rs 175</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $cranberries; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section2-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $cranberries; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section2-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Dried Blueberries, 125gm</h1>
                            <h4>Blueberries possesses loads of benefits since it is a proud member of the superfood family. It is especially beneficial for fitness freaks.</h4>
                            <h1>Rs 356 <small><strike>Rs 375</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $blueberries; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $blueberries; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
            <a href="<?php echo $cranberries; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section2.jpg');">
                </div>
            </a>
        </div>
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <a href="<?php echo $raw_seeds; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section3.jpg');">
                </div>
            </a>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Raw Pumpkin Seeds, 250gm</h1>
                            <h4>AAA graded premium quality seeds, with no nutritional compromise in it. In just a 100g serving, seeds are dense in calories and an excellent source of protein.</h4>
                            <h1>Rs 290 <small><strike>Rs 300</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $raw_pumpkin; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section3-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $raw_pumpkin; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-sb-section3-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Raw Sunflower Seeds, 250gm</h1>
                            <h4>Raw Sunflower Seeds are highly nutritious food, beneficial for the entire body. It is an all-rounder food, which maintains the quality of skin, improves health and nourishes hair.</h4>
                            <h1>Rs 169 <small><strike>Rs 175</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $raw_sunflower; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $raw_sunflower; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>