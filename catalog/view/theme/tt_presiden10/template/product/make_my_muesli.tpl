<?php echo $header; ?>
<style>
div #sticky {
    position: -webkit-sticky;
    position: sticky;
    top: 105px;
    margin-top: 107px;
}
.checkbox input {
    display: none;
}
.selection{
    font-weight: bold;
    color: #f49a25;
}
#product .control-label {
    font-size: 18px;
    font-weight: 600;
    color: #f49a25;
}
body {
    font-family: roboto, 'Oswald', sans-serif;
    font-size: 15px!important;
}

h6 {
    margin-bottom: 0px !important;
    margin-left: 15px !important;
    margin-top: 25px !important;
    line-height: 1.3 !important;
    padding-bottom: 10px !important;
}
#product h3 {
    text-align: center;
    color: #f49a25;
}

.checkbox label {
    padding: 0px!important;
    display: inline-block;
    position: relative;
    cursor: pointer;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-align: center;
    font-size: 15px;
    color: #f49a25;
    width:auto;
    min-height:270px!important;
}

label img {
    min-height: 100%!important;
    width: auto;
    transition-duration: 0.2s;
    transform-origin: 50% 50%;
    position: relative;
}

:checked + label img {
    transform: scale(0.9);
    z-index: -1;
    position:relative;
}

:checked + label {
    background-image:linear-gradient(rgba(0, 0, 0, 0.7), transparent);
}

.checkbox {
    display: inline-block;
    text-align: center;
    min-height: 270px;
    border: 1px solid rgb(28,0,85);
    margin: 30px 10px 30px!important;
    padding-right: 0;
    padding-left: 0;
    border-radius:10px;
    overflow: hidden;
    width: 30%;
}

@media screen and (max-width: 800px) {
    .googleplay-original {
        margin-top: 10px;
    }
    .checkbox {
        width: 44% !important;   
    }
    .priceOnMobile {
        display: inline-block!important;
        z-index: 1;
        background:rgba(255,255,255,0.9);
        width: 100%;
        position: fixed;
        top: 0px;
    }
    .main-price-mobile {
        position: relative;
        text-align: center;
    }
    .banner-container {
        margin-right: -15px;
        margin-left: -15px;
    }
    div #sticky {
        margin-top: 0px;
    }
}
.option_name {
    font-size: 15px;
    font-weight: 500;
}

.nutritional_value {
    font-size: 15px;
    color: #000000;
}

#product {
    max-height: 100%;
    margin-bottom: 0px;
}

.has-option {
    border: none;
    padding: 0px;
    box-shadow: none;
}

.option_help {
    color:#000;
    font-size:15px;
    text-align:center;
}

#top_banner {
    padding-right: 0;
    padding-left: 0;
}

p {
    font-size: 15px;
    margin: 0;
}

.product-title-section {
    background-color: #A193A4;
    margin-top: 10px;
}

.product-title-subsection {
    background-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0),rgba(0, 0, 0, 0.5));
    padding: 45px 20px;
}

.product-title{
    font-size: 28px!important;
    margin-bottom: 0px!important;
    margin-top: 0px!important;
    color: #ffffff!important;
    text-transform: none!important;
}

.selected-options h3 {
    font-size: 17px!important;
    font-weight: 600;
    text-align:left!important;
    margin-top: 5px!important;
    margin-bottom: 5px!important;
    color:#000;
}

.selected-options div {
    padding-left: 0px;
}

.selected-options {
    padding-top:10px!important;
}

#minus {
    border-top-left-radius: 25px!important;
    border-bottom-left-radius: 25px!important;
    border-right:none!important;
    font-size: 15px;
}

#plus {
    border-top-right-radius: 25px!important;
    border-bottom-right-radius: 25px!important;
    border-left:none!important;
    font-size: 15px;
}

#input-quantity {
    padding-left: 5px!important;
    padding-right: 5px!important;
    border-left:none!important;
    border-right:none!important;
    width: 90px!important;
}

#minus:hover, #plus:hover {
    color: #f49a25!important;
    background: #fff!important;
}

#minus,#plus,#input-quantity {
    height:50px!important;
}

.qty-product label {
    margin-top: 15px!important;
}

.qty-product-content {
    position: relative;
    padding-top: 15px;
    width:80%;
    margin:auto;
    float:right;
}

.btn-block {
    border-radius: 25px;
    background: #f49a25;
    width: 100%;
}
button[disabled] {
    cursor: not-allowed!important;
}
.btn-oos {
    border-radius: 25px;
    background: #777!important;
    width: 100%;
    margin-bottom: 7px;
}

.text-total {
    font-size: 18px!important;
    color: #f49a25 !important;
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    font-weight: 800;
    display: table-cell;
}

.main-price {
    font-size: 40px!important;
    color: #f49a25 !important;
    display: table-cell;
}

.packaging-checkbox {
    font-size: 17px !important;
    font-weight: 600 !important;
    text-align: left !important;
}

.packaging {
    margin-right: 10px!important;
    margin-left: -10px!important;
}

.price-section {
    min-height: 60px;
    line-height: 0px!important;
}

.button:hover {
    background: #000;
}
.button {
    font-size: 16px!important;
    padding: 10px 16px!important;
    font-weight: 600!important;
}

.disabled {
    background-image : linear-gradient(to bottom, rgb(241,241,241), transparent);
    cursor : no-drop;
}

p.tip {
    text-decoration: none;
    display:none;
    padding: 1px;
}

.checkbox:hover p.tip {
    display: block;
    border-radius: 6px;
    background-color: gold;
    position: absolute;
    top: 0px;
    color: #000;
    text-decoration: none;
    z-index:2;
    left:0;
}

/*:checked + label:hover p.tip, .disabled:hover p.tip {
    display: none;
}*/

.popuptext {
    position: relative;
    text-align: center;
    visibility: hidden;
    background: #f44336;
    color: #fff;
    padding: 5px 15px;
    width: 250px;
    left: calc( 50% - 125px);
    border-radius: 5px;
    cursor: pointer;
    -webkit-animation: updown 1.4s ease-in-out infinite;
    -moz-animation: updown 1.4s ease-in-out infinite;
    -o-animation: updown 1.4s ease-in-out infinite;
    animation: updown 1.4s ease-in-out infinite;
    top: 10px;
}

/* Popup arrow */
.popuptext::after {
    content: '';
    display: block;
    position: absolute;
    left: calc( 50% - 7px );
    width: 0px;
    text-align: center;
    height: 0;
    border-top: 14px solid #F44336;
    border-right: 14px solid transparent;
    border-bottom: 0 solid transparent;
    border-left: 14px solid transparent;
}

/* Toggle this class - hide and show the popup */
.show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}

#selectedBaseContainer, #selectedSweetenerContainer, #selectedEdfContainer, #selectedSanContainer {
    cursor:pointer;
}

.priceOnMobile {
    display: none;
    padding: 10px;
}

.main-price-mobile {
    font-size: 15px!important;
    color: #f49a25 !important;
}
.banner-container {
    margin: 0!important;
}
#top_banner img {
    width: 100vw!important;
    cursor: pointer;
}
#button-cart::before {
    font-family: 'Material-Design-Iconic-Font';
    display: inline-block;
    font-size: 16px;
    content: "\f1cb";
    margin-right: 10px;
    font-weight: 400;
}
.nav-pills li {
        text-align: center;
        margin-top: 10px;
    }
    .nav-pills > li + li {
        margin-left: 0!important;
    }
    .nav a {
        font-weight: 600;
    }
    .nav a span {
        vertical-align: middle;
    }
    .nav li a {
        padding: 15px 10px;
        font-size: 18px;
    }
.nav-pills li.active a {
        color: #fff!important;
        background-color: #f49a25!important;
    }
    .nav-pills li a {
        color: #000;
        border: 1px solid #f49a25;
        background: #fff;
    }
    .select-types {
        margin-top: 5px;
        margin-bottom: 15px;
    }
</style>
<div class="row banner-container col-md-12 col-sm-12 col-xs-12" style="padding-top: 2%;">
    <img class="p0 img-responsive hidden-xs hidden-sm" src="image/catalog/Banner/mms-mmm-desktop.jpg">
    <img class="p0 img-responsive hidden-md hidden-lg hidden-xl" src="image/catalog/Banner/mms-mmm-mobile.jpg">
</div>
<div class="col-md-12 col-sm-12 col-xs-12 select-types" role="tabpanel">
    <ul class="nav nav-pills nav-fill" role="tablist">
        <li class="nav col-md-4 col-sm-1 col-xs-1"></li>
        <li role="presentation" class="nav active col-md-2 col-sm-5 col-xs-5">
            <a style="cursor:pointer"><span>Make My Muesli</span></a>
        </li>
        <li role="presentation" class="nav col-md-2 col-sm-5 col-xs-5">
            <a href="/index.php?route=product/make_my_snack" ><span>Make My Snack</span></a>
        </li>
        <li class="nav col-md-4 col-sm-1 col-xs-1"></li> 
    </ul>
</div>
<div class="row banner-container">
      <div class="col-md-12 col-sm-12 col-xs-12" id="top_banner">
          <img alt="True Elements" class="img-responsive hidden-xs hidden-sm" src="<?php echo $top_banner_desktop; ?>" />
          <img alt="True Elements" class="img-responsive hidden-md hidden-xl hidden-lg" src="<?php echo $top_banner_mobile; ?>" />
      </div>
</div>
<div class="container">
  <div class="row">
    <div id="content" class="col-md-12 col-sm-12 col-xs-12">
      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center; padding-top:10px; padding-bottom:10px;"><strong>Note:</strong><span> As this is customized product,your muesli will be dispatched in 48 hours.</span></div>
      </div>
      <div class="row">
		<div id="product" class="col-md-8 col-sm-12 col-xs-12">
		    <div class="priceOnMobile">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <p class="main-price-mobile">Cart Value: <?php echo $price; ?></p> 
                </div>
            </div>
            <?php if ($options) { ?>
            <?php foreach ($options as $option) { ?>
                        <?php if ($option['type'] == 'checkbox' || $option['type'] == 'select') { ?>
            <?php if ($option['option_id'] == 13) { ?>
            <div class="form-group" id="base-content">
              <h3 class="control-label col-md-12 center"><?php echo $option['name']; ?></h3><p class="option_help">(77.5% By Weight) (Select Any <span class="selection">One</span>)</p>
              <span class="popuptext" id="basePopup">Please Select Atleast 1!</span>
              <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox col-md-3 col-sm-5 col-xs-5">
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="base" disabled/>
                  <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="base-image"/><br/><p class="option_name"><?php echo $option_value['name']; ?></p><p class="nutritional_value"><?php echo $option_value['nutritional_value']; ?></p></label>
                  
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['option_id'] == 14) { ?>
            <div class="form-group" id="sweetener-content">
              <h3 class="control-label col-md-12 center"><?php echo $option['name']; ?></h3><p class="option_help">(3.5% By Weight) (Select Any <span class="selection">One</span>)</p>
              <span class="popuptext" id="sweetenerPopup">Please Select Atleast 1!</span>
              <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox col-md-3 col-sm-5 col-xs-5">
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="sweetener" disabled/>
                  <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="sweetener-image" /><br/><p class="option_name"><?php echo $option_value['name']; ?></p><p class="nutritional_value"><?php echo $option_value['nutritional_value']; ?></p></label>
                  
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['option_id'] == 15) { ?>
            <div class="form-group" id="edf-content">
              <h3 class="control-label col-md-12 center"><?php echo $option['name']; ?></h3><p class="option_help">(9% By Weight) (Select Any <span class="selection">Four</span>)</p>
              <span class="popuptext" id="edfPopup">Please Select Atleast 4!</span>
              <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox col-md-3 col-sm-5 col-xs-5">
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="edf" disabled/>
                  <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="edf-image" /><br/><p class="option_name"><?php echo $option_value['name']; ?></p><p class="nutritional_value"><?php echo $option_value['nutritional_value']; ?></p></label>
                  
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['option_id'] == 16) { ?>
            <div class="form-group" id="san-content">
              <h3 class="control-label col-md-12 center"><?php echo $option['name']; ?></h3><p class="option_help">(10% By Weight) (Select Any <span class="selection">Two</span>)</p>
              <span class="popuptext" id="sanPopup">Please Select Atleast 2!</span>
              <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox col-md-3 col-sm-5 col-xs-5">
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="san" disabled/>
                  <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="san-image" /><br/><p class="option_name"><?php echo $option_value['name']; ?></p><p class="nutritional_value"><?php echo $option_value['nutritional_value']; ?></p></label>
                  
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
          
          <!--Main Section-->
          <div class="col-md-4 col-sm-12 col-xs-12" id="sticky">
              <div class="product-title-section">
                <div class="product-title-subsection">
			        <h2 class="product-title"><?php echo $heading_title,", 400gm"; ?></h2>
			    </div>
			  </div>
			  <div class="selected-options">
			      <?php if ($options) { ?>
                    <?php foreach ($options as $option) { ?>
                        <?php if ($option['option_id'] == 13) { ?>
                            <div class="col-md-12">
                                <h3 class="col-md-12" id="selectedBaseContainer"><?php echo $option['name']; ?> (1)</h3>
                                <h4 id="selectedBase"></h4>
                            </div>
                        <?php } ?>
                        <?php if ($option['option_id'] == 14) { ?>
                            <div class="col-md-12">
                                <h3 class="col-md-12" id="selectedSweetenerContainer"><?php echo $option['name']; ?> (1)</h3>
                                <h4 id="selectedSweetener"></h4>
                            </div>
                        <?php } ?>
                        <?php if ($option['option_id'] == 15) { ?>
                            <div class="col-md-12">
                                <h3 class="col-md-12" id="selectedEdfContainer"><?php echo $option['name']; ?> (4)</h3>
                                <h4 id="selectedEdf"></h4>
                            </div>
                        <?php } ?>
                        <?php if ($option['option_id'] == 16) { ?>
                            <div class="col-md-12">
                                <h3 class="col-md-12" id="selectedSanContainer"><?php echo $option['name']; ?> (2)</h3>
                                <h4 id="selectedSan"></h4>
                            </div>
                        <?php } ?>
                    <?php } ?>
                  <?php } ?>
			  </div>
			<div class="form-group">
				<div class="qty-product">
				    <div class="qty-product-content">
				        <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
					    <input type="button" id="minus" value="-" class="form-control" />
					    <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
					    <input type="button" id="plus" value="&#43;" class="form-control"/>
				    </div>
					
				</div>
				<div class="price-section">
			    <?php if ($price) { ?>
			        <p class="text-total">TOTAL</p>
			        <p class="main-price" id="main-price"><?php echo $price; ?></p>
			    <?php } ?>
			    </div>
			    
			    <!--29   -->
			    <?php foreach ($options as $option) { ?>
                    <?php if ($option['option_id'] == 29) { ?>
                        <div class="col-md-12">
                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center packaging-checkbox">
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="packaging" disabled/><?php echo $option_value['name']; ?>
                                <?php } ?>
                            </h3>
                        </div>
                    <?php } ?>
                <?php } ?>
			    <?php if ($quantity == 0.00) { ?>
			        <button class="btn-oos button" type="button" disabled>Stock Over!</button>
			        <br/>
			    <?php } else { ?>
				    <button class="button btn-block" type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_cart; ?></button>
				<?php } ?>
				<center style="margin-top:8px;"><a href="mailto:marketing@true-elements.com?Subject=Bulk%20discount" target="_top" style="color:#f49a25;">Contact us</a> for bulk discount.</center>
			</div>
			<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
		</div>
      </div>
	  <?php echo $content_bottom; ?>
	  </div>
	</div>
</div>

<script type="text/javascript"><!--

var minimum = <?php echo $minimum; ?>;
  $("#input-quantity").change(function(){
    if ($(this).val() < minimum) {
      $("#input-quantity").val(minimum);
      ajax_price();
    }
  });
  // increase number of product
  function minus(minimum){
      var currentval = parseInt($("#input-quantity").val());
      $("#input-quantity").val(currentval-1);
      if($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum){
          $("#input-quantity").val(minimum);
     }
     ajax_price();
  };
  // decrease of product
  function plus(){
      var currentval = parseInt($("#input-quantity").val());
     $("#input-quantity").val(currentval+1);
     ajax_price();
  };
  $('#minus').click(function(){
    minus(minimum);
  });
  $('#plus').click(function(){
    plus();
  });

//--></script>
<script type="text/javascript">
$('#button-cart').on('click', function() {
    if(checkboxCount()) {
	    $.ajax({
		    url: 'index.php?route=checkout/cart/add',
		    type: 'post',
		    data: $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, #sticky input[name=\'quantity\'], #sticky input[type=\'checkbox\']:checked'),
		    dataType: 'json',
		    beforeSend: function() {
		    	$('#button-cart').button('loading');
		    },
		    complete: function() {
		    	$('#button-cart').button('reset');
		    },
		    success: function(json) {
		    	$('.alert, .text-danger').remove();
		    	$('.form-group').removeClass('has-error');
			    if (json['error']) {
			    	if (json['error']['option']) {
			    		for (i in json['error']['option']) {
			    			var element = $('#input-option' + i.replace('_', '-'));
			    			if (element.parent().hasClass('input-group')) {
							    element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						    } else {
							    element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						    }
				    	}
				    }
				    if (json['error']['recurring']) {
				    	$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				    }
				    // Highlight any found errors
				    $('.text-danger').parent().addClass('has-error');
			    }
			    if (json['success']) {
			        var success_text = '<div style="margin: 0 0 10px 0">\
                    <div style="float: left"><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div>\
                    <div  style="text-align: center" >' + json['success'] + '</div>\
                    </div>';

                    var np = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button'
                    }).show();
			    	
				    $('#cart-total').html(json['total']);
				    
				    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
			    }
		    },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
	    });
    }
});

    function checkboxCount() {
        var baseinputList = document.getElementsByClassName("base");
        var sweetenerinputList = document.getElementsByClassName("sweetener");
        var edfinputList = document.getElementsByClassName("edf");
        var saninputList = document.getElementsByClassName("san");
        var basenumChecked = 0;
        var sweetenernumChecked = 0;
        var edfnumChecked = 0;
        var sannumChecked = 0;
        for (var i = 0; i < baseinputList.length; i++) {
            if (baseinputList[i].type == "checkbox" && baseinputList[i].checked) {
                basenumChecked = basenumChecked + 1;
            }
        }
        for (var i = 0; i < sweetenerinputList.length; i++) {
            if (sweetenerinputList[i].type == "checkbox" && sweetenerinputList[i].checked) {
                sweetenernumChecked = sweetenernumChecked + 1;
            }
        }
        for (var i = 0; i < edfinputList.length; i++) {
            if (edfinputList[i].type == "checkbox" && edfinputList[i].checked) {
                edfnumChecked = edfnumChecked + 1;
            }
        }
        for (var i = 0; i < saninputList.length; i++) {
            if (saninputList[i].type == "checkbox" && saninputList[i].checked) {
                sannumChecked = sannumChecked + 1;
            }
        }
            if((basenumChecked == 1 && sweetenernumChecked == 1)&&(sannumChecked == 2 && edfnumChecked == 4)) {
                return true;
            }
        if (basenumChecked < 1) {
            $('#basePopup').addClass('show');
            $('html, body').animate({
                scrollTop: $("#base-content").offset().top-100
            }, 1000);
        }
        if (sweetenernumChecked < 1) {
            $('#sweetenerPopup').addClass('show');
        }
        if (edfnumChecked < 4) {
            $('#edfPopup').addClass('show');
        }
        if (sannumChecked < 2) {
            $('#sanPopup').addClass('show');
        }
        if (basenumChecked == 1 && sweetenernumChecked < 1) {
            $('html, body').animate({
                scrollTop: $("#sweetener-content").offset().top-100
            }, 1000);
        }
        if ((basenumChecked == 1 && sweetenernumChecked == 1) && edfnumChecked < 4) {
            $('html, body').animate({
                scrollTop: $("#edf-content").offset().top-100
            }, 1000);
        }
        if ((basenumChecked == 1 && sweetenernumChecked == 1) && (edfnumChecked == 4 && sannumChecked < 2)) {
            $('html, body').animate({
                scrollTop: $("#san-content").offset().top-100
            }, 1000);
        }
        return false;
    }
</script>
    <script type="text/javascript">
$(document).ready(function() {
    if (window.history.replaceState) {
        window.history.replaceState("Make My Muesli", "Make My Muesli", "/makemymuesli");
    }
    ///After Page Load Enable All Checkboxes
    $("input").prop("disabled", false);

    $(".base").on("click", function() {
        $('input.base:checkbox:checked').parent().css({'box-shadow' : 'inset 0px 0px 0px 2px rgb(28,0,85)'});
        $('input.base:checkbox:not(:checked)').parent().css({'box-shadow' : 'none'});
        
        var numberOfChecked = $('input.base:checkbox:checked').length;
        if (numberOfChecked == 1) {
            $('input.base:checkbox:not(:checked)').prop("disabled", true);
            $('input.base:checkbox:not(:checked)').parent().addClass("disabled");
            $('input.base:checkbox:not(:checked)').next().find('.base-image').css({
                'cursor' : 'no-drop',
                'z-index' : '-1'
            });
            $('#basePopup').removeClass('show');
        }
        else {
            $(".checkbox .base").prop("disabled", false);
            $('input.base:checkbox:not(:checked)').next().find('.base-image').css({
                'cursor' : 'pointer'
            });
            $('input.base:checkbox:not(:checked)').parent().removeClass("disabled");
        }
    });
    $(".sweetener").on("click", function() {
        $('input.sweetener:checkbox:checked').parent().css({'box-shadow' : 'inset 0px 0px 0px 2px rgb(28,0,85)'});
        $('input.sweetener:checkbox:not(:checked)').parent().css({'box-shadow' : 'none'});
        
        var numberOfChecked = $('input.sweetener:checkbox:checked').length;
        if (numberOfChecked == 1) {
             $("input.sweetener:checkbox:not(:checked)").prop("disabled", true);
             $('input.sweetener:checkbox:not(:checked)').parent().addClass("disabled");
            $('input.sweetener:checkbox:not(:checked)').next().find('.sweetener-image').css({
                'cursor' : 'no-drop',
                'z-index' : '-1'
            });
            $('#sweetenerPopup').removeClass('show');
        }
        else {
            $(".checkbox .sweetener").prop("disabled", false);
            $('input.sweetener:checkbox:not(:checked)').next().find('.sweetener-image').css({
                'cursor' : 'pointer'
            });
            $('input.sweetener:checkbox:not(:checked)').parent().removeClass("disabled");
        }
    });
    $(".edf").on("click", function() {
        $('input.edf:checkbox:checked').parent().css({'box-shadow' : 'inset 0px 0px 0px 2px rgb(28,0,85)'});
        $('input.edf:checkbox:not(:checked)').parent().css({'box-shadow' : 'none'});
        
        var numberOfChecked = $('input.edf:checkbox:checked').length;
        if (numberOfChecked == 4) {
             $("input.edf:checkbox:not(:checked)").prop("disabled", true);
             $('input.edf:checkbox:not(:checked)').parent().addClass("disabled");
            $('input.edf:checkbox:not(:checked)').next().find('.edf-image').css({
                'cursor' : 'no-drop',
                'z-index' : '-1'
            });
            $('#edfPopup').removeClass('show');
        }
        else {
            $(".checkbox .edf").prop("disabled", false);
            $('input.edf:checkbox:not(:checked)').next().find('.edf-image').css({
                'cursor' : 'pointer'
            });
            $('input.edf:checkbox:not(:checked)').parent().removeClass("disabled");
        }
    });
    $(".san").on("click", function() {
        $('input.san:checkbox:checked').parent().css({'box-shadow' : 'inset 0px 0px 0px 2px rgb(28,0,85)'});
        $('input.san:checkbox:not(:checked)').parent().css({'box-shadow' : 'none'});
        
        var numberOfChecked = $('input.san:checkbox:checked').length;
        if (numberOfChecked == 2) {
             $("input.san:checkbox:not(:checked)").prop("disabled", true);
             $('input.san:checkbox:not(:checked)').parent().addClass("disabled");
            $('input.san:checkbox:not(:checked)').next().find('.san-image').css({
                'cursor' : 'no-drop',
                'z-index' : '-1'
            });
            $('#sanPopup').removeClass('show');
        }
        else {
            $(".checkbox .san").prop("disabled", false);
            $('input.san:checkbox:not(:checked)').next().find('.san-image').css({
                'cursor' : 'pointer'
            });
            $('input.san:checkbox:not(:checked)').parent().removeClass("disabled");
        }
    });

});
</script>
<script type="text/javascript">
$('.base').click(function() {
    var basetext= "";
    $('.base:checked').each(function() {
        basetext+='&emsp;'+$(this).next('label').find('p.option_name').text()+'<br/>';
    });
    document.getElementById("selectedBase").innerHTML = '<p style="border-left: 3px solid #f49a25;">'+basetext+'</p>';
});
$('.sweetener').click(function() {
    var sweetenertext= "";
    $('.sweetener:checked').each(function() {
        sweetenertext+='&emsp;'+$(this).next('label').find('p.option_name').text()+'<br/>';
    });
    document.getElementById("selectedSweetener").innerHTML = '<p style="border-left: 3px solid #f49a25;">'+sweetenertext+'</p>';
});
$('.edf').click(function() {
    var edftext= "";
    $('.edf:checked').each(function() {
        edftext+='&emsp;'+$(this).next('label').find('p.option_name').text()+'<br/>';
    });
    document.getElementById("selectedEdf").innerHTML = '<p style="border-left: 3px solid #f49a25;">'+edftext+'</p>';
});
$('.san').click(function() {
    var santext= "";
    $('.san:checked').each(function() {
        santext+='&emsp;'+$(this).next('label').find('p.option_name').text()+'<br/>';
    });
    document.getElementById("selectedSan").innerHTML = '<p style="border-left: 3px solid #f49a25;">'+santext+'</p>';
});
</script>
<script type="text/javascript">
    document.getElementById("basePopup").onclick = function () {
        $('#basePopup').removeClass('show');
    }
    document.getElementById("edfPopup").onclick = function () {
        $('#edfPopup').removeClass('show');
    }
    document.getElementById("sweetenerPopup").onclick = function () {
        $('#sweetenerPopup').removeClass('show');
    }
    document.getElementById("sanPopup").onclick = function () {
        $('#sanPopup').removeClass('show');
    }
    $("#selectedBaseContainer").click(function() {
    $('html, body').animate({
        scrollTop: $("#base-content").offset().top-100
    }, 1000);
    });
    $("#selectedSweetenerContainer").click(function() {
    $('html, body').animate({
        scrollTop: $("#sweetener-content").offset().top-100
    }, 1000);
    });
    $("#selectedEdfContainer").click(function() {
    $('html, body').animate({
        scrollTop: $("#edf-content").offset().top-100
    }, 1000);
    });
    $("#selectedSanContainer").click(function() {
    $('html, body').animate({
        scrollTop: $("#san-content").offset().top-100
    }, 1000);
    });
    if(performance.navigation.type == 2){
   location.reload(true);
}
</script>
<script type="text/javascript">
var ajax_price = function() {
	$.ajax({
		type: 'POST',
		url: "index.php?route=product/live_options/index&product_id=<?php echo $product_id; ?>",
		data: $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, #sticky input[name=\'quantity\']'),
		dataType: 'json',
			success: function(json) {
			    if (json.success) {
			    	change_price('#main-price', json.new_price.price);
			    	change_price('.main-price-mobile', "Cart Value: " + json.new_price.price);
			    }
		}
	});
}

var change_price = function(id, new_price) {
	$(id).html(new_price);
}
$('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\'], #product input[type=\'checkbox\'], #product select, #product textarea, input[name=\'quantity\']').on('change', function() {
	ajax_price();
});
</script>
<?php echo $footer; ?>
