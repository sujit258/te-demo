<?php echo $header; ?>
<style>
div #sticky {
    position: -webkit-sticky;
    position: sticky;
    top: 71px;
    margin-top: 49px;
}
body {
    font-size: 15px!important;
}
.checkbox input {
    display: none;
}

#product .control-label {
    font-size: 18px;
    font-weight: bold;
    color: #f49a25;
}

h6 {
    margin-bottom: 0px !important;
    margin-left: 15px !important;
    margin-top: 25px !important;
    line-height: 1.3 !important;
    padding-bottom: 10px !important;
}

#product h3 {
    text-align: center;
    color: #f49a25;
}

.checkbox label {
    padding: 0px!important;
    display: inline-block;
    position: relative;
    cursor: pointer;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-align: center;
    font-size: 15px;
    color: #f49a25;
    width:auto;
    min-height:270px!important;
}

label img {
    min-height: 100%!important;
    width: auto;
    transition-duration: 0.2s;
    transform-origin: 50% 50%;
    position: relative;
}

:checked + label img {
    transform: scale(0.9);
    z-index: -1;
    position:relative;
}

:checked + label {
    background-image:linear-gradient(rgba(0, 0, 0, 0.7), transparent);
}

.checkbox {
    display: inline-block;
    text-align: center;
    min-height: 250px;
    border: 1px solid rgb(28,0,85);
    margin: 30px 10px 30px!important;
    padding-right: 0;
    padding-left: 0;
    border-radius:10px;
    overflow: hidden;
    width: 30%;
}

@media screen and (max-width: 800px) {
    .googleplay-original {
        margin-top: 10px;
    }
    .checkbox {
        width: 44% !important;   
    }
    .priceOnMobile {
        display: inline-block!important;
        z-index: 1;
        background:rgba(255,255,255,0.9);
        width: 100%;
        position: fixed;
        top: 0px;
    }
    .main-price-mobile {
        position: relative;
        text-align: center;
    }
    .banner-container {
        margin-right: -15px;
        margin-left: -15px;
    }
    div #sticky {
        margin-top: 0px;
    }
    .nav li a {
        padding: 10px 10px!important;
        font-size: 14px!important;
    }
    .snack-types {
        margin-top: 25px;
    }
    .checkbox:hover p.tip {
        display: none!important;
    }
    .modal-dialog {
        top: 5%;
    }
    .antioxidant-tab, .fibre-tab {
        padding-right: 2.5px;
    }
    .protein-tab, .blank-tab {
        padding-left: 2.5px;
        padding-right: 2.5px;
    }
    .ketogenic-tab, .masti-tab {
        padding-right: 0px;
        padding-left: 2.5px;
    }
    .option-text {
        width: 91%!important;
    }
    .option-text p {
        font-size: 13px!important;
    }
}

.option_name {
    font-size: 15px;
    min-height: 40px;
}

.weight {
    font-size: 15px;
    color: #000000;
}

#product {
    max-height: 100%;
    margin-bottom: 0px;
}

.has-option {
    border: none;
    padding: 0px;
    box-shadow: none;
}

.option_help {
    color:#1C0055;
    font-size:15px;
    text-align:center;
}

#top_banner {
    padding-right: 0;
    padding-left: 0;
}

p {
    font-size: 15px;
    margin: 0;
}

.product-title-section {
    background-color: #A193A4;
    margin-top: 10px;
}

.product-title-subsection {
    background-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0),rgba(0, 0, 0, 0.5));
    padding: 45px 20px;
}

.product-title{
    font-size: 28px!important;
    margin-bottom: 0px!important;
    margin-top: 0px!important;
    color: #ffffff!important;
    text-transform: none!important;
}

.selected-options h3 {
    font-size: 17px!important;
    font-weight: 600!important;
    text-align:left!important;
    margin-top: 5px!important;
    margin-bottom: 5px!important;
    color:#000;
}

.selected-options div {
    padding-left: 0px;
}

.selected-options {
    padding-top:10px!important;
}

#minus {
    border-top-left-radius: 25px!important;
    border-bottom-left-radius: 25px!important;
    border-right:none!important;
    font-size: 15px;
}

#plus {
    border-top-right-radius: 25px!important;
    border-bottom-right-radius: 25px!important;
    border-left:none!important;
    font-size: 15px;
}

#input-quantity {
    padding-left: 5px!important;
    padding-right: 5px!important;
    border-left:none!important;
    border-right:none!important;
    width: 90px!important;
}

#minus:hover, #plus:hover {
    color: #f49a25!important;
    background: #fff!important;
}

#minus,#plus,#input-quantity {
    height:50px!important;
}

.qty-product label {
    margin-top: 15px!important;
}

.qty-product-content {
    position: relative;
    padding-top: 15px;
    width:80%;
    margin:auto;
    float:right;
}

button[disabled] {
    cursor: not-allowed!important;
    background: #000;
}

.btn-block {
    border-radius: 25px;
    background: #f49a25;
    width: 100%;
}

.btn-oos {
    border-radius: 25px;
    background: #777!important;
    width: 100%;
    margin-bottom: 7px;
}

.text-total {
    font-size: 18px!important;
    color: #f49a25 !important;
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    font-weight: 800;
    display: table-cell;
}

.main-price {
    font-size: 40px!important;
    color: #f49a25 !important;
    display: table-cell;
}

.price-section {
    min-height: 60px;
    line-height: 0px!important;
}

.button:hover, .button:active, .button:focus {
    background: #000;
}
.button {
    font-size: 16px!important;
    padding: 10px 16px!important;
    font-weight: 600!important;
}
.disabled {
    background-image : linear-gradient(to bottom, rgb(241,241,241), transparent);
    cursor : no-drop;
}

p.tip {
    text-decoration: none;
    display:none;
    padding: 1px;
}

.checkbox:hover p.tip {
    display: block;
    border-radius: 6px;
    background-color: gold;
    position: absolute;
    top: 0px;
    color: #000;
    text-decoration: none;
    z-index:2;
    left:0;
}

.popuptext {
    position: relative;
    text-align: center;
    visibility: hidden;
    background: #f44336;
    color: #fff;
    padding: 5px 15px;
    width: 250px;
    left: calc( 50% - 125px);
    border-radius: 5px;
    cursor: pointer;
    -webkit-animation: updown 1.4s ease-in-out infinite;
    -moz-animation: updown 1.4s ease-in-out infinite;
    -o-animation: updown 1.4s ease-in-out infinite;
    animation: updown 1.4s ease-in-out infinite;
    top: 10px;
}

/* Popup arrow */
.popuptext::after {
    content: '';
    display: block;
    position: absolute;
    left: calc( 50% - 7px );
    width: 0px;
    text-align: center;
    height: 0;
    border-top: 14px solid #F44336;
    border-right: 14px solid transparent;
    border-bottom: 0 solid transparent;
    border-left: 14px solid transparent;
}

/* Toggle this class - hide and show the popup */
.show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
    display: inline-block!important;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}
#selectedSeedsNutsContainer, #selectedBerriesFruitsContainer {
    cursor: pointer;
}
#selectedSeedsNutsContainer h3 h3, #selectedBerriesFruitsContainer h3 {
    cursor: pointer;
    font-size: 17px!important;
    font-weight: 600!important;
    color: #000!important;
}

.priceOnMobile {
    display: none;
    padding: 10px;
}

.main-price-mobile {
    font-size: 15px!important;
    color: #f49a25 !important;
}

.banner-container {
    margin: 0!important;
}
#top_banner img {
    width: 100vw!important;
    cursor: pointer;
}
.snack-types {
    z-index: 10;
    margin-bottom: 10px;
    margin-top: 50px;
}
#selectedOptions p {
    line-height: 1.2;   
}
.nav-pills li {
    text-align: center;
    margin-top: 10px;
}
.nav-pills > li + li {
    margin-left: 0!important;
}
.nav a {
    font-weight: 600;
}
.nav a span {
    vertical-align: middle;
}
.nav li a {
    padding: 15px 10px;
    font-size: 18px;
}
.option-banner {
    margin-top: 15px;
}
.nav-pills li.active a {
    color: #fff!important;
    background-color: #f49a25!important;
}
.nav-pills li a {
    color: #000;
    border: 1px solid #f49a25;
    background: #fff;
}
.modal-body h4, .modal-title {
    font-weight: 600;
}
.modal-button {
    cursor: pointer;
    font-weight: 600;
}
.modal-dialog {
    top: 15%;
    width: 300px!important;
    margin: auto;
}
.modal-body {
    max-height: calc(100vh - 200px);
    overflow-y: auto;
}
#button-cart::before {
    font-family: 'Material-Design-Iconic-Font';
    display: inline-block;
    font-size: 16px;
    content: "\f1cb";
    margin-right: 10px;
    font-weight: 400;
}
.seasoned-variety {
    margin: 0 0 10px 0 !important;
}
.option-text {
    width: 94%;
    text-align: justify;
    background: #fcfcfc;
    padding: 15px;
    margin-left: 15px;
}
#product .radio { 
	display: inline-block;
	margin: 0px 2px 5px 0px;
}
#product .radio input, #product .radio label > div { 
	display: none !important;
}
#product .radio label span {
	display: block;
	padding: 6px 12px;
}
#product .radio label {
	display: block;
	background-color: #FFFFFF;
	border: 1px solid #D4D4D4;
	border-radius: 3px;
	color: #222222;
	padding: 0;
	text-align: center;
}
#product .radio label img {
	border: 2px solid #FFFFFF;
	margin: 2px;
}
#product .checked-option {
	background-color: #000000 !important;
	color: #FFFFFF !important;
	border: 1px solid #000000 !important;
}
.selection {
    font-weight: bold;
    color: #f49a25;
}
.packaging-checkbox {
    font-size: 17px !important;
    font-weight: 600 !important;
    text-align: left !important;
}
.packaging {
    margin-right: 10px!important;
    margin-left: -10px!important;
}
.product-name-section {
    min-height: 40px;
}
.select-types {
    margin-top: 5px;
    margin-bottom: 15px;
}
</style>
<div class="row banner-container col-md-12 col-sm-12 col-xs-12" style="padding-top: 2%;">
    <img class="p0 img-responsive hidden-xs hidden-sm" src="image/catalog/Banner/mms-mmm-desktop.jpg">
    <img class="p0 img-responsive hidden-md hidden-lg hidden-xl" src="image/catalog/Banner/mms-mmm-mobile.jpg">
</div>
<div class="col-md-12 col-sm-12 col-xs-12 select-types" role="tabpanel">
    <ul class="nav nav-pills nav-fill" role="tablist">
        <li class="nav col-md-4 col-sm-1 col-xs-1"></li>
        <li role="presentation" class="nav active col-md-2 col-sm-5 col-xs-5">
            <a style="cursor: pointer;"><span>Make My Snack</span></a>
        </li>
        <li role="presentation" class="nav col-md-2 col-sm-5 col-xs-5">
            <a href="/index.php?route=product/make_my_muesli" style="cursor:pointer"><span>Make My Muesli</span></a>
        </li>
        <li class="nav col-md-4 col-sm-1 col-xs-1"></li> 
    </ul>
</div>
<div class="row banner-container">
    <div class="col-md-12 col-sm-12 col-xs-12" id="top_banner">
        <img alt="True Elements" class="img-responsive" src="image/catalog/Banner/mms-banner.jpg" />
    </div>
</div>
<div class="container">
    <div class="modal fade" id="save-creation" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Save Your Creation</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="my-creation" placeholder="Enter Custom Name">
                </div>
                <div class="modal-footer">
                    <button class="button btn-wishlist btn-block" type="button" title="Save Your Creation" style="padding: 6px 16px !important;">Save Creation</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="content" class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
		        <div id="product" class="col-md-8 col-sm-12 col-xs-12">
		            <div class="priceOnMobile">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <p class="main-price-mobile">Cart Value: <?php echo $price; ?></p> 
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 snack-types" role="tabpanel">
                        <ul class="nav nav-pills nav-fill" role="tablist">
                            <li role="presentation" class="nav active col-md-4 col-sm-4 col-xs-4 antioxidant-tab">
                                <a href="#antioxidant-content" aria-controls="antioxidant-content" role="tab" data-toggle="tab"><span>Antioxidant</span></a>
                            </li>
                            <li role="presentation" class="nav col-md-4 col-sm-4 col-xs-4 protein-tab">
                                <a href="#protein-content" aria-controls="protein-content" role="tab" data-toggle="tab"><span>Protein</span></a>
                            </li>
                            <li role="presentation" class="nav col-md-4 col-sm-4 col-xs-4 ketogenic-tab">
                                <a href="#ketogenic-content" aria-controls="ketogenic-content" role="tab" data-toggle="tab"><span>Ketogenic</span></a>
                            </li>
                            <li role="presentation" class="nav col-md-4 col-sm-4 col-xs-4 fibre-tab">
                                <a href="#fibre-content" aria-controls="fibre-content" role="tab" data-toggle="tab"><span>Fibre</span></a>
                            </li>
                            <li role="presentation" class="nav col-md-4 col-sm-4 col-xs-4 blank-tab">
                            </li>
                            <li role="presentation" class="nav col-md-4 col-sm-4 col-xs-4 masti-tab">
                                <a href="#masti-content" aria-controls="masti-content" role="tab" data-toggle="tab"><span>Masti</span></a>
                            </li>
                        </ul>
                    </div>
            
                    <?php if ($options) { ?>
                        <div class="tab-content">
                            <?php foreach ($options as $option) { ?>
                                <?php if ($option['type'] == 'checkbox') { ?>
                                    <?php if ($option['option_id'] == 21) { ?>
                                        <div class="form-group tab-pane active" id="antioxidant-content" role="tabpanel">
                                            <div class="option-text col-md-12 col-xs-12 col-sm-12">
                                                <p>It’s super wise and NOT always wrong to kill your enemies before they kill you. On such days, better not encourage the uninvited guests (foreign particles) and it’s time for the unsung heroes in your diet to get the importance that they deserve.</p>
                                                <p>Get the right fix for your antioxidant needs from snackible munchies, and ditch those tasteless medicines doing no good to your body.</p>
                                            </div>
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Seeds & Nuts</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(50% By Weight) (Select any <span class="selection">Five</span>)</p>
                                            <span class="popuptext seedsNutsPopup">Please Select Atleast 5!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Seeds & Nuts") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 seedsNuts">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="antioxidant" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img src="<?php echo $option_value['image']; ?>" alt="True Elements" class="antioxidant-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                  
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
              
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Berries & Fruits</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(50% By Weight) (Select any <span class="selection">Two</span>)</p>
                                            <span class="popuptext berriesFruitsPopup">Please Select Atleast 2!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Berries & Fruits") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 berriesFruits">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="antioxidant" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="antioxidant-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            
                                        </div>
                                    <?php } ?>
            
                                    <?php if ($option['option_id'] == 22) { ?>
                                        <div class="form-group tab-pane" id="protein-content" role="tabpanel">
                                            <div class="option-text col-md-12 col-xs-12 col-sm-12">
                                                <p>Whether you are set to fuel up your body before hitting the gym or you are just satisfying your mid-day snack, high-protein crunchies are your rescue to a lasting energy boost. And if you think eggs will do their job and compensate for the rest, think again!</p>
                                                <p>These healthy, delicious and easy on-the-go options have really quite a high protein content as compared to a large egg. Probably the best bang for your protein buck.</p>
                                            </div>
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Seeds & Nuts</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(65% By Weight) (Select any <span class="selection">Five</span>)</p>
                                            <span class="popuptext seedsNutsPopup">Please Select Atleast 5!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Seeds & Nuts") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 seedsNuts">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="protein" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="protein-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                  
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
              
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Berries & Fruits</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(35% By Weight) (Select any <span class="selection">Two</span>)</p>
                                            <span class="popuptext berriesFruitsPopup">Please Select Atleast 2!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Berries & Fruits") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 berriesFruits">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="protein" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="protein-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($option['option_id'] == 23) { ?>
                                        <div class="form-group tab-pane" id="ketogenic-content" role="tabpanel">
                                            <div class="option-text col-md-12 col-xs-12 col-sm-12">
                                                <p>The key to guilt-free snacking is not only cutting down on carbs and following a ketogenic diet, but also going for extraordinary appealing snacks, such that it becomes difficult to feel confined by your diet. Bump up your fat intake with these crispy snacks while gratifying your taste buds at the same time.</p>
                                                <p>Let your Keto diet be Keto for real, but with less hassle and easy-peasy options!</p>
                                            </div>
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Seeds & Nuts</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(75% By Weight) (Select any <span class="selection">Five</span>)</p>
                                            <span class="popuptext seedsNutsPopup">Please Select Atleast 5!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Seeds & Nuts") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 seedsNuts">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="ketogenic" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="ketogenic-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
              
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Berries & Fruits</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(25% By Weight) (Select any <span class="selection">Two</span>)</p>
                                            <span class="popuptext berriesFruitsPopup">Please Select Atleast 2!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Berries & Fruits") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 berriesFruits">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="ketogenic" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="ketogenic-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($option['option_id'] == 24) { ?>
                                        <div class="form-group tab-pane" id="fibre-content" role="tabpanel">
                                            <div class="option-text col-md-12 col-xs-12 col-sm-12">
                                                <p>And when you hear of “high-fibre snacks”, do you think of bran muffins, extremely crunchy crackers or the chalky supplements. It’s time you rethink again about everything you actually know about high fibre snacking. When it comes to fibrous foods, you don’t always need to force down bland whole grains to get more fibre. All you simply need is to know is which delicious snacks and ingredients possess already high fibre in them.</p>
			                                    <p>Below listed options won’t only satisfy your high-fibre snacks, but will also fill up your stomach with taste, deliciousness and craving for more!</p>
                                            </div>
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Seeds & Nuts</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(65% By Weight) (Select any <span class="selection">Five</span>)</p>
                                            <span class="popuptext seedsNutsPopup">Please Select Atleast 5!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Seeds & Nuts") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 seedsNuts">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="fibre" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="fibre-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
              
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Berries & Fruits</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(35% By Weight) (Select any <span class="selection">Two</span>)</p>
                                            <span class="popuptext berriesFruitsPopup">Please Select Atleast 2!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Berries & Fruits") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 berriesFruits">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="fibre" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="fibre-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($option['option_id'] == 25) { ?>
                                        <div class="form-group tab-pane" id="masti-content" role="tabpanel">
                                            <div class="option-text col-md-12 col-xs-12 col-sm-12">
                                                <p>With a stressful lifestyle leading to difficult choices in choosing the right crunchies for your masti mood swings, we agree you fall into a confused state of mind when it comes to healthy snacking. For you super jolly moments to not fade and make it slightly more joyful, bang on some crunchy mix of cheerful vibes from a set of below mentioned crunchies.</p>
                                                <p>P.S. Beware! It can take your masti moods to a slightly more upper level with more fun, entertainment and probably non-stop laughter.</p>
                                            </div>
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Seeds & Nuts</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(75% By Weight) (Select any <span class="selection">Five</span>)</p>
                                            <span class="popuptext seedsNutsPopup">Please Select Atleast 5!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Seeds & Nuts") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 seedsNuts">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="masti" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="masti-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
              
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Berries & Fruits</h3>
                                            <p class="option_help col-md-12 col-xs-12 col-sm-12">(25% By Weight) (Select any <span class="selection">Two</span>)</p>
                                            <span class="popuptext berriesFruitsPopup">Please Select Atleast 2!</span>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if($option_value['category'] == "Berries & Fruits") { ?>
                                                        <div class="checkbox col-md-3 col-sm-5 col-xs-5 berriesFruits">
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="masti" disabled/>
                                                            <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="masti-image"/><br/><div class="product-name-section"><p class="option_name"><?php echo $option_value['name']; ?></p></div></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            
                            <?php foreach ($options as $option) { ?>
                                <?php if ($option['type'] == 'radio') { ?>
                                    <?php if ($option['option_id'] == 26) { ?>
                                        <div class="col-md-12">
                                            <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center"><?php echo $option['name']; ?></h3>
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <br/>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio col-md-3 col-sm-6 col-xs-6 seasoned-variety" id="seasoned-variety">
                                                    <?php if($option_value['name'] == "Plain") { ?>
                                                        <label class="checked-option">
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="seasoned plain" checked/><span><?php echo $option_value['name']; ?></span>
                                                        </label>
                                                    <?php } else { ?>
                                                        <label>
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="seasoned"/><span><?php echo $option_value['name']; ?></span>
                                                        </label>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
          
                <!--Main Section-->
                <div class="col-md-4 col-sm-12 col-xs-12" id="sticky">
                    <div class="product-title-section">
                        <div class="product-title-subsection">
			                <h2 class="product-title"><?php echo $heading_title; ?>, 250gm</h2>
			            </div>
			        </div>
    			    <div class="selected-options">
    			        <div class="col-md-12">
                            <h3 class="col-md-12" id="selectedSeedsNutsContainer">Seeds & Nuts (5)</h3>
                            <h4 id="selectedSeedsNuts"></h4>
                        </div>
                        <div class="col-md-12">
                            <h3 class="col-md-12" id="selectedBerriesFruitsContainer">Berries & Fruits (2)</h3>
                            <h4 id="selectedBerriesFruits"></h4>
                        </div>
	    		    </div>
		    	    <div class="form-group">
		    		    <div class="qty-product">
		    		        <div class="qty-product-content">
		    		            <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
		    		    	    <input type="button" id="minus" value="-" class="form-control" />
			    	    	    <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
			    	    	    <input type="button" id="plus" value="&#43;" class="form-control"/>
			    	    	    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
			    	        </div>
			    	    </div>
				        <div class="price-section">
			                <?php if ($price) { ?>
			                    <p class="text-total">TOTAL</p>
			                    <p class="main-price" id="main-price"><?php echo $price; ?></p>
			                <?php } ?>
			            </div>
			            <?php foreach ($options as $option) { ?>
                            <?php if ($option['option_id'] == 29) { ?>
                                <div class="col-md-12">
                                    <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center packaging-checkbox">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="packaging" disabled/><?php echo $option_value['name']; ?>
                                        <?php } ?>
                                    </h3>
                                </div>
                            <?php } ?>
                        <?php } ?>
			            <?php if ($quantity == 0.00) { ?>
                            <button class="btn-oos button" type="button" disabled>Stock Over!</button>
			                <br/>
                        <?php } else { ?>
		    		            <button class="button btn-block" type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_cart; ?></button>
		    		    <?php } ?>
				        <?php if($customer_id) { ?>
				            <button class="button modal-button btn-block" type="button" title="Save Your Creation">Save Your Creation</button>
				        <?php } else { ?>
				            <button class="button btn-block btn-oos" type="button" title="Login to Save Your Creation" disabled>Login to Save Your Creation</button>
				        <?php } ?>
				        <center style="margin-top:8px;"><a href="mailto:marketing@true-elements.com?Subject=Bulk%20discount" target="_top" style="color:#FDAD00;">Contact us</a> for bulk discount.</center>
			        </div>
			        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
			    </div>
            </div>
	    <?php echo $content_bottom; ?>
	    </div>
	</div>
</div>
<script type="text/javascript"><!--
$(".nav-pills li").click(function() {
    $(".plain").trigger("click");
    $("input:checkbox").removeAttr("checked");
    $("input").prop("disabled", false);
    $("input:checkbox:not(:checked)").next().find("img").css({
        "cursor" : "pointer"
    });
    $("input:checkbox:not(:checked)").parent().removeClass("disabled");
    $("input:checkbox:not(:checked)").parent().css({"box-shadow" : "none"});
    $("#selectedSeedsNuts, #selectedBerriesFruits").html("");
    $(".popuptext").removeClass("show");
    ajax_price();
});
</script>
<script type="text/javascript"><!--
    var minimum = <?php echo $minimum; ?>;
    $("#input-quantity").change(function(){
        if ($(this).val() < minimum) {
            $("#input-quantity").val(minimum);
            ajax_price();
        }
    });
    
    function minus(minimum){
        var currentval = parseInt($("#input-quantity").val());
        $("#input-quantity").val(currentval-1);
        if($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum){
            $("#input-quantity").val(minimum);
        }
        ajax_price();
    };
    
    function plus(){
        var currentval = parseInt($("#input-quantity").val());
        $("#input-quantity").val(currentval+1);
        ajax_price();
    };
    
    $("#minus").click(function(){
        minus(minimum);
    });
    
    $("#plus").click(function(){
        plus();
    });
</script>
<script type="text/javascript">
    $("#button-cart").click(function() {
        if (checkboxCount()) {
    	    $.ajax({
    		    url: "index.php?route=checkout/cart/add",
    		    type: "post",
	    	    data: $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, #sticky input[name=\'quantity\'], #sticky input[type=\'checkbox\']:checked'),
	    	    dataType: "json",
	    	    beforeSend: function() {
	    	    	$("#button-cart").button("loading");
	    	    },
		        complete: function() {
		        	$("#button-cart").button("reset");
		        },
		        success: function(json) {
		        	$(".alert, .text-danger").remove();
		        	$(".form-group").removeClass("has-error");
			        if (json["error"]) {
			        	if (json["error"]["option"]) {
			        		for (i in json["error"]["option"]) {
			        			var element = $("#input-option" + i.replace("_", "-"));
			        			if (element.parent().hasClass("input-group")) {
			        				element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
			        			} else {
			        				element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
			        			}
    			    		}
    			    	}
    				    if (json["error"]["recurring"]) {
    				    	$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
    				    }
	    			    // Highlight any found errors
	    			    $(".text-danger").parent().addClass("has-error");
	    		    }
	    		    if (json["success"]) {
	    		        var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left"><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center" >' + json['success'] + '</div></div>';
    
                        var np = new Noty({
                            type: "success",
                            layout: json["notice_add_layout"],
                            text:  success_text,
                            textAlign:"right",
                            animateOpen:{"height":"toggle"},
                            timeout: json["notice_add_timeout"],
                            progressBar: false,
                            closeWith: "button"
                        }).show();
			        	
				        $("#cart-total").html(json["total"]);
				        
				        $("#cart > .top-cart-contain ul").load("index.php?route=common/cart/info ul li");
    			    }
    		    },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
	        });
        }
    });
    $(".modal-button").click(function () {
       if (checkboxCount()) {
            $('#save-creation').modal();
       }
    });

    $(".btn-wishlist").click(function() {
        $.ajax({
        	url: 'index.php?route=account/wishlist/add',
        	type: 'post',
        	data: $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, #sticky input[name=\'quantity\'], #sticky input[type=\'checkbox\']:checked, #save-creation input[type=\'text\']'),
	       	dataType: 'json',
	       	success: function(json) {
	       		$('.alert').remove();
        	    	if (json['redirect']) {
	        		location = json['redirect'];
	        	}
		    	if (json["success"]) {
		    	    var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left"><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center" >' + json['success'] + '</div></div>';
    
                    var np = new Noty({
                        type: "success",
                        layout: json["notice_add_layout"],
                        text:  success_text,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json["notice_add_timeout"],
                        progressBar: false,
                        closeWith: "button"
                    }).show();
	    		    $('#wishlist-total span').html(json['total']);
    			    $('#wishlist-total').attr('title', json['total']);
    			}
    		},
    		error: function(xhr, ajaxOptions, thrownError) {
	    		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    	    }
        });
    });

    function checkboxCount() {
        var seedsNutsinputList = $(".seedsNuts input");
        var seedsfruitsnumChecked = 0;
        for (var i = 0; i < seedsNutsinputList.length; i++) {
            if (seedsNutsinputList[i].type == "checkbox" && seedsNutsinputList[i].checked) {
                seedsfruitsnumChecked = seedsfruitsnumChecked + 1;
            }
        }
        
        var berriesfruitsinputList = $(".berriesFruits input");
        var berriesfruitsnumChecked = 0;
        for (var i = 0; i < berriesfruitsinputList.length; i++) {
            if (berriesfruitsinputList[i].type == "checkbox" && berriesfruitsinputList[i].checked) {
                berriesfruitsnumChecked = berriesfruitsnumChecked + 1;
            }
        }
        
        if(seedsfruitsnumChecked == 5 && berriesfruitsnumChecked == 2) {
            return true;
        } else {
            if(seedsfruitsnumChecked < 5) {
                $(".tab-pane.active .seedsNutsPopup").addClass("show");
            }
            if(berriesfruitsnumChecked < 2) {
                $(".tab-pane.active .berriesFruitsPopup").addClass("show");
            }
            
            if(seedsfruitsnumChecked < 5) {
                $("html, body").animate({
                    scrollTop: $(".seedsNutsPopup").offset().top-200
                }, 1000);
            } else if(berriesfruitsnumChecked < 2) {
                $("html, body").animate({
                    scrollTop: $(".berriesFruitsPopup").offset().top-200
                }, 1000);
            }
        }
        return false;
    }
</script>
<script>
    $(document).ready(function() {
        //For changing URL
        if (window.history.replaceState) {
            window.history.replaceState("Make My Snack", "Make My Snack", "/makemysnack");
        }
    
        //After Page Load Enable All Checkboxes
        $("input").prop("disabled", false);
        $(".packaging").prop("disabled", true);
        $(".packaging-checkbox").prop("title", "{Plastic Free Packaging option is temporarily disabled due to COVID-19 Pandemic").css({"opacity" : "0.5", "cursor" : "no-drop"});
    
        //Seeds & Nuts
        $(".seedsNuts").on("click", function() {
            $(".seedsNuts input:checkbox:checked").parent().css({"box-shadow" : "inset 0px 0px 0px 2px rgb(28,0,85)"});
            $(".seedsNuts input:checkbox:not(:checked)").parent().css({"box-shadow" : "none"});
            
            var numberOfChecked = $(".seedsNuts input:checkbox:checked").length;
            if (numberOfChecked == 5) {
                $(".seedsNuts input:checkbox:not(:checked)").prop("disabled", true);
                $(".seedsNuts input:checkbox:not(:checked)").parent().addClass("disabled");
                $(".seedsNuts input:checkbox:not(:checked)").next().find("img").css({
                    "cursor" : "no-drop",
                    "z-index" : "-1"
                });
                $(".seedsNutsPopup").removeClass("show");
            } else {
                $(".seedsNuts input").prop("disabled", false);
                $(".seedsNuts input:checkbox:not(:checked)").next().find("img").css({
                    "cursor" : "pointer"
                });
                $(".seedsNuts input:checkbox:not(:checked)").parent().removeClass("disabled");
            }
        });
        
        //Berries & Fruits
        $(".berriesFruits").on("click", function() {
            $(".berriesFruits input:checkbox:checked").parent().css({"box-shadow" : "inset 0px 0px 0px 2px rgb(28,0,85)"});
            $(".berriesFruits input:checkbox:not(:checked)").parent().css({"box-shadow" : "none"});
            
            var numberOfChecked = $(".berriesFruits input:checkbox:checked").length;
            if (numberOfChecked == 2) {
                $(".berriesFruits input:checkbox:not(:checked)").prop("disabled", true);
                $(".berriesFruits input:checkbox:not(:checked)").parent().addClass("disabled");
                $(".berriesFruits input:checkbox:not(:checked)").next().find("img").css({
                    "cursor" : "no-drop",
                    "z-index" : "-1"
                });
                $(".berriesFruitsPopup").removeClass("show");
            } else {
                $(".berriesFruits input").prop("disabled", false);
                $(".berriesFruits input:checkbox:not(:checked)").next().find("img").css({
                    "cursor" : "pointer"
                });
                $(".berriesFruits input:checkbox:not(:checked)").parent().removeClass("disabled");
            }
        });
    
        $(".seedsNuts").click(function() {
            var seedsnutstext= "";
            $(".seedsNuts input:checked").each(function() {
                seedsnutstext +="&emsp;" + $(this).next("label").find("p.option_name").text()+"<br/>";
            });
            $("#selectedSeedsNuts").html('<p style="border-left: 3px solid #f49a25;">' + seedsnutstext + '</p>');
        });
    
        $(".berriesFruits").click(function() {
            var berriesfruitstext= "";
            $(".berriesFruits input:checked").each(function() {
                berriesfruitstext +="&emsp;" + $(this).next("label").find("p.option_name").text()+"<br/>";
            });
            $("#selectedBerriesFruits").html('<p style="border-left: 3px solid #f49a25;">' + berriesfruitstext + '</p>');
        });
    });
</script>
<script>
    $(".popuptext").click(function() {
        $(".popuptext").removeClass("show");
    });
    $("#selectedSeedsNutsContainer").click(function() {
    $("html, body").animate({
        scrollTop: $(".seedsNutsPopup").offset().top-200
    }, 1000);
    });
    $("#selectedBerriesFruitsContainer").click(function() {
    $("html, body").animate({
        scrollTop: $(".berriesFruitsPopup").offset().top-200
    }, 1000);
    });
    if(performance.navigation.type == 2){
        location.reload(true);
    }
</script>
<script type="text/javascript">
    var ajax_price = function() {
    	$.ajax({
    		type: "POST",
    		url: "index.php?route=product/live_options/index&product_id=<?php echo $product_id; ?>",
    		data: $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, #sticky input[name=\'quantity\']'),
    		dataType: "json",
    		success: function(json) {
    			if (json.success) {
			    	change_price("#main-price", json.new_price.price);
			    	change_price(".main-price-mobile", "Cart Value: " + json.new_price.price);
			    }
    		}
    	});
    }

    var change_price = function(id, new_price) {
    	$(id).html(new_price);
    }
    $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\'], #product input[type=\'checkbox\'], #product select, #product textarea, input[name=\'quantity\']').on("change", function() {
    	ajax_price();
    });
</script>
<script type="text/javascript"><!--
    $('#product input[type="radio"]').click(function() {
        $('#product input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
<?php echo $footer; ?>