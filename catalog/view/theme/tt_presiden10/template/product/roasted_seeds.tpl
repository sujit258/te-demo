<?php echo $header; ?>
<style>
body {
    font-size: 15px;
}
.product-banner, .product-1, .product-2 {
    border: 2px solid #d5dbdb;
    background: rgba(135, 149, 150, 0.095);
}
.product-1, .product-2 {
    margin-top: 0px!important;
    min-height: 375px;
    max-height: 375px;
    height: 100%;
}
.product-text {
    height: 100%;
    min-height: 375px;
    padding-right: 15px;
}
.product-banner, .product-data {
    min-height: 750px!important;
}
.product-banner {
    background-repeat: no-repeat;
    background-size: 101%, 100%;
}
.overlay {
    min-height: 190px;
    background: rgba(236, 238, 239, 1);
    padding: 15px;
    margin: 15px;
    width: 49%;
    color: #002f36;
}
body h4 {
    font-weight: 500;
    font-size: 17px;
    line-height: 22px;
    color: #002f36;
}
body h1 {
    font-weight: 700;
    color: #002f36;
    font-size: 25px;
}
.product-image img {
    margin-top: 10%;
}
.product-image {
    min-height: 375px;
    padding-left: 0px;
}
.top-banner {
    margin-top: 0!important;
    margin-bottom: 25px!important;
}
.top-banner img {
    width: 100%;
}
.breadcrumb {
    margin-bottom: 20px!important;
}
.top-image img {
    width: 100%;
}
.carousel-control span {
    font-size: 30px;
}
.top-banners {
    margin-bottom: 25px;
}
.content-vertical-center {
    margin-top: 15%;
    margin-left: 15px;
}
.buttons .btn-block {
    padding: 10px 16px!important;
}
.buttons .btn-block:active, .buttons .btn-block:focus {
    background: #f49a25!important;
}
@media(max-width:1000px) {
    .product-banner {
        min-height: 330px!important;
        background-size: 100%, 100%!important;
    }
    body h4 {
        font-size: 15px!important;
    }
    body h1 {
        font-size: 17px!important;
    }
    .overlay {
        min-height: 160px;
        width: 70%;
    }
    .product-image img {
        margin-top: 30%;
    }
    .product-image {
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .product-1, .product-2 {
        max-height: 320px;
        min-height: 320px;
    }
    .product-data {
        min-height: 1px !important;
    }
    .top-banner img {
        height: 140px!important;
    }
    .product-text, .product-image {
        min-height: 270px;
    }
    .buttons .btn-block {
        width: 35%!important;
        padding: 7px 16px;
    }
    .content-vertical-center {
        margin-top: 0!important;
        margin-left: 0!important;
    }
    #cart #cart-total {
        top: -5px;
    }
}
#cart #cart-total {
    top: -6px;
}
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container-fluid">
    <div class="row top-banner hidden-xs hidden-sm">
        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-top-banner.jpg" class="img-responsive" />
    </div>
    <div class="row top-banner hidden-md hidden-xl hidden-lg">
        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-top-banner-mobile.jpg" class="img-responsive" />
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <a href="<?php echo $antioxidant; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section-1.jpg');">
                </div>
            </a>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Antioxidant Mix, 125gm</h1>
                            <h4>Antioxidant Mix Seeds, an all-in-one crunchy delight for sudden snack cravings. Loaded with many health benefits and nutrients.</h4>
                            <h1>Rs 114 <small><strike>Rs 120</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $antioxidant; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section1-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $antioxidant; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section1-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>5-in-1 Super Seeds Mix</h1>
                            <h4>5-in-1 Super Seeds Mix as the name says, consists of five super seeds filled with a powerhouse of everyday essential nutrients.</h4>
                            <h1>Rs 114 <small><strike>Rs 120</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $super_seeds; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $super_seeds; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Roasted Pumpkin Seeds, 125gm</h1>
                            <h4>AAA graded premium quality seeds, with no nutritional compromise in it.  Excellent source of protein, dietary fibre, and minerals like zinc, iron, magnesium.</h4>
                            <h1>Rs 114 <small><strike>Rs 120</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $roasted_pumpkin; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section2-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $roasted_pumpkin; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section2-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Roasted Sunflower Seeds, 125gm</h1>
                            <h4>Sunflower Seeds are natural, jumbo sunflower seeds, roasted to perfection to satisfy your crispy snack cravings. They are an appetising yet guilt-free snack.</h4>
                            <h1>Rs 114 <small><strike>Rs 120</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $roasted_sunflower; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $roasted_sunflower; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
            <a href="<?php echo $roasted_pumpkin; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section-2.jpg');">
                </div>
            </a>
        </div>
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <a href="<?php echo $chilli_masala_pumpkin; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section-3.jpg');">
                </div>
            </a>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Protein Mix, 125gm</h1>
                            <h4>Protein Mix is a rich source of fibre, which provides you with the required amount of energy and freshness, keeping you active and energetic throughout.</h4>
                            <h1>Rs 114 <small><strike>Rs 120</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $protein_mix; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section3-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $protein_mix; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section3-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Chilli Masala Pumpkin 125gm</h1>
                            <h4>Light and crispy, with a warm, nutty flavour, these seeds when spiced up with chilli powder can make a lip-smacking hot, peppery on-the-go snack.</h4>
                            <h1>Rs 114 <small><strike>Rs 120</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $chilli_masala_pumpkin; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $chilli_masala_pumpkin; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Roasted Pumpkin Honey Clusters Seeds, 125gm</h1>
                            <h4>They are one of the essential travel-friendly snacks to be carried when running late to work and can be munched on whenever hunger strikes.</h4>
                            <h1>Rs 114 <small><strike>Rs 120</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $pumpkin_honey_cluster; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section4-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $pumpkin_honey_cluster; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section4-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Roasted Pumpkin Seeds Cheesy Onion, 125gm</h1>
                            <h4>They are flavoured with cheese. If cheese pleases your palate, these seeds can satisfy your cheese cravings in a super healthy way.</h4>
                            <!--<h4>Pumpkin Seeds are flavoured with cheese. If cheese pleases your palate, these seeds can satisfy your cheese cravings in a super healthy way.</h4>-->
                            <h1>Rs 114 <small><strike>Rs 120</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $pumpkin_cheesy_onion; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $pumpkin_cheesy_onion; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
            <a href="<?php echo $pumpkin_honey_cluster; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-rs-section-4.jpg');">
                </div>
            </a>
        </div>
    </div>
</div>
<?php echo $footer; ?>