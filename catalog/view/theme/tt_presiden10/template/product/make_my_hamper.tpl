<?php echo $header; ?>
<style>
div #sticky {
    position: -webkit-sticky;
    position: sticky;
    top: 105px;
    margin-top: 117px;
}
body {
    font-family: roboto, 'Oswald', sans-serif;
    font-size: 15px!important;
}
.checkbox input {
    display: none;
}

#product .control-label {
    font-size: 18px;
    font-weight: 600;
    color: #f49a25;
}

h6 {
    margin-bottom: 0px !important;
    margin-left: 15px !important;
    margin-top: 25px !important;
    line-height: 1.3 !important;
    padding-bottom: 10px !important;
}

#product h3 {
    text-align: center;
    color: #f49a25;
}

.checkbox label {
    padding: 0px!important;
    display: inline-block;
    position: relative;
    cursor: pointer;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-align: center;
    font-size: 15px;
    color: #f49a25;
    width:auto;
    min-height:300px!important;
}

label img {
    min-height: 100%!important;
    width: auto;
    transition-duration: 0.2s;
    transform-origin: 50% 50%;
    position: relative;
}

:checked + label img {
    transform: scale(0.9);
    z-index: -1;
    position:relative;
}

:checked + label {
    background-image:linear-gradient(rgba(0, 0, 0, 0.7), transparent);
}

.checkbox {
    display: inline-block;
    text-align: center;
    min-height: 290px;
    border: 1px solid rgb(28,0,85);
    margin: 30px 10px 30px!important;
    padding-right: 0;
    padding-left: 0;
    border-radius:10px;
    overflow: hidden;
    width: 30%;
}

@media screen and (max-width: 800px) {
    .googleplay-original {
        margin-top: 10px;
    }
    .checkbox {
        width: 44% !important;   
    }
    .priceOnMobile {
        display: inline-block!important;
        z-index: 1;
        background:rgba(255,255,255,0.9);
        width: 100%;
        position: fixed;
        top: 0px;
    }
    .main-price-mobile {
        position: relative;
        text-align: center;
    }
    .banner-container {
        margin-right: -15px;
        margin-left: -15px;
    }
    div #sticky {
        margin-top: 0px;
    }
    .taste-logo img {
        height: 25px!important;
    }
    .nav li a {
        padding: 10px 10px!important;
        font-size: 15px!important;
    }
    .hamper-types {
        margin-top: 25px;
    }
    .checkbox:hover p.tip {
        display: none!important;
    }
    .modal-dialog {
        top: 5%;
    }
}

.option_name {
    font-size: 15px;
    min-height: 40px;
}

.weight {
    font-size: 15px;
    color: #000000;
}

#product {
    max-height: 100%;
    margin-bottom: 0px;
}

.has-option {
    border: none;
    padding: 0px;
    box-shadow: none;
}

.option_help {
    color:#1C0055;
    font-size:15px;
    text-align:center;
}

#top_banner {
    padding-right: 0;
    padding-left: 0;
}

p {
    font-size: 15px;
    margin: 0;
}

.product-title-section {
    background-color: #A193A4;
    margin-top: 10px;
}

.product-title-subsection {
    background-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0),rgba(0, 0, 0, 0.5));
    padding: 45px 20px;
}

.product-title{
    font-size: 28px!important;
    margin-bottom: 0px!important;
    margin-top: 0px!important;
    color: #ffffff!important;
    text-transform: none!important;
}

.selected-options h3 {
    font-size: 17px!important;
    font-weight: 600!important;
    text-align:left!important;
    margin-top: 5px!important;
    margin-bottom: 5px!important;
    color:#000;
}

.selected-options {
    padding-top:10px!important;
}

#minus {
    border-top-left-radius: 25px!important;
    border-bottom-left-radius: 25px!important;
    border-right:none!important;
    font-size: 15px;
}

#plus {
    border-top-right-radius: 25px!important;
    border-bottom-right-radius: 25px!important;
    border-left:none!important;
    font-size: 15px;
}

#input-quantity {
    padding-left: 5px!important;
    padding-right: 5px!important;
    border-left:none!important;
    border-right:none!important;
    width: 90px!important;
}

#minus:hover, #plus:hover {
    color: #f49a25!important;
    background: #fff!important;
}

#minus,#plus,#input-quantity {
    height:50px!important;
}

.qty-product label {
    margin-top: 15px!important;
}

.qty-product-content {
    position: relative;
    padding-top: 15px;
    width:80%;
    margin:auto;
    float:right;
}

button[disabled] {
    cursor: not-allowed!important;
    background: #000;
}

.btn-block {
    border-radius: 25px;
    background: #f49a25;
    width: 100%;
}

.btn-oos {
    border-radius: 25px;
    background: #777!important;
    width: 100%;
    margin-bottom: 7px;
}

.text-total {
    font-size: 18px!important;
    color: #f49a25 !important;
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    font-weight: 800;
    display: table-cell;
}

.main-price {
    font-size: 40px!important;
    color: #f49a25 !important;
    display: table-cell;
}

.price-section {
    min-height: 60px;
    line-height: 0px!important;
}

.button:hover, .button:active, .button:focus {
    background: #000;
}
.button {
    font-size: 16px!important;
    padding: 10px 16px!important;
    font-weight: 600!important;
}
.disabled {
    background-image : linear-gradient(to bottom, rgb(241,241,241), transparent);
    cursor : no-drop;
}

p.tip {
    text-decoration: none;
    display:none;
    padding: 1px;
}

.checkbox:hover p.tip {
    display: block;
    border-radius: 6px;
    background-color: gold;
    position: absolute;
    top: 0px;
    color: #000;
    text-decoration: none;
    z-index:2;
    left:0;
}

/*:checked + label:hover p.tip, .disabled:hover p.tip {
    display: none;
}*/

.popuptext {
    position: relative;
    text-align: center;
    visibility: hidden;
    background: #f44336;
    color: #fff;
    padding: 5px 15px;
    width: 250px;
    left: calc( 50% - 125px);
    border-radius: 5px;
    cursor: pointer;
    -webkit-animation: updown 1.4s ease-in-out infinite;
    -moz-animation: updown 1.4s ease-in-out infinite;
    -o-animation: updown 1.4s ease-in-out infinite;
    animation: updown 1.4s ease-in-out infinite;
    top: 10px;
}

/* Popup arrow */
.popuptext::after {
    content: '';
    display: block;
    position: absolute;
    left: calc( 50% - 7px );
    width: 0px;
    text-align: center;
    height: 0;
    border-top: 14px solid #F44336;
    border-right: 14px solid transparent;
    border-bottom: 0 solid transparent;
    border-left: 14px solid transparent;
}

/* Toggle this class - hide and show the popup */
.show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
    display: inline-block!important;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}
#selectedElementsContainer {
    cursor: pointer;
}
#selectedElementsContainer h3{
    cursor: pointer;
    font-size: 17px!important;
    font-weight: 600!important;
    color: #000!important;
}

.priceOnMobile {
    display: none;
    padding: 10px;
}

.main-price-mobile {
    font-size: 15px!important;
    color: #f49a25 !important;
}

#remainingSelection {
    font-size: 15px!important;
}

.banner-container {
    margin: 0!important;
}
#top_banner img {
    width: 100vw!important;
    cursor: pointer;
}
.hamper-types {
    z-index: 10;
    margin-bottom: 10px;
    margin-top: 50px;
}
#selectedElements p {
    line-height: 1.2;   
}
.nav-pills li {
    text-align: center;
}
.nav-pills > li + li {
    margin-left: 0!important;
}
.nav a {
    font-weight: 600;
}
.nav a span {
    vertical-align: middle;
}
.taste-logo img {
    min-height: 25px!important;
    height: 30px;
    width: 30px;
    margin-bottom: 5px;
}
.nav li a {
    padding: 15px 10px;
    font-size: 18px;
}
.option-banner {
    margin-top: 15px;
}
.nav-pills li.active a {
    color: #fff!important;
    background-color: #f49a25!important;
}
.nav-pills li a {
    color: #000;
    border: 1px solid #f49a25;
}
.modal-body h4, .modal-title {
    font-weight: 600;
}
.modal-button {
    cursor: pointer;
    font-weight: 600;
    text-align: right;
    color: #f49a25;
}
.modal-dialog {
    top: 15%;
}
.modal-body {
    max-height: calc(100vh - 200px);
    overflow-y: auto;
}
#button-cart::before {
    font-family: 'Material-Design-Iconic-Font';
    display: inline-block;
    font-size: 16px;
    content: "\f1cb";
    margin-right: 10px;
    font-weight: 400;
}
.selection {
    font-weight: bold;
    color: #f49a25;
}
</style>
<div class="row banner-container">
    <div class="col-md-12 col-sm-12 col-xs-12" id="top_banner">
        <img alt="True Elements" class="img-responsive" src="image/catalog/images-08-19/banner/banner-make-my-hamper.jpg" />
    </div>
</div>
<div class="container">
  <div class="row">
    <div id="content" class="col-md-12 col-sm-12 col-xs-12">
      <div class="row">
		<div id="product" class="col-md-8 col-sm-12 col-xs-12">
		    <div class="priceOnMobile">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <p class="main-price-mobile">Cart Value: <?php echo $price; ?></p> 
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 hamper-types" role="tabpanel">
            <ul class="nav nav-pills nav-fill" role="tablist">
                <li role="presentation" class="nav active col-md-4 col-sm-4 col-xs-4">
                    <a href="#elements-content" aria-controls="elements-content" role="tab" data-toggle="tab"><span>Crispy Munchies</span></a>
                </li>
                <li role="presentation" class="nav col-md-4 col-sm-4 col-xs-4">
                    <a href="#pack-5-content" aria-controls="pack-5-content" role="tab" data-toggle="tab"><span>Exotic Indulgence</span></a>
                </li>
                <li role="presentation" class="nav col-md-4 col-sm-4 col-xs-4">
                  <a href="#pack-3-content" aria-controls="pack-3-content" role="tab" data-toggle="tab"><span>Indulgence Pack</span></a>
                </li>
            </ul>
            </div>
            
            <?php if ($options) { ?>
            <div class="tab-content">
            <?php foreach ($options as $option) { ?>
                        <?php if ($option['type'] == 'checkbox') { ?>
                        
            <?php if ($option['option_id'] == 17) { ?>
            <div class="form-group tab-pane active" id="elements-content" role="tabpanel">
              <img alt="True Elements" class="img-responsive option-banner" src="image/catalog/images-08-19/banner/banner-mmh-crispy-munchies.jpg" />
              <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Our Elements</h3>
              <div class="pull-right col-md-12 col-xs-6 col-sm-6">
                  <span class="modal-button pull-right" data-toggle="modal" data-target="#crispy-munchies">Learn More</span>
              </div>
              <!-- Modal -->
                <div class="modal fade" id="crispy-munchies" role="dialog">
                    <div class="modal-dialog">                  
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Crispy Munchies</h4>
                            </div>
                            <div class="modal-body">
                                <p>With different innovations in snacking and weird addictive food combinations, we totally understand your hunger pangs craving the need for crispy snacks after every 1 hour. While your stomach growls, you ultimately reach out to the nearest local junk stall for your spicy and crunchy hunger needs. But before you enter in the zone of destroying your health due to one bout of hunger,  we’ve got your back, as always.</p>
			                    <p>Being blessed with plenty of options for snacking & staying on track to your health needs, check out this specially curated Hamper of Crispy Munchies, prepared with desired crisp & crunch, enhanced with chatpata spices and seasonings, for your never-ending snack moods.</p>
			                    <h4>Usage Instructions:</h4>
			                    <p>&#9679; Perfect for spicing up your salads and soups.<br/>
			                       &#9679; Easy to blend or garnish in smoothies and bakery food items.<br/>
			                       &#9679; Great as an appetising snack.
			                    </p>
			                    <h4>Storage Information:</h4>
			                    <p>Store it in a cool, dry and dark place away from moisture and direct sunlight.</p>
			                    <h4>Allergen Information:</h4>
			                    <p>Packed in a facility that also handles Tree Nuts and Cereals containing Gluten.</p>
                            </div>
                        </div>
                    </div>
                </div>
              <p class="option_help col-md-12 col-xs-6 col-sm-6">(Select any <span class="selection">Seven</span>)</p>
              <span class="popuptext" id="elementsPopup">Please Select Atleast 7!</span>
              <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox col-md-3 col-sm-5 col-xs-5">
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="elements" disabled/>
                  <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="elements-image"/><br/><div style="min-height: 75px;"><p class="option_name"><?php echo $option_value['name']; ?></p><?php if ($option_value['taste_logo'] != NULL) { ?><span class="pull-left taste-logo col-md-3 col-xs-4 col-sm-4"><img alt="True Elements" src="<?php echo $option_value['taste_logo']; ?>" /></span><p class="weight col-md-9 col-xs-8 col-sm-8 pull-right" style="text-align: right;"><?php echo $option_value['weight']; ?> gm</p><?php } else { ?><p class="weight"><?php echo $option_value['weight']; ?> gm</p><?php } ?></div></label>
                  
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['option_id'] == 19) { ?>
            <div class="form-group tab-pane" id="pack-5-content" role="tabpanel">
              <img alt="True Elements" class="img-responsive option-banner" src="image/catalog/images-08-19/banner/banner-mmh-exotic-indulgence.jpg" />
              <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Our Elements</h3>
              <div class="pull-right col-md-12 col-xs-6 col-sm-6">
                  <span class="modal-button pull-right" data-toggle="modal" data-target="#exotic-indulgence">Learn More</span>
              </div>
              <p class="option_help col-md-12 col-xs-6 col-sm-6">(Select any <span class="selection">Five</span>)</p>
              <!-- Modal -->
                <div class="modal fade" id="exotic-indulgence" role="dialog">
                    <div class="modal-dialog">                  
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Exotic Indulgence</h4>
                            </div>
                            <div class="modal-body">
                                <p>When in a snack mood, if you eventually end up with a handful of the oil-fried regular, not-so-great snacks every day, are you really even a Snack person? The perfect snack pack isn’t always near, but we are!</p>
			                    <p>As far as the nutrients and health factor is taken into consideration (where any which way we are heading 1st in the race) the exotic blend of luscious berries, crispy roasted snacks with lip-smacking seasonings and desire to crave for more with every bite makes you spare some time to indulge and dive into the true elements of your foodie moments. It’s time you engross yourself into the Exotic Indulgence of tasty munching retreats and evolve into the world of guilt-free snacking.</p>
			                    <h4>Usage Instructions:</h4>
			                    <p>&#9679; Perfect for spicing up your salads and soups.<br/>
			                       &#9679; Easy to blend or garnish in smoothies and bakery food items.<br/>
			                       &#9679; Great as an appetising snack.
			                    </p>
			                    <h4>Storage Information:</h4>
			                    <p>Store it in a cool, dry and dark place away from moisture and direct sunlight.</p>
			                    <h4>Allergen Information:</h4>
			                    <p>Packed in a facility that also handles Tree Nuts and Cereals containing Gluten.</p>
                            </div>
                        </div>
                    </div>
                </div>
              <span class="popuptext" id="pack-5-Popup">Please Select Atleast 5!</span>
              <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox col-md-3 col-sm-5 col-xs-5">
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="pack-5" disabled/>
                  <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="pack-5-image"/><br/><div style="min-height: 75px;"><p class="option_name"><?php echo $option_value['name']; ?></p><?php if ($option_value['taste_logo'] != NULL) { ?><span class="pull-left taste-logo col-md-3 col-xs-4 col-sm-4"><img alt="True Elements" src="<?php echo $option_value['taste_logo']; ?>" /></span><p class="weight col-md-9 col-xs-8 col-sm-8 pull-right" style="text-align: right;"><?php echo $option_value['weight']; ?> gm</p><?php } else { ?><p class="weight"><?php echo $option_value['weight']; ?> gm</p><?php } ?></div></label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['option_id'] == 20) { ?>
            <div class="form-group tab-pane" id="pack-3-content" role="tabpanel">
              <img alt="True Elements" class="img-responsive option-banner" src="image/catalog/images-08-19/banner/banner-mmh-indulgence-pack.jpg" />
              <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Our Elements</h3>
              <div class="pull-right col-md-12 col-xs-6 col-sm-6">
                  <span class="modal-button pull-right" data-toggle="modal" data-target="#indulgence-pack">Learn More</span>
              </div>
              <p class="option_help col-md-12 col-xs-6 col-sm-6">(Select any <span class="selection">Three</span>)</p>
              <!-- Modal -->
                <div class="modal fade" id="indulgence-pack" role="dialog">
                    <div class="modal-dialog">                  
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Indulgence Pack</h4>
                            </div>
                            <div class="modal-body">
                                <p>Happiness is in gifting and being gifted, especially when it comes to food filled with fresh sweet vibes, crunch and a feeling of fullness.</p>
			                    <p>Bringing to you, a moment of sheer joy and happiness with all your “feeling good” requirements packed in a box, gratifying enough to make you feel loved and pampered.</p>
			                    <p>This authentic & indulgent gift hamper consists of delicious snacky retreats much-needed to uplift your mood and make your stomach and hearts go AWE. It features a mix of your favourite munchies, delectable for your taste buds and easily palatable to your hungry bellies.</p>
			                    <h4>Usage Instructions:</h4>
			                    <p>&#9679; Perfect for spicing up your salads and soups.<br/>
			                       &#9679; Easy to blend or garnish in smoothies and bakery food items.<br/>
			                       &#9679; Great as an appetising snack.
			                    </p>
			                    <h4>Storage Information:</h4>
			                    <p>Store it in a cool, dry and dark place away from moisture and direct sunlight.</p>
			                    <h4>Allergen Information:</h4>
			                    <p>Packed in a facility that also handles Tree Nuts and Cereals containing Gluten.</p>
                            </div>
                        </div>
                    </div>
                </div>
              <span class="popuptext" id="pack-3-Popup">Please Select Atleast 3!</span>
              <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox col-md-3 col-sm-5 col-xs-5">
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" class="pack-3" disabled/>
                  <label for="<?php echo $option_value['product_option_value_id']; ?>"><p class="tip"><?php echo $option_value['option_desc']; ?></p><img alt="True Elements" src="<?php echo $option_value['image']; ?>" class="pack-3-image"/><br/><div style="min-height: 75px;"><p class="option_name"><?php echo $option_value['name']; ?></p><?php if ($option_value['taste_logo'] != NULL) { ?><span class="pull-left taste-logo col-md-3 col-xs-4 col-sm-4"><img alt="True Elements" src="<?php echo $option_value['taste_logo']; ?>" /></span><p class="weight col-md-9 col-xs-8 col-sm-8 pull-right" style="text-align: right;"><?php echo $option_value['weight']; ?> gm</p><?php } else { ?><p class="weight"><?php echo $option_value['weight']; ?> gm</p><?php } ?></div></label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            </div>
            <?php } ?>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
          
          <!--Main Section-->
          <div class="col-md-4 col-sm-12 col-xs-12" id="sticky">
              <div class="product-title-section">
                <div class="product-title-subsection">
			        <h2 class="product-title"><?php echo $heading_title; ?></h2>
			    </div>
			  </div>
			  <div class="selected-options">
			      <?php if ($options) { ?>
                    <?php foreach ($options as $option) { ?>
                        <?php if ($option['option_id'] == 17) { ?>
                            <h3 class="col-md-6" id="selectedElementsContainer">Our Elements</h3><span id="remainingSelection"></span>
                            <h4 id="selectedElements"></h4>
                        <?php } ?>
                    <?php } ?>
                  <?php } ?>
			  </div>
			<div class="form-group">
				<div class="qty-product">
				    <div class="qty-product-content">
				        <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
					    <input type="button" id="minus" value="-" class="form-control" />
					    <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
					    <input type="button" id="plus" value="&#43;" class="form-control"/>
					    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
				    </div>
					
				</div>
				<div class="price-section">
			    <?php if ($price) { ?>
			        <p class="text-total">TOTAL</p>
			        <p class="main-price" id="main-price"><?php echo $price; ?></p>
			    <?php } ?>
			    </div>
			    <?php if ($quantity == 0.00) { ?>
			        <button class="btn-oos button" type="button" disabled>Stock Over!</button>
			        <br/>
			    <?php } else { ?>
				    <button class="button btn-block" type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_cart; ?></button>
				<?php } ?>
				<center style="margin-top:8px;"><a href="mailto:marketing@true-elements.com?Subject=Bulk%20discount" target="_top" style="color:#FDAD00;">Contact us</a> for bulk discount.</center>
			</div>
			<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
			</div>
      </div>
	  <?php echo $content_bottom; ?>
	  </div>
	</div>
</div>
<script type="text/javascript"><!--
$('.nav li').click(function() {
    $('input:checkbox').removeAttr('checked');
    $(".checkbox input").prop("disabled", false);
    $('input:checkbox:not(:checked)').next().find('.elements-image').css({
        'cursor' : 'pointer'
    });
    $('input:checkbox:not(:checked)').parent().removeClass("disabled");
    $('input:checkbox:not(:checked)').parent().css({'box-shadow' : 'none'});
    $("#selectedElements").html("");
    $('#remainingSelection').html('<span></span>');
    $('.popuptext').removeClass('show');
    ajax_price();
});
</script>
<script type="text/javascript"><!--
var minimum = <?php echo $minimum; ?>;
  $("#input-quantity").change(function(){
    if ($(this).val() < minimum) {
      $("#input-quantity").val(minimum);
      ajax_price();
    }
  });
  // increase number of product
  function minus(minimum){
      var currentval = parseInt($("#input-quantity").val());
      $("#input-quantity").val(currentval-1);
      if($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum){
          $("#input-quantity").val(minimum);
     }
     ajax_price();
  };
  // decrease of product
  function plus(){
      var currentval = parseInt($("#input-quantity").val());
     $("#input-quantity").val(currentval+1);
     ajax_price();
  };
  $('#minus').click(function(){
    minus(minimum);
  });
  $('#plus').click(function(){
    plus();
  });

//--></script>
<script type="text/javascript">
$('#button-cart').click(function() {
    if (checkboxCount()) {
	    $.ajax({
		    url: 'index.php?route=checkout/cart/add',
		    type: 'post',
		    data: $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, #sticky input[name=\'quantity\']'),
		    dataType: 'json',
		    beforeSend: function() {
		    	$('#button-cart').button('loading');
		    },
		    complete: function() {
		    	$('#button-cart').button('reset');
		    },
		    success: function(json) {
		    	$('.alert, .text-danger').remove();
		    	$('.form-group').removeClass('has-error');
			    if (json['error']) {
			    	if (json['error']['option']) {
			    		for (i in json['error']['option']) {
			    			var element = $('#input-option' + i.replace('_', '-'));
			    			if (element.parent().hasClass('input-group')) {
			    				element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
			    			} else {
			    				element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
			    			}
			    		}
			    	}
				    if (json['error']['recurring']) {
				    	$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				    }
				    // Highlight any found errors
				    $('.text-danger').parent().addClass('has-error');
			    }
			    if (json['success']) {
			        var success_text = '<div style="margin: 0 0 10px 0">\
                    <div style="float: left"><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div>\
                    <div  style="text-align: center" >' + json['success'] + '</div>\
                    </div>';

                    var np = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button'
                    }).show();
			    	
				    $('#cart-total').html(json['total']);
				    
				    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
			    }
		    },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
	    });
    }
});

    function checkboxCount() {
        var elementsinputList = $(".elements");
        var elementsnumChecked = 0;
        for (var i = 0; i < elementsinputList.length; i++) {
            if (elementsinputList[i].type == "checkbox" && elementsinputList[i].checked) {
                elementsnumChecked = elementsnumChecked + 1;
            }
        }
        
        var pack5inputList = $(".pack-5");
        var pack5numChecked = 0;
        for (var i = 0; i < pack5inputList.length; i++) {
            if (pack5inputList[i].type == "checkbox" && pack5inputList[i].checked) {
                pack5numChecked = pack5numChecked + 1;
            }
        }
        
        var pack3inputList = $(".pack-3");
        var pack3numChecked = 0;
        for (var i = 0; i < pack3inputList.length; i++) {
            if (pack3inputList[i].type == "checkbox" && pack3inputList[i].checked) {
                pack3numChecked = pack3numChecked + 1;
            }
        }
        
        if(elementsnumChecked == 7) {
            return true;
        } else if(pack5numChecked == 5) {
            return true;
        } else if(pack3numChecked == 3) {
            return true;
        }
        
        if (elementsnumChecked > 0 && elementsnumChecked < 7) {
            $('#elementsPopup').addClass('show');
        }
        
        if (pack5numChecked > 0 && pack5numChecked < 5) {
            $('#pack-5-Popup').addClass('show');
        }
        
        if (pack3numChecked > 0 && pack3numChecked < 3) {
            $('#pack-3-Popup').addClass('show');
        }
        
        if(elementsnumChecked == 0 && pack5numChecked == 0 && pack3numChecked == 0) {
            $('.active .popuptext').addClass('show');
        }
        
        $('html, body').animate({
                scrollTop: $(".tab-content").offset().top+250
        }, 1000);
        
        return false;
    }
</script>
    <script>
$(document).ready(function() {
    if (window.history.replaceState) {
        window.history.replaceState("Make My Hamper", "Make My Hamper", "/makemyhamper");
    }
    //After Page Load Enable All Checkboxes
    $(".checkbox input").prop("disabled", false);
    
    //Pack of 7
    $(".elements").on("click", function() {
        $('input.elements:checkbox:checked').parent().css({'box-shadow' : 'inset 0px 0px 0px 2px rgb(28,0,85)'});
        $('input.elements:checkbox:not(:checked)').parent().css({'box-shadow' : 'none'});
        
        var numberOfChecked = $('input.elements:checkbox:checked').length;
        var remainingSelection = 7 - numberOfChecked;
        $('#remainingSelection').html('<span>(' + remainingSelection + ' Items Remaining)</span>');
        if (numberOfChecked == 7) {
             $("input.elements:checkbox:not(:checked)").prop("disabled", true);
             $('input.elements:checkbox:not(:checked)').parent().addClass("disabled");
            $('input.elements:checkbox:not(:checked)').next().find('.elements-image').css({
                'cursor' : 'no-drop',
                'z-index' : '-1'
            });
            $('#elementsPopup').removeClass('show');
        }
        else {
            $(".checkbox .elements").prop("disabled", false);
            $('input.elements:checkbox:not(:checked)').next().find('.elements-image').css({
                'cursor' : 'pointer'
            });
            $('input.elements:checkbox:not(:checked)').parent().removeClass("disabled");
        }
    });
    
    //Pack of 5
    $(".pack-5").on("click", function() {
        $('input.pack-5:checkbox:checked').parent().css({'box-shadow' : 'inset 0px 0px 0px 2px rgb(28,0,85)'});
        $('input.pack-5:checkbox:not(:checked)').parent().css({'box-shadow' : 'none'});
        
        var numberOfChecked = $('input.pack-5:checkbox:checked').length;
        var remainingSelection = 5 - numberOfChecked;
        $('#remainingSelection').html('<span>(' + remainingSelection + ' Items Remaining)</span>');
        if (numberOfChecked == 5) {
             $("input.pack-5:checkbox:not(:checked)").prop("disabled", true);
             $('input.pack-5:checkbox:not(:checked)').parent().addClass("disabled");
            $('input.pack-5:checkbox:not(:checked)').next().find('.pack-5-image').css({
                'cursor' : 'no-drop',
                'z-index' : '-1'
            });
            $('#pack-5-Popup').removeClass('show');
        }
        else {
            $(".checkbox .pack-5").prop("disabled", false);
            $('input.pack-5:checkbox:not(:checked)').next().find('.pack-5-image').css({
                'cursor' : 'pointer'
            });
            $('input.pack-5:checkbox:not(:checked)').parent().removeClass("disabled");
        }
    });
    
    //Pack of 3
    $(".pack-3").on("click", function() {
        $('input.pack-3:checkbox:checked').parent().css({'box-shadow' : 'inset 0px 0px 0px 2px rgb(28,0,85)'});
        $('input.pack-3:checkbox:not(:checked)').parent().css({'box-shadow' : 'none'});
        
        var numberOfChecked = $('input.pack-3:checkbox:checked').length;
        var remainingSelection = 3 - numberOfChecked;
        $('#remainingSelection').html('<span>(' + remainingSelection + ' Items Remaining)</span>');
        if (numberOfChecked == 3) {
             $("input.pack-3:checkbox:not(:checked)").prop("disabled", true);
             $('input.pack-3:checkbox:not(:checked)').parent().addClass("disabled");
            $('input.pack-3:checkbox:not(:checked)').next().find('.pack-3-image').css({
                'cursor' : 'no-drop',
                'z-index' : '-1'
            });
            $('#pack-3-Popup').removeClass('show');
        }
        else {
            $(".checkbox .pack-3").prop("disabled", false);
            $('input.pack-3:checkbox:not(:checked)').next().find('.pack-3-image').css({
                'cursor' : 'pointer'
            });
            $('input.pack-3:checkbox:not(:checked)').parent().removeClass("disabled");
        }
    });

});
</script>
<script>
$('.elements, .pack-5, .pack-3').click(function() {
    var elementstext= "";
    $('.elements:checked, .pack-5:checked, .pack-3:checked').each(function() {
        elementstext+='&emsp;'+$(this).next('label').find('p.option_name').text()+'<br/>';
    });
    $("#selectedElements").html('<p style="border-left: 3px solid #f49a25;">'+elementstext+'</p>');
});
</script>
<script>
    $(".popuptext").click(function() {
        $('.popuptext').removeClass('show');
    });
    $("#selectedElementsContainer").click(function() {
    $('html, body').animate({
        scrollTop: $(".tab-content").offset().top+250
    }, 1000);
    });
    if(performance.navigation.type == 2){
        location.reload(true);
    }
</script>
<script type="text/javascript">
var ajax_price = function() {
	$.ajax({
		type: 'POST',
		url: "index.php?route=product/live_options/index&product_id=<?php echo $product_id; ?>",
		data: $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, #sticky input[name=\'quantity\']'),
		dataType: 'json',
		success: function(json) {
			if (json.success) {
				change_price('#main-price', json.new_price.price);
				change_price('.main-price-mobile', "Cart Value: " + json.new_price.price);
			}
		}
	});
}

var change_price = function(id, new_price) {
	$(id).html(new_price);
}
$('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\'], #product input[type=\'checkbox\'], #product select, #product textarea, input[name=\'quantity\']').on('change', function() {
	ajax_price();
});
</script>
<?php echo $footer; ?>