<?php echo $header; ?>
<style type="text/css">
     
    h2{
        text-align:center;
    }
    
    .catboxx {
        padding: 0; 
        padding-bottom: 0px;
        padding-bottom: 10%;
    }
    
    .productImg {
        left: 5%;
    }
    
    .catboxx {
        /*width: 100%;*/
        float: left;
        text-align: center;
        background: #fff;
        border-radius: 15px; 
        margin: 0 0px;
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -ms-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
    }
    
    .catboxx{
        border-top-left-radius: 50px 20px;
    }
    
    .catbox{
        margin-bottom: 2%;
        min-height: 220px;
    }
    
    .productImg {
        width: 100%;
        float: left;
        position: relative;
        left: 0%;
        margin: 0 0 5px 0;
    }
    
    .productImg img {
        width: 80%;
        float: left;
    }
    
    a:focus {
        color: #000;
    }
    
    .h3 {
        font-size: 1.6em;
        color: #676767;
        text-align:center;
        margin: 10px 0;
        width: 100%;
        float: left;
    }
       
    
    @media only screen and (min-width:1100px){
        
    }    
    
    @media only screen and (min-width:1100px){
        
    }
    
    @media only screen and (min-width:1023px){
        
    }
    
    @media only screen and (max-width:600px){
        
        .inline-photo {
            min-height: 205px;
        }
        
        .inline-photo2 {
            min-height: 205px;
        }
    }
    
    @media only screen and (max-width:360px){
        
    }
    
    @media only screen and (min-width:1366px){
        .productImg {
            width: 90%;
            left: 12%;
        }
    }

    .inline-photo {
      opacity: 0;
      transform: translateY(4em) rotateZ(-0deg);
      transition: transform 4s .25s cubic-bezier(0,1,.3,1),opacity .3s .25s ease-out;
      will-change: transform, opacity;
    }
    
    .inline-photo.is-visible {
      opacity: 1;
      transform: rotateZ(-0deg);
    }
    
    .inline-photo22 {
      opacity: 0;
      transform: translateX(-15em) rotateZ(-0deg);
      transition: transform 4s .25s cubic-bezier(0,1,.3,1),opacity .3s .25s ease-out;
      will-change: transform, opacity;
    }
    
    .inline-photo2.is-visible2 {
      opacity: 1;
      transform: rotateZ(-0deg);
    }
    
    .inline-photo2 {
      opacity: 0;
      transform: translateX(15em) rotateZ(-0deg);
      transition: transform 4s .25s cubic-bezier(0,1,.3,1),opacity .3s .25s ease-out;
      will-change: transform, opacity;
    }
    .inline-photo2.is-visible3 {
      opacity: 1;
      transform: rotateZ(-0deg);
    }    

    @keyframes animate{
      0%{
        transform: translatez(-100px);
        width: 100%;
        height: 100%;
      }
      25%{
        transform: translatez(100px);
        width: 100%;
        height: 100%;
      }
      50%{
        transform: translatez(100px);
        width: 35%;
        height: 35%;
      }
      75%{
        transform: translatez(-100px);
        width: 35%;
        height: 35%;
      }
      100%{
        transform: translatez(-100px);
        width: 100%;
        height: 100%;
      }
    } 

    @-webkit-keyframes animatebottom {
      from { bottom:0px; opacity:0 } 
      to { bottom:0px; opacity:1 }
    }

    @keyframes animatebottom { 
      from{ bottom:0px; opacity:0 } 
      to{ bottom:0; opacity:1 }
    } 

    @keyframes opaqTransition {
        3% { transform: translateX( 0 ); }
        25% { transform: translateX( 0 ); }
        28% { transform: translateX( -700px ); }
        100% { transform: translateX( -700px ); }
    }

    /* ZOOM ANIMATION */
    .animate {
        -webkit-animation: animatezoom 0.6s;
        animation: animatezoom 0.6s
    }

    @-webkit-keyframes animatezoom {
        from {-webkit-transform: scale(0)} 
        to {-webkit-transform: scale(1)}
    }

    @keyframes animatezoom {
        from {transform: scale(0)} 
        to {transform: scale(1)}
    }
</style>

<style>
   
  .productWrap ul.productListUL {
      width: calc(100% + 20px);
       
  }

  ul.productListUL, ul.productListUL li {
      width: 100%;
      float: left;
  }
  .productListUL {
      cursor: grab;
  } 
  ul.productListUL li .catboxx {
      padding: 0;
       
  }

  ul.productListUL li .catboxx {
      width: 100%;
      float: left;
      text-align: center;
      background: #fff;
      border-radius: 15px; 
      margin: 0 0px;
      -webkit-transition: all .3s ease-in-out;
      -moz-transition: all .3s ease-in-out;
      -ms-transition: all .3s ease-in-out;
      -o-transition: all .3s ease-in-out;
      transition: all .3s ease-in-out;
      min-height: 220px;
  }
   
   
  ul.productListUL li .productImg {
       
      float: left;
      position: relative; 
      margin: 0 0 5px 0;
  }
  ul.productListUL li .productImg img, ul.productListUL, ul.productListUL li {
      width: 100%;
      float: left;
  }

  @media only screen and (max-width:600px){
      .productWrap ul.productListMain > li {
          width: 50%;
          padding: 3px;
          min-height: 221px;
      }
    
      ul{
        margin-left:-14%;
      }
      
      .productWrap ul.productListUL {
          width: calc(100% + 52px);
      }
      
      
        .productImg {
            left: 12%;
        }
  }

  @media only screen and (min-width:640px){
    article {
      padding: 40px 0;
    }
    
  }

  @media only screen and (max-width:360px){
     
   
  }

  @media only screen and (min-width:1100px){

    .middleContainer {
        width: 960px;
    }
    .productWrap ul.productListMain > li {
          width: 25%;
          padding: 5px 10px 15px 10px;
      }
  }

  @media only screen and (min-width:1024px){

    .middleContainer {
        width: 960px;
        margin: 0 auto;
        float: none;
    }
  }

  @media only screen and (min-width:1366px){
    article {
      padding: 50px 0;
    }
  }

  @media only screen and (max-width:1023px){
    .productWrap .middleContainer {
      width: 90%;
      margin: 0 auto;
      display: table;
      float: none;
    }
  }
  
  	/*SECTION4*/
	.section4{
		width:100%;
		height:60vh;
		border-radius:5px;
		text-align:center;
		background-image: url(https://www.true-elements.com/image/cachewebp/catalog/Banner/desktop/mmm-home-slider-desktop-1920x640.webp);
		background-size:cover;
		padding:30px;
	}

	.section4 .sub-title{
		color:#000;
	}
	
	.heading{
	    margin-bottom: 7%;
	}

    .boddy{
        background: #f7f7f7;
        clear: both;
        margin-top: 2%;
    }
    
    h2 {
        position: relative;
        padding: 0;
        margin: 0;
        font-family: "Raleway", sans-serif;
        font-weight: 400;
        font-size: 20px !important;
        color: #080808;
         -webkit-transition: all 0.4s ease 0s;
        -o-transition: all 0.4s ease 0s;
        transition: all 0.4s ease 0s;
    }
    
    
    /* === HEADING STYLE #1 === */
    .one h2 {
        text-align: center;
        text-transform: uppercase;
        padding-bottom: 5px;
        padding-top: 5px;
        margin-bottom: 42px !important;
        margin: 20px;
    }
    
    .one h2:before {
        width: 28px;
        height: 5px;
        display: block;
        content: "";
        position: absolute;
        bottom: 3px;
        left: 50%;
        margin-left: -14px;
        background-color: #f49a25;
    }
    
    .one h2:after {
        width: 100px;
        height: 1px;
        display: block;
        content: "";
        position: relative;
        margin-top: 25px;
        left: 50%;
        margin-left: -50px;
        background-color: #f49a25;
    }

</style>

<ul class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  <?php } ?>
</ul>

<div class=" ">
    <div class="top-banner hidden-xs hidden-sm">
        <div class="category-product-carousel w3-content w3-section">
            <a href="/index.php?route=checkout/cart&product_id=6450&option_id=1&option_value_id=123"><img alt="True Elements" class="mySlides" src="<?php echo $slider_jowar_flakes_desktop; ?>" class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=6462&option_id=1&option_value_id=31"><img alt="True Elements" class="mySlides" src="<?php echo $slider_crunchy_nuts_berries_muesli_desktop; ?>" class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=5057&option_id=1&option_value_id=113"><img alt="True Elements" class="mySlides" src="<?php echo $shop_by_category_desktop_2; ?>"  class="img-responsive"></a>
		</div>

        <script type="text/javascript">
            $(document).ready(function() {
                $(".category-product-carousel").owlCarousel({
                    autoPlay: true,
                    items : 1,
                    slideSpeed : 1000,
                    navigation : true,
                    pagination : true,
                    controls: true,
                    stopOnHover : false,
                    itemsDesktop : [1199,1],
                    itemsDesktopSmall : [991,1],
                    itemsTablet: [700,1],
                    itemsMobile : [400,1],
                });
            });
        </script>
    </div>
    <div class="top-banner hidden-md hidden-xl hidden-lg" style="margin-top: 5%;">
        <div class="category-product-mobile-carousel w3-content w3-section">
            <a href="/index.php?route=checkout/cart&product_id=6450&option_id=1&option_value_id=123"><img alt="True Elements" class="mySlides" src="<?php echo $slider_jowar_flakes_mobile; ?>"  class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=6462&option_id=1&option_value_id=31"><img alt="True Elements" class="mySlides" src="<?php echo $slider_crunchy_nuts_berries_muesli_mobile; ?>" class="img-responsive"></a>
            <a href="/index.php?route=checkout/cart&product_id=5057&option_id=1&option_value_id=113"><img alt="True Elements" class="mySlides" src="<?php echo $shop_by_category_mobile_2; ?>"  class="img-responsive"></a>
		</div>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".category-product-mobile-carousel").owlCarousel({
                    autoPlay: false,
                    items : 1,
                    slideSpeed : 1000,
                    navigation : true,
                    pagination : true,
                    controls: true,
                    stopOnHover : false,
                    itemsDesktop : [1199,1],
                    itemsDesktopSmall : [991,1],
                    itemsTablet: [700,1],
                    itemsMobile : [400,1],
                });
            });
        </script>
    </div>
      
      
    <!--<?php if ($thumb || $description) { ?>
		<div class="category-info">
			<?php if ($thumb) { ?>
				<div class="category-img"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
			<?php } ?>
		</div>
    <?php } ?>-->

<div class="boddy">    
    <div class="container">
        <div class="row">
            
            <!--div class="row heading">
                <h2><?php echo $heading_title; ?></h2> 
            </div-->
            
            <div class="col-md-12">
                <?php foreach ($shop_categoris as $shop_category) { ?>
                    <div class="row"> 
                        <div class="one">
                            <h2><?php echo $shop_category['name']; ?></h2>
                        </div>
                        <?php if ($categories) { ?>
                            <div>
                                <?php foreach ($categories as $category) { ?>
                                    <?php if ($shop_category['shop_by_category_id'] == $category['shop_by_category_id']) { ?>
                                        <div class="col-md-3 col-sm-6 col-xs-6 catbox">
                                            <div class="inline-photo show-on-scroll"> 
                                                <a href="<?php echo $category['href']; ?>" class="productImg"> <img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>"> </a>
                                                    <span class="h3"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a> </span>
                                            </div>
                                        </div> 
                                    <?php } ?>    
                                <?php } ?> 
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                   
            </div>
        </div>
    </div>
</div>
</div>
<!--SECTION4-->
<!--section class="section4">
<!--<h1 class="title">FLEXIBLE PLANS</h1>
<h5 class="sub-title">Skip & Cancel anytime - only order what you want. No commitments.</h5>
<a href="#" class="btn1">ORDER NOW</a> -->
<!--/section-->



<script type="text/javascript">
  //*********************LEVIOOSA ONLINE GROCERY BOX*********************// 
  
    $(document).ready(function(){
        pagenum = 1;
        function AutoRotate() {
           var myele = null;
           var allElements = document.getElementsByTagName('label');
           for (var i = 0, n = allElements.length; i < n; i++) {
             var myfor = allElements[i].getAttribute('for');
             if ((myfor !== null) && (myfor == ('slide_2_' + pagenum))) {
               allElements[i].click();
               break;
             }
           }
           if (pagenum == 4) {
             pagenum = 1;
           } else {
             pagenum++;
           }
        }
        setInterval(AutoRotate, 5000);
    });
      
       
    
    //SCROLL ANIMATE
    var scroll = window.requestAnimationFrame ||
                 function(callback){ window.setTimeout(callback, 1000/500)};
    var elementsToShow = document.querySelectorAll('.show-on-scroll'); 
    function loop() {
    
        Array.prototype.forEach.call(elementsToShow, function(element){
          if (isElementInViewport(element)) {
            element.classList.add('is-visible');
          } else {
            element.classList.remove('is-visible');
          }
        });
    
        scroll(loop);
    }
    loop();
     
    
    
    
    //SCROLL ANIMATE
    var scroll2 = window.requestAnimationFrame ||
                 function(callback2){ window.setTimeout(callback2, 1000/20)};
    var elementsToShow2 = document.querySelectorAll('.show-on-scroll2'); 
    function loop2() {
    
        Array.prototype.forEach.call(elementsToShow2, function(element){
          if (isElementInViewport(element)) {
            element.classList.add('is-visible2');
          } else {
            element.classList.remove('is-visible2');
          }
        });
    
        scroll2(loop2);
    }
    loop2();
     
    
    
    
    //SCROLL ANIMATE
    var scroll3 = window.requestAnimationFrame ||
                 function(callback3){ window.setTimeout(callback3, 1000/830)};
    var elementsToShow3 = document.querySelectorAll('.show-on-scroll3'); 
    function loop3() {
    
        Array.prototype.forEach.call(elementsToShow3, function(element){
          if (isElementInViewport(element)) {
            element.classList.add('is-visible3');
          } else {
            element.classList.remove('is-visible3');
          }
        });
    
        scroll3(loop3);
    }
    loop3();
    
    function isElementInViewport(el) {
      // special bonus for those using jQuery
      if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
      }
      var rect3 = el.getBoundingClientRect();
      return (
        (rect3.top <= 0
          && rect3.bottom >= 0)
        ||
        (rect3.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
          rect3.top <= (window.innerHeight || document.documentElement.clientHeight))
        ||
        (rect3.top >= 0 &&
          rect3.bottom <= (window.innerHeight || document.documentElement.clientHeight))
      );
    }

</script>


<?php echo $footer; ?>
