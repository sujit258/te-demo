<style>
.coupon {
        min-height: 15px!important;
        text-align: left;
        font-size: 11px!important;
        line-height: 15px!important;
        margin-bottom: 0!important;
    }
@media screen and (max-width: 500px) {
    .pro_img {
        width: 58%;
        display: inline;}
    }
    .item-inner .price-old {
        margin-left: 0!important;
    }
    .recipe-title h4 a:hover, .blog-container h4 a:hover {
        color:#f49a25;
    }
    .recipe-title h4 a, .blog-container h4 a {
        color: #222;
        font-weight:600;
        padding-top: 5px;
        line-height: 20px!important;
    }
    .recipe-container {
        min-height: 185px!important;
    }
    .blog-container {
        min-height: 210px!important;
    }
    .box-price {
        padding-bottom: 14px;
    }
    .des-container{
        min-height: 136px;
        padding: 0!important;
    }
    .options {
        min-height: 70px!important;
    }
    .product-name {
        min-height: 55px;
        padding-right: 0!important;
        padding-top: 10px!important;
        padding-bottom: 5px!important;
    }
    .product-options {
        margin-top: 10px;
        text-align: center;
        float: left;
        background: #fff!important;
    }
    .product-container {
        min-height: 570px!important;
    }
    .button-cart {
        float: right!important;
        font-size: 13px!important;
        padding: 8px 10px!important;
    }
    .button-personalize {
        font-size: 13px!important;
        padding: 8px 10px!important;
        float: right !important;
    }
    .price-label {
        padding: 8px 0px!important;
        width: 100%!important;
        min-height: 45px!important;
    }
    .quantity {
        height: 35px!important;
        width: 60%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    
    @media(max-width: 1000px) {
        .coupon {
            text-align: left!important;
        }
        .button-cart, .button-personalize {
            padding: 7px 10px !important;
            font-size: 14px!important;
            margin-bottom: 15px!important;
        }
        .deals-overlay {
            padding: 4px!important;
        }
        .product-container {
            min-height: 460px!important;
            padding: 7px!important;
        }
        .quantity-container {
            width: 60%!important;
        }
        .recipe-container {
            min-height: 140px!important;
        }
        .blog-container {
            min-height: 170px!important;
        }
        .radio label span {
            font-size: 12px!important;
        }
        .des-container .options .radio {
            margin: 0px 0px 5px -2px!important;
        }
        .options {
            min-height: 55px!important;
        }
        .search-products {
            padding: 0!important;
        }
        .searchtopul {
            width: 90%!important;
        }
    }
    
    @media(min-width: 1000px) {
        .des-container {
            min-height: 200px;
        }
        .searchtopul {
            width: 25%!important;
            padding-left: 15px!important;
        }
    }
    
    .searchtopul li {
        list-style-type: disc;
        text-align: left;
    }
    
    .box-price {
        padding-bottom: 0px!important;
    }
    .oos {
        background: #777!important;
        border-radius: 4px!important;
        float: right!important;
        font-size: 13px!important;
        padding: 8px 10px!important;
    }
    .oos:hover {
        cursor: not-allowed!important;
    }
    .button-cart:focus, .button-personalize:focus {
        color: #fff!important;
    }
    .deals-overlay {
        right: 0px;
        top: 10px;
        position: absolute;
        background:#f49a25;
        color:#fff;
        font-weight: 600;
        width: 25%;
        text-align: center;
        z-index: 1;
        font-size: 12px!important;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 23px!important;
        padding: 9px 10px!important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .mobile {
        margin-left: 0px;
        margin-bottom: 10px;
        min-height: 340px!important;
    }
    .personalized {
        min-height: 1px!important;
    }
    .quantity-container {
        width: 50%;
    }
    #tabs .nav-tabs li.active a {
        color: #f49a25!important;
    }
    #tabs .nav-tabs li a {
        color:#000;
    }
    #tabs .nav-tabs li:hover a{
        color:#f49a25!important;
    }
    .col-3 .nav-tabs li a {
        padding: 0 0 10px!important;
    }
    .des-container .options .radio input {
        display: none !important;
    }
    .des-container .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .des-container .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    .des-container .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    .des-container .text-danger {
        font-size: 12px;
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .price, .coupon {
        padding-left: 0px!important;
        padding-right: 0px!important;
    }
    .image1, .image2 {
        padding: 5%!important;
    }
    .price {
        min-height: 30px!important;
    }
    .coupon {
        min-height: 20px!important;
    }
    
    .deals-discount {
        border-radius: 50%;
        right: 5px;
        top: 0%;
        position: absolute;
        background: #f49a25;
        text-align: center;
        z-index: 5;
        width: 50px;
    }
    
    .deals-discount h5 {
        color: #fff;
        font-weight: 600;
        padding: 1px;
    }
    
    .price-coupon{
        font-weight: bold;
    }  
</style>

<?php echo $header; ?>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
    
<div class="col" id="tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-products" data-toggle="tab">Products</a></li>
        
        <li><a href="#tab-blogs" data-toggle="tab">Blogs</a></li>
        
        <li><a href="#tab-recipes" data-toggle="tab">Recipes</a></li>
        
      </ul>
    <div class="tab-content" style="overflow: hidden;">  
      <div class="custom-category tab-pane active" id="tab-products">
        <?php if ($products) { ?>
            <input type="hidden" name="product_id" value="" />
                <!--<h2>Showing	<?php
			 $count1 = substr($results,19);
			 $result = substr($count1, 0, -9);
		     echo $result; ?> Products</h2> -->
                
                <div class="toolbar">
					<div class="toolbar1">
                        <div class="btn-group">
                            <button type="button" id="list-view" class="btn btn-default"></button>
                            <button type="button" id="grid-view" class="btn btn-default"></button>
                        </div>
                    </div>
                    <div class="toolbar2">
                        <select id="input-sort" class="form-control" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $text_sort; ?>&nbsp;<?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $sorts['href']; ?>"><?php echo $text_sort; ?>&nbsp;<?php echo $sorts['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
					<div class="toolbar3">
                        <select id="input-limit" class="form-control" onchange="location = this.value;">
                            <?php foreach ($limits as $limits) { ?>
                            <?php if ($limits['value'] == $limit) { ?>
                            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $text_limit; ?>&nbsp;<?php echo $limits['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $limits['href']; ?>"><?php echo $text_limit; ?>&nbsp;<?php echo $limits['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-md-12 col-xs-12 col-sm-12 search-products">
                    <?php foreach ($products as $product) { ?>
                        <div class="col-md-3 col-xs-6 col-sm-4 product-container">
                          <div class="product-thumb item-inner">
							 <?php if($product['coupon_discount']){ ?> 
				                <div class="deals-discount"><h5><?php echo round($product['discount']); ?> %<br>Off</h5></div>
			                <?php } ?>
			                <div class="images-container ">
								<a href="<?php echo $product['href']; ?>">
								    <div class="pro_img">
									<?php if ($product['thumb']) { ?>
									<?php if($product['rotator_image']){ ?>
									<img class="image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>"/><?php } ?>
									<?php } else { ?>
									<img  src="image/cache/no_image-100x100.png" alt="<?php echo $product['name']; ?>" />
									<?php } ?>
									<img  src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="image1"/></div>
								</a>
                            </div>
                            <div class="des-container">
							    <h2 class="product-name hidden-xs hidden-sm"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
						        <h2 class="product-name hidden-md hidden-xl hidden-lg"><a href="<?php echo $product['href']; ?>"><?php echo $product['name_mobile']; ?></a></h2>
								<?php $is_optionqty = 1; ?>
								<?php if($product['options']) { ?>
						            <div class="options options-<?php echo $product['product_id']; ?>">
						                <?php foreach($product['options'] AS $option) { ?>
                                            <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
						                <?php } ?>
						            </div>
						        <?php } else { ?>
						            <div class="options"></div>
						            <?php $is_optionqty = 0; ?>
						        <?php } ?>
								
								<div class="price-label col-md-12 col-xs-12 col-sm-12">
						            <div class="box-price">
						                <?php if ($product['price']) { ?>
						                    <p class="price col-md-12 col-xs-12 col-sm-12">
						                        <?php if (!$product['special']) { ?>
                                                    <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
						                        <?php } else { ?>
						                            <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
						                            <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
						                        <?php } ?>
						                    </p>
						                   <?php if ($product['coupon_discount']) { ?>
                			    	            <span class="price-coupon price-coupon-<?php echo $product['product_id']; ?>"><b> <?php echo $product['coupon_discount']; ?> </b></span><span> <?php echo $product['coupon'] ? 'Using code: <b>' . $product['coupon'] . '</b>' : ''; ?></span>
                			    	    <?php } ?>
						                <?php } ?>
						            </div>
				                </div>
				                
				                <?php if($product['product_type'] != 5) { ?>
						            <?php if($is_optionqty != 1 && $product['quantity'] > 0) { ?>
						                <div class="quantity-container">
						                    <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $product['product_id']; ?>)" />
						                    <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>)" />
						                    <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $product['product_id']; ?>)" />
						                </div>
						                <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-sm hidden-xs" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><?php echo $button_cart; ?></button>
						                <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    						        <?php } else { ?>
						                <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						                <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						            <?php } ?>
						        <?php } else { ?>
						            <?php if($product['quantity'] > 0) { ?>
						                <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						            <?php } else { ?>
						                <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						                <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						            <?php } ?>
						        <?php } ?>
							</div>
                          </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row toolbar4">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6 text-right"></div>
                </div>
                <?php } else { ?>
                    <center>
                        <div class="contentSe">
                            <h4>Sorry, we coudn't find any results matching your search.</h4>
                            <h3>Search tips</h3>
                            <ul class="searchtopul">
                                <li>Check your spelling and try again.</li>
                                <li>Try a similar but different search term.</li>
                                <li>Keep your search terms simple.</li>
                                <li>Try looking in these sections.</li>
                            </ul>
                
                            <div style="text-align: center; margin-top: 15px;">
                                <a class="button btn" href="/all-products">See All Products</a>
                            </div>
                        </div>
                    </center>
                <?php } ?>
               
            </div>
            	<div class="tab-pane" id="tab-blogs">
            	<?php if ($blogs) { ?>
            	 <?php foreach ($blogs as $blog) { ?>
            	   <div class="col-md-3 col-xs-6 col-sm-4 blog-container">
            	   <?php if($blog['thumb']) { ?>
            		<a href="<?php echo $blog['href']; ?>" ><img style="display: block; width: 100%;" src="<?php echo $blog['thumb']; ?>" title="<?php echo $blog['name']; ?>" alt="<?php echo $blog['name']; ?>" /></a>
            		<?php }  ?>
            	      <h4 class="hidden-xs hidden-sm"><a class="blog-title" href="<?php echo $blog['href']; ?>"><?php echo $blog['name']; ?></a></h4>
            	      <h4 class="hidden-md hidden-lg hidden-xl"><a class="blog-title" href="<?php echo $blog['href']; ?>"><?php echo $blog['name_mobile']; ?></a></h4>
            	  </div>
            	  <?php } ?>
                  <?php } else { ?>
                    <p><?php echo $text_empty_blog; ?></p>
                <?php } ?>
       </div>
      <div class="tab-pane" id="tab-recipes">
	<?php if ($recipies) { ?>
		<!--<h2 style="padding-left: 20px;">Find Your Favorite Recipe..!</h2> -->
	 <?php foreach ($recipies as $recipe) { ?>	
	   <div class="col-md-3 col-xs-6 col-sm-4 recipe-container">
	        <?php if($recipe['image']) { ?>
		        <a href="<?php echo $recipe['href']; ?>"><img style="display: block; width: 100%; height: auto;" src="<?php echo $recipe['image']; ?>" title="<?php echo $recipe['title']; ?>" alt="<?php echo $recipe['title']; ?>" /></a>
		    <?php }  ?>
		    <div class="recipe-title">
	            <h4 class="hidden-xs hidden-sm"><a href="<?php echo $recipe['href']; ?>"><?php echo $recipe['title']; ?></a></h4>
	            <h4 class="hidden-md hidden-xl hidden-lg"><a href="<?php echo $recipe['href']; ?>"><?php echo $recipe['title_mobile']; ?></a></h4>
	        </div>
	       <!-- <h4 style = "padding: 0px 15px 5px 15px;"><?php echo substr($recipe['description'],0,150); ?>...<a style = "font-size:18px;color:#F49A25;" href="<?php echo $recipe['href']; ?>" >Read More</a> </a></h4>
	       <h4><a href="<?php echo $recipe['href']; ?>"><?php echo strlen($recipe['title']) > 24 ? substr($recipe['title'],0,24) . '...' : $recipe['title']; ?></h4>
	       -->
	  </div>
	 <?php } ?>
	  
      <?php } else { ?>
            <p><?php echo $text_empty_recipe; ?></p>
        <?php } ?>
	</div>
		</div>
</div>	
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>

<script>
jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 590) {
      $('.col').removeClass('col-3');
    } else if (ww >= 591) {
      $('.col').addClass('col-3');
    };
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});
</script>
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
	    			$('.price-new-' + product_id).html(json.new_price.special);
					$('.price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.coupon) {
					    $('.price-coupon-' + product_id).html(json.new_price.coupon); 
					}
					
					if(json.new_price.special) {
					    $('.price-new-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('.price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.des-container .options .radio input[type="radio"]').click(function() {
        $('.des-container .options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
<?php echo $footer; ?>