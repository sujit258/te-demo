<style>
    .qty-container {
        width: 50%;
    }

.quantity {
        height: 35px!important;
        width: 45%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
</style>    

<?php if (!empty($photos['items'])) { ?>
<?php $i = 0; ?>

<?php foreach ($photos['items'] as $photo) { ?>
<?php if ($i >= 4) { ?> <?php  break; ?> <?php } else { ?>

  <?php if ($photo['is_extra']) { ?>
    <div class="islip-photo-item col-xs-6 col-sm-3 col-md-3">
      <a href="<?php echo $photo['url']; ?>" target="_blank">
        <img src="<?php echo $photo['image_thumb']; ?>" class="img-responsive">
      </a>
    </div>
  <?php } else { ?>
          <div class="islip-photo-item col-xs-6 col-sm-3 col-md-3">
              <a href="index.php?route=product/insta_shop/modal&<?php echo http_build_query($photo); ?>" class="isl-photo-gallery">
                <img src="<?php echo $photo['image_thumb']; ?>" class="img-responsive">
              </a>
        </div>
 <?php } ?>
 <?php if ($i == 4) { ?>
 <div class="islip-photo-item col-xs-6 col-sm-3 col-md-3">
      <a href="/our-special-selection" target="_blank">
        <img src="https://www.trueelements.co.in/image/catalog/show-more.jpg" class="img-responsive">
      </a>
    </div>
 
 <?php } ?>
 <?php $i++; ?>
 
 <?php } ?>
    
<?php } ?> 

<?php } ?>
 
 



