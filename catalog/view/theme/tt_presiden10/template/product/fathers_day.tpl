<style>
body {
    font-size: 15px;
}

body h1 {
    font-weight: 700;
    color: #002f36;
    font-size: 25px;
}

.price-new {
    display: inline-block !important;
    font-size: 20px !important;
    color: #f49a25 !important;
    text-align: left !important;
    font-weight: 700 !important;
    margin: 0 !important;
}

.price-old {
    font-size: 16px !important;
}

 .disc1 li {
    margin: 0px;
    list-style-type: disc !important;
}
.product-description {
    margin-top: 5px;
    margin-bottom: 5px;
    font-size: 15px;
}
 body {
    font-size: 15px;
}
.product-banner, .product-1, .product-2 {
    border: 2px solid #d5dbdb;
    background: rgba(135, 149, 150, 0.095);
}
.product-1, .product-2 {
    margin-top: 0px!important;
    min-height: 493px;
    max-height: 493px;
    height: 100%;
}
.product-text {
    height: 100%;
    min-height: 375px;
    padding-right: 15px;
}
.product-banner, .product-data {
    min-height: 493px!important;
}
.product-banner {
    background-repeat: no-repeat;
    background-size: 101%, 100%;
}
.overlay {
    min-height: 190px;
    background: rgba(236, 238, 239, 1);
    padding: 15px;
    margin: 15px;
    width: 49%;
    color: #002f36;
}
body h4 {
    font-weight: 500;
    font-size: 17px;
    line-height: 22px;
    color: #002f36;
}
body h1 {
    font-weight: 700;
    color: #002f36;
    font-size: 25px;
}
.product-image img {
    margin-top: 10%;
}
.product-image {
    min-height: 375px;
    padding-left: 0px;
}
.top-banner {
    margin-top: 0!important;
    margin-bottom: 25px!important;
}
.top-banner img {
    width: 100%;
}
.breadcrumb {
    margin-bottom: 20px!important;
}
.top-image img {
    width: 100%;
}
.carousel-control span {
    font-size: 30px;
}
/*.top-banners {
    margin-bottom: 25px;
}*/
.content-vertical-center {
    margin-top: 15%;
    margin-left: 15px;
}
.buttons .btn-block {
    padding: 10px 16px!important;
}
.buttons .btn-block:active, .buttons .btn-block:focus {
    background: #f49a25!important;
}
@media(max-width:1000px) {
    .top-msg h2 {
        font-size: 17px!important;
    }
    .product-banner {
        min-height: 218px !important;
        background-size: 100%, 100%!important;
    }
    body h4 {
        font-size: 15px!important;
    }
    body h1 {
        font-size: 17px!important;
    }
    .overlay {
        min-height: 160px;
        width: 70%;
    }
    .product-image img {
        margin-top: 30%;
    }
    .product-image {
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .product-1, .product-2 {
        max-height: 674px;
        min-height: 320px;
        height: auto!important;
    }
    .product-data {
        min-height: 1px !important;
    }
    .product-text, .product-image {
        min-height: 270px;
    }
    .product-text {
        height: auto!important;
    }
    .buttons .btn-block {
        width: 38%!important;
        padding: 7px 16px;
    }
    .content-vertical-center {
        margin-top: 0!important;
        margin-left: 0!important;
    }
    #cart #cart-total {
        top: -5px;
    }
}
    #cart #cart-total {
        top: -6px;
    }
    
</style>

<style>
    .options {
        min-height: 55px;
    }
    .product-options {
        margin-top: 10px;
        text-align: center;
        float: left;
        background: #fff!important;
    }
    
    .des-container {
        margin-top: 2%;
    }
    
    @media(max-width: 1000px) {
        .radio label span {
            font-size: 12px!important;
        }
        .des-container .options .radio {
            margin: 0px 0px 5px -2px!important;
        }
        .options {
            min-height: 55px!important;
        }
        
        .buttons .btn-block { 
            margin-top: -16%;
        }
    }
    .des-container {
        padding: 0!important;
    }
    @media(min-width: 1000px) {
        .des-container {
            min-height: 200px;
        }
    }
    
    
    .des-container .options .radio input {
        display: none !important;
    }
    .des-container .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .des-container .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    .des-container .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    
    .image1, .image2 {
        padding: 5%!important;
    }
     @media(min-width:741px){ 
        .options {
            min-height: 55px !important;
        }
   }
   
   @media(max-width:740px){
        .options {
            min-height: 55px !important;
        }
   } 
   
   .des-container {
        min-height: 48px !important;
    }
    
    .te-lab-desc {
        font-size: 15px;
        line-height: 20px;
        text-align: justify; 
    }
    
    @media (max-width: 850px) and (min-width: 700px){
        .product-banner{
            min-height: 483px !important;
        }
        
        .col-sm-12 {
            width: 100% !important;
        }
    }

</style> 

<?php echo $header; ?> 

<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>


<div class="container-fluid">
        <div class="container-fluid">
        <div class="row top-banner hidden-xs hidden-sm">
            <div class="gifting-carousel w3-content w3-section">
                <img alt="True Elements" class="mySlides" src="image/catalog/icon-category/desktop-top-banner.jpg"  class="img-responsive">
                <img alt="True Elements" class="mySlides" src="image/catalog/icon-category/desktop-top-banner.jpg" class="img-responsive">
                <img alt="True Elements" class="mySlides" src="image/catalog/icon-category/desktop-top-banner.jpg"  class="img-responsive">
    		</div>
    
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".gifting-carousel").owlCarousel({
                        autoPlay: true,
                        items : 1,
                        slideSpeed : 1000,
                        navigation : true,
                        pagination : true,
                        controls: true,
                        stopOnHover : false,
                        itemsDesktop : [1199,1],
                        itemsDesktopSmall : [991,1],
                        itemsTablet: [700,1],
                        itemsMobile : [400,1],
                    });
                });
            </script>
        </div>
        <div class="row top-banner hidden-md hidden-xl hidden-lg">
            <div class="gifting-mobile-carousel w3-content w3-section">
               <img alt="True Elements" class="mySlides" src="image/catalog/icon-category/mobile-top-banner.jpg"  class="img-responsive">
                <img alt="True Elements" class="mySlides" src="image/catalog/icon-category/mobile-top-banner.jpg" class="img-responsive">
                <img alt="True Elements" class="mySlides" src="image/catalog/icon-category/mobile-top-banner.jpg"  class="img-responsive">
    		</div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".gifting-mobile-carousel").owlCarousel({
                        autoPlay: true,
                        items : 1,
                        slideSpeed : 1000,
                        navigation : true,
                        pagination : true,
                        controls: true,
                        stopOnHover : false,
                        itemsDesktop : [1199,1],
                        itemsDesktopSmall : [991,1],
                        itemsTablet: [700,1],
                        itemsMobile : [400,1],
                    });
                });
            </script>
        </div>
    </div> 
     
     <div class="description">                                             
    <style>
        .productImg {
            left: 5%;
            width: 100%;
            float: left;
            position: relative;
            left: 0%;
            margin: 0 0 5px 0;
        }
         
        .productImg img {
            width: 80%;
            float: left;
        }
        
        @media only screen and (min-width:1366px){
            .productImg {
                width: 90%;
                left: 12%;
            }
        }
        
        @media only screen and (max-width:600px){
            .productImg {
                left: 12%;
            }
        }
        
        
        .catbox{
            margin-bottom: 2%;
            min-height: 160px;
        }
        
        .hctitle{
            text-align: center;
        }
        
        @media only screen and (max-width:600px){
            .inline-photo {
                min-height: 160px;
            }
        }
        
        .ahctitle{
            font-size:15px;
            font-weight: 500;
        }
        
        .inline-photo {
            opacity: 0;
            transform: translateY(4em) rotateZ(-0deg);
            transition: transform 4s .25s cubic-bezier(0,1,.3,1),opacity .3s .25s ease-out;
            will-change: transform, opacity;
        }
        
        .inline-photo.is-visible {
            opacity: 1;
            transform: rotateZ(-0deg);
        }
    </style>

        <div class="container" style="margin-top:30px!important;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3 hidden-xs hidden-sm catbox giftpacks" style="cursor:pointer;"> 
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 catbox gifting" style="cursor:pointer;">
                        <div class="inline-photo show-on-scroll is-visible">
                            <a class="productImg"><img src="image/catalog/icon-category/circle-icon.jpg" alt="Customise Gifting"></a>
                            <p class="hctitle"><a class="ahctitle">Recipes</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 catbox giftcard" style="cursor:pointer;">
                        <div class="inline-photo show-on-scroll is-visible">
                            <a  class="productImg"><img src="image/catalog/icon-category/circle-icon.jpg" alt="Gift cards"></a>
                            <p class="hctitle"><a  class="ahctitle">Videos</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 hidden-xs hidden-sm catbox bulk" style="cursor:pointer;"> 
                    </div> 
                </div>
            </div>
        </div>                                                              
    </div>
    
    <?php if ($products) { ?>
    <input type="hidden" name="product_id" value="" />
    <?php $count=0; ?>
    <?php foreach ($products as $product) { ?>
    <br>
    <?php $remainder = $count % 2; ?>
     
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <?php if ($remainder == 0) { ?> 
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('image/catalog/icon-category/try-out-gif.gif');">
                </div>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                                <h1><?php echo $product['custom_description']; ?></h1>
                            
                            <a href="<?php echo $product['href']; ?>"><h4><?php echo $product['name']; ?> </h4></a>
                             <?php if (!$product['special']) { ?>
                                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } else { ?>
								        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
						                <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } ?> 
                            <!-- options -->
                            <div class="des-container">
                            <?php $is_optionqty = 1; ?>
						<?php if($product['options']) { ?>
						    <div class="options options-<?php echo $product['product_id']; ?>">
						        <?php foreach($product['options'] AS $option) { ?>
						            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
						        <?php } ?>
						    </div>
						<?php } else { ?>
						    <div class="options <?php if($category_id == 586) { echo 'hidden-md hidden-xl hidden-lg hidden-xs hidden-sm';} ?>"></div>
						    <?php $is_optionqty = 0; ?>
						<?php } ?>
                        </div>
                            <!-- options -->
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a onclick ="addtocart(<?php echo $product['product_id']; ?>)" type="button" class="btn btn-primary" style="display:inline-block;">Add to Cart</a> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image"> 
                            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" class="img-responsive" /></a> 
                    </div>
                    
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center; margin-bottom: 5%;">
                        <a onclick ="addtocart(<?php echo $product['product_id']; ?>)" type="button" class="btn btn-primary" style="display:inline-block;">Add to Cart</a> 
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center"> 
                                <h1><?php echo $product['custom_description']; ?></h2>
                             
                            <a href="<?php echo $product['href']; ?>"><h4><?php echo $product['name']; ?></h4></a>
                             <?php if (!$product['special']) { ?>
                                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } else { ?>
								        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
						                <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } ?> 
                            <!-- options -->
                            <div class="des-container">
                            <?php $is_optionqty = 1; ?>
						<?php if($product['options']) { ?>
						    <div class="options options-<?php echo $product['product_id']; ?>">
						        <?php foreach($product['options'] AS $option) { ?>
						            <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <?php if ($option_value['quantity'] > 0) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php $is_optionqty = 0; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select-->
                                    
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
						        <?php } ?>
						    </div>
						<?php } else { ?>
						    <div class="options <?php if($category_id == 586) { echo 'hidden-md hidden-xl hidden-lg hidden-xs hidden-sm';} ?>"></div>
						    <?php $is_optionqty = 0; ?>
						<?php } ?>
                        </div>
                            <!-- options -->
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a onclick ="addtocart(<?php echo $product['product_id']; ?>)" type="button" class="btn btn-primary" style="display:inline-block;">Add to Cart</a>
                                 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image"> 
                            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" class="img-responsive" /> </a>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center; margin-bottom: 5%;">
                        <a onclick ="addtocart(<?php echo $product['product_id']; ?>)" type="button" class="btn btn-primary" style="display:inline-block;">Add to Cart</a> 
                    </div>
                </div>
            </div>
          <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('image/catalog/icon-category/try-out-gif.gif');">
                </div> 
          <?php } ?>  	
        </div>
    </div>
    <?php $count++; ?>
    <?php } ?>
    <?php } ?>
      
</div>

<?php if ($alert_type == 'custom') { ?>
<style type="text/css">
.alert-jpn.alert-custom {
	background: <?php echo $background_color; ?> !important;
	border-color: <?php echo $border_color; ?> !important;
	color: <?php echo $text_color; ?> !important;
}
.alert-jpn > [data-notify="dismiss"]{
    color:#fff !important;
    opacity: 0.9;
}
.alert-jpn > [data-notify="icon"]{
    border :none;
}
</style>

<?php } ?> 

<script type="text/javascript"><!--
var jpn_notifications = <?php echo json_encode($notifications); ?>;
var jpn_total_notifications = jpn_notifications.length;
var jpn_show_index = 0;
	
$(document).ready(function(){
	$.notifyDefaults({
		position: 'fixed',
		height: '14%',
		type: '<?php echo $alert_type; ?>',
		<?php if ($allow_dismiss) { ?>
		allow_dismiss: true,
		<?php } else { ?>
		allow_dismiss: false,
		<?php } ?>
		<?php if ($show_progressbar) { ?>
		showProgressbar: true,
		<?php } else { ?>
		showProgressbar: false,
		<?php } ?>	
		placement: {
			from: '<?php echo $placement_from; ?>',
			align: '<?php echo $placement_align; ?>'
		},		
		z_index: <?php echo $zindex; ?>,
		delay: <?php echo $delay; ?>,
		url_target: '_self',
		mouse_over: 'pause',
		animate: {
			enter: 'animated <?php echo $animate_enter; ?>',
			exit: 'animated <?php echo $animate_exit; ?>'
		},
		icon_type: 'image',
		template: '<div data-notify="container" class="col-md-3 col-sm-6 col-xs-11 alert-jpn alert-{0}" role="alert">' +
				'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
				'<span data-notify="icon"></span> ' +
				'<span data-notify="message" style="margin-top:24px;font-size:15px;">{2}</span>' +
				'<span data-notify="title" style="color:#f49a25;">{1}</span> ' +
				'<div class="progress" data-notify="progressbar">' +
					'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
				'</div>' +
				'<a href="{3}" target="{4}" data-notify="url"></a>' +
			'</div>' 
	});
	
	showNotification(jpn_show_index);
});

function showNotification(show_index){
	$.notify({
		icon: jpn_notifications[show_index]['image'],
		message: jpn_notifications[show_index]['message'],
		title: (jpn_notifications[show_index]['show_time_ago'] != 0) ? jpn_notifications[show_index]['time_ago'] : '',
		url: jpn_notifications[show_index]['product_href']
	},{
		onClosed: function() {
			if (show_index < jpn_total_notifications - 1) {
				setTimeout(showNotification, <?php echo $timeout; ?>, ++show_index);	
			}	
		}
	});
}
--></script>
 
    
<!--script type="text/javascript"> 
      $(document).ready( function() {
        $('.alert-success').delay(1000).fadeOut();
      });
</script-->
    
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        var discount_per = '<?php echo $discount_per; ?>';
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
				    if(discount_per > 0) {
                        $('.price-new-' + product_id).html('Rs.'+ Math.round(json.new_price.price1 - (json.new_price.price1 * discount_per / 100)));
                        $('.price-old-' + product_id).html(json.new_price.price);
				    } else {
				        if(json.new_price.special) {
					        $('.price-new-' + product_id).html(json.new_price.special);
					        $('.price-old-' + product_id).html(json.new_price.price);
					    } else {
					        $('.price-new-' + product_id).html(json.new_price.price);
					    }
				    }
					
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.des-container .options .radio input[type="radio"]').click(function() {
        $('.des-container .options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
<?php echo $footer; ?>