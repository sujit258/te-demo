<?php echo $header; ?>
<style>
body {
    font-size: 15px;
}
.product-banner, .product-1, .product-2 {
    border: 2px solid #d5dbdb;
    background: rgba(135, 149, 150, 0.095);
}
.product-1, .product-2 {
    margin-top: 0px!important;
    min-height: 375px;
    max-height: 375px;
    height: 100%;
}
.product-text {
    height: 100%;
    min-height: 375px;
    padding-right: 15px;
}
.product-banner, .product-data {
    min-height: 750px!important;
}
.product-banner {
    background-repeat: no-repeat;
    background-size: 101%, 100%;
}
.overlay {
    min-height: 190px;
    background: rgba(236, 238, 239, 1);
    padding: 15px;
    margin: 15px;
    width: 49%;
    color: #002f36;
}
body h4 {
    font-weight: 500;
    font-size: 17px;
    line-height: 22px;
    color: #002f36;
}
body h1 {
    font-weight: 700;
    color: #002f36;
    font-size: 25px;
}
.product-image img {
    margin-top: 10%;
}
.product-image {
    min-height: 375px;
    padding-left: 0px;
}
.top-banner {
    margin-top: 0!important;
    margin-bottom: 25px!important;
}
.top-banner img {
    width: 100%;
}
.breadcrumb {
    margin-bottom: 20px!important;
}
.top-image img {
    width: 100%;
}
.carousel-control span {
    font-size: 30px;
}
.top-banners {
    margin-bottom: 25px;
}
.content-vertical-center {
    margin-top: 15%;
    margin-left: 15px;
}
.buttons .btn-block {
    padding: 10px 16px!important;
}
.buttons .btn-block:active, .buttons .btn-block:focus {
    background: #f49a25!important;
}
@media(max-width:1000px) {
    .product-banner {
        min-height: 330px!important;
        background-size: 100%, 100%!important;
    }
    body h4 {
        font-size: 15px!important;
    }
    body h1 {
        font-size: 17px!important;
    }
    .overlay {
        min-height: 160px;
        width: 70%;
    }
    .product-image img {
        margin-top: 30%;
    }
    .product-image {
        padding-right: 0px!important;
        padding-left: 0px!important;
    }
    .product-1, .product-2 {
        max-height: 320px;
        min-height: 320px;
    }
    .product-data {
        min-height: 1px !important;
    }
    .top-banner img {
        height: 140px!important;
    }
    .product-text, .product-image {
        min-height: 270px;
    }
    .buttons .btn-block {
        width: 35%!important;
        padding: 7px 16px;
    }
    .content-vertical-center {
        margin-top: 0!important;
        margin-left: 0!important;
    }
    #cart #cart-total {
        top: -5px;
    }
}
#cart #cart-total {
    top: -6px;
}
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container-fluid">
    <div class="row top-banner hidden-xs hidden-sm">
        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-top-banner.jpg" class="img-responsive" />
    </div>
    <div class="row top-banner hidden-md hidden-xl hidden-lg">
        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-top-banner-mobile.jpg" class="img-responsive" />
    </div>
    <div class="row top-banners">
	    <div class="col-md-4 col-xs-12 col-sm-12 top-image">
	       <a href="<?php echo $oatmeal; ?>"><img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-category-banner-1.jpg"></a>
	    </div>
	    <div class="col-md-4 col-xs-12 col-sm-12 top-image">
	       <a href="<?php echo $muesli; ?>"><img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-category-banner-2.jpg"></a>
	    </div>
	    <div class="col-md-4 col-xs-12 col-sm-12 top-image">
	       <a href="<?php echo $flakes; ?>"><img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-category-banner-3.jpg"></a>
	    </div>
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <a href="<?php echo $strawberry_muesli; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section1.jpg');">
                </div>
            </a>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>No Added Sugar Muesli, 1kg</h1>
                            <h4>With the perfect balance of oats and real fruits, this powerpacked fibre-rich bowl of Muesli will give you a scrumptious kick-start in the morning.</h4>
                            <h1>Rs 439 <small><strike>Rs 450</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $no_added_sugar_muesli; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section1-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $no_added_sugar_muesli; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section1-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Fruit and Nut Muesli, 1kg</h1>
                            <h4>The delicious blend of dried fruits & nuts with natural whole grains makes it a perfect breakfast treat for health-conscious people.</h4>
                            <h1>Rs 439 <small><strike>Rs 450</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $fruit_nut_muesli; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $fruit_nut_muesli; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Multigrain BIX, 350gm</h1>
                            <h4>Made from wholegrain Jowar, Rolled Oats, Farro and blended perfectly with sorghum syrup, Multigrain BIX makes a slightly sweet snack for your sudden hunger needs.</h4>
                            <h1>Rs 209 <small><strike>Rs 220</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $multigrain_bix; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section2-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $multigrain_bix; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section2-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <h1>Jowa BIX, Single Grain BIX, 350gm</h1>
                            <h4>Made from wholegrain Jowar, carrying the benefits of Jowar in one, these bix can provide you with the required nutrition to fill up your diet essentials.</h4>
                            <h1>Rs 190 <small><strike>Rs 200</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $jowa_bix; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $jowa_bix; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
            <a href="<?php echo $jowa_bix; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section2.jpg');">
                </div>
            </a>
        </div>
    </div>
    <div class="row product-row">
        <div class="col-md-12 col-xs-12 col-sm-12 product-layout">
            <a href="<?php echo $oatmeal; ?>">
                <div class="col-md-6 col-xs-12 col-sm-12 product-banner" style="background-image: url('https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section3.jpg');">
                </div>
            </a>
            <div class="col-md-6 col-xs-12 col-sm-12 product-data">
                <div class="row product-1">
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <!--<h1>Whole Oatmeal with Real Fruits & Chia, 500gm</h1>-->
                            <h1>Whole Oatmeal, 500gm</h1>
                            <h4>Filled with real fruits & nuts to give your taste buds a nutty-fruity flavour, this overnight oatmeal will take your breakfast cravings to a tastier and better version.</h4>
                            <h1>Rs 257 <small><strike>Rs 270</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $oatmeal; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section3-p1.png" class="img-responsive" />
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $oatmeal; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
                <div class="row product-2">
                    <div class="col-md-4 col-xs-4 col-sm-4 product-image">
                        <img src="https://www.true-elements.com/image/catalog/images-08-19/category/op-rte-section3-p2.png" class="img-responsive" />
                    </div>
                    <div class="col-md-8 col-xs-8 col-sm-8 product-text">
                        <div class="content-vertical-center">
                            <!--<h1>Whole Oatmeal with Real Fruits & Chia, 1kg</h1>-->
                            <h1>Whole Oatmeal, 1kg</h1>
                            <h4>Made from wholegrain rolled oats, a bowl of oatmeal can keep you full for a longer duration and can help you avoid binge eating as it is packed with dietary fibres.</h4>
                            <h1>Rs 506 <small><strike>Rs 520</strike></small></h1>
                            <div class="buttons hidden-xs hidden-sm" style="text-align: center;">
                                <a href="<?php echo $oatmeal; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="buttons hidden-md hidden-xl hidden-lg" style="text-align: center;">
                        <a href="<?php echo $oatmeal; ?>" type="button" class="btn btn-primary btn-lg btn-block" style="display:inline-block;">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>