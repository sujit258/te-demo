<?php if ($total_reviews > 0) { ?>
<style>
    @media screen and (min-width: 500px) and (max-width: 900px){
        .progress{
            width: 70% !important;
        }

        .review-ratings-text{
            font-size: 16px !important;
        }
    }

@media screen and (min-width: 300px) and (max-width: 490px){
        .progress{
            height: 11px !important;
        }

        .review-ratings-text{
            font-size: 16px !important;
        }
    }

    .review-ratings-text{
        padding: 0 15px;
        font-size: 18px ;
        font-weight: 600 !important;
    }
    .overall-reviews .fa {
        color: #f49a25;
        font-size: 20px !important;
    }
    .p0 {
        padding-left: 0;
        padding-right: 0;
    }
    .overall-reviews {
        display: block;
        padding: 15px;
        margin-bottom: 15px;
        background: #fcfcfc;
        margin: 20px 0;
    }
    .progress-bar {
        background: #f49a25;
    }
    .overall-reviews .review-ratings-box {
        text-align: center;
    }
    @media(min-width: 768px) {
    .progress {
        width: 45%;
        height: 11px;
        margin-bottom: 15px;
    }
    .review-left, .review-right {
        min-height: 158px;
    }
    .review-right {
        padding-top: 15px;
    }
    .review-ratings {
        padding-top: 33px;
    }
    .reviews-author {
        display: inline;
    }
    .individual-rating {
        margin-top: 5px;
    }
    .individual-rating .fa {
        height: 15px;
    }
    .reviews-date {
        float: right;
    }
    .reviews-text {
        margin-top: 15px;
    }
    }
    @media(max-width: 767px) {
        .review-left {
            margin-bottom: 30px;
        }
        .individual-rating {
            width: 100%;
            display: block;
        }
        #review {
            width: 100%;
        }
        .progress {
            margin-bottom: 15px;
        }
    }


</style>
<!-- <div class="module-title" style="margin-bottom: 0;">
    <span>Customer Reviews</span>
</div> -->
<!-- <hr style="margin-top: 0;"> -->
<div class="col-md-12 col-xs-12 col-sm-12 overall-reviews">
    <div class="col-md-6 col-xs-12 col-sm-5 review-left">
        <div class="review-ratings" itemprop="reviewRating">
            <div class="review-ratings-box">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if (round($average_rating) < $i) { ?>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } else { ?>
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } ?>
                <?php } ?>
                <div class="review-ratings-text"><?php echo round($average_rating, 1), '/ 5 by ', $total_reviews, ' Customers'; ?></div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-7 review-right">
        <?php for($k = 5; $k >= 1; $k--) { ?>
            <div class="row">
                <?php if($rating_count[$k] > 0) { ?>
                    <a href="index.php?route=product/product/product_reviews&product_id=<?php echo $product_id; ?>&filter_rating=<?php echo $k; ?>">
                        <div class="col-md-2 col-xs-4 col-sm-3 p0"><?php echo $k; ?> Star (<?php echo $rating_count[$k]; ?>) </div>
                    </a>
                <?php } else { ?>
                    <div class="col-md-2 col-xs-4 col-sm-3 p0"><?php echo $k; ?> Star (<?php echo $rating_count[$k]; ?>) </div>
                <?php } ?>
                <div class="col-md-10 col-xs-8 col-sm-9 p0">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $rating_per[$k]; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $rating_per[$k]; ?>%;">      
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<!-- <?php foreach ($reviews as $review) { ?>
<div class="reviews">
  	<div class="img-circle reviews-avatar"><i class="fa fa-user"></i></div>
  	<div class="reviews-wrap col-md-12 col-xs-12 col-sm-12 p0">
	  	<h4 class="reviews-author"><?php echo $review['author']; ?></h4>
	  	<h4 class="reviews-date hidden-xs hidden-sm"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
	  	<div class="individual-rating">
	  	    <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($review['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
            <?php } ?>
        </div>
        <h4 class="reviews-date hidden-md hidden-xl hidden-lg"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
	    <p class="reviews-text"><?php echo $review['text']; ?></p>
    </div>
</div>
<?php } ?> -->

    <?php if ($total_reviews > 0) { ?>
        <style>
            .p0 {
                padding-left: 0;
                padding-right: 0;
            }
            .reviews1 {
                background: #fff;
                padding: 15px 20px;
                margin: 10px 0;
                border: 1px solid #ddd;
                display: flex;
            }
            .reviews1 .reviews-wrap {
                margin-left: 30px;
            }
           
            @media(min-width: 768px) {
             
                .reviews-author1 {
                    display: inline;
                }
                .individual-rating1 {
                    margin-top: 5px;
                }
                .individual-rating1 .fa {
                    height: 15px;
                }
                .reviews-date1 {
                    float: right;
                }
                .reviews-text1 {
                    margin-top: 15px;
                }
                
            }
            
            @media(max-width: 767px) {
                         
                .individual-rating1 {
                    width: 100%;
                    display: block;
                }  
            }
        </style>
        <!-- <?php foreach ($topreviews as $review) { ?>
            <div class="reviews1">
              	<div class="img-circle reviews-avatar"><i class="fa fa-user"></i></div>
          	<div class="reviews-wrap col-md-12 col-xs-12 col-sm-12 p0">
        	  	<h4 class="reviews-author1"><?php echo $review['author']; ?></h4>
        	  	<h4 class="reviews-date1 hidden-xs hidden-sm"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
        	  	<div class="individual-rating1">
        	  	    <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($review['rating'] < $i) { ?>
                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                        <?php } else { ?>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                        <?php } ?>
                    <?php } ?>
                </div>
            <h4 class="reviews-date1 hidden-md hidden-xl hidden-lg"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
            <p class="reviews-text1"><?php echo $review['text']; ?></p>
            </div>
        </div>
        <?php } ?> 
    <?php } else { ?>
        <p style="font-size: 15px;"><?php echo $text_no_reviews; ?></p>
    <?php } ?>

<!-- <div class="col-md-12 col-xs-12 col-sm-12 text-center">
    <a href="index.php?route=product/product/product_reviews&product_id=<?php echo $product_id; ?>" target="_blank" class="btn button" style="margin-bottom:15px; color: #fff!important;">See All Reviews</a>
</div> -->
<!-- <?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
<?php } ?> -->