<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"-->
<style>
.carousel {
    overflow: hidden;
    position: relative;
    width: 100%;
    height: 455px;
    -webkit-perspective: 500px;
    perspective: 500px;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
    -webkit-transform-origin: 0% 50%;
    transform-origin: 0% 50%;
    margin-bottom: 30px;
}
.carousel-item:hover {
    cursor: pointer;
}
.carousel .carousel-item {
    visibility: hidden;
    width: 350px;
    position: absolute;
    top: 0;
    left: 0;
}
.carousel .carousel-item > img {
    width: 100%;
}
.carousel-item-name {
    text-align: center;
    font-family: roboto, 'Oswald', sans-serif!important;
    font-weight: 600;
}
.disc1 ul {
    margin-left: -25px!important;
}
@media(max-width: 1000px) {
    .carousel .carousel-item {
        width: 200px!important;
    }
    .carousel {
        margin-bottom: 10px!important;
        height: 330px;
        margin-top: -30px!important;
    }
    #button-cart {
        font-size: 13px!important;
        padding: 0 15px!important;
    }
    .gift-data {
        padding-left: 0!important;
        padding-right: 0!important;
    }
}
.gifts-highlights .row:nth-child(1) {
    margin-top: 0;
}
.gifting-main-image img {
    margin: auto;
}
</style>




<style>
    .fa-times, .fa-check-square, .fa-map-marker {
        color: #000!important;
        margin-top: 5px;
    }
  .oos{
    background: #777!important;
    border-radius: 4px!important;
    height: 42px!important;
    margin-right: 20px;
    float: left;
    margin-bottom: 15px!important;
  }
  .oos:hover {
    cursor: not-allowed!important;
  }
  .waorder {
    display:none; 
  }

  #button-cart {
    margin-bottom: 15px !important;
    background: #f49a25;
    height: 38px;
    font-weight: 600;
    color: #fff;
    border: none;
    float: left;
    border-radius: 4px;
    text-transform: none;
    padding: 0 45px;
    font-size: 16px;
    margin-left: 10px;
    -webkit-transition: ease .3s all;
    -moz-transition: ease .3s all;
    transition: ease .3s all;
  }
  #button-cart:hover {
      background: #000;
  }
  @media screen and (max-width: 500px) {
    .waorder { display:block; }

    .pro_img {height: 219px;width: 219px;}

    #button-cart {
      padding: 0!important;
      margin-left: 40px !important;
      font-size: 15px;
      width: 26%;
      border-radius: 4px!important;
      margin-bottom: 15px;
    }
    
    #button-wa {
      display: inline-block;
      cursor: pointer;
      -webkit-transition: ease .3s all;
      -moz-transition: ease .3s all;
      transition: ease .3s all;
    }
  }
  .product-description h3 {
    color: #000000!important;
    font-family: roboto, 'Oswald', sans-serif!important;
    line-height: 1.1!important;
    font-size: 17px!important;
    margin-top: 15px!important;
    font-weight: 600;
  }
  .product-description h4 {
    margin-top: 20px!important;
    line-height: 26px !important;
    font-weight: 600;
  }
  .product-description p, .sub-description {
    font-size: 15px!important;
    color: #000!important;
    font-family: roboto, 'Oswald', sans-serif!important;
    text-align: justify!important;
  }
  .product-description p {
    line-height: 28px!important;
  }
  .product-description ul {
    font-size: 14px!important;
    color: #000!important;
    font-family: roboto, 'Oswald', sans-serif!important;
    line-height: 28px!important;
  }
  .product-description ul .sub-description {
    text-align: justify!important;
  }
  .product-description ul li {
    font-size: 15px!important;
  }
  .product-name {
    font-family: roboto, 'Oswald', sans-serif !important;
    font-weight: 600!important;
    line-height: 1.5 !important;
    text-align: center;
    margin-bottom: 20px;
  }
  .qty-product {
    display: inline!important;
  }
  .has-option {
    border: none!important;
    box-shadow: none!important;
  }
  body {
    font-size: 15px!important;
  }
  .disc1 ul li {
    margin: 0px;
    list-style-type: disc !important;
  }
  .thumbnail img {
    display: block;
  }
  .coupon img {
    width: 15px!important;
    height: 15px!important;
    margin-right: 10px!important;
    margin-bottom: 5px!important;
  }
  .coupon {
    margin-bottom: 10px!important;
    float: left;
    padding-left: 0!important;
  }
  .coupon ul {
      padding-left: 0px!important;
  }
  .coupon ul li {
      margin-top: 5px!important;
  }
  .coupon1 img {
    width: 15px!important;
    height: 15px!important;
    margin-right: 10px!important;
    margin-bottom: 5px!important;
  }
  .coupon1 {
    margin-top: 10px!important;
  }
  #button-check, #button-change {
    border-radius: 0px;
    margin-bottom: 1px;
    font-weight: 500!important;
    background: #000;
  }

  #button-check:hover, #button-change:hover {
    background: #000!important;
  }
  .form-control {
    background: #fff!important;
    border: 1px solid #f2f2f2!important;
    border-radius: 3px!important;
  }
  .fa-exclamation-circle {
    margin-right: 10px!important;
  }
  #button-review {
    border-radius: 4px;
    font-weight: 600!important;
    background: #f49a25!important;
    font-size: 16px!important;
  }
  #button-review:hover {
    background: #000!important;
  }
  #form-review .alert-danger, #form-review .alert-success {
    padding: 10px!important;
    margin-top: 15px!important;
  }
  .proimg{
    border-right: 1px solid #ebebeb;
    
  }
  .owl-carousel .owl-item img {
      display: inline-block!important;
  }
  .quickview-product .row {
      margin-right: -17px;
  }
  @media screen and (max-width: 500px){
    .proimg{
      border-bottom: 1px solid #ebebeb;border-right: 1px solid #ebebeb;border-left: 1px solid #ebebeb;border-top: 1px solid #ebebeb;
    } 
  }
  @media screen and (max-width: 800px) {
    /*.coupon {
      margin-top: 10px!important;
      float: left;
    }*/
    .row_items {
        height: 450px!important;
    }
    .recipe_row_items {
        height: 240px!important;
    }
    .quickview-product .row {
        margin-right: -15px!important;
    }
    .feature-banner h4 {
        clip-path: polygon(2% 0, 100% 0, 98% 100%, 0 100%);
        padding: 10px 5px;
        font-size: 14px;
    }
    .feature-banner {
        padding-left: 0!important;
        padding-right: 0!important;
    }
    .feature-banner-container {
        padding-left: 5px!important;
        padding-right: 5px!important;
    }
    .old-order-date h4 {
        text-align: center;
    }
  }
  .button {
    font-weight: 600!important;
    font-size: 16px!important;
  }
  .alert-info {
    float: left;
    width: 100%;
    margin-bottom: 5px!important;
    margin-top: 10px;
  }
  .fa-info-circle {
    margin-right: 10px;
  }
  .form-control:focus {
    border-color: #80bdff!important;
    outline: 0!important;
    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25)!important;
  }
  #form-review .alert {
    width: 49%;
  }
  @media(max-width:800px) {
    #form-review .alert {
      width: 100%;
    }
  }
  .reviews {
    background: #F2F2F2;
    padding: 15px 20px;
    margin: 10px 0;
    border-left: 5px solid #ddd;
  }
  .reviews-avatar {
    background: #f49a25;
    color: #fff;
    height: 50px;
    width: 50px;
    text-align: center;
    display: table;
    float: left;
  }
  .reviews .reviews-wrap{
    margin-left: 65px;
  }
  .reviews .reviews-wrap h5{
    border-bottom: 1px solid #ddd;
    padding-bottom: 10px;
    margin: 5px 0;
  }
  .reviews-avatar i {
    font-size: 25px;
    display: table-cell;
    vertical-align: middle;
  }
  .reviews-author {
    font-style: italic;
    font-weight: 600;
  }
  .reviews-wrap .fa {
    color: #f49a25;
  }
  .fa-calendar {
    color: #000!important;
    margin-right: 10px!important;
  }
  .reviews-date {
    margin-top: 0!important;
  }
  .reviews-text {
    line-height: 22px!important;
  }
  #product .control-label {
      font-weight: 600;
  }
.feature-banner h4 {
    text-align: center;
    padding: 10px;
    background: #000;
    color: #fff;
    font-weight: bold;
    clip-path: polygon(3% 0, 100% 0, 97% 100%, 0 100%);
}
.feature-banner {
    z-index: 1;
}
.breadcrumb {
    margin-bottom: 20px!important;
}
.old-order-date {
    border: 1px solid #f49a25;
    border-radius: 4px;
    margin: 15px 0!important;
}
.old-order-date h4 {
    font-weight: 600;
}
</style>
<?php echo $header; ?>
<ul class="breadcrumb" style="background-image: none;">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  <?php } ?>
</ul>
<h3 class="product-name"><?php echo $heading_title; ?></h3>
<div class="gifting-main-image">
            <img class="img-responsive hidden-xs hidden-sm" src="<?php echo $gifting_banner_desktop; ?>" />
            <img class="img-responsive hidden-md hidden-xl hidden-lg" src="<?php echo $gifting_banner_mobile; ?>" />
        </div>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } ?>
      <?php if ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } ?>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <!--<?php if($old_order_date) { ?>
            <div class="col-md-12 old-order-date">
                <h4>You Purchased this on <?php echo date('jS M Y', strtotime($old_order_date)); ?></h4>
        </div>
        <?php } ?>-->
        <!--h3 class="product-name"><?php echo $heading_title; ?></h3-->
        <!--div class="gifting-main-image">
            <img class="img-responsive hidden-xs hidden-sm" src="<?php echo $gifting_thumb_desktop; ?>" />
            <img class="img-responsive hidden-md hidden-xl hidden-lg" src="<?php echo $thumb; ?>" />
        </div-->
        <div class="carousel">
            <?php foreach($gifting_images As $images) { ?>
			    <div class="carousel-item">
			    	<img src="<?php echo $images['image']; ?>">
			    	<h4 class="carousel-item-name"><?php echo $images['name']; ?></h4>
			    </div>
		    <?php } ?>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="col-md-6 hidden-xs hidden-sm gifts-highlights">
            <?php echo $short; ?>
        </div>
        <div class="col-md-6 col-xs-12 col-sm-12 gift-data">
            <?php if ($review_status) { ?>
                <!--  <a onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">Review </a> -->
                <div class="ratings" itemprop="reviewRating">
                    <div class="rating-box">
                        <?php for ($i = 0; $i <= 5; $i++) { ?>
                            <?php if ($rating == $i) {
                                $class_r= "rating".$i;
                                echo '<div class="'.$class_r.'">rating</div>';
                            } 
                        } ?>
                    </div>
                </div>
        <?php } ?>
    <?php if ($price) { ?>
    <ul class="list-unstyled price-product">
      <?php if (!$special) { ?>
      <li style="font-weight: 600;">
        Price: <span><?php echo $price; ?></span>
      </li>
      <?php } else { ?>
      <li style="font-weight: 600;">Price: <span class="new-price"><?php echo $special; ?></span><br />M.R.P.:<span class="old-price"><?php echo $price; ?></span></li>
      <span style="color: green; font-size:12px;"><?php echo $saving; ?></span>
      <?php } ?>
    </ul>
    <?php } ?>
    
     <?php if ($product_type != 4) { ?>
    <div class="coupon col-md-12">
        <strong>Offers:</strong>
        <ul>
            <li><img src="https://www.true-elements.com/image/catalog/Banner/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png">Get Extra 10% OFF on orders above Rs 499/-, Use Code: <strong>TRUE10</strong></li>
            <?php if($deal_coupon && $discount) { ?>
                <li><img src="https://www.true-elements.com/image/catalog/Banner/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png">Get <?php echo $discount; ?>% OFF, Use Code: <strong><?php echo strtoupper($deal_coupon); ?></strong></li>
            <?php } ?>
        </ul>
    </div>
    
    <?php } ?>
    <div class="hidden-md hidden-xl hidden-lg"><?php echo $short; ?></div>
    
    <div id="product" class="col-md-12">
      <?php if ($options) { ?>
      <?php foreach ($options as $option) { ?>
      <?php if ($option['type'] == 'select') { ?>
      <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
        <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($option['product_option_value'] as $option_value) { ?>
          <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </option>
          <?php } ?>
        </select>
      </div>
      <?php } ?>
      <?php if ($option['type'] == 'radio') { ?>
      <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
        <label class="control-label"><?php echo $option['name']; ?></label>
        <div id="input-option<?php echo $option['product_option_id']; ?>">
            
          <?php foreach ($option['product_option_value'] as $option_value) { ?>
          <?php $countoption =count($option['product_option_value'] ); ?>
          <div class="radio">
             <?php if ($countoption == 1) { ?>
            <label class="checked-option">
            <?php } else { ?> 
            <label>
            <?php } ?>
              <?php $price_tooltip_radio = 1; ?>
              
              <?php if ($countoption == 1) { ?>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> checked/>
            <?php } else { ?> 
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
            <?php } ?>
            

              <?php if ($price_tooltip_radio && $option_value['price']) { ?>
              <?php if ($option_value['image']) { ?>
              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" data-toggle="tooltip" title="<?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)" class="img-thumbnail" /> 
              <?php } else { ?> 
              <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>"><?php echo $option_value['name']; ?></span>
              <?php } ?>
              <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
              <?php if ($option_value['image']) { ?>
              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
              <?php } ?>
              <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)</span>
              <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
              <?php if ($option_value['image']) { ?>
              <img data-toggle="tooltip" title="<?php echo $option_value['name']; ?>" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" class="img-thumbnail" />
              <?php } else { ?> 
              <span><?php echo $option_value['name']; ?></span>
              <?php } ?>
              <?php } ?>
            </label>
          </div>
          
          <?php } ?>
        </div>
      </div>
      <?php } ?>
      <?php if ($option['type'] == 'checkbox') { ?>
      <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
        <label class="control-label"><?php echo $option['name']; ?></label>
        <div id="input-option<?php echo $option['product_option_id']; ?>">
          <?php foreach ($option['product_option_value'] as $option_value) { ?>
          <div class="checkbox">
            <label>
              <?php $price_tooltip_checkbox = 1; ?>
              <?php if ($price_tooltip_checkbox) { ?>
              <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
              <?php if ($option_value['image']) { ?>
              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
              <?php } ?>
              <span<?php if ($option_value['price']) { ?> data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>"<?php } ?>><?php echo $option_value['name']; ?></span>
              <?php } else { ?>
              <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
              <?php if ($option_value['image']) { ?>
              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
              <?php } ?>
              <span><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?></span>
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'image') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label"><?php echo $option['name']; ?></label>
          <div id="input-option<?php echo $option['product_option_id']; ?>">
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <div class="radio">
              <label>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'text') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'textarea') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'file') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label"><?php echo $option['name']; ?></label>
          <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'date') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <div class="input-group date">
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span></div>
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'datetime') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
            <div class="input-group datetime">
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
              </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
              </div>
              <?php } ?>
              <?php } ?>
              <?php } ?>
              <?php if ($recurrings) { ?>
              <h3><?php echo $text_payment_recurring ?></h3>
              <div class="form-group required">
                <select name="recurring_id" class="form-control">
                  <option value=""><?php echo $text_select; ?></option>
                  <?php foreach ($recurrings as $recurring) { ?>
                  <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                  <?php } ?>
                </select>
                <div class="help-block" id="recurring-description"></div>
              </div>
              <?php } ?>  
              <div class="form-group">
               <?php if ($quantity <= 0) { ?>          
               <button class="oos button btn-block" type="button" disabled>
                Stock Over!
              </button>
              <?php } else { ?>
              
              <div class="qty-product">
                <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
                <input type="button" id="minus" value="-" class="form-control" />
                <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                <input type="button" id="plus" value="&#43;" class="form-control"/></div>&nbsp; &nbsp;  
                
                <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
                <?php } ?>
                <a id="button-wa" class="waorder" href="https://api.whatsapp.com/send?phone=917738442817&text=I'd%20like%20to%20order%20<?php echo $heading_title; ?>"/>
                 <img src="image/catalog/images-08-19/svg-icons/whatsapp-product-page.svg" style="height: 38px;"></a>
               </div>
               
               <?php if ($minimum > 1) { ?>
               <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
               <?php } ?>
             </div>

             <?php if ($price) { ?>
             <ul class="list-unstyled">
              <?php if ($tax) { ?>
              <!--<li><?php echo $text_tax; ?><span class="ex-text"><?php echo $tax; ?></span></li> -->
              <?php } ?>
              <?php if ($points) { ?>
              <!--<li><?php echo $text_points; ?><span class="ex-text"><?php echo $points; ?></span></li> -->
              <?php } ?>
              <?php if ($discounts) { ?>
              <?php foreach ($discounts as $discount) { ?>
              <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><span class="ex-text"><?php echo $discount['price']; ?></span></li>
              <?php } ?>
              <?php } ?> 
            </ul>
            <?php } ?>

            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
            
            <!--- POSTCODE --->
            <?php 
            if($pincode_install_status['total'] > 0){
            if(isset($product_page_status) && $product_page_status == '1'){ 
            ?>
            <div style="width:100%; float:left; overflow: none;">
              <form method = "POST" id = "pincheck1" <?php if(isset($_SESSION['pincode'])){ echo "style='display:none;'";}?>>
                <input placeholder="Enter Delivery Pincode" type="text" id="pin1" style="padding: 5px;">
                <input type = "button"  onClick = "getdata1()" value="Check" id="button-check" class="button" style="background: #000 !important;border-radius: 0px !important;padding: 7px 12px 7px 12px;"/>
                <div id="show_message" style="display:none; color: red;">Enter Delivery Pincode</div>
              </form>
              <div id="msg1" >
                <?php
                if(isset($_SESSION['pincode'])){
                echo "<div id='msg' ><span><i class='fa fa-map-marker'></i> Pincode : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display:inline;'><input type = 'button'  onclick = 'showform1()' value='Change' id='button-change' class='button'/></form></span><br/>";
                    if(isset($_SESSION['pin_check_delivery']) &&        $_SESSION['pin_check_delivery'] != ''){
                      echo $_SESSION['pin_check_delivery'];
                    }
                    echo $_SESSION['pin_check_result'];
                    echo"</font></font></div>";
                }
            ?>
          </div>
        </div>          
        <?php } }?>
        <!--- POSTCODE --->
        
        <br /><br />
        
        
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
        <!-- AddThis Button END -->
      </div>
      </div>
    </div>
    <div class="col-3">
  
     
           <div class="module-title" style="margin: 5px 0 5px 0 !important;    border-bottom: 1px solid #ebebeb;"><span><?php echo $tab_description; ?> </span><br /></div>
        <div class="tab-pane active product-description"><?php echo $description; ?></div>
        
     <?php if ($product_type != 4) { ?> 
       <div class="module-title" style="margin: 25px 0 5px 0 !important;    border-bottom: 1px solid #ebebeb;"><span><?php echo $tab_attribute; ?> </span><br /></div><?php } ?>    
        <?php if ($attribute_groups) { ?>
        <div class="tab-pane">
          <table class="table table-bordered">
            <?php foreach ($attribute_groups as $attribute_group) { ?>
            <thead>
              <tr>
                <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
              <tr>
                <td><?php echo $attribute['name']; ?></td>
                <td><?php echo $attribute['text']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
            <?php } ?>
          </table>
        </div>
        <?php } ?>
        
        
        <div class="module-title" style="margin: 25px 0 5px 0 !important;    border-bottom: 1px solid #ebebeb;"><span><?php echo $tab_review; ?> </span><br /></div>
        <?php if ($review_status) { ?>
        <div class="tab-pane">
          
          


          <form class="form-horizontal" id="form-review">
            <div id="review"></div>
            <div class="module-title" style="margin: 25px 0 5px 0 !important;    border-bottom: 1px solid #ebebeb;"><span><?php echo $text_write; ?></span></div>
            <?php if ($review_guest) { ?>
            <div class="form-group required">
              <div class="col-md-6 col-xs-12">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
              </div>
            </div>
            <div class="form-group required">
              <div class="col-md-6 col-xs-12">
                  
                  
                <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                
                
                <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                <div class="help-block"><?php echo $text_note; ?></div>
              </div>
            </div>
            <div class="form-group required">
              <div class="col-md-6 col-xs-12">
               
                <!-- star-rating start -->
                <div class="form-rating">
                  <label class="control-label"><?php echo $entry_rating; ?></label>
                  <div class="form-rating-container">
                    <input id="rating-5" type="radio" name="rating" value="5" />
                    <label class="fa fa-stack" for="rating-5">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-4" type="radio" name="rating" value="4" />
                    <label class="fa fa-stack" for="rating-4">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-3" type="radio" name="rating" value="3" />
                    <label class="fa fa-stack" for="rating-3">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-2" type="radio" name="rating" value="2" />
                    <label class="fa fa-stack" for="rating-2">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-1" type="radio" name="rating" value="1" />
                    <label class="fa fa-stack" for="rating-1">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                  </div>
                </div>
                <!-- star-rating end -->
              </div>
            </div>
            <?php echo $captcha; ?>
            <div class="buttons clearfix">
              <div class="pull-left">
                <button type="button" id="button-review" class="btn btn-primary">Add Comment</button>
              </div>
            </div>
            <?php } else { ?>
            <?php echo $text_login; ?>
            <?php } ?>
          </form>
        </div>
        <?php } ?>
        
        
             
    </div>
    <?php echo $content_bottom; ?>
  </div>
  <?php echo $column_right; ?>
</div><?php if ($products) { ?>
<div class="related-product-container quickview-product">
  <div class="module-title"><span>Recommended for you</span></div>
  <div class="row">
    <div class="related-product quickview-added qv-wtext">
      <?php foreach ($products as $product) { ?>
      <div class="row_items" style="margin-bottom: 5px; border-bottom: 1px solid #ebebeb;border-right: 1px solid #ebebeb;border-left: 1px solid #ebebeb;border-top: 1px solid #ebebeb;height: 380px;">
        <div class="product-layout product-grid">
          <div class="product-thumb item-inner">
            <div class="images-container">
              <a href="<?php echo $product['href']; ?>">
                <?php if ($product['thumb']) { ?>
                
                <?php if($product['rotator_image']){ ?>
                <img class="image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" style="max-width: 70% !important;"/>
                <?php } ?>
                <?php } else { ?>
                <img src="image/cache/no_image-100x100.png" alt="<?php echo $product['name']; ?>" />
                <?php } ?>
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="image1" style="max-width: 70% !important;"/>
              </a>
        <!--  <div class="actions"> 
            <div class="add-to-links">
              <div class="cart">
                <button class="button btn-cart" type="button" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><span><span><?php echo $button_cart; ?></span></span></button>
              </div>
             
            </div>
          </div> -->
          
          <?php if (isset($product['rating'])) { ?>
          <center>    <div class="ratings">
            <div class="rating-box">
              <?php for ($i = 0; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] == $i) {
              $class_r= "rating".$i;
              echo '<div class="'.$class_r.'">rating</div>';
            } 
          }  ?>
        </div>
      </div> </center>
      <?php } ?>
      
    </div>
    <div class="des-container">
      <div class="name-wishlist">
        <h2 class="product-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
        
      </div>
      <p class="product-des"><?php //echo $product['description']; ?></p>
      <div class="price-rating">
        <div class="price-label">
          <?php if ($product['price']) { ?>
          <p class="price">
            <?php if (!$product['special']) { ?>
            <!--<?php echo $product['price']; ?>-->
            <?php if ($product['joseanmatias_preco_cupom']) { ?>
            <strike class="price-old"> <?php echo                    $product['price']; ?></strike>&nbsp;&nbsp;<?php echo $product['joseanmatias_preco_cupom'];} else { ?>
            <?php echo $product['price']; ?>
            <?php } ?>
            <?php } else { ?>
            <span class="price-new"><?php echo $product['special']; ?></span>
            <span class="price-old"><?php echo $product['price']; ?></span>
            <?php } ?>
          </p>
          <?php } ?>
        </div>

      </div>
    </div>
    
  </div>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
<?php } ?>
<!--SOF Related Recipe-->
<?php if ($recipes) { ?>
    <div class="related-product-container quickview-product">
        <div class="module-title">
            <span>Recommended Recipes for you</span>
        </div>
        <div class="row">
            <div class="related-product quickview-added qv-wtext">
                <?php foreach ($recipes as $recipe) { ?>
                    <div class="recipe_row_items" style="margin-bottom: 5px; border-bottom: 1px solid #ebebeb;border-right: 1px solid #ebebeb;border-left: 1px solid #ebebeb;border-top: 1px solid #ebebeb;height: 195px;">
                        <div class="product-layout product-grid">
                            <div class="product-thumb item-inner">
                                <div class="images-container">
                                    <a href="<?php echo $recipe['href']; ?>">
                                        <img src="<?php echo $recipe['thumb']; ?>" alt="<?php echo $recipe['name']; ?>" title="<?php echo $recipe['name']; ?>" class="image1"/>
                                    </a>
                                </div>
                                <div class="des-container">
                                    <div class="name-wishlist">
                                        <h2 class="product-name"><a href="<?php echo $recipe['href']; ?>"><?php echo $recipe['name']; ?></a></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!--EOF Related Recipe-->
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("input[type=\'radio\']:disabled").parent().css({"background" : "#ddd", "cursor" : "not-allowed"});
        $("input[type=\'radio\']:disabled").next("span").css({"user-select" : "none"});
    });
</script>
<script type="text/javascript"><!--
  $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
    $.ajax({
      url: 'index.php?route=product/product/getRecurringDescription',
      type: 'post',
      data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
      dataType: 'json',
      beforeSend: function() {
        $('#recurring-description').html('');
      },
      success: function(json) {
        $('.alert, .text-danger').remove();

        if (json['success']) {
          $('#recurring-description').html(json['success']);
        }
      }
    });
  });
  //--></script>
  <script type="text/javascript"><!--
    $('#button-cart').on('click', function() {
      $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        data: $('input[name=\'product_id\'], #input-quantity, #product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
        dataType: 'json',
        beforeSend: function() {
          $('#button-cart').button('loading');
        },
        complete: function() {
          $('#button-cart').button('reset');
        },
        success: function(json) {
          $('.alert, .text-danger').remove();
          $('.form-group').removeClass('has-error');

          if (json['error']) {
            if (json['error']['option']) {
              for (i in json['error']['option']) {
                var element = $('#input-option' + i.replace('_', '-'));

                if (element.parent().hasClass('input-group')) {
                  element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                } else {
                  element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                }
              }
            }

            if (json['error']['recurring']) {
              $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
            }

        // Highlight any found errors
        $('.text-danger').parent().addClass('has-error');
      }

      if (json['success']) {
        //$('body').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        // Added to cart
        var success_text = '<div style="margin: 0 0 10px 0">\
        <div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt=""></div>\
        <div  style="text-align: center" >' + json['success'] + '</div>\
        </div>';

        var n = new Noty({
          type: 'success',
          layout: json['notice_add_layout'],
          text:  success_text ,
          textAlign:"right",
          animateOpen:{"height":"toggle"},
          timeout: json['notice_add_timeout'],
          progressBar: false,
          closeWith: 'button',
        }).show();
            // Added to cart
            $('#cart-total').html(json['total']);

        //$('html, body').animate({ scrollTop: 0 }, 'slow');

        $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
    });
    //--></script>
    <script type="text/javascript"><!--
      $('.date').datetimepicker({
        pickTime: false
      });

      $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
      });

      $('.time').datetimepicker({
        pickDate: false
      });

      $('button[id^=\'button-upload\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
          clearInterval(timer);
        }

        timer = setInterval(function() {
          if ($('#form-upload input[name=\'file\']').val() != '') {
            clearInterval(timer);

            $.ajax({
              url: 'index.php?route=tool/upload',
              type: 'post',
              dataType: 'json',
              data: new FormData($('#form-upload')[0]),
              cache: false,
              contentType: false,
              processData: false,
              beforeSend: function() {
                $(node).button('loading');
              },
              complete: function() {
                $(node).button('reset');
              },
              success: function(json) {
                $('.text-danger').remove();

                if (json['error']) {
                  $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                }

                if (json['success']) {
                  alert(json['success']);

                  $(node).parent().find('input').attr('value', json['code']);
                }
              },
              error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
            });
          }
        }, 500);
      });
      //--></script>
      <script type="text/javascript"><!--
        $('#review').delegate('.pagination a', 'click', function(e) {
          e.preventDefault();

          $('#review').fadeOut('slow');

          $('#review').load(this.href);

          $('#review').fadeIn('slow');
        });

        $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

        $('#button-review').on('click', function() {
          $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            complete: function() {
              $('#button-review').button('reset');
            },
            success: function(json) {
              $('.alert-success, .alert-danger').remove();

              if (json['error']) {
                $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
              }

              if (json['success']) {
                $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                $('input[name=\'name\']').val('');
                $('textarea[name=\'text\']').val('');
                $('input[name=\'rating\']:checked').prop('checked', false);
              }
            }
          });
        });
        var minimum = <?php echo $minimum; ?>;
        $("#input-quantity").change(function(){
          if ($(this).val() < minimum) {
            alert("Minimum Quantity: "+minimum);
            $("#input-quantity").val(minimum);
          }
        });
  // increase number of product
  function minus(minimum){
    var currentval = parseInt($("#input-quantity").val());
    $("#input-quantity").val(currentval-1);
    if($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum){
      alert("Minimum Quantity: "+minimum);
      $("#input-quantity").val(minimum);
    }
  };
  // decrease of product
  function plus(){
    var currentval = parseInt($("#input-quantity").val());
    $("#input-quantity").val(currentval+1);
  };
  $('#minus').click(function(){
    minus(minimum);
  });
  $('#plus').click(function(){
    plus();
  });
  // zoom
  $(".thumbnails img").elevateZoom({
    zoomType : "window",
    cursor: "crosshair",
    gallery:'gallery_01', 
    galleryActiveClass: "active", 
    imageCrossfade: true,
    responsive: true,
    zoomWindowOffetx: 0,
    zoomWindowOffety: 0,
  });
  // slider  
  $(".image-additional").owlCarousel({
    navigation: true,
    pagination: false,
    slideSpeed : 500,
    goToFirstSpeed : 1500,
    addClassActive: true,
    items : 5, 
    itemsDesktop : [1199,4],
    itemsDesktopSmall : [1024,3],
    itemsTablet: [640,3],
    itemsMobile : [480,3],
    afterInit: function(){
      $('#gallery_01 .owl-wrapper .owl-item:first-child').addClass('active');
    },
    beforeInit: function(){
      $(".image-additional .thumbnail").show();
    }
  }); 
  $('#gallery_01 .owl-item .thumbnail').each(function(){
    $(this).click(function(){
      $('#gallery_01 .owl-item').removeClass('active2');
      $(this).parent().addClass('active2');
    });
  });
  // related products  
  $(".related-product").owlCarousel({
    navigation: true,
    pagination: false,
    slideSpeed : 500,
    goToFirstSpeed : 1500,
    items : 4, 
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [767,2],
    itemsTablet: [640,2],
    itemsMobile : [480,1],
    addClassActive: true,
    afterAction: function(el){
     this.$owlItems.removeClass('before-active')
     this.$owlItems.removeClass('last-active')
     this.$owlItems .eq(this.currentItem).addClass('before-active')  
     this.$owlItems .eq(this.currentItem + (this.owl.visibleItems.length - 1)).addClass('last-active') 
   }
 });
 //--></script>

 <script type="text/javascript"><!--
  $('#product input[type="radio"], #product input[type="checkbox"]').click(function() {
    $('#product input[type="radio"], #product input[type="checkbox"]').each(function() {
      $(this).parents("label").toggleClass('checked-option', this.checked);
    });
  });
  //--></script>
  
  <!--- POSTCODE --->     
  <style>
    #msg1 p{margin:0;}
    #msg1{color:#000}
    #msg1 .fa-2x{font-size:2em!important;}
  </style>
  <script type="text/javascript">
    $('#pincheck1').submit(function (e) {
     e.preventDefault();
     getdata1();
   });
    function getdata1(){
      var pin = $("#pin1").val();
      if(pin != ''){
        $.ajax({
          type: "POST",
          url: "?route=product/product/pinCheck",
          data: {pincode: pin},
          dataType : "text"
        }).done(function( result ) 
        {
          $("#msg1").show();
          $("#msg1").html( result );
          $("#pincheck1").hide();
          $("#show_message").hide();
          
        });
      }
      else{
        $("#show_message").show();
      }
    }
    function showform1(){
      $("#msg1").hide();
      $("#pincheck1").show();
    }
  </script>
  <!--- POSTCODE --->

  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '355421528440199');
      fbq('track', 'AddToCart');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=355421528440199&ev=AddToCart&noscript=1"
      /></noscript>
      <!-- End Facebook Pixel Code -->
      
<script src="/catalog/view/javascript/materialize.min.js"></script>
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script-->
<script type="text/javascript">
	$(document).ready(function(){
    	$('.carousel').carousel();
 	});     
</script>
<?php echo $footer; ?>