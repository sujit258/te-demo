<style type="text/css">
    @import url("https://fonts.googleapis.com/css?family=Amatic+SC");
    
    @media screen and (min-width:300px) and (max-width:500px) {
        .icons{
            padding:0 !important;
        }
        
        .icons span.h3{
            font-size: 14px;
        }
    }

    body {
      margin: 0px;
      padding: 0px;
      font-family: "gothamBold";s
      overflow-x: hidden;
    }
    
    .p0 {
        padding: 0;
    }
    
    .msg11 {
        font-size: 15px;
    }
    
    .msg12 {
        font-size: 15px;
        margin-top: 4px;
    }
    
    .cat-image .images-container {
        margin-bottom: 15px;
    }
    .cat-image .images-container img {
        cursor: pointer;
    }
    
    .qty-container {
        width: 50%;
    }
    .breadcrumb {
        margin-bottom: 15px!important;
    }
    
    .category-title {
        font-size: 35px;
    }
    .sew {
      width: 100%;
    }
    
    .banner-text-lg {
        font-size: 6em!important;
    }
    
    .banner-text-md {
        font-size: 4em!important;
    }
    
    h1 {
      font-size: 6em;
      font-weight: bold;
    }
    
    h3 {
      font-size: 4em;
    }
    .intro {
      position: absolute;
      text-align: center;
      top: 280px;
      left: 470px;
    }
    
    .space {
      height: 80px;
    }
    
    h2 {
      font-weight: bold;
    }
    
    .space1 {
      padding: 0;
      margin: 0;
      background-color: #f49a25;
      height: 50px;
      text-align: center;
      font-size: 2em;
    }

  .product-image {
      padding: 5%;
  }
  h2 {
      text-align: center;
  }
  
  .product-name-text{
        font-size: 16px;
        font-weight: 600;
        text-align: center; 
        font-family: roboto, 'Oswald', sans-serif;
    }
    .product-name-text a {
        color: #000;
    }
    .options {
        padding: 0px!important;
        min-height: 55px!important;
    }
    .product-name-text {
        min-height: 40px;
        text-align: left;
    }
    .product-options {
        text-align: center;
        float: left;
        background: #fff!important;
        width: 90%;
        color: #000;
    }
    .button-product-cart {
        float: right!important;
        font-size: 13px!important;
        background: #f49a25;
        color: #fff;
    }
    .button-personalize {
        font-size: 13px!important;
        float: right !important;
        background: #f49a25;
        color: #fff;
        margin-top: 0px;
    }
    .price-label {
        padding: 5px 0px !important;
        min-height: 38px!important;
        width: 100%!important;
    }
    .price {
        margin-bottom: 0px!important;
        min-height: 22px!important;
    }
    .quantity {
        height: 35px!important;
        width: 45%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    .oos {
        background: #777!important;
        border-radius: 4px!important;
        float: right!important;
        font-size: 13px!important;
        color: #fff;
    }
    .oos:hover {
        cursor: not-allowed!important;
        color: #fff!important;
    }
    .button-product-cart:focus, .button-personalize:focus {
        color: #fff!important;
    }
    .button-product-cart:hover, .button-personalize:hover {
        color: #fff!important;
        background: #000!important;
    }
    .center {
        text-align: center;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 23px !important;
        padding: 9px 10px !important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .category-title {
        margin: 70px 0 20px 0;
    }
    .main-row {
        margin-right: 0px!important;
        margin-left: 0px!important;
    }
    
    .price-coupon{
        font-weight: bold;
        padding-right: 5px;
    }
    .price-new {
        display: inline-block!important;
        font-size: 15px!important;
        color: #f49a25!important;
        text-align: left!important;
        font-weight: 700!important;
        margin: 0!important;
    }
    .price-old {
        display: inline-block!important;
        color: #5c5c5c!important;
        font-size: 12px!important;
        margin-left: 5px!important;
        text-decoration: line-through!important;
    }
    .deals-overlay {
        right: 0px;
        top: 10px;
        position: absolute;
        background:#f49a25;
        color:#fff;
        padding: 4px;
        font-weight: 600;
        width: 25%;
        text-align: center;
        z-index: 1;
    }
    .top-banner {
        margin-top: 0!important;
        margin-bottom: 25px!important;
    }
    .top-banner img {
        width: 100%;
    }
    .top-image img {
        width: 100%;
    }
    .top-banners {
        margin-bottom: 25px;
    }
    .readyToEatBanner, .readyToCookBanner, .seedsMixesBanner, .rawAndroastedSeedsBanner {
        cursor: pointer;
    }
    #readyToEatBreakfast, #seedsMixes, #readyToCookBreakfast, #rawAndRoastedSeeds, #others, #tea, #wcbs, #combo {
        padding-top: 15px;
    }
    
    .sub-category {
        font-size: 25px;
        margin-top: 25px;
        font-weight: bold;
    }
    .price, .coupon {
        padding-left: 0px!important;
        padding-right: 0px!important;
    }
    .coupon {
        min-height: 20px!important;
        text-align: left;
        font-size: 12px!important;
        margin-bottom: 0!important;
    }
    .options .radio input {
        display: none!important;
    }
    .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px!important;
    }
    .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0!important;
        text-align: center;
        min-width: 52px;
    }
    .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    .product-container {
        padding: 0!important;
    }
    @media(min-width: 1000px) {
        .mobile {
            min-height: 570px!important;
        }
        .youtubeVideo {
            padding: 0 30px!important;
        }
    }
    @media (max-width: 767px){
        .youtubeVideo {
            padding: 0!important;
        }
        .qty-container {
            width: 70%!important;
        }
        .coupon {
            text-align: left!important;
        }
        .intro {
            position: absolute;
            text-align: center;
            top: 15%;
            left: 22%;
        }
        .button-personalize {
            font-size: 14px!important;
            margin-top: 0px!important;
        }
        .product-name-text {
            min-height: 40px!important;
            font-size: 14px!important;
        }
        .banner-text-lg {
            font-size: 3em!important;
        }
        .banner-text-md {
            font-size: 1.5em!important;
        }
        .category-title {
            margin: 40px 0 20px 0!important;
            font-size: 28px!important;
        }
        .mobile{
            margin-left: 0px;
            margin-bottom: 10px;
            min-height: 470px!important;
            padding: 7px!important;
        } 
        .container {
            width: 100%!important;
        }
        .product-name-text .select{
            height: 90px;
            font-size: 20px;
            text-align: center; 
            font-family: roboto, 'Oswald', sans-serif;
        }
        .deals-overlay {
            width: 35%!important;
        }
        .readyToEatBanner, .readyToCookBanner, .seedsMixesBanner, .rawAndroastedSeedsBanner {
            margin-top: 10px;
        }
        .quantity-container {
            width: 60%!important;
        }
        .radio label span {
            font-size: 12px!important;
        }
        .options .radio {
            margin: 0px 0px 5px -2px!important;
        }
        .options {
            min-height: 55px!important;
        }
        .category-name h4 {
            line-height: 15px;
            font-size: 12px!important;
            padding: 8px 1px!important;
            margin-top: 0!important;
        }
        .category-name, .bookmark-heading {
            padding-left: 5px!important;
            padding-right: 5px!important;
        }
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .category-name h4 {
        border: 1px solid #c6c6c6!important;
        border-radius: 4px;
        padding: 14px;
        margin-top: 5px!important;
        font-size: 12px!important;
        font-weight: bold;
        text-align: center;
    }
    .category-name h4:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
        cursor: pointer!important;
    }
    .bookmark-heading {
        font-weight: 600;
    }
    
    .deals-discount {
        border-radius: 50%;
        right: 0px;
        top: 5%;
        position: absolute;
        background: #f49a25;
        text-align: center;
        z-index: 5;
        width: 60px;
        height: 60px;
    }
    
    .deals-discount h5 {
        color: #fff;
        font-weight: 600;
        padding: 1px;
        line-height: 15px;
        margin-top: 15px;
    }
</style>

  <?php echo $header; ?>
  <ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
  </ul>

<div class="container-fluid">
    <div class="row top-banner hidden-xs hidden-sm">
        <div class="category-product-carousel w3-content w3-section">
            <a href="/index.php?route=checkout/cart&product_id=6512&option_id=1&option_value_id=377"><img alt="True Elements" class="mySlides img-responsive" src="<?php echo $granola_combo_desktop; ?>"></a>
            <a href="/index.php?route=checkout/cart&product_id=4535&option_id=1&option_value_id=80"><img alt="True Elements" class="mySlides img-responsive" src="<?php echo $slider_raw_pumpkin_seeds_desktop; ?>"></a>
            <a href="/index.php?route=checkout/cart&product_id=6481&option_id=1&option_value_id=80"><img alt="True Elements" class="mySlides img-responsive" src="<?php echo $slider_daily_dose_trail_mix_desktop; ?>"></a>
		</div>

        <script type="text/javascript">
            $(document).ready(function() {
                $(".category-product-carousel").owlCarousel({
                    autoPlay: true,
                    items : 1,
                    slideSpeed : 1000,
                    navigation : true,
                    pagination : true,
                    controls: true,
                    stopOnHover : false,
                    itemsDesktop : [1199,1],
                    itemsDesktopSmall : [991,1],
                    itemsTablet: [700,1],
                    itemsMobile : [400,1],
                });
            });
        </script>
    </div>
    <div class="row top-banner hidden-md hidden-xl hidden-lg">
        <div class="category-product-mobile-carousel w3-content w3-section">
            <a href="/index.php?route=checkout/cart&product_id=6512&option_id=1&option_value_id=377"><img alt="True Elements" class="mySlides img-responsive" src="<?php echo $granola_combo_mobile; ?>"></a>
            <a href="/index.php?route=checkout/cart&product_id=4535&option_id=1&option_value_id=80"><img alt="True Elements" class="mySlides img-responsive" src="<?php echo $slider_raw_pumpkin_seeds_mobile; ?>"></a>
            <a href="/index.php?route=checkout/cart&product_id=6481&option_id=1&option_value_id=80"><img alt="True Elements" class="mySlides img-responsive" src="<?php echo $slider_daily_dose_trail_mix_mobile; ?>"></a>
		</div>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".category-product-mobile-carousel").owlCarousel({
                    autoPlay: true,
                    items : 1,
                    slideSpeed : 1000,
                    navigation : true,
                    pagination : true,
                    controls: true,
                    stopOnHover : false,
                    itemsDesktop : [1199,1],
                    itemsDesktopSmall : [991,1],
                    itemsTablet: [700,1],
                    itemsMobile : [400,1],
                });
            });
        </script>
    </div>
     
    <style>
        .inline-photo{
            text-align:center;
        }
        
        .catbox:hover {
            cursor: pointer!important;
        }
        
        .productImg {
            width: 100%;
            float: left;
            position: relative;
            left: 0%;
            margin: 0 0 5px 0;
        }
        .productImg {
            left: 5%;
        }
    
        .productImg img {
            width: 80%;
            float: left;
        }
        
        .productImg {
            float: left;
            position: relative;
            margin: 0 0 5px 0;
        }
        
        @media only screen and (min-width:1366px){
            .productImg {
                width: 92%;
                left: 12%;
            }
        }
        
        .h3, h3 {
            font-size: 15px;
        }
    </style>   
    <div class="container">
        <div class="col-md-12 hidden-xs hidden-sm icons">
            <div class="row">
            	<div class="col-md-2 col-sm-6 col-xs-6 col-lg-offset-1 catbox oatsbookmark">
            		<div class="inline-photo show-on-scroll is-visible"> 
            		<a class="productImg"><img src="<?php echo $oats_and_oatmeal_icon; ?>" alt="Wholegrain Muesli"></a>
            			<span class="h3">Oats &amp; Oatmeal</span>
            		</div>
            	</div> 
            
            	<div class="col-md-2 col-sm-6 col-xs-6 catbox cerealsbookmark">
            		<div class="inline-photo show-on-scroll is-visible"> 
            			<a class="productImg"><img src="<?php echo $cereals_icon; ?>" alt="Cereals"></a>
            			<span class="h3">Cereals</span>
            		</div>
            	</div> 
            	
            	<div class="col-md-2 col-sm-6 col-xs-6 catbox breakfast">
            		<div class="inline-photo show-on-scroll is-visible"> 
            			<a class="productImg"><img src="<?php echo $breakfast_mix_icon; ?>" alt="Breakfast Mix"></a>
            			<span class="h3">Breakfast Mix</span>
            		</div>
            	</div> 
            
            	<div class="col-md-2 col-sm-6 col-xs-6 catbox sgbookmark">
            		<div class="inline-photo show-on-scroll is-visible"> 
            			<a class="productImg"><img src="<?php echo $supergrains_icon ?>" alt="Roasted Seeds"></a>
            			<span class="h3">Supergrains</span>
            		</div>
            	</div> 
            
            	<div class="col-md-2 col-sm-6 col-xs-6 catbox hsbookmark">
            		<div class="inline-photo show-on-scroll is-visible"> 
            			<a class="productImg"><img src="<?php echo $healthy_seeds_icon; ?>" alt="Seeds Mixes"></a>
            			<span class="h3">Healthy Seeds</span>
            		</div>
            	</div> 
            	<div class="col-md-2 col-sm-6 col-xs-6 col-lg-offset-1 catbox rsbookmark">
            		<div class="inline-photo show-on-scroll is-visible"> 
            		<a class="productImg"><img src="<?php echo $roasted_seed_mixes_icon; ?>" alt="Wholegrain Muesli"></a>
            			<span class="h3">Roasted Seeds</span>
            		</div>
            	</div> 
            	
            	<div class="col-md-2 col-sm-6 col-xs-6 catbox seedsmixes">
            		<div class="inline-photo show-on-scroll is-visible"> 
            		<a class="productImg"><img src="<?php echo $seeds_mixes_icon; ?>" alt="Seeds Mixes"></a>
            			<span class="h3">Seeds Mixes</span>
            		</div>
            	</div> 
            
            	<div class="col-md-2 col-sm-6 col-xs-6 catbox stbookmark">
            		<div class="inline-photo show-on-scroll is-visible"> 
            			<a class="productImg"><img src="<?php echo $sweet_tooth_icon; ?>" alt="Roasted Seeds"></a>
            			<span class="h3">Sweet Tooth</span>
            		</div>
            	</div> 
                
                <div class="col-md-2 col-sm-6 col-xs-6 catbox otgbookmark">
            		<div class="inline-photo show-on-scroll is-visible"> 
            			<a class="productImg"><img src="<?php echo $on_the_go_snacks_icon; ?>" alt="Cereals"></a>
            			<span class="h3">On the Go Snacks</span>
            		</div>
            	</div> 
            	
            	<div class="col-md-2 col-sm-6 col-xs-6 catbox vpbookmark">
            		<div class="inline-photo show-on-scroll is-visible"> 
            			<a class="productImg"><img src="<?php echo $value_packs_icon; ?>" alt="Seeds Mixes"></a>
            			<span class="h3">Value Packs</span>
            		</div>
            	</div> 
            </div>
        </div>
    </div>    
</div>

<input type="hidden" name="product_id" value="" />

    <div class="row main-row">
        <div class="container">
            <?php foreach ($shopcatgeories as $shopcatgeory) { ?> 
                <div class="col-md-12 col-xs-12 col-sm-12 product-container" id="<?php echo $shopcatgeory['class_name']; ?>">
                    <?php if($shopcatgeory['name'] == "Others") { ?>
                        <h4 class="sub-category">For Your Sweet Tooth</h4>
                    <?php } else if($shopcatgeory['name'] == "Flakes &amp; Bix" || $shopcatgeory['name'] == "Flakes & Bix") { ?>
                        <h4 class="sub-category">Flakes</h4>
                    <?php } else { ?>
                        <h4 class="sub-category"><?php echo $shopcatgeory['name']; ?></h4>
                    <?php } ?>
                    
                <hr/>
                <?php $i = 0; ?>
                <?php foreach ($products as $product) { ?>
                    <?php if($product['category_name'] == $shopcatgeory['name']) { ?>
                        <?php if($shopcatgeory['name'] == 'Gift Packs') { ?>
                            <?php if($i == 0) { ?>
                                <a class="hidden-xs hidden-sm" href="/index.php?route=product/gift_hampers"><img src="<?php echo $gifting_banner_desktop; ?>" /></a>
                                <a class="hidden-md hidden-lg" href="/index.php?route=product/gift_hampers"><img src="<?php echo $gifting_banner_mobile; ?>" /></a>
                            <?php } ?>
                            <?php $i++; ?>
                        <?php } else { ?>
                        <div class="col-md-3 col-sm-6 col-xs-6 mobile">
                        <div class="product-thumb item-inner">
					   <?php if($product['max_per_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo $product['max_per_discount']; ?></h5></div>
					    <?php } else if($product['discount']) { ?>
					        <div class="deals-discount"><h5><?php echo $product['discount']; ?>%<br/>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                            <a href="<?php echo $product['href']; ?>">
                                <div class="pro_img">
                                    <?php if($product['rotator_image']){ ?>
    									<img class="image2 product-image" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" />
    								<?php } ?>
        		                    <img alt="True Elements" class="product-image" src="<?php echo $product['thumb']; ?>">
        		                </div>
        	                </a>
    	                </div>
    	                <div class="ratings" itemprop="reviewRating" style="width: 100%;">
                            <div class="rating-box" style="min-height: 20px;">
                                <?php for ($i = 0; $i <= 5; $i++) { ?>
                                    <?php if ($product['rating'] == $i) {
                                        $class_r= "rating" . $i;
                                        if($product['rating'] > 0) {
                                            echo '<div class="' . $class_r . ' rating-img col-md-offset-3 col-xs-offset-1 col-sm-offset-3 col-md-5 col-xs-8 col-sm-3">rating</div>';
                                        } else {
                                            echo '<div class="' . $class_r . ' rating-img col-md-offset-3 col-xs-offset-1 col-sm-offset-3 col-md-5 col-xs-8 col-sm-3">rating</div>';
                                        }
                                        echo '<div style="text-align: right;" class="col-md-1 col-xs-2 col-sm-1 p0">(', $product['reviews'],')</div>';
                                    } 
                                }  ?>
                            </div>
                        </div>
                        <p class="product-name-text"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($product['options']) { ?>
    	                    <div class="options options-<?php echo $product['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                        <?php foreach($product['options'] AS $option) { ?>
                                    <?php $countoption = count($option['product_option_value']); ?>
                                    <?php $price_tooltip_radio = 1; ?>
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                        <div class="radio">
                                            <?php if($countoption == 1) { ?>
                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                    <input data-abc="<?php echo $option_value['price']; ?>" type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                            <?php } else { ?>
                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                    <input data-abc="<?php echo $option_value['price']; ?>" type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                            <?php } ?>
                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                <span><?php echo $option_value['name']; ?></span>
                                            <?php } ?>
                                            </label>
                                        </div>
                                    <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($product['product_type'] != 5) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	   			                <?php if ($product['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$product['special']) { ?>
                                                <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($product['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $product['product_id']; ?>"><b> <?php echo $product['coupon_discount']; ?> </b></span><span> <?php echo $product['coupon'] ? 'Using code: <b>' . $product['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			    	                
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty != 1 && $product['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $product['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>)" />
    		                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $product['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $product['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart(<?php echo $product['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $product['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($product['quantity'] > 0) { ?>
						        <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>  
                    <?php } ?>
                    <?php } ?>
                <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>


<!--/div></div-->
<!-- main close -->
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left" ><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center">' + json['success'] + '</div></div>';
                    
                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('.price-new-' + product_id).html(json.new_price.special);
					$('.price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.coupon) {
					    $('.price-coupon-' + product_id).html(json.new_price.coupon); 
					}
					 
					if(json.new_price.special) {
					    $('.price-new-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('.price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        }); 
	     
        $(function() {
            let isMobile = window.matchMedia("only screen and (max-width: 1000px)").matches;

            if (isMobile) {
                $(".oatsbookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#oats").offset().top-100
                    }, 1000);
                });
                
                $(".cerealsbookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#granola").offset().top-100
                    }, 1000);
                });
                
                $(".breakfast").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#breakfast").offset().top-100
                    }, 1000);
                });
                
                $(".sgbookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#supergrains").offset().top-100
                    }, 1000);
                });
                
                $(".seedsmixes").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#seedsmixes").offset().top-100
                    }, 1000);
                });
                
                $(".hsbookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#rawseeds").offset().top-100
                    }, 1000);
                });
                
                $(".rsbookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#roastedseeds").offset().top-100
                    }, 1000);
                });
                
                $(".otgbookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#otgsnacks").offset().top-100
                    }, 1000);
                });
                
                $(".stbookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#others").offset().top-100
                    }, 1000);
                });
                $(".vpbookmark").click(function() {
                    $("html, body").animate({
                        scrollTop: $("#giftpacks").offset().top-100
                    }, 1000);
                });
                
            } else {
                $(".oatsbookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#oats").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".cerealsbookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#granola").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".breakfast").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#breakfast").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".seedsmixes").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#seedsmixes").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".sgbookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#supergrains").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".hsbookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#rawseeds").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".rsbookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#roastedseeds").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".otgbookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#otgsnacks").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".stbookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#others").offset().top - offsetValue
                    }, 1000);
                });
                
                $(".vpbookmark").click(function() {
                    if($('header').hasClass('fix-nav')) {
                        var offsetValue = 150;
                    } else {
                        var offsetValue = 250;
                    }
                    $("html, body").animate({
                        scrollTop: $("#giftpacks").offset().top - offsetValue
                    }, 1000);
                });
            }
        }); 
       
	});
</script>
<script type="text/javascript"><!--
    $('.options .radio input[type="radio"]').click(function() {
        $('.options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>

 
<?php echo $footer; ?>