<?php echo $header; ?>
<style>
#content {
    margin-top: 35px;
}
    .review-right div {
        color:  #000;
    }
    .review-right div:hover {
        color:  #f49a25;
    }
    .reviews {
    background: #fff;
    padding: 15px 20px;
    margin: 10px 0;
    border: 1px solid #ddd;
    display: flex;
  }
  .reviews-avatar {
    background: #f49a25;
    color: #fff;
    height: 50px;
    width: 50px;
    text-align: center;
    display: table;
    float: left;
  }
  .reviews .reviews-wrap{
    margin-left: 30px;
  }
  .reviews .reviews-wrap h5{
    border-bottom: 1px solid #ddd;
    padding-bottom: 10px;
    margin: 5px 0;
  }
  .reviews-avatar i {
    font-size: 25px;
    display: table-cell;
    vertical-align: middle;
  }
  .reviews-author {
    font-style: italic;
    font-weight: 600;
  }
  .reviews-wrap .fa {
    color: #f49a25;
  }
  .fa-calendar {
    color: #000!important;
    margin-right: 10px!important;
  }
  .reviews-date {
    margin-top: 0!important;
  }
  .reviews-text {
    line-height: 22px!important;
    font-size: 15px;
  }
  .overall-reviews .fa {
        color: #f49a25;
        font-size: 20px !important;
    }
    .p0 {
        padding-left: 0;
        padding-right: 0;
    }
    .overall-reviews {
        display: block;
        padding: 15px;
        margin-bottom: 15px;
        background: #fcfcfc;
    }
    .progress-bar {
        background: #f49a25;
    }
    .overall-reviews .review-ratings-box {
        text-align: center;
    }
    @media(min-width: 768px) {
    .progress {
        width: 40%;
        height: 15px;
        margin-bottom: 15px;
    }
    .review-left, .review-right {
        min-height: 158px;
    }
    .review-right {
        padding-top: 15px;
    }
    .review-ratings {
        padding-top: 50px;
    }
    .reviews-author {
        display: inline;
    }
    .individual-rating {
        margin-top: 5px;
    }
    .individual-rating .fa {
        height: 15px;
    }
    .reviews-date {
        float: right;
    }
    .reviews-text {
        margin-top: 15px;
    }
    }
    @media(max-width: 767px) {
        .review-left {
            margin-bottom: 30px;
        }
        .individual-rating {
            width: 100%;
            display: block;
        }
        #review {
            width: 100%;
        }
        .progress {
            margin-bottom: 15px;
        }
    }
</style>
<!--<ul class="breadcrumb" style="background-image: none;">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  <?php } ?>
</ul>-->
<div class="container">
    <div id="content" class="col-md-12 col-xs-12 col-sm-12">
<?php if ($reviews) { ?>
<div class="module-title" style="margin-bottom: 0;">
    <span>Customer Reviews</span>
    <hr/>
    <span><a href="<?php echo $product_url; ?>"><?php echo $product_name; ?></a></span>
    <hr>
</div>
<div class="col-md-12 col-xs-12 col-sm-12 overall-reviews">
    <div class="col-md-6 col-xs-12 col-sm-12 review-left">
        <div class="col-md-4 col-xs-12 col-sm-12" style="padding-bottom: 15px;">
            <a href="<?php echo $product_url; ?>"><img src="<?php echo $image; ?>" alt="True Elements" /></a>
        </div>
        <div class="col-md-8 col-xs-12 col-sm-12 review-ratings" itemprop="reviewRating">
            <div class="review-ratings-box">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if (round($average_rating) < $i) { ?>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } else { ?>
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } ?>
                <?php } ?>
                <div class="review-ratings-text"><?php echo round($average_rating, 1), '/ 5 by ', $total_reviews, ' Customers'; ?></div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12 col-sm-12 review-right">
        <?php for($k = 5; $k >= 1; $k--) { ?>
            <div class="row">
                <?php if($rating_count[$k] > 0) { ?>
                    <a href="index.php?route=product/product/review&product_id=<?php echo $product_id; ?>&filter_rating=<?php echo $k; ?>">
                        <div class="col-md-2 col-xs-4 col-sm-6 p0"><?php echo $k; ?> Star (<?php echo $rating_count[$k]; ?>) </div>
                    </a>
                <?php } else { ?>
                    <div class="col-md-2 col-xs-4 col-sm-6 p0"><?php echo $k; ?> Star (<?php echo $rating_count[$k]; ?>) </div>
                <?php } ?>
                <div class="col-md-10 col-xs-8 col-sm-6 p0">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $rating_per[$k]; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $rating_per[$k]; ?>%;">      
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<div id="review">
<?php foreach ($reviews as $review) { ?>
<div class="reviews">
  	<div class="img-circle reviews-avatar"><i class="fa fa-user"></i></div>
  	<div class="reviews-wrap col-md-12 col-xs-12 col-sm-12 p0">
	  	<h4 class="reviews-author"><?php echo $review['author']; ?></h4>
	  	<h4 class="reviews-date hidden-xs hidden-sm"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
	  	<div class="individual-rating">
	  	    <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($review['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
            <?php } ?>
        </div>
        <h4 class="reviews-date hidden-md hidden-xl hidden-lg"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
	    <p class="reviews-text"><?php echo $review['text']; ?></p>
    </div>
</div>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p style="font-size: 15px;"><?php echo $text_no_reviews; ?></p>
<?php } ?>
</div>
</div>
</div>
<script type="text/javascript">
    $('#review .pagination a, .review-right a').click(function(e) {
        e.preventDefault();
        $('#review, .overall-reviews, .module-title').fadeOut('slow');
        $('#review').load(this.href);
        $('#review').fadeIn('slow');
    });
    $('.review-right a').click(function(){
        $("html, body").animate({
            scrollTop: $("#review").offset().top - 250
        }, 500);
    });
</script>
<?php echo $footer; ?>