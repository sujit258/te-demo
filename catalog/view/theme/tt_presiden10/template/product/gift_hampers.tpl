<?php echo $header; ?>
<style>
div #sticky {
    position: -webkit-sticky;
    position: sticky;
    top: 71px;
    margin-top: 49px;
}
.checkbox input {
    display: none;
}

#note{
    width: 300Px;
    height: 90px;
    border: 3px solid #cccccc;
    padding: 5px;
    font-family: Tahoma, sans-serif;
    background-image: url(bg.gif);
    background-position: bottom right;
    background-repeat: no-repeat;
    margin-bottom: 19px;
}

#is_invoice {
	margin: 0;
	vertical-align:middle;
}

.product_name {
    padding: 3px;
    min-height: 67px;
}
#product .control-label {
    font-size: 18px;
    font-weight: bold;
    color: #f49a25;
}

h6 {
    margin-bottom: 0px !important;
    margin-left: 15px !important;
    margin-top: 25px !important;
    line-height: 1.3 !important;
    padding-bottom: 10px !important;
}

#product h3 {
    text-align: center;
    color: #f49a25;
}

.checkbox + label {
    padding: 0px!important;
    display: inline-block;
    position: relative;
    cursor: pointer;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-align: center;
    font-size: 15px;
    color: #f49a25;
    width:auto;
    min-height:270px!important;
}

label img {
    min-height: 100%!important;
    width: auto;
    transition-duration: 0.2s;
    transform-origin: 50% 50%;
    position: relative;
}

:checked + label img {
    transform: scale(0.9);
    z-index: -1;
    position:relative;
}

:checked + label {
    background-image:linear-gradient(rgba(0, 0, 0, 0.7), transparent);
}

.checkbox {
    text-align: center;
    min-height: 390px;
    border: 1px solid rgb(28,0,85);
    margin: 30px 10px 30px!important;
    padding-right: 0;
    padding-left: 0;
    border-radius:10px;
    overflow: hidden;
    width: 30%;
    user-select: none;
}
@media screen and (max-width: 360px) {
    .product_name {
        min-height: 90px;
    }
}
@media screen and (max-width: 800px) {
    .fixed {
        position: relative!important; /*Temporary*/
    }
    .checkbox {
        width: 44% !important;
        min-height: 300px;
    }
    .priceOnMobile {
        display: inline-block!important;
        z-index: 1;
        background:rgba(255,255,255,0.9);
        width: 100%;
        position: fixed;
        top: 0px;
    }
    .main-price-mobile {
        position: relative;
        text-align: center;
    }
    .banner-container {
        margin-right: -15px;
        margin-left: -15px;
    }
    div #sticky {
        margin-top: 0px;
    }
    .nav li a {
        padding: 10px 10px!important;
        font-size: 14px!important;
    }
    .snack-types {
        margin-top: 25px;
    }
    .checkbox:hover p.tip {
        display: none!important;
    }
    .modal-dialog {
        top: 5%;
    }
    .upto-500-tab, .fibre-tab {
        padding-right: 2.5px;
    }
    .upto-1000-tab, .blank-tab {
        padding-left: 2.5px;
        padding-right: 2.5px;
    }
    .upto-1500-tab, .masti-tab {
        padding-right: 0px;
        padding-left: 2.5px;
    }
    .option-text {
        width: 91%!important;
    }
    .option-text p {
        font-size: 13px!important;
    }
}

.option_name {
    font-size: 15px;
    min-height: 40px;
}

.weight {
    font-size: 15px;
    color: #000000;
}

#product {
    max-height: 100%;
    margin-bottom: 0px;
}

.has-option {
    border: none;
    padding: 0px;
    box-shadow: none;
}

.option_help {
    color:#1C0055;
    font-size:15px;
    text-align:center;
}

#top_banner {
    padding-right: 0;
    padding-left: 0;
}

p {
    font-size: 15px;
    margin: 0;
}

.product-title-section {
    background-color: #A193A4;
    margin-top: 10px;
}

.product-title-subsection {
    background-image: linear-gradient(to bottom right,rgba(255, 255, 255, 0),rgba(0, 0, 0, 0.5));
    padding: 45px 20px;
}

.product-title{
    font-size: 28px!important;
    margin-bottom: 0px!important;
    margin-top: 0px!important;
    color: #ffffff!important;
    text-transform: none!important;
}

.selected-options h3 {
    font-size: 17px!important;
    font-weight: 600!important;
    text-align:left!important;
    margin-top: 10px!important;
    margin-bottom: 5px!important;
    color:#000;
}

.selected-options div {
    padding-left: 0px;
}

.selected-options {
    padding-top:10px!important;
}

#minus {
    border-top-left-radius: 25px!important;
    border-bottom-left-radius: 25px!important;
    border-right:none!important;
    font-size: 15px;
}

#plus {
    border-top-right-radius: 25px!important;
    border-bottom-right-radius: 25px!important;
    border-left:none!important;
    font-size: 15px;
}

#input-quantity {
    padding-left: 5px!important;
    padding-right: 5px!important;
    border-left:none!important;
    border-right:none!important;
    width: 90px!important;
}

#minus:hover, #plus:hover {
    color: #f49a25!important;
    background: #fff!important;
}

#minus,#plus,#input-quantity {
    height:50px!important;
}

.qty-product label {
    margin-top: 15px!important;
}

.qty-product-content {
    position: relative;
    padding-top: 15px;
    width:80%;
    margin:auto;
    float:right;
}

button[disabled] {
    cursor: not-allowed!important;
    background: #000;
}

.btn-block {
    border-radius: 25px;
    background: #f49a25;
    width: 100%;
}

.btn-oos {
    border-radius: 25px;
    background: #777!important;
    width: 100%;
    margin-bottom: 7px;
}

.text-total {
    font-size: 18px!important;
    color: #f49a25 !important;
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    font-weight: 800;
    display: table-cell;
}

.text-remaining{
    font-size: 18px !important;
    color: #f49a25 !important;
    font-weight: 800;
    display: table-cell;
}

.main-price {
    font-size: 40px!important;
    color: #f49a25 !important;
    display: table-cell;
}

.remaining-price {
    font-size: 24px !important;
    color: #f49a25 !important;
    display: table-cell;
    padding-left: 18px;
}

.remaining-section {
    min-height: 60px;
    line-height: 0px!important;
}

.price-section {
    min-height: 60px;
    line-height: 0px!important;
}

.button:hover, .button:active, .button:focus {
    background: #000;
}
.button {
    font-size: 16px!important;
    padding: 10px 16px!important;
    font-weight: 600!important;
}
.disabled {
    background-image : linear-gradient(to bottom, rgb(241,241,241), transparent);
    cursor : no-drop;
}

p.tip {
    text-decoration: none;
    display:none;
    padding: 1px;
}

.checkbox:hover p.tip {
    display: block;
    border-radius: 6px;
    background-color: gold;
    position: absolute;
    top: 0px;
    color: #000;
    text-decoration: none;
    z-index:2;
    left:0;
}

/*:checked + label:hover p.tip, .disabled:hover p.tip {
    display: none;
}*/

.popuptext {
    position: relative;
    display: block;
    text-align: center;
    visibility: hidden;
    background: #f44336;
    color: #fff;
    padding: 5px 15px;
    width: 250px;
    left: calc( 50% - 125px);
    border-radius: 5px;
    cursor: pointer;
    -webkit-animation: updown 1.4s ease-in-out infinite;
    -moz-animation: updown 1.4s ease-in-out infinite;
    -o-animation: updown 1.4s ease-in-out infinite;
    animation: updown 1.4s ease-in-out infinite;
    top: 10px;
}

/* Popup arrow */
.popuptext::after {
    content: '';
    display: block;
    position: absolute;
    left: calc( 50% - 7px );
    width: 0px;
    text-align: center;
    height: 0;
    border-top: 14px solid #F44336;
    border-right: 14px solid transparent;
    border-bottom: 0 solid transparent;
    border-left: 14px solid transparent;
}

/* Toggle this class - hide and show the popup */
.show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
    display: inline-block!important;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}
#selectedSeedsContainer, #selectedNutsContainer, #selectedBerriesContainer, #selectedFruitsContainer, #selectedPuffedGrainsContainer {
    cursor: pointer;
}
#selectedSeedsContainer h3, #selectedNutsContainer h3, #selectedBerriesContainer h3, #selectedFruitsContainer h3, #selectedPuffedGrainsContainer h3{
    cursor: pointer;
    font-size: 17px!important;
    font-weight: 600!important;
    color: #000!important;
}

.priceOnMobile {
    display: none;
    padding: 10px;
}

.main-price-mobile {
    font-size: 15px!important;
    color: #f49a25 !important;
}

.banner-container {
    margin: 0!important;
}
#top_banner img {
    width: 100vw!important;
    cursor: pointer;
}
.snack-types {
    z-index: 10;
    margin-bottom: 10px;
    margin-top: 50px;
}
#selectedOptions p {
    line-height: 1.2;   
}
.nav-pills li {
    text-align: center;
    margin-top: 10px;
}
.nav-pills > li + li {
    margin-left: 0!important;
}
.nav a {
    font-weight: 600;
}
.nav a span {
    vertical-align: middle;
}
.nav li a {
    padding: 15px 10px;
    font-size: 18px;
}
.option-banner {
    margin-top: 15px;
}
.nav-pills li.active a {
    color: #fff!important;
    background-color: #f49a25!important;
}
.nav-pills li a {
    color: #000;
    border: 1px solid #f49a25;
    background: #fff;
}
.modal-body h4, .modal-title {
    font-weight: 600;
}
.modal-dialog {
    top: 15%;
    width: 300px!important;
    margin: auto;
}
.modal-body {
    max-height: calc(100vh - 200px);
    overflow-y: auto;
}
#button-cart::before {
    font-family: 'Material-Design-Iconic-Font';
    display: inline-block;
    font-size: 16px;
    content: "\f1cb";
    margin-right: 10px;
    font-weight: 400;
}
.seasoned-variety {
    margin: 0 0 10px 0 !important;
}
.option-text {
    width: 94%;
    text-align: justify;
    background: #fcfcfc;
    padding: 15px;
    margin-left: 15px;
}
#product .radio { 
	/*display: inline-block;*/
	display: none;
	margin: 0px 2px 5px 0px;
}
#product .radio input, #product .radio label > div { 
	display: none !important;
}
#product .radio label span {
	display: block;
	padding: 6px 12px;
}
#product .radio label {
	display: block;
	background-color: #FFFFFF;
	border: 1px solid #D4D4D4;
	border-radius: 3px;
	color: #222222;
	padding: 0;
	text-align: center;
}
.checkbox label, .radio label {
    padding-left: 0!important;
}
#product .radio label img {
	border: 2px solid #FFFFFF;
	margin: 2px;
}
.radio .checked-option {
	background-color: #000000 !important;
	color: #FFFFFF !important;
	border: 1px solid #000000 !important;
}
.selection {
    font-weight: bold;
    color: #f49a25;
}
.packaging-checkbox {
    font-size: 17px !important;
    font-weight: 600 !important;
    text-align: left !important;
}
.packaging {
    margin-right: 10px!important;
    margin-left: -10px!important;
}
.product-name-section {
    min-height: 40px;
}
.options {
    min-height: 40px;
    margin-top: 10px;
}
#selectedProducts p span {
    padding-left: 15px;
    width: 100%;
    display: block;
    line-height: 20px;
}

/*SOF Gift Packs*/
.options .radio {
    display: inline-block;
    margin: 0px 5px 5px 0px !important;
}
.options .checked-option {
    background-color: #000 !important;
    color: #fff !important;
    border: 1px solid #000 !important;
}
.options label {
    display: block;
    background-color: #fff;
    border: 0.5px solid #000;
    border-radius: 3px;
    padding: 0 !important;
    text-align: center;
    min-width: 52px;
}
.options .radio input {
    display: none !important;
}
.minus-button:hover, .plus-button:hover {
    background: #f49a25 !important;
    color: #fff !important;
    border: 1px solid #f2f2f2 !important;
}
.minus-button {
    border-top-left-radius: 4px !important;
    border-bottom-left-radius: 4px !important;
}
.minus-button, .plus-button {
    font-weight: 700;
    text-align: center;
    height: 35px !important;
    width: 20% !important;
    font-size: 14px !important;
    padding: 0px !important;
    background: #fff !important;
    border: 1px solid #f2f2f2 !important;
    float: left;
}
.quantity {
    height: 35px !important;
    width: 45% !important;
    text-align: center;
    background: #fff !important;
    border: 1px solid #f2f2f2 !important;
    float: left;
}
.qty-container {
    width: 50%;
}
.options label span {
    outline: none!important;
}
.button-product-cart {
    padding: 7px 16px !important;
    float: right;
    color: #fff!important;
}
.giftpacks-row {
    margin-bottom: 15px;
}
.p0 {
    padding: 0;
}
.product-image {
    width: 100%;
    padding: 5%;
}
.button-cart-mobile, .btn-oos-mobile {
    font-size: 18px !important;
    padding: 4px 10px !important;
    margin-top: 0;
}
.product-name-text {
    min-height: 40px;
}
.giftpacks-row .price-label {
    width: 100%!important;
}
/*EOF Gift Packs*/
</style>
<style>
    /* HIDE RADIO */
[type=radio] { 
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
  display: none;
}

/* IMAGE STYLES */
[type=radio] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
[type=radio]:checked + img {
  outline: 2px solid #fb9935;
  height: 90px;
}

[type=radio]:checked + span{
  outline: 2px solid #fb9935;
}

.voucher-image {
    cursor: pointer;
    width: 100%;
    height: 80px;
    width: 80px;
} 
  
.ui-button-text{
    background-color: #fff;
    color: #454545;
    border-color: transparent;
    height: 36px;
    width: 95px;
    display: inline-block;
    padding: 5px;
    border: 2px solid #6d6d6d;
    border-radius: 10px;
}

.form-horizontal{
    /*background: #f5f5f5;*/
    padding: 3%;
}
 
input[type="text"].form-control, input[type="number"].form-control, input[type="email"].form-control, input[type="tel"].form-control, input[type="color"].form-control,textarea.form-control {
        font-size: 16px;
        background: #fff;
        border-radius: 8px !important;
}
 #btn1{
     cursor: pointer !important;
 }
 
 
 .voucher_custom_name_6 {
    top: 27%;
    text-align: center;
    position: absolute;
    width: 100%;
    /*text-shadow: -1px -1px 1px #fff, 1px 1px 1px #000;*/
    letter-spacing: 1px;
}
.voucher_custom_name_7 {
    bottom: 3%;
    position: absolute;
    width: 100%;
    /*text-shadow: -1px -1px 1px #fff, 1px 1px 1px #000;*/
    letter-spacing: 1px;
    text-align: center;
}
.voucher_custom_message_6 {
    bottom: -2%;
    position: absolute;
    text-align: center!important;
    width: 100%;
    /*text-shadow: -1px -1px 1px #fff, 1px 1px 1px #000;*/
}
.voucher_custom_message_7 {
    top: 34%;
    position: absolute;
    text-align: center!important;
    width: 84%;
    margin-left: 8%;
    margin-right: 8%;
    /*text-shadow: -1px -1px 1px #fff, 1px 1px 1px #000;*/
}
.voucher_custom_amount_6 {
    bottom: 20%;
    left: 15%;
    /*text-shadow: -1px -1px 1px #fff, 1px 1px 1px #000;*/
    position: absolute;
}
.voucher_custom_amount_7 {
    bottom: 18%;
    left: 15%;
    /*text-shadow: -1px -1px 1px #fff, 1px 1px 1px #000;*/
    position: absolute;
}
[type=radio]:checked + .theme_color {
    /*outline: 2px solid #fb9935!important;*/
    box-shadow: -0.5px -0.5px 0px 2px #fb9935;
}
.theme_color {
    border-radius: 10px;
     height: 50px;
     width: 80px;
}
@media(max-width: 768px) {
    .mobile {
        margin-left: 0px;
        margin-bottom: 10px;
        min-height: 400px!important;
        padding: 7px!important;
    }
    .qty-container {
        width: 70% !important;
    }
    .voucher_custom_name_6 {
        top: 29%;
    }
    .voucher_custom_name_7 {
        font-size: 20px;
        bottom: 2%;
    }
    .voucher_custom_message_7 {
        top: 34%;
        width: 88%;
        margin-left: 6%;
        margin-right: 6%;
    }
    .voucher_custom_amount_6 {
        left: 13%;
        bottom: 19%;
    }
    .voucher_custom_amount_7 {
        bottom: 16%;
    }
}
@media(min-width:768px) {
    .form-horizontal #content label {
        padding-left: 14%;
        text-align: left;
    }
}
#btn1, #btn2 {
    padding: 2%;
}
.amount [type=radio]:checked + span {
    border: 2px solid #fb9935;
    outline: none!important;
}
.sp {
    display: inline!important;
}
.mrp {
    font-size: 12px;
}
    .deals-discount {
        border-radius: 50%;
        right: 0px;
        top: 5%;
        position: absolute;
        background: #f49a25;
        text-align: center;
        z-index: 5;
        width: 50px;
    }
    .deals-discount h5 {
        color: #fff;
        font-weight: 600;
        padding: 1px;
    }
    .price-coupon {
        font-weight: bold;
        padding-right: 5px;
    }
</style>
<script type="text/javascript">
$(document).ready(function(){
  $('#btn1').on("click",function(e){
   $('#giftcard').show();
   $('#gifting').hide();
  });
  $('#btn2').on("click",function(e){
   $('#gifting').show();
   $('#giftcard').hide();
  });
});
</script>

<div class="container">
    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 2%;padding-top: 2%;">
        <img id="btn1" class="col-md-6 col-xs-12 col-sm-12 p0 img-responsive" src="https://www.trueelements.co.in/demo/image/gifts-1.jpg" style="cursor: pointer; margin-top: 2%;"> 
        <img id="btn2" class="col-md-6 col-xs-12 col-sm-12 p0 img-responsive" src="https://www.trueelements.co.in/demo/image/gifts-2.jpg" style="cursor: pointer; margin-top: 2%;"> 
</div></div></div>

<div id="giftcard" style="display:none">
    <div class="container">
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="hidden-xs hidden-sm"></div>
            <form action="<?php echo $giftvoucher; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div id="content" class="col-md-8 col-sm-12 col-xs-12" style="background: #f5f5f5; padding: 3%;">
                    <?php echo $content_top; ?>
                    <h3 class="text-center">GIFT A True Voucher</h3>
                    <div class="form-group required">
                        <label class="col-md-12 col-xs-12 col-sm-12 control-label design-label" style="text-align: left;">Choose Your Design</label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6 theme">
                            <?php foreach ($voucher_themes as $voucher_theme) { ?>
                                <?php if($voucher_theme['voucher_theme_id'] != 8) { ?><!--Temp Adjustment -->
                                <?php if ($voucher_theme['voucher_theme_id'] == $voucher_theme_id) { ?>
                                    <div class="radio col-sm-3 col-xs-4 col-md-4 p0"> 
                                        <label>
                                            <input type="radio" name="voucher_theme_id"  value="<?php echo $voucher_theme['voucher_theme_id']; ?>" onchange="preview('image/<?php echo $voucher_theme['image']; ?>');" checked="checked" />
                                            <?php if($voucher_theme['voucher_theme_id'] == 6) { ?>
                                                <div data-src="image/<?php echo $voucher_theme['image']; ?>" class="theme_color" style="background: #efc65d; border: 2px solid #000;"></div>
                                            <?php } else { ?>
                                                <div data-src="image/<?php echo $voucher_theme['image']; ?>" class="theme_color" style="background: #000; border: 2px solid #efc65d;"></div>
                                            <?php } ?>
                                            <p class="img_src text-center"><?php echo $voucher_theme['name']; ?></p>
                                        </label>
                                    </div>
                                <?php } else { ?>
                                    <div class="radio col-sm-3 col-xs-4 col-md-4 p0"> 
                                        <label>
                                            <input type="radio" name="voucher_theme_id"  value="<?php echo $voucher_theme['voucher_theme_id']; ?>" onchange="preview('image/<?php echo $voucher_theme['image']; ?>');" />
                                            <?php if($voucher_theme['voucher_theme_id'] == 6) { ?>
                                                <div data-src="image/<?php echo $voucher_theme['image']; ?>" class="theme_color" style="background: #efc65d; border: 2px solid #000;"></div>
                                            <?php } else { ?>
                                                <div data-src="image/<?php echo $voucher_theme['image']; ?>" class="theme_color" style="background: #000; border: 2px solid #efc65d;"></div>
                                            <?php } ?>
                                                <p class="img_src text-center"><?php echo $voucher_theme['name']; ?></p>
                                        </label>
                                    </div>
                                <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <div class="small text-danger error-theme col-md-12 col-xs-12 col-sm-12 p0" style="margin-top: 10px;"></div>
                            <?php if ($error_theme) { ?>
                                <div class="text-danger"><?php echo $error_theme; ?></div>
                            <?php } ?>
                        </div>
                    </div>
        
                    <div class="form-group required">
                        <label class="col-sm-12 col-md-12 col-xs-12 control-label amount-label" style="text-align: left;">Select or Enter Amount (Minimum Rs.250)</label>
                    </div>
                    <div class="form-group required">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9 amount">
                            <div class="radio col-sm-3 col-xs-4 col-md-3 p0"> 
                                <label>
                                    <input type="radio" name="amountgroup" class="amountgroup" value="500" />
                                    <span class="ui-button-text ui-clickable text-center">Rs.500</span> 
                                </label>
                            </div>
                            <div class="radio col-sm-3 col-xs-4 col-md-3 p0"> 
                                <label>
                                      <input type="radio" name="amountgroup" class="amountgroup" value="1000" />
                                          <span class="ui-button-text ui-clickable text-center">Rs.1000</span> 
                                </label>
                            </div>
                            <div class="radio col-sm-3 col-xs-4 col-md-3 p0"> 
                                <label>
                                      <input type="radio" name="amountgroup" class="amountgroup custom_amount_radio" value="0" />
                                        <span class="ui-button-text ui-clickable text-center"><span>Rs.</span><input type="text" id="custom_amount" onkeypress="return event.keyCode!=13" style="border: none!important; width: 55px;" placeholder="Custom"/></span>
                                </label>
                            </div>
                            <div class="small text-danger error-custom col-md-12 col-xs-12 col-sm-12 p0" style="margin-top: 10px;"></div>
                        </div>
                        <div class="col-sm-2"> </div>
                        <div style="padding-top: 2%;padding-left: 7%;" class="col-sm-4"><input type="hidden" name="amount" value="<?php echo $amount; ?>" id="input-amount" class="form-control" size="5" /> </div>
                        <?php if ($error_amount) { ?>
                            <div class="text-danger"><?php echo $error_amount; ?></div>
                        <?php } ?>
                    </div>
        
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-to-name">Name</label>
                        <div class="col-sm-6">
                            <input type="text" name="to_name" value="<?php echo $to_name; ?>" id="input-to-name" class="form-control" maxlength="20" placeholder="Character Limit is 20!" />
                            <?php if ($error_to_name) { ?>
                                <div class="text-danger"><?php echo $error_to_name; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-to-email">Email</label>
                        <div class="col-sm-6">
                            <input type="text" name="to_email" value="<?php echo $to_email; ?>" id="input-to-email" class="form-control" />
                            <?php if ($error_to_email) { ?>
                                <div class="text-danger"><?php echo $error_to_email; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="input-from-name">From</label>
                        <div class="col-sm-6">
                            <input type="text" name="from_name" value="<?php echo $from_name; ?>" id="input-from-name" class="form-control" />
                            <?php if ($error_from_name) { ?>
                                <div class="text-danger"><?php echo $error_from_name; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <!--div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-from-email"><?php echo $entry_from_email; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="from_email" value="<?php echo $from_email; ?>" id="input-from-email" class="form-control" />
                            <?php if ($error_from_email) { ?>
                                <div class="text-danger"><?php echo $error_from_email; ?></div>
                            <?php } ?>
                        </div>
                    </div-->
        
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input-message"><span data-toggle="tooltip" title="<?php echo $help_message; ?>">Message</span></label>
                        <div class="col-sm-6">
                            <textarea name="message" cols="40" rows="2" id="input-message" class="form-control" maxlength="30" placeholder="Character Limit is 30!"><?php echo $message; ?></textarea>
                        </div>
                    </div>
                    <!--div class="form-group">
                        <label class="col-sm-3 control-label" for="input-amount"><span data-toggle="tooltip" title="<?php echo $help_amount; ?>"><?php echo $entry_amount; ?></span></label>
                        <div class="col-sm-6">
                            <input type="text" name="amount" value="<?php echo $amount; ?>" id="input-amount" class="form-control" size="5" />
                            <?php if ($error_amount) { ?>
                                <div class="text-danger"><?php echo $error_amount; ?></div>
                            <?php } ?>
                        </div>
                    </div-->
                    <div class="buttons clearfix">
                        <div class="pull-right">&nbsp;</div>
                    </div>
    
                    <div class="form-group required">
                        <?php echo $content_bottom; ?>
                    </div>
                </div>  
      
      <div class="col-md-4 col-sm-12 col-xs-12" id="sticky" style="margin-top: 15px;">
			  <div class="selected-theme-options">
			        <div class="col-md-12 p0">
                        <p><?php echo $text_description; ?></p>
                        <h3 class="col-md-12 p0" id="selectedThemesContainer">My Gift Card</h3>
                        <div id="selectedtheme">
                            <h5>Your gift card preview will be shown here!</h5>
                        </div>
                    </div>
			    </div>
			  <div class="form-group">
                <label class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 15px;">
                    <?php if ($agree) { ?>
                        <input type="checkbox" class="agree-gift-voucher" name="agree" value="1" checked="checked" />
                    <?php } else { ?>
                        <input type="checkbox" class="agree-gift-voucher" name="agree" value="1" />
                    <?php } ?>
                    <?php echo $text_agree; ?>
                </label>
                <div class="error-agree small text-danger col-md-12 col-xs-12 col-sm-12"></div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 text-center"><input type="button" value="Buy Now" id="buy-gift" class="btn btn-primary" /></div>
		</div>
		    
    <?php echo $column_right; ?></div>
    
      </form>
</div>
</div>
<script>
    $('#buy-gift').click(function() {
        var error = 0;
        $('.form-horizontal .text-danger').text('');
        
        var agree = $('.agree-gift-voucher:checked').length;
        if(agree == 0) {
            $('.error-agree').text("Please agree to the term!");
            error = 1;
        }
        
        var theme = $('input[name=voucher_theme_id]:checked').length;
        if(theme == 0) {
            $('.error-theme').text("Please Select Design");
            error = 1;
        }
            
        if($('.custom_amount_radio').is(':checked')) {
            if($('.custom_amount_radio').val() < 250) {
                $('.error-custom').text("Voucher Amount should be minimum Rs.250!");
                error = 1;
            }
        } else {
            var amount = $('.amountgroup:checked').length;
            if(amount == 0) {
                $('.error-custom').text("Voucher Amount should be minimum Rs.250!");
                error = 1;
            }
        }
        
        var name = $('#input-to-name').val();
        if(name.length <= 3) {
            $('#input-to-name').next('.text-danger').remove();
            $('#input-to-name').after('<div class="small text-danger">Please Enter Name!</div>');
            error = 1;
        }
        
        var email = $('#input-to-email').val();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(regex.test(email) == false) {
            $('#input-to-email').next('.text-danger').remove();
            $('#input-to-email').after('<div class="small text-danger">Invalid Email Id!</div>');
            error = 1;
        }
        
        var from = $('#input-from-name').val();
        if(from.length <= 3) {
            $('#input-from-name').next('.text-danger').remove();
            $('#input-from-name').after('<div class="small text-danger">Please Enter Sender Name!</div>');
            error = 1;
        }
        
        if(error == 1) {
            $("html, body").animate({
                scrollTop: $(".design-label").offset().top - 200
            }, 1000);
        }
        
        if(error == 0) {
            $('.form-horizontal').submit();
        }
    });
    $('#input-to-name, #input-message').on('input', function() {
        var src = $(".theme input[type=radio]:checked").next('div').data('src');
        if(src != '' && src != undefined) {
            preview(src);
        }
    });
    $('#custom_amount').bind('input click', function() {
        if(parseInt($(this).val()) >= 250) {
            $('.custom_amount_radio').val(parseInt($(this).val()));
            $('.custom_amount_radio').trigger('click');
            var src = $(".theme input[type=radio]:checked").next('div').data('src');
            if(src != '' && src != undefined) {
                preview(src);
            }
            $('#input-amount').val(parseInt($(this).val()));
        } else {
            $('.custom_amount_radio').val(0);
            $('.amount input[type=radio]:first-child').trigger('click');
            var src = $(".theme input[type=radio]:checked").next('div').data('src');
            if(src != '' && src != undefined) {
                preview(src);
            }
        }
    });
    
    $('.amount input[type=radio]').on('change', function() {
        var src = $(".theme input[type=radio]:checked").next('div').data('src');
        if(src != '' && src != undefined) {
            preview(src);
        }
    });
    function preview(src) {
        if(src != '' && src != undefined) {
            var name = $("#input-to-name").val();
            if(name == '') {
                name = 'Voucher Receivers Name';
            }
        
            var message = $("#input-message").val();
            if(message == '') {
                message = 'Custom Message';
            }
            
            var amount = $(".amount input[type=radio]:checked").val();
            if(amount == undefined) {
                amount = "__/-";
            }
            
            var voucher_theme_id = $(".theme input[type=radio]:checked").val();
            if(voucher_theme_id == 6) {
                var text_color_css = ' color: #000!important;';
            } else if(voucher_theme_id == 7) {
                var text_color_css = ' color: #efc65d!important;';
            } else {
                var text_color_css = ' color: #000!important;';
            }
            var html = '<img src="' + src + '" alt="True Elements" class="img-responsive" style="width: 100%;" /><h3 class="voucher_custom_name voucher_custom_name_' + voucher_theme_id + '" style="' + text_color_css + '">' + name + '</h3><h4 class="voucher_custom_message voucher_custom_message_' + voucher_theme_id + '" style="' + text_color_css + '">' + message + '</h4><h3 class="voucher_custom_amount voucher_custom_amount_' + voucher_theme_id + '" style="' + text_color_css + '">Rs. ' + amount + '</h3>';
            $("#selectedtheme").html(html);
        }
    }
</script>
<script>
    $('.amountgroup').change(function(e){
    var selectedValue = $(this).val();
    $('#input-amount').val(selectedValue)
});
</script> 
</div>


<div id="gifting" style="display:none">
<div class="container">
    <div class="row">
        <div id="content" class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
		        <div id="product" class="col-md-8 col-sm-12 col-xs-12">
		            <div class="priceOnMobile">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <p class="main-price-mobile">Cart Value: Rs.0</p> 
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 snack-types" role="tabpanel">
                        <ul class="nav nav-pills nav-fill" role="tablist">
                            <li role="presentation" class="nav active col-md-3 col-sm-3 col-xs-3 upto-500-tab" data-limit="500">
                                <a href="#upto-500-content" aria-controls="upto-500-content" role="tab" data-toggle="tab"><span>Upto 500</span></a>
                            </li>
                            <li role="presentation" class="nav col-md-3 col-sm-3 col-xs-3 upto-1000-tab" data-limit="1000">
                                <a href="#upto-1000-content" aria-controls="upto-1000-content" role="tab" data-toggle="tab"><span>Upto 1000</span></a>
                            </li>
                            <li role="presentation" class="nav col-md-3 col-sm-3 col-xs-3 upto-1500-tab" data-limit="1500">
                                <a href="#upto-1500-content" aria-controls="upto-1500-content" role="tab" data-toggle="tab"><span>Upto 1500</span></a>
                            </li>
                            <li role="presentation" class="nav col-md-3 col-sm-3 col-xs-3 upto-custom-tab" data-limit="20">
                                <a href="#upto-1500-content"><input type="text" id="upto_custom" style="width: 100%;height: 51px;" value="0"></a>
                            </li>
                        </ul>
                    </div>
            
                    <?php if ($products) { ?>
                        <div class="tab-content">
                            <!-- Upto 500 -->
                            <div class="form-group tab-pane active" id="upto-500-content" role="tabpanel">
                                <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Products</h3>
                                <?php foreach ($products as $product) { ?>
                                    <?php if($product['options']) { ?>
                                        <?php foreach($product['options'] AS $option) { ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <?php if(($option_value['quantity'] > 0 || $option_value['quantity'] <= 0) && $option_value['option_price_int'] <= 500 && strpos($option_value['name'],'*') == false) { ?>
                                                    <div class="checkbox col-md-3 col-sm-5 col-xs-5 products">
                                                        <input type="text" name="product_id" value="<?php echo $product['product_id']; ?>">
                                                        <input type="checkbox" name="product[<?php echo $product['product_id']; ?>][<?php echo $option_value['option_value_id']; ?>][]" value="<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>" id="inp-500<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>" disabled/>
                                                        <label for="inp-500<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>">
                                                            <p class="tip"></p>
                                                            <img src="<?php echo $product['thumb']; ?>" alt="True Elements" class="img-responsive"/><br/>
                                                            <div class="product-name-section">
                                                                <p class="product_name"><?php echo $product['name']," ", $option_value['name']; ?></p>
                                                                <p class="price"><span class="sp"><?php echo $option_value['option_price']; ?></span> <strike class="mrp price-old"><?php echo $option_value['option_mrp']; ?></strike></p>
                                                            </div>
                                                            <div class="radio">
                                                                <label class="checked-option">
                                                                    <input type="radio" data-product-option-id="<?php echo $option['product_option_id']; ?>" class="option-<?php echo $option['product_option_id']; ?>"  name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" checked/>
                                                                        <span><?php echo $option_value['name']; ?></span>
                                                                </label>
                                                            </div>
                                                        </label>
                                                    </div>
                                                <?php } ?>
						                    <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            
                            <!-- Upto 1000 -->
                            <div class="form-group tab-pane" id="upto-1000-content" role="tabpanel">
                                <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Products</h3>
                                <?php foreach ($products as $product) { ?>
                                    <?php if($product['options']) { ?>
                                        <?php foreach($product['options'] AS $option) { ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <?php if(($option_value['quantity'] > 0 || $option_value['quantity'] <= 0) && $option_value['option_price_int'] <= 1000 && strpos($option_value['name'],'*') == false) { ?>
                                                    <div class="checkbox col-md-3 col-sm-5 col-xs-5 products">
                                                        <input type="text" name="product_id" value="<?php echo $product['product_id']; ?>">
                                                        <input type="checkbox" name="product[<?php echo $product['product_id']; ?>][<?php echo $option_value['option_value_id']; ?>][]" value="<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>" id="inp-1000<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>" disabled/>
                                                        <label for="inp-1000<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>">
                                                            <p class="tip"></p>
                                                            <img src="<?php echo $product['thumb']; ?>" alt="True Elements" class="img-responsive"/><br/>
                                                            <div class="product-name-section">
                                                                <p class="product_name"><?php echo $product['name']," ", $option_value['name']; ?></p>
                                                                <p class="price"><span class="sp"><?php echo $option_value['option_price']; ?></span> <strike class="mrp price-old"><?php echo $option_value['option_mrp']; ?></strike></p>
                                                            </div>
                                                            <div class="radio" class="checked-option">
                                                                <label>
                                                                    <input type="radio" data-product-option-id="<?php echo $option['product_option_id']; ?>" class="option-<?php echo $option['product_option_id']; ?>"  name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" checked/>
                                                                        <span><?php echo $option_value['name']; ?></span>
                                                                </label>
                                                            </div>
                                                        </label>
                                                    </div>
                                                <?php } ?>
						                    <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            
                            <!-- Upto 1500 -->
                            <div class="form-group tab-pane" id="upto-1500-content" role="tabpanel">
                                <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Products</h3>
                                <?php foreach ($products as $product) { ?>
                                    <?php if($product['options']) { ?>
                                        <?php foreach($product['options'] AS $option) { ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <?php if(($option_value['quantity'] > 0 || $option_value['quantity'] <= 0) && $option_value['option_price_int'] <= 1500 && strpos($option_value['name'],'*') == false) { ?>
                                                    <div class="checkbox col-md-3 col-sm-5 col-xs-5 products">
                                                        <input type="text" name="product_id" value="<?php echo $product['product_id']; ?>">
                                                        <input type="checkbox" name="product[<?php echo $product['product_id']; ?>][<?php echo $option_value['option_value_id']; ?>][]" value="<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>" id="inp-1500<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>" disabled/>
                                                        <label for="inp-1500<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>">
                                                            <p class="tip"></p>
                                                            <img src="<?php echo $product['thumb']; ?>" alt="True Elements" class="img-responsive"/><br/>
                                                            <div class="product-name-section">
                                                                <p class="product_name"><?php echo $product['name']," ", $option_value['name']; ?></p>
                                                                <p class="price"><span class="sp"><?php echo $option_value['option_price']; ?></span> <strike class="mrp price-old"><?php echo $option_value['option_mrp']; ?></strike></p>
                                                            </div>
                                                            <div class="radio" class="checked-option">
                                                                <label>
                                                                    <input type="radio" data-product-option-id="<?php echo $option['product_option_id']; ?>" class="option-<?php echo $option['product_option_id']; ?>"  name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" checked/>
                                                                        <span><?php echo $option_value['name']; ?></span>
                                                                </label>
                                                            </div>
                                                        </label>
                                                    </div>
                                                <?php } ?>
						                    <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            
                            <!-- Upto Custom -->
                            <div class="form-group tab-pane" id="upto-custom-content" role="tabpanel">
                                <h3 class="control-label col-md-12 col-xs-12 col-sm-12 center">Products</h3>
                                <?php foreach ($products as $product) { ?>
                                    <?php if($product['options']) { ?>
                                        <?php foreach($product['options'] AS $option) { ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <?php if(($option_value['quantity'] > 0 || $option_value['quantity'] <= 0) && $option_value['option_price_int'] <= 1500 && strpos($option_value['name'],'*') == false) { ?>
                                                    <div class="checkbox col-md-3 col-sm-5 col-xs-5 products">
                                                        <input type="text" name="product_id" value="<?php echo $product['product_id']; ?>">
                                                        <input type="checkbox" name="product[<?php echo $product['product_id']; ?>][<?php echo $option_value['option_value_id']; ?>][]" value="<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>" id="inp-custom<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>" disabled/>
                                                        <label for="inp-custom<?php echo $product['product_id'],".",$option_value['option_value_id']; ?>">
                                                            <p class="tip"></p>
                                                            <img src="<?php echo $product['thumb']; ?>" alt="True Elements" class="img-responsive"/><br/>
                                                            <div class="product-name-section">
                                                                <p class="product_name"><?php echo $product['name']," ", $option_value['name']; ?></p>
                                                                <p class="price"><span class="sp"><?php echo $option_value['option_price']; ?></span> <strike class="mrp price-old"><?php echo $option_value['option_mrp']; ?></strike></p>
                                                            </div>
                                                            <div class="radio" class="checked-option">
                                                                <label>
                                                                    <input type="radio" data-product-option-id="<?php echo $option['product_option_id']; ?>" class="option-<?php echo $option['product_option_id']; ?>"  name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" checked/>
                                                                        <span><?php echo $option_value['name']; ?></span>
                                                                </label>
                                                            </div>
                                                        </label>
                                                    </div>
                                                <?php } ?>
						                    <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
          <!--Main Section-->
          <div class="col-md-4 col-sm-12 col-xs-12" id="sticky">
              <div class="product-title-section">
                <div class="product-title-subsection">
			        <h2 class="product-title"><?php echo $heading_title; ?></h2>
			    </div>
			  </div>
			  <div class="selected-options">
			      <div class="col-md-12">
                    <h3 class="col-md-12" id="selectedProductsContainer">Products in My Basket</h3>
                    <h4 id="selectedProducts"></h4>
                  </div>
			  </div>
			<div class="form-group">
				<div class="qty-product">
				    <div class="qty-product-content">
				        <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
					    <input type="button" id="minus" value="-" class="form-control" />
					    <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" />
					    <input type="button" id="plus" value="&#43;" class="form-control"/>
				    </div>
					
				</div>
				<div class="remaining-section">
			        <p class="text-remaining">Remaining</p>
			        <p class="remaining-price" id="remaining-price">Rs.500</p>
			    </div>
			    
				<div class="price-section">
			        <p class="text-total">TOTAL</p>
			        <p class="main-price" id="main-price">Rs.0</p>
			    </div>
			    
			    <div>
			        <textarea placeholder="Personal Note!" cols="30" rows="5" name="note" id="note"></textarea>
			    </div>
			    
			    <div>
			        <label for="is_invoice"  style="word-wrap:break-word">Don't send Invoice 
                        <input id="is_invoice"  type="checkbox" value="1" />
                    </label>
			    </div>
			    
			    <?php if ($quantity == 0.00) { ?>
			        <button class="btn-oos button" type="button" disabled>Stock Over!</button>
			        <br/>
			    <?php } else { ?>
				    <button class="button btn-block" type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" disabled><?php echo $button_cart; ?></button>
				<?php } ?>
				<center style="margin-top:8px;"><a href="mailto:marketing@true-elements.com?Subject=Bulk%20discount" target="_top" style="color:#FDAD00;">Contact us</a> for bulk discount.</center>
			</div>
			</div>
      </div>
	  <?php echo $content_bottom; ?>
	  </div>
	</div>
</div>
</div>

<div class="main-row giftpacks-row">
    <div class="container" style="margin-top: 20px; margin-bottom: 20px;">
        <?php if ($gift_products) { ?>
                <div class="col-md-12 col-xs-12 col-sm-12 product-container p0">
                <h1 class="page-heading"><span class="pink">Gift</span> Packs</h1>
                <!--h4 class="sub-category">Gift Packs</h4-->
                <hr/>
                <?php foreach ($gift_products as $giftpacksproduct) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 mobile giftpacks">
                        <div class="product-thumb item-inner">
					    <?php if($giftpacksproduct['coupon_discount']){ ?> 
					        <div class="deals-discount"><h5><?php echo round($giftpacksproduct['discount']); ?> %<br>Off</h5></div>
					    <?php } ?>
					    <div class="images-container">
                        <a href="<?php echo $giftpacksproduct['href']; ?>">
                            <div class="pro_img">
                                <?php if($giftpacksproduct['rotator_image']){ ?>
									<img class="image2 product-image" src="<?php echo $giftpacksproduct['rotator_image']; ?>" alt="<?php echo $giftpacksproduct['name']; ?>" />
								<?php } ?>
    		                    <img alt="True Elements" class="product-image" src="<?php echo $giftpacksproduct['thumb']; ?>">
    		                </div>
    	                </a>
    	                </div>
                        <p class="product-name-text"><a href="<?php echo $giftpacksproduct['href']; ?>"><?php echo $giftpacksproduct['name']; ?></a></p>
                        <?php $is_optionqty = 1; ?>
    		            <?php if($giftpacksproduct['options']) { ?>
    	                    <div class="options options-<?php echo $giftpacksproduct['product_id']; ?> col-md-12 col-xs-12 col-sm-12 p0">
    	                        <?php foreach($giftpacksproduct['options'] AS $option) { ?>
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $giftpacksproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $giftpacksproduct['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $giftpacksproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $giftpacksproduct['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
    	                        <?php } ?>
    	                    </div>
    	                <?php } else { ?>
    	                    <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                    <?php $is_optionqty = 0; ?>
    	                <?php } ?>
    	                <?php if($giftpacksproduct['personalized'] == 0) { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12 p0">
    	    	                <div class="box-price">
    	   			                <?php if ($giftpacksproduct['price']) { ?>
    	        		                <p class="price">
    	    			                    <?php if (!$giftpacksproduct['special']) { ?>
                                                <span class="price-new price-new-<?php echo $giftpacksproduct['product_id']; ?>"><?php echo $giftpacksproduct['price']; ?></span>
    			    		                <?php } else { ?>
    			    	                        <span class="price-new price-new-<?php echo $giftpacksproduct['product_id']; ?>"><?php echo $giftpacksproduct['special']; ?></span>
    			    	                        <span class="price-old price-old-<?php echo $giftpacksproduct['product_id']; ?>"><?php echo $giftpacksproduct['price']; ?></span>
    			    	                    <?php } ?>
    			    	                </p>
    			    	                <?php if ($giftpacksproduct['coupon_discount']) { ?>
    			    	                    <span class="price-coupon price-coupon-<?php echo $giftpacksproduct['product_id']; ?>"><b> <?php echo $giftpacksproduct['coupon_discount']; ?> </b></span><span> <?php echo $giftpacksproduct['coupon'] ? 'Using code: <b>' . $giftpacksproduct['coupon'] . '</b>' : ''; ?></span>
    			    	                <?php } ?>
    			                    <?php } ?>
    		                    </div>
    	                    </div>
    	                    <?php if($is_optionqty == 1 && $giftpacksproduct['quantity'] > 0) { ?>
    	                        <div class="qty-container" style="margin-top: 10px;">
    	                            <input type="button" value="-" class="minus-button" onclick="minus2(<?php echo $giftpacksproduct['product_id']; ?>)" />
    	                            <input type="text" class="quantity-<?php echo $giftpacksproduct['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify2(<?php echo $giftpacksproduct['product_id']; ?>)" />
    		                        <input type="button" value="&#43;" class="plus-button" onclick="plus2(<?php echo $giftpacksproduct['product_id']; ?>)" />
    	                        </div>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $giftpacksproduct['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocart2(<?php echo $giftpacksproduct['product_id']; ?>)">Add to Cart</button>
    	                        <button type="button" class="btn button button-product-cart cart-<?php echo $giftpacksproduct['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocart2(<?php echo $giftpacksproduct['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } else { ?>
    	                        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                    <?php } ?>
    	                <?php } else { ?>
    	                    <div class="price-label col-md-12 col-xs-12 col-sm-12 p0">
    	    	                <div class="box-price">
    	                            <p class="price"></p>
                                </div>
                            </div>
                            <?php if($giftpacksproduct['quantity'] > 0) { ?>
						        <a href="<?php echo $giftpacksproduct['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
    	                <?php } ?>
    	                </div>
                    </div>
                <?php } ?>
                </div>
        <?php } ?>
    </div>
</div>

<div class="container" style="padding-top: 15px; padding-bottom: 15px;">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <h4 class="text-center hidden-sm hidden-xs">Corporate Gifts / queries? Call us at +91-8767-120-120 / Whatsapp us at +91-9321-532-959</h4>
        <h4 class="text-center hidden-md hidden-lg hidden-xl p0" style="line-height: 20px;">Corporate Gifts / queries? Call us at <a href="tel:+918767120120">+91-8767-120-120</a> / Whatsapp us at <a href="whatsapp://send?phone=91 9321532959&text=I have query regarding corporate gifts">+91-9321-532-959</a></h4>
    </div>
</div>
<script type="text/javascript">
    function addtocart2(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left" ><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center">' + json['success'] + '</div></div>';
                    
                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus2(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus2(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify2(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript"><!--
$(".nav-pills li").click(function() { 
    var limit = $(this).data('limit');
     
    
    $(".plain").trigger("click");
    $("input:checkbox").removeAttr("checked");
    $("input").prop("disabled", false);
    $("input:checkbox:not(:checked)").next().find("img").css({
        "cursor" : "pointer"
    });
    $("input:checkbox:not(:checked)").parent().removeClass("disabled");
    $("input:checkbox:not(:checked)").parent().css({"box-shadow" : "none"});
    $("#selectedProducts").html("");
    $("#main-price, .main-price-mobile").text("Rs.0"); 
    
    $("#remaining-price, .remaining-price-mobile").text("Rs." + limit);
    
});
</script>
<script type="text/javascript"><!--
  // decrease number of product
  function minus(minimum){
      var currentval = parseInt($("#input-quantity").val());
      $("#input-quantity").val(currentval-1);
      if($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum){
          $("#input-quantity").val(minimum);
     }
  };
  // increase of product
  function plus(){
      var currentval = parseInt($("#input-quantity").val());
     $("#input-quantity").val(currentval+1);
  };
  $("#minus").click(function(){
    minus(1);
  });
  $("#plus").click(function(){
    plus();
  });
</script>
<script type="text/javascript">
$("#button-cart").click(function() {
    $(".products > input:checked").each(function() {
        var product_option_id = parseInt($(this).next("label").find("input:radio").data("productOptionId"));
        var product_option_value_id = $(this).next("label").find("input:radio").val();
        var note = $("#note").val();
        var is_invoice = $('#is_invoice').val();
        var option_array = {};
        option_array[product_option_id] = product_option_value_id;
        
	    $.ajax({
		    url: "index.php?route=checkout/cart/add",
		    type: "post",
		    data: {product_id: $(this).parent().find("input[name=\'product_id\']").val(), quantity: $("#sticky input[name=\'quantity\']").val(), option: option_array, note: note, is_invoice: is_invoice},
		    dataType: "json",
		    beforeSend: function() {
		    	$("#button-cart").button("loading");
		    },
		    complete: function() {
		    	$("#button-cart").button("reset");
		    },
		    success: function(json) {
		    	$(".alert, .text-danger").remove();
		    	$(".form-group").removeClass("has-error");
			    if (json["error"]) {
			    	if (json["error"]["option"]) {
			    		for (i in json["error"]["option"]) {
			    			var element = $("#input-option" + i.replace("_", "-"));
			    			if (element.parent().hasClass("input-group")) {
			    				element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
			    			} else {
			    				element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
			    			}
			    		}
			    	}
				    if (json["error"]["recurring"]) {
				    	$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				    }
				    // Highlight any found errors
				    $(".text-danger").parent().addClass("has-error");
			    }
			    if (json["success"]) {
			    	
				    $("#cart-total").html(json["total"]);
				    
				    $("#cart > .top-cart-contain ul").load("index.php?route=common/cart/info ul li");
			    }
		    },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
	    });
    });
    var success_text = '<div style="margin: 0 0 10px 0"><div><p style="font-weight: bold; text-align: center;">Success! Products added in the cart.</p></div></div>';
    
    var np = new Noty({
        type: "success",
        layout: 'bottomCenter',
        text:  success_text,
        textAlign:"right",
        animateOpen:{"height":"toggle"},
        timeout: '3000',
        progressBar: false,
        closeWith: "button"
    }).show();
});

$(".btn-wishlist").click(function() {
    $.ajax({
    	url: 'index.php?route=account/wishlist/add',
    	type: 'post',
    	data: $('#product input[type=\'text\'], #sticky input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea, #sticky input[name=\'quantity\'], #sticky input[type=\'checkbox\']:checked, #save-creation input[type=\'text\']'),
	   	dataType: 'json',
	   	success: function(json) {
	   		$('.alert').remove();
    	    	if (json['redirect']) {
	    		location = json['redirect'];
	    	}
			if (json["success"]) {
			    var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left"><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center" >' + json['success'] + '</div></div>';

                var np = new Noty({
                    type: "success",
                    layout: json["notice_add_layout"],
                    text:  success_text,
                    textAlign:"right",
                    animateOpen:{"height":"toggle"},
                    timeout: json["notice_add_timeout"],
                    progressBar: false,
                    closeWith: "button"
                }).show();
			    $('#wishlist-total span').html(json['total']);
			    $('#wishlist-total').attr('title', json['total']);
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    	}
    });
});

</script>
<script>
$(document).ready(function() {
    //For changing URL
    /*if (window.history.replaceState) {
        window.history.replaceState("Healthy Food Gift Hampers", "Healthy Food Gift Hampers", "/gift-hampers");
    }*/
    
    //After Page Load Enable All Checkboxes
    $("input").prop("disabled", false);
    
    //Seeds
    $(".products").on("click", function() {
        var price_limit = $(".nav.active").data("limit");
        var total = 0;
        $(".products > input:checked").each(function() {
            total = total + parseInt($(this).next("label").find("p.price .sp").text().replace(/[^0-9]/g,''));
        });
        
        if(total > 0) {
            $("#button-cart").prop("disabled", false);
        } else {
            $("#button-cart").prop("disabled", true);
        }
        
        var remaining_price = price_limit - total;
        var not_checked_price = 0;
        $(".products > input:not(:checked)").each(function() {
            not_checked_price = parseInt($(this).next("label").find("p.price .sp").text().replace(/[^0-9]/g,''));
            if(remaining_price < not_checked_price) {
                $(this).parent().css({"box-shadow" : "inset 0px 0px 0px 2px rgb(28,0,85)"});
                $(this).prop("disabled", true);
                $(this).parent().addClass("disabled");
                $(this).next().find("img").css({
                    "cursor" : "no-drop",
                    "z-index" : "-1"
                });
            } else {
                $(this).prop("disabled", false);
                $(this).parent().removeClass("disabled");
                $(this).next().find("img").css({
                    "cursor" : "pointer",
                    "z-index" : "-1"
                });
            }
        });
        
        $("#remaining-price, .remaining-price-mobile").text("Rs." + remaining_price);
        $("#main-price, .main-price-mobile").text("Rs." + total);

        $(".tab-pane.active .products > input:checkbox:checked").parent().css({"box-shadow" : "inset 0px 0px 0px 2px rgb(28,0,85)"});
        $(".tab-pane.active .products > input:checkbox:not(:checked)").parent().css({"box-shadow" : "none"});
        
        var productsText= "";
        $(".products > input:checked").each(function() {
            productsText +="<span>" + $(this).next("label").find("p.product_name").text() + "</span>";
        });
        $("#selectedProducts").html('<p style="border-left: 3px solid #f49a25;">' + productsText + '</p>');
    });
});
</script>
<script>
    $("#selectedProductsContainer").click(function() {
    $("html, body").animate({
        scrollTop: $(".tab-pane.active").offset().top-200
    }, 1000);
    });
    if(performance.navigation.type == 2){
        location.reload(true);
    }
</script>
<script type="text/javascript"><!--
  $('#product input[type="radio"]').click(function() {
    $('#product input[type="radio"]').each(function() {
      $(this).parents("label").toggleClass('checked-option', this.checked);
    });
  });
</script>
<?php echo $footer; ?>