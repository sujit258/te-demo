<?php if ($total_reviews > 0) { ?>
<style>
    .overall-reviews .fa {
        color: #f49a25;
        font-size: 20px !important;
    }
    .p0 {
        padding-left: 0;
        padding-right: 0;
    }
    .overall-reviews {
        display: block;
        padding: 15px;
        margin-bottom: 15px;
        background: #fcfcfc;
    }
    .progress-bar {
        background: #f49a25;
    }
    .overall-reviews .review-ratings-box {
        text-align: center;
    }
    @media(min-width: 768px) {
    .progress {
        width: 40%;
        height: 15px;
        margin-bottom: 15px;
    }
    .review-left, .review-right {
        min-height: 158px;
    }
    .review-right {
        padding-top: 15px;
    }
    .review-ratings {
        padding-top: 50px;
    }
    .reviews-author {
        display: inline;
    }
    .individual-rating {
        margin-top: 5px;
    }
    .individual-rating .fa {
        height: 15px;
    }
    .reviews-date {
        float: right;
    }
    .reviews-text {
        margin-top: 15px;
    }
    }
    @media(max-width: 767px) {
        .review-left {
            margin-bottom: 30px;
        }
        .individual-rating {
            width: 100%;
            display: block;
        }
        #review {
            width: 100%;
        }
        .progress {
            margin-bottom: 15px;
        }
    }
</style>
<?php foreach ($reviews as $review) { ?>
<div class="reviews">
  	<div class="img-circle reviews-avatar"><i class="fa fa-user"></i></div>
  	<div class="reviews-wrap col-md-12 col-xs-12 col-sm-12 p0">
	  	<h4 class="reviews-author"><?php echo $review['author']; ?></h4>
	  	<h4 class="reviews-date hidden-xs hidden-sm"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
	  	<div class="individual-rating">
	  	    <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($review['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
            <?php } ?>
        </div>
        <h4 class="reviews-date hidden-md hidden-xl hidden-lg"><i class="fa fa-calendar"></i><?php echo $review['date_added']; ?></h4>
	    <p class="reviews-text"><?php echo $review['text']; ?></p>
    </div>
</div>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
    <p style="font-size: 15px;"><?php echo $text_no_reviews; ?></p>
<?php } ?>
<script type="text/javascript">
    $('#review .pagination a').click(function(e) {
        e.preventDefault();
        $('#review').fadeOut('slow');
        $('#review').load(this.href);
        $('#review').fadeIn('slow');
    });
    $('.review-right a').click(function(){
        $("html, body").animate({
            scrollTop: $("#review").offset().top - 250
        }, 500);
    });
</script>