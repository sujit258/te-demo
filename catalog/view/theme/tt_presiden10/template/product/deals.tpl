<style>
body {
  margin: 0px;
  padding: 0px;
  font-family: "gothamBold";
  overflow-x: hidden;
}

.qty-container {
    width: 50%;
}
.breadcrumb {
    margin-bottom: 15px!important;
}

.category-title {
    font-size: 35px;
}
.sew {
  width: 100%;
}

.banner-text-lg {
    font-size: 6em!important;
}

.banner-text-md {
    font-size: 4em!important;
}

h1 {
  font-size: 6em;
  font-weight: bold;
}

h3 {
  font-size: 4em;
}
.intro {
  position: absolute;
  text-align: center;
  top: 280px;
  left: 470px;
}

.space {
  height: 80px;
}

h2 {
  font-weight: bold;
}

.space1 {
  padding: 0;
  margin: 0;
  background-color: #f49a25;
  height: 50px;
  text-align: center;
  font-size: 2em;
}

  .product-image {
      margin: auto;
      display: table;
      padding: 14%;
  }
  h2 {
      text-align: center;
  }
  
  .product-name-text{
        font-size: 15px;
        text-align: center; 
        font-family: gilroy, 'Oswald', sans-serif;
    }
    .mobile {
        min-height: 560px!important;
        padding: 7px !important;
    }
    .product-name-text a {
        color: #000;
    }
  
  @media (max-width: 1000px){
    .qty-container {
        width: 70%!important;
    }
    .button-cart {
        margin-right: 0!important;
    }
    .intro {
        position: absolute;
        text-align: center;
        top: 15%;
        left: 22%;
    }
    .button-personalize {
        font-size: 14px!important;
        margin-top: 0px!important;
    }
    .product-name-text {
        min-height: 60px; !important;
        font-size: 14px!important;
    }
    .banner-text-lg {
        font-size: 3em!important;
    }

    .banner-text-md {
        font-size: 1.5em!important;
    }
    
    .category-title {
        margin: 40px 0 20px 0!important;
        font-size: 28px!important;
    }
    
    .mobile {
        margin-left: 0px;
        margin-bottom: 10px;
        min-height: 480px !important;
    } 
    .container {
        width: 100%!important;
    }
    
    .product-name-text .select{
        height: 90px;
        font-size: 20px;
        text-align: center; 
        font-family: gilroy, 'Oswald', sans-serif;
    }
    .readyToEatBanner, .readyToCookBanner, .seedsMixesBanner, .rawAndroastedSeedsBanner {
        margin-top: 10px;
    }
    .products-container {
        padding-left: 0!important;
        padding-right: 0!important;
    }
}
    .options {
        padding: 0px!important;
        min-height: 55px!important;
    }
    .product-name-text {
        min-height: 40px;
        text-align: left;
    }
    .product-options {
        text-align: center;
        float: left;
        background: #fff!important;
        width: 90%;
        color: #000;
    }
    .button-product-cart {
        float: right!important;
        font-size: 13px!important;
        background: #f49a25;
        color: #fff;
    }
    .button-personalize {
        font-size: 13px!important;
        float: right !important;
        background: #f49a25;
        color: #fff;
        margin-top: 0px;
    }
    .price-label {
        padding: 8px 0px !important;
        min-height: 38px!important;
        width: 100%!important;
    }
    .price {
        margin-bottom: 0px!important;
    }
    .quantity {
        height: 35px!important;
        width: 45%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }

    @media(min-width: 1000px) {
        .des-container {
            height: 200px;
        }
    }
    .oos {
        background: #777!important;
        border-radius: 4px!important;
        float: right!important;
        font-size: 13px!important;
        color: #fff;
    }
    .oos:hover {
        cursor: not-allowed!important;
        color: #fff!important;
    }
    .button-product-cart:focus, .button-personalize:focus {
        color: #fff!important;
    }
    .button-product-cart:hover, .button-personalize:hover {
        color: #fff!important;
        background: #000!important;
    }
    .center {
        text-align: center;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 23px !important;
        padding: 9px 10px !important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .category-title {
        margin: 70px 0 20px 0;
    }
    .main-row {
        margin-right: 0px!important;
        margin-left: 0px!important;
    }
    .price-deal {
        display: inline-block!important;
        font-size: 14px!important;
        color: #f49a25!important;
        text-align: left!important;
        font-weight: 700!important;
        margin: 0!important;
    }
    .price-special {
        display: inline-block!important;
        font-size: 12px!important;
        color: #5c5c5c!important;
        margin-left: 0!important;
        margin-right: 5px!important;
        text-decoration: line-through!important;
    }
    .price-old {
        display: inline-block!important;
        color: #5c5c5c!important;
        margin-left: 0!important;
        font-size: 12px!important;
        text-decoration: line-through!important;
    }
    .deals-discount {
        border-radius: 50%;
        right: 0px;
        top: 5%;
        position: absolute;
        background:#f49a25;
        text-align: center;
        z-index: 1;
        width: 50px;
    }
    .deals-discount h5 {
        color:#fff;
        font-weight: 600;
        padding: 1px;
    }
    .top-banner {
        margin-top: 0!important;
        margin-bottom: 25px!important;
    }
    .top-banner img {
        width: 100%;
    }
    .top-image img {
        width: 100%;
    }
    .top-banners {
        margin-bottom: 25px;
    }
    .readyToEatBanner, .readyToCookBanner, .seedsMixesBanner, .rawAndroastedSeedsBanner {
        cursor: pointer;
    }
    #readyToEatBreakfast, #seedsMixes, #readyToCookBreakfast, #rawAndRoastedSeeds, #others {
        padding-top: 15px;
    }
    .sub-category {
        font-size: 25px;
        margin-top: 25px;
        font-weight: bold;
    }
    .price, .coupon {
        padding-left: 0px!important;
        padding-right: 0px!important;
    }
    .coupon {
        min-height: 20px !important;
        text-align: left;
        font-size: 12px !important;
        margin-bottom: 0 !important;
    }
    .category-name h4 {
        border: 1px solid #c6c6c6!important;
        border-radius: 4px;
        padding: 15px;
        margin-top: 5px!important;
        font-size: 12px!important;
        font-weight: bold;
        text-align: center;
    }
    .category-name h4:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    }
    .category-name-2 h4 {
        background: #000;
        color: #fff;
        font-size: 23px;
        text-align: center;
        min-height: 156px;
        padding: 57px 10px;
        font-family: gothamBold!important;
    }
    .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .options label {
        font-size: 13px!important;
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0!important;
        text-align: center;
        min-width: 52px;
    }
    .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000 !important;
    }
    label.disabled {
        background: #ddd !important;
        cursor: not-allowed !important;
    }
    .options .radio input {
        display: none !important;
    }
    @media(max-width: 1000px) {
        .coupon {
            font-size: 11px!important;
        }
        .options .radio {
            margin: 0px 0px 5px -2px !important;
        }
        .radio label span {
            font-size: 12px !important;
        }
        .product-image {
            padding: 5%!important;
        }
        .category-name h4 {
            line-height: 15px;
            font-size: 12px!important;
            padding: 8px 4px;
            margin-top: 0!important;
            /*min-height: 63px!important;*/
        }
        .category-name-2 h4 {
            font-size: 15px;
            min-height: 100px;
            padding: 30px 7px;
            line-height: 20px;
        }
        .category-name, .category-name-2, .bookmark-heading {
            padding-left: 5px!important;
            padding-right: 5px!important;
        }
    }
    .bookmark-heading {
        font-weight: 600;
    }
</style>
<?php echo $header; ?>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="row main-row">
  <div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-md-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-md-12'; ?>
        <?php } else { ?>
            <?php $class = 'col-md-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    	    <h3><?php echo $heading_title; ?></h3><br />
	        <div class="custom-category">
	            <?php if ($deals_data) { ?>
	                <input type="hidden" name="product_id" value="" />
	                <br />
	                <div class="toolbar" hidden> 
			            <div class="toolbar1">
				            <div class="btn-group">
					            <button type="button" id="list-view" class="btn btn-default"></button>
					            <button type="button" id="grid-view" class="btn btn-default"></button>
				            </div>
		            	</div>
		            </div>
		            <div class="custom-products">
			            <div class="col-md-12 col-xs-12 col-sm-12 products-container">
			                <?php foreach ($deals_data as $deals) { ?>
			                    <div class="col-md-3 col-sm-6 col-xs-6 mobile">
				                    <div class="product-thumb item-inner"  >
				                        <div class="images-container">
				                            <div class="deals-discount"><h5><?php echo $deals['discount']; ?>%<br/>Off</h5></div>
					                        <a href="<?php echo $deals['href']; ?>">
					                            <div class="pro_img">
					                                <?php if($deals['rotator_image']){ ?>
									                    <img class="image2 product-image" src="<?php echo $deals['rotator_image']; ?>" alt="<?php echo $deals['name']; ?>" />
								                    <?php } ?>
                                                    <img alt="True Elements" class="product-image" src="<?php echo $deals['thumb']; ?>">
                                                </div>
                                            </a>
                                        </div>
					                    <div>
						                    <p class="product-name-text">
						                        <a class="hidden-xs hidden-sm" href="<?php echo $deals['href']; ?>"><?php echo $deals['name']; ?></a>
						                        <a class="hidden-md hidden-xl hidden-lg" href="<?php echo $deals['href']; ?>"><?php echo $deals['name_mobile']; ?></a>
						                    </p>
						                    <?php $is_optionqty = 1; ?>
						                    <?php if ($deals['options']) { ?>
                                                <div class="options options-<?php echo $deals['product_id']; ?> col-md-12 col-xs-12 col-sm-12">
    	                                            <?php foreach($deals['options'] AS $option) { ?>
    	                                                <?php $countoption = count($option['product_option_value']); ?>
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                                            <div class="radio">
                                                                <?php if($countoption == 1) { ?>
                                                                    <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                        <input type="radio" class="option-<?php echo $deals['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $deals['product_id']; ?>, <?php echo $deals['coupon_discount']; ?>, '<?php echo $deals['coupon_type']; ?>')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                                <?php } else { ?>
                                                                    <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                        <input type="radio" class="option-<?php echo $deals['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $deals['product_id']; ?>, <?php echo $deals['coupon_discount']; ?>, '<?php echo $deals['coupon_type']; ?>')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                                <?php } ?>
                                                                <?php if($option_value['price']) { ?>
                                                                    <span><?php echo $option_value['name']; ?></span>
                                                                <?php } ?>
                                                                </label>
                                                            </div>
                                                        <?php } ?>
    	                                            <?php } ?>
    	                                        </div>
                                            <?php } else { ?>
    	                                        <div class="options col-md-12 col-xs-12 col-sm-12"></div>
    	                                        <?php $is_optionqty = 0; ?>
    	                                    <?php } ?>
    	                                    <?php if($deals['personalized'] == 0) { ?>
						                        <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                                    <div class="box-price">
    	   			                                    <?php if ($deals['price']) { ?>
    	        		                                    <p class="price">
    	    			                                        <?php if (!$deals['special']) { ?>
    	    			                                            <span class="price-old price-old-<?php echo $deals['product_id']; ?>" style="text-decoration: none!important;"><?php echo $deals['price']; ?></span>
    			    		                                    <?php } else { ?>
    			    	                                            <span class="price-special price-special-<?php echo $deals['product_id']; ?>" style="text-decoration: none!important;"><?php echo $deals['special']; ?></span>
    			    	                                            <span class="price-old price-old-<?php echo $deals['product_id']; ?>"><?php echo $deals['price']; ?></span>
    			    	                                        <?php } ?>
    			    	                                    </p>
    			    	                                    <p class="coupon col-md-12 col-xs-12 col-sm-12 pull-right"><span class="price-deal price-deal-<?php echo $deals['product_id']; ?>"><?php echo $deals['deal_price']; ?></span> with <?php echo $deals['coupon'] ? 'Coupon: <b>' . $deals['coupon'] . '</b>' : ''; ?></p>
    			                                        <?php } ?>
    		                                        </div>
    	                                        </div>
    	                                        <?php if($is_optionqty == 0 && $deals['quantity'] > 0) { ?>
    	                                            <div class="qty-container" style="margin-top: 10px;">
    	                                                <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $deals['product_id']; ?>)" />
    	                                                <input type="text" class="quantity-<?php echo $deals['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $deals['product_id']; ?>)" />
    		                                            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $deals['product_id']; ?>)" />
    	                                            </div>
    	                                            <button type="button" class="btn button button-product-cart cart-<?php echo $deals['product_id']; ?> hidden-xs hidden-sm" title="Add to Cart" onclick ="addtocartDeals(<?php echo $deals['product_id']; ?>, '<?php echo $deals['coupon']; ?>')">Add to Cart</button>
    	                                            <button type="button" class="btn button button-product-cart cart-<?php echo $deals['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" title="Add to Cart" onclick ="addtocartDeals(<?php echo $deals['product_id']; ?>, '<?php echo $deals['coupon']; ?>')"><i class="fa fa-shopping-cart"></i></button>
    	                                        <?php } else { ?>
    	                                            <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
    	                                            <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
    	                                        <?php } ?>
						                    <?php } else { ?>
						                        <div class="price-label col-md-12 col-xs-12 col-sm-12">
    	    	                                    <div class="box-price">
    	                                                <p class="price"></p>
                                                    </div>
                                                </div>
                                                <?php if($deals['quantity'] > 0) { ?>
						                            <a href="<?php echo $deals['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						                        <?php } else { ?>
						                            <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
						                            <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						                        <?php } ?>
    	                                    <?php } ?>
					                    </div>
				                    </div>
			                    </div>
			                <?php } ?>
		            	</div>
		            </div>
		            <div class="row toolbar4">
	                    <div class="col-md-12 col-sm-12 col-xs-12">
	                        <center><?php echo $pagination; ?></center>
	                    </div>
	                </div>
                <?php } ?>
	        </div>
            <?php if (!$deals_data) { ?>
                <p><?php echo $text_empty; ?></p>
                <div class="buttons">
                    <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                </div>
            <?php } ?>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
  </div>
</div>
<script type="text/javascript">
    function addtocartDeals(product_id, coupon) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add&coupon=' + coupon,
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left" ><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center">' + json['success'] + '</div></div>';
                    
                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id, coupon_discount, coupon_type) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					if(json.new_price.special) {
					    var special = parseInt(json.new_price.special.replace(/\,/g,'').substr(3));
					    var discounted_price = special - (special * parseInt(coupon_discount) / 100);
					    $('.price-deal-' + product_id).html("Rs." + Math.round(discounted_price));
					    $('.price-special-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					    //$('.price-new-' + product_id).html(json.new_price.special);
					    //$('.price-old-' + product_id).html(json.new_price.price);
					    
					} else {
					    var price = parseInt(json.new_price.price.replace(/\,/g,'').substr(3));
					    var discounted_price = price - (price * parseInt(coupon_discount) / 100);
					    
					    $('.price-deal-' + product_id).html("Rs." + Math.round(discounted_price));
					    $('.price-old-' + product_id).html(json.new_price.price);
					    //$('.price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
</script>
<script type="text/javascript"><!--
    $('.options .radio input[type="radio"]').click(function() {
        $('.options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
<?php echo $footer; ?>