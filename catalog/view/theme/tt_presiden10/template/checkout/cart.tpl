<?php echo $header; ?>
<link href="catalog/view/javascript/addtocart/noty.css" type="text/css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/addtocart/notice_add.min.js" type="text/javascript"></script>
<style>
        .centerplease { margin: 0 auto; max-width: 270px; font-size: 40px; } 
        .question {color:#000;background:#f2f2f2;padding: 6px 6px 6px 10px;width: 100%;cursor: pointer;border-radius: 5px;} 
        .panel-title {font-size:15px;}
        .answers {margin-top: -32px; padding: 0px 15px; margin: 5px 0; height: 0; overflow: hidden; z-index: -1; opacity: 0; -webkit-transition: .7s ease; -moz-transition: .7s ease; -o-transition: .7s ease; transition: .7s ease; } 
        .questions:checked ~ .answers{ height: auto; opacity: 1; padding: 15px; } 
        .plus, .minus { position: absolute; margin-top: 13px;margin-left: 78%; z-index: 5; font-size: 2em; line-height: 100%; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none; -webkit-transition: .3s ease; -moz-transition: .3s ease; -o-transition: .3s ease; transition: .3s ease;}
        
        .pluss, .minuss {
             float: right;
             font-size: 25px;
        }
        .minuss { display: none;}
        .questions:checked ~ .pluss { display: none; } 
        .questions:checked ~ .minuss { display: block; }
        .questions { display: none; } 
        
    .center {
        text-align: center;
    }
    body {
        font-size: 15px;
    }
    .table-bordered tbody tr td, .table-bordered thead tr td {
        border: 1px solid #f2f2f2!important;
    }
    .checkout-cart #content h1 {
        text-transform: none;
        color: #000!important;
        margin-bottom: 10px!important;
    }
    .text-left .btn-block {
        width: 100%!important;
    }
    
    .btn-default:active, .btn-default:focus, .btn-default:active:focus {
        border-color: #f49a25!important;
        color: #fff;
        background: #f49a25!important;
    }
    .alert {
        padding: 10px!important;
        margin-bottom: 15px !important;
    }
    .text-left a {
        color: #000!important;
    }
    .text-left a:hover {
        color: #f49a25!important;
    }
    .empty-cart {
        text-align: center;
    }
    .empty-cart img {
        height: 250px;
        width: 300px;
    }
    .text-empty-cart {
        font-size: 20px!important;
    }
    
    .table-responsive {
        overflow-x: hidden!important;
        border: 1px solid #ddd;
    }
    .table-responsive hr {
        margin-top: 10px!important;
        margin-bottom: 10px!important;
    }
    .table-heading {
        border: 1px solid #ddd;
        margin-top: 0px;
    }
    .table-content {
        margin-top: 10px;
    }
    .table-content div:nth-child(3), .table-content div:nth-child(4), .table-content div:nth-child(5) {
        margin-top: 3%;
    }
    .quantity {
        margin-left: 25%;
        margin-right: 25%;
    }
    .table-heading h4 {
        font-weight: 600;
    }
    .order-total {
        padding-bottom: 10px;
    }
    @media(max-width: 800px) {
        .product-list {
            min-height: 110px;
        }
        .img-thumbnail {
            margin-left: 10px;
        }
        .quantity {
            margin-left: 10px!important;
        }
        .order-total {
            padding-bottom: 10px!important;
        }
        .free-delivery-value {
             margin-top: -10px!important;
        }
        .top-banner {
            margin-right: 0 !important;
            margin-left: 0 !important;
        }
        #content h1 {
  	        padding: 10px;
  	    }
  	    .plus, .minus {
  	        margin-top: 40px;
  	    }
  	    .quantity-container {
            width: 50%;
        }
        .try-new-image {
            padding-right: 0!important;
        }
        .radio label span {
            font-size: 12px!important;
        }
        .des-container .options .radio {
            margin: 0px 0px 5px -2px!important;
        }
        .button-cart, .button-personalize, .oos {
            padding: 7px 10px!important;
            font-size: 14px!important;
            margin-bottom: 15px!important;
        }
        .quantity-new-launch {
            width: 60%!important;
        }
        .new-launch-container {
            padding: 7px!important;
        }
    }
    .product-list a {
        font-weight: 600;
    }
    #popup_totals_sub_total,#popup_totals_total_customer_group_discount, #popup_totals_salescombo, #popup_totals_tax {
        display: none;
    }
    #overlay {
  		position: fixed;
  		display: none;
  		width: 100%; 
  		height: 100%; 
  		top: 0;
  		left: 0;
  		right: 0;
  		bottom: 0;
  		background-color: rgba(0,0,0,0.1);
  		cursor: pointer;
  		z-index: 10000;
  	}
  	#overlay .fa {
  		font-size: 25px!important;
		margin-top: 50vh;
		margin-left: 50vw;
		color: #fff;
  	}
  	.remove-button {
  		font-size: 25px;
		margin-right: 20px;
		cursor: pointer;
  	}
  	.btn-plus, .btn-minus {
  	    background: #fff;
        border: 1px solid #ddd;
  	}
  	.btn-plus:hover, .btn-minus:hover, .btn-plus:focus, .btn-minus:focus  {
  	    background: #f49a25;
  	    color: #fff;
        border: 1px solid #f49a25;
  	}
  	.quantity .btn-block {
  	    display: flex;
  	}
  	.top-banner{
  	    margin-bottom: 18px;
  	}
  	.free-delivery-value {
  	    text-align: right;
        padding-right: 0;
        margin: 5px 0;
  	}
  	.panel-title {
  	    margin-left: 0%;
        font-weight: 600;
  	}
  	.quantity-input {
  	    background: #fff;
  	}
  	.alert-success, .alert-danger {
  	    margin-bottom: 30px !important;
  	}
  	.alert-danger {
  	    margin-left: 15px;
  	    margin-right: 15px;
  	}
  	.cart-msg h4 {
        text-align: center;
        font-weight: 600;
        line-height: 20px;
    }
    .des-container {
        padding: 0!important;
    }
    .product-name {
        padding-right: 0!important;
        padding-top: 5px;
        font-size: 15px!important;
        min-height: 50px;
    }
    .price-label {
        width: 100%!important;
        padding-left: 0!important;
    }
    .button-cart-mobile {
        float: right;
        padding: 7px 10px!important;
    }
    .product-options {
        background: #fff;
    }
    .quantity-container {
        width: 60%;
    }
    .button-cart {
        font-size: 13px !important;
        padding: 8px 10px !important;
        float: right!important;
    }
    .try-new-image {
        padding-left: 0!important;
    }
    .try-new-image img {
        width: 100%;
    }
    .des-container .options .radio input {
        display: none !important;
    }
    .des-container .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .des-container .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    .des-container .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    .options {
        min-height: 55px!important;
    }
    .oos {
        background: #777 !important;
        border-radius: 4px !important;
        float: right !important;
        font-size: 13px !important;
        padding: 8px 10px !important;
    }
    .button-cart {
        float: right!important;
        font-size: 13px!important;
        padding: 8px 10px!important;
    }
    .button-personalize {
        font-size: 13px!important;
        padding: 8px 10px!important;
        float: right!important;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 13px!important;
        padding: 8px 10px!important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .quantity-new-launch {
        height: 35px!important;
        width: 50%;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container">
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row cart-data"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
     <?php if($text_error) { ?>
    <i></i>
    <?php } else { ?>
    <div class="col-md-12 top-banner hidden-xs hidden-sm">
        <img src="<?php echo $top_banner_cart_desktop; ?>" class="img-responsive" alt="True Elements" />
    </div>
   
    <?php } ?>
    <div class="container cart-msg">
        <!--h4>Due to Coronavirus (COVID-19), shipments are temporarily delayed.</h4>
        <h4 style="font-size: 12px!important;">We have temporarily disabled <span style="font-size: 14px!important;">'cash on delivery'</span> due to safety concerns. You can continue using multiple prepaid options from our menu. Thanks for your support. Stay Safe!</h4-->
    </div>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if($text_error) { ?>
        <div class="empty-cart"><img src="<?php echo $empty_cart; ?>" alt="True Elements" /></div>
        <center class="text-empty-cart"><?php echo $text_error; ?></center>
        <br/>
        <div class="center"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_shopping; ?></a></div>
      <?php } else { ?>
        <h1>
            <?php echo $heading_title; ?>  
            <?php if ($weight < 1) { ?>
                &nbsp;(<?php $weight = $weight * 1000; echo round($weight,0); ?>Gm) &nbsp;&nbsp; <?php if ($remaining > 1) { ?> <span class="pull-right">Add more items worth Rs <?php echo $remaining; ?> & get 15% off </span><?php } ?>
            <?php } else { ?>
                &nbsp;(<?php echo round($weight,1); ?>Kg) <?php if ($remaining > 1) { ?> <span class="pull-right">Add more items worth Rs <?php echo $remaining; ?> & get 15% off </span><?php } ?>
            <?php } ?>
        </h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="table-responsive">
            <div id="overlay"></div>
            <div class="row table-heading text-center hidden-xs hidden-sm">
                <div class="col-md-2">
                    <h4><?php echo $column_image; ?></h4>
                </div>
                <div class="col-md-3">
                    <h4><?php echo $column_name; ?></h4>
                </div>
                <div class="col-md-3">
                    <h4><?php echo $column_quantity; ?></h4>
                </div>
                <div class="col-md-2">
                    <h4><?php echo $column_price; ?></h4>
                </div>
                <div class="col-md-2">
                    <h4><?php echo $column_total; ?></h4>
                </div>
            </div>
            <div class="cart-products">
            <?php foreach ($products as $product) { ?>
                <div class="row table-content">
                    <div class="col-md-2 col-xs-4 col-sm-4">
                        <?php if ($product['thumb']) { ?>
                            <div class="text-center">
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-md-3 col-xs-8 col-sm-8">
                        <div class="product-list">
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            <?php if (!$product['stock']) { ?>
                                <span class="text-danger">***</span>
                            <?php } ?>
                            <?php if($product['product_id'] == 6449) { ?>
                                <?php if($product['option_category_count']) { ?>
                                    <?php $i = 1; ?>
                                    <br/>
                                    <?php $length = count($product['option_category_count']); ?>
                                    <small><small>
                                    <?php foreach($product['option_category_count'] AS $option_category) { ?>
                                        <?php echo $option_category['name'],"(",$option_category['value'],")"; ?>
                                        <?php if($i != $length) { ?>
                                            ,
                                        <?php } ?>
                                        <?php $i++; ?>
                                    <?php } ?>
                                    </small></small>
                                <?php } ?>
                            <?php } ?>
                            <span title="Remove" class="remove-button pull-right hidden-xl hidden-lg hidden-md" onclick="ajax_remove(<?php echo $product['cart_id']; ?>);">&times;</span>
                            <?php if ($product['option']) { ?>
                                <?php $duplicate = ''; ?>
                                <?php foreach ($product['option'] as $option) { ?>
                                    <br />
                                    <?php if($option['name'] == $duplicate) { ?>
                                        <small style="padding-left: 25px;">- <?php echo $option['value']; ?></small>
                                    <?php } else { ?>
                                        <small><b><?php echo $option['name']; ?>:</b></small><br/><small style="padding-left: 25px;">- <?php echo $option['value']; ?></small>
                                    <?php } ?>
                                    <?php $duplicate = $option['name']; ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($product['reward']) { ?>
                                <br />
                                <small><?php echo $product['reward']; ?></small>
                            <?php } ?>
                            <?php if ($product['recurring']) { ?>
                                <br />
                                <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-8 col-sm-8">
                        <div class="quantity cart-quantity-<?php echo $product['product_id']; ?>">
                            <div class="btn-block" style="max-width: 200px;">
                                <input type="button" onclick="minus(<?php echo $product['cart_id']; ?>);" value="-" class="btn btn-minus">
                    	        <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" style="width: 50px; text-align: center;" value="<?php echo round($product['quantity']); ?>" size="1" class="form-control quantity-input quantity_<?php echo $product['cart_id']; ?>" onchange="ajaxcall(<?php echo $product['cart_id']; ?>);"/>
                    	        <input type="button" onclick="plus(<?php echo $product['cart_id']; ?>);" value="+" class="btn btn-plus">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 hidden-xs hidden-sm">
                        <div class="text-center">
                            <?php echo $product['price']; ?>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-4 col-sm-4">
                        <div class="text-center">
                            <?php echo $product['te_total']; ?>
                            <span title="Remove" class="remove-button pull-right hidden-xs hidden-sm" onclick="ajax_remove(<?php echo $product['cart_id']; ?>);">&times;</span>
                        </div>
                    </div>
                </div>
                <hr/>
            <?php } ?>
            
            <?php foreach ($vouchers as $voucher) { ?>
              <div class="row table-content">
                <div class="col-md-2 col-xs-4 col-sm-4"><img src="<?php echo $voucher['voucher_image']; ?>" alt="<?php echo $voucher['description']; ?>" title="<?php echo $voucher['description']; ?>" class="img-thumbnail" /></div>
                <div class="col-md-3 col-xs-8 col-sm-8"><div class="text-center"><?php echo $voucher['description']; ?></div></div>
                <div class="col-md-3 col-xs-8 col-sm-8"><div class="text-center">1</div></div>
                <div class="col-md-2 col-xs-4 col-sm-4"><div class="text-center"><?php echo $voucher['amount']; ?></div></div>
                 <div class="col-md-2 hidden-xs hidden-sm"></div>
                 <div class="col-md-2 col-xs-4 col-sm-4"><span title="Remove" class="remove-button pull-right hidden-xs hidden-sm" onclick="voucher.remove('<?php echo $voucher['key']; ?>');">&times;</span></div>
                </div>
              <?php } ?>
            </div>
            <div class="col-md-3 col-sm-10 col-xs-10 col-md-offset-9 col-xs-offset-2 col-sm-offset-2 order-total" style="padding-bottom: 10px;">
                <?php foreach ($totals as $total) { ?>
                    <div id="popup_totals_<?php echo $total['code']; ?>">
                        <div class="col-md-8 col-xs-8 col-sm-8 text-right"><strong><?php echo $total['title']; ?>:</strong></div>
                        <div class="col-md-4 col-xs-4 col-sm-4 text-right"><?php echo $total['text']; ?></div>
                    </div>
                <?php } ?>
            </div>
            <!--SOF Coupons-->
            <div class="content discount-coupons">
                <?php if($coupons) { ?>
                    <div class="row coupon-list">
                        <div class="col-sm-4 col-sm-offset-8">
                            <input id="question1" name="q" class="questions" type="checkbox">
                            <div class="panel-heading">
  	                            <label for="question1" class="question"><span class="panel-title">Discount Coupons</span><span class="pluss">+</span><span class="minuss">&#8722;</span></label>
                            </div>
                            <div class="answers">
                                <div class="panel-body" style="margin-top: -6%;">
                                    <table class="table table-bordered">
                                        <?php foreach($coupons AS $coupon) { ?>
                                            <tr>
                                                <td class="text-right"><span class="promo"><img src="image/coupon.png" alt="True Elements" width= "17%;">&nbsp;<?php echo $coupon['coupon_code']; ?></span><br><h5>- <?php echo $coupon['description']; ?></h5></td>
                                                <td class="text-right"><button type="button" class="btn btn-primary applyNow" data-coupon_code="<?php echo $coupon['coupon_code']; ?>">Apply Now</button></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <!--EOF Coupons-->      
      
            <div class="row" style="display:none;">
                <?php if ($modules) { ?>
                    <div class="panel-group container" id="accordion">
                        <?php foreach ($modules as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
      </form>
      
      <small class="col-md-12 col-xl-12 col-lg-12 col-sm-12 col-xs-12 free-delivery-value"><?php if($free_delivery_value > 0 && strtolower($coupon) != "freeshipping") { ?>You're just Rs.<?php echo $free_delivery_value; ?>/- away from <b>Free Shipping</b>.<?php } else { ?>Congrats! You're eligible for <b>Free Shipping</b>.<?php } ?></small>
      <small>Apply the coupon code OR Redeem your True Cash in the next step</small>
      <div class="buttons clearfix" style="padding-top: 14px;">
        <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-default" style="background: #000;">Continue</a></div>
               <div class="pull-right"><a href="<?php echo $checkout; ?>" class="btn btn-primary"><?php echo $button_checkout; ?></a></div>
      </div>
      <br/><br/>
      <div class="row top-banner hidden-md hidden-xl hidden-lg">
        <a href="/index.php?route=checkout/cart&product_id=5787&option_id=1&option_value_id=32"><img src="<?php echo $top_banner_cart_mobile; ?>" class="img-responsive" alt="True Elements" /></a>
        </div>
      <?php } ?>
     
    <?php if ($sample_products) { ?>
      	<input type="hidden" name="product_id" value="" />
        <div class="related-product-container quickview-product">
            <div class="module-title"><span>Try our New Launches</span><hr/></div>
            <div class="row">
                <div class="related-product quickview-added qv-wtext col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6 hidden-xs hidden-sm try-new-image">
                        <a href="/index.php?route=checkout/cart&product_id=6468&option_id=1&option_value_id=80"><img alt="True Elements" class="img-responsive" src="<?php echo $cart_new_launch_desktop; ?>"></a>
                    </div>
                    <div class="hidden-md hidden-xl hidden-lg col-xs-12 col-sm-12 try-new-image">
                        <a href="/index.php?route=checkout/cart&product_id=6468&option_id=1&option_value_id=80"><img alt="True Elements" class="img-responsive" src="<?php echo $cart_new_launch_mobile; ?>"></a>
                    </div>
                    <?php foreach ($sample_products as $product) { ?>
                        <div class="col-md-3 col-xs-6 col-sm-6 new-launch-container">
                            <div class="product-thumb item-inner">
                                <div class="images-container">
                                    <a href="<?php echo $product['href']; ?>">
                                        <?php if ($product['thumb']) { ?>
                                            <?php if($product['rotator_image']){ ?>
                                                <img class="image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" style="max-width: 70% !important;"/>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <img src="image/cache/no_image-100x100.png" alt="<?php echo $product['name']; ?>" />
                                        <?php } ?>
                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="image1" style="max-width: 70% !important;"/>
                                    </a>
                                </div>
                                <div class="des-container">
                                    <?php if (isset($product['rating'])) { ?>
                                        <center><div class="ratings">
                                            <div class="rating-box">
                                                <?php for ($i = 0; $i <= 5; $i++) { ?>
                                                    <?php if ($product['rating'] == $i) {
                                                        $class_r= "rating".$i;
                                                        echo '<div class="'.$class_r.'">rating</div>';
                                                    }
                                                }  ?>
                                            </div>
                                        </div></center>
                                    <?php } ?>
                                    <div class="name-wishlist">
                                        <h2 class="product-name hidden-xs hidden-sm"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                                        <h2 class="product-name hidden-md hidden-lg hidden-xl"><a href="<?php echo $product['href']; ?>"><?php echo $product['name_mobile']; ?></a></h2>
                                    </div>
                                	<?php $is_optionqty = 1; ?>
				                    <?php if($product['options']) { ?>
				                        <div class="options options-<?php echo $product['product_id']; ?>">
				                            <?php foreach($product['options'] AS $option) { ?>
                                                <?php $countoption = count($option['product_option_value']); ?>
                                                <?php $price_tooltip_radio = 1; ?>
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <?php if($countoption == 1) { ?>
                                                            <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                        <?php } else { ?>
                                                            <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                        <?php } ?>
                                                        <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                            <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                        <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                            <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                        <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                            <span><?php echo $option_value['name']; ?></span>
                                                        <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
					                        <?php } ?>
					                    </div>
				                    <?php } else { ?>
				                   	    <div class="options"></div>
				                           <?php $is_optionqty = 0; ?>
				                    <?php } ?>
				                    <div class="price-label col-md-12 col-xs-12 col-sm-12">
				                        <div class="box-price">
				                            <?php if ($product['price']) { ?>
				                                <p class="price">
				                                    <?php if (!$product['special']) { ?>
                                                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
				                   	                <?php } else { ?>
				                   	                    <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
				                   	                    <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
				                   	                <?php } ?>
				                                </p>
				                            <?php } ?>
				                        </div>
			                        </div>
				                    <?php if($product['personalized'] == 0) { ?>
			                            <?php if($is_optionqty == 1 && $product['quantity'] > 0) { ?>
						                    <div class="quantity-container">
						                        <input type="button" value="-" class="minus-button" onclick="minus1(<?php echo $product['product_id']; ?>)" />
						                        <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity-new-launch" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>)" />
						                        <input type="button" value="&#43;" class="plus-button" onclick="plus1(<?php echo $product['product_id']; ?>)" />
						                    </div>
						                    <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-sm hidden-xs" onclick ="addtocart(<?php echo $product['product_id']; ?>)">Add to Cart</button>
						                    <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
						                <?php } else { ?>
						                    <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						                    <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						                <?php } ?>
                                    <?php } else { ?>
				                        <?php if($product['quantity'] > 0) { ?>
						                    <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						                <?php } else { ?>
						                    <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						                    <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						                <?php } ?>
				                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    
    
    
    
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div><br />
<script type="text/javascript">
	function ajaxcall(cart_id) {
	    var qty = $('.quantity_' + cart_id).val();
		var rounded_qty = Math.round(qty);
		$('.quantity_' + cart_id).val(rounded_qty);
		
		$.ajax({
			url: 'index.php?route=checkout/cart/ajaxupdatecart',
			type: 'post',
			data: $('.quantity-input'),
			dataType: 'json',
			beforeSend: function() {
			    $("#overlay").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>');
				$("#overlay").css({"display":"block"});
			},
			success: function(json) {
				if (json['weight'] < 1) {
				    var weight = '';
				    var weight_in_gm = json['weight'] * 1000;
					weight += '<?php echo $heading_title; ?>&nbsp;(' + Math.round(weight_in_gm) + 'Gm)';
					if(json['remaining'] > 1){
					    weight += '<span class="pull-right">Add more items worth Rs' + json['remaining'] + '& get 15% off </span>';
					}
					
					$('#content h1').html(weight);
				} else {
				    var weight = '';
					weight += '<?php echo $heading_title; ?>&nbsp;(' + json['weight'].toFixed(1) + 'Kg)';
					if(json['remaining'] > 1){
					    weight += '<span class="pull-right">Add more items worth Rs' + json['remaining'] + '& get 15% off </span>';
					}
					$('#content h1').html(weight);
				}
				if (json['free_delivery_value'] > 0 && json['coupon'].toLowerCase() != "freeshipping") {
				    var free_delivery_value = '';
				    free_delivery_value += "You're just Rs." + json['free_delivery_value'] + "/- away from <b>Free Shipping</b>.";
				    $('.free-delivery-value').html(free_delivery_value);
				} else {
				    var free_delivery_value = '';
				    free_delivery_value += "Congrats! You're eligible for <b>Free Shipping</b>.";
				    $('.free-delivery-value').html(free_delivery_value);
				}

				if (json['products'] != '') {
					var html = '';
					for (i = 0; i < json['products'].length; i++) {
					    html += '<div class="row table-content">';
					    
					    html += '<div class="col-md-2 col-xs-4 col-sm-4">';
					    if (json['products'][i]['thumb']) {
      						html += '<div class="text-center"><a href="' + json['products'][i]['href'] + '"><img src="' + json['products'][i]['thumb'] + '" alt="' + json['products'][i]['name'] + '" title="' + json['products'][i]['name'] + '" class="img-thumbnail"></a></div>' ;
      					}
      					html += '</div>';
      					
      					html += '<div class="col-md-3 col-xs-8 col-sm-8"><div class="product-list">';
      					html += '<a href="' + json['products'][i]['href'] + '">' + json['products'][i]['name'] + '</a><span title="Remove" class="remove-button pull-right hidden-xl hidden-lg hidden-md" onclick="ajax_remove(' + json['products'][i]['cart_id'] + ');">&times;</span>';
      					if(!json['products'][i]['stock']) {
      						html += '<span class="text-danger">***</span>';
      					}
      					
      					if(json['products'][i]['product_id'] == 6449) {
      					    var option_category_count = 0;
      					    if(json['products'][i]['option_category_count']) {
                            	var counter = 0;
                            	var option_category = json['products'][i]['option_category_count'];
                            	html += '<br/>';
                            	for(j = 0; j < option_category.length; j++) {
                            	    option_category_count++;
                            	}
                            	
                        	    html += '<small><small>';
                            	for(j = 0; j < option_category.length; j++) {
                            		
                            	    html += option_category[j]['name'] + '(' + option_category[j]['value'] + ')';
                            	    if(counter != option_category_count) {
                            		    html += ', ';
                                	}
                            	    counter++;
                                }
                                html += '</small></small>';
                            }
      					}
      					
      					if(json['products'][i]['option']) {
      					    var duplicate = '';
      						for (j = 0; j < json['products'][i]['option'].length; j++) {
      							html += '<br/>';
      							if(json['products'][i]['option'][j]['name'] == duplicate) {
      								html += '<small style="padding-left: 25px;">- ' + json['products'][i]['option'][j]['value'] + '</small>';
      							} else {
      								html += '<small><b>' + json['products'][i]['option'][j]['name'] + ':</b></small><br/><small style="padding-left: 25px;">- ' + json['products'][i]['option'][j]['value'] + '</small>';
      							}
      							var duplicate = json['products'][i]['option'][j]['name'];
      						}
      					}
      					if(json['products'][i]['reward']) {
      						html += '<br/><small>' + json['products'][i]['reward'] + '</small>';
      					}
      					if (json['products'][i]['recurring']) {
      						html += '<br/><span class="label label-info"><?php echo $text_recurring_item; ?></span><small>' + json['products'][i]['recurring'] + '</small>';
      					}
      					html += '</div></div>';
      					
      					html += '<div class="col-md-3 col-xs-8 col-sm-8"><div class="quantity cart-quantity-' + json['products'][i]['product_id'] + '"><div class="btn-block" style="max-width: 200px;">';
      					html += '<input type="button" onclick="minus(' + json['products'][i]['cart_id'] + ');" value="-" class="btn btn-minus">';
                        html += '<input type="text" name="quantity[' + json['products'][i]['cart_id'] + ']" value="' + Math.round(json['products'][i]['quantity']) + '" size="1" onchange="ajaxcall(' + json['products'][i]['cart_id'] + ');" style="width: 50px; text-align: center;" class="form-control quantity-input quantity_' + json['products'][i]['cart_id'] + '" />';
               	 		html += '<input type="button" onclick="plus(' + json['products'][i]['cart_id'] + ');" value="+" class="btn btn-plus">';
               	 		html += '</div></div></div>';
               	 		
               	 		html += '<div class="col-md-2 hidden-xs hidden-sm"><div class="text-center">' + json['products'][i]['price'] + '</div></div>';
               	 		
               	 		html += '<div class="col-md-2 col-xs-4 col-sm-4"><div class="text-center">' + json['products'][i]['te_total'] + '<span title="Remove" class="remove-button pull-right hidden-xs hidden-sm" onclick="ajax_remove(' + json['products'][i]['cart_id'] + ');">&times;</span></div></div>';
               	 		
               	 		html += '</div><hr/>';
      				}
      				$('.cart-products').html(html);
      				if(json['error_warning']) {
      				    var alert_div = '';
      				    alert_div += '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error_warning'];
                        alert_div += '<button type="button" class="close" data-dismiss="alert">&times;</button></div>';
                        $('.alert-danger').remove();
                        $('.cart-data').prepend(alert_div);
      				} else {
      				    $('.alert-danger').remove();
      				}
					var totals = '';
					if (json['totals'] != '') {
						for(i = 0; i < json['totals'].length; i++) {
						    totals += '<div id="popup_totals_' + json['totals'][i]['code'] + '">';
						    totals += '<div class="col-md-8 col-xs-8 col-sm-8 text-right"><strong>' + json['totals'][i]['title'] + '</strong></div>';
						    totals += '<div class="col-md-4 col-xs-4 col-sm-4 text-right">' + json['totals'][i]['text'] + '</div>';
                            totals += '</div>';
						}
					}
					$('.order-total').html(totals);
					
					var coupons = '';
					if(json['coupons']) {
					    coupons += '<div class="row coupon-list"><div class="col-sm-4 col-sm-offset-8"><input id="question1" name="q" class="questions" type="checkbox"><div class="panel-heading"><label for="question1" class="question"><span class="panel-title">Discount Coupons</span><span class="pluss">+</span><span class="minuss">&#8722;</span></label></div><div class="answers"><div class="panel-body" style="margin-top: -6%;"><table class="table table-bordered">';
					    
					    for(i = 0; i < json['coupons'].length; i++) {
					        coupons += '<tr>';
					        
					        coupons += '<td class="text-right"><span class="promo"><img alt="True Elements" src="image/coupon.png" width= "17%;">' + json['coupons'][i]['coupon_code'] + '</span><br><h5>- ' + json['coupons'][i]['description'] + '</h5></td>';
					        
					        coupons += '<td class="text-right"><button type="button" class="btn btn-primary applyNow" data-coupon_code="' + json['coupons'][i]['coupon_code'] + '">Apply Now</button></td></tr>';
					        
					    }
					    
					    coupons += '</table></div></div></div></div>';
					    $('.discount-coupons').html(coupons);
					}
				} else {
					var html = '';
					
					html += '<div class="empty-cart"><img alt="True Elements" src="' + json['empty_cart'] + '" /></div>';
					html += '<center class="text-empty-cart">' + json['text_error'] + '</center><br/>';
					html += '<div class="center"><a href="' + json['continue'] + '" class="btn btn-default">' + json['button_shopping'] + '</a></div>';
					$('#content').html(html);
				}
				$('#cart-total').html(json['total']);
				$('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
			},
			complete: function() {
				$("#overlay").css({"display":"none"});
			}
		});
	}

	function ajax_remove(cart_id) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + cart_id,
			beforeSend: function() {
			    $("#overlay").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>');
				$("#overlay").css({"display":"block"});
			},
			success: function() {
				ajaxcall(cart_id);
			},
			complete: function() {
				$("#overlay").css({"display":"none"});
			}
		});
	}
</script>
<script type="text/javascript">
	function plus(cart_id) {
		var qty = $('.quantity_' + cart_id).val();
		var incr = parseInt(qty) + 1;
		if (incr <= 0) { incr = 1; }
		$('.quantity_' + cart_id).val(incr);
		ajaxcall(cart_id);
	}

	function minus(cart_id) {
		var qty = $('.quantity_' + cart_id).val();
		var decr = parseInt(qty) - 1;
		if (decr <= 0) { decr = 1; }
		$('.quantity_' + cart_id).val(decr);
		ajaxcall(cart_id);
	}
</script>
<script type="text/javascript">
$('body').on('click','.applyNow', function() {
  var coupon_code = $(this).data('coupon_code');
  $('#input-coupon').val(coupon_code);
  $('#button-coupon').trigger('click');
});

$('body').on('click','#applyNow', function() {
  $('#input-coupon').val(coupons[coupon_id]['code']);
  $('#button-coupon').trigger('click');
});

$(document).ready(function () {
    $('.question').click(function() {
        if($('#question1').is(":checked")) {
            $('.pluss').show();
            $('.minuss').hide();
        } else {
            $('.pluss').hide();
            $('.minuss').show();
        }
    });
});
</script>
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        var currentQty = $('.cart-quantity-' + product_id + ' .quantity-input').val();
        var moreQty = $('.quantity-' + product_id).val();
        var newQty = parseInt(currentQty) + parseInt(moreQty);
        $('.cart-quantity-' + product_id + ' .quantity-input').val(newQty);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                    ajaxcall();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus1(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus1(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('.price-new-' + product_id).html(json.new_price.special);
					$('.price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.special) {
					    $('.price-new-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('.price-new-' + product_id).html(json.new_price.price);
					}
				if(json.new_price.quantity < 1) {
				    
				        $('#button-cart-'+ product_id).html('<?php echo "Preorder"; ?>');
						 $('#button-cart-'+ product_id).addClass('preorder');
                    } else {
                        $('#button-cart-'+ product_id).html('<?php echo "Add to Cart"; ?>');
					    $('#button-cart-'+ product_id).removeClass('preorder');
                    }	
				}
				
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.des-container .options .radio input[type="radio"]').click(function() {
        $('.des-container .options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>
<?php echo $footer; ?>