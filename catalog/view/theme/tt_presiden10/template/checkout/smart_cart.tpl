<?php echo $header; ?>
<style>
    .breadcrumb {
    margin-bottom: 0!important;
}
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul> 

 <script type="text/javascript">
 	$(document).ready(function() {
 		$(".slider .tab_content_category").hide();
 		<?php if($order_history_products) { ?>
 		    $(".slider .tab_content_category:first").show(); 
 		    $(".slider ul.tabs-categorys li:first").addClass("active");
 		 <?php } else { ?>
 			    $(".slider .tab_content_category:last").show(); 
 		    $(".slider ul.tabs-categorys li:last").addClass("active");
 		<?php } ?> 
 		$(".slider ul.tabs-categorys li").click(function() {
 			$(".slider ul.tabs-categorys li").removeClass("active");
 			$(".slider ul.tabs-categorys .owl-item").removeClass("active");
 			$(this).addClass("active");
 			$(this).parent().addClass("active");
 			$(".slider .tab_content_category").hide();
 			$(".slider .tab_content_category").removeClass("animate1 <?php echo $tab_effect;?>");
 			var activeTab = $(this).attr("rel"); 
 			$("#"+activeTab) .addClass("animate1 <?php echo $tab_effect;?>");
 			$("#"+activeTab).show(); 
 		});
 	});
 </script>
 <?php
 $row = $config_slide['f_rows'];
 if(!$row) {$row=1;}
 ?>
 <style>
 	.item-inner .des-container {
 		padding: 0px 0 0 21px;
 	}

 	.options {
 		min-height: 45px;
 	}
 	.product-name {
 		min-height: 50px!important;
 	}
 	.product-options {
 		margin-top: 10px;
 		text-align: center;
 		float: left;
 		background: #fff!important;
 	}
 	.button-cart {
 		float: right!important;
 		margin-right: 21px!important;
 		font-size: 13px!important;
 		padding: 8px 10px!important;
 	}
 	.button-personalize {
 		font-size: 13px!important;
 		padding: 8px 10px!important;
 		float: right !important;
 		margin-right: 21px !important;
 	}
 	.price-label {
 		padding: 8px 0px !important;
 		min-height: 37px!important;
 		width: 100% !important;
 	}
 	.quantity {
 		height: 35px!important;
 		width: 60%!important;
 		text-align: center;
 		background: #fff!important;
 		border: 1px solid #f2f2f2 !important;
 		float: left;
 	}
 	.minus-button, .plus-button {
 		font-weight: 700;
 		text-align: center;
 		height: 35px!important;
 		width: 20%!important;
 		font-size: 14px!important;
 		padding: 0px!important;
 		background: #fff!important;
 		border: 1px solid #f2f2f2!important;
 		float: left;
 	}
 	
 	.minus-button:hover, .plus-button:hover {
 		background: #f49a25!important;
 		color: #fff!important;
 		border: 1px solid #f2f2f2!important;
 	}
 	
 	.minus-button {
 		border-top-left-radius: 4px !important;
 		border-bottom-left-radius: 4px !important;
 	}
 	
 	.plus-button {
 		border-top-right-radius: 4px !important;
 		border-bottom-right-radius: 4px !important;
 	}
 	.image1, .image2 {
 		padding: 14%;
 	}
 	
 	@media(max-width: 1000px) {
 		.button-cart, .button-personalize, .oos {
 			padding: 7px 10px !important;
 			font-size: 14px!important;
 			margin-bottom: 15px!important;
 		}
 		.des-container .options .radio {
 			margin: 0px 0px 5px -2px!important;
 		}
 		.image1, .image2 {
 			padding: 5%!important;
 		}
 	}
 	
 	@media(min-width: 1000px) {
 		.des-container {
 			height: 200px;
 		}
 	}
 	.oos {
 		background: #777!important;
 		border-radius: 4px!important;
 		float: right!important;
 		margin-right: 21px!important;
 		font-size: 13px!important;
 		padding: 8px 10px!important;
 	}
 	.oos:hover {
 		cursor: not-allowed!important;
 	}
 	.button-cart:focus, .button-personalize:focus {
 		color: #fff!important;
 	}
 	.tabs-categorys {
 		margin-top: 10px!important;
 	}
 	.des-container .options .radio input {
 		display: none !important;
 	}
 	.des-container .options .radio {
 		display: inline-block;
 		margin: 0px 5px 5px 0px;
 	}
 	.des-container .options label {
 		display: block;
 		background-color: #fff;
 		border: 0.5px solid #000;
 		border-radius: 3px;
 		padding: 0;
 		text-align: center;
 		min-width: 52px;
 	}
 	.des-container .options .checked-option {
 		background-color: #000 !important;
 		color: #fff !important;
 		border: 1px solid #000!important;
 	}
 	.radio label span {
 		display: block;
 		font-size: 13px;
 	}
 	label.disabled {
 		background: #ddd!important;
 		cursor: not-allowed!important;
 	}
 	.price, .coupon {
 		padding-left: 0px!important;
 		padding-right: 0px!important;
 	}
 	.coupon {
 		min-height: 20px!important;
 		text-align: left;
 		font-size: 12px!important;
 		margin-bottom: 0!important;
 		color: #000!important;
 	}
 	.breakfast-carousel img, .snacks-carousel img {
 		padding-left: 14%!important;
 		padding-right: 14%!important;
 	}
 	.images-container {
 		padding: 10px;
 	}
 	.img-cat {
 		margin-left: auto;
 		margin-right: auto;
 		display: table; 
 		-webkit-transition: all 0.6s ease;
 		transition: all 0.6s ease;
 	}
 	.p0 {
 		padding: 0;
 	}
 </style>
 <div class="product-tabs-category-container-slider tabcategory-container slider quickview-product" style="margin-bottom:2%">
 	<div class="container">
 		<div class="tab-title">
 			 
 			<ul class="tabs-categorys"> 
 				<li rel="tab_cate_combo">Previously Ordered</li>
 				<li rel="tab_cate_best">Best Sellers</li
 			</ul>
 		</div>
 		<div class="row">
 			<div class="tab_container_category quickview-added" id="tab_container_category"> 
 				<input type="hidden" name="product_id" value="" />
 				
 				<div id="tab_cate_combo" class="tab_content_category">
 					<?php
 					$count = 0;
 					$rows = $config_slide['f_rows'];
 					if(!$rows) { $rows=1; }
 					?>
 					
 					<div class="container">  
 						<div class="row"> 
 							<div class="col-md-12 col-sm-6 cl-ol-xs-6 col-sms-12">
 								<div class="row row2">
 									<div class="previous-products-slider quickview-added category-products" id="previous-products-slider">
 										<?php if($order_history_products): ?>
 											<?php foreach ($order_history_products as $order_history_product) { ?>
 												<?php  if($count % $rows == 0 ) { echo '<div class="row_items product-container">'; } $count++; ?>
 												<div class="item">
 													<div class="item-inner">
 														<?php if($config_slide['f_show_label']): ?>
 															<?php if($order_history_product['special']) { ?>
 																<div class="label-product <?php if($order_history_product['is_new']){ echo " f-label "; } ?>">
 																	<span><?php echo $text_sale; ?></span>
 																</div>
 															<?php }?>
 															<?php if($order_history_product['is_new']){ ?>
 																<div class="label-product l-new">
 																	<span><?php echo $text_new; ?></span>
 																</div>
 															<?php } ?>
 														<?php endif;?>
 														<div class="images-container">
 															<a class="image" href="<?php echo $order_history_product['href']; ?>/?tracking=<?php echo $tracking_code; ?>">
 																<img class="image1" src="<?php echo $order_history_product['thumb']; ?>" alt="<?php echo $order_history_product['name']; ?>" />
 																<?php if($order_history_product['rotator_image']): ?>
 																	<img class="image2" src="<?php echo $order_history_product['rotator_image']; ?>" alt="<?php echo $order_history_product['name']; ?>" />
 																<?php endif; ?>
 															</a>
 														</div><!--images-container-->
 														<?php if (isset($order_history_product['rating'])) { ?>
 															<center> <div class="ratings">
 																<div class="rating-box">
 																	<?php for ($i = 0; $i <= 5; $i++) { ?>
 																		<?php if ($order_history_product['rating'] == $i) {
 																			$class_r= "rating".$i;
 																			echo '<div class="'.$class_r.'">rating</div>';
 																		} 
 																	}  ?>
 																</div>
 															</div></center>
 														<?php } ?>
 														
 														<div class="des-container">
 															<div class="name-wishlist">
 																<h2 class="product-name hidden-xs hidden-sm" ><a href="<?php echo $order_history_product['href']; ?>/?tracking=<?php echo $tracking_code; ?>"><?php echo $order_history_product['name']; ?></a></h2>
 																<h2 class="product-name hidden-md hidden-xl hidden-lg" ><a href="<?php echo $order_history_product['href']; ?>/?tracking=<?php echo $tracking_code; ?>"><?php echo $order_history_product['name_mobile']; ?></a></h2>
 															</div>
 															<?php $is_optionqty = 1; ?>
 															<?php if($order_history_product['options']) { ?>
 																<div class="options options-<?php echo $order_history_product['product_id']; ?>">
 																	<?php foreach($order_history_product['options'] AS $option) { ?>
                                <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $order_history_product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $order_history_product['product_id']; ?>,'previous-products-slider')">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if ($option_value['quantity'] > 0) { ?>
                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                        <?php $is_optionqty = 0; ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select-->
                                            <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            	<div class="radio">
                                            		<?php if($countoption == 1) { ?>
                                            			<label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                            				<input type="radio" class="option-<?php echo $order_history_product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $order_history_product['product_id']; ?>,'previous-products-slider')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                            			<?php } else { ?>
                                            				<label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                            					<input type="radio" class="option-<?php echo $order_history_product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $order_history_product['product_id']; ?>,'previous-products-slider')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                            				<?php } ?>
                                            				<?php if($price_tooltip_radio && $option_value['price']) { ?>
                                            					<span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                            				<?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                            					<span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                            				<?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                            					<span><?php echo $option_value['name']; ?></span>
                                            				<?php } ?>
                                            			</label>
                                            		</div>
                                            	<?php } ?>
                                            <?php } ?>
                                        </div>
                                    <?php } else { ?>
                                    	<div class="options hidden-sm hidden-xs hidden-sm"></div>
                                    	<?php $is_optionqty = 0; ?>
                                    <?php } ?>
                                    
                                    <div class="price-rating">
                                    	<div class="price-label">
                                    		<div class="box-price">
                                    			 
                                    				<?php if ($order_history_product['price']) { ?>
                                    					<?php if (!$order_history_product['special']) { ?>
                                    						<div class="price-box box-regular">
                                    							<span class="regular-price">
                                    								<span class="price-new-<?php echo $order_history_product['product_id']; ?> price"><?php echo $order_history_product['price']; ?></span>
                                    							</span>
                                    						</div>
                                    					<?php } else { ?>
                                    						<div class="price-box box-special">
                                    							<p class="special-price"><span class="price-new-<?php echo $order_history_product['product_id']; ?> price"><?php echo $order_history_product['special']; ?></span></p>
                                    							<p class="price-old-<?php echo $order_history_product['product_id']; ?> old-price"><span class="price"><?php echo $order_history_product['price']; ?></span></p>
                                    						</div>
                                    					<?php } ?>
                                    					<p class="coupon col-md-12 col-xs-12 col-sm-12 pull-right"><?php echo $order_history_product['coupon'] ? 'Coupon: <b>' . $order_history_product['coupon'] . '</b>' : ''; ?></p>
                                    				<?php } ?> 
                                    		</div>
                                    	</div>
                                    	
                                    </div>
                                    <div class="add-to-cart" style="padding-top: 7px;">
                                    	<?php if($order_history_product['personalized'] == 0) { ?>
                                    		<?php if($is_optionqty != 1 && $order_history_product['quantity'] > 0) { ?>
                                    			<div style="width: 50%;">
                                    				<input type="button" value="-" class="minus-button" onclick="minus(<?php echo $order_history_product['product_id']; ?>,'previous-products-slider')" />
                                    				<input type="text" class="quantity-<?php echo $order_history_product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $order_history_product['product_id']; ?>,'previous-products-slider')" />
                                    				<input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $order_history_product['product_id']; ?>,'previous-products-slider')" />
                                    			</div>
                                    			<button type="button" class="hidden-md hidden-lg hidden-xl btn button button-cart cart-<?php echo $order_history_product['product_id']; ?>" onclick ="addtocart(<?php echo $order_history_product['product_id']; ?>,'previous-products-slider')"><i class="fa fa-shopping-cart"></i></button>
                                    			<button type="button" class="hidden-xs hidden-sm btn button button-cart cart-<?php echo $order_history_product['product_id']; ?>" onclick ="addtocart(<?php echo $order_history_product['product_id']; ?>,'previous-products-slider')"><?php echo $button_cart; ?></button>
                                    		<?php } else { ?>
                                    			<button class="hidden-md hidden-lg hidden-xl oos btn button" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                    			<button class="hidden-xs hidden-sm oos btn button" type="button" disabled>Stock Over!</button>
                                    		<?php } ?>
                                    	<?php } else { ?>
                                    		<?php if($order_history_product['quantity'] > 0) { ?>
                                    			<a href="<?php echo $order_history_product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
                                    		<?php } else { ?>
                                    			<button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
                                    			<button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                    		<?php } ?>
                                    	<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($count % $rows == 0 || $count == count($order_history_products)): ?>
                        </div>
                    <?php endif; ?>
                <?php } ?>
                <?php else: ?>
                	<p>Empty</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<div id="tab_cate_best" class="tab_content_category">
	<?php
 		$count = 0;
 		$rows = $config_slide['f_rows'];
 		if(!$rows) { $rows=1; }
 	?>
 					
 	<div class="container">  
 					<div class="row"> 
 							<div class="col-md-12 col-sm-6 cl-ol-xs-6 col-sms-12">
 								<div class="row row2">
 									<div class="bestseller-products-slider quickview-added category-products" id="bestseller-products-slider">
 										<?php if($bestproducts): ?>
 											<?php foreach ($bestproducts as $bestproduct) { ?>
 												<?php  if($count % $rows == 0 ) { echo '<div class="row_items product-container">'; } $count++; ?>
 												<div class="item">
 													<div class="item-inner">
 														<?php if($config_slide['f_show_label']): ?>
 															<?php if($bestproduct['special']) { ?>
 																<div class="label-product <?php if($bestproduct['is_new']){ echo " f-label "; } ?>">
 																	<span><?php echo $text_sale; ?></span>
 																</div>
 															<?php }?>
 															<?php if($bestproduct['is_new']){ ?>
 																<div class="label-product l-new">
 																	<span><?php echo $text_new; ?></span>
 																</div>
 															<?php } ?>
 														<?php endif;?>
 														<div class="images-container">
 															<a class="image" href="<?php echo $bestproduct['href']; ?>/?tracking=<?php echo $tracking_code; ?>">
 																<img class="image1" src="<?php echo $bestproduct['thumb']; ?>" alt="<?php echo $bestproduct['name']; ?>" />
 																<?php if($bestproduct['rotator_image']): ?>
 																	<img class="image2" src="<?php echo $bestproduct['rotator_image']; ?>" alt="<?php echo $bestproduct['name']; ?>" />
 																<?php endif; ?>
 															</a>
 														</div><!--images-container-->
 														<?php if (isset($bestproduct['rating'])) { ?>
 															<center> <div class="ratings">
 																<div class="rating-box">
 																	<?php for ($i = 0; $i <= 5; $i++) { ?>
 																		<?php if ($bestproduct['rating'] == $i) {
 																			$class_r= "rating".$i;
 																			echo '<div class="'.$class_r.'">rating</div>';
 																		} 
 																	}  ?>
 																</div>
 															</div></center>
 														<?php } ?>
 														
 														<div class="des-container">
 															<div class="name-wishlist">
 																<h2 class="product-name hidden-xs hidden-sm" ><a href="<?php echo $bestproduct['href']; ?>/?tracking=<?php echo $tracking_code; ?>"><?php echo $bestproduct['name']; ?></a></h2>
 																<h2 class="product-name hidden-md hidden-xl hidden-lg" ><a href="<?php echo $bestproduct['href']; ?>/?tracking=<?php echo $tracking_code; ?>"><?php echo $bestproduct['name_mobile']; ?></a></h2>
 															</div>
 															<?php $is_optionqty = 1; ?>
 															<?php if($bestproduct['options']) { ?>
 																<div class="options options-<?php echo $bestproduct['product_id']; ?>">
 																	<?php foreach($bestproduct['options'] AS $option) { ?>
                                <!--select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control product-options option-<?php echo $bestproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $bestproduct['product_id']; ?>,'bestseller-products-slider')">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <?php if ($option_value['quantity'] > 0) { ?>
                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                        <?php $is_optionqty = 0; ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select-->
                                            <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            	<div class="radio">
                                            		<?php if($countoption == 1) { ?>
                                            			<label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                            				<input type="radio" class="option-<?php echo $bestproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $bestproduct['product_id']; ?>,'bestseller-products-slider')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                            			<?php } else { ?>
                                            				<label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                            					<input type="radio" class="option-<?php echo $bestproduct['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $bestproduct['product_id']; ?>,'bestseller-products-slider')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                            				<?php } ?>
                                            				<?php if($price_tooltip_radio && $option_value['price']) { ?>
                                            					<span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                            				<?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                            					<span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                            				<?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                            					<span><?php echo $option_value['name']; ?></span>
                                            				<?php } ?>
                                            			</label>
                                            		</div>
                                            	<?php } ?>
                                            <?php } ?>
                                        </div>
                                    <?php } else { ?>
                                    	<div class="options hidden-sm hidden-xs hidden-sm"></div>
                                    	<?php $is_optionqty = 0; ?>
                                    <?php } ?>
                                    
                                    <div class="price-rating">
                                    	<div class="price-label">
                                    		<div class="box-price">
                                    			 
                                    				<?php if ($bestproduct['price']) { ?>
                                    					<?php if (!$bestproduct['special']) { ?>
                                    						<div class="price-box box-regular">
                                    							<span class="regular-price">
                                    								<span class="price-new-<?php echo $bestproduct['product_id']; ?> price"><?php echo $bestproduct['price']; ?></span>
                                    							</span>
                                    						</div>
                                    					<?php } else { ?>
                                    						<div class="price-box box-special">
                                    							<p class="special-price"><span class="price-new-<?php echo $bestproduct['product_id']; ?> price"><?php echo $bestproduct['special']; ?></span></p>
                                    							<p class="price-old-<?php echo $bestproduct['product_id']; ?> old-price"><span class="price"><?php echo $bestproduct['price']; ?></span></p>
                                    						</div>
                                    					<?php } ?>
                                    					<p class="coupon col-md-12 col-xs-12 col-sm-12 pull-right"><?php echo $bestproduct['coupon'] ? 'Coupon: <b>' . $bestproduct['coupon'] . '</b>' : ''; ?></p>
                                    				<?php } ?> 
                                    		</div>
                                    	</div>
                                    	
                                    </div>
                                    <div class="add-to-cart" style="padding-top: 7px;">
                                    	<?php if($bestproduct['personalized'] == 0) { ?>
                                    		<?php if($is_optionqty != 1 && $bestproduct['quantity'] > 0) { ?>
                                    			<div style="width: 50%;">
                                    				<input type="button" value="-" class="minus-button" onclick="minus(<?php echo $bestproduct['product_id']; ?>,'bestseller-products-slider')" />
                                    				<input type="text" class="quantity-<?php echo $bestproduct['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $bestproduct['product_id']; ?>,'bestseller-products-slider')" />
                                    				<input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $bestproduct['product_id']; ?>,'bestseller-products-slider')" />
                                    			</div>
                                    			<button type="button" class="hidden-md hidden-lg hidden-xl btn button button-cart cart-<?php echo $bestproduct['product_id']; ?>" onclick ="addtocart(<?php echo $bestproduct['product_id']; ?>,'bestseller-products-slider')"><i class="fa fa-shopping-cart"></i></button>
                                    			<button type="button" class="hidden-xs hidden-sm btn button button-cart cart-<?php echo $bestproduct['product_id']; ?>" onclick ="addtocart(<?php echo $bestproduct['product_id']; ?>,'bestseller-products-slider')"><?php echo $button_cart; ?></button>
                                    		<?php } else { ?>
                                    			<button class="hidden-md hidden-lg hidden-xl oos btn button" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                    			<button class="hidden-xs hidden-sm oos btn button" type="button" disabled>Stock Over!</button>
                                    		<?php } ?>
                                    	<?php } else { ?>
                                    		<?php if($bestproduct['quantity'] > 0) { ?>
                                    			<a href="<?php echo $bestproduct['href']; ?>" class="btn button button-personalize">Personalize Now</a>
                                    		<?php } else { ?>
                                    			<button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
                                    			<button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                    		<?php } ?>
                                    	<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($count % $rows == 0 || $count == count($order_history_product)): ?>
                        </div>
                    <?php endif; ?>
                <?php } ?>
                <?php else: ?>
                	<p><?php echo $text_empty; ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- .tab_container_category -->
</div>    
    
</div>
</div>

<script type="text/javascript">
	function addtocart(product_id, id) {
		$('input[name=\'product_id\']').val(product_id);
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: $('input[name=\'product_id\'], #' + id +' .quantity-' + product_id + ', #' + id +' .checked-option .option-' + product_id),
			dataType: 'json',
			success: function(json) {
				if (json['error']) {
					if (json['error']['option']) {
						if($(".text-danger." + product_id).length == 0) {
							$('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
						}
					}
				}
				if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                    	type: 'success',
                    	layout: json['notice_add_layout'],
                    	text:  success_text ,
                    	textAlign:"right",
                    	animateOpen:{"height":"toggle"},
                    	timeout: json['notice_add_timeout'],
                    	progressBar: false,
                    	closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
            	alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
	}
	function minus(product_id, id) {
		var currentval = parseInt($(".quantity-" + product_id).val());
		$("#" + id + " .quantity-" + product_id).val(currentval-1);
		if($("#" + id + " .quantity-" + product_id).val() <= 0) {
			$("#" + id + " .quantity-" + product_id).val(1);
		}
	}
	
	function plus(product_id, id) {
		var currentval = parseInt($("#" + id + " .quantity-" + product_id).val());
		$("#" + id + " .quantity-" + product_id).val(currentval+1);
	}
	
	function quantityVerify(product_id, id) {
		var currentval = parseInt($("#" + id + " .quantity-" + product_id).val());
		if(currentval <= 0) {
			$("#" + id + " .quantity-" + product_id).val(1); 
		}
	}
</script>
<script type="text/javascript">
	function price_with_options_ajax_call(product_id, id) {
		$(".text-danger." + product_id).remove();
		$.ajax({
			type: 'POST',
			url: 'index.php?route=product/live_options/index&product_id=' + product_id,
			data: $('#'+id+' .checked-option .option-' + product_id),
			dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('#'+id+' .price-new-' + product_id).html(json.new_price.special);
					$('#'+id+' .price-old-' + product_id).html('<span class="price">' + json.new_price.price + '</span>');
					
					if(json.new_price.special) {
						$('#'+id+' .price-new-' + product_id).html(json.new_price.special);
						$('#'+id+' .price-old-' + product_id).html('<span class="price">' + json.new_price.price + '</span>');
					} else {
						$('#'+id+' .price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
		$(".product-options").each(function () {
			if ($(this)[0].length == 0) {
				$(this).hide();
			}
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".bestseller-products-slider").owlCarousel({
			autoPlay: <?php if($config_slide['autoplay']) { echo 'true' ;} else { echo 'false'; } ?>,
			items : <?php if($config_slide['items']) { echo $config_slide['items'] ;} else { echo 4; } ?>,
			slideSpeed : <?php if($config_slide['f_speed']) { echo $config_slide['f_speed']; } else { echo 200;} ?>,
			navigation : true,
			paginationNumbers : true,
			pagination : <?php if($config_slide['f_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
			stopOnHover : false,
			itemsDesktop : [1199,1],
			itemsDesktopSmall : [991,1],
			itemsTablet: [700,1],
			itemsMobile : [480,2]
			
		});

	});
	
	$(document).ready(function() {
		$(".previous-products-slider").owlCarousel({
			autoPlay: <?php if($config_slide['autoplay']) { echo 'true' ;} else { echo 'false'; } ?>,
			items : <?php if($config_slide['items']) { echo $config_slide['items'] ;} else { echo 4; } ?>,
			slideSpeed : <?php if($config_slide['f_speed']) { echo $config_slide['f_speed']; } else { echo 200;} ?>,
			navigation : true,
			paginationNumbers : true,
			pagination : <?php if($config_slide['f_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
			stopOnHover : false,
			itemsDesktop : [1199,1],
			itemsDesktopSmall : [991,1],
			itemsTablet: [700,1],
			itemsMobile : [480,2]
			
		});

	});
	$(document).ready(function() {
		$(".icon-boxes").owlCarousel({
			autoPlay: <?php if($config_slide['autoplay']) { echo 'true' ;} else { echo 'true'; } ?>,
			items : <?php if($config_slide['items']) { echo $config_slide['items'] ;} else { echo 3; } ?>,
			slideSpeed : <?php if($config_slide['f_speed']) { echo $config_slide['f_speed']; } else { echo 200;} ?>,
			navigation : false,
			paginationNumbers : true,
			pagination : <?php if($config_slide['f_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
			stopOnHover : false,
			itemsDesktop : [1199,1],
			itemsDesktopSmall : [991,1],
			itemsTablet: [700,1],
			itemsMobile : [480,1]
			
		});

	});
	
	
</script>
<script type="text/javascript"><!--
	$('.des-container .options .radio input[type="radio"]').click(function() {
		$('.des-container .options .radio input[type="radio"]').each(function() {
			$(this).parents("label").toggleClass('checked-option', this.checked);
		});
	});
</script>
<?php echo $footer; ?>