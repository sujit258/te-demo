<?php if(count($products) == 0): ?>
    <p class="ajax-result-msg"><?php echo $text_empty ?></p>
<?php else: ?>
    <ul class="ajax-result-list" style="text-align: left !important;">
        <?php $i=0; foreach($products as $product): $i++;?>
            <li class="ajax-result-item col-lg-12 col-md-12 col-xs-12 col-sm-12">
	<a href="<?php echo $product['href']; ?>" style="color: #000!important;">
	      	<div class="friend">
	      	    <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4" style="margin-top: 5px!important;">
            	    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
                </div>
                <div class="col-lg-8 col-md-8 col-xs-8 col-sm-8">
                    <p style="padding: 11px 0 0 2px;">
 	                    <span><span style="font-size: 13px!important;"><?php echo $product['name']; ?></span></span>
                    </p>
                </div>
            </div>
            </a>
            
            
	
            </li> <?php if($i%2==0){ ?>
            <div style="clear:both"></div>
            <?php }?>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<style>
    
    .friend{
	height:84px;
	border-bottom:1px solid #e7ebee;		
	position:relative;
}
.friend:hover{
	background:#f1f4f6;
	cursor:pointer;
}
.friend img{
 	border-radius:50%;
 	float:left;
}
.floatingImg{
	width:40px;
	border-radius:50%;
	position:absolute;
	top:0;
	left:12px;
	border:3px solid #fff;
}
 
 
.friend p span{
	font-size:13px;
	font-weight:400;
	color:#000000;
	
}

</style>