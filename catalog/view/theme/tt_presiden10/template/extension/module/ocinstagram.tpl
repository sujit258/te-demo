<style>
    ul.storyUL li img {
        width: 100%;
        float: left;
        border-radius: 5px;
    }
    .socialStories {
        background: #ffffff;
    }
    
    article {
        padding: 50px 0;
    }
    
    article {
        padding: 30px 0;
        
    }
    
     
    .topDesc {
        width: 100%;
        float: left;
        padding: 0px 20px 20px 20px;
        z-index: 2;
        position: relative;
    }
    
    h2.title, h1.title {
        text-align: center;
        color: #676767;
        font-size: 3.2em;
        font-weight: 700;
        padding: 0;
        line-height: 1em;
    }
    
    h1, h2, h3, h4, h5, h6 {
        width: 100%;
        float: left;
    }
    
    ul.storyUL, ul.storyUL li{width: 100%; float: left}
    ul.leftStory{width: 58.2%; float: left}
    ul.rightStory{width: 41.8%; float: left}
    ul.storyUL li{padding: 2px; width: 33.33%; }
    ul.storyUL li img{width: 100%; float: left; border-radius: 5px; }
    ul.storyUL li p{width: 100%; float: left; font-size: 14px; color: #006539; font-weight: 700; margin: 10px 0 0 0; text-align: center;}
    ul.storyUL li.img50{width: 50%;}
    
     
    @media only screen and (min-width:1100px){
        ul.storyUL li{width: 25%}
    	.socialStories ul.storyUL li:last-child{display: none;}
    	.pairedWrap ul.storyUL li{width: 33.33%}
    	h2.title, h1.title {font-size: 3.8em;}
        h2.title, h1.title {margin: 15px 0; }
        .midWrapper, .middleContainer {width: 960px;}
        .midWrapper, .middleContainer {width: 960px;margin: 0 auto;float: none;}
    }	
    
    @media only screen and (max-width:779px){
         	ul.storyUL li{width: 33.33%;} 
    	     
    }    
</style>
<article class="socialStories" id="socialStoriesa">
    <div class="new-title">
            <h2 class="te">Social Stories</h2> 
        </div>
      
            <div class="col-sm-3 col-md-4"></div>
            <div class="title-line col-sm-6 col-md-4" style="margin-top: -10px;margin-bottom: 20px;">
                <div class="tl-1" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
                <div class="tl-2" style="display: inline-block;height: 13px;margin: 0 5px;position: relative;top: 5px;width: 13px;border: 1px solid #ebc131;border-radius: 50px;"></div>
                <div class="tl-3" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
            </div>
            <div class="col-sm-3 col-md-4"></div>
      
        <div class="middleContainer">
       <ul class="storyUL"></ul>
       
       <ul class="storyUL">
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-01.jpg" alt="Curry Tree" /></li>
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-02.jpg" alt="Curry Tree" /></li>
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-03.jpg" alt="Curry Tree" /></li>
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-04.jpg" alt="Curry Tree" /></li>
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-05.jpg" alt="Curry Tree" /></li>
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-06.jpg" alt="Curry Tree" /></li>
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-07.jpg" alt="Curry Tree" /></li>
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-08.jpg" alt="Curry Tree" /></li>
        <li> <img src="https://www.growcurrytree.com/image/catalog/mobile/story-09.jpg" alt="Curry Tree" /></li>
      </ul>
      
    </div>
  </article>
  
<script src="catalog/view/javascript/jquery.instagramFeed.min.js" type="text/javascript"></script>   
<script>
    (function($){
        $(window).on('load', function(){
            $.instagramFeed({
                'username': 'true.elements',
                'container': ".storyUL",
                'display_profile': false,
                'display_biography': false,
                'display_gallery': true,
                'callback': null,
                'styling': true,
                'items': 9,
                'items_per_row': 4,
                'margin': 1,
                'lazy_load': true 
            });
        });
    })(jQuery);
</script>
<!--<style>
    .fancybox1{
                display: block;
                width: 100%;
                padding-top: 100%;
                background-size: cover;
                background-repeat: no-repeat;
                background-position: 50% 50%;
    }
</style>
<div class= "container">
<div id="instagram_block_home" class="instagram-container" >
    
     	<div class="module-title block-title">
     	    <br>
	        <center> <h2 style="font-family: rocketclouds;"># TrueElements</h2><br>
	       </center>
	        
		</div>
		<?php
        $count = 0;
       $rows = $config_slide['f_rows'];
        if(!$rows) { $rows=1; }
    ?>
    <div class="content_block" style="padding-left: 10%;padding-bottom: 5%;padding-right: 10%;">
        <?php foreach($instagrams as $instagram) : ?>
            <?php if($count % $rows == 0 ) : ?>
                <div class="row_items">
            <?php endif; ?>
            <?php $count++; ?>
                    
                    <a class="fancybox1"  href="<?php echo $instagram['link']; ?>" target="_blank" style="background-image: url('<?php echo $instagram['image'] ?>');"></a>
                    
            <?php if($count % $rows == 0 ): ?>
                </div>
            <?php else: ?>
                <?php if($count == count($instagrams)): ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
<?php if($config_slide['f_view_mode'] == 'slider') : ?>
<script type="text/javascript">
    $("#instagram_block_home .content_block").owlCarousel({
        autoPlay: <?php if($config_slide['autoplay']) { echo 'true' ;} else { echo 'false'; } ?>,
        navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
        items : <?php echo $config_slide['items']; ?>,
        slideSpeed : <?php if($config_slide['f_speed']) { echo $config_slide['f_speed']; } else { echo 3000;} ?>,
        navigation : <?php if($config_slide['f_show_nextback']) { echo 'true' ;} else { echo 'false'; } ?>,
        pagination : <?php if($config_slide['f_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
        stopOnHover : true,
        responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            nav:true,
            loop:false
        }
    }
    });
</script>
<?php endif; ?>
<script type="text/javascript">
    $('.content_block').({
        type: 'image',
        delegate: 'a',
        gallery: {
            enabled : true
        }
    });
</script>
-->