<?php if($deals_data) { ?>
    <script type="text/javascript">
	    var listdeal1 = [];
	</script>
	<section class="container" style="margin-top: 25px;">
	    <div class="new-title">
            <h2><p class="te">Hot Deals</p></h2>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4"></div>
            <div class="title-line col-sm-4 col-md-4" style="margin-top: -10px;margin-bottom: 20px;">
                <div class="tl-1" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
                <div class="tl-2" style="display: inline-block;height: 13px;margin: 0 5px;position: relative;top: 5px;width: 13px;border: 1px solid #ebc131;border-radius: 50px;"></div>
                <div class="tl-3" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
            </div>
            <div class="col-sm-4 col-md-4"></div>
        </div>
        <div class="row deals-row" style="margin-bottom:20px;">
	        <div class="col-md-4 hidden-xs hidden-sm">
	            <a href="/index.php?route=checkout/cart&product_id=6453&option_id=1&option_value_id=338&coupon=OATMEAL40"><img src="/image/catalog/Banner/deals-banner-1.jpg" alt="True Elements" id="deal-image" /></a>
	        </div>
            <div class="col-md-8 col-xs-12 col-sm-12 deal-product-container">
                <div id="deals-carousel" class="deal-product-carousel owl-carousel owl-loaded owl-drag">
                    <?php foreach($deals_data AS $deals) { ?>
                        <div class="item">
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <a href="<?php echo $deals['href']; ?>"><img alt="True Elements" class="img-responsive" src="<?php echo $deals['thumb']; ?>" style="padding: 14%;" /></a>
                            </div>
                            <div class="col-md-7 col-xs-12 col-sm-12">
                                <h3 class="deal-product-name"><a href="<?php echo $deals['href']; ?>"><?php echo $deals['name']; ?></a></h3>
                                <div class="item-inner" style="text-align: left;">
                                    <small class="old-price"><span class="price"><?php echo $deals['price']; ?></span></small>
                                    <span class="new-price"><span class="price">₹<?php echo $deals['deal_price']; ?></span></span>
                                    <span class="deal-discount">
		    		                    <?php echo $deals['discount']; ?>
		    		                </span>
                                </div>
                                <div class="item-time font-ct">
		    						<div class="item-timer product_time_<?php echo $deals['product_id']; ?>"></div>
		    						<script type="text/javascript">
	    								listdeal1.push('product_time_<?php echo $deals["product_id"]; ?>|<?php echo $deals["date_end"]; ?>');
		    						</script>
		    					</div>
		    					<div class="buttons hidden-xs hidden-sm">
		    					    <a class="btn button" href="index.php?route=product/deals" style="margin-top: 20px;">More Deals</a>
		    					</div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <hr/>
                <div id="deals-thumb-carousel" class="deal-product-carousel owl-carousel owl-loaded owl-drag">
                    <?php foreach($deals_data AS $deals) { ?>
                        <div class="item">
                            <img alt="True Elements" class="img-responsive" src="<?php echo $deals['thumb']; ?>" />
                        </div>
                    <?php } ?>
                </div>
                <div class="buttons hidden-md hidden-xl hidden-lg">
                    <a class="btn button" href="index.php?route=product/deals" style="margin-top: 20px;">More Deals</a>
                </div>
            </div>
        </div>
    </section>
<?php } ?>



<script type="text/javascript">
    $(document).ready(function() {
    var sync1 = $("#deals-carousel");
    var sync2 = $("#deals-thumb-carousel");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: true,
    pagination:false,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 8,
    itemsDesktop      : [1199,10],
    itemsDesktopSmall     : [979,10],
    itemsTablet       : [768,8],
    itemsMobile       : [479,4],
    pagination: false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#deals-thumb-carousel")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#deals-thumb-carousel").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#deals-thumb-carousel").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
    function CountDown(date, id) {
		dateNow = new Date();
		amount = date.getTime() - dateNow.getTime();
		if (amount < 0 && $('#' + id).length) {
			$('.' + id).html("Now!");
		} else {
			days = 0;
			hours = 0;
			mins = 0;
			secs = 0;
			out = "";
			amount = Math.floor(amount / 1000);
			days = Math.floor(amount / 86400);
			amount = amount % 86400;
			hours = Math.floor(amount / 3600);
			amount = amount % 3600;
			mins = Math.floor(amount / 60);
			amount = amount % 60;
			secs = Math.floor(amount);

			out += "<div class='time-item time-day'>" + "<div class='num-time'>" + days + "</div>" + " <div class='name-time'>" + ((days == 1) ? "Day" : "Days") + "</div>" + "</div> ";
			out += "<div class='time-item time-hour'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "Hour" : "Hours") + "</div>" + "</div> ";
			out += "<div class='time-item time-min' >" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "Min" : "Mins") + "</div>" + "</div> ";
			out += "<div class='time-item time-sec' >" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "Sec" : "Secs") + "</div>" + "</div> ";
			out = out.substr(0, out.length - 2);
			

			$('.' + id).html(out);

			setTimeout(function () {
				CountDown(date, id);
			}, 1000);
		}
	}

	if (listdeal1.length > 0) {
		for (var i = 0; i < listdeal1.length; i++) {
			var arr = listdeal1[i].split("|");
			if (arr[1].length) {
				var data = new Date(arr[1]);
				CountDown(data, arr[0]);
			}
		}
	}
	})('#deals-carousel');
	

</script>
