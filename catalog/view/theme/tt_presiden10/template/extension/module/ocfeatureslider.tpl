<style>
    b, i, sup, sub, u, del {
    color: #000;
}
.what-we-make-container {
    margin-top: 20px;
}

/*.sec1, .sec2, .sec31, .sec32, .sec33, .sec34, .sec4, .sec5 {
    display: none;
}*/
.caption {
    min-height: 30px;
}
.media-image-container{
    margin: 10px 13px 0 13px;
    border-radius: 10px;
    margin-bottom: 20px;
}
.media-image-container img {
    border-radius: 10px;
    -webkit-transition: all 0.6s ease;
    transition: all 0.6s ease;
}
.media-image-container img:hover {
    -webkit-transform: scale(1.1);
    -moz-transform: scale(1.1);
    -ms-transform: scale(1.1);
    -o-transform: scale(1.1);
    transform: scale(1.1);
}
.media-grid {
    padding-left: 15px;
    padding-right: 15px;
}
.read-more {
    text-align: left;
    color: #000;
    border-bottom: 2px solid #000;
    padding-bottom: 3px;
    margin-left: 10px;
}
.read-more:hover {
    color: #f49a25;
    border-bottom: 2px solid #f49a25;
}
.read-more-button {
    margin-bottom: 10px;
}
.pl10 {
    padding-left: 10px!important;
}
.pr10 {
    padding-right: 10px!important;
}
.caption {
    min-height: 30px;
}
.media-name, {
    min-height: 45px;
    padding-left: 15px;
    padding-right: 15px;
}


#deals-thumb-carousel .item {
    border: 1px solid #ccc;
    border-radius: 5px;
    padding: 5px;
    margin-left: 5px;
    margin-right: 5px;
    cursor: pointer;
}

#deals-thumb-carousel .synced .item {
    border: 1px solid #f49a25;
}

#deals-thumb-carousel {
    margin-top: 20px;
}
#deals-carousel .item-timer .time-item .num-time {
    background: #ccc;
    border-radius: 5px;
    padding: 30px 0 30px 0;
    width: 75px;
    font-size: 40px;
    font-family: gothamBold;
}
#deals-carousel .item-timer .time-item {
    display: inline-block;
    font-weight: bold;
    padding: 7px;
    text-align: center;
}
#deals-carousel .item-timer {
    margin-top: 30px;
 }
.deal-product-name {
    text-align: left;
    font-size: 20px;
}
.media-name {
    min-height: 28px;
    padding-left: 15px;
    padding-right: 15px;
}

.deal-product-name a, .recipe-name a,.media-name a, .blog-name a, .sub-blog-name a  {
    color: #000;
}
.deal-product-name a:hover, .recipe-name a:hover, .media-name a:hover, .sub-recipe-name a:hover, .blog-name a:hover, .sub-blog-name a:hover {
    color: #f49a25!important;
}
.deal-product-container .buttons {
    text-align: center;
}

.recipe-name, .media-name,.blog-name {
    text-align: left;
    font-size: 18px;
    margin-bottom: 10px !important;
}
.sub-recipe-name {
    font-size: 15px!important;
}
.sub-blog-name {
    font-size: 16px;
    margin-top: 5px;
}
.deal-discount {
    border: 1px solid #f49a25;
    padding: 7px;
    border-radius: 5px;
    font-size: 13px;
    padding: 7px;
    margin-left: 20px;
}
.deals-row {
    margin-top: 10px;
}
.more-blogs {
    margin-top: 15px;
    padding-left: 30px!important;
    padding-right: 30px!important;
}
.more-blogs:focus {
    color: #fff;
}
@media(max-width: 1000px) {
    .mp-0 {
        padding-left: 0!important;
        padding-right: 0!important;
    }
    .what-we-make-container {
        margin-top: 15px!important;
    }
    .top-image {
        padding-left: 5px;
        padding-right: 5px;
    }
    .deal-product-name, .recipe-name,.media-name, .blog-name {
        font-size: 15px;
        text-align: center;
    }
    #deals-carousel .item-inner {
        text-align: center!important;
    }
    #deals-carousel .item-timer .time-item .num-time {
        padding: 15px 0 15px 0;
        width: 50px;
        font-size: 30px;
    }
    .deal-product-container {
         padding-right: 0px!important;
         padding-left: 0px!important;
    }
    #deals-carousel .item-time {
        text-align: center;
    }
    .deals-row {
        margin-top: 0!important;
    }
    .more-blogs {
        padding: 7px 16px!important;
    }
}
</style>
<style>
    .recipe, .blog {
        padding: 10px;
    }
    /*@media(max-width: 767px) {
        .recipe-image img {
            border-radius: 10px;
        }
    }
    
    @media(min-width: 768px) {
        .recipe-image img {
            border-radius: 20px;
        }
    }*/
    .recipe-name h4,.media-name h4, .blog-name h4 {
        line-height: 20px;
    }
    
    .te {
        font-family: gothamBold!important;
    }
    
    .recipe-image img, .blog-image img {
        border-radius: 5px;
    }
    .view-all-recipes, .view-all-blogs {
        width: 100%;
        text-align: center;
        color: #000;
        font-weight: 600;
    }
    .view-all-recipes:after, .view-all-blogs:after {
        content: '';
        width: 100%;
        height: 100%;
        position: absolute;
        background: #fff;
        left: 0;
        top: 0;
    }
    .view-all-recipes .text, .view-all-blogs .text {
        position: absolute;
        left: 0px;
        right: 0px;
        bottom: 0px;
        top: 45%;
        margin: auto;
        z-index: 2;
        font-size: 15px;
        color: #000;
    }
    .view-all-recipes:hover .fa, .view-all-recipes:hover .text, .view-all-blogs:hover .fa, .view-all-blogs:hover .text {
        color: #f49a25!important;
    }

    @media (max-width: 850px) and (min-width: 700px){
        .media-grid .desc-container .desc{
            min-height: 100px !important;
        }
    }
</style>
    <section class="container">
        <div class="new-title">
	        <h2 class="te">Delicious True Food Recipes</h2>
	    </div>
        <div class="row">
            <div class="col-sm-3 col-md-4"></div>
            <div class="title-line col-sm-6 col-md-4" style="margin-top: -10px;margin-bottom: 20px;">
                <div class="tl-1" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
                <div class="tl-2" style="display: inline-block;height: 13px;margin: 0 5px;position: relative;top: 5px;width: 13px;border: 1px solid #ebc131;border-radius: 50px;"></div>
                <div class="tl-3" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
            </div>
            <div class="col-sm-3 col-md-4"></div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12 col-xs-12 col-sm-12 recipe-container">
                <?php foreach($recipes AS $recipe) { ?>
                    <div class="recipe">
                        <div class="recipe-image">
                            <a href="<?php echo $recipe['href']; ?>"><img src="<?php echo $recipe['image']; ?>" alt="<?php echo $recipe['title']; ?>"></a>
                        </div>
                        <div class="recipe-name">
                            <a href="<?php echo $recipe['href']; ?>"><h4><?php echo $recipe['title']; ?></h4></a>
                        </div>
                    </div>
                <?php } ?>
                <div class="recipe view-all-recipes">
                    <a href="/recipes"><img src="<?php echo $view_more_img; ?>" alt="Give me more Ideas"><span class="text">Give me more Ideas <i class="fa fa-arrow-right" style="color: #000;"></i></span></a>
                </div>
            </div>
            <div class="col-lg-12" style="display: inline-block;text-align: center;margin: auto;">
                <?php if($recipe_categories) { ?>
                <ul class="tabs-categorys" style="text-align: center;">
                <?php foreach($recipe_categories AS $category) { ?> 
                <li rel="tab_cate589" class="active" style="text-align: center;">
                  <?php echo $category['name']; ?>                          
                </li>
                 <?php } ?>
                </ul>
              <?php } ?>
           </div>
           </div>
          </section>
    
    <section class="container">
        <div class="new-title">
	        <h2 class="te">True in News</h2>
	    </div>
        <div class="row">
            <div class="col-sm-3 col-md-4"></div>
            <div class="title-line col-sm-6 col-md-4" style="margin-top: -10px;margin-bottom: 20px;">
                <div class="tl-1" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
                <div class="tl-2" style="display: inline-block;height: 13px;margin: 0 5px;position: relative;top: 5px;width: 13px;border: 1px solid #ebc131;border-radius: 50px;"></div>
                <div class="tl-3" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
            </div>
            <div class="col-sm-3 col-md-4"></div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12 col-xs-12 col-sm-12 recipe-carousel">
                <?php foreach($media_article AS $media) { ?>
                    <div class="media-grid">
                        <div class="media media-container">
                            <div class="media-image-container">
                                <a href="<?php echo $media['href']; ?>" target="_blank"><img src="<?php echo $media['image']; ?>" title="<?php echo $media['media_name']; ?>" alt="<?php echo $media['media_name']; ?>" /></a>
                            </div>
                            <div class="desc-container">
                                
                                <p class="desc" style="min-height: 75px;color:#000;"><?php echo $media['content']; ?></p>
                                 
				                <div class="caption">
				                    <div class="col-md-5 col-xs-4 col-sm-6 pl10"><a href="<?php echo $media['href'];?>" class="read-more" target="_blank">Read more</a></div>
				                   <!--  <div class="col-md-2 col-xs-2 col-sm-2 "></div> -->
				                    <div class="col-md-5 col-xs-6 col-sm-6 pr10" style="text-align: right;"><i style="color: #000;" class="fa fa-calendar"></i> <?php echo $media['publish_date'] ? date('d M', strtotime($media['publish_date'])) : 'Apr 14'; ?></div>
			            	    </div>
                            </div> 
                        </div>
                    </div>
                <?php } ?>
                <div class="recipe view-all-recipes">
                    <a href="index.php?route=information/media" target="_blank" ><img src="<?php echo $view_more_img; ?>" alt="Give me more Ideas"><span class="text" style="top: 76%;">Show More <i class="fa fa-arrow-right" style="color: #000;"></i></span></a>
                </div> 
            </div>
        </div>
    </section>
    
    <section class="container">
        <div class="new-title">
	        <h2 class="te">Few True Words</h2>
	    </div>
        <div class="row">
            <div class="col-sm-3 col-md-4"></div>
            <div class="title-line col-sm-6 col-md-4" style="margin-top: -10px;margin-bottom: 20px;">
                <div class="tl-1" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
                <div class="tl-2" style="display: inline-block;height: 13px;margin: 0 5px;position: relative;top: 5px;width: 13px;border: 1px solid #ebc131;border-radius: 50px;"></div>
                <div class="tl-3" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
            </div>
            <div class="col-sm-3 col-md-4"></div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12 col-xs-12 col-sm-12 blog-container">
                <?php foreach($blogPosts AS $post) { ?>
                    <div class="blog">
                        <div class="blog-image">
                            <a href="<?php echo $post['href']; ?>"><img src="<?php echo $post['image']; ?>" alt="<?php echo $post['title']; ?>"></a>
                        </div>
                        <div class="blog-name">
                            <a href="<?php echo $post['href']; ?>"><h4><?php echo $post['title']; ?></h4></a>
                        </div>
                    </div>
                <?php } ?>
                <div class="blog view-all-blogs">
                    <a href="/m-blogs"><img src="<?php echo $view_more_img; ?>" alt="More Blogs"><span class="text">More Blogs <i class="fa fa-arrow-right" style="color: #000;"></i></span></a>
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">
/*$(document).ready(function() {
    $(".special-collection").owlCarousel({
        autoPlay: true,
        items : 4,
        slideSpeed : 1000,
        navigation : true,
        paginationNumbers : false,
        pagination : true,
        stopOnHover : false,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [991,1],
		itemsTablet: [700,1],
		itemsMobile : [480,2]
    });
});*/
$(".health-slider").owlCarousel({
    autoPlay: true,
    items : 4,
    slideSpeed : 1000,
    navigation : true,
    paginationNumbers : true,
    pagination : false,
    stopOnHover : false,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [991,3],
    itemsTablet: [700,1],
    itemsMobile : [480,1]
});
</script>


<script type="text/javascript">
$(".recipe-container,.recipe-carousel, .blog-container").owlCarousel({
	    autoPlay: true,
	    items : 4,
	    slideSpeed : 1000,
	    autoplayTimeout: 8000,
	    navigation : true,
	    paginationNumbers : true,
	    pagination : false,
	    stopOnHover : false,
	    itemsDesktop : [1199,3],
	    itemsDesktopSmall : [991,3],
	    itemsTablet: [700,1],
	    itemsMobile : [480,1]
});
var owl = $('#review_part_cotent');
owl.owlCarousel({
      items: 1,
      slideSpeed : 1000,
      loop: true,
      dots: false,
      autoPlay: true,
      //rewindNav: false,
      margin: 40,
      autoplayHoverPause: true,
      autoplayTimeout: 5000,
      nav: true,
      //navText: ['<span class="flaticon-left-arrow"></span>','<span class="flaticon-arrow-pointing-to-right"></span>'],
      responsive: {
        0: {
          nav: false,
          items: 1
        },
        575: {
          nav: false,
          items: 1
        },
        991: {
          nav: true,
          items: 1
        },
        1200: {
          nav: true,
          items: 1
        },
      }
    });
    
    $(".tc-slider").owlCarousel({
	    autoPlay: true,
	    items : 1,
	    slideSpeed : 1000,
	    navigation : true,
	    paginationNumbers : true,
	    pagination : false,
	    stopOnHover : false,
	    itemsDesktop : [1199,1],
	    itemsDesktopSmall : [991,1],
	    itemsTablet: [700,1],
	    itemsMobile : [480,1]
    });
</script>	