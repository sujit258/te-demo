<?php $tab_effect = 'wiggle'; ?>
<script type="text/javascript">
$(document).ready(function() {
	$(".<?php echo $category_alias;?> .tab_content_category").hide();
	$(".<?php echo $category_alias;?> .tab_content_category:first").show(); 
    $(".<?php echo $category_alias;?> ul.tabs-categorys li:first").addClass("active");
	$(".<?php echo $category_alias;?> ul.tabs-categorys li").click(function() {
		$(".<?php echo $category_alias;?> ul.tabs-categorys li").removeClass("active");
		$(".<?php echo $category_alias;?> ul.tabs-categorys .owl-item").removeClass("active");
		$(this).addClass("active");
		$(this).parent().addClass("active");
		$(".<?php echo $category_alias;?> .tab_content_category").hide();
		$(".<?php echo $category_alias;?> .tab_content_category").removeClass("animate1 <?php echo $tab_effect;?>");
		var activeTab = $(this).attr("rel"); 
		$("#"+activeTab) .addClass("animate1 <?php echo $tab_effect;?>");
		$("#"+activeTab).show(); 
	});
});
</script>
<?php
	$row = $config_slide['f_rows'];
	if(!$row) {$row=1;}
?>
<style>
    .item-inner .des-container {
        padding: 0px 0 0 21px;
    }

    .options {
        min-height: 55px;
        display: inline-block;
        width: 100%;
    }
    .product-name {
        min-height: 50px!important;
    }
    .product-options {
        margin-top: 10px;
        text-align: center;
        float: left;
        background: #fff!important;
    }
    .button-cart {
        float: right!important;
        margin-right: 21px!important;
        font-size: 13px!important;
        padding: 8px 10px!important;
    }
    .button-personalize {
        font-size: 13px!important;
        padding: 8px 10px!important;
        float: right !important;
        margin-right: 21px !important;
    }
    .price-label {
        padding: 8px 0px !important;
        min-height: 37px!important;
        width: 100% !important;
    }
    .quantity {
        height: 35px!important;
        width: 60%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2 !important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px !important;
        border-bottom-left-radius: 4px !important;
    }
    
    .plus-button {
        border-top-right-radius: 4px !important;
        border-bottom-right-radius: 4px !important;
    }
    .image1, .image2 {
        padding: 14%;
    }
    
    @media(max-width: 1000px) {
        .button-cart, .button-personalize, .oos {
            padding: 7px 10px !important;
            font-size: 14px!important;
            margin-bottom: 15px!important;
        }
        .des-container .options .radio {
            margin: 0px 0px 5px -2px!important;
        }
        .image1, .image2 {
            padding: 5%!important;
        }
    }
    
    @media(min-width: 1000px) {
        .des-container {
            height: 200px;
        }
    }
    .oos {
        background: #777!important;
        border-radius: 4px!important;
        float: right!important;
        margin-right: 21px!important;
        font-size: 13px!important;
        padding: 8px 10px!important;
    }
    .oos:hover {
        cursor: not-allowed!important;
    }
    .button-cart:focus, .button-personalize:focus {
        color: #fff!important;
    }
    .tabs-categorys {
        margin-top: 10px!important;
    }
    .des-container .options .radio input {
        display: none !important;
    }
    .des-container .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .des-container .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    .des-container .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .price, .coupon {
        padding-left: 0px!important;
        padding-right: 0px!important;
    }
    .coupon {
        min-height: 20px!important;
        text-align: left;
        font-size: 12px!important;
        margin-bottom: 0!important;
        color: #000!important;
    }
    .breakfast-carousel img, .snacks-carousel img {
        padding-left: 14%!important;
        padding-right: 14%!important;
    }
    
    .deals-discount {
        border-radius: 50%;
        right: 0px;
        top: 5%;
        position: absolute;
        background: #f49a25;
        text-align: center;
        z-index: 5;
        width: 50px;
    }
    
    .deals-discount h5 {
        color: #fff;
        font-weight: 600;
        padding: 1px;
    }
    
    .price-coupon{
        font-weight: bold;
    }
    .images-container {
        padding: 10px;
    }
    .img-cat {
        margin-left: auto;
        margin-right: auto;
        display: table;
        -webkit-transition: all 0.6s ease;
        transition: all 0.6s ease;
    }
    .p0 {
        padding: 0;
    }
</style>
<div class="product-tabs-category-container-slider tabcategory-container <?php echo $category_alias;?> quickview-product">
	<div class="container">
		<!--div class="tab-title"-->
		<div class="">
			<div class="new-title">
        <h2 class="te"><?php echo $title; ?></h2>
    </div>
	<div class="col-sm-3 col-md-4"></div>
    <div class="title-line col-sm-6 col-md-4" style="margin-top: -10px;margin-bottom: 20px;">
          <div class="tl-1" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
          <div class="tl-2" style="display: inline-block;height: 13px;margin: 0 5px;position: relative;top: 5px;width: 13px;border: 1px solid #ebc131;border-radius: 50px;"></div>
          <div class="tl-3" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
    </div>
    <div class="col-sm-3 col-md-4"></div>
		</div>
		
		<?php if($category_alias == "Breakfast") { ?>
        <div class="col-md-12 col-xs-12 col-sm-12 p0">
            <div class="category-carousel">
                <div class="images-container hidden-md hidden-xl hidden-lg col-xs-6 col-sm-4">
                    <a href="https://www.true-elements.com/our-products#oats">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/oats-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <div class="images-container hidden-md hidden-xl hidden-lg col-xs-6 col-sm-4">
                    <a href="https://www.true-elements.com/our-products#oatmeal">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/oatmeal-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <div class="images-container hidden-xs hidden-sm col-md-3">
                    <a href="https://www.true-elements.com/our-products#oats">
                        <img class="img-responsive img-cat" src="https://www.true-elements.com/image/catalog/cat-slider/oats-and-oatmeal-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <div class="images-container col-md-3 col-xs-6 col-sm-4">
                    <a href="https://www.true-elements.com/our-products#muesli">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/wholegrain-muesli-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <div class="images-container col-md-3 col-xs-6 col-sm-4">
                    <a href="https://www.true-elements.com/our-products#granola">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/granola-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <div class="images-container col-md-3 col-xs-6 col-sm-4">
                    <a href="https://www.true-elements.com/our-products#flakesBix">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/flakes-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <div class="images-container hidden-md hidden-lg hidden-xl col-xs-6 col-sm-4">
                    <a href="https://www.true-elements.com/our-products#supergrains">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/super-grains-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
            </div>
        </div>
    <?php } else if($category_alias == "Snacks") { ?>
        <div class="col-md-12 col-xs-12 col-sm-12 p0">
            <div class="category-carousel">
                <div class="images-container col-lg-3 col-md-3 col-xs-6 col-sm-3">
                    <a href="https://www.true-elements.com/our-products#rawSeeds">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/healthy-seeds-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <div class="images-container col-lg-3 col-md-3 col-xs-6 col-sm-3">
                    <a href="https://www.true-elements.com/our-products#seedsMixes">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/seeds-mixes-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <div class="images-container col-lg-3 col-md-3 col-xs-6 col-sm-3">
                    <a href="https://www.true-elements.com/our-products#onTheGoSnacks">
                        <img class="img-responsive img-cat" src="image/catalog/cat-slider/otg-snacks-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
                <!--div class="images-container col-md-3 col-xs-6 col-sm-4">
                    <a href="https://www.true-elements.com/our-products#roastedSeeds">
                    <img class="img-responsive img-cat" src="image/catalog/cat-slider/roasted-seeds-category-banner.jpg" alt="True Elements" />
                    </a>
                </div-->
                <div class="images-container col-lg-3 col-md-3 col-xs-6 col-sm-3">
                    <a href="https://www.true-elements.com/our-products#others">
                    <img class="img-responsive img-cat" src="image/catalog/cat-slider/berries-category-banner.jpg" alt="True Elements" />
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
	</div>
</div><!-- <?php echo $category_alias;?> -->
<script type="text/javascript">
/*$(document).ready(function() {
    $(".category-carousel").owlCarousel({
        autoPlay: true,
        items : 4,
        slideSpeed : 1000,
        navigation : true,
        paginationNumbers : false,
        pagination : true,
        stopOnHover : false,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [991,1],
		itemsTablet: [700,1],
		itemsMobile : [480,1]
    });
});*/
</script>
<script type="text/javascript">
$(document).ready(function() {
  $(".<?php echo $category_alias;?> .owl-demo-tabcate").owlCarousel({
	autoPlay: <?php if($config_slide['tab_cate_autoplay']) { echo 'true' ;} else { echo 'false';} ?>,
	items : <?php if($config_slide['items']) { echo $config_slide['items'] ;} else { echo 3;} ?>,
	slideSpeed : <?php if($config_slide['tab_cate_speed_slide']) { echo $config_slide['tab_cate_speed_slide'] ;} else { echo 200;} ?>,
	navigation : <?php if($config_slide['tab_cate_show_nextback']) { echo 'true' ;} else { echo 'false';} ?>,
	paginationNumbers : true,
	pagination : <?php if($config_slide['tab_cate_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
	stopOnHover : false,
	itemsDesktop : [1199,3],
	itemsDesktopSmall : [991,3],
	itemsTablet: [700,2],
	itemsMobile : [480,2],
	addClassActive: true,
	afterAction: function(el){
	   this.$owlItems.removeClass('before-active')
	   this.$owlItems.removeClass('last-active')
	   this.$owlItems .eq(this.currentItem).addClass('before-active')  
	   this.$owlItems .eq(this.currentItem + (this.owl.visibleItems.length - 1)).addClass('last-active') 
	}
  });
});
</script>
<script type="text/javascript">
    function addtocart(product_id, id) {
        $('#'+id+' input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#'+id+' input[name=\'product_id\'], #'+id+' .quantity-' + product_id + ', #'+id+' .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id, id) {
        var currentval = parseInt($("#"+id+" .quantity-" + product_id).val());
        $("#"+id+" .quantity-" + product_id).val(currentval-1);
        if($("#"+id+" .quantity-" + product_id).val() <= 0) {
            $("#"+id+" .quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id, id) {
        var currentval = parseInt($("#"+id+" .quantity-" + product_id).val());
        $("#"+id+" .quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id, id) {
        var currentval = parseInt($("#"+id+" .quantity-" + product_id).val());
        if(currentval <= 0) {
           $("#"+id+" .quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id, id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('#'+id+' .checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('#'+id+' .price-new-' + product_id).html(json.new_price.special);
					$('#'+id+' .price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.coupon) {
					     $('#'+id+' .price-coupon-' + product_id).html(json.new_price.coupon);
					}
					
					if(json.new_price.special) {
					    $('#'+id+' .price-new-' + product_id).html(json.new_price.special);
					    $('#'+id+' .price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('#'+id+' .price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.des-container .options .radio input[type="radio"]').click(function() {
        $('.des-container .options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>