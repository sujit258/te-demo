<?php echo $header; ?>

<!--Google Login Script-->
<script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      auth2 = gapi.auth2.init({
        client_id: '688467726932-ftoln2tngeqru6ahv73sqh2rlg1aeq8k.apps.googleusercontent.com'
    });
      attachSignin(document.getElementById('customBtn1'));
  });
};

function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
            var apply_te_lab = $('input[name ="apply_te_lab"]:checked').val();
            if(apply_te_lab > 0) {
                apply_te_lab = 1;
            } else {
                apply_te_lab = 0;
            }
          var profile = googleUser.getBasicProfile();
          var id = profile.getId(); // Do not send to your backend! Use an ID token instead.
          var name = profile.getName();
          var first_name = profile.getGivenName();
          var last_name = profile.getFamilyName();
          var imageUrl = profile.getImageUrl();
          var email = profile.getEmail(); // This is null if the 'email' scope is not present. 
          var location = "<?php echo $redirect ?>";
          document.getElementById('name').innerHTML = 'Please Wait...!';
          googleUser.disconnect();
          $.ajax({
            url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
            data : 'email_address='+email+'&fname='+first_name+'&lname='+last_name+'&login_via=google&apply_te_lab=' + apply_te_lab,
            type : 'post',
            success: function(result) {
                console.log(result);
                if(result == 'success'){
                    //window.location.reload();
                    window.location = location;
                } else if(result == 'registered'){
                 location.href = "/index.php?route=account/edit/savedetails";
             } else{
                $('.alert.alert-danger').remove();
                $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
            }
            
        }
    });
      }, function(error) {
        $('.alert.alert-danger').remove();
        $('<div class="alert alert-danger">There was a problem with the authorisation or user may have closed the popup.</div>').insertBefore($('div.error'));
    });
}
</script>
<!--Facebook Login Script-->
<script>

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '587428728308321',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v3.0' // use graph api version 3.1
  });

};

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.async=true;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
  
  // Facebook login with JavaScript SDK
  function xfbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {        
            getFbUserData();
        } else {
            autherror();
        }
    }, {scope: 'email'});
}

function testAPI() { 
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
        function(response) {
          if(response.email == 'undefined')
              $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please check your email id associated with your facebook account.</div>').insertBefore($('div.customAlert'));
          else{
            document.getElementById('name').innerHTML ='Please Wait...!';

            $.ajax({
                url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
                data : 'email_address='+response.email+'&fname='+response.first_name+'&lname='+response.last_name+'&login_via=facebook',
                type : 'post',
                success: function(result) {
                    if(result == 'success'){
                        window.location.reload();
                    } else if(result == 'registered'){
                        window.location.reload();
                    } else{
                        $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
                    }
                    
                }
            });
        }
    });
}
</script>

<!--Linkedin Login Script-->
<script type="text/javascript">
  function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
    
}

function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(onSuccess).error(onError);
}

function onSuccess(data){
    var user = data.values[0];
    var first_name = user.firstName;
    var last_name = user.lastName;
    var email = user.emailAddress;
    document.getElementById('name').innerHTML = 'Please Wait...!';
    $.ajax({
        url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
        data : 'email_address='+email+'&fname='+first_name+'&lname='+last_name+'&login_via=linkedin',
        type : 'post',
        success: function(result) {
            if(result == 'success'){
                window.location.reload();
            } else if(result == 'registered'){
                window.location.reload();
            } else{
                $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
            }     
        }
    });

}

function onError(error) {
    console.log(error);
}

</script>

<style>
    .login-button-container {
        margin: 10px auto;
        display: table;
        table-layout: fixed;
        width: 100%;
    }
    .login-button-container .social-container {
        min-width: 133px;
    }
    
    .login-button-container .social-container .social-container-box {
        border: 1px solid #bfc0c6;
        background-color: #fff;
        border-radius: 5px;
        text-align: left;
        padding: 14px 14px 0 0;
        height: 50px;
        cursor: pointer;
        max-width: 150px;
        margin: 0 auto;
    }
    .login-button {
        display: block;
        margin: 0 auto;
        width: 100%;
        max-width: 186px;
        padding-left: 50px;
        position: relative;
        font-weight: 500;
    }
    .login-button-container .login-gplus-logo {
        background-position: -298px 0 !important;
        width: 23px;
        top: -2px;
    }
    .login-button-container .login-fb-logo, .login-button-container .login-gplus-logo, .login-button-container .login-linkedin-logo {
        height: 29px;
        position: absolute;
        left: 15px;
    }
    .login-button-container .header-sprite {
        background: url(https://www.true-elements.com/catalog/view/javascript/xtensions/image/social.png) no-repeat 0 0;
        background-position-x: 0px;
        background-position-y: 0px;
        background-size: auto;
        background-size: 336px 48px;
    }
    .group {
        position: relative;
        margin-bottom: 20px;
    }
    .group .inputMaterial {
        font-size: 13px;
        display: block;
        width: 100%;
        height: 42px;
        box-shadow: none;
        -webkit-box-shadow: none;
        color: #333333;
        padding: 16px 15px 4px;
        border-radius: 4px;
        border: 1px solid  #dddddd;
    }
    
    .group .inputMaterial:focus {
      outline: 0;
      -webkit-box-shadow: none;
      box-shadow: none;
      border-color: #96858d;
    }
  
    .group label {
        color: #cecbe2;
        font-size: 12px;
        font-weight: 600;
        position: absolute;
        pointer-events: none;
        left: 5px;
        top: 12px;
        transition: 0.2s ease all;
        -moz-transition: 0.2s ease all;
        -webkit-transition: 0.2s ease all;
        z-index: 22;
        padding: 0px 10px;
    }
    .group.focus label {
      top: -8px !important;
      background: #ffffff;
    }
    .group.focus label span.required:after {
      display: inline-block;
    }
    .group.focus select.inputMaterial {
      padding-top: 18px;
    }
    .group.focus label {
      color: #96858d;
    }
    *:focus,
    a:focus {
      outline: none;
    }


    .group.focus label {
      top: -8px !important;
      background: #ffffff !important;
    }
    .group.focus label {
      color: #000 !important;
    }


    .footer-separator {
        margin: 32px 0;
        padding: 10px 0px 5px;
        border-top: 1px solid #ddd;
        width: 100%
    }
    
    .field-icon {
      float: right;
      margin-left: -25px;
      margin-top: -25px;
      position: relative;
      z-index: 2;
      color: #000;
    } 

</style>

<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
</ul>
<div class="container">
  <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  
  
  <div class="row customAlert"><?php echo $column_left; ?>
  <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
<?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
<?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
<?php } ?>
<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
<div class="row">
 <br/>
 <div class="col-sm-4">
          <!--div class="well">
            <h2><?php echo $text_new_customer; ?></h2>
            <p><strong><?php echo $text_register; ?></strong></p>
            <p><?php echo $text_register_account; ?></p>
            <a href="<?php echo $register; ?>" class="btn btn-primary"><?php echo "Sign Up"; ?></a></div-->
        </div>
        <div class="col-sm-4">
          <div class="well" style="box-shadow: 0 5px 6px rgba(0,0,0,0.5);">
              <div class="error"> </div>
              <p class="text-center"><strong>LOGIN USING SOCIAL ACCOUNT</strong></p>
              <div class="login-button-container">
                <div class="social-container">
                    <div class="social-container-box" id="customBtn1">
                        <span class="login-google login-button"><span class="header-sprite login-gplus-logo"></span>GOOGLE</a>
                    </div>
                </div>
            </div>
            <p class="login-info-text text-center hide_guest">- OR USING EMAIL -</p>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="group">
                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="inputMaterial" />
                <label for="email">E-Mail</label>
            </div>
            <div class="group ">
                <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="inputMaterial" />
                <span toggle="#input-password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                <label for="password">Password</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="apply_te_lab" value="1" checked> I want to be part of TE Labs program</label>
            </div>
            <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary pull-right" />
            <br />
            <div class="footer-separator">
              <span class="pull-left">
                  <a style="text-decoration: none;color: #000000" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a><br />
              </span>
              <span class="pull-right">
                <a style="text-decoration: none;color: #000000;font-weight:900" href="<?php echo $register; ?>" ><?php echo "Create an Account"; ?></a>
            </span>   
            <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
          <?php } ?>
      </div>
      <!--Google Signin Button-->
      
    <script>startApp();</script>
    <script>
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
            $("#input-password").parents(".group").addClass("focus");
        });
    </script>
    <script>
       (function ($) {
            $(function () {
                $("#loginFB").on("click", function () {
                    FB.login(function(response) {
                        if (response.authResponse) {
                            testAPI();
                        }
                    });
                });
            });
        })(jQuery);
    </script>

    <script type="text/javascript">
       (function ($) {
        $(function () {
            $("#loginLinkedIn").on("click", function () {
                IN.User.authorize(function(){
                    onLinkedInLoad();
                });
            });
        });
    })(jQuery);

        function liAuth(){
            IN.User.authorize(function(){
                    getProfileData();
            });
        }
    </script>

    <div id="name"></div>
        
    </form>
</div>
</div>
<div class="col-sm-4"></div>

</div>
<?php echo $content_bottom; ?></div>
<?php echo $column_right; ?></div>
</div>
<br/><br/>
<script>
    jQuery(document).ready(function($) {

       $(".inputMaterial").focus(function(){
         $(this).parent().addClass("focus");

     }).blur(function(){
         $(this).parent().removeClass("focus");
     })

 }); 
</script>
<?php echo $footer; ?>