<?php echo $header; ?>
<style>
    .breadcrumb {
        margin-bottom: 0!important;
    }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container" style="background-color:#fafbfd; width:auto;">
	<div class="container" style="background-color:#fff;">
		<div class="row account_dashboard new_account_dashboard">
			<aside id="column-left" class="col-sm-3 hidden-xs">

				<div class="admin_wrapper">
					<div class="admin_images_section">
						<div class="images_border">
							<img id="changeprofilepic" src="/image/acoount.jpg" class="img-responsive" alt="True Elements">
						</div>	
						<h3><?php  echo $name; ?></h3>
						<p><?php if (isset($email)) { echo $email; } ?></p>
					</div>
					<div class="clear"></div>
				</div>
				<div class="list-group ad_my_account">
                  <a href="/index.php?route=account/account" class="list-group-item"><i class="fa fa-user"></i> My Account</a>
                  <a href="/index.php?route=account/edit" class="list-group-item"><i class="fa fa-pencil-square-o"></i> Edit Account</a>
                  <a href="/index.php?route=account/password" class="list-group-item"><i class="fa fa-key"></i> Password</a>  
                  <a href="/index.php?route=account/address" class="list-group-item"><i class="fa fa-book"></i> Address Book</a>   
                  <a href="/index.php?route=account/order" class="list-group-item"><i class="fa fa-undo"></i> Order History</a> 
                  <a href="/index.php?route=account/wishlist" class="list-group-item"><i class="fa fa-heart"></i> Wish List</a>
                  <a href="/index.php?route=account/reward" class="list-group-item"><i class="fa fa-shield"></i>True Cash</a>
	            </div>

			</aside>

    <div id="content" class="col-sm-9">
	<div class="row" style="margin:28px 0 0 0">
        <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p style="font-size: 15px;"><?php echo $text_total; ?> <b><?php echo $total; ?></b>.</p>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-left"><?php echo $column_description; ?></td>
              <td class="text-right"><?php echo $column_points; ?></td>
              <td class="text-right">Expiring On</td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($rewards) { ?>
            <?php foreach ($rewards  as $reward) { ?>
            <tr>
              <td class="text-left"><?php if ($reward['order_id']) { ?>
                <a href="<?php echo $reward['href']; ?>"><?php echo $reward['description']; ?></a>
                <?php } else { ?>
                <?php echo $reward['description']; ?>
                <?php } ?></td>
                <td class="text-right"><?php echo $reward['points']; ?><br><span style="color:red;"><?php echo $reward['is_expire']; ?></span></td>
                <?php if ($reward['points'] <=0) { ?>
					<td class="text-right">-</td>
				<?php } else { ?>
					<td class="text-left"><?php echo $reward['date_valid_till']; ?></td>
				<?php } ?> 
                <td class="text-left"><?php echo $reward['date_added']; ?></td>
                
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="text-center" colspan="4"><?php echo $text_empty; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>

			</div>
		</div>
	</div>

</div>

<?php echo $footer; ?>