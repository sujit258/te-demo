<?php echo $header; ?>
<style>
    .breadcrumb {
        margin-bottom: 0!important;
    }
    label[for="input-quantity"] {
        margin-right: 0px!important;
    }
    #input-quantity {
         border-bottom: 2px solid #f49a25!important;
         background: #f5f5f5!important;
     }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container" style="background-color:#fafbfd; width:auto;">
	<div class="container" style="background-color:#fff;">
		<div class="row account_dashboard new_account_dashboard">
			<aside id="column-left" class="col-sm-3 hidden-xs">

				<div class="admin_wrapper">
					<div class="admin_images_section">
						<div class="images_border">
							<img id="changeprofilepic" src="/image/acoount.jpg" class="img-responsive" alt="True Elements">
						</div>	
						<h3><?php  echo $name; ?></h3>
						<p><?php if (isset($email)) { echo $email; } ?></p>
					</div>
					<div class="clear"></div>
				</div>
				<div class="list-group ad_my_account">
                  <a href="/index.php?route=account/account" class="list-group-item"><i class="fa fa-user"></i> My Account</a>
                  <a href="/index.php?route=account/edit" class="list-group-item"><i class="fa fa-pencil-square-o"></i> Edit Account</a>
                  <a href="/index.php?route=account/password" class="list-group-item"><i class="fa fa-key"></i> Password</a>  
                  <a href="/index.php?route=account/address" class="list-group-item"><i class="fa fa-book"></i> Address Book</a>   
                  <a href="/index.php?route=account/order" class="list-group-item"><i class="fa fa-undo"></i> Order History</a> 
                  <a href="/index.php?route=account/wishlist" class="list-group-item"><i class="fa fa-heart"></i> Wish List</a>
                  <a href="/index.php?route=account/reward" class="list-group-item"><i class="fa fa-shield"></i> Reward Points</a>
	            </div>

			</aside>

			<div id="content" class="col-sm-9">
				<div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-9 col-sm-12 col-xs-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_description; ?></p>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_order; ?></legend>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
            <div class="col-sm-9">
              <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
              <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php echo $error_firstname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
            <div class="col-sm-9">
              <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
              <?php if ($error_lastname) { ?>
              <div class="text-danger"><?php echo $error_lastname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-9">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
            <div class="col-sm-9">
              <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-order-id"><?php echo $entry_order_id; ?></label>
            <div class="col-sm-9">
              <input type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
              <?php if ($error_order_id) { ?>
              <div class="text-danger"><?php echo $error_order_id; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="input-date-ordered"><?php echo $entry_date_ordered; ?></label>
            <div class="col-sm-3">
              <div class="input-group date"><input type="text" name="date_ordered" value="<?php echo $date_ordered; ?>" placeholder="<?php echo $entry_date_ordered; ?>" data-date-format="YYYY-MM-DD" id="input-date-ordered" class="form-control" /><span class="input-group-btn">
                <button type="button" class="btn btn-default" style="margin-top: -10px!important; height: 40px;!important"><i class="fa fa-calendar" style="color: #f49a25;"></i></button>
                </span></div>
            </div>
          </div>
        </fieldset>
        <fieldset>
          <legend><?php echo $text_product; ?></legend>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-product"><?php echo $entry_product; ?></label>
            <div class="col-sm-9">
              <input type="text" name="product" value="<?php echo $product; ?>" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
              <?php if ($error_product) { ?>
              <div class="text-danger"><?php echo $error_product; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-model"><?php echo $entry_model; ?></label>
            <div class="col-sm-9">
              <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
              <?php if ($error_model) { ?>
              <div class="text-danger"><?php echo $error_model; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
            <div class="col-sm-9">
              <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label"><?php echo $entry_reason; ?></label>
            <div class="col-sm-9">
              <?php foreach ($return_reasons as $return_reason) { ?>
              <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>
              <div class="radio">
                <label>
                  <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" checked="checked" />
                  <?php echo $return_reason['name']; ?></label>
              </div>
              <?php } else { ?>
              <div class="radio">
                <label>
                  <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" />
                  <?php echo $return_reason['name']; ?></label>
              </div>
              <?php  } ?>
              <?php  } ?>
              <?php if ($error_reason) { ?>
              <div class="text-danger"><?php echo $error_reason; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label"><?php echo $entry_opened; ?></label>
            <div class="col-sm-9">
              <label class="radio-inline">
                <?php if ($opened) { ?>
                <input type="radio" name="opened" value="1" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="opened" value="1" />
                <?php } ?>
                <?php echo $text_yes; ?></label>
              <label class="radio-inline">
                <?php if (!$opened) { ?>
                <input type="radio" name="opened" value="0" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="opened" value="0" />
                <?php } ?>
                <?php echo $text_no; ?></label>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="input-comment"><?php echo $entry_fault_detail; ?></label>
            <div class="col-sm-9">
              <textarea name="comment" rows="10" placeholder="<?php echo $entry_fault_detail; ?>" id="input-comment" class="form-control"><?php echo $comment; ?></textarea>
            </div>
          </div>
          <?php echo $captcha; ?>
        </fieldset>
        <?php if ($text_agree) { ?>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-danger"><?php echo $button_back; ?></a></div>
          <div class="pull-right"><?php echo $text_agree; ?>
            <?php if ($agree) { ?>
            <input type="checkbox" name="agree" value="1" checked="checked" />
            <?php } else { ?>
            <input type="checkbox" name="agree" value="1" />
            <?php } ?>
            <input type="submit" value="<?php echo $button_submit; ?>" class="btn btn-primary" />
          </div>
        </div>
        <?php } else { ?>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_submit; ?>" class="btn btn-primary" />
          </div>
        </div>
        <?php } ?>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
 
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script>

			</div>
		</div>
	</div>

</div>

<?php echo $footer; ?>