<?php echo $header; ?>
<style>
    
.product-image, .product-name, .product-price, .product-buttons {
    min-height: 110px;
}
.footer-breaking {
    margin-bottom: 0;
}
@media(max-width:1000px) {
    .product-image, .product-name{
        min-height: 85px;
    }
    .product-price, .product-buttons {
        min-height: 1px;    
    }
    .order-data .buttons {
        margin-top: 5px !important;
    }
    .qty {
        margin-top: 14px !important;
    }
    .buttons .pull-right {
        margin-top: 10px;
    }
}
.order-header {
    border: 1px solid #dfdfdf;
    min-height: 40px;
    background: #f1f1f1;
}
body {
    font-size: 15px;
}
.order-info {
    margin-top: 0px;
    border: 1px solid #dfdfdf;
    background: #fff;
}

.order-id, .order-date {
    padding-top: 10px;
    margin-bottom: 0;
}
.order-id {
    font-weight: 600;
}
hr {
    border-top: 2px solid #f2f2f2!important;
    margin-top: 0px!important;
    margin-bottom: 0px!important;
}
.product-name h4 {
    line-height: 22px;
    font-weight: 500;
}
.product-name h4 a {
    color: #000;
}
.product-name h4 a:hover, .product-name h4 a:focus {
    color: #f49a25;
}
.btn-primary:active, .btn-primary:focus {
    background:#000!important;
}
.order-data .buttons {
    margin-bottom: 10px!important;
    margin-top: 35px;
}
.order-data p {
    margin-bottom: 10px!important;
    margin-top: 10px;
}
.order-data .row {
    margin-top: 0px!important;
}
.product-price p {
    text-align: center;
    margin-top: 45% !important;
}
.product-quantity .qty {
    margin-top: 45%;
}
.page-title {
    margin-left: -15px!important;
}
.pagination {
    background: #fafbfd;
    margin-left: -15px;
}
.buttons .pull-right {
    margin-right: -15px;
    margin-bottom: 10px;
}
.breadcrumb {
    margin-bottom: 0!important;
}
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container" style="background-color:#fafbfd; width:auto;">
	<div class="container" style="background-color:#fff;">
		<div class="row account_dashboard new_account_dashboard">
			<aside id="column-left" class="col-sm-3 hidden-xs">

				<div class="admin_wrapper">
					<div class="admin_images_section">
						<div class="images_border">
							<img id="changeprofilepic" src="/image/acoount.jpg" class="img-responsive" alt="True Elements">
						</div>	
						<h3><?php  echo $name; ?></h3>
						<p><?php if (isset($email)) { echo $email; } ?></p>
					</div>
					<div class="clear"></div>
				</div>
				<div class="list-group ad_my_account">
                  <a href="/index.php?route=account/account" class="list-group-item"><i class="fa fa-user"></i> My Account</a>
                  <a href="/index.php?route=account/edit" class="list-group-item"><i class="fa fa-pencil-square-o"></i> Edit Account</a>
                  <a href="/index.php?route=account/password" class="list-group-item"><i class="fa fa-key"></i> Password</a>  
                  <a href="/index.php?route=account/address" class="list-group-item"><i class="fa fa-book"></i> Address Book</a>   
                  <a href="/index.php?route=account/order" class="list-group-item"><i class="fa fa-undo"></i> Order History</a> 
                  <a href="/index.php?route=account/wishlist" class="list-group-item"><i class="fa fa-heart"></i> Wish List</a>
                  <a href="/index.php?route=account/reward" class="list-group-item"><i class="fa fa-shield"></i> True Cash</a>
	            </div>

			</aside>

			<div id="content" class="col-sm-9">
				<div class="row" style="margin-top: 36px;">
    
    <?php if($cancelorder_status == 1) { //check module is enabled or disabled ?>
	  <?php if ($success) { ?>
	  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	  <?php } ?>
	  <?php if ($error_warning) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	  <?php } ?>
					<?php } ?>
					
  <div class="row" style="margin-left: 2px;margin-right: 2px;"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div class="col-md-2 hidden-xs hidden-sm"></div>
    <div id="content" class="col-md-8 col-sm-12 col-xs-12"><?php echo $content_top; ?>
      <h1 class="page-title"><?php echo $heading_title; ?></h1>
      <?php if ($orders) { ?>
        <?php foreach ($orders as $order) { ?>
            <div class="row order-header">
                <div class="col-md-6 col-xs-4 col-sm-4">
                    <p class="pull-left order-id"><a href="<?php echo $order['view']; ?>">#<?php echo $order['order_id']; ?></a></p>
                    
                    <form action="<?php echo $track; ?>" method="post" target="_blank" style="margin-top: 9px;margin-left: 41%;">
                     	   <input type="hidden" name="order_id"  value="<?php echo $order['order_id']; ?>" />  
                     	   
                     	   <a onclick="$('#submit').trigger('click')" target="new" type="submit" style="cursor: pointer;"><i style="color:#000;font-size: 22px !important;" class="fa fa-truck"></i></a>
				     <input style="display:none;" type="submit" id="submit"/>
				      
			</form>
			
                </div>
                <div class="col-md-6 col-xs-8 col-sm-8">
                    <p class="pull-right order-date"><b>Ordered On - </b><?php echo $order['date_added']; ?></p>
                </div>
            </div>
            <div class="row order-info">
                <div class="col-md-12 col-xs-12 col-sm-12 order-data">
                    <?php foreach ($order['products'] AS $product) { ?>
                      <div class="row">
                        <div class="col-md-2 col-xs-4 col-sm-4 product-image">
                            <img src="<?php echo $product['image']; ?>" class="img-responsive" alt="True Elements">
                        </div>
                        <div class="col-md-4 col-xs-8 col-sm-8 product-name">
                            <h4><a href="<?php echo $product['product_link']; ?>" target="_blank"><?php echo $product['name']; ?></a></h4>
                            <?php if($product['option']) { ?>
                                <?php foreach ($product['option'] as $option) { ?>
                                <small>- <?php echo $option['name']," ", $option['value']; ?></small><br/>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-2 col-xs-6 col-sm-6 product-quantity">
                            <p class="qty"><b>Quantity: </b><?php echo round($product['quantity'],0); ?></p>
                        </div>
                        <div class="col-md-2 hidden-xs product-price">
                            <p><?php echo $product['price']; ?></p>
                        </div>
                        <div class="col-md-2 col-xs-6 col-sm-6 product-buttons">
                            <div class="pull-right buttons">
                                <?php if ($product['reorder']) { ?>
                                    <a href="<?php echo $product['reorder']; ?>" title="<?php echo $button_reorder; ?>" class="btn btn-primary">Reorder</a>
                                <?php } ?>
                            </div>
                        </div>
                      </div>
                        <hr/>
                    <?php } ?>
                    <p class="pull-right"><b>Order Total: </b><?php echo $order['total']; ?></p>
                </div>
            </div>
            <br/>
        <?php } ?>
        <div class="row">
            <div class="col-md-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-md-6 text-right"><?php echo $results; ?></div>
        </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
      <div class="col-md-2 hidden-xs hidden-sm"></div>
    <?php echo $column_right; ?></div>
</div>

			</div>
		</div>
	</div>

</div>

<?php echo $footer; ?>