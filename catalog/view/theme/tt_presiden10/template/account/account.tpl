<?php echo $header; ?>
<style>
.deals-overlay {
        right: 0px;
        top: 10px;
        position: absolute;
        background: #f49a25;
        color: #fff;
        padding: 4px;
        font-weight: 600;
        width: 25%;
        text-align: center;
        z-index: 1;
    }
    .breadcrumb {
        margin-bottom: 0!important;
    }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container" style="background-color:#fafbfd; width:auto;">
 <br />     
    <div class="container top-banner hidden-xs hidden-sm">
        <img alt="True Elements" src="image/catalog/my-account-banner.jpg" class="img-responsive">
    </div>
    <div class="row top-banner hidden-md hidden-xl hidden-lg">
        <img alt="True Elements" src="image/catalog/my-account-banner-mobile.jpg" class="img-responsive">
    </div>
 <br />  
 <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/shipway_default.css" />
 <div id="shipway_track_wrapper" >
 	    	<form action="<?php echo $track; ?>" method="post" target="_blank">
				<fieldset>
					<legend>Track my order</legend>
						<table style="width: 100%;">
							<tbody>
								<tr>
									<th width="70">ORDER ID :</th><td><input type="text" name="order_id"  value="<?php if(isset($order_id)){ echo $order_id;} ?>" /></td>									
									<td><input type="submit" value="Track"  name="btnSubmit" class="btn btn-primary btn-block"/></td>
								</tr>
							</tbody>
						</table>
				</fieldset>
			</form>
		</div>
		<br />  	
<div class="container" style="background-color:#fff;">
<div class="row account_dashboard new_account_dashboard">
<aside id="column-left" class="col-sm-3 hidden-xs">
    
<div class="admin_wrapper">
	<div class="admin_images_section">
		<div class="images_border">
			<img alt="True Elements" id="changeprofilepic" src="/image/acoount.jpg" class="img-responsive">
		</div>	
		<h3><?php  echo $name; ?></h3>
		<p><?php if (isset($email)) { echo $email; } ?></p>
	</div>
	<div class="clear"></div>
</div>
<div class="list-group ad_my_account">
  <a href="/index.php?route=account/account" class="list-group-item"><i class="fa fa-user"></i> My Account</a>
  <a href="/te-labs" class="list-group-item"><i class="fa fa-cutlery"></i> TE Labs</a>
  <a href="/index.php?route=account/edit" class="list-group-item"><i class="fa fa-pencil-square-o"></i> Edit Account</a>
  <a href="/index.php?route=account/password" class="list-group-item"><i class="fa fa-key"></i> Password</a>  
  <a href="/index.php?route=account/address" class="list-group-item"><i class="fa fa-book"></i> Address Book</a>   
  <a href="/index.php?route=account/order" class="list-group-item"><i class="fa fa-undo"></i> Order History</a> 
  <a href="/index.php?route=account/wishlist" class="list-group-item"><i class="fa fa-heart"></i> Wish List</a>
  <a href="/index.php?route=account/reward" class="list-group-item"><i class="fa fa-shield"></i> True Cash</a>
    <?php if ($customergroup != '1') { ?>
      <a href="/index.php?route=account/myvoucher" class="list-group-item"><i class="fa fa-gift"></i> My Vouchers</a>   
    <?php } ?>
  <!--a href="/index.php?route=account/voucher" class="list-group-item"><i class="fa fa-gift"></i>Gift Voucher</a-->
	  </div>
	
	<!--<div class="ad_my_account">
<h3>Hi, <?php  echo $name; ?> Need help ?</h3>
</div>
<form id="quickcontact" class="form-horizontal">
	<div class="form-group">
		<div class="col-sm-12">
		  <input type="text" name="name" value="<?php  echo $name; ?>" id="input-name" placeholder="Your Name " class="form-control">
		</div>
	</div>
	<div class="form-group">	
		<div class="col-sm-12">
		  <input type="text" name="email" value="<?php  echo $email; ?>" id="input-email" placeholder="E-Mail Address" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-12" for="input-enquiry"></label>
		<div class="col-sm-12">
			<textarea class="form-control" id="input-enquiry" placeholder="Enquiry" name="enquiry"></textarea>
		</div>
	</div>
	<a class="btn btn-primary" id="sendenquery">Submit</a>
</form>-->
<script>
$('#sendenquery').on('click',function(){
$.ajax({
url: 'index.php?route=extension/module/eb_contactus/contactus',
type: 'post',
data: $('#quickcontact input[type=\'text\'],#quickcontact textarea'),
dataType: 'json',
beforeSend: function() {
	$('#sendenquery').button('loading');
},
complete: function() {
	$('#sendenquery').button('reset');
},
success: function(json) {
	$('.alert, .text-danger').remove();
	$('.form-group').removeClass('has-error');

	if (json['error']) {
		if (json['error']) {
			for (i in json['error']) {
				var element = $('#input-' + i.replace('_', '-'));

				if (element.parent().hasClass('input-group')) {
					element.parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
				} else {
					element.after('<div class="text-danger">' + json['error'][i] + '</div>');
				}
			}
		}

		if (json['error']['recurring']) {
			$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
		}

		// Highlight any found errors
		$('.text-danger').parent().addClass('has-error');
	}

	if (json['success']) {
		$('#input-name').val('');
		$('#input-email').val('');
		$('#input-enquiry').val('');
		$('#quickcontact').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
	}
},
error: function(xhr, ajaxOptions, thrownError) {
	alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}
});
});
</script>
  </aside>
        <?php if ($customergroup != '1') {?>
                 <?php  $class ="col-sm-3"; ?> 
        <?php } else { ?>
                   <?php $class ="col-sm-4"; ?>
		<?php } ?>
		
    <div id="content" class="col-sm-9">
      <div class="account_dashboard_wrapper">
		<h1></h1>
		<p></p>
		<div class="row">
					<div class="<?php echo $class; ?>">
				<div class="right_order_wrapper right_order_wrapper1">
					<div class="totle_order_wrapper">
					<h5 class="order_text"><?php echo $order_total; ?></h5>
						<div class="totle_icons_wrapper"><i class="fa fa-shopping-cart"></i></div>
					</div>
					<div class="totle_text_wrapper totle_text_wrapper1">
						<h4 class="order_heading"><a href="/index.php?route=account/order">Total Orders</a></h4>
					</div>
				</div>
			</div>
						<!-- -->
						<div class="<?php echo $class; ?>">
				<div class="right_order_wrapper right_order_wrapper2">
					<div class="totle_order_wrapper">
						<h5 class="transaction_text"><?php echo $reward_points; ?></h5>
						<div class="totle_icons_wrapper"><i class="fa fa-money"></i></div>
					</div>
					<div class="totle_text_wrapper totle_text_wrapper2">
						<h4 class="transaction_heading"><a href="/index.php?route=account/reward">Total balance</a></h4>
					</div>
				</div>
			</div>
						<!-- -->
						<div class="<?php echo $class; ?>">
			<div class="right_order_wrapper right_order_wrapper3">
				<div class="totle_order_wrapper">
					<h5 class="wishlist_text"><?php echo $wishlist_count; ?></h5>
					<div class="totle_icons_wrapper"><i class="fa fa-heart"></i></div>
				</div>
				<div class="totle_text_wrapper totle_text_wrapper3">
					<h4 class="wishlist_heading"><a href="/index.php?route=account/wishlist">Total wishlist</a></h4>
				</div>
			</div>
			</div>
			
			<?php if ($customergroup != '1') { ?>
			
                <div class="<?php echo $class; ?>">
		            	<div class="right_order_wrapper right_order_wrapper3">
				<div class="totle_order_wrapper">
					<h5 class="wishlist_text"><?php echo $voucher_count; ?></h5>
					<div class="totle_icons_wrapper"><i class="fa fa-gift"></i></div>
				</div>
				<div class="totle_text_wrapper totle_text_wrapper3">
					<h4 class="wishlist_heading"><a href="/index.php?route=account/myvoucher">My Vouchers</a></h4>
				</div>
			</div>
			</div>      
        
		<?php } ?>
		
		</div>
		<!--- --->
		<div class="row change_items">
								<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="change_password_wrapper">
					<div class="change_icon_wrapper"><i class="fa fa-user"></i></div>
					<h3><a href="index.php?route=account/edit">Account</a></h3>
				</div>
			</div>
											<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="change_password_wrapper">
					<div class="change_icon_wrapper"><i class="fa fa-key"></i></div>
					<h3><a href="index.php?route=account/password">Password</a></h3>
				</div>
			</div>
											<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="change_password_wrapper">
					<div class="change_icon_wrapper"><i class="fa fa-book"></i></div>
					<h3><a href="index.php?route=account/address">Address</a></h3>
				</div>
			</div>
											<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="change_password_wrapper">
					<div class="change_icon_wrapper"><i class="fa fa-shopping-cart"></i></div>
					<h3><a href="index.php?route=account/order">Order History</a></h3>
				</div>
			</div>
						
		</div>
		<!-- Recent Orders -->
				<div class="recent_orders">
		<h3>Recent Orders</h3>
		<div class="table-responsive">
		<?php if ($orders) { ?>
			<table class="table table-responsive">
				<thead>
            <tr>
              <td class="text-right">Order Id</td>
              <td class="text-left">Customer</td>
              <td class="pending_section">Order Status</td>
              <td class="text-right">Total</td>
              <td class="text-left">Ordered date</td>
              <td> View </td>
             <!-- <td>Cancel</td> -->
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; ?>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td class="text-left">#<?php echo $order['order_id']; ?></td>
              <td class="text-left"><?php echo $order['name']; ?></td>
              <td class="pending_section"><span><?php echo $order['status']; ?></span></td>
              <td class="text-left"><?php echo $order['total']; ?></td>
              <td class="text-left"><?php echo $order['date_added']; ?></td>
              <td class="eye_icons">
                  <a href="<?php echo $order['view']; ?>"><i class="fa fa-eye"></i></a>
			<form action="<?php echo $track; ?>" method="post" target="_blank">
				  
				     <a onclick="$('#submit').trigger('click')" target="new" type="submit" style="cursor: pointer;"><i class="fa fa-truck"></i></a>
				     <input style="display:none;" type="submit" id="submit"/>
				       <input type="hidden" name="order_id"  value="<?php echo $order['order_id']; ?>" />  
			</form>
				 </td>
              
            </tr>
            <?php  if ($i++ == 8) break; ?>
            <?php } ?>
          </tbody>
			</table>
		<?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
		</div>
		</div>
		
		
				<!-- Prdoucts -->
		 	<div class="recent_orders">
			<h3><?php echo $name; ?>, it may be interesting for you!</h3>
		</div>
		<div class="row">
		<div class="most-viewed-products-slider">
		<?php if($products): ?>
		<?php foreach ($products as $product) { ?>
		<?php  if($count % $rows == 0 ) { echo '<div class="item">'; } $count++; ?>
		<div class="item-inner">
			<?php if($config_slide['f_show_label']): ?>
				<?php if ($product['is_new']):
					if($product['special']): ?>
						<div class="deals-overlay"><?php echo $text_sale; ?></div>
					<?php else: ?>
						<div class="new"><?php echo $text_new; ?></div>
					<?php endif; ?>
				<?php else: ?>
					<?php if($product['special']): ?>
						<div class="deals-overlay"><?php echo $text_sale; ?></div>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
			<div class="images-container">
				<?php if ($product['thumb']) { ?>
					<a class="image" href="<?php echo $product['href']; ?>">
						<?php if($product['rotator_image']): ?>
						<img class="image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" />
						<?php endif; ?>
						<img class="image1" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
					</a>
				<?php } else { ?>
					<a class="product-image" href="<?php echo $product['href']; ?>">
						<div class="product-image">
							<img src="image/cache/no_image-100x100.png" alt="<?php echo $product['name']; ?>" />
						</div>
					</a>
				<?php } ?>
			</div><!--images-container-->
			<div class="des-container">
				<h2 class="product-name" style="height: 70px;"><a href="<?php echo $product['href']; ?>">
				   <?php echo substr($product['name'],0,52); ?></a></h2>
				<?php if (isset($product['rating'])) { ?>
					<div class="ratings">
						<div class="rating-box">
							<?php for ($i = 0; $i <= 5; $i++) { ?>
								<?php if ($product['rating'] == $i) {
									$class_r= "rating".$i;
									echo '<div class="'.$class_r.'">rating</div>';
								} 
							}  ?>
						</div>
					</div>
				<?php } ?>
				<?php if($config_slide['f_show_des']) { ?>
					<div class="product-des"><?php echo $product['description']; ?></div>
				<?php } ?>
				<div class="price-label">
					<div class="box-price">
						<?php if($config_slide['f_show_price']) { ?>
							<?php if ($product['price']) { ?>
							<div class="price-box">
								<?php if (!$product['special']) { ?>
									<span class="price"><?php echo $product['price']; ?></span>
								<?php } else { ?>
									<p class="special-price"><span class="price"><?php echo $product['special']; ?></span></p>
									<p class="old-price"><span class="price"><?php echo $product['price']; ?></span></p>
								<?php } ?>
							</div>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<div class="actions">
					<div class="add-to-links">
						<div class="cart">
							<?php if($config_slide['f_show_addtocart']) { ?>
								<button class="button btn-cart" type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');">
									<span><span><?php echo $button_cart; ?></span></span>
								</button>
							<?php } ?>
						</div>
						<div class="wishlist">
							<a class="link-wishlist" title="<?php echo $button_wishlist; ?>" data-toggle="tooltip" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
								<em><?php echo $button_wishlist; ?></em>
							</a>
						</div>
						
					</div>
				</div>
			</div><!-- des-container -->
		</div> <!-- item-inner -->
			<?php if($count % $rows == 0 ): ?>
			</div>
		<?php else: ?>
			<?php if($count == count($products)): ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<?php } ?>
		<?php else: ?>
			<p><?php echo $text_empty; ?></p>
		<?php endif; ?>
		</div>
	
		  </div>
		  	
	 </div>
	 </div>
    </div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() { 
  	$(".most-viewed-products-slider").owlCarousel({
	  autoPlay: <?php if($config_slide['autoplay']) { echo 'true' ;} else { echo 'false'; } ?>,
		items : <?php if($config_slide['items']) { echo $config_slide['items'] ;} else { echo 3; } ?>,
		slideSpeed : <?php if($config_slide['f_speed']) { echo $config_slide['f_speed']; } else { echo 200;} ?>,
		navigation : <?php if($config_slide['f_show_nextback']) { echo 'true' ;} else { echo 'false'; } ?>,
		paginationNumbers : true,
		pagination : <?php if($config_slide['f_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
		stopOnHover : false,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [991,1],
		itemsTablet: [700,1],
		itemsMobile : [400,1],
  	});
 
});
</script>
<?php echo $footer; ?>