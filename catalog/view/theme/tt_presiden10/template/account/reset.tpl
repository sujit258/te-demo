<?php echo $header; ?>
<style>
    .group {
        position: relative;
        margin-bottom: 20px;
    }
    .group .inputMaterial {
        font-size: 13px;
        display: block;
        width: 100%;
        height: 42px;
        box-shadow: none;
        -webkit-box-shadow: none;
        color: #333333;
        padding: 16px 15px 4px;
        border-radius: 4px;
        border: 1px solid  #dddddd;
    }
    
    .group .inputMaterial:focus {
      outline: 0;
      -webkit-box-shadow: none;
      box-shadow: none;
      border-color: #96858d;
    }
    
    .group label {
        color: #cecbe2;
        font-size: 12px;
        font-weight: 600;
        position: absolute;
        pointer-events: none;
        left: 5px;
        top: 12px;
        transition: 0.2s ease all;
        -moz-transition: 0.2s ease all;
        -webkit-transition: 0.2s ease all;
        z-index: 22;
        padding: 0px 10px;
    }
    .group.focus label {
      top: -8px !important;
      background: #ffffff;
    }
    .group.focus label span.required:after {
      display: inline-block;
    }
    .group.focus select.inputMaterial {
      padding-top: 18px;
    }
    .group.focus label {
      color: #96858d;
    }
    *:focus,
    a:focus {
      outline: none;
    }
    
    
    .group.focus label {
      top: -8px !important;
      background: #ffffff !important;
    }
    .group.focus label {
      color: #000 !important;
    }
     
    .group.filled label {
        top: -8px !important;
        background: #fff;
        color: #96858d;
    }   
    .group.filled label span.required:after {
      display: inline-block;
    }
    .group.filled select.inputMaterial {
      padding-top: 18px;
    }
    select.inputMaterial.group.filled+labelselect::before {
      top: 10px;
    }
    select.inputMaterial.group.filled+labelselect::after {
      top: 12px;
    }
    .group.filled .is_floating_tooltip {
      visibility: visible;
    }
    .group.filledalways label {
      top: -8px !important;
      background: #fff;
    }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="col-md-4"></div>
      <div class="col-md-4" style="box-shadow: 0 5px 6px rgba(0,0,0,0.5); border: 1px solid rgb(240, 240, 240); margin: 10px 10px 10px 10px; padding-bottom: 20px; padding-top: 20px;">
       <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <h1 style="text-align: center;"><?php echo $text_password; ?></h1>
          <div class="group required">
              <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="inputMaterial" />
              <?php if ($error_password) { ?>
                <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
              <label for="input-password"><?php echo $entry_password; ?></label>
          </div>
          <div class="group required">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="inputMaterial" />
              <?php if ($error_confirm) { ?>
                <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
              <label for="input-confirm"><?php echo $entry_confirm; ?></label>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-right"><button type="submit" class="btn btn-primary button">Save</button></div>
        </div>
       </form>
      </div>
      <div class="col-md-4"></div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script>
   $('.inputMaterial').focusout(function() {
    $('.group').removeClass('focus');    
    if(false && $(this).val().length < 1 && $(this).closest('.group').children().has('.xerror')){
    	$(this).closest('.group').addClass('has-error');
    	$(this).closest('.group').children('.xerror').removeClass('hidden');
    }    
});
$('.inputMaterial').focus(function() {
    $(this).closest('.group').addClass('focus');
    if(false){
    	$(this).closest('.group').removeClass('has-error');
    	$(this).closest('.group').children('.xerror').addClass('hidden');
    }
});
$('input.inputMaterial, textarea.inputMaterial').bind("change",function(){
	if($(this).val().length > 0){
        $(this).closest('.group').addClass('filled');
    }
    else{
        $(this).closest('.group').removeClass('filled');
    }
});
$('input.inputMaterial, textarea.inputMaterial').bind("change keyup",function(){
	if($(this).val().length > 0){
        $(this).closest('.group').addClass('filled');
    }
    else{
        $(this).closest('.group').removeClass('filled');
    }
});
$('select.inputMaterial').bind("change keyup",function(){
	
   $(this).closest('.group').addClass('filled');
    
});
/// Input Kepress Filled  Focus
$('input.inputMaterial, textarea.inputMaterial').keyup(function() {
    if($(this).val().length > 0){
        $(this).closest('.group').addClass('filled');
    }

    else{
        $(this).closest('.group').removeClass('filled');
    }
});

/// Input Check Filled Focus
var $formControl = $('input.inputMaterial, textarea.inputMaterial');
var values = {};
var validate =    $formControl.each(function() {
    if($(this).val() != null && $(this).val().length > 0){
        $(this).closest('.group').addClass('filled');
    }
    else{
        $(this).closest('.group').removeClass('filled');
    }
});
</script>
<?php echo $footer; ?>