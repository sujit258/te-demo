<?php echo $header; ?>
<style>
    /* HIDE RADIO */
[type=radio] { 
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
}

/* IMAGE STYLES */
[type=radio] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
[type=radio]:checked + img {
  outline: 2px solid #fb9935;
  
}

[type=radio]:checked + span{
  outline: 2px solid #fb9935;
}

.voucher-image {
    cursor: pointer;
    /*width: 100%;
    height: 80px;
    width: 80px;*/
} 
  
.ui-button-text{
    background-color: #fff;
    color: #454545;
    border-color: transparent;
    height: 36px;
    width: 80px;
    display: inline-block;
    padding: 8px;
    outline: 2px solid #000;
}

.form-horizontal{
    background: #f5f5f5;
    padding: 3%;
}
 
input[type="text"].form-control, input[type="number"].form-control, input[type="email"].form-control, input[type="tel"].form-control, input[type="color"].form-control,textarea.form-control {
        font-size: 16px;
        background: #fff;
        border-radius: 8px !important;
}

.checkbox label, .radio label {
    text-align: center;
}   

.text-danger { 
    text-align: center;
    font-size: 15px;
}

</style>

<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<a href="/gift-hampers"><img src="<?php echo $value_gift_banner_desktop; ?>" class="hidden-xs hidden-sm" alt="True Elements" style="padding-bottom: 15px;"></a>
<a href="/gift-hampers"><img src="<?php echo $value_gift_banner_mobile; ?>" class="hidden-md hidden-xl hidden-lg" alt="True Elements" style="padding-bottom: 15px;"></a>
<img src="<?php echo $gifting_banner_desktop; ?>" alt="True Elements" style="margin-bottom: 25px; width: 100%; margin-top: -15px;" />
<div class="container">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
   <div class="col-md-2 hidden-xs hidden-sm"></div>
    <div id="content" class="col-md-8 col-sm-12 col-xs-12"><?php echo $content_top; ?>
       
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
          
          <div class="form-group">
          <label class="col-xs-2 col-sm-4 control-label"> </label>
          <label class="col-xs-8 col-sm-4 control-label" style="font-size: 21px;margin-bottom: 2%; text-align: center;">Gift a True Voucher</label>
          <label class="col-xs-2 col-sm-4 control-label"> </label>
          </div>
          
          
          <?php if (!empty($is_voucher_in_cart)) { ?>
                    <label class="col-sm-1 control-label"> </label>
                  <label class="col-sm-9 control-label" style="font-size: 21px;margin-bottom: 2%;">You have already added gift voucher in cart</label>
                  <label class="col-sm-1 control-label"> </label> 
                <?php } ?>
                
          <div class="form-group required">
            <div class="col-md-3 col-sm-3 col-xs-1"></div>
            <div class="col-md-6 col-sm-6 col-xs-10">
                <div class="radio col-md-offset-2 col-sm-offset-3 col-xs-offset-2 col-md-4 col-sm-3 col-xs-4"> 
                    <label style="padding: 0;">
                          <input type="radio" name="amountgroup" class="amountgroup" value="500" />
                          <span class="ui-button-text ui-clickable">Rs.500</span> 
                    </label>
                </div>
                <div class="radio col-md-4 col-sm-3 col-xs-4"> 
                    <label style="padding: 0;">
                          <input type="radio" name="amountgroup" class="amountgroup" value="1000" />
                              <span class="ui-button-text ui-clickable">Rs.1000</span> 
                    </label>
                </div>
                 <?php if ($error_amount) { ?>
                <div class="text-danger"><?php echo $error_amount; ?></div>
            <?php } ?>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-1"> </div>
            <input type="hidden" name="amount" value="<?php echo $amount; ?>" id="input-amount" class="form-control" size="5" />
           
        </div>
        
          <div class="form-group"> 
          <label class="col-xs-1 col-sm-4 control-label"> </label>
          <label class="col-xs-10 col-sm-4 control-label" style="font-size: 21px;margin-bottom: 2%; text-align: center;">Choose your design</label>
          <label class="col-xs-1 col-sm-4 control-label"> </label>
          </div>
          <div class="form-group required"> 
            <div class="col-sm-12 ">
                
            <?php foreach ($voucher_themess as $voucher_theme) { ?>
            <?php if ($voucher_theme['voucher_theme_id'] == $voucher_theme_id) { ?>
            <div class="radio col-sm-4 col-xs-12"> 
                <label>
                      <input type="radio" name="voucher_theme_id"  value="<?php echo $voucher_theme['voucher_theme_id']; ?>"  checked="checked" />
                      <img src="<?php echo $voucher_theme['image']; ?>" class="voucher-image">
                      <span class="col-md-12 col-xs-12 col-sm-12" style="padding: 10px;"><?php echo $voucher_theme['name']; ?></span>
                </label>
            </div>
            <?php } else { ?>
            <div class="radio col-sm-4 col-xs-12"> 
                <label>
                      <input type="radio" name="voucher_theme_id"  value="<?php echo $voucher_theme['voucher_theme_id']; ?>" />
                      <img src="<?php echo $voucher_theme['image']; ?>" class="voucher-image">
                      <span class="col-md-12 col-xs-12 col-sm-12" style="padding: 10px;"><?php echo $voucher_theme['name']; ?></span>
                </label>
            </div>
            <?php } ?>
            <?php } ?>
            <?php if ($error_theme) { ?>
            <div class="text-danger"><?php echo $error_theme; ?></div>
            <?php } ?>
          </div>
        </div>
        
        <div class="form-group required">
          <label class="col-sm-3 control-label" for="input-to-name">Name</label>
          <div class="col-sm-6">
            <input type="text" name="to_name" value="<?php echo $to_name; ?>" id="input-to-name" class="form-control" />
            <?php if ($error_to_name) { ?>
            <div class="text-danger"><?php echo $error_to_name; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-3 control-label" for="input-to-email">Email</label>
          <div class="col-sm-6">
            <input type="text" name="to_email" value="<?php echo $to_email; ?>" id="input-to-email" class="form-control" />
            <?php if ($error_to_email) { ?>
            <div class="text-danger"><?php echo $error_to_email; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-3 control-label" for="input-from-name">From</label>
          <div class="col-sm-6">
            <input type="text" name="from_name" value="<?php echo $from_name; ?>" id="input-from-name" class="form-control" />
            <?php if ($error_from_name) { ?>
            <div class="text-danger"><?php echo $error_from_name; ?></div>
            <?php } ?>
          </div>
        </div>
        <!--div class="form-group required">
          <label class="col-sm-2 control-label" for="input-from-email"><?php echo $entry_from_email; ?></label>
          <div class="col-sm-10">
            <input type="text" name="from_email" value="<?php echo $from_email; ?>" id="input-from-email" class="form-control" />
            <?php if ($error_from_email) { ?>
            <div class="text-danger"><?php echo $error_from_email; ?></div>
            <?php } ?>
          </div>
        </div-->
        
        <div class="form-group">
          <label class="col-sm-3 control-label" for="input-message"><span data-toggle="tooltip" title="<?php echo $help_message; ?>">Message</span></label>
          <div class="col-sm-6">
            <textarea name="message" cols="40" rows="5" id="input-message" class="form-control"><?php echo $message; ?></textarea>
          </div>
        </div>
        <!--div class="form-group">
          <label class="col-sm-3 control-label" for="input-amount"><span data-toggle="tooltip" title="<?php echo $help_amount; ?>"><?php echo $entry_amount; ?></span></label>
          <div class="col-sm-6">
            <input type="text" name="amount" value="<?php echo $amount; ?>" id="input-amount" class="form-control" size="5" />
            <?php if ($error_amount) { ?>
            <div class="text-danger"><?php echo $error_amount; ?></div>
            <?php } ?>
          </div>
        </div-->
        <div class="buttons clearfix">
          <div class="pull-right"> 
            &nbsp;
            
          </div>
        </div>
        
         <div class="form-group">
            <label class="col-sm-8 control-label">
                <?php if ($agree) { ?>
                <input type="checkbox" name="agree" value="1" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="agree" value="1" />
                <?php } ?>
                <?php echo $text_agree; ?>
            </label>
          </div>
          <div class="form-group required">
            <div class="col-sm-12 col-md-12 col-xs-1" style="text-align: center;">
                <?php if (empty($is_voucher_in_cart)) { ?>
                    <input type="submit" value="Buy Now" class="btn btn-primary" />
                <?php } ?>
            </div><br/>
            
            <label class="col-sm-8" style="margin-top: 3%;"><?php echo $text_description; ?></label> 
      </form>
      <?php echo $content_bottom; ?></div>
      
      <div class="col-md-2 hidden-xs hidden-sm"></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<script>
    $('.amountgroup').change(function(e){
    var selectedValue = $(this).val();
    $('#input-amount').val(selectedValue)
});
</script>
<?php echo $footer; ?>