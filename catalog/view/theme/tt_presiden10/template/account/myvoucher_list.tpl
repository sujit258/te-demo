<?php echo $header; ?>
 
<style>
 

    td,th {
      border: 1px solid #ddd;
      padding: 8px;
    }

     tr:nth-child(even){background-color: #f2f2f2;}

     tr:hover {background-color: #ddd;}

     th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #000;
          color: white;
    }
    
    h4,h5 {
        text-align:center;
    }
    
    .oos{
        background: #777!important;
        border-radius: 4px!important; 
        font-size: 13px!important;
        padding: 8px 10px!important;
    } 
    .button-product-cart{
        border-radius: 4px!important; 
        font-size: 13px!important;
        padding: 8px 10px!important;
    }    
    .card{
      background-color: #fff;
    }
    
    label {
        padding-top: 7px;
    }
    .form-control {
        border-radius: 0 !important;
        background: #fff;
    }
</style> 
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
     <div class="container" style="background-color:#fafbfd; width:auto; margin-top:-35px;">
<div class="container">
  <div class="row" style="margin-top: 36px;">
    <div class="col-md-3">
    <aside id="column-left" class="col-sm-3 hidden-xs row-md-3" style="display: block; width: 100%;">
    
<div class="admin_wrapper">
	<div class="admin_images_section">
		<div class="images_border">
			<img id="changeprofilepic" src="/image/acoount.jpg" class="img-responsive" alt="True Elements">
		</div>	
		<h3><?php  echo $name; ?></h3>
		<p><?php if (isset($email)) { echo $email; } ?></p>
	</div>
	<div class="clear"></div>
</div>
<div class="list-group ad_my_account">
  <a href="/index.php?route=account/account" class="list-group-item"><i class="fa fa-user"></i> My Account</a>
  <a href="/index.php?route=account/edit" class="list-group-item"><i class="fa fa-pencil-square-o"></i> Edit Account</a>
  <a href="/index.php?route=account/password" class="list-group-item"><i class="fa fa-key"></i> Password</a>  
  <a href="/index.php?route=account/address" class="list-group-item"><i class="fa fa-book"></i> Address Book</a>   
  <a href="/index.php?route=account/order" class="list-group-item"><i class="fa fa-undo"></i> Order History</a> 
  <a href="/index.php?route=account/wishlist" class="list-group-item"><i class="fa fa-heart"></i> Wish List</a>
  <a href="/index.php?route=account/reward" class="list-group-item"><i class="fa fa-shield"></i> True Cash</a>
    <?php if ($customergroup != '1') { ?>
      <a href="/index.php?route=account/myvoucher" class="list-group-item"><i class="fa fa-gift"></i> My Vouchers</a>   
    <?php } ?>
  <!--a href="/index.php?route=account/voucher" class="list-group-item"><i class="fa fa-gift"></i>Gift Voucher</a-->
	  </div>
	
	<!--<div class="ad_my_account">
<h3>Hi, <?php  echo $name; ?> Need help ?</h3>
</div>
<form id="quickcontact" class="form-horizontal">
	<div class="form-group">
		<div class="col-sm-12">
		  <input type="text" name="name" value="<?php  echo $name; ?>" id="input-name" placeholder="Your Name " class="form-control">
		</div>
	</div>
	<div class="form-group">	
		<div class="col-sm-12">
		  <input type="text" name="email" value="<?php  echo $email; ?>" id="input-email" placeholder="E-Mail Address" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-12" for="input-enquiry"></label>
		<div class="col-sm-12">
			<textarea class="form-control" id="input-enquiry" placeholder="Enquiry" name="enquiry"></textarea>
		</div>
	</div>
	<a class="btn btn-primary" id="sendenquery">Submit</a>
</form>-->
<script>
$('#sendenquery').on('click',function(){
$.ajax({
url: 'index.php?route=extension/module/eb_contactus/contactus',
type: 'post',
data: $('#quickcontact input[type=\'text\'],#quickcontact textarea'),
dataType: 'json',
beforeSend: function() {
	$('#sendenquery').button('loading');
},
complete: function() {
	$('#sendenquery').button('reset');
},
success: function(json) {
	$('.alert, .text-danger').remove();
	$('.form-group').removeClass('has-error');

	if (json['error']) {
		if (json['error']) {
			for (i in json['error']) {
				var element = $('#input-' + i.replace('_', '-'));

				if (element.parent().hasClass('input-group')) {
					element.parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
				} else {
					element.after('<div class="text-danger">' + json['error'][i] + '</div>');
				}
			}
		}

		if (json['error']['recurring']) {
			$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
		}

		// Highlight any found errors
		$('.text-danger').parent().addClass('has-error');
	}

	if (json['success']) {
		$('#input-name').val('');
		$('#input-email').val('');
		$('#input-enquiry').val('');
		$('#quickcontact').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
	}
},
error: function(xhr, ajaxOptions, thrownError) {
	alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}
});
});
</script>
  </aside>
  <div class="row-md-9">
    <?php echo $column_left; ?>
    <?php echo $column_right; ?>
   </div>
</div>
<div id="content" class="col-md-9"><?php echo $content_top; ?>
      <h1 class="page-title"><?php echo $heading_title; ?></h1>
      <?php if ($myvouchers) { ?>
        <table style="width:100%">
              <tr>
                <th>Code</th>
                <th>Amount</th>
                <th>Action</th>
              </tr>
            <?php foreach ($myvouchers as $myvoucher) { ?>
             
                 <tr>
                    <td><?php echo $myvoucher['code']; ?></td>
                    <td><?php echo $myvoucher['amount']; ?></td>
                    
                    <?php if ($myvoucher['is_send'] != '1') { ?>      
                        <td><button type="button" class="btn button button-product-cart" data-toggle="modal" data-target="#myModal-<?php echo $myvoucher['voucher_id']; ?>">View</button></td>
                    <?php } else { ?> 
                            <td><button class="oos btn button" type="button" disabled="">Already sent!</button></td>
                            <?php } ?>
                            
              </tr>
              
                <!-- Modal -->
                  <div class="modal fade" id="myModal-<?php echo $myvoucher['voucher_id']; ?>" role="dialog">
                      <div id="history-<?php echo $myvoucher['voucher_id']; ?>"></div>
                    <div class="modal-dialog">
                    
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Voucher details</h4>
                        </div>
                        <div class="modal-body">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="card-body">
                            
                            <div class="col-md-6 required">
                                <label class="control-label" for="input-email">From Name</label>
                                <div class="">
                                  <input type="text" name="from_name" value="<?php echo $myvoucher['from_name']; ?>" id="input-from_name" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6 required">
                                <label class="control-label" for="input-email">From Email</label>
                                <div class="">
                                  <input type="text" name="from_email" value="<?php echo $myvoucher['from_email']; ?>" id="input-from_email" class="form-control" />
                                </div>
                            </div>
                            
                            <div class="col-md-6 required">
                                <label class="control-label" for="input-email">Recipient\'s Name</label>
                                <div class="">
                                  <input type="text" name="to_name" value="<?php echo $myvoucher['to_email']; ?>" id="input-to_email" class="form-control" />
                                </div>
                            </div>
                            
                            <div class="col-md-6 required">
                                <label class="control-label" for="input-email">Recipient\'s e-mail</label>
                                <div class="">
                                  <input type="text" name="to_email" value="<?php echo $myvoucher['to_email']; ?>" id="input-to_email" class="form-control" />
                                </div>
                            </div>
                            
                            <div class="col-md-6 required">
                                <label class="control-label" for="input-email">Amount</label>
                                <div class="">
                                  <input type="text" name="amount" value="<?php echo $myvoucher['amount']; ?>" id="input-ammount" class="form-control" readonly/>
                                </div>
                            </div>
                            
                            <div class="col-md-6 required">
                                <label class="control-label" for="input-email">VoucherCode</label>
                                <div class="">
                                  <input type="text" name="code" value="<?php echo $myvoucher['code']; ?>" id="input-code" class="form-control" readonly/>
                                  <input type="hidden" name="voucher_id" value="<?php echo $myvoucher['voucher_id']; ?>" id="input-voucher_id" class="form-control" />
                                </div>
                            </div>
                         <br><br>      
                             <div class="buttons clearfix">
                              <div class="pull-right" style="margin-top: 4%;">
                             
                             <?php if ($myvoucher['is_send'] != '1') { ?>      
                                <input id="button-send-mail-<?php echo $myvoucher['voucher_id']; ?>" value="Send Mail" class="btn button button-product-cart" />
                             <?php } else { ?> 
                            <input type="submit" value="Already sent" class="btn btn-primary" disabled/>
                            <?php } ?> 
                              </div>
                            </div>  
                        </form>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                  
                  <script type="text/javascript">   
                    $('#button-send-mail-<?php echo $myvoucher['voucher_id']; ?>').bind('click', function() {
                    	$.ajax({
                    		url: 'index.php?route=account/myvoucher/sendmail&sendmail=1',
                    		type: 'post',
                    		dataType: 'html',
                    		data: 'from_name=' + encodeURIComponent($('#myModal-<?php echo $myvoucher['voucher_id']; ?> input[name=\'from_name\']').val()) + '&from_email=' + encodeURIComponent($('#myModal-<?php echo $myvoucher['voucher_id']; ?> input[name=\'from_email\']').val()) + '&voucher_id=' + encodeURIComponent($('#myModal-<?php echo $myvoucher['voucher_id']; ?> input[name=\'voucher_id\']').val()) + '&to_email=' + encodeURIComponent($('#myModal-<?php echo $myvoucher['voucher_id']; ?> input[name=\'to_email\']').val())+ '&to_name=' + encodeURIComponent($('#myModal-<?php echo $myvoucher['voucher_id']; ?> input[name=\'to_name\']').val())+ '&to_email=' + encodeURIComponent($('#myModal-<?php echo $myvoucher['voucher_id']; ?> input[name=\'to_email\']').val())+ '&code=' + encodeURIComponent($('#myModal-<?php echo $myvoucher['voucher_id']; ?> input[name=\'code\']').val())+ '&amount=' + encodeURIComponent($('#myModal-<?php echo $myvoucher['voucher_id']; ?> input[name=\'amount\']').val()),
                    		beforeSend: function() {
                    			$('.success, .warning').remove();
                    			$('#button-send-mail-<?php echo $myvoucher['voucher_id']; ?>').attr('disabled', true);
                    			$('#history-<?php echo $myvoucher['voucher_id']; ?>').before('<div class="attention"><img src="view/image/loading.gif" alt="True Elements" /> <?php echo $text_wait; ?></div>');
                    		},
                    		complete: function() {
                    			$('#button-send-mail-<?php echo $myvoucher['voucher_id']; ?>').attr('disabled', false);
                    			$('.attention').remove();
                    		},
                    		success: function(html) {
                    			$('#history-<?php echo $myvoucher['voucher_id']; ?>').html(html);
                    			$('#button-send-mail-<?php echo $myvoucher['voucher_id']; ?>').attr('value', 'Mail sent');
                    			
                    			  location.reload();
                    			  
                    			$('#button-send-mail-<?php echo $myvoucher['voucher_id']; ?>').attr("disabled", true);
                    		}
                    	});
                    });
                    </script> 
                 
            <?php } ?> 
        </table>
        <br>
        <div class="row">
            <div class="col-md-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-md-6 text-right"><?php echo $results; ?></div>
        </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      
      <?php echo $content_bottom; ?></div> 
    <?php echo $column_right; ?>

   </div></div></div>
   
   


<?php echo $footer; ?>
