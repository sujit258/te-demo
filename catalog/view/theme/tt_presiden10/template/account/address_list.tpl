<?php echo $header; ?>
<style>
    .breadcrumb {
        margin-bottom: 0!important;
    }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container" style="background-color:#fafbfd; width:auto;">
	<div class="container" style="background-color:#fff;">
		<div class="row account_dashboard new_account_dashboard">
			<aside id="column-left" class="col-sm-3 hidden-xs">

				<div class="admin_wrapper">
					<div class="admin_images_section">
						<div class="images_border">
							<img id="changeprofilepic" src="/image/acoount.jpg" class="img-responsive">
						</div>	
						<h3><?php  echo $name; ?></h3>
						<p><?php if (isset($email)) { echo $email; } ?></p>
					</div>
					<div class="clear"></div>
				</div>
				<div class="list-group ad_my_account">
                  <a href="/index.php?route=account/account" class="list-group-item"><i class="fa fa-user"></i> My Account</a>
                  <a href="/index.php?route=account/edit" class="list-group-item"><i class="fa fa-pencil-square-o"></i> Edit Account</a>
                  <a href="/index.php?route=account/password" class="list-group-item"><i class="fa fa-key"></i> Password</a>  
                  <a href="/index.php?route=account/address" class="list-group-item"><i class="fa fa-book"></i> Address Book</a>   
                  <a href="/index.php?route=account/order" class="list-group-item"><i class="fa fa-undo"></i> Order History</a> 
                  <a href="/index.php?route=account/wishlist" class="list-group-item"><i class="fa fa-heart"></i> Wish List</a>
                  <a href="/index.php?route=account/reward" class="list-group-item"><i class="fa fa-shield"></i> True Cash</a>
	            </div>	

			</aside>

			<div id="content" class="col-sm-9">
				  <div class="row" style="margin-top: 36px;">
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    
<div><?php echo $content_top; ?>
    <div class="col-md-12">
      <div class="row">     
       
        
       <div class="panel panel-info">
          <div class="panel-heading" style="background-color: #000;color: #fff;font-size: 15px;font-weight: 600;"><?php echo $text_address_book; ?></div>
          <div class="panel-body">
              <?php if ($addresses) { ?>
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <?php foreach ($addresses as $result) { ?>
                  <tr>
                    <td class="text-left"><?php echo $result['address']; ?></td>
                    <td class="text-right"><a href="<?php echo $result['update']; ?>" class="btn btn-info"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn btn-danger"><?php echo $button_delete; ?></a></td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
        <?php } ?>
        <div class="buttons clearfix">
            <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
            <div class="pull-right"><a href="<?php echo $add; ?>" class="btn btn-primary"><?php echo $button_new_address; ?></a></div>
        </div>
        <?php echo $content_bottom; ?></div>
    </div>
    
</div>
</div>

<?php echo $content_bottom; ?>

</div>
<div class="col-sm-12">
  <div class="col-sm-6 text-left"></div>
  <div class="col-sm-6 text-right"></div>
</div>
</div>

			</div>
		</div>
	</div>

</div>

<?php echo $footer; ?>