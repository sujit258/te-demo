<?php echo $header; ?>
<style>
    .breadcrumb {
        margin-bottom: 0!important;
    }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container" style="background-color:#fafbfd; width:auto;">
	<div class="container" style="background-color:#fff;">
		<div class="row account_dashboard new_account_dashboard" style="margin-top: 0!important">
			<aside id="column-left" class="col-sm-3 hidden-xs">

				<div class="admin_wrapper">
					<div class="admin_images_section">
						<div class="images_border">
							<img id="changeprofilepic" src="/image/acoount.jpg" class="img-responsive" alt="True Elements">
						</div>	
						<h3><?php  echo $name; ?></h3>
						<p><?php if (isset($email)) { echo $email; } ?></p>
					</div>
					<div class="clear"></div>
				</div>
			    <div class="list-group ad_my_account">
                  <a href="/index.php?route=account/account" class="list-group-item"><i class="fa fa-user"></i> My Account</a>
                  <a href="/index.php?route=account/edit" class="list-group-item"><i class="fa fa-pencil-square-o"></i> Edit Account</a>
                  <a href="/index.php?route=account/password" class="list-group-item"><i class="fa fa-key"></i> Password</a>  
                  <a href="/index.php?route=account/address" class="list-group-item"><i class="fa fa-book"></i> Address Book</a>   
                  <a href="/index.php?route=account/order" class="list-group-item"><i class="fa fa-undo"></i> Order History</a> 
                  <a href="/index.php?route=account/wishlist" class="list-group-item"><i class="fa fa-heart"></i> Wish List</a>
                  <a href="/index.php?route=account/reward" class="list-group-item"><i class="fa fa-shield"></i>True Cash</a>
	            </div>

			</aside>

	<div id="content" class="col-sm-9">
	    <div class="row" style="margin-top: 36px;">
    	    <?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-md-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-md-9'; ?>
            <?php } else { ?>
            <?php $class = 'col-md-12'; ?>
            <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_code; ?></legend>
          <div class="form-group required">
            <label class="col-md-2 control-label" for="input-code"><?php echo $entry_code; ?></label>
            <div class="col-md-4">
              <input type="text" name="code" value="<?php echo $code; ?>" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control" />
              <?php if ($error_code) { ?>
              <div class="text-danger"><?php echo $error_code; ?></div>
              <?php } ?>
            </div>
          </div> 
        </fieldset>
         <?php if ($success) { ?>
            <div class="alert  "><i class="fa fa-check-circle"></i><span style="color:#228B22"><?php echo $success; ?> </span> </br> <span><a href="https://www.true-elements.com/index.php?route=account/reward" class="btn btn-primary" style="margin-top: 2%;margin-left: 38%;">View TrueCash balance</a></span></div>
        <?php } ?>
        <div class="buttons clearfix">
          <div class="pull-rdight">
            <input type="submit" value="Redeem" class="btn btn-primary" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>

			</div>
		</div>
	</div>

</div>

<?php echo $footer; ?>