<?php echo $header; ?>
<style>
    .wishlist-options {
        text-align: left;
    }
    .wishlist-option-value {
        padding-left: 20px;
    }
    @media(max-width:1000px) {
        .wishlist-option-value {
            padding: 0;
        }
    }
    .breadcrumb {
        margin-bottom: 0!important;
    }
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container" style="background-color:#fafbfd; width:auto;">
	<div class="container" style="background-color:#fff;">
		<div class="row account_dashboard new_account_dashboard" style="margin-top: 0!important;">
			<aside id="column-left" class="col-sm-3 hidden-xs">

				<div class="admin_wrapper">
					<div class="admin_images_section">
						<div class="images_border">
							<img alt="True Elements" id="changeprofilepic" src="/image/acoount.jpg" class="img-responsive">
						</div>	
						<h3><?php  echo $name; ?></h3>
						<p><?php if (isset($email)) { echo $email; } ?></p>
					</div>
					<div class="clear"></div>
				</div>
				<div class="list-group ad_my_account">
                  <a href="/index.php?route=account/account" class="list-group-item"><i class="fa fa-user"></i> My Account</a>
                  <a href="/index.php?route=account/edit" class="list-group-item"><i class="fa fa-pencil-square-o"></i> Edit Account</a>
                  <a href="/index.php?route=account/password" class="list-group-item"><i class="fa fa-key"></i> Password</a>  
                  <a href="/index.php?route=account/address" class="list-group-item"><i class="fa fa-book"></i> Address Book</a>   
                  <a href="/index.php?route=account/order" class="list-group-item"><i class="fa fa-undo"></i> Order History</a> 
                  <a href="/index.php?route=account/wishlist" class="list-group-item"><i class="fa fa-heart"></i> Wish List</a>
                  <a href="/index.php?route=account/reward" class="list-group-item"><i class="fa fa-shield"></i>True Cash</a>
	            </div>

			</aside>

			<div id="content" class="col-sm-9">
				<div class="row" style="margin-top: 5px!important;"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div ><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($products) { ?>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-center"><?php echo $column_image; ?></td>
              <td class="text-left"><?php echo $column_name; ?></td>
              <td class="text-right"><?php echo $column_stock; ?></td>
              <td class="text-right"><?php echo $column_price; ?></td>
              <td class="text-right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td class="text-center"><?php if ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                <?php } ?></td>
              <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?><?php if($product['custom_name']) { echo " - ",$product['custom_name']; } ?></a>
                <?php if($product['options']) { ?>
                    <p class="wishlist-options">
                        <?php $duplicate = ''; ?>
                        <?php foreach($product['options'] AS $option) { ?>
                            <?php if($duplicate != '') { ?>
                                <br/>
                            <?php } ?>
                            <?php if($option['option_name'] == $duplicate) { ?>
                                <small class="wishlist-option-value">- <?php echo $option['option_value']; ?></small>
                            <?php } else { ?>
                                <small><b><?php echo $option['option_name']; ?>:</b></small><br/><small class="wishlist-option-value">- <?php echo $option['option_value']; ?></small>
                            <?php } ?>
                            <?php $duplicate = $option['option_name']; ?>
                        <?php } ?>
                    </p>
                <?php } ?>
              </td>
              <td class="text-right"><?php echo $product['stock']; ?></td>
              <td class="text-right"><?php if ($product['price']) { ?>
                <div class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <b><?php echo $product['special']; ?></b> <s><?php echo $product['price']; ?></s>
                  <?php } ?>
                </div>
                <?php } ?></td>
              <td class="text-right"><button type="button" onclick="addtocart('<?php echo $product['wishlist_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_cart; ?>" class="btn btn-primary"><i class="fa fa-shopping-cart"></i></button>
                <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-times"></i></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>

			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
    function addtocart(wishlist_id) {
        $.ajax({
			url: 'index.php?route=account/wishlist/addtocart&wishlist_id=' + wishlist_id,
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				$('#cart > button').button('reset');

				if (json['success']) {
					var success_text = '<div style="margin: 0 0 10px 0"><div style="float: left"><img style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div style="text-align: center" >' + json['success'] + '</div></div>';

                    var np = new Noty({
                        type: "success",
                        layout: json["notice_add_layout"],
                        text:  success_text,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json["notice_add_timeout"],
                        progressBar: false,
                        closeWith: "button"
                    }).show();
			    	
				    $("#cart-total").html(json["total"]);
				    
				    $("#cart > .top-cart-contain ul").load("index.php?route=common/cart/info ul li");
				}
			}
		});
	}
</script>
<?php echo $footer; ?>