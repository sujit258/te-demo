<?php echo $header; ?>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
     <br /><br />
     <div class="row">
     <div class="col-md-4 col-xs-1"></div>
     <div class="col-md-4 col-xs-10"> 
    <center>
        <h3><span style="font-size: 6em;font-weight: bold;">404</span></h3>
   
      <p style="text-transform: uppercase;font-size: 2em;font-weight: lighter;letter-spacing: 16px;">The Page</p>
      <p style="text-transform: uppercase;font-size: 1.2em;margin: 1em 0;font-weight: lighter;letter-spacing: 4px;color: #777;">Was Not Found</p>
      
      
      <div class="buttons clearfix">
        <a href="<?php echo $continue; ?>" class="btn btn-primary" style="background: #feac00;border: 2px solid #feac00; padding: 11px 30px;"><?php echo $button_continue; ?></a> 
      </div>
    </center>
      </div>
      <div class="col-md-4 col-xs-1"></div>
     </div>
     <?php echo $content_bottom; ?>
      </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
