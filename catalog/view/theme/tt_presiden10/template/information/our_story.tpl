<?php echo $header; ?>
<style>

p {
    font-size: 15px;
    line-height: 28px;
    text-align: justify;
}
    #pt_custommenu {
    width: 121%;
    }
    
    html,body{
overflow-x:hidden;
}

.nature{
top: -77px;
}

@media(max-width:590px){
    .nature { top: 18px;}
}

.only{
top: -123px;
}

@media(max-width:590px){
    .only { top: 0px;}
    
}

.earth{
top: -124px;
}

@media(max-width:590px){
    .earth { top: 0px;}
    
}

.tea{
top:  -77px;
}

@media(max-width:590px){
    .tea { top: 0px;}
}

 .sujitr {  padding-left: 165px;}
   
   @media(max-width:590px){
    .sujitr { padding-left: 28px;
        padding-right: 28px;
    }}
    
    
    .sujitt {  padding-left: 165px;
               padding-right: 165px;
            }
    
    @media(max-width:590px){
    .sujitt { padding-left: 28px;
              padding-right: 28px;
    }
    }
    .heading3 {
        font-size: 17px!important;
        font-weight: 600!important;
    }
    .heading4 {
        font-size: 15px!important;
        font-weight: 600!important;
    }
    .pink {
        color: #feac00;
    }
    .page-heading {
        font-size: 33px !important;
        margin-left: 165px;
    }
    @media(max-width:800px) {
        .page-heading {
            margin-left: 28px;
        }
    }
</style>
<link rel='stylesheet' id='theme-custom-styles-css'  href='http://true-elements.com/catalog/view/javascript/about/bundle.css' type='text/css' media='all' />

<script type='text/javascript' src='http://true-elements.com/catalog/view/javascript/about/jquery.js'></script>

<script type='text/javascript' src='http://true-elements.com/catalog/view/javascript/about/jquery.viewportchecker.min.js'></script>


<script type='text/javascript' src='http://true-elements.com/catalog/view/javascript/about/select2.min.js'></script>

<script type='text/javascript' src='http://true-elements.com/catalog/view/javascript/about/functions.js'></script>

  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>


<div class="row">
    <h1 class="page-heading"><span class="pink">Our </span>Story</h1>
    <div class="hidden-xs-sm visible-lg visible-md">
    <br><br><br><br>
    </div>
    <div class="col-md-8 col-sm-12 col-xs-12 sujitr" >
        	<h3 class="heading3">True to Nature!</h3>
        <p> As it’s rightly said by Shakespeare, “To thine own self be true”, we start with what works best for you, i.e. what is true to you!
            We purely believe in putting nature’s goodness into our packets, one which makes every human body  feel fresh and energised. That’s why our products include ingredients in their natural form, purely to experience nature’s goodness and having the same natural taste within while leaving your taste buds wanting for more.</p>





<div class="buttons">
    <a href="/our-products" type="button" class="btn btn-primary" style="display:inline-block; width: 90px;">Shop</a>
                            </div>
        	
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12 nature">
        <img style="" src="/image/catalog/about/true_to_nature2.png">
    </div>
    
</div>



<div class="row">
    <div class="col-md-4 hidden-xs-sm visible-lg visible-md">
        <img style=" padding-left: 56px;" src="/image/catalog/about/natural_ingredients2.png">
    </div>
     <div class="hidden-xs-sm visible-lg visible-md">
    <br><br><br><br><br><br>
    </div>
     <div class="hidden-md-lg visible-xs visible-sm">
        <br><br>
    </div>
    <div class="col-md-8 col-sm-12 col-xs-12 sujitt" >
        	<h3 class="heading3">100% Natural Ingredients<span style="color: #f0483e;">!</span></h3>
<p>We are a 100% natural food brand and goes without saying that we do not add even an ounce of chemicals. While you go through any of our products’ nutritional label, you will see all possible information about the elements in it. We sincerely do not hide or skip anything on our labels. We believe to provide information to you with nothing more or less on our labels, staying as True to Nature as possible. </p>
        	
    </div>
      <div class="col-sm-12 col-xs-12 hidden-md-lg visible-xs visible-sm">
        <img style=" padding-left: 56px;" src="/image/catalog/about/natural_ingredients2.png">
    </div>
</div>


<div class="row">
     <div class="hidden-xs-sm visible-lg visible-md">
    <br><br><br><br><br><br><br><br>
    </div>
     <div class="hidden-md-lg visible-xs visible-sm">
        <br><br>
    </div>
    <div class="col-md-8 col-sm-12 col-xs-12 sujitr" >
        		<h3 class="heading3">Only Food, No Preservatives<span style="color: #f0483e;">:</span></h3>
<p>Our products are made with FOOD and nothing else. No preservatives, no artificial colours or added flavours. Our products are manufactured in such a way that children can dig in and snack on whenever they feel like, and parents need not to skip a heartbeat worrying about their health. The pest control activities in the factory are completely natural and without the usage of any chemicals, making us do whatever it takes to stay True to You.</p>
        </div>
    <div class="col-md-4 col-sm-12 col-xs-12 only" >
              <img style="" src="/image/catalog/about/only_food_no_preservatives3.png">
    </div>
    
</div>


<!--div class="row">
    <div class="col-md-4 hidden-xs-sm visible-lg visible-md">
        <img style="padding-left: 27px;" src="/image/catalog/about/tea_leaves2.png">
    </div>
     <div class="hidden-xs-sm visible-lg visible-md">
    <br><br><br>
    </div>
     <div class="hidden-md-lg visible-xs visible-sm">
        <br><br>
    </div>
    <div class="col-md-8 col-sm-12 col-xs-12 sujitt" >
        	<h3 class="heading3">Natural Tea Leaves,</h3>
<h4 class="heading4">Straight from the Mountains of Himalayas<span style="color: #f49a25;">:</span></h4>
<p>Packed with flavonoids, B-Complex vitamins and minerals, our Darjeeling Black Teas are totally unique, providing exactly the same natural elements that should go into the tea leaves, before putting it through any type of human intervention. Providing you with the same natural taste of the Himalayan tea leaves, we want our customers to enjoy the exotic tea ranges from nature’s best tea productions, Himalayas.</p>
    </div>
    
        
        
        <div class="col-sm-12 col-xs-12 hidden-md-lg visible-xs visible-sm">
        <img style="padding-left: 27px;" src="/image/catalog/about/tea_leaves2.png">
    
    </div>
</div-->

<div class="row">
    <div class="col-md-4 hidden-xs-sm visible-lg visible-md">
        <img style="padding-left: 27px;" src="/image/catalog/about/care_for_mother_earth2.png">
    </div>
    <div class="hidden-xs-sm visible-lg visible-md">
        <br><br><br><br><br><br>
    </div>
     <div class="hidden-md-lg visible-xs visible-sm">
        <br><br>
    </div>
    <div class="col-md-8 col-sm-12 col-xs-12 sujitt" >
        	<h3 class="heading3">We Care for our Mother Earth<span style="color: #f0483e;">:</span></h3>
        	<p>There are many small, easy ways to contribute towards saving the planet. One such step we inculcate in our productions is the usage of boxes for packing our Honey bottles. Rather than using thermocol boxes, we opt for using carton boxes of higher thickness made from recycled paper, thus saving the earth from the burden of plastic. The containers used for our range of Muesli are purely made of recycled cardboard box, which is 100% biodegradable. Saving the earth from 5 tons of plastic being dumped by using recyclable metalized polyester packaging is what we truly believe in, thereby showing our love towards the nature. </p>
    </div>
    <div class="col-sm-12 col-xs-12 hidden-md-lg visible-xs visible-sm">
        <img style="padding-left: 27px;" src="/image/catalog/about/care_for_mother_earth2.png">
    </div>
    
</div>
<?php echo $footer; ?>