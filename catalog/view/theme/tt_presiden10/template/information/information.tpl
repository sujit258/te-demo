<?php echo $header; ?>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container">
  <div class="row" style="margin-top: 0!important;"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
     <!-- <h1><?php echo $heading_title; ?></h1> -->
      <?php echo $description; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script>
    $(document).ready(function() {
        $(".button-1").click(function() {
            var offsetValue = 250;
            $("html, body").animate({
              scrollTop: $("#story-of-true").offset().top - offsetValue
            }, 1000);
        });
        
        $('.button-2').click(function() {
            var offsetValue = 250;
            $("html, body").animate({
              scrollTop: $("#te-true-you").offset().top - offsetValue
            }, 1000);
        });
        
        $('.button-3').click(function() {
            var offsetValue = 250;
            $("html, body").animate({
              scrollTop: $("#health-foods").offset().top - offsetValue
            }, 1000);
        });
    });
</script>
<?php echo $footer; ?>