<?php echo $header; ?>
<style type="text/css">
  .pink{
    color: #feac00;
  }
  .mt-5{
   margin-top: 3rem !important;
 } 
p {
  font-size: 15px;
  color: #000;
  line-height: 1.8em;
  text-align: justify;
}

/*--/contact-- */
.contact-hny-form input,
.contact-hny-form textarea {
  padding: 13px 15px;
  border: 0;
  border: none;
  outline: none;
  background: #fff;
  border-radius: 3px;
  font-size: 13px;
  letter-spacing: 0.4px;
  -webkit-border-radius: 3px;
  -o-border-radius: 3px;
  -moz-border-radius: 3px;
  -ms-border-radius: 3px;
  border: 1px solid #F2F2F2;
  width: 100%;
}

.form-group label {
  color: #000;
  font-size: 15px;
}
.contact-hny-form textarea {
  min-height: 257px;
}

.map-w3pvt {
  background: #d7d7d7;
  padding: 0.5em;
  margin-top: 2.5rem important;
}

.map-w3pvt iframe {
  width: 100%;
  min-height: 400px;
  border: none;
}
.form-control {
    background: #fff;
    border: 1px solid #f2f2f2;
    border-radius: 3px!important;
    padding: 20px 12px!important;
    color: #000!important;
}
.form-control:focus {
    border-color: #80bdff!important;
    outline: 0!important;
    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25)!important;
}
#button-send {
    border-radius: 4px!important;
}
h3 {
    font-size: 17px!important;
    font-weight: 600!important;
}
.data-container {
    margin-left: 0px!important;
    margin-right: 0px!important;
}
.text-danger {
    font-size: 13px !important;
}
.text-data {
    padding-left: 0;
}
@media(max-width: 1000px) {
    .text-data, .address {
        padding-left: 0;
        padding-right: 0;
    }
    .p-m-0 {
        padding: 0!important;
    }
}
.social-buttons ul {
    padding: 0!important;
}
.social-buttons li {
    display: inline-block;
        margin-right: 10px;
}
.social-buttons li a {
    display: inline-block;
    position: relative;
    line-height: 38px;
    width: 40px;
    height: 40px;
    text-align: center;
    letter-spacing: 0;
}
.address-mobile, .address-email {
    margin-bottom: 0!important;
}
</style>
<ul class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  <?php } ?>
</ul>
<div class="container">
  <div class="row data-container">
   <h1><span class="pink">Contact</span> Us</h1>
   <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 text-data" style="padding-left: 0;">
        <p class="mt-5 pr-lg-5">What you see around here is part of our world that is Made of Truth – Not a world full of lies, jargon or False Promises. Where what is written on the pack is what's in it, and vice versa.<br/>Food ranging from ‘Nashta’ to ‘Tiffin’, from Breakfast to Snacks.</p>
        
   </div>
   <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 address">
       <p class="mt-5 pr-lg-5"><label><strong>Address :&nbsp;</strong></label>Sr no 246/6, Phase 2 Road Hinjawadi,<br>Opp. Naples &amp; Staples Pizza, Next to KTA Spindle Toolings,<br>Pune - 411057 Maharashtra</p>
        <?php if($store_telephone) { ?>
            <p class="address-mobile"><label><strong>Mobile :&nbsp;</strong></label><a href="tel:+<?php echo preg_replace('/[^0-9]/', '', $store_telephone); ?>"><?php echo $store_telephone; ?></a></p>
        <?php } ?>
        <?php if($store_email) { ?>
            <p class="address-email"><label><strong>Email :&nbsp;</strong><a href="mailto:<?php echo $store_email; ?>"><?php echo $store_email; ?></a></label></p>
        <?php } ?>
       
        
   </div>
   <div class="col-md-offset-6 col-md-6 col-xs-12 col-sm-12 social-buttons p-m-0">
        <ul>
            <?php if($whatsapp_number) { ?>
                <li class="hidden-md hidden-xl hidden-lg">
                    <a id="button-wa" class="waorder" href="whatsapp://send?phone=<?php echo $whatsapp_number; ?>&text=Hi">
                        <img alt="True Elements" src="image/catalog/images-08-19/svg-icons/whatsapp-product-page.svg" style="height: 40px;">
                    </a>
                </li>
            <?php } ?>
	        <li><a href="https://www.messenger.com/t/trueelements.in" target="_blank"><img alt="True Elements" src="image/catalog/images-08-19/svg-icons/facebook.svg" class="img-responsive"></a></li>
            <li><a href="https://www.instagram.com/true.elements/" target="_blank"><img alt="True Elements" src="image/catalog/images-08-19/svg-icons/instagram.svg" class="img-responsive"></a></li>
	        <li><a href="https://twitter.com/trueelements" target="_blank"><img alt="True Elements" src="image/catalog/images-08-19/svg-icons/twitter.svg" class="img-responsive"></a></li>
            <li><a href="https://www.linkedin.com/company/13197183/" target="_blank"><img alt="True Elements" src="image/catalog/images-08-19/svg-icons/linkedin.svg" class="img-responsive"></a></li>
        </ul>
   </div>
 </div>
</div>    
<!-- Contact -->
<section class="about-info py-5 px-lg-5">
  <div class="content-w3ls-inn px-lg-5">
    <div class="container py-md-5 py-3">
      <div class="px-lg-5">

        <div class="contact-hny-form mt-lg-5 mt-3">
          <h3 class="title-hny mb-lg-5 mb-3">Drop a Line</h3>

          <div class="col-md-6 col-sm-12 col-xs-12 p-m-0">
            <div class="caption">
             <div style=" padding-top: 20px; margin-bottom: 20px;">
               <form id="form-enquiry" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div id="success"></div><div id="not_success"></div>

                <div class="form-group required">
                  <label><?php echo $entry_name; ?></label>
                  <input type="text" class="form-control" name="name" value="<?php echo $name; ?>" >
                  <?php if ($error_name) { ?>
                  <div class="text-danger"><?php echo $error_name; ?></div>
                  <?php } ?>
                </div>

                <div class="form-group required">
                  <label>Your Email</label>
                  <input type="text" class="form-control" name="email" value="<?php echo $email; ?>" >
                  <?php if ($error_name) { ?>
                  <div class="text-danger"><?php echo $error_email; ?></div>
                  <?php } ?>
                </div>

                <div class="form-group required"> 
                 <label><?php echo $entry_enquiry; ?></label>  
                 <textarea name="enquiry" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
                 <?php if ($error_enquiry) { ?>
                 <div class="text-danger"><?php echo $error_enquiry; ?></div>
                 <?php } ?>
               </div>
               <?php if ($site_key) { ?>
                    <div class="form-group" align="center">
                        <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                        <?php if ($error_captcha) { ?>
                            <div class="text-danger"><?php echo $error_captcha; ?></div>
                        <?php } ?>
                    </div>
                <?php } ?>
               <div class="buttons clearfix">                
                <button type="submit" id="button-send" data-loading-text="Sending" class="button">Submit</button>                   
              </div>

            </form></div>
          </div></div>


          <div class="col-md-6 col-sm-12 col-xs-12 p-m-0">
            <div class="map-w3pvt mt-5">
              <!--<iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=TrueElements&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>-->
              <iframe width="600" height="500" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d472.6909080682018!2d73.72362220300667!3d18.595339610360636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bbb8825c3e55%3A0x32aac7cd97a4ecf8!2zMTjCsDM1JzQzLjYiTiA3M8KwNDMnMjYuOSJF!5e0!3m2!1sen!2sin!4v1567666032965!5m2!1sen!2sin" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
              
              
            </div>
          </div>

        </div>
      </div>
    </div>


  </div>



</section>
<!-- //Contact -->

<!-- /news-letter -->

<!-- //news-letter -->

<!-- footer -->
<script type="text/javascript">
$('#button-send').on('click', function() {
	$("#").submit();
});
</script>
<?php echo $footer; ?>
