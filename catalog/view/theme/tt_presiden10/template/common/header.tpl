<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="msvalidate.01" content="B5E43AE553166C79F84EC456B18D730B" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
 

<!-- Twitter Card data -->
  <meta name="twitter:card" content="<?php echo $keywords; ?>">
  <meta name="twitter:site" content="https://www.true-elements.com">
  <meta name="twitter:title" content="<?php echo $title; ?>">
  <meta name="twitter:description" content="<?php echo $description; ?>">
  <meta name="twitter:creator" content="@trueelements">
  <!--Twitter Summary card images must be at least 120x120px -->
  <meta name="twitter:image" content="https://www.true-elements.com/image/catalog/brands/te-logo.svg">


<meta name="google-signin-client_id" content="688467726932-ftoln2tngeqru6ahv73sqh2rlg1aeq8k.apps.googleusercontent.com">
<meta name="google-signin-client_id" content="4893139527-oara2pq46pe56oqgcdptlqlt28un0ppb.apps.googleusercontent.com">
  
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery-ui.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/css/jquery-ui.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/javascript/material-design-iconic-font/css/material-design-iconic-font.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700,700i" rel="stylesheet" type="text/css">
<link href="catalog/view/theme/tt_presiden10/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/tt_presiden10/stylesheet/animate2.css" rel="stylesheet">
<script src="catalog/view/javascript/opentheme/ocslideshow/jquery.nivo.slider.js" type="text/javascript"></script>
<link href="catalog/view/theme/tt_presiden10/stylesheet/opentheme/ocslideshow/ocslideshow.css" rel="stylesheet" />
<link href="catalog/view/theme/tt_presiden10/stylesheet/opentheme/css/animate.css" rel="stylesheet" />
<script src="catalog/view/javascript/opentheme/hozmegamenu/custommenu.js" type="text/javascript"></script>
<script src="catalog/view/javascript/opentheme/hozmegamenu/mobile_menu.js" type="text/javascript"></script>
<link href="catalog/view/theme/tt_presiden10/stylesheet/opentheme/hozmegamenu/css/custommenu.css" rel="stylesheet" />
<link href="catalog/view/theme/tt_presiden10/stylesheet/opentheme/css/owl.carousel.css" rel="stylesheet" />
<script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.js" type="text/javascript"></script>
<script src="catalog/view/javascript/opentheme/ocquickview/ocquickview.js" type="text/javascript"></script>
<link href="catalog/view/theme/tt_presiden10/stylesheet/opentheme/ocquickview/css/ocquickview.css" rel="stylesheet">
<link href="catalog/view/theme/tt_presiden10/stylesheet/opentheme/oclayerednavigation/css/oclayerednavigation.css" rel="stylesheet">
<script src="catalog/view/javascript/opentheme/oclayerednavigation/oclayerednavigation.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/elevatezoom/jquery.elevatezoom.js" type="text/javascript"></script>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

<script type="text/javascript" src="//platform.linkedin.com/in.js" async>
    api_key: 81ij4j6wk1ypum
    authorize: false
    onLoad: onLinkedInLoad
</script>

<script type="text/javascript">
var gr_goal_params = {
 param_0 : '',
 param_1 : '',
 param_2 : '',
 param_3 : '',
 param_4 : '',
 param_5 : ''
};</script>
<script type="text/javascript" src="https://app.getresponse.com/goals_log.js?p=1090504&u=hq4kQ"></script>

<script>!function(e,t,a){var c=e.head||e.getElementsByTagName("head")[0],n=e.createElement("script");n.async=!0,n.defer=!0, n.type="text/javascript",n.src=t+"/static/js/chat_widget.js?config="+JSON.stringify(a),c.appendChild(n)}(document,"https://app.engati.com",{bot_key:"66fde9ce1a074f45",welcome_msg:true,branding_key:"default",server:"https://app.engati.com",e:"p" });</script>


    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/364df4ab8ee08474e55678d2e/c0e9bfdbbda83a5380051e3ba.js");</script>
 
  
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '355421528440199');
  fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=355421528440199&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<script language="javascript">
//paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");
	});
</script>
<!--  loading page -->


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '2044791438972837'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=2044791438972837&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->


<style>
/*#pt_menu533 .parentMenu a {
    background: #f49a25;
    color: #fff;
    border-radius: 7px;
    padding: 6px 24px;
}
#pt_menu533 .parentMenu a:hover {
    background: #000;
}*/
@media(max-width: 1000px) {
    .mobilerow {
        /*margin-top: -5%;*/
        margin-top: -2%;
    }
    /*.top-cart {
        padding-top: 41px!important;
    }
    .header-link {
        padding-top: 47px!important;
    }
    .header-search {
        padding-top: 44px!important;
    }*/
    .top-cart {
        padding-top: 20px!important;
        margin-right: -5%;
    }
    .header-link {
        padding-top: 25px!important;
    }
    .header-search {
        padding-top: 20px!important;
    }
    .mt-0 {
        margin-top: 0!important;
    }
    .navbar-toggle {
        padding: 15px 16px!important;
    }
    .logo {
        margin-top: 10px !important;
    }
    .header-link .header-content .box-content {
        /*top: 76px!important;*/
        top: 47px!important;
    }
    #cart .dropdown-menu {
        /*top: 116%!important;*/
        top: 100%!important;
    }
    .search-box {
        top: 50px;
        right: -135px;
    }
}
.fix-nav .dropdown-menu {
    top: 97%!important;
}
.fix-nav .box-content {
    margin-top: 30px!important;
}
.header-container .nav-container {
    padding-top: 5px;
}
    .ajax-result-msg {
        margin: 10px 20px 10px!important;
    }
    .right-arrow {
        color: #fff;
        font-size: 12px !important;
    }
    .mobilemenu li {
        width: 100%!important;
    }
    
    .fa {
    font-size: 15px !important;
    color: #fff;
    }

@media(max-width:590px){.logoa {/*margin-top: 35%;*/margin-top: 0%;margin-left: 7%;}}

@media(min-width:591px){.logoa {margin-top:0%;}}

.te-sj .btn-success, .te-sj .btn-success:hover, .te-sj .btn-success:focus, .te-sj .btn-success:active  {
    background-color: #f49a25;
    border: none!important;
}
.te-sj {
    z-index: 10;
}
a:focus {
    color: inherit;
    text-decoration: none;
}
.facebook a, .twitter a, .youtube a, .linkedin a, .instagram a, .blogger a {
    border: 1px solid #fff;
    color: #fff;
}
#subscribe-normal::placeholder {
  color: #fff;
}
.footer-static .footer-content .social-footer li a {
    border: none !important;
}
.footer-static .footer-content .social-footer li a:hover {
    background: none !important;
    border-color: none !important;
}
.preview__header {
    font-size: 12px;
}
.preview__header a:focus {
    color: #f49a25;
}
.btn-primary:active, .btn-primary:focus, .btn-primary:active:focus {
    background: #f49a25;
    color: #fff;
}
.social-title ul li {
    width: 10%!important;
}
.header-shop {
    width: 50px;
    height: 50px;
    padding-top: 22px;
    float: right;
}
#top .box-top {
    width: 200px!important;
}
.header-container .nav-container {
    margin-left: 40px!important;
}

@media (max-width: 850px) and (min-width: 700px){
   header nav .container .row.mobilerow .col-sm-2{
        margin-right: -4% !important;
    }

    header nav .container .row.mobilerow  .col-sm-7 .box-top .top-cart{
        margin-right: -22% !important;
    }
}
</style>
</head>
<body class="<?php echo $class; ?>">
    <!-- Preloader loading page -->
<div class="se-pre-con"></div>
<!--  loading page -->
    <div class="preview__header sujit">
            <div class="col-md-8" style="margin: 0 3% 0 -3%; padding: 0;"><?php if($store_telephone) { ?><i class="fa fa-phone-square" style="font-size: 17px !important;margin-left: 60px; font-size: 15px; color: #fff; !important;"></i><span style="color:#f49a25; font-size: 13px;font-weight: 600; ">&nbsp;&nbsp;<?php echo $store_telephone; ?></span>&nbsp;&nbsp;<?php } ?><?php if($store_email) { ?><i class="fa fa-envelope" style="font-size: 15px !important;color: #fff;"></i><span style="color:#f49a25; font-size: 13px;font-weight: 600; ">&nbsp;&nbsp;<?php echo $store_email; ?></span> &nbsp;&nbsp;<?php } ?><i class="fa fa-truck" style="font-size: 15px !important; color: #fff; -ms-transform: scaleX(-1); -webkit-transform: scaleX(-1); transform: scaleX(-1);"></i><span style="color:#f49a25;font-size: 13px;font-weight: 600;">&nbsp;&nbsp;Get Free Shipping across India using code: FREESHIPPING</span></div>
            <div class="col-md-4" style="padding: 0;"><div style="margin-left: 20%;"><a href="/recycle" style="font-size: 13px;font-weight: 600;"><i class="fa fa-recycle" style="font-size: 15px !important; color:#fff;"></i><span style="font-size: 13px;font-weight: 600;color:#f49a25;">&nbsp;&nbsp;Let's Recycle&nbsp;&nbsp;</span></a> <a id="pincheck-bookmark" style="font-size: 13px;font-weight: 600;cursor: pointer;"><i class="fa fa-map-marker" style="font-size: 15px !important; color:#fff!important;"></i><span style="font-size: 13px;font-weight: 600;color:#f49a25;">&nbsp;&nbsp;Do you deliver at..&nbsp;&nbsp;</span></a> <a href="index.php?route=information/shipway_track" style="color:#fff; font-size: 13px; z-index: 10;"><i class="fa fa-truck" style="font-size: 15px !important;color: #fff; -ms-transform: scaleX(-1); -webkit-transform: scaleX(-1); transform: scaleX(-1);"></i>&nbsp;&nbsp;<span style="font-size: 13px;font-weight: 600;color:#f49a25;">Track order</span></a></div></div>
    </div>
    <div class="preview__header hidden-lg hidden-md hidden-xl" style="line-height: 15px!important;">
            <div class="col-xs-12" style="margin-top: 7px; text-align: center;">
                <i class="fa fa-truck" style="font-size: 15px !important; color: #fff; -ms-transform: scaleX(-1); -webkit-transform: scaleX(-1); transform: scaleX(-1);"></i><span style="color:#f49a25;font-size: 12px;font-weight: 600;">&nbsp;&nbsp;Free Shipping in India use Code: FREESHIPPING</span>
            </div>
    </div> 
    
<header>

    
<nav id="top">
	<div class="container" style="width:auto!important;"> 
		<div class="row mobilerow">
			<div class="col-lg-1 hidden-md hidden-sm hidden-xs " style="margin-top: -13px;margin-left: 34px;">
				<div class="logo" style="padding-top: 10px;margin-left: 12px;margin-bottom: -10px;">
				  <?php if ($logo) { ?>
				  <a href="<?php echo $home; ?>"><img src="/image/catalog/brands/te-logo.svg" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
				  <?php } else { ?>
				  <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
				  <?php } ?>
				</div>
			</div>
		
			<div class="col-lg-9 col-md-3 col-sm-2 col-xs-3 mt-0" style="margin-right: -7%;">
			<div class="row mt-0">	<div class="header-container col-xs-12" style="margin-top: 0px;">
					<?php if(isset($block1)){ echo $block1; }?>
				</div></div>
			</div>
		
			<div class="col-md-3 col-sm-3 col-xs-3 hidden-lg" style="margin-left: -30px;">
						<div class="logo">
				  <?php if ($logo) { ?>
				  <a href="<?php echo $home; ?>"><img src="/image/catalog/brands/te-logo.svg" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive logoa" /></a>
				  <?php } else { ?>
				  <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
				  <?php } ?>
				</div>
			</div>
			<!--div class="col-xs-2"></div-->
			
			
			<div class="col-lg-2 col-md-6 col-sm-7 col-xs-6">
						    
				<div class="box-top" style="margin-top: 2%;">
				
				
				    <div class="top-cart">
							<?php echo $cart; ?>
					</div>
						
						
					<div class="header-link">
						<div class="header-content">
							<div class="icon-link"></div>
								<div class="box-content" style="border: 1px solid rgb(240, 240, 240);margin-top: 24px;">
								    
									<div id="top-links">
									    
										<div class="box-link">
										<ul class="list-inline links">
									
										<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>"><span><?php echo $text_account; ?></span></a></li>
									
										<li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><span><?php echo $text_shopping_cart; ?></span></a></li>
									
										<li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><span ><?php echo $text_checkout; ?></span></a></li>
									
										 <?php if ($logged) { ?>		
										<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?> <i class="fa fa-sign-out fa-lg"></i></a></li>
												<?php } ?>
										
										
										</ul>
									</div>
										
								</div>
								 
							</div>
						</div>
					</div>
						<div class="header-shop hidden-md hidden-lg hidden-xl">
						    <a href="/all-products"><img src="/image/catalog/our-store-icon.png" style="width: 50px;" alt="Our Store" /></a>
						</div>
						
						<div class="header-search">
							<?php echo $search; ?>
						</div>
				</div>
					
       
       
            </div>
<!--div class="col-xs-1"></div-->
		</div>
	</div>
</nav>
<!--a href="index.php?route=information/shipway_track" style="position: fixed;bottom: 40%;right:0; z-index: 10;"><img src="https://www.true-elements.com/image/icons/track_order.svg" style="height: 130px;width: 26px;"></a-->
  
</header>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "True Elements",
    "url": "http://www.true-elements.com",
    "address": "Sr no 246/6, Phase 2 Road Hinjawadi, Opp. Naples & Staples Pizza, Next to KTA Spindle Toolings, Pune - 411057 Maharashtra",
    "sameAs": [
      "https://www.facebook.com/trueelements.in",
      "https://twitter.com/trueelements",
      "https://www.instagram.com/true.elements/",
      "https://www.linkedin.com/company/13197183/"
    ]
  }
</script>
<?php if(isset($block2)){ echo $block2; }?>
<script type="text/javascript">
$(document).ready(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() > 47) {
		$('header').addClass("fix-nav");
		} else {
		$('header').removeClass("fix-nav");
		}
	});
	$(function dropDown()
	{
		elementClick = '.header-link .icon-link,#search-by-category .icon-search,#cart .btn-cart';
		elementSlide =  '.dropdown-menu,.search-box,.box-content';
		activeClass = 'active';

		$(elementClick).on('click', function(e){
		e.stopPropagation();
		var subUl = $(this).next(elementSlide);
		if(subUl.is(':hidden'))
		{
		subUl.slideDown();
		$(this).addClass(activeClass);
		}
		else
		{
		subUl.slideUp();
		$(this).removeClass(activeClass);
		}
		$(elementClick).not(this).next(elementSlide).slideUp();
		$(elementClick).not(this).removeClass(activeClass);
		e.preventDefault();
		});

		$(elementSlide).on('click', function(e){
		e.stopPropagation();
		});

		$(document).on('click', function(e){
		e.stopPropagation();
		var elementHide = $(elementClick).next(elementSlide);
		$(elementHide).slideUp();
		$(elementClick).removeClass('active');
		});
		
	});
});
</script>