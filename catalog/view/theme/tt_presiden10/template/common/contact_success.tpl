<?php echo $header; ?>
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
<div class="container">

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
        
        <?php echo $content_top; ?>
        
    <div class="col-md-2 col-xs-1">    </div>
   <div class="col-md-8 col-xs-10">   <h1><?php echo $heading_title; ?></h1>
      <?php echo $text_message; ?>
      <div class="buttons">
       
      </div>
      </div>
    <div class="col-md-2 col-xs-1">    </div> 
      
      <?php echo $content_bottom; ?>
      
      
      </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>