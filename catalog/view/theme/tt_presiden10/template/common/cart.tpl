<style>
    .free-delivery-msg p {
        text-align: left!important;
        padding: 0!important;
        margin-bottom: 0!important;
        font-size: 13px!important;
    }
    .free-delivery-msg {
        padding-left: 0!important;
        padding-right: 0!important;
    }
    .button-cart.text-right {
        margin-right: 0!important;
    }
</style>
<div id="cart" class="btn-group btn-block">
	<div class="top-cart-contain">
	<div class="arrow-cart"></div>
	<button type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-cart btn-inverse btn-block btn-lg dropdown-toggle">
		<span id="cart-total" style="width: 18px;height: 18px;line-height: 19px;background: #fb9935;border-radius: 100%;color: #fff;font-size: 10px;font-weight: 400;text-align: center;position: absolute;right: -9px;bottom: -1px;"><?php echo $text_items; ?></span>
	</button>
	<ul class="dropdown-menu pull-right" style="border: 1px solid rgb(240, 240, 240);box-shadow: 0 0 20px 0.4px rgba(0, 0, 0, 0.1);">
	    
	    <div style="justify-content: space-between;border-bottom: 1px solid #cccccc;padding-bottom: 4px;margin-bottom: 12px;">
                                            <h4>Your Cart</h4>
          </div>
                                        
                                        
		<?php if ($products || $vouchers) { ?>
		<li style="overflow-y:scroll; height:150px; width: 100%;">
			<div class="div-sdiviped">
					<?php foreach ($products as $product) { ?>
					<div class="row-cart">
						<div class="image-cart text-center"><?php if ($product['thumb']) { ?>
							<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail hidden-xs hidden-sm" /><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail hidden-md hidden-xl hidden-lg" style="width: 65px!important;" /></a>
							<?php } ?>
						</div>
						<div class="cart-content">
							<div class="product-name text-lef">
								<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
								<?php if ($product['option']) { ?>
								<?php foreach ($product['option'] as $option) { ?>
								    <br />
                                    <?php if($option['name'] == $duplicate) { ?>
                                        <small style="padding-left: 2px;">- <?php echo $option['value']; ?></small>
                                    <?php } else { ?>
                                        <small><b><?php echo $option['name']; ?>:</b></small><small style="padding-left: 2px;">- <?php echo $option['value']; ?></small>
                                    <?php } ?>
                                    <?php $duplicate = $option['name']; ?>
								<?php } ?>
								<?php } ?>
								<?php if ($product['recurring']) { ?>
								- <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
								<?php } ?>
							</div>
							<strong class="text-right"><?php echo $product['quantity']; ?> x</strong>
							<span class="cart-price text-right"><?php echo $product['price']; ?></span >
							<div class="cart-button text-center"><button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i></button></div>
						</div>
					</div>
					<hr style="margin-top: 10px!important; margin-bottom: 10px!important;"/>
					<?php } ?>
					<?php foreach ($vouchers as $voucher) { ?>
					<div class="row1-cart">
					  <div class="text-center"></div>
					  <div class="text-left"><?php echo $voucher['description']; ?></div>
					  <div class="text-right">x&nbsp;1</div>
					  <div class="text-right"><?php echo $voucher['amount']; ?></div>
					  <div class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></div>
					</div>
					<?php } ?>
			</div>
		</li>
		<li>
		<div class="row2-cart  div-bordered">
		    <style>#popup_totals_sub_total,#popup_totals_total_customer_group_discount,#popup_totals_salescombo, #popup_totals_tax { display: none; }
		    #popup_totals_total{text-align: right;}		    </style>
			  <?php foreach ($totals as $total) { ?>
			  <div class="box-cart" id="popup_totals_<?php echo $total['code']; ?>">
				<span class="text text-right"><strong><?php echo $total['title']; ?> : </strong></span>
				<span class="text1 text-right"><?php echo $total['text']; ?></span>
			  </div>
			  <?php } ?>
			  <?php if($free_delivery_value > 0 && strtolower($coupon) != "freeshipping") { ?>
			    <div class="col-md-12 col-xs-12 col-sm-12 free-delivery-msg">
			        <p>You're just Rs.<?php echo $free_delivery_value; ?>/- away from Free Shipping.</p>
			    </div>
			  <?php } ?>
			<div class="button-cart text-right" style="margin-bottom: 5px !important; margin-top: 10px!important; padding: 0px 10px !important;"><a style="float: left;" href="<?php echo $cart; ?>"><strong><?php echo $text_cart; ?></strong></a><a href="<?php echo $checkout; ?>"><strong><i class="fa fa-sign-in"></i> <?php echo $text_checkout; ?></strong></a></div>
		</div>
		</li>
		<?php } else { ?>
		<li>
		  <p class="text-center"><?php echo $text_empty; ?></p>
		</li>
		<?php } ?>
	  </ul>
	</div>
</div>