<?php echo $header; ?>
<style>
    .table-responsive {
        margin-right: 10%!important;
    }
    .table-responsive table{
        width: 80%;
    }
    .table-responsive table thead tr:nth-child(odd) {
        width: 20%;
    }
</style>
<div class="container">
  <!-- <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> -->
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-10'; ?>
    <?php } ?>
    <div class="col-sm-1"></div>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
     <?php if (isset($order_id)) { ?>
    <div class="container">
      <center><span style="font-size: 30px;font-family: rocketclouds;font-weight: 501;color: #000;"></span></center><br /><br />
      <span style="font-size: 30px;font-weight: 501;color: #000;">YOUR  <span style="color: #feac00;">ORDER </span>(#<?php echo $order_id; ?>)</span><br /><br />
      <span style="font-size: 15px;color: #000;">Thanks a lot for letting True Elements be a part of your story. It means a lot to us and we are truly delighted to serve you.</span><br /><br />
      <span style="font-size: 15px;color: #000;">We will try our best to dispatch the order within the next 48 working hours, if not earlier.</span><br />
      <span style="font-size: 15px;color: #000;"><br>
      <?php   $NewDate=Date('F d, Y', strtotime("+" . $expected_delivery)); ;?>
        Expected Delivery: In  <?php echo $NewDate; ?>
          <!--<?php echo $delivery; ?>-->
          </span><br />
      </div>
     <!--  <?php echo $text_message; ?>-->      
     
  <div id="print">
      <br>
      <div class="container">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td class="name t-head">  </td>
                  <td class="name t-head">Product</td>
                  <td class="quantity t-head">Quantity</td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($products as $product) { ?>
                    <tr>
                        <td class="image" >
                            <img src="<?php echo  $product['thumb'] ?>" alt="<?php echo $product['name'] ?>">
                        </td>
                        <td class="name">
                            <?php echo $product['name'] ?><br>
                            <small>
                                <?php foreach ($product['option'] as $option) { ?>
                                    <?php echo $option['value'],"<br/>"; ?>
                                <?php } ?>
                            </small>
                        </td>
                        <td class="quantity"><?php echo round($product['quantity']); ?></td>
                    </tr>
                <?php } ?>
              </table>
              <table id='totals' class='table'>
                <tbody>
                  <!--<?php foreach ($totals as $total) { ?>
                    <tr class="subtotal">
                      <td class="name subtotal"><strong><?php echo $total['title']; ?>:</strong></td>
                      <td class="price"><?php echo $total['text']; ?></td>
                    </tr>
                  <?php } ?> -->
                  
                </tbody>
              </table>
            </div>
      </div>
      <?php if ($comment) { ?>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left">Order Comment</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $comment; ?></td>
          </tr>
        </tbody>
      </table>
      <?php } ?>
  </div>
<?php } else { ?>
    <?php header("Location: http://www.true-elements.com"); ?>
<?php } ?>
      <iframe src="https://tracking.salesleaf.com/aff_l?offer_id=2136&adv_sub=<?php echo $order_id; ?>&amount=<?php echo $total; ?>" scrolling="no" frameborder="0" width="1" height="1"></iframe>
      <!--PIXEL FOR PURCHASE:After Sale-->


<!----Conversion Pixel for Trueelements CPS starts ------->
<iframe src="https://gopaisa.go2pixel.org/c/?offer_id=69&orderAmount=<?php echo $total; ?>&customerGroup=1&orderID=<?php echo $order_id; ?>" height="0" width="0"></iframe>
 <!----Conversion Pixel for Trueelements CPS ends ------->
 
 
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '355421528440199');
  fbq('track', 'Purchase');
</script>
<noscript><img alt="True Elements" height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=355421528440199&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '557130271297891'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img alt="True Elements" height="1" width="1" src="https://www.facebook.com/tr?id=557130271297891&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->


<?php if(isset($products_data) && isset($order_id)) { ?>
    <?php if($shipping == '') { $shipping = 0; } if($tax == '') { $tax = 0; } ?>
    <script type="text/javascript">
    var google_conversion_id = 1000810033;
    gtag('event', 'purchase', {
        "transaction_id": "<?php echo $order_id; ?>",
        "value": "<?php echo $order_total; ?>",
        "currency": "INR",
        "shipping": "<?php echo $shipping; ?>",
        "tax": "<?php echo $tax; ?>",
        "items": [
            <?php foreach($products_data as $product_data) { ?> 
                {
                "id": "<?php echo $product_data['product_id']; ?>",
                "name": "<?php echo html_entity_decode($product_data['name']); ?>",
                "quantity": "<?php echo $product_data['quantity']; ?>",
                "price": "<?php echo $product_data['total']; ?>"
                },
            <?php } ?>
        ]
    });
    </script>
<?php } ?>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="True Elements" src="//www.googleadservices.com/pagead/conversion/1000810033/?value=0.00&amp;currency_code=INR&amp;label=HHhPCLiwp24Qscyc3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

 
 <!-- Event snippet for Thank you Page conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-728346050/XMLMCPbT9KQBEMLbptsC'});
</script>
    
      <?php echo $content_bottom; ?></div>
      <div class="col-sm-1"></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>