
<?php echo $header; ?>
<ul class="breadcrumb">
<?php foreach ($breadcrumbs as $breadcrumb) { ?>
<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
<?php } ?>
</ul>
<div class="container">
<div class="row"><?php echo $column_left; ?>
   <?php if ($column_left && $column_right) { ?>
   <?php $class = 'col-sm-6'; ?>
   <?php } elseif ($column_left || $column_right) { ?>
   <?php $class = 'col-sm-9 col-xs-12'; ?>
   <?php } else { ?>
   <?php $class = 'col-sm-12'; ?>
   <?php } ?>
   
   <?php echo $content_top; ?>
     <?php if ($articles) { ?>
       <?php $i=0; foreach ($articles as $article) { $i++; ?>
       <div class="col-lg-4 col-sm-6 col-xs-12">
    <div class="thumbnail">
  <img class="img-responsive" src="<?php echo $article['image']; ?>" alt="<?php echo $article['name']; ?>"/>
  


<p class="articledate">
<i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $article['date_added']; ?>
<?php if($article['author'] != null && $article['author'] != ""){ ?>  
<?php echo "/ " . $text_post_by . $article['author']; ?>
<?php } ?>
</p>
<div class="caption">

<h3><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h3>

<div class="intro-content"><?php echo $article['intro_text']; ?></div>
<a class="readmore-page " href="<?php echo $article['href']; ?>" >
<?php echo $button_read_more; ?><i class="fa fa-arrow-right" aria-hidden="true"></i>
</a>
</div>  
</div>
</div>
       <?php } ?>
   
     <div class="toolbar4">
       <div class="col-sm-6 show-page"><?php echo $pagination; ?></div>
       <div class="col-sm-6 text-right"><?php echo $results; ?></div>
     </div>
     <?php } ?>
     <?php if (!$articles) { ?>
     <p><?php echo $text_empty; ?></p>
     <?php } ?>
     <?php echo $content_bottom; ?>
   <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>