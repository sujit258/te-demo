<?php
class ModelCatalogMedia extends Model {
	 
	
	public function getLatestMedia($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "media_articles WHERE status = '1' ORDER BY publish_date DESC LIMIT " . (int)$limit);
		return $query->rows;
	}
    
    public function getTotalFilterMedia($data = array()) {
	    
	    $sql = "SELECT  COUNT(m.media_articles_id) AS total  FROM ". DB_PREFIX ."media_articles m";
		    
		    $sql .= " WHERE m.status = '1'";
		    
		    $query = $this->db->query($sql);
		    

		   return $query->row['total'];
	}
	
	
	public function getMediaInfo() {
		$sql = "SELECT * FROM ".DB_PREFIX."media_articles WHERE status = '1' ORDER BY publish_date DESC";
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
}