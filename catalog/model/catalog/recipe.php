<?php
class ModelCatalogRecipe extends Model {
	
	// Recipies
	
	public function getTotalRecipiesCount($data = array()) {
		$sql = "SELECT COUNT(DISTINCT r.recipe_id) AS total FROM " . DB_PREFIX . "recipe r";
		
		if(!empty($data['filter_filter'])) {
		    $sql .= " LEFT JOIN " . DB_PREFIX . "recipe_filter rf ON (r.recipe_id = rf.recipe_id)";
		}
		$sql .= " WHERE r.status = '1'";
		
		if (!empty($data['filter_filter'])) {
			$implode = array();
    		$filters = explode(',', $data['filter_filter']);

			foreach ($filters as $filter_id) {
				$implode[] = (int)$filter_id;
			}
			$sql .= " AND rf.filter_id IN (" . implode(',', $implode) . ")";
		}
		
		if(isset($data['category_id'])) {
			$sql .= " AND r.category_id = '". (int)$data['category_id'] ."'";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getTotalRecipies($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "recipe WHERE status = '1' ";		
		
		if(isset($data['category_id']))
		{
			$sql .= " AND category_id = '". (int)$data['category_id'] ."' ";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getTotalFilterRecipies($data = array()) {
	    
	    $sql = "SELECT  COUNT(r.recipe_id) AS total  FROM ". DB_PREFIX ."recipe r";
			
            if(!empty($data['filter_filter'])) {
		        $sql .= " LEFT JOIN " . DB_PREFIX . "recipe_filter rf ON (r.recipe_id = rf.recipe_id)";
		    }
		    
		    $sql .= " WHERE r.status = '1'";
		    
		    if(isset($data['category_id'])) {
			    $sql .= " AND r.category_id = '". (int)$data['category_id'] ."'";
		    }
		    
		    if (!empty($data['filter_filter'])) {
			    $implode = array();
    		    $filters = explode(',', $data['filter_filter']);

			    foreach ($filters as $filter_id) {
			    	$implode[] = (int)$filter_id;
			    }
			    $sql .= " AND rf.filter_id IN (" . implode(',', $implode) . ")";
	    	}
		    
		    $query = $this->db->query($sql);
		    

		   return $query->row['total'];
	}
	
	public function getRecipies($data = array()) {
			$sql = "SELECT * FROM ". DB_PREFIX ."recipe r";
			
            if(!empty($data['filter_filter'])) {
		        $sql .= " LEFT JOIN " . DB_PREFIX . "recipe_filter rf ON (r.recipe_id = rf.recipe_id)";
		    }
		    
		    $sql .= " WHERE r.status = '1'";
		    
		    if(isset($data['category_id'])) {
			    $sql .= " AND r.category_id = '". (int)$data['category_id'] ."'";
		    }
		    
		    if (!empty($data['filter_filter'])) {
			    $implode = array();
    		    $filters = explode(',', $data['filter_filter']);

			    foreach ($filters as $filter_id) {
			    	$implode[] = (int)$filter_id;
			    }
			    $sql .= " AND rf.filter_id IN (" . implode(',', $implode) . ")";
	    	}
		    
		    $sql .= " ORDER BY r.recipe_id DESC";
		     //$sql .= " ORDER BY r.sort_order";
		     
		    if (isset($data['start']) || isset($data['limit'])) {
			    if ($data['start'] < 0) {
			    	$data['start'] = 0;
			    }

			    if ($data['limit'] < 1) {
			    	$data['limit'] = 12;
			    }
			    $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
	    	}
		    $query = $this->db->query($sql);
		    
		    foreach ($query->rows as $result) {
			    $recipe_data[$result['recipe_id']] = $this->getRecipe($result['recipe_id']);
		    }

		    return $recipe_data;
		}
	
	public function getLatestRecipes($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recipe WHERE status = '1' ORDER BY recipe_id DESC LIMIT " . (int)$limit);
		return $query->rows;
	}
	
	public function getRecipe($recipe_id) {
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."recipe WHERE recipe_id = '". (int)$recipe_id ."' ");
		$result = $query->row;
		return $result;
	}
	
		 // For Search Result in Main Search
	public function getRecipiesList($data) {
		$sql = "SELECT * FROM ". DB_PREFIX ."recipe WHERE status = '1' ";
		
		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";
			if (!empty($data['filter_name'])) {
				$implode = array();
				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));
				
				foreach ($words as $word) {
					$implode[] = "title LIKE '%" . $this->db->escape($word) . "%'";
				}
				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}
			}
			$or = false;
			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
				$or = true;
			}
			if (!empty($data['filter_tag'])) {
				$implode = array();
				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_tag'])));
				foreach ($words as $word) {
					$implode[] = "tag LIKE '%" . $this->db->escape($word) . "%'";
				}
				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}
			}
			$sql .= ")";
		}
						
		$query = $this->db->query($sql);

		return $query->rows;			
	}
	
	//Related Products
	
	// Recipe Related Products
	
	public function getRecipeRelatedProduct($recipe_id) {
		$product_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recipe_related_product WHERE recipe_id = '" . (int)$recipe_id . "'");
		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}
		return $product_data;
	}
	
	// For Fetching Product's complete description
	
	public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return array(
				'product_id'       => $query->row['product_id'],
				'product_type'     => $query->row['product_type'],
				'name'             => $query->row['name'],
				'description'      => $query->row['description'],
				'meta_title'       => $query->row['meta_title'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'special'          => $query->row['special'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating']),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed']
			);
		} else {
			return false;
		}
	}
	
	public function getContestWinnersRecipes($limit = 20) {
	    $query = $this->db->query("SELECT recipe_id, title, author, contest_name, image FROM " . DB_PREFIX . "recipe WHERE is_winner = 1 AND status = 1 LIMIT " . (int)$limit);
	    return $query->rows;
	}
	
	// Recipe Categories
	
	public function getRecipeCategories() {
		$sql = "SELECT * FROM ".DB_PREFIX."recipe_category WHERE status = '1' ";
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getCategory($category_id) {
	    
        $sql = "SELECT * FROM ". DB_PREFIX ."recipe_category WHERE category_id = '". (int)$category_id ."' ";
        
        $query = $this->db->query($sql);
        
        return $query->row;
    }
	
	// Recipe Review
	
	public function getRecipeReviews($recipe_id) {
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."recipe_review WHERE recipe_id = '". (int)$recipe_id ."' ");
		$result = $query->row;
		return $result;
	}
	
	
	public function getTotalReviewsByRecipeId($recipe_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "recipe_review WHERE recipe_id = '". (int)$recipe_id ."' ");

		return $query->row['total'];
	}
	
	public function getReviewsByRecipeId($recipe_id, $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."recipe_review WHERE recipe_id = '". (int)$recipe_id ."' AND status = '1' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}
	
	public function addReview($recipe_id, $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "recipe_review SET author = '" . $this->db->escape($data['name']) . "', customer_id = '" . (int)$this->customer->getId() . "', recipe_id = '" . (int)$recipe_id . "', review = '" . $this->db->escape($data['review']) . "', rating = '" . (int)$data['rating'] . "', date_added = NOW()");

		$review_id = $this->db->getLastId();
	}
	
	public function getRecipesRelatedRecipe($recipe_id) {
		$related_recipe_data = array();
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."recipes_related_recipe WHERE recipe_id = '". (int)$recipe_id ."'");
		foreach ($query->rows as $result) {
		    $recipe_data_query = $this->db->query("SELECT title, description, image,homepage_image FROM ". DB_PREFIX ."recipe WHERE recipe_id = '". (int)$result['related_recipe_id'] ."' AND status = 1");
		    if($recipe_data_query->row) {
		        $related_recipe_data[] = array(
		            'recipe_id' => $result['related_recipe_id'],
		            'title' => $recipe_data_query->row['title'],
		            'description' => $recipe_data_query->row['description'],
		            'image' => $recipe_data_query->row['image'],
		            'homepage_image' => $recipe_data_query->row['homepage_image']
		        );
		    }
		}
		return $related_recipe_data;
	}
	
	public function getRecipeFilters($recipe_id = 0) {
		$implode = array();
        
        if($recipe_id > 0) {
		    $query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "recipe_filter WHERE recipe_id = '" . (int)$recipe_id . "'");
        } else {
            $query = $this->db->query("SELECT DISTINCT rf.filter_id FROM " . DB_PREFIX . "recipe_filter rf LEFT JOIN " . DB_PREFIX . "recipe r ON (rf.recipe_id = r.recipe_id) WHERE r.status = 1");
        }

		foreach ($query->rows as $result) {
			$implode[] = (int)$result['filter_id'];
		}

		$filter_group_data = array();

		if ($implode) {
			$filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

			foreach ($filter_group_query->rows as $filter_group) {
				$filter_data = array();

				$filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

				foreach ($filter_query->rows as $filter) {
					$filter_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name']
					);
				}

				if ($filter_data) {
					$filter_group_data[] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $filter_data
					);
				}
			}
		}

		return $filter_group_data;
	}
}