<?php
class ModelCatalogRecycles extends Model {
	public function addRecycles($customer_id,$data)	{		
		$this->db->query("INSERT INTO " . DB_PREFIX . "recycles SET customer_id = '". (int)$customer_id ."', name = '". $this->db->escape($data['cst_name']) ."', email = '". $this->db->escape($data['cst_email']) ."', mobile = '". $this->db->escape($data['cst_mobile']) ."', address = '". $this->db->escape($data['cst_address']) ."', total_packets = '". (int)$this->db->escape($data['cst_no_of_packets']) ."', request_date = NOW(), pickup_date = '" . $this->db->escape($data['pickup_date']) . "', pickup_slot = '" . $this->db->escape($data['pickup_slot']) . "' ");
			
			   
	    $html = '<table style="border-collapse: collapse;"><tr><td style="border: 1px solid; text-align: center;">Name</td><td style="border: 1px solid; text-align: center;">Email</td><td style="border: 1px solid; text-align: center;">Phone</td><td style="border: 1px solid; text-align: center;">Address</td><td style="border: 1px solid; text-align: center;">No.of Packets</td><td style="border: 1px solid; text-align: center;">Pickup Date(yyyy-mm-dd)</td><td style="border: 1px solid; text-align: center;">Pickup Slot</td></tr>';
	    
	    if($data['pickup_slot'] == 1) {
	        $pickup_slot = "9AM - 1PM";
	    } else if($data['pickup_slot'] == 2) {
	        $pickup_slot = "1PM - 4PM";
	    } else if($data['pickup_slot'] == 3) {
	        $pickup_slot = "4PM - 7PM";
	    }
    			
    	$html .= '<tr><td style="border: 1px solid; text-align: center;">'. $data['cst_name'] .'</td><td style="border: 1px solid; text-align: center;">'. $data['cst_email'] .'</td><td style="border: 1px solid; text-align: center;">'. $data['cst_mobile'] .'</td><td style="border: 1px solid; text-align: center;">'. $data['cst_address'] .'</td><td style="border: 1px solid; text-align: center;">'. $data['cst_no_of_packets'] .'</td><td style="border: 1px solid; text-align: center;">' . $data['pickup_date'] . '</td><td style="border: 1px solid; text-align: center;">' . $pickup_slot . '</td></tr>';
    					
    	$html .= '</table>';
			
		$subject = 'New Recycle Request';
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
		$mail->setTo("care@true-elements.com");
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender("True Elements");
		$mail->setSubject($subject);
		$mail->setHtml($html);
		$mail->send();
		
		
		//Mail to customer
		$html = '<p>Hello ' . $data['cst_name'] . ',<br/><br/>';
    	$html .= "Thank you for being part of our recycling program. Now that you've registered to our recycle portal, it would be really great of you if you could just keep the packets for recycling ready in a box, such that the delivery person can easily hand it over to us without any discomfort on either side.<br/><br/>";
    	$html .= 'We would really appreciate it.';
			
		$subject = 'True Elements Recycle Program';
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
		$mail->setTo($data['cst_email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender("True Elements");
		$mail->setSubject($subject);
		$mail->setHtml($html);
		$mail->send();
	}
	
	public function getCustomer($customer_id){		
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX."customer c LEFT JOIN ". DB_PREFIX . "address a ON c.customer_id = a.customer_id WHERE c.customer_id = '". (int)$customer_id ."' ");
			
			return $query->row;
	}
}
?>