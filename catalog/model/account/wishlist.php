<?php
class ModelAccountWishlist extends Model {
	public function addWishlist($product_id, $option = array(), $custom_name = NULL) {
	    if(isset($option)) {
	        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "' AND options = '" . $this->db->escape(json_encode($option)) . "'");

		    $this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist SET customer_id = '" . (int)$this->customer->getId() . "', product_id = '" . (int)$product_id . "', custom_name = '" . $custom_name . "', options = '" . $this->db->escape(json_encode($option)) . "', date_added = NOW()");
	    } else {
	        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "' AND (options IS NULL OR options = '')");
	        
	        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist SET customer_id = '" . (int)$this->customer->getId() . "', product_id = '" . (int)$product_id . "', date_added = NOW()");
	    }
	}

	public function deleteWishlist($wishlist_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE id = '" . (int)$wishlist_id . "'");
	}

	public function getWishlist() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->rows;
	}

	public function getTotalWishlist() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
	
	public function getWishlistById($wishlist_id) {
	    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE id = '" . (int)$wishlist_id . "' LIMIT 1");
	    
	    return $query->row;
	}
}
