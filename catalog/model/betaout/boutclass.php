<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ModelBetaoutBoutclass extends Model {

    public function updateCart($session_obj, $c_id = 0, $cart_obj) {
        try {


            $res = $this->db->query("SELECT * FROM " . DB_PREFIX . "betaout WHERE session = '" . $session_obj . "'");
            if ($res->num_rows) {
                $res = $this->db->query("UPDATE " . DB_PREFIX . "betaout SET cart_obj = '" . $cart_obj . "' WHERE session = '" . $session_obj . "'"); // UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'"
            }
            else {
                $res = $this->db->query("INSERT INTO " . DB_PREFIX . "betaout SET customer_id = '" . (int) $c_id . "', session = '" . $session_obj . "', cart_obj = '" . $cart_obj . "'");
            }
        }
        catch (Exception $ex) {

            return false;
        }
        return true;
    }

    public function getProductFromCart($session_obj) {

        $res;
        try {
            $res = $this->db->query("SELECT * FROM " . DB_PREFIX . "betaout WHERE session = '" . $session_obj . "'"); //   WHERE session = " . $session_obj
        }
        catch (Exception $ex) {

            return false;
        }
        return $res->row;
    }

    public function cartCreate() {

        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        }
        else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            if (isset($this->request->post['quantity'])) {
                $quantity = $this->request->post['quantity'];
            }
            else {
                $quantity = 1;
            }

            if (isset($this->request->post['option'])) {
                $option = array_filter($this->request->post['option']);
            }
            else {
                $option = array();
            }

            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                    $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                }
            }

            if (!$json) {
                $this->cart->add($this->request->post['product_id'], $quantity, $option);

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);

                // Totals
                $this->load->model('setting/extension');

                $total_data = array();
                $total = 0;
                $taxes = $this->cart->getTaxes();

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $sort_order = array();

                    $results = $this->model_setting_extension->getExtensions('total');

                    foreach ($results as $key => $value) {
                        $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                    }

                    array_multisort($sort_order, SORT_ASC, $results);

                    foreach ($results as $result) {
                        if ($this->config->get($result['code'] . '_status')) {
                            $this->load->model('total/' . $result['code']);

                            $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                        }

                        $sort_order = array();

                        foreach ($total_data as $key => $value) {
                            $sort_order[$key] = $value['sort_order'];
                        }

                        array_multisort($sort_order, SORT_ASC, $total_data);
                    }
                }

                $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
            }
            else {
                $json['redirect'] = str_replace('&', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
            }
        }
    }

    public function boutCartClear($session_obj) {
        try {
            $this->db->query("DELETE FROM " . DB_PREFIX . "betaout WHERE session = '" . $session_obj . "'");
        }
        catch (Exception $ex) {
            return false;
        }
        return true;
    }

}
