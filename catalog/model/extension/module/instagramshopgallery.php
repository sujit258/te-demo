<?php
class ModelExtensionModuleInstagramShopGallery extends Model
{
    private $module = array();

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->config->load('isenselabs/instagramshopgallery');
        $this->module = $this->config->get('instagramshopgallery');
    }

    /**
     * Used to get photos with complete data
     */
    public function getPhotos($page = 1, $limit = 48, $select = '*', $where = '', $clause = '')
    {
        $extra_limit = $limit + 5; // incase some image is not available in IG server

        if ($where) {
            $where = 'WHERE ' . $where;
        }

        $query = $this->db->query("SELECT " . $select . ", (SELECT COUNT(*) FROM `" . DB_PREFIX . "instagramshopgallery_to_product` isgp WHERE isgp.shortcode = isg.shortcode) as related_product
            FROM `" . DB_PREFIX . "instagramshopgallery` isg
            " . $where . "
            GROUP BY isg.shortcode
            " . $clause . "
            ORDER BY isg.timestamp DESC
            LIMIT " . (int)(($page-1) * $limit) . ", " . (int)$extra_limit);

        return $query->rows;
    }
 
     
     /**
     * Used to get photos with complete data
     */
    public function getProductPhotos($product_id)  {
            
            $query = $this->db->query(" SELECT *, isgp.product_id FROM " . DB_PREFIX . "instagramshopgallery_to_product isgp LEFT JOIN " . DB_PREFIX . "instagramshopgallery isg ON (isgp.shortcode = isg.shortcode) WHERE isgp.product_id = '" . (int)$product_id . "' GROUP BY isg.shortcode   ORDER BY isg.timestamp DESC LIMIT 0 , 53 ");  

        return $query->rows;
    }

    /**
     * Summary used to check if photos feed have related data in database
     */
    public function getPhotosSummary()
    {
        $data = array();
        $photos = $this->getPhotos(1, 1000, 'isg.shortcode, isg.approve');

        foreach ($photos as $photo) {
            $data[$photo['shortcode']] = $photo;
        }

        return $data;
    }

    public function getPhotosTotal()
    {
        $query = $this->db->query("SELECT COUNT(shortcode) AS total FROM `" . DB_PREFIX . "instagramshopgallery`");

        return $query->row['total'];
    }

    /**
     * Get photo detailed info
     */
    public function getPhoto($shortcode)
    {
        $data = array();
        $query = $this->db->query("SELECT *
            FROM `" . DB_PREFIX . "instagramshopgallery` isg
            WHERE isg.shortcode = '" . $this->db->escape($shortcode) . "'");

        if ($query->num_rows) {
            $data = $query->row;
        }

        return $data;
    }

    /**
     * Get photo related product
     */
    public function getRelatedProduct($shortcode)
    {
        $this->load->model('catalog/product');

        $products = array();
        $query = $this->db->query("SELECT isgp.*
            FROM `" . DB_PREFIX . "instagramshopgallery_to_product` isgp
            LEFT JOIN `" . DB_PREFIX . "product` p ON (p.product_id = isgp.product_id)
            LEFT JOIN `" . DB_PREFIX . "product_to_store` p2s ON (p2s.product_id = isgp.product_id)
            WHERE isgp.shortcode = '" . $this->db->escape($shortcode) . "'
                AND p.status = '1'
                AND p.date_available <= NOW()
                AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
            LIMIT 0, 100");

        foreach ($query->rows as $product) {
            $products[] = $this->model_catalog_product->getProduct($product['product_id']);
        }

        return $products;
    }
}
