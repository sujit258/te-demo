<?php 
class ModelExtensionModulePreOrder extends Model {
	
	public function checkPreOrder($product_id, $option_value_id = null) {
		
		$this->load->model('catalog/product');
		$this->load->model('setting/setting');
		
		$productinfo = $this->model_catalog_product->getProduct($product_id);
		$preorderinfo = $this->GetPreorderedProduct($product_id);
		$data['preorder_available_on'] = $this->language->get('preorder_available_on');
		
		if(!empty($preorderinfo)) {
			$data['preorder_date'] = $preorderinfo['preorder_date'];
			$data['preorder_note'] = $preorderinfo['preorder_note'];
		} else {
			$data['preorder_date'] = 0;
			$data['preorder_note'] = '';
		}
		
		if(isset($productinfo['quantity'])) {
			$quantity = $productinfo['quantity'];
		}
		$stock_status_id = $productinfo['stock_status_id'];
		
		$preorder_settings = $this->model_setting_setting->getSetting('preorder', $this->config->get('config_store_id'));
		$preorder_settings = (isset($preorder_settings['preorder'])) ? $preorder_settings['preorder'] : array(); 

		if(isset($preorder_settings['DateNote'])) {
            $data['preorder_date_note'] = $preorder_settings['DateNote'][$this->config->get('config_language_id')];
        } else {
            $data['preorder_date_note'] = '';
        }
		
		if(isset($preorder_settings['FontColor'])) {
            $data['custom_text_color'] = $preorder_settings['FontColor'];
        } else {
            $data['custom_text_color'] = 'FFFFFF';
        }
		
		if(isset($preorder_settings['BackgroundColor'])) {
            $data['custom_background_color'] = $preorder_settings['BackgroundColor'];
        } else {
            $data['custom_background_color'] = '19BDF0';
        }
		
		if(isset($preorder_settings['BorderColor'])) {
            $data['custom_border_color'] = $preorder_settings['BorderColor'];
        } else {
            $data['custom_border_color'] = '19BDF0';
        }
		
		if(isset($preorder_settings['CustomCSS'])) {
			$data['po_custom_css'] = $preorder_settings['CustomCSS'];
		}

		// Check if stock status of the product is within the enabled one for preorder
		$stock_statuses = $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_status")->rows;

		$enabled_stock_status = array();
		
		foreach($stock_statuses as $stock_status) {  
		   if(isset($preorder_settings[$stock_status['stock_status_id']])) {
			$enabled_stock_status[] = $stock_status['stock_status_id'];
		   }
		}
		
		if(!empty($enabled_stock_status) && in_array($stock_status_id, $enabled_stock_status)) {
			$within_stock_status = true;
		} else {
			$within_stock_status = false;
		}
		$data['within_stock_status'] = $within_stock_status;

		// Check if the chosen option is with a negative quantity
		if(isset($preorder_option)) {
			unset($preorder_option);
		}
		
		if(isset($option_value_id) && !empty($option_value_id)) {
		  	$product_option = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option_value` WHERE product_id = ".$product_id. " AND option_value_id = ".$option_value_id)->row;
			
			if(isset($product_option['quantity']) && strlen($product_option['quantity']) && $product_option['quantity'] <= 0 && !empty($product_option['subtract'])) {
				$preorder_option = true;
			}
		
		}
		
		if(isset($quantity) && $quantity <=0 && !$option_value_id || isset($preorder_option) && $preorder_option) {
			$negative_quantity = true;
		} else {
			$negative_quantity = false;
		}

		if(!empty($preorder_settings) && ($preorder_settings['Enabled'] == 'yes') && $within_stock_status && $negative_quantity) {
		   $data['preorder_product'] = true;
		} else {
		   	$data['preorder_product'] = false;
		}
		
		return $data;
	}
	
	public function sendPreOrderEmail($order_id, $pre_order) {
		if($pre_order==true) {
			
		$this->load->model('checkout/order');

		$preorder = $this->config->get('preorder');
		
		if($preorder['Enabled']=='yes') {

			$order = $this->model_checkout_order->getOrder($order_id);
			$preordered_products = $this->GetPreorderedProducts($order_id, $this->config->get('config_store_id'));
			$OrderLanguage = $this->model_localisation_language->getLanguage($order['language_id']);
			$LangVars = $this->loadLanguage($OrderLanguage['directory'].'/module','preorder');
			$store['url'] = $this->config->get('config_url');
      		$this->load->model('tool/image');
			$Products = '';
			$ProductIDs = '';
			
			if (!empty($preordered_products)) {
				if (sizeof($preordered_products)==1) {
					if(isset($preordered_products[0]['preorder_date'])) {
						$available_on = ' '.$LangVars['preorder_available_on']. ' '. date_format(date_create($preordered_products[0]['preorder_date']), $this->language->get('date_format_short'));
					} else {
						$available_on = ' ';
					}
					
					if(isset($preorder['ImageWidth']) && isset($preorder['ImageHeight'])) {
						$image[0] = $this->model_tool_image->resize($preordered_products[0]['image'], $preorder['ImageWidth'], $preorder['ImageHeight']);
					} else {
						$image[0] = $this->model_tool_image->resize($preordered_products[0]['image'], 200, 200);
					}
			
					$Products = '<div style="display:inline-block;text-align:center;margin:10px"><div><a href="'.$store['url'].'index.php?route=product/product&amp;product_id=' . $preordered_products[0]['product_id'].'">'.'<img src="'.str_replace(' ', '%20', $image[0]).'" /><div>'.$preordered_products[0]['name'].'</div></a></div><div> '.$available_on.'</div></div>';
					$ProductIDs = $preordered_products[0]['product_id'];
				} else {
					for ($i=0; $i<sizeof($preordered_products); $i++) {
						if (($i+1) == sizeof($preordered_products)) {
							$Products .= ' </br> ';
						}  else if (($i+1) < sizeof($preordered_products) && ($i>0)) {
							$Products .= ', ';	
						}
						
						if(isset($preordered_products[$i]['preorder_date'])) {
							
						$available_on = ' '.$LangVars['preorder_available_on']. ' '. date_format(date_create($preordered_products[$i]['preorder_date']), $this->language->get('date_format_short'));
						if(isset($preorder['ImageWidth']) && isset($preorder['ImageHeight'])) {
							$image[$i] = $this->model_tool_image->resize($preordered_products[$i]['image'], $preorder['ImageWidth'], $preorder['ImageHeight']);
						} else {
							$image[$i] = $this->model_tool_image->resize($preordered_products[$i]['image'], 200, 200);
						}
					
					} else {
						$available_on = ' ';
					} 
						$Products .= '<div style="display:inline-block;text-align:center;margin:10px"><div><a href="'.$store['url'].'index.php?route=product/product&amp;product_id=' . $preordered_products[$i]['product_id'].'">'.'<img src="'.str_replace(' ', '%20', $image[$i]).'" /><div>'.$preordered_products[$i]['name'].'</div></a></div><div> '.$available_on.'</div></div>';
						$ProductIDs .= $preordered_products[$i]['product_id'];
						
						if (!(($i+1) == sizeof($preordered_products)))
								$ProductIDs .= '_';
					}
				}
			}

				$messageToCustomer = html_entity_decode($preorder['EmailText'][$order['language_code']], ENT_QUOTES, 'UTF-8'); 
				$wordTemplates = array("{firstname}", "{lastname}", "{products}");
				$words   = array($order['firstname'], $order['lastname'], $Products);	
				
				$messageToCustomer = str_replace($wordTemplates, $words, $messageToCustomer);

				if (VERSION < '2.0.2.0') {
					$mailToUser = new Mail($this->config->get('config_mail'));
				} else {
					$mailToUser = new Mail();
					$mailToUser->protocol = $this->config->get('config_mail_protocol');
					$mailToUser->parameter = $this->config->get('config_mail_parameter');
					$mailToUser->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mailToUser->smtp_username = $this->config->get('config_mail_smtp_username');
					$mailToUser->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mailToUser->smtp_port = $this->config->get('config_mail_smtp_port');
					$mailToUser->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
				}
				
				$mailToUser->setTo($order['email']);
				$mailToUser->setFrom($this->config->get('config_email'));
					
				$mailToUser->setSender($order['store_name']);
				$mailToUser->setSubject(html_entity_decode($preorder['EmailSubject'][$order['language_code']], ENT_QUOTES, 'UTF-8'));
				$mailToUser->setHtml($messageToCustomer);
				$mailToUser->send();
				
				if($preorder['Notifications'] == 'yes') { 
					$mailToUser->setSubject(html_entity_decode('[Admin Notification] '.$preorder['EmailSubject'][$order['language_code']], ENT_QUOTES, 'UTF-8'));
					$mailToUser->setTo($this->config->get('config_email'));
					$mailToUser->send();
				}
			}
		}
		
	}
	public function GetPreorderedProduct($product_id) { 
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "preorder_product` WHERE product_id = ".$product_id);
			if(isset($query->num_rows) && $query->num_rows > 0) {
				return $query->row; 
			}
	}
	
    public function GetPreorderedProducts($order_id, $store_id=0){ 
  		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "preorder` as po JOIN `" . DB_PREFIX . "product_description` as pd ON (po.product_id = pd.product_id AND po.language_id=pd.language_id) LEFT JOIN `" . DB_PREFIX . "preorder_product` as pp ON (po.product_id = pp.product_id AND po.language_id = pp.language_id) JOIN `" . DB_PREFIX . "product` as p ON (po.product_id = p.product_id) WHERE order_id = ".$order_id." AND po.language_id = " . (int)$this->config->get('config_language_id') . " AND store_id=".$store_id);        return $query->rows; 
    }
	
	public function loadLanguage($directory, $filename) {
		$default = 'english/extension/module';
		$data = array();
		
		$file = DIR_LANGUAGE . $directory . '/' . $filename . '.php';
		if (file_exists($file)) {
			$_ = array();

			require($file);
			 $data = array_merge($data, $_);
			return $data;
		}
		
		$file = DIR_LANGUAGE . $default . '/' . $filename . '.php';
		if (file_exists($file)) {
			$_ = array();
			require($file);
			$data = array_merge($data, $_);
			return $data;
		} else {
			trigger_error('Error: Could not load language ' . $filename . '!');
		}
	}
	
}
?>