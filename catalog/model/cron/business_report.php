<?php
class ModelCronBusinessReport extends Model {
	public function getDailyReport($current_date, $date_start) {
	    $inflows_order_statuses = $this->getInflowsOrderStatusList();
        $shipped_complete_order_statuses = $this->getShippedCompleteOrderStatusList();
        $other_order_statuses = $this->getOtherOrderStatusList();
        
        $total_sale = 0;
        $total_orders = 0;
        $total_sale_today = 0;
        $total_orders_today = 0;
        
        foreach($inflows_order_statuses AS &$inflows_order_status) {
            if($inflows_order_status['order_status_id'] == 19) {
                $inflows_order_status['order_status_id'] = 0;
            }
            $monthly_inflows_query = $this->db->query("SELECT SUM(total) AS total_sale, COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE order_status_id = '" . $inflows_order_status['order_status_id'] . "' AND (DATE(date_added) >= '" . $this->db->escape($date_start) . "' AND DATE(date_added) <= '" . $this->db->escape($current_date) . "')");
	        $today_inflows_query = $this->db->query("SELECT SUM(total) AS total_sale, COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE order_status_id = '" . $inflows_order_status['order_status_id'] . "' AND DATE(date_added) = '" . $this->db->escape($current_date) . "'");
	        
	        $monthly_tc_query = $this->db->query("SELECT SUM(abs(ot.value)) AS total_tc FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX . "order_total ot USING(order_id) WHERE o.order_status_id = '" . $inflows_order_status['order_status_id'] . "' AND (DATE(o.date_added) >= '" . $this->db->escape($date_start) . "' AND DATE(o.date_added) <= '" . $this->db->escape($current_date) . "') AND ot.code LIKE 'reward'");
	        $today_tc_query = $this->db->query("SELECT SUM(abs(ot.value)) AS total_tc FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX . "order_total ot USING(order_id) WHERE o.order_status_id = '" . $inflows_order_status['order_status_id'] . "' AND DATE(o.date_added) = '" . $this->db->escape($current_date) . "'  AND ot.code LIKE 'reward'");
            
	        $total_sale += $monthly_inflows_query->row['total_sale'];
	        $total_tc += $monthly_tc_query->row['total_tc'];
	        $total_orders += $monthly_inflows_query->row['total_orders'];
	        $total_sale_today += $today_inflows_query->row['total_sale'];
	        $total_tc_today += $today_tc_query->row['total_tc'];
	        $total_orders_today += $today_inflows_query->row['total_orders'];
	        
	    }
	    
	    $funnel_data[] = array(
	        'status'                => 'Inflows',
	        'total_sale'            => $total_sale + $total_tc,
	        'total_tc'              => $total_tc,
	        'total_orders'          => $total_orders,
	        'total_sale_today'      => $total_sale_today + $total_tc_today,
	        'total_tc_today'        => $total_tc_today,
	        'total_orders_today'    => $total_orders_today
	    );
	    
	    foreach($shipped_complete_order_statuses AS $shipped_complete_order_status) {
	        $monthly_shipped_complete_query = $this->db->query("SELECT SUM(total) AS total_sale, COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE order_status_id = '" . $shipped_complete_order_status['order_status_id'] . "' AND (DATE(date_shipped) >= '" . $this->db->escape($date_start) . "' AND DATE(date_shipped) <= '" . $this->db->escape($current_date) . "')");
	        $today_shipped_complete_query = $this->db->query("SELECT SUM(total) AS total_sale, COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE order_status_id = '" . $shipped_complete_order_status['order_status_id'] . "' AND DATE(date_shipped) = '" . $this->db->escape($current_date) . "'");
	        
	        $monthly_tc_query = $this->db->query("SELECT SUM(abs(ot.value)) AS total_tc FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX . "order_total ot USING(order_id) WHERE o.order_status_id = '" . $shipped_complete_order_status['order_status_id'] . "' AND (DATE(o.date_shipped) >= '" . $this->db->escape($date_start) . "' AND DATE(o.date_shipped) <= '" . $this->db->escape($current_date) . "') AND ot.code LIKE 'reward'");
	        $today_tc_query = $this->db->query("SELECT SUM(abs(ot.value)) AS total_tc FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX . "order_total ot USING(order_id) WHERE o.order_status_id = '" . $shipped_complete_order_status['order_status_id'] . "' AND DATE(o.date_shipped) = '" . $this->db->escape($current_date) . "'  AND ot.code LIKE 'reward'");
	        
	        $funnel_data[] = array(
	            'status'                => $shipped_complete_order_status['name'],
	            'total_sale'            => $monthly_shipped_complete_query->row['total_sale'] + $monthly_tc_query->row['total_tc'],
	            'total_orders'          => $monthly_shipped_complete_query->row['total_orders'],
	            'total_tc'              => $monthly_tc_query->row['total_tc'],
	            'total_sale_today'      => $today_shipped_complete_query->row['total_sale'] + $today_tc_query->row['total_tc'],
	            'total_orders_today'    => $today_shipped_complete_query->row['total_orders'],
	            'total_tc_today'        => $today_tc_query->row['total_tc']
	        );
	    }
	    
	    foreach($other_order_statuses AS &$other_order_status) {
	        if($other_order_status['order_status_id'] == 19) {
                $other_order_status['order_status_id'] = 0;
            }
	        $monthly_other_query = $this->db->query("SELECT SUM(total) AS total_sale, COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE order_status_id = '" . $other_order_status['order_status_id'] . "' AND (DATE(date_added) >= '" . $this->db->escape($date_start) . "' AND DATE(date_added) <= '" . $this->db->escape($current_date) . "')");
	        $today_other_query = $this->db->query("SELECT SUM(total) AS total_sale, COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE order_status_id = '" . $other_order_status['order_status_id'] . "' AND DATE(date_added) = '" . $this->db->escape($current_date) . "'");
	        
	        $monthly_tc_query = $this->db->query("SELECT SUM(abs(ot.value)) AS total_tc FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX . "order_total ot USING(order_id) WHERE o.order_status_id = '" . $other_order_status['order_status_id'] . "' AND (DATE(o.date_shipped) >= '" . $this->db->escape($date_start) . "' AND DATE(o.date_shipped) <= '" . $this->db->escape($current_date) . "') AND ot.code LIKE 'reward'");
	        $today_tc_query = $this->db->query("SELECT SUM(abs(ot.value)) AS total_tc FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX . "order_total ot USING(order_id) WHERE o.order_status_id = '" . $other_order_status['order_status_id'] . "' AND DATE(o.date_shipped) = '" . $this->db->escape($current_date) . "'  AND ot.code LIKE 'reward'");
	        
	        $funnel_data[] = array(
	            'status'                => $other_order_status['name'],
	            'total_sale'            => $monthly_other_query->row['total_sale'] + $monthly_tc_query->row['total_tc'],
	            'total_orders'          => $monthly_other_query->row['total_orders'],
	            'total_tc'              => $monthly_tc_query->row['total_tc'],
	            'total_sale_today'      => $today_other_query->row['total_sale'] + $today_tc_query->row['total_tc'],
	            'total_orders_today'    => $today_other_query->row['total_orders'],
	            'total_tc_today'        => $today_tc_query->row['total_tc']
	        );
	    }
	    return $funnel_data;
	}
	
	public function getCustomProductsData($current_date, $date_start) {
	    $customized_product_data = array();
	    $monthly_query = $this->db->query("SELECT p.product_id, pd.name, SUM(op.total + (op.tax * op.quantity)) AS total_sales, COUNT(o.order_id) AS total_orders FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) LEFT JOIN " . DB_PREFIX . "order_product op USING (product_id) LEFT JOIN " . DB_PREFIX . "order o USING (order_id) WHERE o.order_status_id IN (3,5) AND (DATE(o.date_shipped) >= '" . $this->db->escape($date_start) . "' AND DATE(o.date_shipped) <= '" . $this->db->escape($current_date) . "') AND p.product_id IN (6345,6449) GROUP BY p.product_id, pd.name");
	    
	    foreach($monthly_query->rows AS $data) {
	        $customized_product_data[$data['product_id']] = array(
	            'name'                  => $data['name'],
	            'total_sales'           => round($data['total_sales']),
	            'total_orders'          => $data['total_orders'],
	            'total_sales_today'     => 0,
	            'total_orders_today'    => 0
	        );
	    }
	    if(!empty($customized_product_data)) {
	        $daily_query = $this->db->query("SELECT p.product_id, pd.name, SUM(op.total + (op.tax * op.quantity)) AS total_sales, COUNT(o.order_id) AS total_orders FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) LEFT JOIN " . DB_PREFIX . "order_product op USING (product_id) LEFT JOIN " . DB_PREFIX . "order o USING (order_id) WHERE o.order_status_id IN (3,5) AND DATE(o.date_shipped) = '" . $this->db->escape($current_date) . "' AND p.product_id IN (6345,6449) GROUP BY p.product_id, pd.name");
	    
	        foreach($daily_query->rows AS $data) {
	            $customized_product_data[$data['product_id']]['total_sales_today'] = round($data['total_sales']);
	            $customized_product_data[$data['product_id']]['total_orders_today'] = $data['total_orders'];
	        }
	    }
	    
	    return $customized_product_data;
	}
	
	public function getTeLabsData($current_date, $date_start) {
	    $te_labs_data = array();
	    $te_labs_product_ids = "";
	    $i = 0;
	    $customer_group_id = 9;
	    $category_id = 622;
	    $discounts = $this->config->get('total_customer_group_discount_customer_group_id');
        $discount_per = $discounts[$customer_group_id];
        
        $te_lab_products_query = $this->db->query("SELECT p.product_id, pd.name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) LEFT JOIN " . DB_PREFIX . "product_to_category ptc USING (product_id) WHERE ptc.category_id = '" . (int)$category_id . "' AND p.status = 1 ORDER BY p.product_id DESC");
        
        
        foreach($te_lab_products_query->rows AS $te_lab_product) {
            if($i == 0) {
                $te_labs_product_ids .=  $te_lab_product['product_id'];
            } else {
                $te_labs_product_ids .=  "," . $te_lab_product['product_id'];
            }
            $te_labs_data[$te_lab_product['product_id']] = array(
                'name'                  => $te_lab_product['name'],
                'total_sales'           => 0,
                'total_orders'          => 0,
                'total_sales_today'     => 0,
                'total_orders_today'    => 0
            );
            $i++;
        }
        
	    $monthly_query = $this->db->query("SELECT p.product_id, op.name, SUM(op.total + (op.tax * op.quantity)) AS total_sales, COUNT(o.order_id) AS total_orders FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX ."order_product op USING (order_id) LEFT JOIN " . DB_PREFIX . "product p USING (product_id) WHERE o.order_status_id IN (3,5) AND o.customer_group_id = '" . (int)$customer_group_id . "' AND (DATE(o.date_shipped) >= '" . $this->db->escape($date_start) . "' AND DATE(o.date_shipped) <= '" . $this->db->escape($current_date) . "') AND p.product_id IN (" . $te_labs_product_ids . ") GROUP BY p.product_id, op.name");
	    
	    foreach($monthly_query->rows AS $data) {
	        $te_labs_data[$data['product_id']]['total_sales'] = $discount_per > 0 ? round($data['total_sales'] * (1 - $discount_per / 100)) : $data['total_sales'];
	        $te_labs_data[$data['product_id']]['total_orders'] = $data['total_orders'];
	    }
	    if(!empty($te_labs_data)) {
	        $daily_query = $this->db->query("SELECT p.product_id, op.name, SUM(op.total + (op.tax * op.quantity)) AS total_sales, COUNT(o.order_id) AS total_orders FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX ."order_product op USING (order_id) LEFT JOIN " . DB_PREFIX . "product p USING (product_id) WHERE o.order_status_id IN (3,5) AND o.customer_group_id = '" . (int)$customer_group_id . "' AND DATE(o.date_shipped) = '" . $this->db->escape($current_date) . "' AND p.product_id IN (" . $te_labs_product_ids . ") GROUP BY p.product_id, op.name");
	    
	        foreach($daily_query->rows AS $data) {
	            $te_labs_data[$data['product_id']]['total_sales_today'] = $discount_per > 0 ? round($data['total_sales'] * (1 - $discount_per / 100)) : $data['total_sales'];
	            $te_labs_data[$data['product_id']]['total_orders_today'] = $data['total_orders'];
	        }
	    }
	    return $te_labs_data;
	}
	
	public function getOrderCountByPaymentCode($current_date, $date_start) {
	    $other_order_statuses = $this->getOtherOrderStatusList();
	    
	    foreach($other_order_statuses AS &$other_order_status) {
	        if($other_order_status['order_status_id'] == 19) {
                $other_order_status['order_status_id'] = 0;
            }
	    
	        if($other_order_status['order_status_id'] == 0 || $other_order_status['order_status_id'] == 2 || $other_order_status['order_status_id'] == 7 || $other_order_status['order_status_id'] == 23 || $other_order_status['order_status_id'] == 24 ) {
	            $razorpay_query = $this->db->query("SELECT COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE payment_code = 'razorpay' AND order_status_id = '" . $other_order_status['order_status_id'] . "' AND (DATE(date_added) >= '" . $this->db->escape($date_start) . "' AND DATE(date_added) <= '" . $this->db->escape($current_date) . "')");
	            $cod_query = $this->db->query("SELECT COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE payment_code = 'cod' AND order_status_id = '" . $other_order_status['order_status_id'] . "' AND (DATE(date_added) >= '" . $this->db->escape($date_start) . "' AND DATE(date_added) <= '" . $this->db->escape($current_date) . "')");
	            $payu_query = $this->db->query("SELECT COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE payment_code = 'payu' AND order_status_id = '" . $other_order_status['order_status_id'] . "' AND (DATE(date_added) >= '" . $this->db->escape($date_start) . "' AND DATE(date_added) <= '" . $this->db->escape($current_date) . "')");
	            $paytm_query = $this->db->query("SELECT COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE payment_code = 'paytm' AND order_status_id = '" . $other_order_status['order_status_id'] . "' AND (DATE(date_added) >= '" . $this->db->escape($date_start) . "' AND DATE(date_added) <= '" . $this->db->escape($current_date) . "')");
	            
	            $payment_zone_data[] = array(
	                'status'                    => $other_order_status['name'],
	                'razorpay_total_orders'     => $razorpay_query->row['total_orders'],
	                'cod_total_orders'          => $cod_query->row['total_orders'],
	                'payu_total_orders'         => $payu_query->row['total_orders'],
	                'paytm_total_orders'        => $paytm_query->row['total_orders'],
	            );
	        }
	    }
	    return $payment_zone_data;
	}
	
	public function getShippedCompleteOrderStatusList() {
	    $query = $this->db->query("SELECT order_status_id, name FROM " . DB_PREFIX . "order_status WHERE order_status_id IN (3,5) ORDER BY order_status_id ASC");
	    return $query->rows;
	}
	
	public function getInflowsOrderStatusList() {
	    $query = $this->db->query("SELECT order_status_id, name FROM " . DB_PREFIX . "order_status WHERE order_status_id NOT IN (8,11,12,14,18,25) ORDER BY order_status_id ASC");
	    return $query->rows;
	}
	
	public function getOtherOrderStatusList() {
	    $query = $this->db->query("SELECT order_status_id, name FROM " . DB_PREFIX . "order_status WHERE order_status_id NOT IN (3,5,8,14,15,18,25) ORDER BY order_status_id ASC");
	    return $query->rows;
	}
	
	public function getOrderAndComment() {
	    $query = $this->db->query("SELECT o.order_id, o.date_added, o.comment, os.name FROM " . DB_PREFIX . "order o LEFT JOIN " . DB_PREFIX ."order_status os ON (o.order_status_id = os.order_status_id) WHERE (o.comment != '' AND o.comment IS NOT NULL AND o.comment != '0') AND (o.order_status_id = 0 OR o.order_status_id = 23) ORDER BY o.order_status_id ASC");
	    return $query->rows;
	}
}
?>