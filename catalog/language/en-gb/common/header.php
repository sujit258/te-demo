<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_phone']           = 'Call us';
$_['text_store']           = 'Store Location ';
$_['text_guarantee']           = 'Guarantee';
$_['text_free_shipping']           = 'Free Shipping on Orders $50+';
$_['text_compare']           = 'Compare';
$_['text_block']           = 'Summer sale - up to 50% off. <a href="#">More details</a>
';
