<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Join Our True Club to Get Offers & Updates';
$_['text_payment']   = 'payment';
$_['text_powered']  = 'Copyright &copy; 2018 <a href="http://www.plazathemes.com">plazathemes</a>. All Rights Reserved.';
$_['text_logo_footer']   = 'About us';
$_['text_desc_footer']   = 'We are a team of designers and developers that create high quality Magento, Prestashop, Opencart.';
$_['text_address']   = 'Adress:&nbsp;';
$_['text_email']   = 'Email:&nbsp;';
$_['text_telephone']   = 'Phone:&nbsp;';
$_['text_instagram']   = 'Instagram';