<?php
// Text
$_['button_read_more']         		 = 'Read more';
$_['text_empty']           			 = 'No articles';
$_['text_headingtitle']           			 = 'Latest From Our blog';
$_['text_blog'] = 'Blog';
$_['text_post_by'] = 'posted by: ';
$_['text_title']      = 'Gain extra and useful knowledge regarding your health, free of cost under one roof by our frequent blog posts.';
