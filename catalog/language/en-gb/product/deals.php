<?php
$_['heading_title']            = 'All Deals';

// Text
$_['text_home']                = 'Home';
$_['text_empty']               = 'Currently no active deals found.';
$_['text_error']               = 'Deals not found!';
$_['text_coupon']              = 'Coupon: ';
$_['text_quantity']            = 'Quantity: ';

//Button
$_['button_continue']          = 'Continue';
?>