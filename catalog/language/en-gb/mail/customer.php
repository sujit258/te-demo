<?php
// Text
$_['text_subject']        = '%s - Thank you for registering';
$_['text_welcome']        = 'Welcome and thank you for registering at %s!';
$_['text_login']          = 'Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_approval']       = 'Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_services']       = 'Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.';
$_['text_thanks']         = 'Thanks,';
$_['text_new_customer']   = 'New customer';
$_['text_signup']         = 'A new customer has signed up:';
$_['text_website']        = 'Web Site:';
$_['text_customer_group'] = 'Customer Group:';
$_['text_firstname']      = 'First Name:';
$_['text_lastname']       = 'Last Name:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Telephone:';


$_['text_subject_te_lab']        = '%s - Thank you for Applying TE Lab Program';
$_['text_hello_te_lab']        = 'Hello %s';
$_['text_welcome_te_lab']        = 'We are true-ly pleased to receive your application and looking at your interest for our upcoming new launch, we are excited to know your feedback for the same.';
$_['text_thanks_te_lab']        = 'Thank you for your application. We shall again drop you an email once your application is approved.';
$_['text_footer_te_lab']        = 'Stay True-ly excited ;)';

$_['text_new_customer_te_lab']   = 'New TE Lab request received(%s)';
$_['text_signup_te_lab']  = 'New request received for TE Lab';

