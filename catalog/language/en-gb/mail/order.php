<?php
// Text
$_['text_new_subject']          = '%s - Order %s';
$_['text_customer_order_subject'] = 'Thank you for your order %s. Welcome to the world of True!';
$_['text_new_greeting']         = "Thanks a lot for letting True Elements be a part of your story. It means a lot to us and we are truly delighted to serve you. We will try our best to dispatch the order within the next 48 working hours, if not earlier.";
$_['text_new_received']         = 'You have received an order.';
$_['text_new_link']             = 'To view your order click on the link below:';
$_['text_new_order_detail']     = 'Order Details';
$_['text_new_instruction']      = 'Instructions';
$_['text_new_order_id']         = 'Order ID:';
$_['text_new_date_added']       = 'Date Added:';
$_['text_new_order_status']     = 'Order Status:';
$_['text_new_payment_method']   = 'Payment Method:';
$_['text_new_shipping_method']  = 'Shipping Method:';
$_['text_new_email']            = 'E-mail:';
$_['text_new_telephone']        = 'Mobile:';
$_['text_new_ip']               = 'IP Address:';
$_['text_new_payment_address']  = 'Payment Address';
$_['text_new_shipping_address'] = 'Shipping Address';
$_['text_new_products']         = 'Products';
$_['text_new_product']          = 'Product';
$_['text_new_model']            = 'Model';
$_['text_new_quantity']         = 'Quantity';
$_['text_new_price']            = 'Price';
$_['text_new_order_total']      = 'Order Totals';
$_['text_new_total']            = 'Total';
$_['text_new_download']         = 'Once your payment has been confirmed you can click on the link below to access your downloadable products:';
$_['text_new_comment']          = 'The comments for your order are:';
$_['text_new_footer']           = "<p style='color:#000000;'>What you see above is part of our world that is Made of Truth – Not a world full of lies, jargon or False Promises. Where what is written on the pack is what's in it, and vice versa.<br/>
<i>India's First Whole-food plant-based Nashta (snacks and breakfast) brand.</i><br/><br/>
For any queries, please feel free to get in touch at %s (Ph) or %s (Whatsapp) with our team - we are there at your service, always!<br/>";
$_['text_update_subject']       = '%s - Order Update %s';
$_['text_update_order']         = 'Order ID:';
$_['text_update_date_added']    = 'Date Ordered:';
$_['text_update_order_status']  = 'Your order has been updated to the following status:';
$_['text_update_comment']       = 'The comments for your order are:';
$_['text_update_link']          = 'To view your order click on the link below:';

$_['text_update_footer']        = 'For any queries, please feel free to get in touch at +91-8767-120-120 (Ph) or 7738442817 (Whatsapp) with our team - we are there at your service, always!';
