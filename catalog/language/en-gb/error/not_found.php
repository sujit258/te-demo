<?php
// Heading
$_['heading_title'] = 'Page not found';

// Text
$_['text_error']    = 'The page you requested cannot be found.';