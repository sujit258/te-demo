<?php 
//text
$_['text_title']               = 'Save Earth & Earn Rewards';

// enrty
$_['entry_name']               = 'Your Name';
$_['entry_email']              = 'Your Email';
$_['entry_mobile']             = 'Mobile No';
$_['entry_address']            = 'Address';
$_['entry_no_of_packets']      = 'Total No Of Packets / Container';
$_['entry_order_id']           = 'Your Order Id';
$_['entry_pickup_date']        = 'Pickup Date';
$_['entry_pickup_slot']        = 'Pickup Slot';

// Placeholder
$_['tag_name']     = 'Your Name';
$_['tag_email']    = 'Email';
$_['tag_mobile']   = 'Mobile No';
$_['tag_address']  = 'Your Address';

$_['tag_no_of_packets']  = 'How much ?';

// Button
$_['button_send']               = 'Submit';
$_['button_catalog']            = 'View Product Catalogue';

?>