<?php 
//text
$_['text_title']               = 'Partner With Us';
$_['heading_title']               = 'Partner With Us';

// enrty
$_['entry_name']               = 'Your Name';
$_['entry_email']              = 'Your Email';
$_['entry_mobile']             = 'Mobile No';
$_['entry_city']               = 'City';
$_['entry_scope']              = 'Scope of Partnership';

// Placeholder
$_['tag_name'] = 'Enter Your Name';
$_['tag_email'] = 'Enter Valid Email Id';
$_['tag_mobile'] = '10-Digit Mobile Number';
$_['tag_city'] = 'Enter Your city';


// Button
$_['button_send']               = 'Send';
$_['button_catalog']            = 'View Product Catalogue';

?>