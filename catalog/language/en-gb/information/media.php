<?php	
// Heading
$_['heading_title']     = 'True Elements | Recipe';

// Entry
$_['entry_qty']                = 'Qty';
$_['entry_name']               = 'Your Name';
$_['entry_review']             = 'Your Review';
$_['entry_rating']             = 'Rating';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

$_['place_holder']      = 'Enter Recipe Name';

// Text
$_['text_media']        = 'Media';
$_['text_description']  = 'Description';
$_['text_directions']   = 'Directions';
$_['text_ingredients']  = 'Ingredients';
$_['text_index']        = 'Recipe Index:';
$_['text_tax']          = 'Ex Tax:';
$_['text_related']      = 'Recommended Products For Recipe';
$_['text_error']        = 'Sorry, Details not found.!';
$_['text_empty']        = 'Recipe not found.';
$_['text_write']        = 'Write a review';
$_['text_reviews']      = '%s reviews';
$_['text_sort']         = 'Sort By:';
$_['text_no_reviews']   = 'There are no reviews for this Recipe..!';
$_['text_note']         = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_default']      = 'Default';
$_['tab_review']        = 'Reviews (%s)';
$_['text_success']      = 'Thank you for your review. It has been submitted to the webmaster for approval.';

$_['text_limit']        = 'Show:';

$_['button_submit']     = 'Submit';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';