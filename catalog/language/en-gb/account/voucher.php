<?php
// Heading
$_['heading_title']    = 'Purchase a Gift Certificate';

// Text
$_['text_account']     = 'Account';
$_['text_voucher']     = 'Gift Certificate';
$_['text_description'] = 'This gift certificate will be emailed to the recipient after your order has been paid for.';
$_['text_agree']       = 'I understand that gift certificates are non-refundable.';
$_['text_message']     = '<p>Thank you for purchasing a gift certificate! Once you have completed your order your gift certificate recipient will be sent an e-mail with details how to redeem their gift certificate.</p>';
$_['text_for']         = '%s Gift Certificate for %s';

// Entry
$_['entry_to_name']    = 'Recipient\'s Name';
$_['entry_to_email']   = 'Recipient\'s e-mail';
$_['entry_from_name']  = 'Your Name';
$_['entry_from_email'] = 'Your e-mail';
$_['entry_theme']      = 'Gift Certificate Theme';
$_['entry_message']    = 'Message';
$_['entry_amount']     = 'Amount';

// Help
$_['help_message']     = 'Optional';
$_['help_amount']      = 'Value must be between %s and %s';

// Error
$_['error_to_name']    = 'Recipient\'s Name must be between 1 and 64 characters!';
$_['error_from_name']  = 'Your Name must be between 1 and 64 characters!';
$_['error_email']      = 'E-Mail Address does not appear to be valid!';
$_['error_theme']      = 'You must select a theme!';
$_['error_amount']     = 'Amount must be between %s and %s!';
$_['error_agree']      = 'Warning: You must agree that the gift certificates are non-refundable!';

$_['text_subject']  = 'Hurray! %s sent you a Gift Voucher worth  %s/-';
$_['text_greeting'] = 'Congratulations...! You have received a Gift Certificate worth %s/-';
$_['text_from']     = 'This Gift Certificate has been sent to you by %s';
$_['text_message1']  = 'With a message saying';
$_['text_redeem']   = 'Stick to your diet essentials even during lockdown with True Elements. Grab your favourite breakfast cereals and snacks by using the voucher code (<b>%s</b>) at checkout ';
$_['text_footer']   = ' ';
