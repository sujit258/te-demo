<?php
// Heading
$_['heading_title']  = 'Redeem Voucher';

// Text
$_['text_account']   = 'Account';
$_['text_code']  = 'Your Voucher Code';
$_['text_success']   = 'Your voucher has been successfully reddemed into True cash.';

// Entry
$_['entry_code'] = 'Voucher code'; 

// Error
$_['error_exists'] = 'Voucher code is not valid'; 