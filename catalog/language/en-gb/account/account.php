<?php
// Heading
$_['heading_title']      = 'My Account';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'My Account';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit my account information';
$_['text_password']      = 'Change my password';
$_['text_address']       = 'Modify my address book entries';
$_['text_credit_card']   = 'Manage Stored Credit Cards';
$_['text_wishlist']      = 'Modify my wish list';
$_['text_order']         = 'View order history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'My True Cash';
$_['text_return']        = 'View return requests';
$_['text_transaction']   = 'My Transactions';
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';
$_['text_recurring']     = 'Recurring payments';
$_['text_transactions']  = 'Transactions';