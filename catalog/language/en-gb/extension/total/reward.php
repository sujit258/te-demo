<?php
// Heading
$_['heading_title'] = 'Use True Cash (Available %s)';

// Text
$_['text_reward']   = 'True Cash(%s)';
$_['text_order_id'] = 'Order ID: #%s';
$_['text_success']  = 'Success: Your True Cash has been applied!';

// Entry
$_['entry_reward']  = 'True Cash to use (Max %s)';

// Error
$_['error_reward']  = 'Warning: Please enter the amount of True Cash to use!';
$_['error_points']  = 'Warning: You don\'t have %s True Cash!';
$_['error_maximum'] = 'Warning: The maximum number of True Cash that can be applied is %s!';
