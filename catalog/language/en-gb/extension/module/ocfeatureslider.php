<?php
// Heading
$_['heading_title'] = 'Featured Product slider';

// Text
$_['text_tax']      = 'Ex Tax:';
$_['text_sale']      = 'Sale';
$_['text_new']      = 'New';
$_['text_title']    = 'We supply 100% natural products which can add many benefits to your health.';
$_['text_empty']    = 'There is no Featured Products!';
