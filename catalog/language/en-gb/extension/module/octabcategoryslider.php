<?php
// Heading
$_['heading_title'] = 'Tab Category slider';

// Text
$_['text_tax']      = 'Ex Tax:';
$_['text_sale']      = 'Sale';
$_['text_new']      = 'New';
$_['text_title']      = 'Browse the collection of our best selling and trending products';
$_['text_empty']    = 'There is no products match this category';
