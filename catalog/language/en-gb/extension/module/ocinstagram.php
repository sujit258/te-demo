<?php
// Heading
$_['heading_title2']    = '<span>#Wenro</span>.Lookbook';
$_['heading_title']    = 'Instagram';

$_['text_follow']        = 'Follow us on Instagram';
$_['text_des']        = 'View Our 2016 Lookbook Feed';
$_['text_title']      = 'Show us how you style your rare pieces on Instagram with the hashtag #trueelements';
$_['text_copyright']        = 'Instagram -- &copy; %s';