<?php
// Heading 
$_['heading_title'] 	 = 'Newsletter';

$_['newletter_lable'] 	 = 'Get the latest deals and special offers';
$_['newletter_des'] 	 = 'Subscribe to our newsletters now and stay up-to-date with new collections, the latest lookbooks and exclusive offers.';

//Fields
$_['entry_email'] 		 = 'Email';
$_['entry_name'] 		 = 'Name';

//Buttons
$_['entry_button'] 		 = 'Subscribe';
$_['entry_unbutton'] 	 = 'Unsubscribe';

//text
$_['text_subscribe'] 	 = 'Subscribe Here';

$_['mail_subject']   	 = 'NewsLetter Subscribe';

//Messages
$_['error_invalid'] 	 = 'Please enter valid email address.';
$_['subscribe']	    	 = 'Subscribed Successfully.';
$_['unsubscribe'] 	     = 'Unsubscribed Successfully.';
$_['alreadyexist'] 	     = 'You are already subscribed.';
$_['notexist'] 	    	 = ' Email doesn\'t exist.';
$_['entry_show_again']   =  "Don't show this popup again";