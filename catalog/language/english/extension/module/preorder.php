<?php
// Heading 
$_['heading_title']  = 'Pre-order';
$_['preorder_Title'] = $_['heading_title'];
$_['preorder_Error1'] = 'This field is required!';
$_['preorder_Error2'] = 'The email you entered is not valid!';
$_['preorder_Success'] = 'Success: You have pre-ordered <a href="%s">%s</a>. Go to your <a href="%s">shopping cart</a>.';
$_['preorder_available_on'] = 'Available On:';
?>