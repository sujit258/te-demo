<?php
$_['button_back']   = 'Back';

$_['text_submit']	= 'Submit review';
$_['text_review']	= 'Your review:';
$_['text_and']		= 'and';
$_['text_reviewmail_link']		  = '<div style="font-family:inherit;font-size:11px;text-align:center;background: #f2f2f2;padding:3px;"><p>If this email is not displayed correctly or you cannot submit the form, please <a href="{reviewmail_link_href}"><strong>click here</strong></a>.</p></div>';
$_['link_replacement']		  = 
'<div style="width:100%;height:25px;font-family:inherit;font-size:11px;text-align:center;background: #f2f2f2;padding:3px;"></div>';

$_['heading_title']	= 'Submit a review';
$_['successfull_review'] = 'Thank you for your review. It has been submitted to the webmaster for approval.';
$_['text_errors'] = 'We\'ve encountered the following errors:';

$_['text_discount'] = 'We would like to give you a special discount code - <strong>{discount_code}</strong> - which gives you <strong>{discount_value} OFF</strong>. <br />The code applies after you spent <strong>{total_amount}</strong>. This promotion is just for you and expires on <strong>{date_end}</strong>.';

$_['no_reviews']	= 'Not a single review was written. Please, write a review to at least one product.';

$_['error_form']	= 'There seems to be an issue with the submission of the form. If the issue persist, you can try the web-based form which you can open from the link in the email message.';

$_['error_duplicate'] = 'You have already reviewed the products from that order.';
?>