$(document).ready(function() {

	// Set last page opened on the menu
	$('#mpblog-menu a[href]').on('click', function() {
		sessionStorage.setItem('mpblog_menu', $(this).attr('href'));
	});

	if (!sessionStorage.getItem('mpblog_menu')) {
		$('#mpblog-menu #dashboard').addClass('active');
	} else {
		// Sets active and open to selected page in the left column menu.
		$('#mpblog-menu a[href=\'' + sessionStorage.getItem('mpblog_menu') + '\']').parents('li').addClass('active open');
	}

	// check menu from url and make it active
	if(sessionStorage.getItem('mpblog_menu') != window.location) {
		$.each($('#mpblog-menu a'), function() {
			if($(this).attr('href') == window.location) {
				$('#mpblog-menu a[href=\'' + sessionStorage.getItem('mpblog_menu') + '\']').parents('li').removeClass('active open');
				$(this).parents('li').addClass('active open');
				sessionStorage.setItem('mpblog_menu', $(this).attr('href'));
			}
		});
	}


	$('#mpblog-menu li li.active').has('ul').children('ul').addClass('collapse in');
	$('#mpblog-menu li li').not('.active').has('ul').children('ul').addClass('collapse');


	// Menu
	$('#mpblog-menu').find('li').has('ul').children('a').on('click', function() {
		 if (!$(this).parent().parent().is('#mpblog-menu')) {
			$(this).parent('li').toggleClass('open').children('ul').collapse('toggle');
			$(this).parent('li').siblings().removeClass('open').children('ul.in').collapse('hide');
		}
	});	
});