<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
<style type="text/css">
body {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
}
body, td, th, input, textarea, select, a {
	font-size: 12px;
}
p {
	margin-top: 0px;
	margin-bottom: 20px;
}
a, a:visited, a b {
	 cursor: pointer;
} 
a img {
	border: none;
}
#container {
	width: 680px;
}
.button{
    color: #fff !important;
    background-color: #f49a25;
    height: 34px;
    display: inline-block;
    padding: 0px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border-radius: 4px;
    padding-top: 1%;
    text-decoration: none !important;
}
</style>
</head>
<body>
<div id="container">
   
  <div>
    Hey <span style="text-transform: capitalize;"><?php echo $name; ?> </span>,
    <p><?php echo $text_greeting; ?></p>
    <p><?php echo $text_from; ?></p>
    <?php if ($message) { ?>
    <p><?php echo $text_message; ?></p>
    <p><?php echo $message; ?></p>
    <?php } ?>
    <p><?php echo $text_redeem_1; ?></p>
    <p><?php echo $text_redeem_2; ?></p>
    <p><?php echo $text_redeem; ?></p>
    
    <p><a href="https://www.true-elements.com/index.php?route=account/redeemvoucher&code=<?php echo $code; ?>" title="<?php echo $store_name; ?>" class="button">Redeem Here</a></p>
    
    <p><?php echo $text_footer; ?></p>
      <!--img src="https://www.true-elements.com/image/catalog/makemymuesli.jpg" width="100%">
      <img src="<?php echo $image; ?>" alt="<?php echo $store_name; ?>" /-->
      <img src="<?php echo $image; ?>" style="width: 69%;">
  </div>
</div>
</body>
</html>
