<style>
    .navbar {
    
        display: -ms-flexbox !important;
    }
</style>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
       
        
          <table class="table table-striped table-bordered" cellspacing="50" style="  border-spacing: 0px 0px;" width="100%" id="example">
            <thead>
              <tr>
			    <?php if ($filter_product_id == "") { ?>
					<td class="text-left"><?php echo $column_product_name; ?></td>
					<td class="text-left"><?php echo $column_model; ?></td>
					<td class="text-right"><?php echo $column_quantity; ?></td>
					<td class="text-right"><?php echo $column_total; ?></td>
				<?php } else { ?>
					<td class="text-left"><?php echo $column_customer_name; ?></td>
					<td class="text-left"><?php echo $column_email; ?></td>
					<td class="text-right"><?php echo $column_country; ?></td>
					<td class="text-right"><?php echo $column_order_status; ?></td>
					<td class="text-right"><?php echo $column_order_date; ?></td>
					<td class="text-right"><?php echo $column_quantity; ?></td>
					<td class="text-right"><?php echo $column_total; ?></td>
				<?php } ?>
              </tr>
            </thead>
            <tbody>
              <?php if ($products) { ?>
			  <?php if ($filter_product_id != "") { ?>
			  <?php
				$back_url = "index.php?route=report/product_purchased_advanced&token=".$token;
				if ($filter_date_start != "")
					$back_url .= "&filter_date_start=".$filter_date_start;
				if ($filter_date_end != "")
					$back_url .= "&filter_date_end=".$filter_date_end;
				if ($filter_date_shipped_start != "")
					$back_url .= "&filter_date_shipped_start=".$filter_date_shipped_start;
				if ($filter_date_shipped_end != "")
					$back_url .= "&filter_date_shipped_end=".$filter_date_shipped_end;
				if ($filter_order_status_id != "")
					$back_url .= "&filter_order_status_id=".$filter_order_status_id;	
					
				?>
				    <tr><td class="text-left" colspan="7"><a class="btn btn-info" href="<?php echo $back_url; ?>"><i class="fa fa-arrow-left"></i> BACK</a></td></tr>
				  	<tr><td style="background-color:#f5f5f5; padding-top:15px; padding-left:15px;" class="text-left" colspan="7"><h3><i class="fa fa-cubes"></i> <?php echo $products[0]['name']; ?></h3></td>
              <?php }  ?>
              <?php foreach ($products as $product) { ?>
              <tr>
			    <?php if ($filter_product_id != "") { ?>
				
					<td class="text-left">
						<a href="index.php?route=sale/order/info&token=<?php echo $token; ?>&order_id=<?php echo $product['order_id']; ?>" data-toggle="tooltip" title="View" class="btn btn-info"><i class="fa fa-eye"></i></a> 
						<?php echo $product['customer']; ?>
					</td>
					<td class="text-left"><?php echo $product['email']; ?></td>
					<td class="text-right"><?php echo $product['country']; ?></td>
					<td class="text-right"><?php echo $product['order_status']; ?></td>
					<td class="text-right"><?php echo $product['order_date']; ?></td>
					<td class="text-right"><?php echo $product['quantity']; ?></td>
					<td class="text-right"><?php echo $product['total']; ?></td>
                <?php } else { ?>
					<?php
					$product_url = "index.php?route=report/product_purchased_advanced&token=".$token."&filter_product_id=".$product['product_id'];
					if ($filter_date_start != "")
						$product_url .= "&filter_date_start=".$filter_date_start;
					if ($filter_date_end != "")
						$product_url .= "&filter_date_end=".$filter_date_end;
					if ($filter_date_shipped_start != "")
					    $product_url .= "&filter_date_shipped_start=".$filter_date_shipped_start;
				    if ($filter_date_shipped_end != "")
					    $product_url .= "&filter_date_shipped_end=".$filter_date_shipped_end;
					if ($filter_order_status_id != "")
						$product_url .= "&filter_order_status_id=".$filter_order_status_id;	
					?>
					<td class="text-left"><a href="<?php echo $product_url ?>"><?php echo $product['name']; ?></a></td></td>
                    <td class="text-left"><?php echo $product['model']; ?></td>
					<td class="text-right"><?php echo $product['quantity']; ?></td>
					<td class="text-right"><?php echo $product['total']; ?></td>
				<?php } ?>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
     
        
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	<?php if(isset($product['product_id'])) { ?>
		url = 'index.php?route=report/product_purchased_advanced&token=<?php echo $token; ?>&filter_product_id=<?php echo $product['product_id'] ?>';
	<?php }	else { ?>
		url = 'index.php?route=report/product_purchased_advanced&token=<?php echo $token; ?>';
	<?php } ?>
	var filter_date_start = $('input[name=\'filter_date_start\']').val();
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();
	
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}
	
	var filter_order_status_id = $('select[name=\'filter_order_status_id\']').val();
	
	if (filter_order_status_id != 0) {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script> 
</div>

<script type="text/javascript"><!--
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'lBfrtip',
        buttons: [
            'csv', 'excel'
        ],
        "pagingType": "full_numbers",
        "pageLength": 50
    } );
} );

</script>
<?php echo $footer; ?>