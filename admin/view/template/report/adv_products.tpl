<?php echo $header; ?> 
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right" style="white-space:nowrap;"><span style="padding-top:5px; padding-right:5px; font-size:11px; color:#666; text-align:right;"><?php echo $heading_version; ?></span></div>    
      <h1 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  
    <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
      <div align="right">
      <button type="button" onclick="filter()" title="<?php echo $button_filter; ?> Report" class="btn btn-primary" style="margin-bottom:10px;"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>&nbsp;
      <button type="button" data-toggle="modal" data-target="#load_save" data-backdrop="static" title="<?php echo $button_load_save; ?> Report" class="btn btn-load-save" style="margin-bottom:10px;"><i class="fa fa-search-plus"></i> <?php echo $button_load_save; ?></button>&nbsp;
      <button type="button" data-toggle="modal" data-target="#export" data-backdrop="static" title="<?php echo $button_export; ?> Report" class="btn btn-success" style="margin-bottom:10px;"><i class="fa fa-save"></i> <?php echo $button_export; ?></button>&nbsp;
      <?php if ($products) { ?><?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders' && $filter_details != 'all_details' && $filter_group == 'no_group') { ?><button type="button" title="Show / Hide <?php echo $button_chart; ?>" class="btn btn-danger chart-button" style="margin-bottom:10px;"><i class="fa fa-area-chart"></i> <?php echo $button_chart; ?></button>&nbsp;<?php } ?><?php } ?>
	  <div class="btn-group" style="margin-bottom:10px;">
		<button type="button" class="btn btn-set dropdown-toggle" data-toggle="dropdown" title="<?php echo $button_settings; ?>"><i class="fa fa-cog"></i> <?php echo $button_settings; ?> <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li><a href="#" data-toggle="modal" data-target="#settings"><?php echo $text_report_settings; ?></a></li>           
            <li><a href="#" data-toggle="modal" data-target="#cron"><?php echo $text_cron_settings; ?></a></li>
		  </ul>
	  </div>&nbsp;
      <button type="button" onclick="window.open('http://www.opencartreports.com/documentation/pp/index.html');" title="<?php echo $button_documentation; ?>" class="btn btn-warning" style="margin-bottom:10px;"><i class="fa fa-book"></i> <?php echo $button_documentation; ?></button>
      <div class="row">
      	  <div class="col-lg-3" style="z-index:5;"><div class="input-group" style="width:100%;"><span class="input-group-addon" style="width:35%; text-align:left; font-weight:bold; color:#666;"><?php echo $entry_report; ?></span>
          <select name="filter_report" id="filter_report" onchange="checkValidOptions(); filter();" data-style="btn-type" data-width="65%" class="select show-tick"> 
			<?php foreach ($report as $report) { ?>
			<?php if ($report['value'] == $filter_report) { ?>
			<option value="<?php echo $report['value']; ?>" title="<?php echo $report['text']; ?>" data-subtext="<?php echo $report['subtext']; ?>" data-divider="<?php echo $report['divider']; ?>" selected="selected"><?php echo $report['text']; ?></option>
			<?php } else { ?>
			<option value="<?php echo $report['value']; ?>" title="<?php echo $report['text']; ?>" data-subtext="<?php echo $report['subtext']; ?>" data-divider="<?php echo $report['divider']; ?>"><?php echo $report['text']; ?></option>
			<?php } ?>
			<?php } ?>
          </select></div></div>
          <div class="col-lg-3" style="z-index:4;"><div class="input-group" style="width:100%;"><span class="input-group-addon" style="width:35%; text-align:left; font-weight:bold; color:#666;"><?php echo $entry_show_details; ?></span>
		  <select name="filter_details" id="filter_details" onchange="checkValidOptions();" data-style="btn-select" data-width="65%" class="select" <?php echo ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? 'disabled="disabled"' : '' ?>>                      
			<?php foreach ($details as $details) { ?>
			<?php if ($details['value'] == $filter_details) { ?>
			<option value="<?php echo $details['value']; ?>" title="<?php echo $details['text']; ?>" selected="selected"><?php echo $details['text']; ?></option>
			<?php } else { ?>
			<option value="<?php echo $details['value']; ?>" title="<?php echo $details['text']; ?>"><?php echo $details['text']; ?></option>
			<?php } ?> 
            <?php } ?>              
          	<?php if ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') { ?>
			<option value="" selected="selected"></option>
			<?php } ?>               
          </select></div></div>          
      	  <div class="col-lg-2" style="z-index:3;"><div class="input-group" style="width:100%;"><span class="input-group-addon" style="width:35%; text-align:left; font-weight:bold; color:#666;"><?php echo $entry_group; ?></span>
          <select name="filter_group" id="filter_group" data-style="btn-select" data-width="65%" class="select" <?php echo ($filter_details == 'all_details' or $filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? 'disabled="disabled"' : '' ?>> 
			<?php foreach ($group as $group) { ?>

			<?php if ($group['value'] == $filter_group) { ?>
			<option value="<?php echo $group['value']; ?>" selected="selected"><?php echo $group['text']; ?></option>
			<?php } else { ?>
			<option value="<?php echo $group['value']; ?>"><?php echo $group['text']; ?></option>
			<?php } ?>
			<?php } ?> 
          	<?php if ($filter_details == 'all_details' or $filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') { ?>
			<option value="" selected="selected"></option>
			<?php } ?>                        
          </select></div></div>
          <div class="col-lg-2" style="z-index:2;"><div class="input-group" style="width:100%;"><span class="input-group-addon" style="width:35%; text-align:left; font-weight:bold; color:#666;"><?php echo $entry_sort_by; ?></span>
		  <select name="filter_sort" id="filter_sort" data-style="btn-select" data-width="65%" class="select" <?php echo ($filter_details == 'all_details') ? 'disabled="disabled"' : '' ?>>
			<?php foreach ($sort as $sort) { ?>
			<?php if ($sort['value'] == $filter_sort) { ?>
			<option id="<?php echo $sort['value']; ?>" value="<?php echo $sort['value']; ?>" title="<?php echo $sort['text']; ?>" selected="selected"><?php echo $sort['text']; ?></option>
			<?php } else { ?>
			<option id="<?php echo $sort['value']; ?>" value="<?php echo $sort['value']; ?>" title="<?php echo $sort['text']; ?>"><?php echo $sort['text']; ?></option>
			<?php } ?> 
            <?php } ?>         
          	<?php if ($filter_details == 'all_details') { ?>
			<option value="" selected="selected"></option>
			<?php } ?>            
          </select></div></div>
          <div class="col-lg-2" style="z-index:1;"><div class="input-group" style="width:100%;"><span class="input-group-addon" style="width:35%; text-align:left; font-weight:bold; color:#666;"><?php echo $entry_limit; ?></span>
		  <select name="filter_limit" id="filter_limit" data-style="btn-select" data-width="65%" class="select"> 
			<?php foreach ($limit as $limit) { ?>
			<?php if ($limit['value'] == $filter_limit) { ?>
			<option value="<?php echo $limit['value']; ?>" title="<?php echo $limit['text']; ?>" selected="selected"><?php echo $limit['text']; ?></option>
			<?php } else { ?>
			<option value="<?php echo $limit['value']; ?>" title="<?php echo $limit['text']; ?>"><?php echo $limit['text']; ?></option>
			<?php } ?> 
            <?php } ?>            
          </select></div></div>
		</div>          
	  </div>
	</div>
	
 
<div class="panel-body">
<div class="well">
    <div class="row" style="margin-bottom:10px;">
      <div class=<?php echo (($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_abandoned_orders') ? "col-lg-6" : ($filter_report == 'products_without_orders' ? "col-lg-12" : "col-lg-4")) ?> style="background:#f2f2f2; border:1px solid #DDD; margin-top:1px;">
        <div align="center" style="margin-top:5px;"><label class="control-label"><?php echo ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? $entry_product_added : ($filter_report == 'products_abandoned_orders' ? $entry_order_abandoned : $entry_order_created) ?></label></div>     
        <div class="row">
          <div class="col-sm-6" style="padding-bottom:15px;"><?php echo $entry_range; ?><br /> 
            <select name="filter_range" id="filter_range" data-style="btn-select" class="form-control select" style="z-index:5;">
              <?php foreach ($ranges as $range) { ?>
              <?php if ($range['value'] == $filter_range) { ?>
              <option value="<?php echo $range['value']; ?>" title="<?php echo $range['text']; ?>" style="<?php echo $range['style']; ?>" selected="selected"><?php echo $range['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $range['value']; ?>" title="<?php echo $range['text']; ?>" style="<?php echo $range['style']; ?>"><?php echo $range['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select></div>
          <div class="col-sm-3" style="padding-bottom:15px;"><span style="white-space:nowrap;"><?php echo $entry_date_start; ?></span><br />
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" data-date-format="YYYY-MM-DD" id="date-start" class="form-control" style="color:#F90;" />
		  </div>
          <div class="col-sm-3" style="padding-bottom:15px;"><span style="white-space:nowrap;"><?php echo $entry_date_end; ?></span><br />
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" data-date-format="YYYY-MM-DD" id="date-end" class="form-control" style="color:#F90;" />
          </div>
        </div>  
      </div> 
	  <?php if ($filter_report != 'products_without_orders' && $filter_report != 'products_abandoned_orders') { ?>
      <div class=<?php echo ($filter_report == 'all_products_with_without_orders') ? "col-lg-6" : "col-lg-4" ?> style="background:#f2f2f2; border:1px solid #DDD; margin-top:1px;">
        <div align="center" style="margin-top:5px;"><label class="control-label"><?php echo ($filter_report == 'all_products_with_without_orders') ? substr($entry_status,0,-1) : $entry_status_changed ?></label></div>       
        <div class="row">
          <?php if ($filter_report != 'all_products_with_without_orders') { ?>
          <div class="col-sm-3" style="padding-bottom:15px;"><span style="white-space:nowrap;"><?php echo $entry_date_start; ?></span><br />
            <input type="text" name="filter_status_date_start" value="<?php echo $filter_status_date_start; ?>" data-date-format="YYYY-MM-DD" id="status-date-start" class="form-control" style="color:#F90;">
		  </div>
          <div class="col-sm-3" style="padding-bottom:15px;"><span style="white-space:nowrap;"><?php echo $entry_date_end; ?></span><br />
            <input type="text" name="filter_status_date_end" value="<?php echo $filter_status_date_end; ?>" data-date-format="YYYY-MM-DD" id="status-date-end" class="form-control" style="color:#F90;">
          </div>
          <?php } ?>      
          <div class=<?php echo ($filter_report == 'all_products_with_without_orders') ? "col-sm-12" : "col-sm-6" ?> style="padding-bottom:15px;"><?php echo $entry_status; ?><br />
            <select name="filter_order_status_id" id="filter_order_status_id" class="form-control" multiple="multiple" size="1">
              <?php foreach ($order_statuses as $order_status) { ?>
              <?php if (in_array($order_status['order_status_id'], $filter_order_status_id)) { ?>
              <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select></div>    
        </div>  
      </div> 
	  <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <div class=<?php echo ($filter_report == 'products_abandoned_orders' ? "col-lg-3" : "col-lg-2") ?> style="background:#f2f2f2; border:1px solid #DDD; margin-top:1px;">
        <div align="center" style="margin-top:5px;"><label class="control-label"><?php echo $entry_order_id; ?></label></div>       
        <div class="row">
          <div class="col-sm-6" style="padding-bottom:15px;"><?php echo $entry_order_id_from; ?><br />
            <input type="text" name="filter_order_id_from" value="<?php echo $filter_order_id_from; ?>" size="12" class="form-control">
		  </div>
          <div class="col-sm-6" style="padding-bottom:15px;"><?php echo $entry_order_id_to; ?><br />
            <input type="text" name="filter_order_id_to" value="<?php echo $filter_order_id_to; ?>" size="12" class="form-control">
          </div>
        </div>  
      </div>  
      <div class=<?php echo ($filter_report == 'products_abandoned_orders' ? "col-lg-3" : "col-lg-2") ?> style="background:#f2f2f2; border:1px solid #DDD; margin-top:1px;">
        <div align="center" style="margin-top:5px;"><label class="control-label"><?php echo $entry_price_value; ?></label></div>       
        <div class="row">
          <div class="col-sm-6" style="padding-bottom:15px;"><?php echo $entry_price_value_min; ?><br />
            <input type="text" name="filter_prod_price_min" value="<?php echo $filter_prod_price_min; ?>" size="12" class="form-control" />
		  </div>
          <div class="col-sm-6" style="padding-bottom:15px;"><?php echo $entry_price_value_max; ?><br />
            <input type="text" name="filter_prod_price_max" value="<?php echo $filter_prod_price_max; ?>" size="12" class="form-control" />
          </div>
        </div>  
      </div>       
      <?php } ?>      
    </div>
    <div class="row"> 
      <?php if (in_array('store', $advpp_settings_filters)) { ?>
   		<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_store_id" style="white-space:nowrap;"><?php echo $entry_store; ?></label>
          <select name="filter_store_id" id="filter_store_id" class="form-control" multiple="multiple" size="1">
            <?php foreach ($stores as $store) { ?>   
            <?php if (in_array($store['store_id'], $filter_store_id)) { ?>        
            <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['store_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $store['store_id']; ?>"><?php echo $store['store_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
	  <?php } ?>      
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	  <?php if (in_array('currency', $advpp_settings_filters)) { ?>  
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_currency" style="white-space:nowrap;"><?php echo $entry_currency; ?></label>
          <select name="filter_currency" id="filter_currency" class="form-control" multiple="multiple" size="1">
            <?php foreach ($currencies as $currency) { ?>
            <?php if (in_array($currency['currency_id'], $filter_currency)) { ?>
            <option value="<?php echo $currency['currency_id']; ?>" selected="selected"><?php echo $currency['title']; ?> (<?php echo $currency['code']; ?>)</option>
            <?php } else { ?>
            <option value="<?php echo $currency['currency_id']; ?>"><?php echo $currency['title']; ?> (<?php echo $currency['code']; ?>)</option>
            <?php } ?>
            <?php } ?>
          </select></div>
	  <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>     
	  <?php if (in_array('tax', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_taxes" style="white-space:nowrap;"><?php echo $entry_tax; ?></label>
		  <select name="filter_taxes" id="filter_taxes" class="form-control" multiple="multiple" size="1">
            <?php foreach ($taxes as $tax) { ?>
            <?php if (in_array($tax['tax'], $filter_taxes)) { ?> 
            <option value="<?php echo $tax['tax']; ?>" selected="selected"><?php echo $tax['tax_title']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $tax['tax']; ?>"><?php echo $tax['tax_title']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
	  <?php } ?>
      <?php } ?>
	  <?php if (in_array('tax_class', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
   		<label class="control-label" for="filter_tax_classes" style="white-space:nowrap;"><?php echo $entry_tax_classes; ?></label>
		  <select name="filter_tax_classes" id="filter_tax_classes" class="form-control" multiple="multiple" size="1">
            <?php foreach ($tax_classes as $tax_class) { ?>
            <?php if (in_array($tax_class['tax_class'], $filter_tax_classes)) { ?>              
            <option value="<?php echo $tax_class['tax_class']; ?>" selected="selected"><?php echo $tax_class['tax_class_title']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $tax_class['tax_class']; ?>"><?php echo $tax_class['tax_class_title']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>      
	  <?php if (in_array('geo_zone', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_geo_zones" style="white-space:nowrap;"><?php echo $entry_geo_zone; ?></label>
		  <select name="filter_geo_zones" id="filter_geo_zones" class="form-control" multiple="multiple" size="1">
            <?php foreach ($geo_zones as $geo_zone) { ?>
            <?php if (in_array($geo_zone['geo_zone_country_id'], $filter_geo_zones)) { ?>
            <option value="<?php echo $geo_zone['geo_zone_country_id']; ?>" selected="selected"><?php echo $geo_zone['geo_zone_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $geo_zone['geo_zone_country_id']; ?>"><?php echo $geo_zone['geo_zone_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
	  <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>          
	  <?php if (in_array('customer_group', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_customer_group_id" style="white-space:nowrap;"><?php echo $entry_customer_group; ?></label>
          <select name="filter_customer_group_id" id="filter_customer_group_id" class="form-control" multiple="multiple" size="1">
            <?php foreach ($customer_groups as $customer_group) { ?>
            <?php if (in_array($customer_group['customer_group_id'], $filter_customer_group_id)) { ?>
            <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>   
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>         
      <?php if (in_array('customer_name', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_customer_name" style="white-space:nowrap;"><?php echo $entry_customer_name; ?></label>
        <input type="text" name="filter_customer_name" id="filter_customer_name" value="<?php echo $filter_customer_name; ?>" class="form-control" style="color:#F90;" />
		</div>
      <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('customer_email', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_customer_email" style="white-space:nowrap;"><?php echo $entry_customer_email; ?></label>
        <input type="text" name="filter_customer_email" id="filter_customer_email" value="<?php echo $filter_customer_email; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>   
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('customer_telephone', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_customer_telephone" style="white-space:nowrap;"><?php echo $entry_customer_telephone; ?></label>
        <input type="text" name="filter_customer_telephone" id="filter_customer_telephone" value="<?php echo $filter_customer_telephone; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('ip', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_ip" style="white-space:nowrap;"><?php echo $entry_ip; ?></label>
        <input type="text" name="filter_ip" id="filter_ip" value="<?php echo $filter_ip; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('payment_company', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_payment_company" style="white-space:nowrap;"><?php echo ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? substr(strstr($entry_payment_company,' '), 1) : $entry_payment_company ?></label>
        <input type="text" name="filter_payment_company" id="filter_payment_company" value="<?php echo $filter_payment_company; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('payment_address', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_payment_address" style="white-space:nowrap;"><?php echo ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? substr(strstr($entry_payment_address,' '), 1) : $entry_payment_address ?></label>
        <input type="text" name="filter_payment_address" id="filter_payment_address" value="<?php echo $filter_payment_address; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('payment_city', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_payment_city" style="white-space:nowrap;"><?php echo ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? substr(strstr($entry_payment_city,' '), 1) : $entry_payment_city ?></label>
        <input type="text" name="filter_payment_city" id="filter_payment_city" value="<?php echo $filter_payment_city; ?>" class="form-control" style="color:#F90;" />
		</div>
      <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('payment_zone', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_payment_zone" style="white-space:nowrap;"><?php echo ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? substr(strstr($entry_payment_zone,' '), 1) : $entry_payment_zone ?></label>
        <input type="text" name="filter_payment_zone" id="filter_payment_zone" value="<?php echo $filter_payment_zone; ?>" class="form-control" style="color:#F90;" />
		</div>
      <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('payment_postcode', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_payment_postcode" style="white-space:nowrap;"><?php echo ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? substr(strstr($entry_payment_postcode,' '), 1) : $entry_payment_postcode ?></label>
        <input type="text" name="filter_payment_postcode" id="filter_payment_postcode" value="<?php echo $filter_payment_postcode; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
	  <?php } ?>   
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('payment_country', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_payment_country" style="white-space:nowrap;"><?php echo ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') ? substr(strstr($entry_payment_country,' '), 1) : $entry_payment_country ?></label>
        <input type="text" name="filter_payment_country" id="filter_payment_country" value="<?php echo $filter_payment_country; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>      
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>       
      <?php if (in_array('payment_method', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_payment_method" style="white-space:nowrap;"><?php echo $entry_payment_method; ?></label>
		  <select name="filter_payment_method" id="filter_payment_method" class="form-control" multiple="multiple" size="1">
            <?php foreach ($payment_methods as $payment_method) { ?>
            <?php if (in_array($payment_method['payment_code'], $filter_payment_method)) { ?>
            <option value="<?php echo $payment_method['payment_code']; ?>" selected="selected"><?php echo preg_replace('~\(.*?\)~', '', $payment_method['payment_method']); ?></option>
            <?php } else { ?>
            <option value="<?php echo $payment_method['payment_code']; ?>"><?php echo preg_replace('~\(.*?\)~', '', $payment_method['payment_method']); ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>   
      <?php if (in_array('shipping_company', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_shipping_company" style="white-space:nowrap;"><?php echo $entry_shipping_company; ?></label>
        <input type="text" name="filter_shipping_company" id="filter_shipping_company" value="<?php echo $filter_shipping_company; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('shipping_address', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_shipping_address" style="white-space:nowrap;"><?php echo $entry_shipping_address; ?></label>
        <input type="text" name="filter_shipping_address" id="filter_shipping_address" value="<?php echo $filter_shipping_address; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('shipping_city', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_shipping_city" style="white-space:nowrap;"><?php echo $entry_shipping_city; ?></label>
        <input type="text" name="filter_shipping_city" id="filter_shipping_city" value="<?php echo $filter_shipping_city; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('shipping_zone', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_shipping_zone" style="white-space:nowrap;"><?php echo $entry_shipping_zone; ?></label>
        <input type="text" name="filter_shipping_zone" id="filter_shipping_zone" value="<?php echo $filter_shipping_zone; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('shipping_postcode', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_shipping_postcode" style="white-space:nowrap;"><?php echo $entry_shipping_postcode; ?></label>
        <input type="text" name="filter_shipping_postcode" id="filter_shipping_postcode" value="<?php echo $filter_shipping_postcode; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('shipping_country', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_shipping_country" style="white-space:nowrap;"><?php echo $entry_shipping_country; ?></label>
        <input type="text" name="filter_shipping_country" id="filter_shipping_country" value="<?php echo $filter_shipping_country; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('shipping_method', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_shipping_method" style="white-space:nowrap;"><?php echo $entry_shipping_method; ?></label>
		  <select name="filter_shipping_method" id="filter_shipping_method" class="form-control" multiple="multiple" size="1">
            <?php foreach ($shipping_methods as $shipping_method) { ?>
            <?php if (in_array($shipping_method['shipping_code'], $filter_shipping_method)) { ?>
            <option value="<?php echo $shipping_method['shipping_code']; ?>" selected="selected"><?php echo preg_replace('~\(.*?\)~', '', $shipping_method['shipping_method']); ?></option>
            <?php } else { ?>
            <option value="<?php echo $shipping_method['shipping_code']; ?>"><?php echo preg_replace('~\(.*?\)~', '', $shipping_method['shipping_method']); ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php } ?>
      <?php if (in_array('category', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_category" style="white-space:nowrap;"><?php echo $entry_category; ?></label>
          <select name="filter_category" id="filter_category" class="form-control" multiple="multiple" size="1">
            <?php foreach ($categories as $category) { ?>          
			<?php if (in_array($category['category_id'], $filter_category)) { ?>
			<option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
			<?php } else { ?>
			<option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
			<?php } ?>
			<?php } ?>
          </select></div>
      <?php } ?>
      <?php if (in_array('manufacturer', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_manufacturer" style="white-space:nowrap;"><?php echo $entry_manufacturer; ?></label>
          <select name="filter_manufacturer" id="filter_manufacturer" class="form-control" multiple="multiple" size="1">
            <?php foreach ($manufacturers as $manufacturer) { ?>
            <?php if (in_array($manufacturer['manufacturer_id'], $filter_manufacturer)) { ?>
            <option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option> 
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php if (in_array('sku', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_sku" style="white-space:nowrap;"><?php echo $entry_sku; ?></label>
        <input type="text" name="filter_sku" id="filter_sku" value="<?php echo $filter_sku; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php if (in_array('product', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-4" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_product_name" style="white-space:nowrap;"><?php echo $entry_product; ?></label>
        <input type="text" name="filter_product_name" id="filter_product_name" value="<?php echo $filter_product_name; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php if (in_array('model', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_model" style="white-space:nowrap;"><?php echo $entry_model; ?></label>
        <input type="text" name="filter_model" id="filter_model" value="<?php echo $filter_model; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('option', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_option" style="white-space:nowrap;"><?php echo $entry_option; ?></label>
          <select name="filter_option" id="filter_option" class="form-control" multiple="multiple" size="1" <?php echo ($filter_report == 'products_purchased_with_options') ? '' : 'disabled="disabled"' ?>>
            <?php foreach ($order_options as $order_option) { ?>
            <?php if (in_array($order_option['options'], $filter_option)) { ?>
            <option value="<?php echo $order_option['options']; ?>" selected="selected"><?php echo $order_option['option_name']; ?>: <?php echo $order_option['option_value']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_option['options']; ?>"><?php echo $order_option['option_name']; ?>: <?php echo $order_option['option_value']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php } ?>
      <?php if (in_array('attribute', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_attribute" style="white-space:nowrap;"><?php echo $entry_attributes; ?></label>
		  <select name="filter_attribute" id="filter_attribute" class="form-control" multiple="multiple" size="1">
            <?php foreach ($attributes as $attribute) { ?>
            <?php if (in_array($attribute['attribute_title'], $filter_attribute)) { ?>
            <option value="<?php echo $attribute['attribute_title']; ?>" selected="selected"><?php echo $attribute['attribute_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $attribute['attribute_title']; ?>"><?php echo $attribute['attribute_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php if (in_array('product_status', $advpp_settings_filters)) { ?>
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_product_status" style="white-space:nowrap;"><?php echo $entry_product_status; ?></label>
          <select name="filter_product_status" id="filter_product_status" multiple="multiple" size="1">
            <?php foreach ($product_statuses as $product_status) { ?>
            <?php if (in_array($product_status['status'], $filter_product_status) && $product_status['status'] == 1) { ?>
            <option value="<?php echo $product_status['status']; ?>" selected="selected"><?php echo $text_enabled; ?></option>
            <?php } elseif (!in_array($product_status['status'], $filter_product_status) && $product_status['status'] == 1) { ?>
            <option value="<?php echo $product_status['status']; ?>"><?php echo $text_enabled; ?></option>
            <?php } ?>
            <?php if (in_array($product_status['status'], $filter_product_status) && $product_status['status'] == 0) { ?>
            <option value="<?php echo $product_status['status']; ?>" selected="selected"><?php echo $text_disabled; ?></option>
            <?php } elseif (!in_array($product_status['status'], $filter_product_status) && $product_status['status'] == 0) { ?>
            <option value="<?php echo $product_status['status']; ?>"><?php echo $text_disabled; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>        
      <?php if (in_array('location', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_location" style="white-space:nowrap;"><?php echo $entry_location; ?></label>
		  <select name="filter_location" id="filter_location" class="form-control" multiple="multiple" size="1">
            <?php foreach ($locations as $location) { ?>
            <?php if (in_array($location['location_title'], $filter_location)) { ?>
            <option value="<?php echo $location['location_title']; ?>" selected="selected"><?php echo $location['location_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $location['location_title']; ?>"><?php echo $location['location_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('affiliate_name', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_affiliate_name" style="white-space:nowrap;"><?php echo $entry_affiliate_name; ?></label>
          <select name="filter_affiliate_name" id="filter_affiliate_name" class="form-control" multiple="multiple" size="1">
            <?php foreach ($affiliate_names as $affiliate_name) { ?>
            <?php if (in_array($affiliate_name['affiliate_id'], $filter_affiliate_name)) { ?>
            <option value="<?php echo $affiliate_name['affiliate_id']; ?>" selected="selected"><?php echo $affiliate_name['affiliate_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $affiliate_name['affiliate_id']; ?>"><?php echo $affiliate_name['affiliate_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('affiliate_email', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_affiliate_email" style="white-space:nowrap;"><?php echo $entry_affiliate_email; ?></label>
          <select name="filter_affiliate_email" id="filter_affiliate_email" class="form-control" multiple="multiple" size="1">
            <?php foreach ($affiliate_emails as $affiliate_email) { ?>
            <?php if (in_array($affiliate_email['affiliate_id'], $filter_affiliate_email)) { ?>
            <option value="<?php echo $affiliate_email['affiliate_id']; ?>" selected="selected"><?php echo $affiliate_email['affiliate_email']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $affiliate_email['affiliate_id']; ?>"><?php echo $affiliate_email['affiliate_email']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('coupon_name', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_coupon_name" style="white-space:nowrap;"><?php echo $entry_coupon_name; ?></label>
          <select name="filter_coupon_name" id="filter_coupon_name" class="form-control" multiple="multiple" size="1">
            <?php foreach ($coupon_names as $coupon_name) { ?>
            <?php if (in_array($coupon_name['coupon_id'], $filter_coupon_name)) { ?>
            <option value="<?php echo $coupon_name['coupon_id']; ?>" selected="selected"><?php echo $coupon_name['coupon_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $coupon_name['coupon_id']; ?>"><?php echo $coupon_name['coupon_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('coupon_code', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_coupon_code" style="white-space:nowrap;"><?php echo $entry_coupon_code; ?></label>
        <input type="text" name="filter_coupon_code" id="filter_coupon_code" value="<?php echo $filter_coupon_code; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>
      <?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?> 
      <?php if (in_array('voucher_code', $advpp_settings_filters)) { ?>      
    	<div class="col-lg-2" style="padding-bottom:15px;">
    	<label class="control-label" for="filter_voucher_code" style="white-space:nowrap;"><?php echo $entry_voucher_code; ?></label>
        <input type="text" name="filter_voucher_code" id="filter_voucher_code" value="<?php echo $filter_voucher_code; ?>" class="form-control" style="color:#F90;" />
        </div>
      <?php } ?>
      <?php } ?>      
      </div>
</div>

 <div class="modal fade" id="load_save" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-info">
    <div class="modal-content" id="modal-content">
      <div class="modal-header" style="background-color:#6ab0d6;">
        <button type="button" class="close" data-dismiss="modal" style="color:#FFF;"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $button_close; ?></span></button>
        <h4 class="modal-title" style="color:#FFF;"><?php echo $button_load_save; ?> Report</h4>
      </div>
      <div class="modal-body">
      <fieldset>
      <legend><?php echo $text_load_save_options; ?></legend> 
      <div class="table-responsive">
      <div><?php echo $text_load_save; ?></div><br />
        <table width="100%" id="adv_load_save" class="table table-bordered table-hover">
          <thead>
            <tr>
              <td width="5%"></td>
              <td width="50%" class="text-left"><?php echo $entry_title; ?></td>
              <td width="40%" class="text-left"><?php echo $entry_link; ?></td>              
              <td width="5%"></td>
            </tr>
          </thead>        
          <?php if ($advpp_load_save_reports) { ?>
		   <?php $adv_load_save_reports_row = 0; ?>
			<?php foreach ($advpp_load_save_reports as $advpp_load_save_report) { ?>
			  <tbody id="adv_load_save_reports_row<?php echo $adv_load_save_reports_row; ?>">
				<tr> 
				  <td width="5%" class="text-center"><button type="button" onclick="$('#adv_load_save_reports_row<?php echo $adv_load_save_reports_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>                
				  <td width="45%" class="text-left"><input type="text" name="advpp_load_save_report[<?php echo $adv_load_save_reports_row; ?>][save_report_title]" value="<?php echo $advpp_load_save_report['save_report_title']; ?>" placeholder="<?php echo $text_report_title; ?>" class="form-control" /></td>
				  <td width="40%" class="text-left"><textarea readonly name="advpp_load_save_report[<?php echo $adv_load_save_reports_row; ?>][save_report_link]" class="form-control"><?php echo $advpp_load_save_report['save_report_link']; ?></textarea></td>    
				  <td width="10%" class="text-center"><button type="button" onclick="location = '<?php echo ($report_link . str_replace('index.php?route=report/adv_products', '', $advpp_load_save_report['save_report_link'])); ?>'" data-toggle="tooltip" title="<?php echo $button_load; ?>" class="btn btn-primary"><i class="fa fa-circle-o-notch"></i> <?php echo $button_load; ?></button></td>                                
				</tr>
			  </tbody>
            <?php $adv_load_save_reports_row++; ?>
  		    <?php } ?>
          <?php } else { ?>
		     <?php $adv_load_save_reports_row = 0; ?>
		  <?php } ?>
		  <tfoot>
            <tr>
              <td colspan="3"></td>
              <td class="text-left"> <button type="button" onclick="addSaveReport(); this.parentNode.parentNode.style.display = 'none';" title="<?php echo $button_add_report; ?>" class="btn btn-load-save"><i class="fa fa-plus-circle"></i> <?php echo $button_add_report; ?></button></td>
            </tr>
          </tfoot>          
        </table>
        </div>
		</fieldset>
        <div align="right" style="padding-top:10px;">
		  <button type="button" class="btn btn-default" title="<?php echo $button_close; ?>" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo $button_close; ?></button>
		  <button type="button" class="btn btn-primary" id="save_report" title="<?php echo $button_save; ?>" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
		</div>
      </div>
    </div>
  </div>
</div>     
<div class="modal fade" id="export" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-info">
    <div class="modal-content" id="modal-content">
      <div class="modal-header" style="background-color:#5cb85c;">
        <button type="button" class="close" data-dismiss="modal" style="color:#FFF;"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $button_close; ?></span></button>
        <h4 class="modal-title" style="color:#FFF;"><?php echo $button_export; ?></h4>
      </div>
      <div class="modal-body">
      <fieldset>
        <legend><?php echo $text_export_options; ?></legend> 
		<div class="row" style="padding:2px;">
			<div class="col-sm-6" style="padding-bottom:15px;"><div id="report_to_export">
				<label for="report_type" class="control-label"><span data-toggle="tooltip" title="<?php echo $help_report_type; ?>"><?php echo $text_report_type; ?></span></label>
				<select id="report_type" name="report_type" data-style="btn-select" class="form-control select">
                	<?php foreach ($report_types as $report_type) { ?>                  
					<?php if ($report_type == $report_type['type']) { ?>
                    <option data-icon="<?php echo $report_type['icon']; ?>" value="<?php echo $report_type['type']; ?>" title="<?php echo $report_type['name']; ?>" selected="selected"><?php echo $report_type['name']; ?></option>
					<?php } else { ?>
					<option data-icon="<?php echo $report_type['icon']; ?>" value="<?php echo $report_type['type']; ?>" title="<?php echo $report_type['name']; ?>"><?php echo $report_type['name']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div></div>
			<div class="col-sm-6" style="padding-bottom:15px;"><div id="type_to_export" class="required">
				<label for="export_type" class="control-label"><?php echo $text_export_type; ?></label>
				<select id="export_type" name="export_type" data-style="btn-select" class="form-control select">
                	<option value=""></option>
                	<?php foreach ($export_types as $export_type) { ?>                
					<?php if ($export_type == $export_type['type']) { ?>
                    <option data-icon="<?php echo $export_type['icon']; ?>" value="<?php echo $export_type['type']; ?>" title="<?php echo $export_type['name']; ?>" selected="selected"><?php echo $export_type['name']; ?></option>
					<?php } else { ?>
					<option data-icon="<?php echo $export_type['icon']; ?>" value="<?php echo $export_type['type']; ?>" title="<?php echo $export_type['name']; ?>"><?php echo $export_type['name']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div></div>            
		</div> 
		<div class="row" style="padding:2px; padding-bottom:20px;">              
			<div class="col-sm-6" style="z-index:5;"><div class="input-group" style="width:100%;">
				<span class="input-group-addon" style="width:70%; text-align:left;"><?php echo $text_export_logo_criteria; ?></span>
				<select name="export_logo_criteria" data-style="btn-select" data-width="30%" class="select">
    				<?php if ($export_logo_criteria) { ?>
					<option value="1" title="<?php echo $text_yes; ?>" selected="selected"><?php echo $text_yes; ?></option>
					<option value="0" title="<?php echo $text_no; ?>"><?php echo $text_no; ?></option>
					<?php } else { ?>
					<option value="1" title="<?php echo $text_yes; ?>"><?php echo $text_yes; ?></option>
					<option value="0" title="<?php echo $text_no; ?>" selected="selected"><?php echo $text_no; ?></option>
					<?php } ?>
				</select>
			</div></div>
			<div class="col-sm-6"><div id="csv_delimiter" class="input-group" style="width:100%;">
				<span class="input-group-addon" style="width:55%; text-align:left;"><?php echo $text_export_csv_delimiter; ?></span>
				<select id="export_csv_delimiter" name="export_csv_delimiter" data-style="btn-select" data-width="45%" class="select">
                	<?php foreach ($export_csv_delimiters as $export_csv_delimiter) { ?>                
					<?php if ($export_csv_delimiter == $export_csv_delimiter['type']) { ?>
                    <option value="<?php echo $export_csv_delimiter['type']; ?>" title="<?php echo $export_csv_delimiter['name']; ?>" selected="selected"><?php echo $export_csv_delimiter['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $export_csv_delimiter['type']; ?>" title="<?php echo $export_csv_delimiter['name']; ?>"><?php echo $export_csv_delimiter['name']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div></div>
		</div>
        <div class="text-danger">* <?php echo $text_export_notice1; ?> <a href="http://www.opencartreports.com/documentation/pp/index.html#req_limit" target="_blank"><strong><?php echo $text_export_limit; ?></strong></a> <?php echo $text_export_notice2; ?></div>      
		</fieldset>
        <div align="right">
		  <button type="button" class="btn btn-default" title="<?php echo $button_close; ?>" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo $button_close; ?></button>
		  <button type="button" id="export_report" class="btn btn-primary" title="<?php echo $button_export; ?>" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-save"></i> <?php echo $button_export; ?></button>
		</div>
      </div>
    </div>
  </div>
</div>               
<div class="modal fade" id="settings" data-backdrop="static" data-keyboard="false" style="z-index:9999;">
  <div class="modal-dialog modal-lg modal-info">
    <div class="modal-content" id="modal-content">
      <div class="modal-header" style="background-color:#777;">
        <button type="button" class="close" data-dismiss="modal" style="color:#FFF;"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $button_close; ?></span></button>
        <h4 class="modal-title" style="color:#FFF;"><?php echo $text_report_settings; ?></h4>
      </div>
      <div class="modal-body">
      <fieldset>
		<legend><?php echo $text_local_settings; ?></legend> 
		<div class="row" style="padding:2px; padding-bottom:20px;">
			<div class="col-lg-4"><div class="input-group" style="width:100%;">
				<span class="input-group-addon" style="width:45%; text-align:left;"><?php echo $text_format_date; ?></span>
				<select name="advpp<?php echo $user; ?>_date_format" data-style="btn-select" data-width="55%" class="select">
					<?php if ($advpp_date_format == 'DDMMYYYY') { ?>
					<option value="DDMMYYYY" selected="selected"><?php echo $text_format_date_eu; ?></option>
					<option value="MMDDYYYY"><?php echo $text_format_date_us; ?></option>
					<?php } else { ?>
					<option value="DDMMYYYY"><?php echo $text_format_date_eu; ?></option>
					<option value="MMDDYYYY" selected="selected"><?php echo $text_format_date_us; ?></option>
					<?php } ?>
				</select>
			</div></div>
			<div class="col-lg-4"><div class="input-group" style="width:100%;">
				<span class="input-group-addon" style="width:45%; text-align:left;"><?php echo $text_format_hour; ?></span>
				<select name="advpp<?php echo $user; ?>_hour_format" data-style="btn-select" data-width="55%" class="select">
					<?php if ($advpp_hour_format == '24') { ?>
					<option value="24" selected="selected"><?php echo $text_format_hour_24; ?></option>
					<option value="12"><?php echo $text_format_hour_12; ?></option>
					<?php } else { ?>
					<option value="24"><?php echo $text_format_hour_24; ?></option>
					<option value="12" selected="selected"><?php echo $text_format_hour_12; ?></option>
					<?php } ?>
				</select>
			</div></div>            
			<div class="col-lg-4"><div class="input-group" style="width:100%;">
				<span class="input-group-addon" style="width:45%; text-align:left;"><?php echo $text_format_week; ?></span>
				<select name="advpp<?php echo $user; ?>_week_days" data-style="btn-select" data-width="55%" class="select">
					<?php if ($advpp_week_days == 'mon_sun') { ?>
					<option value="mon_sun" selected="selected"><?php echo $text_format_week_mon_sun; ?></option>
					<option value="sun_sat"><?php echo $text_format_week_sun_sat; ?></option>
					<?php } else { ?>
					<option value="mon_sun"><?php echo $text_format_week_mon_sun; ?></option>
					<option value="sun_sat" selected="selected"><?php echo $text_format_week_sun_sat; ?></option>
					<?php } ?>
				</select>
			</div></div>
		</div>        
<script type="text/javascript">
 function toggle_filters(ele) {
     var checkboxes = document.getElementsByName('advpp<?php echo $user; ?>_settings_filters[]');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
			 $("#check_filters").html('<span><strong><?php echo $text_uncheck_all; ?></strong></span>');
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
			 $("#check_filters").html('<span><strong><?php echo $text_check_all; ?></strong></span>');
         }
     }
 }
</script>
	
		<legend><?php echo $text_filtering_options; ?></legend> 
		<div class="row">
			<div class="col-sm-12">
				<div class="well well-sm" style="background:#f5f5f5; height:260px; overflow:auto;">
                	<div class="col-sm-12" align="right">
                    <label><input type="checkbox" onchange="toggle_filters(this)" style="opacity:0; position:fixed; left:9999px;" /> <span id="check_filters" style="cursor:pointer; border:thin dotted #666; padding:2px; background-color:#EEE;"><strong><?php echo $text_check_all; ?></strong></span></label>
                    </div>   
                    <input type="hidden" value="filters_null" name="advpp<?php echo $user; ?>_settings_filters[]" checked="checked"/>                 
					<?php foreach ($filters as $key => $filter) { ?>
						<div class="checkbox col-md-3">
						<label>
                        <?php if (in_array($key, $advpp_settings_filters)) { ?>
                        	<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_filters[]" checked="checked"/> <?php echo $filter; ?>
                        <?php } else { ?>
							<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_filters[]" /> <?php echo $filter; ?>
                        <?php } ?>
						</label>
						</div>
					<?php } ?>
				</div>										
			</div>
		</div>
	
	<script type="text/javascript">
 function toggle_mv_columns(ele) {
     var checkboxes = document.getElementsByName('advpp<?php echo $user; ?>_settings_mv_columns[]');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
			 $("#check_mv_columns").html('<span><strong><?php echo $text_uncheck_all; ?></strong></span>');
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
			 $("#check_mv_columns").html('<span><strong><?php echo $text_check_all; ?></strong></span>');
         }
     }
 }
</script>  
	
 
       
		<legend><?php echo $text_column_settings; ?> <span style="color:#390; font-size:small;"> [<?php echo $text_export_note; ?>]</span></legend>  
		<div class="row">
			<div class="col-sm-12">
               	<label class="control-label"><i class="fa fa-columns"></i> <?php echo $text_mv_columns; ?></label>
				<div class="well well-sm" style="background:#f5f5f5; height:175px; overflow:auto;">
                	<div class="col-sm-12" align="right">
                    <label><input type="checkbox" onchange="toggle_mv_columns(this)" style="opacity:0; position:fixed; left:9999px;" /> <span id="check_mv_columns" style="cursor:pointer; border:thin dotted #666; padding:2px; background-color:#EEE;"><strong><?php echo $text_check_all; ?></strong></span></label>
                    </div>   
                    <input type="hidden" value="mv_null" name="advpp<?php echo $user; ?>_settings_mv_columns[]" checked="checked"/>                 
					<?php foreach ($mv_columns as $key => $mv_column) { ?>
						<div class="checkbox col-md-3">
						<label>
                        <?php if (in_array($key, $advpp_settings_mv_columns)) { ?>
                        	<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_mv_columns[]" checked="checked"/> <?php echo $mv_column; ?>
                        <?php } else { ?>
							<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_mv_columns[]" /> <?php echo $mv_column; ?>
                        <?php } ?>
						</label>
						</div>
                    <?php } ?>
				</div>		               								
			</div>
		</div>
<script type="text/javascript">
 function toggle_ol_columns(ele) {
     var checkboxes = document.getElementsByName('advpp<?php echo $user; ?>_settings_ol_columns[]');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
			 $("#check_ol_columns").html('<span><strong><?php echo $text_uncheck_all; ?></strong></span>');
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
			 $("#check_ol_columns").html('<span><strong><?php echo $text_check_all; ?></strong></span>');
         }
     }
 }
</script>        
		<div class="row">
			<div class="col-sm-12">
               	<label class="control-label"><i class="fa fa-columns"></i> <?php echo $text_bd_columns; ?></label>
				<div class="well well-sm" style="background:#f5f5f5; height:155px; overflow:auto;">
                    <div style="margin-bottom:5px;"><small><?php echo $entry_report; ?> <b><?php echo $text_products_purchased; ?></b></small></div>
                	<div class="col-sm-12" align="right">
                    <label><input type="checkbox" onchange="toggle_ol_columns(this)" style="opacity:0; position:fixed; left:9999px;" /> <span id="check_ol_columns" style="cursor:pointer; border:thin dotted #666; padding:2px; background-color:#EEE;"><strong><?php echo $text_check_all; ?></strong></span></label>
                    </div>   
                    <input type="hidden" value="ol_null" name="advpp<?php echo $user; ?>_settings_ol_columns[]" checked="checked"/>                     
					<?php foreach ($ol_columns as $key => $ol_column) { ?>
						<div class="checkbox col-md-3">
						<label>
                        <?php if (in_array($key, $advpp_settings_ol_columns)) { ?>
                        	<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_ol_columns[]" checked="checked"/> <?php echo $ol_column; ?>
                        <?php } else { ?>
							<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_ol_columns[]" /> <?php echo $ol_column; ?>
                        <?php } ?>
						</label>
						</div>
					<?php } ?>
				</div>		               								
			</div>
		</div> 
<script type="text/javascript">
 function toggle_pl_columns(ele) {
     var checkboxes = document.getElementsByName('advpp<?php echo $user; ?>_settings_pl_columns[]');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
			 $("#check_pl_columns").html('<span><strong><?php echo $text_uncheck_all; ?></strong></span>');
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
			 $("#check_pl_columns").html('<span><strong><?php echo $text_check_all; ?></strong></span>');
         }
     }
 }
</script>        
		<div class="row">
			<div class="col-sm-12">
				<div class="well well-sm" style="background:#f5f5f5; height:175px; overflow:auto;">
                    <div style="margin-bottom:5px;"><small><?php echo $entry_report; ?> <b><?php echo $text_manufacturers; ?> / <?php echo $text_categories; ?></b></small></div>
                	<div class="col-sm-12" align="right">
                    <label><input type="checkbox" onchange="toggle_pl_columns(this)" style="opacity:0; position:fixed; left:9999px;" /> <span id="check_pl_columns" style="cursor:pointer; border:thin dotted #666; padding:2px; background-color:#EEE;"><strong><?php echo $text_check_all; ?></strong></span></label>
                    </div>   
                    <input type="hidden" value="pl_null" name="advpp<?php echo $user; ?>_settings_pl_columns[]" checked="checked"/>                     
					<?php foreach ($pl_columns as $key => $pl_column) { ?>
						<div class="checkbox col-md-3">
						<label>
                        <?php if (in_array($key, $advpp_settings_pl_columns)) { ?>
                        	<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_pl_columns[]" checked="checked"/> <?php echo $pl_column; ?>
                        <?php } else { ?>
							<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_pl_columns[]" /> <?php echo $pl_column; ?>
                        <?php } ?>
						</label>
						</div>
					<?php } ?>
				</div>		               								
			</div>
		</div>  
<script type="text/javascript">
 function toggle_cl_columns(ele) {
     var checkboxes = document.getElementsByName('advpp<?php echo $user; ?>_settings_cl_columns[]');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
			 $("#check_cl_columns").html('<span><strong><?php echo $text_uncheck_all; ?></strong></span>');
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
			 $("#check_cl_columns").html('<span><strong><?php echo $text_check_all; ?></strong></span>');
         }
     }
 }
</script>         
		<div class="row">
			<div class="col-sm-12">
				<div class="well well-sm" style="background:#f5f5f5; height:175px; overflow:auto;">
                    <div style="margin-bottom:5px;"><small><?php echo $entry_report; ?> <b><?php echo $text_products_purchased; ?> / <?php echo $text_manufacturers; ?> / <?php echo $text_categories; ?></b></small></div>
                	<div class="col-sm-12" align="right">
                    <label><input type="checkbox" onchange="toggle_cl_columns(this)" style="opacity:0; position:fixed; left:9999px;" /> <span id="check_cl_columns" style="cursor:pointer; border:thin dotted #666; padding:2px; background-color:#EEE;"><strong><?php echo $text_check_all; ?></strong></span></label>
                    </div>   
                    <input type="hidden" value="cl_null" name="advpp<?php echo $user; ?>_settings_cl_columns[]" checked="checked"/>                    
					<?php foreach ($cl_columns as $key => $cl_column) { ?>
						<div class="checkbox col-md-3">
						<label>
                        <?php if (in_array($key, $advpp_settings_cl_columns)) { ?>
                        	<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_cl_columns[]" checked="checked"/> <?php echo $cl_column; ?>
                        <?php } else { ?>
							<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_cl_columns[]" /> <?php echo $cl_column; ?>
                        <?php } ?>
						</label>
						</div>
					<?php } ?>
				</div>		               								
			</div>
		</div>
<script type="text/javascript">
 function toggle_all_columns(ele) {
     var checkboxes = document.getElementsByName('advpp<?php echo $user; ?>_settings_all_columns[]');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
			 $("#check_all_columns").html('<span><strong><?php echo $text_uncheck_all; ?></strong></span>');
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
			 $("#check_all_columns").html('<span><strong><?php echo $text_check_all; ?></strong></span>');
         }
     }
 }
</script>        
		<div class="row">
			<div class="col-sm-12">
               	<label class="control-label"><i class="fa fa-columns"></i> <?php echo $text_all_columns; ?></label>
				<div class="well well-sm" style="background:#f5f5f5; height:440px; overflow:auto;">
                	<div class="col-sm-12" align="right">
                    <label><input type="checkbox" onchange="toggle_all_columns(this)" style="opacity:0; position:fixed; left:9999px;" /> <span id="check_all_columns" style="cursor:pointer; border:thin dotted #666; padding:2px; background-color:#EEE;"><strong><?php echo $text_check_all; ?></strong></span></label>
                    </div>   
                    <input type="hidden" value="all_null" name="advpp<?php echo $user; ?>_settings_all_columns[]" checked="checked"/>                 
					<?php foreach ($all_columns as $key => $all_column) { ?>
						<div class="checkbox col-md-3">
						<label>
                        <?php if (in_array($key, $advpp_settings_all_columns)) { ?>
                        	<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_all_columns[]" checked="checked"/> <?php echo $all_column; ?>
                        <?php } else { ?>
							<input type="checkbox" value="<?php echo $key; ?>" name="advpp<?php echo $user; ?>_settings_all_columns[]" /> <?php echo $all_column; ?>
                        <?php } ?>
						</label>
						</div>
					<?php } ?>
				</div>		               								
			</div>
		</div>
		</fieldset>
        <div align="right">
		  <button type="button" class="btn btn-default" title="<?php echo $button_close; ?>" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo $button_close; ?></button>
		  <button type="button" class="btn btn-primary" id="save_settings" title="<?php echo $button_save; ?>" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
		</div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="cron" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-info">
    <div class="modal-content" id="modal-content">
      <div class="modal-header" style="background-color:#777;">
        <button type="button" class="close" data-dismiss="modal" style="color:#FFF;"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $button_close; ?></span></button>
        <h4 class="modal-title" style="color:#FFF;"><?php echo $text_cron_settings; ?></h4>
      </div>
      <div class="modal-body">
      <div align="justify"><?php echo $text_cron; ?></div><br />      
      <fieldset>
        <legend><?php echo $text_cron_1; ?></legend> 
        <div><?php echo $text_cron_1_text; ?></div><br />     
        <legend><?php echo $text_cron_2; ?></legend> 
		<div class="row" style="padding:2px;">
			<div class="col-sm-6" style="padding-bottom:15px;"><div id="cron_report_to_export">
				<label for="cron_report_type" class="control-label"><?php echo $text_report_type; ?></label>
				<select name="cron_report_type" id="cron_report_type" data-style="btn-select" class="form-control select">
                	<?php foreach ($report_types as $report_type) { ?>                  
					<?php if ($report_type == $report_type['type']) { ?>
                    <option data-icon="<?php echo $report_type['icon']; ?>" value="<?php echo $report_type['type']; ?>" title="<?php echo $report_type['name']; ?>" selected="selected"><?php echo $report_type['name']; ?></option>
					<?php } else { ?>
					<option data-icon="<?php echo $report_type['icon']; ?>" value="<?php echo $report_type['type']; ?>" title="<?php echo $report_type['name']; ?>"><?php echo $report_type['name']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div></div>
			<div class="col-sm-6" style="padding-bottom:15px;"><div id="cron_type_to_export">
				<label for="cron_export_type" class="control-label"><?php echo $text_export_type; ?></label>
				<select name="cron_export_type" id="cron_export_type" data-style="btn-select" class="form-control select">
                	<option value=""></option>
                	<?php foreach ($export_types as $export_type) { ?>                
					<?php if ($export_type == $export_type['type']) { ?>
                    <option data-icon="<?php echo $export_type['icon']; ?>" value="<?php echo $export_type['type']; ?>" title="<?php echo $export_type['name']; ?>" selected="selected"><?php echo $export_type['name']; ?></option>
					<?php } else { ?>
					<option data-icon="<?php echo $export_type['icon']; ?>" value="<?php echo $export_type['type']; ?>" title="<?php echo $export_type['name']; ?>"><?php echo $export_type['name']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div></div>            
		</div> 
		<div class="row" style="padding:2px; padding-bottom:20px;">              
			<div class="col-sm-6" style="z-index:5;"><div class="input-group" style="width:100%;">
				<span class="input-group-addon" style="width:70%; text-align:left;"><?php echo $text_export_logo_criteria; ?></span>
				<select name="cron_export_logo_criteria" id="cron_export_logo_criteria" data-style="btn-select" data-width="30%" class="select">
    				<?php if ($export_logo_criteria == '1') { ?>
					<option value="1" title="<?php echo $text_yes; ?>" selected="selected"><?php echo $text_yes; ?></option>
					<option value="0" title="<?php echo $text_no; ?>"><?php echo $text_no; ?></option>
					<?php } else { ?>
					<option value="1" title="<?php echo $text_yes; ?>"><?php echo $text_yes; ?></option>
					<option value="0" title="<?php echo $text_no; ?>" selected="selected"><?php echo $text_no; ?></option>
					<?php } ?>
				</select>
			</div></div>
			<div class="col-sm-6"><div id="cron_csv_delimiter" class="input-group" style="width:100%;">
				<span class="input-group-addon" style="width:55%; text-align:left;"><?php echo $text_export_csv_delimiter; ?></span>
				<select name="cron_export_csv_delimiter" id="cron_export_csv_delimiter" data-style="btn-select" data-width="45%" class="select">
                	<?php foreach ($export_csv_delimiters as $export_csv_delimiter) { ?>                
					<?php if ($export_csv_delimiter == $export_csv_delimiter['type']) { ?>
                    <option value="<?php echo $export_csv_delimiter['type']; ?>" title="<?php echo $export_csv_delimiter['name']; ?>" selected="selected"><?php echo $export_csv_delimiter['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $export_csv_delimiter['type']; ?>" title="<?php echo $export_csv_delimiter['name']; ?>"><?php echo $export_csv_delimiter['name']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div></div>
		</div>																								 
       <legend><?php echo $text_cron_3; ?></legend> 
		<div class="row" style="padding:2px;">
			<div class="col-sm-12"><div class="input-group" style="width:100%;">
				<span class="input-group-addon" style="width:70%; white-space:normal; text-align:left;"><?php echo $text_cron_3_text; ?></span>
				<select name="cron_export_file" id="cron_export_file" data-style="btn-select" data-width="30%" class="select">
					<?php if ($cron_export_file == 'send_to_email') { ?>
					<option id="send_to_email" value="send_to_email" selected="selected"><?php echo $text_cron_3_option1; ?></option>
					<option id="save_on_server" value="save_on_server"><?php echo $text_cron_3_option2; ?></option>
					<?php } else { ?>
					<option id="send_to_email" value="send_to_email"><?php echo $text_cron_3_option1; ?></option>
					<option id="save_on_server" value="save_on_server" selected="selected"><?php echo $text_cron_3_option2; ?></option>
					<?php } ?>
				</select>
			</div></div>
		</div>   
		<div class="row" style="padding:2px; padding-top:10px;">
			<div class="col-sm-3" style="padding-bottom:15px;">
				<label for="cron_email" class="control-label"><span data-toggle="tooltip" data-original-title="<?php echo $help_cron_email ;?>"><?php echo $text_cron_email; ?></span></label>            
				<input type="text" name="cron_email" id="cron_email" placeholder="<?php echo $text_cron_email; ?>" class="form-control" value="" />	
			</div>
			<div class="col-sm-3" style="padding-bottom:15px;">
				<label for="cron_file_name" class="control-label"><span data-toggle="tooltip" data-original-title="<?php echo $help_cron_file_name ;?>"><?php echo $text_cron_file_name; ?></span></label>            
				<input type="text" name="cron_file_name" id="cron_file_name" placeholder="<?php echo $text_cron_file_name; ?>" class="form-control" value="" />	
			</div>     
			<div class="col-sm-6" style="padding-bottom:15px;">
				<label for="cron_file_save_path" class="control-label"><span data-toggle="tooltip" data-original-title="<?php echo $help_save_path ;?>"><?php echo $text_save_path; ?></span></label> <span><?php echo $root_dir; ?></span>
                <div class="input-group" style="width:100%;">
				<input type="text" name="cron_file_save_path" id="cron_file_save_path" class="form-control" data-width="95%" value="" /><span class="input-group-addon" style="width:5%;"><?php echo $dir_sep; ?></span>
				</div>
			</div>         
		</div>  
        <legend><?php echo $text_cron_4; ?></legend>   
		<div class="row" style="padding:2px;">
			<div class="col-sm-3" style="padding-bottom:20px;">
				<label for="cron_user" class="control-label"><span data-toggle="tooltip" data-original-title="<?php echo $help_cron_user ;?>"><?php echo $text_cron_user; ?></span></label>            
				<input type="text" name="cron_user" id="cron_user" placeholder="<?php echo $text_cron_user; ?>" class="form-control" value="" />	
			</div>
			<div class="col-sm-3" style="padding-bottom:20px;">
				<label for="cron_pass" class="control-label"><span data-toggle="tooltip" data-original-title="<?php echo $help_cron_pass ;?>"><?php echo $text_cron_pass; ?></span></label>            
				<input type="password" name="cron_pass" id="cron_pass" placeholder="<?php echo $text_cron_pass; ?>" class="form-control" value="" />	
			</div>    
			<div class="col-sm-6" style="padding-bottom:20px;">
              <div class="input-group">
				<label for="cron_token" class="control-label"><span data-toggle="tooltip" data-original-title="<?php echo $help_cron_token ;?>"><?php echo $text_cron_token; ?></span></label>            
				<input type="text" name="cron_token" id="cron_token" placeholder="<?php echo $text_cron_token; ?>" class="form-control" value="" />
				<span class="input-group-btn" style="padding-top:23px;"><button class="btn btn-secondary" type="button" onclick="cron_token_gen(); cron_command();" style="border:1px solid #CCC;"><?php echo $text_token_generate; ?></button></span>
              </div>
			</div>               
		</div>    
        <legend><?php echo $text_cron_5; ?></legend> 
        <div align="justify"><?php echo $text_cpanel_setting; ?></div>
		<div class="row" style="padding:2px;">
			<div class="col-sm-12" style="padding-bottom:10px;">
				<label for="cpanel" class="control-label"><?php echo $text_cron_command; ?></label>            
				<div id="cpanel" onclick="selectText('cpanel')" style="border:1px solid #CCC; background-color:#EEE; padding:3px;"><span style="font-size:smaller;"><?php echo $text_cron_command_empty; ?></span></div>	
			</div>             
		</div> 
        <div><?php echo $text_cpanel_setting_note; ?></div><br />
        <legend><?php echo $text_cron_6; ?></legend> 
        <div><?php echo $text_save_cron; ?><?php echo $adv_ext_name; ?>!</div> 
        <input type="hidden" name="cron_user_id" id="cron_user_id" value="<?php echo $cron_user_id ?>" />                  
	  </fieldset>
	  <div align="right" style="padding-top:10px;">
		<button type="button" class="btn btn-default" title="<?php echo $button_close; ?>" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo $button_close; ?></button>
		<button type="button" class="btn btn-primary" id="save_cron" title="<?php echo $button_save; ?>" data-loading-text="<?php echo $text_loading; ?>"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
      </div>
      </div>
    </div>
  </div>
</div>  
<?php if ($products) { ?>
<div class="chart-switcher <?php echo (isset($_COOKIE['show-pp-chart']) && $_COOKIE['show-pp-chart'] == 1) ? '' : 'hide-chart'; ?>">
	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders' && $filter_details != 'all_details' && $filter_group == 'no_group') { ?>       
    <div id="tab_chart" class="row">
      <div class="col-lg-6">
      	<div style="width:100%;" id="chart1_div"></div>
      </div>
      <div class="col-lg-6">      
        <div style="width:100%;" id="chart2_div"></div>
      </div>
    </div>
	<?php } ?>
</div>
<?php } ?>
  
<?php if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) { ?>
<div style="overflow:scroll; padding:1px;"> 
<?php } else { ?>
<div style="overflow:auto; padding:1px;">     
<?php } ?>
<?php if ($filter_details == 'all_details') { ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">       
	<tr> 
    <td> 
	<?php if ($products) { ?>          
		<table class="list_detail">
		<thead>
		<tr>
          <td class="left" nowrap="nowrap"><?php echo $column_order_order_id; ?></td>        
          <td class="left" nowrap="nowrap"><?php echo $column_order_date_added; ?></td> 
          <?php if (in_array('all_order_inv_no', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_order_inv_no; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_customer_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_order_customer; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_email', $advpp_settings_all_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $column_order_email; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_customer_group', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_customer_group; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_prod_id; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_sku', $advpp_settings_all_columns)) { ?>           
		  <td class="left" nowrap="nowrap"><?php echo $column_prod_sku; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_model', $advpp_settings_all_columns)) { ?>           
		  <td class="left" nowrap="nowrap"><?php echo $column_prod_model; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_prod_name; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_option', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_prod_option; ?></td>
          <?php } ?>
          
          <?php if (in_array('all_hw_pid', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_hw_pid; ?></td>
          <?php } ?>
          <?php if (in_array('all_hw_quantiy', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_hw_quantiy; ?></td>
          <?php } ?>
          
          
          <?php if (in_array('all_prod_attributes', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_prod_attributes; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_manu', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_prod_manu; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_category', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_prod_category; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_currency', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_prod_currency; ?></td>
          <?php } ?>          
          <?php if (in_array('all_prod_price', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $column_prod_price; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_quantity', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $column_prod_quantity; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_total_excl_vat', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $column_prod_total_excl_vat; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_tax', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $column_prod_tax; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_total_incl_vat', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $column_prod_total_incl_vat; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_qty_refund', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_qty_refunded; ?></td>
          <?php } ?> 
          <?php if (in_array('all_prod_refund', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_refunded; ?></td>
          <?php } ?>     
          <?php if (in_array('all_prod_reward_points', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_reward_points; ?></td>
          <?php } ?> 
          <?php if (in_array('all_sub_total', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_sub_total; ?></td>
          <?php } ?>              
          <?php if (in_array('all_handling', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_handling; ?></td>
          <?php } ?>
          <?php if (in_array('all_loworder', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_loworder; ?></td>
          <?php } ?>                  
          <?php if (in_array('all_shipping', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_order_shipping; ?></td>
          <?php } ?>
          <?php if (in_array('all_reward', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_reward; ?></td>
          <?php } ?>
          <?php if (in_array('all_reward_points', $advpp_settings_all_columns)) { ?>          
          <td class="right" style="min-width:85px;"><?php echo $column_earned_reward_points; ?></td>
          <?php } ?>  
          <?php if (in_array('all_reward_points', $advpp_settings_all_columns)) { ?>          
          <td class="right" style="min-width:85px;"><?php echo $column_used_reward_points; ?></td>
          <?php } ?>            
          <?php if (in_array('all_coupon', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_coupon; ?></td>
          <?php } ?>
          <?php if (in_array('all_coupon_code', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_coupon_code; ?></td>
          <?php } ?>                              
          <?php if (in_array('all_order_tax', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_order_tax; ?></td>
          <?php } ?>
          <?php if (in_array('all_credit', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_credit; ?></td>
          <?php } ?>
          <?php if (in_array('all_voucher', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_voucher; ?></td>
          <?php } ?>
          <?php if (in_array('all_voucher_code', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_voucher_code; ?></td>
          <?php } ?>     
          <?php if (in_array('all_order_commission', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_commission; ?></td>
          <?php } ?>                                   
          <?php if (in_array('all_order_value', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_order_value; ?></td>
          <?php } ?>
          <?php if (in_array('all_refund', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_order_refund; ?></td>
          <?php } ?> 
          <?php if (in_array('all_order_shipping_method', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_shipping_method; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_payment_method', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_payment_method; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_status', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_status; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_store', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_store; ?></td>
          <?php } ?>
          <?php if (in_array('all_customer_cust_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_customer_cust_id; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_first_name; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_last_name; ?></td>
          <?php } ?>          
          <?php if (in_array('all_billing_company', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_company; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_address_1', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_address_1; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_address_2', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_address_2; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_city', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_city; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_zone', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_zone; ?></td> 
          <?php } ?>
          <?php if (in_array('all_billing_zone_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_zone_id; ?></td> 
          <?php } ?>
          <?php if (in_array('all_billing_zone_code', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_zone_code; ?></td> 
          <?php } ?>                    
          <?php if (in_array('all_billing_postcode', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_postcode; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_country', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_country; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_country_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_country_id; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_country_code', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_billing_country_code; ?></td>
          <?php } ?>                    
          <?php if (in_array('all_customer_telephone', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_customer_telephone; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_first_name; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_last_name; ?></td> 
          <?php } ?>          
          <?php if (in_array('all_shipping_company', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_company; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_address_1', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_address_1; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_address_2', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_address_2; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_city', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_city; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_zone', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_zone; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_zone_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_zone_id; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_zone_code', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_zone_code; ?></td> 
          <?php } ?>                    
          <?php if (in_array('all_shipping_postcode', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_postcode; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_country', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_country; ?></td>
          <?php } ?> 
          <?php if (in_array('all_shipping_country_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_country_id; ?></td>
          <?php } ?> 
          <?php if (in_array('all_shipping_country_code', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_country_code; ?></td>
          <?php } ?>                     
          <?php if (in_array('all_order_weight', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $column_order_weight; ?></td>
          <?php } ?>           
          <?php if (in_array('all_order_comment', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $column_order_comment; ?></td>
          <?php } ?>        
		</tr>
		</thead>
        <tbody>	
	
	                        
   
	<?php foreach ($products as $product) { ?>
    <?php if ($product['product_id']) { ?>          
		<tr bgcolor="#FFFFFF">
          <td class="left" nowrap="nowrap" style="background-color:#FFC;" title="<?php echo $column_order_order_id; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><a><?php echo $product['order_id_link']; ?></a></td>        
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_date_added; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['date_added']; ?></td>
          <?php if (in_array('all_order_inv_no', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_inv_no; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['invoice']; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_customer_name', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_customer; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['name']; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_email', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_email; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['email']; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_customer_group', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_customer_group; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['cust_group']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_id; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_id_link']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_sku', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_sku; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_sku']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_model', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_model; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_model']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_name; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_name']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_option', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_option; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_option']; ?></td>
          <?php } ?>
          
        <?php if (in_array('all_hw_pid', $advpp_settings_all_columns)) { ?>
            <td class="left" nowrap="nowrap" title="<?php echo $column_hw_pid; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['hw_pid']; ?></td>
          <?php } ?>
           <?php if (in_array('all_hw_quantiy', $advpp_settings_all_columns)) { ?>
            <td class="left" nowrap="nowrap" title="<?php echo $column_hw_quantiy; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['hw_quantiy']; ?></td>
          <?php } ?>
          
          <?php if (in_array('all_prod_attributes', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_attributes; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_attributes']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_manu', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_manu; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_manu']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_category', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_category; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_category']; ?></td>
          <?php } ?>  
          <?php if (in_array('all_prod_currency', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_prod_currency; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['currency_code']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_price', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap" title="<?php echo $column_prod_price; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_price']; ?></td>
          <?php } ?>                  
          <?php if (in_array('all_prod_quantity', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap" title="<?php echo $column_prod_quantity; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_quantity']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_total_excl_vat', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap" title="<?php echo $column_prod_total_excl_vat; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_total_excl_vat']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_tax', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap" title="<?php echo $column_prod_tax; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_tax']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_total_incl_vat', $advpp_settings_all_columns)) { ?>           
          <td class="right" nowrap="nowrap" title="<?php echo $column_prod_total_incl_vat; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_total_incl_vat']; ?></td>
          <?php } ?>
          <?php if (in_array('all_prod_qty_refund', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_prod_qty_refunded; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_qty_refund']; ?></td>
          <?php } ?>   
          <?php if (in_array('all_prod_refund', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_prod_refunded; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_refund']; ?></td>
          <?php } ?>       
          <?php if (in_array('all_prod_reward_points', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_prod_reward_points; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['product_reward_points']; ?></td>
          <?php } ?>
          <?php if (in_array('all_sub_total', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_sub_total; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_sub_total']; ?></td>
          <?php } ?>
          <?php if (in_array('all_handling', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_handling; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_handling']; ?></td>
          <?php } ?>
          <?php if (in_array('all_loworder', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_loworder; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_low_order_fee']; ?></td>
          <?php } ?>                    
          <?php if (in_array('all_shipping', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_shipping; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_shipping']; ?></td>
          <?php } ?>
          <?php if (in_array('all_reward', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_reward; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_reward']; ?></td>
          <?php } ?>
          <?php if (in_array('all_reward_points', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_earned_reward_points; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_earned_points']; ?></td>
          <?php } ?> 
          <?php if (in_array('all_reward_points', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_used_reward_points; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_used_points']; ?></td>
          <?php } ?>            
          <?php if (in_array('all_coupon', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_coupon; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_coupon']; ?></td>
          <?php } ?>
          <?php if (in_array('all_coupon_code', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_coupon_code; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_coupon_code']; ?></td>
          <?php } ?>                              
          <?php if (in_array('all_order_tax', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_order_tax; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_tax']; ?></td>
          <?php } ?>
          <?php if (in_array('all_credit', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_credit; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_credit']; ?></td>
          <?php } ?>
          <?php if (in_array('all_voucher', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_voucher; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_voucher']; ?></td>
          <?php } ?>
          <?php if (in_array('all_voucher_code', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_voucher_code; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_voucher_code']; ?></td>
          <?php } ?>         
          <?php if (in_array('all_order_commission', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_commission; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_commission']; ?></td>
          <?php } ?>          
          <?php if (in_array('all_order_value', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_order_value; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_value']; ?></td>
          <?php } ?>
          <?php if (in_array('all_refund', $advpp_settings_all_columns)) { ?>          
          <td class="right" nowrap="nowrap" title="<?php echo $column_order_refund; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_refund']; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_shipping_method', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_shipping_method; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_method']; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_payment_method', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_payment_method; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_method']; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_status', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_status; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_status']; ?></td>
          <?php } ?>
          <?php if (in_array('all_order_store', $advpp_settings_all_columns)) { ?>          
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_store; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['store_name']; ?></td> 
          <?php } ?>
          <?php if (in_array('all_customer_cust_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_customer_cust_id; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]">
          <?php if ($product['customer_id'] == 0) { ?>
          <?php echo $product['customer_id']; ?>       
          <?php } else { ?>
          <?php echo $product['customer_id_link']; ?>
          <?php } ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_first_name); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_firstname']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_last_name); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_lastname']; ?></td>
          <?php } ?>          
          <?php if (in_array('all_billing_company', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_company); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_company']; ?></td> 
          <?php } ?>
          <?php if (in_array('all_billing_address_1', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_address_1); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_address_1']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_address_2', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_address_2); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_address_2']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_city', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_city); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_city']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_zone', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_zone); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_zone']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_zone_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_zone_id); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_zone_id']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_zone_code', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_zone_code); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_zone_code']; ?></td>
          <?php } ?>                    
          <?php if (in_array('all_billing_postcode', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_postcode); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_postcode']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_country', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_country); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_country']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_country_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_country_id); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_country_id']; ?></td>
          <?php } ?>
          <?php if (in_array('all_billing_country_code', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_billing_country_code); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['payment_country_code']; ?></td>
          <?php } ?>                    
          <?php if (in_array('all_customer_telephone', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo $column_customer_telephone; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['telephone']; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_first_name); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_firstname']; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_name', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_last_name); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_lastname']; ?></td>
          <?php } ?>          
          <?php if (in_array('all_shipping_company', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_company); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_company']; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_address_1', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_address_1); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_address_1']; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_address_2', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_address_2); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_address_2']; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_city', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_city); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_city']; ?></td> 
          <?php } ?>                
          <?php if (in_array('all_shipping_zone', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_zone); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_zone']; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_zone_id', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_zone_id); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_zone_id']; ?></td> 
          <?php } ?>
          <?php if (in_array('all_shipping_zone_code', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_zone_code); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_zone_code']; ?></td> 
          <?php } ?>                    
          <?php if (in_array('all_shipping_postcode', $advpp_settings_all_columns)) { ?>           
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_postcode); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_postcode']; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_country', $advpp_settings_all_columns)) { ?> 
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_country); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_country']; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_country_id', $advpp_settings_all_columns)) { ?> 
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_country_id); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_country_id']; ?></td>
          <?php } ?>
          <?php if (in_array('all_shipping_country_code', $advpp_settings_all_columns)) { ?> 
          <td class="left" nowrap="nowrap" title="<?php echo strip_tags($column_shipping_country_code); ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['shipping_country_code']; ?></td>
          <?php } ?>                    
          <?php if (in_array('all_order_weight', $advpp_settings_all_columns)) { ?> 
          <td class="right" nowrap="nowrap" title="<?php echo $column_order_weight; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_weight']; ?></td>
          <?php } ?>           
          <?php if (in_array('all_order_comment', $advpp_settings_all_columns)) { ?> 
          <td class="left" nowrap="nowrap" title="<?php echo $column_order_comment; ?> [<?php echo $column_order_order_id; ?>: <?php echo $product['order_id']; ?>]"><?php echo $product['order_comment']; ?></td>
          <?php } ?>              
          </tr>   
	<?php } ?>
	<?php } ?>
	      </table>            
		</td>               
		</tr>       
    	</tbody>         
		</table>
	<?php } else { ?>
		<table width="100%">    
		<tr>
		<td align="center"><?php echo $text_no_results; ?></td>
		</tr>
        </table>          
	<?php } ?>      
    </td></tr>
    </table>  
<?php } ?>
<?php if ($filter_details != 'all_details') { ?>
    <table class="list_main">
        <thead>
          <tr>
          <?php if ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') { ?>
          <td class="left" nowrap="nowrap"><?php echo $column_date_added; ?></td>
          <?php } else { ?> 
		  <?php if ($filter_group == 'year') { ?>           
          <td class="left" colspan="2" nowrap="nowrap"><?php echo $column_year; ?></td>
		  <?php } elseif ($filter_group == 'quarter') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $column_year; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $column_quarter; ?></td>       
		  <?php } elseif ($filter_group == 'month') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $column_year; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $column_month; ?></td> 
		  <?php } elseif ($filter_group == 'day') { ?> 
          <td class="left" colspan="2" nowrap="nowrap"><?php echo $column_date; ?></td>
		  <?php } elseif ($filter_group == 'order') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $column_order_order_id; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $column_order_date_added; ?></td>             
		  <?php } else { ?>    
          <td class="left" width="70" nowrap="nowrap"><?php echo $column_date_start; ?></td>
          <td class="left" width="70" nowrap="nowrap"><?php echo $column_date_end; ?></td>
		  <?php } ?>
          <?php } ?>
          <?php if ($filter_report != 'manufacturers' && $filter_report != 'categories') { ?>  
          <?php if (in_array('mv_id', $advpp_settings_mv_columns)) { ?>            
          <td class="right"><?php echo $column_id; ?></td>
          <?php } ?>   
          <?php if (in_array('mv_image', $advpp_settings_mv_columns)) { ?>            
          <td class="center"><?php echo $column_image; ?></td>
          <?php } ?>   
          <?php if (in_array('mv_sku', $advpp_settings_mv_columns)) { ?>            
          <td class="left"><?php echo $column_sku; ?></td>
          <?php } ?>   
          <?php if (in_array('mv_name', $advpp_settings_mv_columns)) { ?>            
          <td class="left"><?php if ($filter_report == 'products_purchased_with_options') { ?><?php echo $column_name; ?><?php } else { ?><?php echo $column_prod_name; ?><?php } ?></td>
          <?php } ?>     
          <?php if (in_array('mv_model', $advpp_settings_mv_columns)) { ?>            
          <td class="left"><?php echo $column_model; ?></td>
          <?php } ?>   
          <?php if (in_array('mv_category', $advpp_settings_mv_columns)) { ?>            
          <td class="left"><?php echo $column_category; ?></td>
          <?php } ?>   
          <?php if (in_array('mv_manufacturer', $advpp_settings_mv_columns)) { ?>
          <td class="left"><?php echo $column_manufacturer; ?></td>
          <?php } ?>
          <?php if (in_array('mv_attribute', $advpp_settings_mv_columns)) { ?>
          <td class="left"><?php echo $column_attribute; ?></td>
          <?php } ?>
          <?php if (in_array('mv_status', $advpp_settings_mv_columns)) { ?>          
          <td class="left"><?php echo $column_status; ?></td> 
          <?php } ?>
          <?php if (in_array('mv_location', $advpp_settings_mv_columns)) { ?>            
          <td class="left"><?php echo $column_location; ?></td>
          <?php } ?>   
          <?php if (in_array('mv_tax_class', $advpp_settings_mv_columns)) { ?>          
          <td class="left"><?php echo $column_tax_class; ?></td>
          <?php } ?>                    
          <?php if (in_array('mv_price', $advpp_settings_mv_columns)) { ?>          
          <td class="right"><?php echo $column_price; ?></td>
          <?php } ?>
          <?php if (in_array('mv_viewed', $advpp_settings_mv_columns)) { ?>          
          <td class="right"><?php echo $column_viewed; ?></td> 
          <?php } ?>           
          <?php if (in_array('mv_stock_quantity', $advpp_settings_mv_columns)) { ?>                           
          <td class="right"><?php echo $column_stock_quantity; ?></td>
          <?php } ?>
          <?php } elseif ($filter_report == 'manufacturers') { ?> 
          <?php if (in_array('mv_manufacturer', $advpp_settings_mv_columns)) { ?>
          <td class="left"><?php echo $column_manufacturer; ?></td>
          <?php } ?>
		  <?php } elseif ($filter_report == 'categories') { ?>
          <?php if (in_array('mv_category', $advpp_settings_mv_columns)) { ?>            
          <td class="left"><?php echo $column_category; ?></td>
          <?php } ?>   
		  <?php } ?>  
          <?php if ($filter_report != 'products_without_orders') { ?> 
          <?php if (in_array('mv_sold_quantity', $advpp_settings_mv_columns)) { ?>          
          <td class="right"><?php echo $column_sold_quantity; ?></td>
          <?php } ?>                  
          <?php if (in_array('mv_sold_percent', $advpp_settings_mv_columns)) { ?>          
          <td class="right"><?php echo $column_sold_percent; ?></td>
          <?php } ?>          
          <?php if (in_array('mv_total_excl_vat', $advpp_settings_mv_columns)) { ?>          
          <td class="right" style="min-width:75px;"><?php echo $column_prod_total_excl_vat; ?></td>
          <?php } ?>
          <?php if (in_array('mv_total_tax', $advpp_settings_mv_columns)) { ?>          
          <td class="right"><?php echo $column_total_tax; ?></td>
          <?php } ?>
          <?php if (in_array('mv_total_incl_vat', $advpp_settings_mv_columns)) { ?>          
          <td class="right" style="min-width:75px;"><?php echo $column_prod_total_incl_vat; ?></td>
          <?php } ?>                 
          <?php if (in_array('mv_app', $advpp_settings_mv_columns)) { ?>          
          <td class="right" style="min-width:90px;"><?php echo $column_app; ?></td>
          <?php } ?>
          <?php if (in_array('mv_refunds', $advpp_settings_mv_columns)) { ?>          
          <td class="right"><?php echo $column_product_refunds; ?></td>
          <?php } ?>
          <?php if (in_array('mv_reward_points', $advpp_settings_mv_columns)) { ?>          
          <td class="right"><?php echo $column_product_reward_points; ?></td>
          <?php } ?>
          <?php if ($filter_details == 'basic_details') { ?><td class="right" nowrap="nowrap"><?php echo $column_action; ?> <span class="toggle-all expand" title="<?php echo $button_toggle; ?>" style="cursor:pointer;"><i id="circle" class="fa fa-arrow-circle-down"></i></span></td><?php } ?> 
          <?php } ?>
          </tr>
      	  </thead>
          <?php if ($products) { ?>
          <?php foreach ($products as $product) { ?>
      	  <tbody>     
          <?php if ($filter_report == 'all_products_with_without_orders' or $filter_report == 'products_without_orders') { ?>
          <tr>
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['date_added']; ?></td>  
          <?php } else { ?>             
          <tr <?php echo ($filter_details == 'basic_details') ? 'style="cursor:pointer;" title="' . $text_detail . '"' : '' ?> id="show_details_<?php echo $product['order_product_id']; ?>">          
		  <?php if ($filter_group == 'year') { ?>           
          <td class="left" colspan="2" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['year']; ?></td>
		  <?php } elseif ($filter_group == 'quarter') { ?> 
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['year']; ?></td>
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['quarter']; ?></td>  
		  <?php } elseif ($filter_group == 'month') { ?> 
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['year']; ?></td>
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['month']; ?></td>
		  <?php } elseif ($filter_group == 'day') { ?> 
          <td class="left" colspan="2" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['date_start']; ?></td>
		  <?php } elseif ($filter_group == 'order') { ?> 
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['order_id']; ?></td>
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['date_start']; ?></td>         
		  <?php } else { ?>    
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['date_start']; ?></td>
          <td class="left" nowrap="nowrap" style="background-color:#F9F9F9;"><?php echo $product['date_end']; ?></td>         
		  <?php } ?>
          <?php } ?>
          <?php if ($filter_report != 'manufacturers' && $filter_report != 'categories') { ?> 
          <?php if (in_array('mv_id', $advpp_settings_mv_columns)) { ?>          
          <td class="right"><?php echo $product['product_id']; ?></td>
          <?php } ?>  
          <?php if (in_array('mv_image', $advpp_settings_mv_columns)) { ?>          
          <td class="center"><img src="<?php echo $product['image']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
          <?php } ?>   
          <?php if (in_array('mv_sku', $advpp_settings_mv_columns)) { ?>          
          <td class="left"><?php echo $product['sku']; ?></td>
          <?php } ?>  
          <?php if (in_array('mv_name', $advpp_settings_mv_columns)) { ?>          
          <td class="left">
          <?php if ($product['status'] != NULL) { ?>
          <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
          <?php } else { ?>
          <?php echo $product['name']; ?>
          <?php } ?>
          <?php if ($filter_report == 'products_purchased_with_options') { ?>
          <?php if ($product['option']) { ?>          
          <div style="display:table; margin-left:3px;">
          <?php foreach ($product['option'] as $option) { ?>            
          <div style="display:table-row; white-space:nowrap;">         
		  <div style="display:table-cell; white-space:nowrap;"><small><?php echo $option['name']; ?>:</small></div>
          <div style="display:table-cell; white-space:nowrap; padding-left:5px;"><small><?php echo $option['value']; ?></small></div>
          <div style="display:table-cell; white-space:nowrap; padding-left:5px;"><small><?php echo $option['hw_pid']; ?></small></div>
          </div>
          <?php } ?>
          </div>
          <?php } ?><?php } ?></td>          
          <?php } ?>    
          <?php if (in_array('mv_model', $advpp_settings_mv_columns)) { ?>          
          <td class="left"><?php echo $product['model']; ?></td>
          <?php } ?> 
          <?php if (in_array('mv_category', $advpp_settings_mv_columns)) { ?>          
          <td class="left">
          <?php foreach ($categories as $category) { ?>
          <?php if (in_array($category['category_id'], $product['category'])) { ?>
          <?php echo $category['name'];?><br />
          <?php } ?> <?php } ?></td>  
          <?php } ?>
          <?php if (in_array('mv_manufacturer', $advpp_settings_mv_columns)) { ?>          
          <td class="left">
		  <?php foreach ($manufacturers as $manufacturer) { ?>
          <?php if (in_array($manufacturer['manufacturer_id'], $product['manufacturer'])) { ?>
          <?php echo $manufacturer['name'];?>
          <?php } ?> <?php } ?></td>          
          <?php } ?>
          <?php if (in_array('mv_attribute', $advpp_settings_mv_columns)) { ?>          
          <td class="left"><?php echo $product['attribute']; ?></td>
          <?php } ?>
          <?php if (in_array('mv_status', $advpp_settings_mv_columns)) { ?>          
          <td class="left"><?php echo $product['status']; ?></td>
          <?php } ?>
          <?php if (in_array('mv_location', $advpp_settings_mv_columns)) { ?>          
          <td class="left"><?php echo $product['location']; ?></td>
          <?php } ?>  
          <?php if (in_array('mv_tax_class', $advpp_settings_mv_columns)) { ?>          
          <td class="left"><?php echo $product['tax_class']; ?></td>
          <?php } ?>                     
          <?php if (in_array('mv_price', $advpp_settings_mv_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $product['price']; ?></td>  
          <?php } ?>  
          <?php if (in_array('mv_viewed', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['viewed']; ?></td>
          <?php } ?>           
          <?php if (in_array('mv_stock_quantity', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap">
          <?php if ($product['stock_quantity'] <= 0) { ?>
		  <span style="color:#FF0000;"><?php echo $product['stock_quantity']; ?></span>
		  <?php } elseif ($product['stock_quantity'] <= 5) { ?>
		  <span style="color:#FFA500;"><?php echo $product['stock_quantity']; ?></span>
		  <?php } else { ?>
		  <?php echo $product['stock_quantity']; ?>
		  <?php } ?>
		  <?php if ($filter_report == 'products_purchased_with_options') { ?> 
		  <?php if ($product['option']) { ?><br />
		  <small><?php echo $product['stock_oquantity']; ?></small>
		  <?php } ?>
		  <?php } ?></td>
          <?php } ?>
          <?php } elseif ($filter_report == 'manufacturers') { ?> 
          <?php if (in_array('mv_manufacturer', $advpp_settings_mv_columns)) { ?>          
          <td class="left">
		  <?php foreach ($manufacturers as $manufacturer) { ?>
          <?php if (in_array($manufacturer['manufacturer_id'], $product['manufacturer'])) { ?>
          <?php echo $manufacturer['name'];?>
          <?php } ?> <?php } ?></td>          
          <?php } ?>
		  <?php } elseif ($filter_report == 'categories') { ?>
          <?php if (in_array('mv_category', $advpp_settings_mv_columns)) { ?>          
          <td class="left">
          <?php foreach ($categories as $category) { ?>
          <?php if (in_array($category['category_id'], $product['category'])) { ?>
          <?php echo $category['name'];?><br />
          <?php } ?> <?php } ?></td>  
          <?php } ?>  
		  <?php } ?>   
          <?php if ($filter_report != 'products_without_orders') { ?>        
          <?php if (in_array('mv_sold_quantity', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap" style="background-color:#FFC;"><?php echo $product['sold_quantity']; ?></td>
          <?php } ?>
          <?php if (in_array('mv_sold_percent', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap" style="background-color:#FFC;"><?php echo $product['sold_percent']; ?></td>
          <?php } ?>
          <?php if (in_array('mv_total_excl_vat', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['total_excl_vat']; ?></td>
          <?php } ?>
          <?php if (in_array('mv_total_tax', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['total_tax']; ?></td>
          <?php } ?>
          <?php if (in_array('mv_total_incl_vat', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['total_incl_vat']; ?></td>
          <?php } ?>    
          <?php if (in_array('mv_app', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['app']; ?></td>
          <?php } ?> 
          <?php if (in_array('mv_refunds', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['refunds']; ?></td>
          <?php } ?> 
          <?php if (in_array('mv_reward_points', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['reward_points']; ?></td>
          <?php } ?>
          <?php if ($filter_details == 'basic_details') { ?><td class="right" nowrap="nowrap">[ <a><?php echo $text_detail; ?></a> ]</td><?php } ?> 
          <?php } ?>
          </tr>        
<tr>
<td colspan="25" class="center">
<?php if (($filter_report == 'products_purchased_without_options' or $filter_report == 'products_purchased_with_options' or $filter_report == 'new_products_purchased' or $filter_report == 'old_products_purchased' or $filter_report == 'products_abandoned_orders') && $filter_details == 'basic_details') { ?>
<script type="text/javascript">
$('#show_details_<?php echo $product["order_product_id"]; ?>').click(function() {
	$('#tab_details_<?php echo $product["order_product_id"]; ?>').slideToggle('slow');
});
</script>
<div id="tab_details_<?php echo $product['order_product_id']; ?>" class="more" style="display:none;">
    <table class="list_detail">
      <thead>
        <tr>
          <?php if (in_array('ol_order_order_id', $advpp_settings_ol_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $column_order_order_id; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_date_added', $advpp_settings_ol_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $column_order_date_added; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_inv_no', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_inv_no; ?></td> 
          <?php } ?>
          <?php if (in_array('ol_order_customer', $advpp_settings_ol_columns)) { ?>                           
          <td class="left" nowrap="nowrap"><?php echo $column_order_customer; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_email', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_email; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_customer_group', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_customer_group; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_shipping_method', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_shipping_method; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_payment_method', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_payment_method; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_status', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_status; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_store', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_order_store; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_currency', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_order_currency; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_price', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_price; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_quantity', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_quantity; ?></td>  
          <?php } ?>          
          <?php if (in_array('ol_prod_total_excl_vat', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_total_excl_vat; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_tax', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_tax; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_total_incl_vat', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_total_incl_vat; ?></td>
          <?php } ?>
        </tr>
      </thead>
        <tr bgcolor="#FFFFFF">
          <?php if (in_array('ol_order_order_id', $advpp_settings_ol_columns)) { ?>
          <td class="left" nowrap="nowrap" style="background-color:#FFC;"><a><?php echo $product['order_prod_ord_id_link']; ?></a></td>
          <?php } ?>
          <?php if (in_array('ol_order_date_added', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_ord_date']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_inv_no', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_inv_no']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_customer', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_name']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_email', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_email']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_customer_group', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_group']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_shipping_method', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_shipping_method']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_payment_method', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_payment_method']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_status', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_status']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_store', $advpp_settings_ol_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['order_prod_store']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_order_currency', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['order_prod_currency']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_price', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['order_prod_price']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_quantity', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['order_prod_quantity']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_total_excl_vat', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['order_prod_total_excl_vat']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_tax', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['order_prod_tax']; ?></td>
          <?php } ?>
          <?php if (in_array('ol_prod_total_incl_vat', $advpp_settings_ol_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $product['order_prod_total_incl_vat']; ?></td>
          <?php } ?>
         </tr>
    </table> 
<?php } ?>    
<?php if (($filter_report == 'manufacturers' or $filter_report == 'categories') && $filter_details == 'basic_details') { ?>
<script type="text/javascript">$(function(){ 
$('#show_details_<?php echo $product["order_product_id"]; ?>').click(function() {
		$('#tab_details_<?php echo $product["order_product_id"]; ?>').slideToggle('slow');
	});
});
</script>
<div id="tab_details_<?php echo $product['order_product_id']; ?>" class="more" style="display:none">
    <table class="list_detail">
      <thead>
        <tr>
          <?php if (in_array('pl_prod_order_id', $advpp_settings_pl_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $column_prod_order_id; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_date_added', $advpp_settings_pl_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $column_prod_date_added; ?></td>
          <?php } ?> 
          <?php if (in_array('pl_prod_inv_no', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_inv_no; ?></td>
          <?php } ?>                 
          <?php if (in_array('pl_prod_id', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_id; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_sku', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_sku; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_model', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_model; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_name', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_name; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_option', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_option; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_attributes', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_attributes; ?></td>
          <?php } ?>
          <?php if ($filter_report == 'categories') { ?>          
          <?php if (in_array('pl_prod_manu', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_manu; ?></td>
          <?php } ?>
          <?php } ?>
          <?php if ($filter_report == 'manufacturers') { ?>          
          <?php if (in_array('pl_prod_category', $advpp_settings_pl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_prod_category; ?></td>
          <?php } ?>
          <?php } ?>
          <?php if (in_array('pl_prod_currency', $advpp_settings_pl_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_currency; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_price', $advpp_settings_pl_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_price; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_quantity', $advpp_settings_pl_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_quantity; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_total_excl_vat', $advpp_settings_pl_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_total_excl_vat; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_tax', $advpp_settings_pl_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_tax; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_total_incl_vat', $advpp_settings_pl_columns)) { ?>          
          <td class="right" nowrap="nowrap"><?php echo $column_prod_total_incl_vat; ?></td>
          <?php } ?>
        </tr>
      </thead>
        <tr bgcolor="#FFFFFF">
          <?php if (in_array('pl_prod_order_id', $advpp_settings_pl_columns)) { ?>
          <td class="left" nowrap="nowrap" style="background-color:#FFC;"><a><?php echo $product['product_ord_id_link']; ?></a></td>
          <?php } ?>
          <?php if (in_array('pl_prod_date_added', $advpp_settings_pl_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $product['product_ord_date']; ?></td>
          <?php } ?> 
          <?php if (in_array('pl_prod_inv_no', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_inv_no']; ?></td>
          <?php } ?>     
          <?php if (in_array('pl_prod_id', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_prod_id_link']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_sku', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_sku']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_model', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_model']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_name', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_name']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_option', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_option']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_attributes', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_attributes']; ?></td>
          <?php } ?>
          <?php if ($filter_report == 'categories') { ?>          
          <?php if (in_array('pl_prod_manu', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_manu']; ?></td>
          <?php } ?>
          <?php } ?>
          <?php if ($filter_report == 'manufacturers') { ?>          
          <?php if (in_array('pl_prod_category', $advpp_settings_pl_columns)) { ?>           
          <td class="left" nowrap="nowrap"><?php echo $product['product_category']; ?></td>
          <?php } ?>
          <?php } ?>            
          <?php if (in_array('pl_prod_currency', $advpp_settings_pl_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $product['product_currency']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_price', $advpp_settings_pl_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $product['product_price']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_quantity', $advpp_settings_pl_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $product['product_quantity']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_total_excl_vat', $advpp_settings_pl_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $product['product_total_excl_vat']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_tax', $advpp_settings_pl_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $product['product_tax']; ?></td>
          <?php } ?>
          <?php if (in_array('pl_prod_total_incl_vat', $advpp_settings_pl_columns)) { ?>           
          <td class="right" nowrap="nowrap"><?php echo $product['product_total_incl_vat']; ?></td>
          <?php } ?>        
         </tr>       
    </table>
<?php } ?>
<?php if ($filter_details == 'basic_details') { ?>   
    <table class="list_detail">
      <thead>
        <tr>
          <?php if (in_array('cl_customer_order_id', $advpp_settings_cl_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $column_customer_order_id; ?></td>
          <?php } ?>
          <?php if (in_array('cl_customer_date_added', $advpp_settings_cl_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $column_customer_date_added; ?></td>
          <?php } ?>         
          <?php if (in_array('cl_customer_cust_id', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_customer_cust_id; ?></td>
          <?php } ?>
          <?php if (in_array('cl_billing_name', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_billing_name; ?></td>
          <?php } ?>
          <?php if (in_array('cl_billing_company', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_billing_company; ?></td>
          <?php } ?>
          <?php if (in_array('cl_billing_address_1', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_billing_address_1; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_billing_address_2', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_billing_address_2; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_billing_city', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_billing_city; ?></td>
          <?php } ?>
          <?php if (in_array('cl_billing_zone', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_billing_zone; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_billing_postcode', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_billing_postcode; ?></td>
          <?php } ?>
          <?php if (in_array('cl_billing_country', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_billing_country; ?></td>
          <?php } ?>
          <?php if (in_array('cl_customer_telephone', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_customer_telephone; ?></td>
          <?php } ?>
          <?php if (in_array('cl_shipping_name', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_name; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_company', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_company; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_address_1', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_address_1; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_address_2', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_address_2; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_city', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_city; ?></td>
          <?php } ?>
          <?php if (in_array('cl_shipping_zone', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_zone; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_postcode', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_postcode; ?></td>
          <?php } ?>
          <?php if (in_array('cl_shipping_country', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $column_shipping_country; ?></td>
          <?php } ?>          
        </tr>
      </thead>
        <tr bgcolor="#FFFFFF">
          <?php if (in_array('cl_customer_order_id', $advpp_settings_cl_columns)) { ?>
          <td class="left" nowrap="nowrap" style="background-color:#FFC;"><a><?php echo $product['customer_ord_id_link']; ?></a></td>
          <?php } ?>
          <?php if (in_array('cl_customer_date_added', $advpp_settings_cl_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $product['customer_ord_date']; ?></td>
          <?php } ?>          
          <?php if (in_array('cl_customer_cust_id', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['customer_cust_id_link']; ?></td>
          <?php } ?>
          <?php if (in_array('cl_billing_name', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['billing_name']; ?></td>
          <?php } ?>
          <?php if (in_array('cl_billing_company', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['billing_company']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_billing_address_1', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['billing_address_1']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_billing_address_2', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['billing_address_2']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_billing_city', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['billing_city']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_billing_zone', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['billing_zone']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_billing_postcode', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['billing_postcode']; ?></td>
          <?php } ?>
          <?php if (in_array('cl_billing_country', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['billing_country']; ?></td>
          <?php } ?>
          <?php if (in_array('cl_customer_telephone', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['customer_telephone']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_name', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['shipping_name']; ?></td>
          <?php } ?>
          <?php if (in_array('cl_shipping_company', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['shipping_company']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_address_1', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['shipping_address_1']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_address_2', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['shipping_address_2']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_city', $advpp_settings_cl_columns)) { ?>
          <td class="left" nowrap="nowrap"><?php echo $product['shipping_city']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_zone', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['shipping_zone']; ?></td> 
          <?php } ?>
          <?php if (in_array('cl_shipping_postcode', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['shipping_postcode']; ?></td>
          <?php } ?>
          <?php if (in_array('cl_shipping_country', $advpp_settings_cl_columns)) { ?>          
          <td class="left" nowrap="nowrap"><?php echo $product['shipping_country']; ?></td>
          <?php } ?>           
         </tr>
    </table>
</div> 
<?php } ?>
</td>
</tr>
          <?php } ?>
<?php if ($filter_report != 'products_without_orders') { ?>            
        <tr>
        <td colspan="25"></td>
        </tr>
        <tr>
          <td <?php echo ($filter_report == 'all_products_with_without_orders') ? '' : 'colspan="2"' ?> class="right" style="background-color:#E5E5E5;"><strong><?php echo $text_filter_total; ?></strong></td>
          <?php if ($filter_report != 'manufacturers' && $filter_report != 'categories') { ?> 
          <?php if (in_array('mv_id', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_image', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_sku', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_name', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_model', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_category', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_manufacturer', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_attribute', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_status', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>

          <?php if (in_array('mv_location', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_tax_class', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_price', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
          <?php if (in_array('mv_viewed', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>                                                                      
          <?php if (in_array('mv_stock_quantity', $advpp_settings_mv_columns)) { ?>          
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>  
          <?php } elseif ($filter_report == 'manufacturers') { ?> 
          <?php if (in_array('mv_manufacturer', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
		  <?php } elseif ($filter_report == 'categories') { ?>
          <?php if (in_array('mv_category', $advpp_settings_mv_columns)) { ?>
          <td style="background-color:#E5E5E5;"></td>
          <?php } ?>
		  <?php } ?>  
          <?php if ($filter_report != 'products_without_orders') { ?>          
          <?php if (in_array('mv_sold_quantity', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['sold_quantity_total']; ?></strong></td>
          <?php } ?>
          <?php if (in_array('mv_sold_percent', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['sold_percent_total']; ?></strong></td>
          <?php } ?>
          <?php if (in_array('mv_total_excl_vat', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['total_excl_vat_total']; ?></strong></td>
          <?php } ?>
          <?php if (in_array('mv_total_tax', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['total_tax_total']; ?></strong></td>
          <?php } ?>
          <?php if (in_array('mv_total_incl_vat', $advpp_settings_mv_columns)) { ?>          
          <td class="right" nowrap="nowrap" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['total_incl_vat_total']; ?></strong></td>
          <?php } ?>               
          <?php if (in_array('mv_app', $advpp_settings_mv_columns)) { ?>            
          <td class="right" nowrap="nowrap" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['app_total']; ?></strong></td>
          <?php } ?>
          <?php if (in_array('mv_refunds', $advpp_settings_mv_columns)) { ?>            
          <td class="right" nowrap="nowrap" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['refunds_total']; ?></strong></td>
          <?php } ?>
          <?php if (in_array('mv_reward_points', $advpp_settings_mv_columns)) { ?>            
          <td class="right" nowrap="nowrap" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['reward_points_total']; ?></strong></td>
          <?php } ?>      
          <?php if ($filter_details == 'basic_details') { ?><td></td><?php } ?>   
          <?php } ?>                
        </tr>   
<?php } ?>                      
          <?php } else { ?>
          <tr>
          <td class="noresult" colspan="25"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>  
          </tbody>
      </table>
<?php } ?>
    </div> 
        <div>
          <div style="float:left;"><?php echo $pagination; ?></div>
          <div style="float:right;"><?php echo $results; ?></div>
        </div>          
    </div>
  </div>
</div>
<?php if (defined('_JEXEC')) { ?>
<?php MijoShop::getClass('base')->addHeader(JPATH_MIJOSHOP_OC . '/admin/view/javascript/bootstrap/js/bootstrap-multiselect.js', false); ?>
<?php MijoShop::getClass('base')->addHeader(JPATH_MIJOSHOP_OC . '/admin/view/javascript/bootstrap/css/bootstrap-multiselect.css'); ?>
<?php MijoShop::getClass('base')->addHeader(JPATH_MIJOSHOP_OC . '/admin/view/javascript/bootstrap/js/bootstrap-select.min.js', false); ?>
<?php MijoShop::getClass('base')->addHeader(JPATH_MIJOSHOP_OC . '/admin/view/javascript/bootstrap/css/bootstrap-select.css'); ?>
<?php } ?>
	
	
	
<script type="text/javascript">
function filter() {
	url = 'index.php?route=report/adv_products&token=<?php echo $token; ?>';										 

	var filter_report = $('select[name=\'filter_report\']').val();
	if (filter_report) {
		url += '&filter_report=' + encodeURIComponent(filter_report);
	}
	
	var filter_group = $('select[name=\'filter_group\']').val();
	if (filter_group) {
		url += '&filter_group=' + encodeURIComponent(filter_group);
	}
	
	var filter_sort = $('select[name=\'filter_sort\']').val();
	if (filter_sort) {
		url += '&filter_sort=' + encodeURIComponent(filter_sort);
	}

	var filter_details = $('select[name=\'filter_details\']').val();
	if (filter_details) {
		url += '&filter_details=' + encodeURIComponent(filter_details);
	}
	
	var filter_limit = $('select[name=\'filter_limit\']').val();
	if (filter_limit) {
		url += '&filter_limit=' + encodeURIComponent(filter_limit);
	}
	
	var filter_range = $('select[name=\'filter_range\']').val();
	if (filter_range) {
		url += '&filter_range=' + encodeURIComponent(filter_range);
	}
	
	var filter_date_start = $('input[name=\'filter_date_start\']').val();
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	var filter_status_date_start = $('input[name=\'filter_status_date_start\']').val();
	if (filter_status_date_start) {
		url += '&filter_status_date_start=' + encodeURIComponent(filter_status_date_start);
	}

	var filter_status_date_end = $('input[name=\'filter_status_date_end\']').val();
	if (filter_status_date_end) {
		url += '&filter_status_date_end=' + encodeURIComponent(filter_status_date_end);
	}
	
	var order_status_id = [];
	$('#filter_order_status_id option:selected').each(function() {
		order_status_id.push($(this).val());
	});
	var filter_order_status_id = order_status_id.join(',');
	if (filter_order_status_id) {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}

	var filter_order_id_from = $('input[name=\'filter_order_id_from\']').val();
	if (filter_order_id_from) {
		url += '&filter_order_id_from=' + encodeURIComponent(filter_order_id_from);
	}

	var filter_order_id_to = $('input[name=\'filter_order_id_to\']').val();
	if (filter_order_id_to) {
		url += '&filter_order_id_to=' + encodeURIComponent(filter_order_id_to);
	}

	var filter_prod_price_min = $('input[name=\'filter_prod_price_min\']').val();
	if (filter_prod_price_min) {
		url += '&filter_prod_price_min=' + encodeURIComponent(filter_prod_price_min);
	}

	var filter_prod_price_max = $('input[name=\'filter_prod_price_max\']').val();
	if (filter_prod_price_max) {
		url += '&filter_prod_price_max=' + encodeURIComponent(filter_prod_price_max);
	}
	
	var store_id = [];
	$('#filter_store_id option:selected').each(function() {
		store_id.push($(this).val());
	});
	var filter_store_id = store_id.join(',');
	if (filter_store_id) {
		url += '&filter_store_id=' + encodeURIComponent(filter_store_id);
	}
	
	var currency = [];
	$('#filter_currency option:selected').each(function() {
		currency.push($(this).val());
	});
	var filter_currency = currency.join(',');
	if (filter_currency) {
		url += '&filter_currency=' + encodeURIComponent(filter_currency);
	}	
	
	var taxes = [];
	$('#filter_taxes option:selected').each(function() {
		taxes.push($(this).val());
	});
	var filter_taxes = taxes.join(',');
	if (filter_taxes) {
		url += '&filter_taxes=' + encodeURIComponent(filter_taxes);
	}

	var tax_classes = [];
	$('#filter_tax_classes option:selected').each(function() {
		tax_classes.push($(this).val());
	});
	var filter_tax_classes = tax_classes.join(',');
	if (filter_tax_classes) {
		url += '&filter_tax_classes=' + encodeURIComponent(filter_tax_classes);
	}

	var geo_zones = [];
	$('#filter_geo_zones option:selected').each(function() {
		geo_zones.push($(this).val());
	});
	var filter_geo_zones = geo_zones.join(',');
	if (filter_geo_zones) {
		url += '&filter_geo_zones=' + encodeURIComponent(filter_geo_zones);
	}
	
	var customer_group_id = [];
	$('#filter_customer_group_id option:selected').each(function() {
		customer_group_id.push($(this).val());
	});
	var filter_customer_group_id = customer_group_id.join(',');
	if (filter_customer_group_id) {
		url += '&filter_customer_group_id=' + encodeURIComponent(filter_customer_group_id);
	}
	
	var filter_customer_name = $('input[name=\'filter_customer_name\']').val();
	if (filter_customer_name) {
		url += '&filter_customer_name=' + encodeURIComponent(filter_customer_name);
	}

	var filter_customer_email = $('input[name=\'filter_customer_email\']').val();
	if (filter_customer_email) {
		url += '&filter_customer_email=' + encodeURIComponent(filter_customer_email);
	}
	
	var filter_customer_telephone = $('input[name=\'filter_customer_telephone\']').val();
	if (filter_customer_telephone) {
		url += '&filter_customer_telephone=' + encodeURIComponent(filter_customer_telephone);
	}
	
	var filter_ip = $('input[name=\'filter_ip\']').val();
	if (filter_ip) {
		url += '&filter_ip=' + encodeURIComponent(filter_ip);
	}
	
	var filter_payment_company = $('input[name=\'filter_payment_company\']').val();
	if (filter_payment_company) {
		url += '&filter_payment_company=' + encodeURIComponent(filter_payment_company);
	}
	
	var filter_payment_address = $('input[name=\'filter_payment_address\']').val();
	if (filter_payment_address) {
		url += '&filter_payment_address=' + encodeURIComponent(filter_payment_address);
	}
	
	var filter_payment_city = $('input[name=\'filter_payment_city\']').val();
	if (filter_payment_city) {
		url += '&filter_payment_city=' + encodeURIComponent(filter_payment_city);
	}

	var filter_payment_zone = $('input[name=\'filter_payment_zone\']').val();
	if (filter_payment_zone) {
		url += '&filter_payment_zone=' + encodeURIComponent(filter_payment_zone);
	}
	
	var filter_payment_postcode = $('input[name=\'filter_payment_postcode\']').val();
	if (filter_payment_postcode) {
		url += '&filter_payment_postcode=' + encodeURIComponent(filter_payment_postcode);
	}
	
	var filter_payment_country = $('input[name=\'filter_payment_country\']').val();
	if (filter_payment_country) {
		url += '&filter_payment_country=' + encodeURIComponent(filter_payment_country);
	}
	
	var payment_method = [];
	$('#filter_payment_method option:selected').each(function() {
		payment_method.push($(this).val());
	});
	var filter_payment_method = payment_method.join(',');
	if (filter_payment_method) {
		url += '&filter_payment_method=' + encodeURIComponent(filter_payment_method);
	}

	var filter_shipping_company = $('input[name=\'filter_shipping_company\']').val();
	if (filter_shipping_company) {
		url += '&filter_shipping_company=' + encodeURIComponent(filter_shipping_company);
	}
	
	var filter_shipping_address = $('input[name=\'filter_shipping_address\']').val();
	if (filter_shipping_address) {
		url += '&filter_shipping_address=' + encodeURIComponent(filter_shipping_address);
	}
	
	var filter_shipping_city = $('input[name=\'filter_shipping_city\']').val();
	if (filter_shipping_city) {
		url += '&filter_shipping_city=' + encodeURIComponent(filter_shipping_city);
	}

	var filter_shipping_zone = $('input[name=\'filter_shipping_zone\']').val();
	if (filter_shipping_zone) {
		url += '&filter_shipping_zone=' + encodeURIComponent(filter_shipping_zone);
	}
	
	var filter_shipping_postcode = $('input[name=\'filter_shipping_postcode\']').val();
	if (filter_shipping_postcode) {
		url += '&filter_shipping_postcode=' + encodeURIComponent(filter_shipping_postcode);
	}
	
	var filter_shipping_country = $('input[name=\'filter_shipping_country\']').val();
	if (filter_shipping_country) {
		url += '&filter_shipping_country=' + encodeURIComponent(filter_shipping_country);
	}
	
	var shipping_method = [];
	$('#filter_shipping_method option:selected').each(function() {
		shipping_method.push($(this).val());
	});
	var filter_shipping_method = shipping_method.join(',');
	if (filter_shipping_method) {
		url += '&filter_shipping_method=' + encodeURIComponent(filter_shipping_method);
	}

	var category = [];
	$('#filter_category option:selected').each(function() {
		category.push($(this).val());
	});
	var filter_category = category.join(',');
	if (filter_category) {
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}

	var manufacturer = [];
	$('#filter_manufacturer option:selected').each(function() {
		manufacturer.push($(this).val());
	});
	var filter_manufacturer = manufacturer.join(',');
	if (filter_manufacturer) {
		url += '&filter_manufacturer=' + encodeURIComponent(filter_manufacturer);
	}

	var filter_sku = $('input[name=\'filter_sku\']').val();
	if (filter_sku) {
		url += '&filter_sku=' + encodeURIComponent(filter_sku);
	}
	
	var filter_product_name = $('input[name=\'filter_product_name\']').val();
	if (filter_product_name) {
		url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
	}
	
	var filter_model = $('input[name=\'filter_model\']').val();
	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}
	
	var option = [];
	$('#filter_option option:selected').each(function() {
		option.push($(this).val());
	});
	var filter_option = option.join(',');
	if (filter_option) {
		url += '&filter_option=' + encodeURIComponent(filter_option);
	}
	
	var attribute = [];
	$('#filter_attribute option:selected').each(function() {
		attribute.push($(this).val());
	});
	var filter_attribute = attribute.join(',');
	if (filter_attribute) {
		url += '&filter_attribute=' + encodeURIComponent(filter_attribute);
	}

	var product_statuses = [];
	$('#filter_product_status option:selected').each(function() {
		product_statuses.push($(this).val());
	});
	var filter_product_status = product_statuses.join(',');
	if (filter_product_status) {
		url += '&filter_product_status=' + encodeURIComponent(filter_product_status);
	}
	
	var locations = [];
	$('#filter_location option:selected').each(function() {
		locations.push($(this).val());
	});
	var filter_location = locations.join(',');
	if (filter_location) {
		url += '&filter_location=' + encodeURIComponent(filter_location);
	}
	
	var affiliate_name = [];
	$('#filter_affiliate_name option:selected').each(function() {
		affiliate_name.push($(this).val());
	});
	var filter_affiliate_name = affiliate_name.join(',');
	if (filter_affiliate_name) {
		url += '&filter_affiliate_name=' + encodeURIComponent(filter_affiliate_name);
	}

	var affiliate_email = [];
	$('#filter_affiliate_email option:selected').each(function() {
		affiliate_email.push($(this).val());
	});
	var filter_affiliate_email = affiliate_email.join(',');
	if (filter_affiliate_email) {
		url += '&filter_affiliate_email=' + encodeURIComponent(filter_affiliate_email);
	}

	var coupon_name = [];
	$('#filter_coupon_name option:selected').each(function() {
		coupon_name.push($(this).val());
	});
	var filter_coupon_name = coupon_name.join(',');
	if (filter_coupon_name) {
		url += '&filter_coupon_name=' + encodeURIComponent(filter_coupon_name);
	}

	var filter_coupon_code = $('input[name=\'filter_coupon_code\']').val();
	if (filter_coupon_code) {
		url += '&filter_coupon_code=' + encodeURIComponent(filter_coupon_code);
	}
	
	var filter_voucher_code = $('input[name=\'filter_voucher_code\']').val();
	if (filter_voucher_code) {
		url += '&filter_voucher_code=' + encodeURIComponent(filter_voucher_code);
	}
	
	location = url;
}	
</script>  
<script type="text/javascript">
$(document).ready(function() {
		$('#filter_order_status_id').multiselect({
			checkboxName: 'order_status_id[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_status; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});		

	<?php if (in_array('store', $advpp_settings_filters)) { ?>
		$('#filter_store_id').multiselect({
			checkboxName: 'store_id[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_stores; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});		
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('currency', $advpp_settings_filters)) { ?>
		$('#filter_currency').multiselect({
			checkboxName: 'currency[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_currencies; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});
	<?php } ?>
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('tax', $advpp_settings_filters)) { ?>
		$('#filter_taxes').multiselect({
			checkboxName: 'taxes[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_taxes; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>
	<?php } ?>

	<?php if (in_array('tax_class', $advpp_settings_filters)) { ?>
		$('#filter_tax_classes').multiselect({
			checkboxName: 'tax_classes[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_tax_classes; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('geo_zone', $advpp_settings_filters)) { ?>
		$('#filter_geo_zones').multiselect({
			checkboxName: 'geo_zones[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_zones; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('customer_group', $advpp_settings_filters)) { ?>
		$('#filter_customer_group_id').multiselect({
			checkboxName: 'customer_group_id[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_groups; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('payment_method', $advpp_settings_filters)) { ?>
		$('#filter_payment_method').multiselect({
			checkboxName: 'payment_method[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_payment_methods; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('shipping_method', $advpp_settings_filters)) { ?>
		$('#filter_shipping_method').multiselect({
			checkboxName: 'shipping_method[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_shipping_methods; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>
	<?php } ?>

	<?php if (in_array('category', $advpp_settings_filters)) { ?>
		$('#filter_category').multiselect({
			checkboxName: 'category[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_categories; ?>',
			numberDisplayed: 0,
			enableClickableOptGroups: true,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>

	<?php if (in_array('manufacturer', $advpp_settings_filters)) { ?>
		$('#filter_manufacturer').multiselect({
			checkboxName: 'manufacturer[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_manufacturers; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('option', $advpp_settings_filters)) { ?>
		$('#filter_option').multiselect({
			checkboxName: 'option[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_options; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>
	<?php } ?>

	<?php if (in_array('attribute', $advpp_settings_filters)) { ?>
		$('#filter_attribute').multiselect({
			checkboxName: 'attribute[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_attributes; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>

	<?php if (in_array('product_status', $advpp_settings_filters)) { ?>
		$('#filter_product_status').multiselect({
			checkboxName: 'product_statuses[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_status; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300

		});	
	<?php } ?>
	
	<?php if (in_array('location', $advpp_settings_filters)) { ?>
		$('#filter_location').multiselect({
			checkboxName: 'locations[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_locations; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('affiliate_name', $advpp_settings_filters)) { ?>
		$('#filter_affiliate_name').multiselect({
			checkboxName: 'affiliate_name[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_affiliate_names; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('affiliate_email', $advpp_settings_filters)) { ?>
		$('#filter_affiliate_email').multiselect({
			checkboxName: 'affiliate_email[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_affiliate_emails; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});	
	<?php } ?>
	<?php } ?>

	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders') { ?>
	<?php if (in_array('coupon_name', $advpp_settings_filters)) { ?>
		$('#filter_coupon_name').multiselect({
			checkboxName: 'coupon_name[]',
			includeSelectAllOption: true,
			enableFiltering: true,
			selectAllText: '<?php echo $text_select_all; ?>',
			allSelectedText: '<?php echo $text_selected; ?>',
			nonSelectedText: '<?php echo $text_all_coupon_names; ?>',
			numberDisplayed: 0,
			disableIfEmpty: true,
			maxHeight: 300
		});		
	<?php } ?>
	<?php } ?>
});
</script> 
<script type="text/javascript">
function setCookie(name,value,days) {
	var expires;
	if (days) {
	var date = new Date();
	date.setTime(date.getTime()+(days*24*60*60*1000));
	expires = "; expires="+date.toGMTString();
	} else {
	expires = "";
	}
	document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
	var c = ca[i];
	while (c.charAt(0)==' ') c = c.substring(1,c.length);
	if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

	var initCookies = function() {
		setCookie('show-pp-chart', 1, 365);
		$('.chart-switcher').removeClass('hide-chart');
	};
	
	if (getCookie('show-pp-chart') == 1) {
		$('.chart-switcher').show();
	} else if (getCookie('show-pp-chart') == 2) {
		$('.chart-switcher').hide();
	} else {
		$('.chart-switcher').show();
	}
	
	$(document).on('click', '.chart-button', function(e) {
		e.preventDefault();
		if (getCookie('show-pp-chart') == 1) {
			$('.chart-switcher').hide();
			setCookie('show-pp-chart', 2, 365);
		} else if (getCookie('show-pp-chart') == 2) {
			$('.chart-switcher').show();
			setCookie('show-pp-chart', 1, 365);		
		} else {
			$('.chart-switcher').hide();
			setCookie('show-pp-chart', 2, 365);
		}
	});
</script>  
<script type="text/javascript"><!--
$('input[name=\'filter_customer_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_customer_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['cust_name'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_customer_name\']').val(item['label']);
	}
});

$('input[name=\'filter_customer_email\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_customer_email=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['cust_email'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_customer_email\']').val(item['label']);
	}
});

$('input[name=\'filter_customer_telephone\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_customer_telephone=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['cust_telephone'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_customer_telephone\']').val(item['label']);
	}
});

$('input[name=\'filter_ip\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_ip=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['cust_ip'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_ip\']').val(item['label']);
	}
});

$('input[name=\'filter_payment_company\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_payment_company=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['payment_company'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_payment_company\']').val(item['label']);
	}
});

$('input[name=\'filter_payment_address\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_payment_address=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['payment_address'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_payment_address\']').val(item['label']);
	}
});

$('input[name=\'filter_payment_city\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_payment_city=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['payment_city'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_payment_city\']').val(item['label']);
	}
});

$('input[name=\'filter_payment_zone\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_payment_zone=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['payment_zone'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_payment_zone\']').val(item['label']);
	}
});

$('input[name=\'filter_payment_postcode\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_payment_postcode=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['payment_postcode'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_payment_postcode\']').val(item['label']);
	}
});

$('input[name=\'filter_payment_country\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_payment_country=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['payment_country'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_payment_country\']').val(item['label']);
	}
});

$('input[name=\'filter_shipping_company\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_shipping_company=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['shipping_company'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_shipping_company\']').val(item['label']);
	}
});

$('input[name=\'filter_shipping_address\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_shipping_address=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['shipping_address'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_shipping_address\']').val(item['label']);
	}
});

$('input[name=\'filter_shipping_city\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_shipping_city=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['shipping_city'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_shipping_city\']').val(item['label']);
	}
});

$('input[name=\'filter_shipping_zone\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_shipping_zone=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['shipping_zone'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_shipping_zone\']').val(item['label']);
	}
});

$('input[name=\'filter_shipping_postcode\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_shipping_postcode=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['shipping_postcode'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_shipping_postcode\']').val(item['label']);
	}
});

$('input[name=\'filter_shipping_country\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/customer_autocomplete&token=<?php echo $token; ?>&filter_shipping_country=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['shipping_country'],
						value: item['customer_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_shipping_country\']').val(item['label']);
	}
});

$('input[name=\'filter_sku\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/product_autocomplete&token=<?php echo $token; ?>&filter_sku=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {

					return {
						label: item['prod_sku'],
						value: item['product_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_sku\']').val(item['label']);
	}
});

$('input[name=\'filter_product_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/product_autocomplete&token=<?php echo $token; ?>&filter_product_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['prod_name'],
						value: item['product_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_product_name\']').val(item['label']);
	}
});

$('input[name=\'filter_model\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/product_autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['prod_model'],
						value: item['product_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_model\']').val(item['label']);
	}
});

$('input[name=\'filter_coupon_code\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/coupon_autocomplete&token=<?php echo $token; ?>&filter_coupon_code=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['coupon_code'],
						value: item['coupon_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_coupon_code\']').val(item['label']);
	}
});

$('input[name=\'filter_voucher_code\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_products/voucher_autocomplete&token=<?php echo $token; ?>&filter_voucher_code=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['voucher_code'],
						value: item['voucher_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'filter_voucher_code\']').val(item['label']);
	}
});
//--></script> 
<?php if ($products) { ?> 
	<?php if ($filter_report != 'all_products_with_without_orders' && $filter_report != 'products_without_orders' && $filter_details != 'all_details' && $filter_group == 'no_group') { ?>  
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript"><!--
	google.load('visualization', '1', {packages: ['corechart']});
      google.setOnLoadCallback(drawChart);      
	  function drawChart() { 
   		var data = google.visualization.arrayToDataTable([
		<?php if ($filter_report == 'products_purchased_without_options' || $filter_report == 'products_purchased_with_options' || $filter_report == 'new_products_purchased' || $filter_report == 'old_products_purchased' || $filter_report == 'products_abandoned_orders') {
			echo "['" . $column_name . "','". $column_sold_quantity . "'],";
					$i = 0;
					foreach ($products as $key => $product) {
						if (count($products)==($key+1)) {
							echo "['" . $product['gname'] . "',". $product['gsold'] . "]";
						} else {
							echo "['" . $product['gname'] . "',". $product['gsold'] . "],";
						}
						if (++$i > 9) break;
					}
		} elseif ($filter_report == 'manufacturers') {
			echo "['" . $column_name . "','". $column_sold_quantity . "'],";
					$i = 0;
					foreach ($products as $key => $product) {
						if (count($products)==($key+1)) {
							echo "['" . $product['gmanufacturer'] . "',". $product['gsold'] . "]";
						} else {
							echo "['" . $product['gmanufacturer'] . "',". $product['gsold'] . "],";
						}
						if (++$i > 9) break;
					}
		} elseif ($filter_report == 'categories') {
			echo "['" . $column_name . "','". $column_sold_quantity . "'],";
					$i = 0;
					foreach ($products as $key => $product) {
						if (count($products)==($key+1)) {
							echo "['" . $product['gcategories'] . "',". $product['gsold'] . "]";
						} else {
							echo "['" . $product['gcategories'] . "',". $product['gsold'] . "],";
						}
						if (++$i > 9) break;
					}
		}
		;?>
		]);


        var options = {
			title: <?php if ($filter_report != 'products_abandoned_orders' && $filter_report != 'manufacturers' && $filter_report != 'categories') {
			echo "'Top Sold Products'";
			} elseif ($filter_report == 'products_abandoned_orders') {
			echo "'Top Products from Abandoned Orders'";			
			} elseif ($filter_report == 'manufacturers') {
			echo "'Top Manufacturers'";
			} elseif ($filter_report == 'categories') {
			echo "'Top Categories'";
			} ?>,
			height: 280,			
			pieSliceText: 'none',
			tooltip: {text: 'value'},
			pieHole: 0.4,
			chartArea: {right: 75, top: 40, width: "75%", height: "75%"}
        };

			var chart = new google.visualization.PieChart(document.getElementById('chart1_div'));
			chart.draw(data, options);

    		$(window).resize(function(){
        		chart.draw(data, options);
    		});
	}
//--></script>
<script type="text/javascript"><!--
	google.load('visualization', '1', {packages: ['corechart']});
      google.setOnLoadCallback(drawChart);      
	  function drawChart() { 
   		var data = google.visualization.arrayToDataTable([
		<?php if ($filter_report == 'products_purchased_without_options' || $filter_report == 'products_purchased_with_options' || $filter_report == 'new_products_purchased' || $filter_report == 'old_products_purchased' || $filter_report == 'products_abandoned_orders') {
			echo "['" . $column_name . "','". $column_gtotal . "'],";
					$i = 0;
					foreach ($products as $key => $product) {
						if (count($products)==($key+1)) {
							echo "['" . $product['gname'] . "',". $product['gtotal'] . "]";
						} else {
							echo "['" . $product['gname'] . "',". $product['gtotal'] . "],";
						}
						if (++$i > 9) break;
					}
		} elseif ($filter_report == 'manufacturers') {
			echo "['" . $column_name . "','". $column_gtotal . "'],";
					$i = 0;
					foreach ($products as $key => $product) {
						if (count($products)==($key+1)) {
							echo "['" . $product['gmanufacturer'] . "',". $product['gtotal'] . "]";
						} else {
							echo "['" . $product['gmanufacturer'] . "',". $product['gtotal'] . "],";
						}
						if (++$i > 9) break;
					}
		} elseif ($filter_report == 'categories') {
			echo "['" . $column_name . "','". $column_gtotal . "'],";
					$i = 0;
					foreach ($products as $key => $product) {
						if (count($products)==($key+1)) {
							echo "['" . $product['gcategories'] . "',". $product['gtotal'] . "]";
						} else {
							echo "['" . $product['gcategories'] . "',". $product['gtotal'] . "],";
						}
						if (++$i > 9) break;
					}
		}
		;?>
		]);

        var options = {
			width: "100%",
			height: 280,			
			colors: ['#b5e08b'],			
			bar: {groupWidth: "80%"},
			chartArea: {top: 35, width: "60%", height: "70%"}		
		};

			var chart = new google.visualization.BarChart(document.getElementById('chart2_div'));
			chart.draw(data, options);

    		$(window).resize(function(){
        		chart.draw(data, options);
    		});
	}
//--></script>
	<?php } ?>
<?php } ?> 
<script type="text/javascript"><!--
$('#date-start').datetimepicker({
	pickTime: false
});
$('#date-end').datetimepicker({
	pickTime: false
});
$('#status-date-start').datetimepicker({
	pickTime: false
});
$('#status-date-end').datetimepicker({
	pickTime: false
});
$('.select').selectpicker();
//--></script>
<script type="text/javascript"><!--
$('#save_settings').on('click', function(){
	$.ajax ({
		url: 'index.php?route=report/adv_products/settings&token=<?php echo $token; ?>',		
		type: 'post',
		data: $('#settings input[type=\'text\'], #settings input[type=\'hidden\'], #settings input[type=\'radio\']:checked, #settings input[type=\'checkbox\']:checked, #settings select'),
		dataType: 'json',
    	beforeSend: function() {
			$('.alert').remove();
			$('#save_settings').button('loading');
		},  
		complete: function() {
				$('#save_settings').button('reset');
		},          
		success: function(json) {
			if (json['error']) {
				$('#settings').animate({scrollTop:0});				
				$('#settings .modal-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				$(".alert-danger").show(0).delay(1500).fadeOut(2500);
			}
			if (json['success']) {
				$('#settings').animate({scrollTop:0});				
				$('#settings .modal-body').prepend('<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				location=('<?php echo $url; ?>').replace(/&amp;/g, '&');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#save_report').on('click', function(){
	$.ajax ({
		url: 'index.php?route=report/adv_products/load_save_report&token=<?php echo $token; ?>',		
		type: 'post',
		data: $('#load_save input[type=\'text\'], #load_save textarea'),
		dataType: 'json',
    	beforeSend: function() {
			$('.alert').remove();
			$('#save_report').button('loading');			
		},  
		complete: function() {
				$('#save_report').button('reset');			
		},          
		success: function(json) {
			if (json['error']) {
				$('#load_save').animate({scrollTop:0});				
				$('#load_save .modal-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');				
				$(".alert-danger").show(0).delay(1500).fadeOut(2500);
			}
			if (json['success']) {
				$('#load_save').animate({scrollTop:0});				
				$('#load_save .modal-body').prepend('<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');	
				location=('<?php echo $url; ?>').replace(/&amp;/g, '&');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

var adv_load_save_reports_row = <?php echo $adv_load_save_reports_row; ?>;

function addSaveReport() {
	html  = '<tbody id="adv_load_save_reports_row' + adv_load_save_reports_row + '">';
	html += '<tr>';
	html += '<td width="5%"></td>';	
	html += '<td width="45%" class="text-left"><input type="text" name="advpp_load_save_report[' + adv_load_save_reports_row + '][save_report_title]" placeholder="<?php echo $text_report_title; ?>" value="" class="form-control" /></td>';
	html += '<td width="40%" class="text-left"><textarea readonly name="advpp_load_save_report[' + adv_load_save_reports_row + '][save_report_link]" class="form-control"><?php echo $save_report_link; ?></textarea></td>';
	html += '<td width="10%"></td>';
	html += '</tr>';
	html += '</tbody>';

	$('#adv_load_save > tfoot').before(html);

	adv_load_save_reports_row++;
}

$('#export_report').on('click', function(){
	var url_exp ='index.php?route=report/adv_products/export&token=<?php echo $token; ?>';

	var report_type = $('select[name=\'report_type\']').val();
	if (report_type) {
		url_exp += '&report_type=' + encodeURIComponent(report_type);
	}
	
	var export_type = $('select[name=\'export_type\']').val();
	if (export_type) {
		url_exp += '&export_type=' + encodeURIComponent(export_type);
	}
	
	var export_logo_criteria = $('select[name=\'export_logo_criteria\']').val();
	if (export_logo_criteria) {
		url_exp += '&export_logo_criteria=' + encodeURIComponent(export_logo_criteria);
	}
	
	var export_csv_delimiter = $('select[name=\'export_csv_delimiter\']').val();
	if (export_csv_delimiter) {
		url_exp += '&export_csv_delimiter=' + encodeURIComponent(export_csv_delimiter);
	}
	
	$.ajax ({
		url: 'index.php?route=report/adv_products/export_validate&token=<?php echo $token; ?>',
		type: 'post',
		data: $('#export input[type=\'text\'], #export input[type=\'hidden\'], #export input[type=\'radio\']:checked, #export input[type=\'checkbox\']:checked, #export select'),
		dataType: 'json',
    	beforeSend: function() {
			$('.alert').remove();
			$('#export_report').button('loading');
		},  
		complete: function() {
			$('#export_report').button('reset');
		},          
		success: function(json) {
			if (json['error']) {
					$('#export .modal-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					$(".alert-danger").show(0).delay(2000).fadeOut(2000);
			} else {
				location = url_exp;
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#save_cron').on('click', function(){
	if ($("#filter_order_status_id").val() != null) {filter_order_status_id_val = $("#filter_order_status_id").val().join(',')} else {filter_order_status_id_val = ''};
	if ($("#filter_store_id").val() != null) {filter_store_id_val = $("#filter_store_id").val().join(',')} else {filter_store_id_val = ''};
	if ($("#filter_currency").val() != null) {filter_currency_val = $("#filter_currency").val().join(',')} else {filter_currency_val = ''};
	if ($("#filter_taxes").val() != null) {filter_taxes_val = $("#filter_taxes").val().join(',')} else {filter_taxes_val = ''};
	if ($("#filter_tax_classes").val() != null) {filter_tax_classes_val = $("#filter_tax_classes").val().join(',')} else {filter_tax_classes_val = ''};
	if ($("#filter_geo_zones").val() != null) {filter_geo_zones_val = $("#filter_geo_zones").val().join(',')} else {filter_geo_zones_val = ''};
	if ($("#filter_customer_group_id").val() != null) {filter_customer_group_id_val = $("#filter_customer_group_id").val().join(',')} else {filter_customer_group_id_val = ''};
	if ($("#filter_payment_method").val() != null) {filter_payment_method_val = $("#filter_payment_method").val().join(',')} else {filter_payment_method_val = ''};
	if ($("#filter_shipping_method").val() != null) {filter_shipping_method_val = $("#filter_shipping_method").val().join(',')} else {filter_shipping_method_val = ''};
	if ($("#filter_category").val() != null) {filter_category_val = $("#filter_category").val().join(',')} else {filter_category_val = ''};
	if ($("#filter_manufacturer").val() != null) {filter_manufacturer_val = $("#filter_manufacturer").val().join(',')} else {filter_manufacturer_val = ''};
	if ($("#filter_option").val() != null) {filter_option_val = $("#filter_option").val().join(',')} else {filter_option_val = ''};
	if ($("#filter_attribute").val() != null) {filter_attribute_val = $("#filter_attribute").val().join(',')} else {filter_attribute_val = ''};
	if ($("#filter_product_status").val() != null) {filter_product_status_val = $("#filter_product_status").val().join(',')} else {filter_product_status_val = ''};
	if ($("#filter_location").val() != null) {filter_location_val = $("#filter_location").val().join(',')} else {filter_location_val = ''};	
	if ($("#filter_affiliate_name").val() != null) {filter_affiliate_name_val = $("#filter_affiliate_name").val().join(',')} else {filter_affiliate_name_val = ''};
	if ($("#filter_affiliate_email").val() != null) {filter_affiliate_email_val = $("#filter_affiliate_email").val().join(',')} else {filter_affiliate_email_val = ''};
	if ($("#filter_coupon_name").val() != null) {filter_coupon_name_val = $("#filter_coupon_name").val().join(',')} else {filter_coupon_name_val = ''};
	
	$.ajax ({
		url: 'index.php?route=report/adv_products/cron&token=<?php echo $token; ?>',		
		type: 'post',
		data: {
			advpp0cfs_filter_report: $("#filter_report").val(), 
			advpp0cfs_filter_details: $("#filter_details").val(), 
			advpp0cfs_filter_group: $("#filter_group").val(), 
			advpp0cfs_filter_sort: $("#filter_sort").val(), 
			advpp0cfs_filter_limit: $("#filter_limit").val(), 
			advpp0cfs_filter_range: $("#filter_range").val(), 
			advpp0cfs_filter_date_start: $("#date-start").val(), 
			advpp0cfs_filter_date_end: $("#date-start").val(), 
			advpp0cfs_filter_order_status_id: filter_order_status_id_val,
			advpp0cfs_filter_status_date_start: $("#status-date-start").val(), 
			advpp0cfs_filter_status_date_end: $("#status-date-end").val(), 
			advpp0cfs_filter_order_id_from: $("#filter_order_id_from").val(), 
			advpp0cfs_filter_order_id_to: $("#filter_order_id_to").val(), 
			advpp0cfs_filter_prod_price_min: $("#filter_prod_price_min").val(), 
			advpp0cfs_filter_prod_price_max: $("#filter_prod_price_max").val(), 
			advpp0cfs_filter_store_id: filter_store_id_val, 
			advpp0cfs_filter_currency: filter_currency_val,
			advpp0cfs_filter_taxes: filter_taxes_val,
			advpp0cfs_filter_tax_classes: filter_tax_classes_val,
			advpp0cfs_filter_geo_zones: filter_geo_zones_val,
			advpp0cfs_filter_customer_group_id: filter_customer_group_id_val,
			advpp0cfs_filter_customer_name: $("#filter_customer_name").val(), 
			advpp0cfs_filter_customer_email: $("#filter_customer_email").val(), 
			advpp0cfs_filter_customer_telephone: $("#filter_customer_telephone").val(), 
			advpp0cfs_filter_ip: $("#filter_ip").val(), 
			advpp0cfs_filter_payment_company: $("#filter_payment_company").val(), 
			advpp0cfs_filter_payment_address: $("#filter_payment_address").val(), 
			advpp0cfs_filter_payment_city: $("#filter_payment_city").val(), 
			advpp0cfs_filter_payment_zone: $("#filter_payment_zone").val(), 
			advpp0cfs_filter_payment_postcode: $("#filter_payment_postcode").val(), 
			advpp0cfs_filter_payment_country: $("#filter_payment_country").val(), 
			advpp0cfs_filter_payment_method: filter_payment_method_val,
			advpp0cfs_filter_shipping_company: $("#filter_shipping_company").val(), 
			advpp0cfs_filter_shipping_address: $("#filter_shipping_address").val(), 
			advpp0cfs_filter_customer_telephone: $("#filter_customer_telephone").val(), 
			advpp0cfs_filter_shipping_city: $("#filter_shipping_city").val(), 
			advpp0cfs_filter_shipping_zone: $("#filter_shipping_zone").val(), 
			advpp0cfs_filter_shipping_postcode: $("#filter_shipping_postcode").val(), 
			advpp0cfs_filter_shipping_country: $("#filter_shipping_country").val(), 
			advpp0cfs_filter_shipping_method: filter_shipping_method_val,
			advpp0cfs_filter_category: filter_category_val,
			advpp0cfs_filter_manufacturer: filter_manufacturer_val, 
			advpp0cfs_filter_sku: $("#filter_sku").val(), 
			advpp0cfs_filter_product_name: $("#filter_product_name").val(), 
			advpp0cfs_filter_model: $("#filter_model").val(), 
			advpp0cfs_filter_option: filter_option_val, 
			advpp0cfs_filter_attribute: filter_attribute_val, 
			advpp0cfs_filter_product_status: filter_product_status_val, 
			advpp0cfs_filter_location: filter_location_val, 
			advpp0cfs_filter_affiliate_name: filter_affiliate_name_val, 
			advpp0cfs_filter_affiliate_email: filter_affiliate_email_val, 
			advpp0cfs_filter_coupon_name: filter_coupon_name_val, 
			advpp0cfs_filter_coupon_code: $("#filter_coupon_code").val(), 
			advpp0cfs_filter_voucher_code: $("#filter_voucher_code").val(), 
			
			advpp0cfs_report_type: $("#cron_report_type").val(), 
			advpp0cfs_export_type: $("#cron_export_type").val(), 
			advpp0cfs_export_logo_criteria: $("#cron_export_logo_criteria").val(), 
			advpp0cfs_export_csv_delimiter: $("#cron_export_csv_delimiter").val(), 
			advpp0cfs_export_file: $("#cron_export_file").val(), 
			advpp0cfs_file_save_path: $("#cron_file_save_path").val(), 
			advpp0cfs_file_name: $("#cron_file_name").val(), 
			advpp0cfs_email: $("#cron_email").val(),
			advpp0cfs_user: $("#cron_user").val(),
			advpp0cfs_pass: btoa(btoa($("#cron_pass").val())),
			advpp0cfs_token: $("#cron_token").val(),
			advpp0cfs_user_id: $("#cron_user_id").val()
			},
		dataType: 'json',
    	beforeSend: function() {
			$('.alert').remove();
			$('#save_cron').button('loading');			
		},  
		complete: function() {
				$('#save_cron').button('reset');			
		},          
		success: function(json) {
			if (json['error']) {
				$('#cron').animate({scrollTop:0});				
				$('#cron .modal-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');				
				$(".alert-danger").show(0).delay(1500).fadeOut(2500);
			}
			if (json['success']) {
				$('#cron').animate({scrollTop:0});				
				$('#cron .modal-body').prepend('<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');	
				location=('<?php echo $url; ?>').replace(/&amp;/g, '&');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

function cron_token_gen() {
	var text = "";
	var arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for(var i=0; i<25; i++) { text += arr.charAt(Math.floor(Math.random() * arr.length)); }
	$("#cron_token").val(text);
}

function cron_command() {
	var link = window.location+'';
	link = link.split("index.php");
	link = link[0] + "index.php?route=common/login";
	var token = $("#cron_token").val();
	var addr = link+"&cron_route="+"report/adv_products"+"&cron_token="+token+"&cron=1";
	$("#cpanel").html('wget -q '+addr)
}

function selectText(containerid) {
	var node = document.getElementById(containerid);
	if (document.selection) {
		var range = document.body.createTextRange();
		range.moveToElementText(node);
		range.select();
	} else if (window.getSelection) {
		var range = document.createRange();
		range.selectNodeContents(node);
		window.getSelection().removeAllRanges();
		window.getSelection().addRange(range);
	}
}
//--></script>
<script type="text/javascript"><!--
function checkValidOptions() {
  var filter_report = document.getElementById('filter_report');
  var filter_details = document.getElementById('filter_details');
    if ((filter_report.options[0].selected === true) || (filter_report.options[3].selected === true)) {
        document.getElementById("filter_details").options[0].disabled = true;		
        document.getElementById("filter_details").options[1].disabled = true;
        document.getElementById("filter_details").options[2].disabled = true;
		
        document.getElementById("filter_group").options[0].disabled = true;
        document.getElementById("filter_group").options[1].disabled = true;		
        document.getElementById("filter_group").options[2].disabled = true;
        document.getElementById("filter_group").options[3].disabled = true;
		document.getElementById("filter_group").options[4].disabled = true;
		document.getElementById("filter_group").options[5].disabled = true;
		document.getElementById("filter_group").options[6].disabled = true;
	}	
    if (filter_report.options[3].selected === true) {
		<?php if (in_array('mv_sold_quantity', $advpp_settings_mv_columns)) { ?>document.getElementById("sold_quantity").disabled = true;<?php } ?>
		<?php if (in_array('mv_total_excl_vat', $advpp_settings_mv_columns)) { ?>document.getElementById("total_excl_vat").disabled = true;<?php } ?>
		<?php if (in_array('mv_total_tax', $advpp_settings_mv_columns)) { ?>document.getElementById("total_tax").disabled = true;<?php } ?>
		<?php if (in_array('mv_total_incl_vat', $advpp_settings_mv_columns)) { ?>document.getElementById("total_incl_vat").disabled = true;<?php } ?>
		<?php if (in_array('mv_app', $advpp_settings_mv_columns)) { ?>document.getElementById("app").disabled = true;<?php } ?>
		<?php if (in_array('mv_refunds', $advpp_settings_mv_columns)) { ?>document.getElementById("refunds").disabled = true;<?php } ?>
		<?php if (in_array('mv_reward_points', $advpp_settings_mv_columns)) { ?>document.getElementById("reward_points").disabled = true;<?php } ?>
	}	
    if (filter_report.options[6].selected === true) {
		<?php if (in_array('mv_category', $advpp_settings_mv_columns)) { ?>document.getElementById("category").disabled = true;<?php } ?>		
		<?php if ($filter_report != 'manufacturers' && $filter_report != 'categories') { ?>
		<?php if (in_array('mv_id', $advpp_settings_mv_columns)) { ?>document.getElementById("id").disabled = true;<?php } ?>
		<?php if (in_array('mv_sku', $advpp_settings_mv_columns)) { ?>document.getElementById("sku").disabled = true;<?php } ?>
		<?php if (in_array('mv_name', $advpp_settings_mv_columns)) { ?>document.getElementById("name").disabled = true;<?php } ?>
		<?php if (in_array('mv_model', $advpp_settings_mv_columns)) { ?>document.getElementById("model").disabled = true;<?php } ?>
		<?php if (in_array('mv_attribute', $advpp_settings_mv_columns)) { ?>document.getElementById("attribute").disabled = true;<?php } ?>
		<?php if (in_array('mv_status', $advpp_settings_mv_columns)) { ?>document.getElementById("status").disabled = true;<?php } ?>
		<?php if (in_array('mv_location', $advpp_settings_mv_columns)) { ?>document.getElementById("location").disabled = true;<?php } ?>
		<?php if (in_array('mv_tax_class', $advpp_settings_mv_columns)) { ?>document.getElementById("tax_class").disabled = true;<?php } ?>
		<?php if (in_array('mv_price', $advpp_settings_mv_columns)) { ?>document.getElementById("price").disabled = true;<?php } ?>
		<?php if (in_array('mv_viewed', $advpp_settings_mv_columns)) { ?>document.getElementById("viewed").disabled = true;<?php } ?>
		<?php if (in_array('mv_stock_quantity', $advpp_settings_mv_columns)) { ?>document.getElementById("stock_quantity").disabled = true;<?php } ?>
		<?php } ?>
	}	
    if (filter_report.options[7].selected === true) {
		<?php if (in_array('mv_manufacturer', $advpp_settings_mv_columns)) { ?>document.getElementById("manufacturer").disabled = true;<?php } ?>		
		<?php if ($filter_report != 'manufacturers' && $filter_report != 'categories') { ?>
		<?php if (in_array('mv_id', $advpp_settings_mv_columns)) { ?>document.getElementById("id").disabled = true;<?php } ?>
		<?php if (in_array('mv_sku', $advpp_settings_mv_columns)) { ?>document.getElementById("sku").disabled = true;<?php } ?>
		<?php if (in_array('mv_name', $advpp_settings_mv_columns)) { ?>document.getElementById("name").disabled = true;<?php } ?>
		<?php if (in_array('mv_model', $advpp_settings_mv_columns)) { ?>document.getElementById("model").disabled = true;<?php } ?>
		<?php if (in_array('mv_attribute', $advpp_settings_mv_columns)) { ?>document.getElementById("attribute").disabled = true;<?php } ?>
		<?php if (in_array('mv_status', $advpp_settings_mv_columns)) { ?>document.getElementById("status").disabled = true;<?php } ?>
		<?php if (in_array('mv_location', $advpp_settings_mv_columns)) { ?>document.getElementById("location").disabled = true;<?php } ?>
		<?php if (in_array('mv_tax_class', $advpp_settings_mv_columns)) { ?>document.getElementById("tax_class").disabled = true;<?php } ?>
		<?php if (in_array('mv_price', $advpp_settings_mv_columns)) { ?>document.getElementById("price").disabled = true;<?php } ?>
		<?php if (in_array('mv_viewed', $advpp_settings_mv_columns)) { ?>document.getElementById("viewed").disabled = true;<?php } ?>
		<?php if (in_array('mv_stock_quantity', $advpp_settings_mv_columns)) { ?>document.getElementById("stock_quantity").disabled = true;<?php } ?>
		<?php } ?>
	}	
    if (filter_details.options[2].selected === true) {
        document.getElementById("filter_group").options[0].disabled = true;
        document.getElementById("filter_group").options[1].disabled = true;		
        document.getElementById("filter_group").options[2].disabled = true;
        document.getElementById("filter_group").options[3].disabled = true;
		document.getElementById("filter_group").options[4].disabled = true;
		document.getElementById("filter_group").options[5].disabled = true;
		document.getElementById("filter_group").options[6].disabled = true;
		
		document.getElementById("date").disabled = true;
		<?php if (in_array('mv_id', $advpp_settings_mv_columns)) { ?>document.getElementById("id").disabled = true;<?php } ?>
		<?php if (in_array('mv_sku', $advpp_settings_mv_columns)) { ?>document.getElementById("sku").disabled = true;<?php } ?>
		<?php if (in_array('mv_name', $advpp_settings_mv_columns)) { ?>document.getElementById("name").disabled = true;<?php } ?>
		<?php if (in_array('mv_model', $advpp_settings_mv_columns)) { ?>document.getElementById("model").disabled = true;<?php } ?>
		<?php if (in_array('mv_category', $advpp_settings_mv_columns)) { ?>document.getElementById("category").disabled = true;<?php } ?>
		<?php if (in_array('mv_manufacturer', $advpp_settings_mv_columns)) { ?>document.getElementById("manufacturer").disabled = true;<?php } ?>
		<?php if (in_array('mv_attribute', $advpp_settings_mv_columns)) { ?>document.getElementById("attribute").disabled = true;<?php } ?>
		<?php if (in_array('mv_status', $advpp_settings_mv_columns)) { ?>document.getElementById("status").disabled = true;<?php } ?>
		<?php if (in_array('mv_location', $advpp_settings_mv_columns)) { ?>document.getElementById("location").disabled = true;<?php } ?>
		<?php if (in_array('mv_tax_class', $advpp_settings_mv_columns)) { ?>document.getElementById("tax_class").disabled = true;<?php } ?>
		<?php if (in_array('mv_price', $advpp_settings_mv_columns)) { ?>document.getElementById("price").disabled = true;<?php } ?>
		<?php if (in_array('mv_viewed', $advpp_settings_mv_columns)) { ?>document.getElementById("viewed").disabled = true;<?php } ?>
		<?php if (in_array('mv_stock_quantity', $advpp_settings_mv_columns)) { ?>document.getElementById("stock_quantity").disabled = true;<?php } ?>		
		<?php if (in_array('mv_sold_quantity', $advpp_settings_mv_columns)) { ?>document.getElementById("sold_quantity").disabled = true;<?php } ?>
		<?php if (in_array('mv_total_excl_vat', $advpp_settings_mv_columns)) { ?>document.getElementById("total_excl_vat").disabled = true;<?php } ?>
		<?php if (in_array('mv_total_tax', $advpp_settings_mv_columns)) { ?>document.getElementById("total_tax").disabled = true;<?php } ?>
		<?php if (in_array('mv_total_incl_vat', $advpp_settings_mv_columns)) { ?>document.getElementById("total_incl_vat").disabled = true;<?php } ?>
		<?php if (in_array('mv_app', $advpp_settings_mv_columns)) { ?>document.getElementById("app").disabled = true;<?php } ?>
		<?php if (in_array('mv_refunds', $advpp_settings_mv_columns)) { ?>document.getElementById("refunds").disabled = true;<?php } ?>
		<?php if (in_array('mv_reward_points', $advpp_settings_mv_columns)) { ?>document.getElementById("reward_points").disabled = true;<?php } ?>	
	}	
}
//--></script> 
<script type="text/javascript">
$(document).ready(function() {
var $filter_range = $('#filter_range'), $date_start = $('#date-start'), $date_end = $('#date-end');
$filter_range.change(function () {
    if ($filter_range.val() == 'custom') {
        $date_start.removeAttr('disabled');
		$date_start.css('background-color', '#F9F9F9');	
        $date_end.removeAttr('disabled');
		$date_end.css('background-color', '#F9F9F9');	
    } else {	
        $date_start.attr('disabled', 'disabled').val('');
		$date_start.css('background-color', '#EEE');
        $date_end.attr('disabled', 'disabled').val('');
		$date_end.css('background-color', '#EEE');
    }
}).trigger('change');

var $filter_report_to_export = $('select[name=\'filter_report\']');
var $filter_details_to_export = $('select[name=\'filter_details\']');
	if ($filter_report_to_export.val() == 'all_products_with_without_orders' || $filter_report_to_export.val() == 'products_without_orders') {
		$("#report_to_export option[value='export_no_details']").prop('disabled', false); 
		$("#report_to_export option[value='export_no_details']").prop('selected', true); 		
		$("#report_to_export option[value='export_basic_details']").prop('disabled', true);		
		$("#report_to_export option[value='export_all_details']").prop('disabled', true); 
		$("#type_to_export option[value='']").prop('selected', true); 
		$('#csv_delimiter select').prop('disabled', true);
		$("#type_to_export option[value='export_xls']").prop('disabled', false); 
		$("#type_to_export option[value='export_xlsx']").prop('disabled', false); 
		$("#type_to_export option[value='export_csv']").prop('disabled', false); 
		$("#type_to_export option[value='export_pdf']").prop('disabled', false); 
		$("#type_to_export option[value='export_html']").prop('disabled', false); 		
	} else {
		if ($filter_details_to_export.val() == 'no_details') {
			$("#report_to_export option[value='export_no_details']").prop('disabled', false); 
			$("#report_to_export option[value='export_no_details']").prop('selected', true); 			
			$("#report_to_export option[value='export_basic_details']").prop('disabled', true); 
			$("#report_to_export option[value='export_all_details']").prop('disabled', true); 
			$("#type_to_export option[value='']").prop('selected', true); 
			$('#csv_delimiter select').prop('disabled', true);
			$("#type_to_export option[value='export_xls']").prop('disabled', false); 
			$("#type_to_export option[value='export_xlsx']").prop('disabled', false); 
			$("#type_to_export option[value='export_csv']").prop('disabled', false); 
			$("#type_to_export option[value='export_pdf']").prop('disabled', false); 
			$("#type_to_export option[value='export_html']").prop('disabled', false); 			
		} else if ($filter_details_to_export.val() == 'basic_details') {
			$("#report_to_export option[value='export_no_details']").prop('disabled', true); 
			$("#report_to_export option[value='export_basic_details']").prop('disabled', false); 
			$("#report_to_export option[value='export_basic_details']").prop('selected', true); 
			$("#report_to_export option[value='export_all_details']").prop('disabled', true);
			$("#type_to_export option[value='']").prop('selected', true); 
			$('#csv_delimiter select').prop('disabled', true);
			$("#type_to_export option[value='export_xls']").prop('disabled', true); 
			$("#type_to_export option[value='export_xlsx']").prop('disabled', true); 
			$("#type_to_export option[value='export_csv']").prop('disabled', true); 
			$("#type_to_export option[value='export_pdf']").prop('disabled', false); 
			$("#type_to_export option[value='export_html']").prop('disabled', false); 			
		} else if ($filter_details_to_export.val() == 'all_details') {
			$("#report_to_export option[value='export_no_details']").prop('disabled', true); 
			$("#report_to_export option[value='export_basic_details']").prop('disabled', true); 
			$("#report_to_export option[value='export_all_details']").prop('disabled', false);	
			$("#report_to_export option[value='export_all_details']").prop('selected', true);
			$("#type_to_export option[value='']").prop('selected', true); 
			$('#csv_delimiter select').prop('disabled', true);
			$("#type_to_export option[value='export_xls']").prop('disabled', false); 
			$("#type_to_export option[value='export_xlsx']").prop('disabled', false); 
			$("#type_to_export option[value='export_csv']").prop('disabled', false); 
			$("#type_to_export option[value='export_pdf']").prop('disabled', true); 
			$("#type_to_export option[value='export_html']").prop('disabled', true); 			
		} else {
			$("#report_to_export option[value='export_no_details']").prop('disabled', false); 
			$("#report_to_export option[value='export_basic_details']").prop('disabled', false); 		
			$("#report_to_export option[value='export_all_details']").prop('disabled', false); 
			$("#type_to_export option[value='']").prop('selected', true); 
			$('#csv_delimiter select').prop('disabled', true);
			$("#type_to_export option[value='export_xls']").prop('disabled', true); 
			$("#type_to_export option[value='export_xlsx']").prop('disabled', true); 
			$("#type_to_export option[value='export_csv']").prop('disabled', true); 
			$("#type_to_export option[value='export_pdf']").prop('disabled', true); 
			$("#type_to_export option[value='export_html']").prop('disabled', true);  			
		}
	}
	$('#report_to_export .select').selectpicker('refresh');
	$('#type_to_export .select').selectpicker('refresh');
	$('#csv_delimiter .select').selectpicker('refresh');	
	
	if ($filter_report_to_export.val() == 'all_products_with_without_orders' || $filter_report_to_export.val() == 'products_without_orders') {
		$("#cron_report_to_export option[value='export_no_details']").prop('disabled', false); 
		$("#cron_report_to_export option[value='export_no_details']").prop('selected', true); 		
		$("#cron_report_to_export option[value='export_basic_details']").prop('disabled', true);		
		$("#cron_report_to_export option[value='export_all_details']").prop('disabled', true); 
		$("#cron_type_to_export option[value='']").prop('selected', true); 
		$('#cron_csv_delimiter select').prop('disabled', true);
		$("#cron_type_to_export option[value='export_xls']").prop('disabled', false); 
		$("#cron_type_to_export option[value='export_xlsx']").prop('disabled', false); 
		$("#cron_type_to_export option[value='export_csv']").prop('disabled', false); 
		$("#cron_type_to_export option[value='export_pdf']").prop('disabled', false); 
		$("#cron_type_to_export option[value='export_html']").prop('disabled', false); 		
	} else {
		if ($filter_details_to_export.val() == 'no_details') {
			$("#cron_report_to_export option[value='export_no_details']").prop('disabled', false); 
			$("#cron_report_to_export option[value='export_no_details']").prop('selected', true); 			
			$("#cron_report_to_export option[value='export_basic_details']").prop('disabled', true); 
			$("#cron_report_to_export option[value='export_all_details']").prop('disabled', true); 
			$("#cron_type_to_export option[value='']").prop('selected', true); 
			$('#cron_csv_delimiter select').prop('disabled', true);
			$("#cron_type_to_export option[value='export_xls']").prop('disabled', false); 
			$("#cron_type_to_export option[value='export_xlsx']").prop('disabled', false); 
			$("#cron_type_to_export option[value='export_csv']").prop('disabled', false); 
			$("#cron_type_to_export option[value='export_pdf']").prop('disabled', false); 
			$("#cron_type_to_export option[value='export_html']").prop('disabled', false); 			
		} else if ($filter_details_to_export.val() == 'basic_details') {
			$("#cron_report_to_export option[value='export_no_details']").prop('disabled', true); 
			$("#cron_report_to_export option[value='export_basic_details']").prop('disabled', false); 
			$("#cron_report_to_export option[value='export_basic_details']").prop('selected', true); 
			$("#cron_report_to_export option[value='export_all_details']").prop('disabled', true);
			$("#cron_type_to_export option[value='']").prop('selected', true); 
			$('#cron_csv_delimiter select').prop('disabled', true);
			$("#cron_type_to_export option[value='export_xls']").prop('disabled', true); 
			$("#cron_type_to_export option[value='export_xlsx']").prop('disabled', true); 
			$("#cron_type_to_export option[value='export_csv']").prop('disabled', true); 
			$("#cron_type_to_export option[value='export_pdf']").prop('disabled', false); 
			$("#cron_type_to_export option[value='export_html']").prop('disabled', false); 			
		} else if ($filter_details_to_export.val() == 'all_details') {
			$("#cron_report_to_export option[value='export_no_details']").prop('disabled', true); 
			$("#cron_report_to_export option[value='export_basic_details']").prop('disabled', true); 
			$("#cron_report_to_export option[value='export_all_details']").prop('disabled', false);	
			$("#cron_report_to_export option[value='export_all_details']").prop('selected', true);
			$("#cron_type_to_export option[value='']").prop('selected', true); 
			$('#cron_csv_delimiter select').prop('disabled', true);
			$("#cron_type_to_export option[value='export_xls']").prop('disabled', false); 
			$("#cron_type_to_export option[value='export_xlsx']").prop('disabled', false); 
			$("#cron_type_to_export option[value='export_csv']").prop('disabled', false); 
			$("#cron_type_to_export option[value='export_pdf']").prop('disabled', true); 
			$("#cron_type_to_export option[value='export_html']").prop('disabled', true); 			
		} else {
			$("#cron_report_to_export option[value='export_no_details']").prop('disabled', false); 
			$("#cron_report_to_export option[value='export_basic_details']").prop('disabled', false); 		
			$("#cron_report_to_export option[value='export_all_details']").prop('disabled', false); 
			$("#cron_type_to_export option[value='']").prop('selected', true); 
			$('#cron_csv_delimiter select').prop('disabled', true);
			$("#cron_type_to_export option[value='export_xls']").prop('disabled', true); 
			$("#cron_type_to_export option[value='export_xlsx']").prop('disabled', true); 
			$("#cron_type_to_export option[value='export_csv']").prop('disabled', true); 
			$("#cron_type_to_export option[value='export_pdf']").prop('disabled', true); 
			$("#cron_type_to_export option[value='export_html']").prop('disabled', true);  			
		}		
	}
	$('#cron_report_to_export .select').selectpicker('refresh');
	$('#cron_type_to_export .select').selectpicker('refresh');
	$('#cron_csv_delimiter .select').selectpicker('refresh');	
	document.getElementById("cron_file_save_path").defaultValue = "report";
	
	$('#cron_export_file').on('change', function() {
		if ($("#send_to_email").is(":selected")) {
			$("#save_path_status").addClass("disabled");
		} else if ($("#save_on_server").is(":selected")) {
			$("#save_path_status").removeClass("disabled");
		}
	});		
});

$('select[name=\'export_type\']').on('change', function() {
	var export_type = $('select[name=\'export_type\']').val();
	if (export_type == 'export_csv') {
		$('#csv_delimiter select').prop('disabled', false);
	} else {
		$('#csv_delimiter select').prop('disabled', true);
	}
	$('#csv_delimiter .select').selectpicker('refresh');
});
$('select[name=\'export_type\']').trigger('change');

$('select[name=\'cron_export_type\']').on('change', function() {
	var cron_export_type = $('select[name=\'cron_export_type\']').val();
	if (cron_export_type == 'export_csv') {
		$('#cron_csv_delimiter select').prop('disabled', false);
	} else {
		$('#cron_csv_delimiter select').prop('disabled', true);
	}
	$('#cron_csv_delimiter .select').selectpicker('refresh');
});
$('select[name=\'cron_export_type\']').trigger('change');
</script> 
<script type="text/javascript">
$(window).load(function() {
	$(".loader").fadeOut("slow");
});
</script>
<?php if ($filter_details == 'basic_details') { ?>  
<script type="text/javascript">
$(".toggle-all").click(function() {
	if ($(this).hasClass("expand")) {
		$(this).removeClass("expand");
		$(".more").show();
		$("#circle").removeClass("fa fa-arrow-circle-down");
		$("#circle").addClass("fa fa-arrow-circle-up");			
	} else {
		$(this).addClass("expand");
		$(".more").hide();
		$("#circle").removeClass("fa fa-arrow-circle-up");
		$("#circle").addClass("fa fa-arrow-circle-down");	
	}
});
</script>
<?php } ?>
<?php if ($initialise) { ?>
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function(){$('#settings').modal('show');},800);
	$('#settings .modal-body').prepend('<div class="alert alert-warning"><i class="fa fa-info"></i> <?php echo $text_initialise_use; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
});
</script>
<?php } ?>
<?php echo $footer; ?>
	
	
	
	
	
	
	
	
	
	
	
	
	
	