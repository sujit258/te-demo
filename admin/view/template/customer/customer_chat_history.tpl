<div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr>
        <td class="text-left"><?php echo $column_date_added; ?></td>
        <td class="text-left">Order ID</td>
        <td class="text-left"><?php echo $column_comment; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php if ($chat_histories) { ?>
      <?php foreach ($chat_histories as $chat_history) { ?>
      <tr>
        <td class="text-left"><?php echo $chat_history['date_added']; ?></td>
        <td class="text-left"><?php echo $chat_history['order_id']; ?></td>
        <td class="text-left"><?php echo $chat_history['comment']; ?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="2"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
