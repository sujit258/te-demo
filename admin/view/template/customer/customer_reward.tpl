<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-left"><?php echo $column_points; ?></td>
        <td class="text-left"><?php echo $column_description; ?></td>
        <td class="text-left">Expiry Date</td>
        <td class="text-right"><?php echo $column_date_added; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php if ($rewards) { ?>
      <?php foreach ($rewards as $reward) { ?>
      <tr>
        <td class="text-left"><?php echo $reward['points']; ?></td>
        <td class="text-left"><?php echo $reward['description']; ?><span class="text-danger"><br/><?php echo $reward['is_expire']; ?></span></td>
        <td class="text-left"><?php if($reward['points'] > 0) { echo $reward['date_valid_till']; } else { echo '-'; } ?></td>
        <td class="text-right"><?php echo $reward['date_added']; ?></td>
      </tr>
      <?php } ?>
      <tr>
        <td class="text-left"><b><?php echo $text_balance," : ",$balance; ?></b></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
