<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <style>
        .btn-category {
            border-radius: 5px;
            width: 50%;
        }
        #product-performance h4 {
            font-weight: bold;
        }
        .navigation-btn {
            color: #fff!important;
            margin-bottom: 10px;
        }
        #product-performance table tbody tr td:nth-child(n+2), #product-performance table thead {
            font-weight: bold;
            color: #000;
        }
        .mb25 {
            margin-bottom: 25px;
        }
        .DataTables_sort_icon {
            margin-top: -15px;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff!important;
            border: none;
            background-color: #585858;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #585858), color-stop(100%, #111));
            /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #585858 0%, #111 100%);
            /* Chrome10+,Safari5.1+ */
            background: -moz-linear-gradient(top, #585858 0%, #111 100%);
            /* FF3.6+ */
            background: -ms-linear-gradient(top, #585858 0%, #111 100%);
            /* IE10+ */
            background: -o-linear-gradient(top, #585858 0%, #111 100%);
            /* Opera 11.10+ */
            background: linear-gradient(to bottom, #585858 0%, #111 100%);
            /* W3C */
        }
        .dataTables_wrapper .dataTables_filter input {
            margin-left: 0.5em;
            height: 26px;
            width: 370px;
        }
        table.dataTable tbody th, table.dataTable tbody td {
            border: 1px solid #ddd;
        }
        .dataTables_wrapper .dataTables_length {
            float: right;
            margin-bottom: 8px;
        }
        .dataTables_wrapper .dataTables_filter {
            float: left;
            text-align: left;
        }
        
        .onoffswitch {
            position: relative;
            width: 90px;
            -webkit-user-select:none;
            -moz-user-select:none;
            -ms-user-select: none;
        }
    
        .onoffswitch-checkbox {
            display: none;
        }
    
        .onoffswitch-label {
            display: block;
            overflow: hidden;
            cursor: pointer;
            border: 2px solid #ccc;
            border-radius: 20px;
        }
        
        .onoffswitch-inner {
            display: block;
            width: 200%;
            margin-left: -100%;
            -moz-transition: margin 0.3s ease-in 0s;
            -webkit-transition: margin 0.3s ease-in 0s;
            -o-transition: margin 0.3s ease-in 0s;
            transition: margin 0.3s ease-in 0s;
        }
    
        .onoffswitch-inner:before, .onoffswitch-inner:after {
            display: block;
            float: left;
            width: 50%;
            height: 30px;
            padding: 0;
            line-height: 30px;
            font-size: 14px;
            color: white;
            font-weight: bold;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }
    
        .onoffswitch-inner:before {
            content: "Daily";
            padding-left: 10px;
            background-color: #f49a25;
            color: #fff;
        }
    
        .onoffswitch-inner:after {
            content: "Weekly";
            padding-right: 10px;
            background-color: #31b0d5;
            color: #fff;
            text-align: right;
        }
    
        .onoffswitch-switch {
            display: block;
            width: 18px;
            margin: 6px;
            background: #FFFFFF;
            border: 2px solid #999999;
            border-radius: 20px;
            position: absolute;
            top: 0;
            bottom: 0;
            right: 56px;
            -moz-transition: all 0.3s ease-in 0s;
            -webkit-transition: all 0.3s ease-in 0s;
            -o-transition: all 0.3s ease-in 0s;
            transition: all 0.3s ease-in 0s; 
        }
    
        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
            margin-left: 0;
        }
    
        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
            right: 0px; 
        }
    </style>
    <div class="container-fluid">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <?php if ($category != '') { ?>
                <div class="col-md-offset-2 col-md-1 pull-right">
                    <div class="onoffswitch">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                        <label class="onoffswitch-label" for="myonoffswitch">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            <?php }?>
        </div>
        <?php if($mapped_categories) { ?>
            <div class="col-md-1 col-xs-12 col-sm-12" style="padding: 0;">
                <?php foreach($mapped_categories AS $mapped_category) { ?>
                    <?php if($mapped_category['name'] == $category) { ?>
                        <a href="<?php echo $mapped_category['href']; ?>" class="btn btn-block navigation-btn" style="background: #f49a25;"><?php echo $mapped_category['name']; ?></a>
                    <?php } else { ?>
                        <a href="<?php echo $mapped_category['href']; ?>" class="btn btn-block btn-info navigation-btn"><?php echo $mapped_category['name']; ?></a>
                    <?php } ?>
                <?php } ?>
            </div>
            <?php $class_name = "col-md-10"; ?>
            <?php $category_page = 1; ?>
        <?php } else { ?>
            <?php $class_name = "col-md-12"; ?>
            <?php $category_page = 0; ?>
        <?php } ?>
        
        <div class="<?php echo $class_name; ?> col-xs-12 col-sm-12" id="product-performance">
            
            <?php if($category && $daily_product_report) { ?>
                <div id="quantityByDayContainer" class="table-responsive mb25">
                    <h4>Summary By Quantity (Daily)</h4>
                    <table class="table table-bordered table-hover" id="quantityByDay">
	                    <thead>
	                        <tr>
                                <td class="center">Product Name</td>
                                <?php foreach($report_date_data AS $date) { ?>
                                    <td class="center" style="text-transform: capitalize;"><?php echo date('j M', strtotime($date)); ?></td>
	                            <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($daily_product_report AS $data) { ?>
                                <tr>
                                    <td class="left"><a href="<?php echo $data['href']; ?>"><?php echo $data['name']; ?></a></td>
                                    <?php foreach($report_date_data AS $date) { ?>
                                        <td class="center"><?php echo $data[$date]['total_units'] ? number_format($data[$date]['total_units']) : '-'; ?></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
	                
                <div id="valueByDayContainer" class="table-responsive mb25">
	                <h4>Summary By Value (Daily)</h4>
	                <table class="table table-bordered table-hover" id="valueByDay">
	                    <thead>
	                        <tr>
	                            <td class="center">Product Name</td>
	                            <?php foreach($report_date_data AS $date) { ?>
	                                <td class="center" style="text-transform: capitalize;"><?php echo date('j M', strtotime($date)); ?></td>
	                            <?php } ?>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php foreach($daily_product_report AS $data) { ?>
	                            <tr>
	                                <td class="left"><a href="<?php echo $data['href']; ?>"><?php echo $data['name']; ?></a></td>
	                                <?php foreach($report_date_data AS $date) { ?>
    	                                <td class="center"><?php echo $data[$date]['total_sale'] ? number_format($data[$date]['total_sale']) : '-'; ?></td>
	                                <?php } ?>
	                            </tr>
                            <?php } ?>
	                    </tbody>
	                </table>
	            </div>
            <?php } ?>
            <div id="quantityByWeekContainer" class="table-responsive mb25">
            <h4><?php echo $text_weekly_units_summary; ?></h4>
            <table class="table table-bordered table-hover" id="quantityByWeek">
                <thead>
                    <tr>
                        <td class="text-center"><?php echo $category_page > 0 ? $column_product_name : $column_category_name; ?></td>
                        <?php foreach($months_data As $month) { ?>
                            <td class="text-center"><?php echo ucfirst(str_replace('_', ' ',$month)); ?></td>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($results AS $result) { ?>
                        <tr>
                            <?php if($category_page > 0) { ?>
                                <td class="text-center"><a href="<?php echo $result['href']; ?>"><?php echo $result['name']; ?></a></td>
                            <?php } else { ?>
                                <td class="text-center"><a class="btn btn-sm btn-primary btn-category" href="<?php echo $result['href']; ?>"><?php echo $result['name']; ?></a></td>
                            <?php } ?>
                            <?php foreach($months_data As $month) { ?>
                                <td class="text-center"><?php echo $result[$month]['weekly_units_ordered'] ? number_format($result[$month]['weekly_units_ordered']) : '-'; ?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
            
            <div id="quantityByMonthContainer" class="table-responsive mb25">
            <h4><?php echo $text_monthly_units_summary; ?></h4>
            <table class="table table-bordered table-hover" id="quantityByMonth">
                <thead>
                    <tr>
                        <td class="text-center"><?php echo $category_page > 0 ? $column_product_name : $column_category_name; ?></td>
                        <?php foreach($months_data As $month) { ?>
                            <td class="text-center"><?php echo ucfirst(str_replace('_', ' ',$month)); ?></td>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($results AS $result) { ?>
                        <tr>
                            <?php if($category_page > 0) { ?>
                                <td class="text-center"><a href="<?php echo $result['href']; ?>"><?php echo $result['name']; ?></a></td>
                            <?php } else { ?>
                                <td class="text-center"><a class="btn btn-sm btn-primary btn-category" href="<?php echo $result['href']; ?>"><?php echo $result['name']; ?></a></td>
                            <?php } ?>
                            <?php foreach($months_data As $month) { ?>
                                <td class="text-center"><?php echo $result[$month]['monthly_units_ordered'] ? number_format($result[$month]['monthly_units_ordered']) : '-'; ?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
            
            <div id="valueByMonthContainer" class="table-responsive mb25">
            <h4><?php echo $text_monthly_sales_summary; ?></h4>
            <table class="table table-bordered table-hover" id="valueByMonth">
                <thead>
                    <tr>
                        <td class="text-center"><?php echo $category_page > 0 ? $column_product_name : $column_category_name; ?></td>
                        <?php foreach($months_data As $month) { ?>
                            <td class="text-center"><?php echo ucfirst(str_replace('_', ' ',$month)); ?></td>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($results AS $result) { ?>
                        <tr>
                            <?php if($category_page > 0) { ?>
                                <td class="text-center"><a href="<?php echo $result['href']; ?>"><?php echo $result['name']; ?></a></td>
                            <?php } else { ?>
                                <td class="text-center"><a class="btn btn-sm btn-primary btn-category" href="<?php echo $result['href']; ?>"><?php echo $result['name']; ?></a></td>
                            <?php } ?>
                            <?php foreach($months_data As $month) { ?>
                                <td class="text-center"><?php echo $result[$month]['monthly_sales'] ? number_format($result[$month]['monthly_sales']) : '-'; ?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function heatmap_table(tableid, nthchild) {
    var i=1,j=0,arr,max,min;
    $('table#' + tableid + ' tbody tr').each(function() {
        arr = $('table#' + tableid + ' tbody tr:nth-child(' + i + ') td:nth-child(n + ' + nthchild + ')').map(function() {
            if($(this).text() == '-') {
                return parseFloat(0);
            } else {
                return parseFloat($(this).text().replace(/,/g, ''));
            }
        }).get();
        max =  Math.max.apply(Math, arr);
        min = Math.min.apply(Math, arr);
        j = 0;
        
        var sort_array = arr.slice();
        sort_array.sort(function(a, b){return a-b});
        var colors = ['#ff2e00', '#ff7800', '#ffbd00', '#fbff00', '#a6ff00', '#42ff00', '#00c80f'];
        
        arr.forEach(function(){
            var td_index = 0;
            td_index = nthchild + j;
            if(arr[j] > 0) {
                $('table#' + tableid + ' tbody tr:nth-child(' + i + ') td:nth-child(' + td_index +')').css("background", colors[sort_array.indexOf(arr[j])]);
            }
            j++;
        });
        i++;
    });
}

$(document).ready(function() {
    heatmap_table('quantityByWeek', 2);
    heatmap_table('quantityByMonth', 2);
    heatmap_table('valueByMonth', 2);
    
    var category = "<?php echo $category; ?>";
    var order = 1;
    $('#quantityByDay, #valueByDay, #quantityByWeek, #quantityByMonth, #valueByMonth').DataTable({
        dom: 'lBfrtip',
        buttons: [
            'csv', 'excel'
        ],
        "pagingType": "full_numbers",
        "pageLength": 100,
        "order": [[ order , "desc" ]]
    });
    
    if(category != '') {
        $("#quantityByWeekContainer, #quantityByMonthContainer, #valueByMonthContainer").hide();
    }
    
    $("#myonoffswitch").change(function() {
        if($('#myonoffswitch:checkbox:checked').length > 0) {
            $("#quantityByWeekContainer, #quantityByMonthContainer, #valueByMonthContainer").hide();
            $("#quantityByDayContainer, #valueByDayContainer").show();
        } else {
            $("#quantityByWeekContainer, #quantityByMonthContainer, #valueByMonthContainer").show();
            $("#quantityByDayContainer, #valueByDayContainer").hide();
        }
        
    });
    
});
</script>
<?php echo $footer; ?>