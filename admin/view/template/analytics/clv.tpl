<?php echo $header; ?>
<style>
        .DataTables_sort_icon {
            margin-top: -15px;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff!important;
            border: none;
            background-color: #585858;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #585858), color-stop(100%, #111));
            /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #585858 0%, #111 100%);
            /* Chrome10+,Safari5.1+ */
            background: -moz-linear-gradient(top, #585858 0%, #111 100%);
            /* FF3.6+ */
            background: -ms-linear-gradient(top, #585858 0%, #111 100%);
            /* IE10+ */
            background: -o-linear-gradient(top, #585858 0%, #111 100%);
            /* Opera 11.10+ */
            background: linear-gradient(to bottom, #585858 0%, #111 100%);
            /* W3C */
        }
        .dataTables_wrapper .dataTables_filter input {
            margin-left: 0.5em;
            height: 26px;
            width: 370px;
        }
        table.dataTable tbody th, table.dataTable tbody td {
            border: 1px solid #ddd;
        }
        .dataTables_wrapper .dataTables_length {
            float: right;
            margin-bottom: 8px;
        }
        .dataTables_wrapper .dataTables_filter {
            float: left;
            text-align: left;
        }
        .filter_date {
            cursor: pointer;
        }
</style>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td colspan="3">Filters</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 25%;">
                            <select name="filter_range" class="form-control" id="filter_range">
                                <option value="custom" <?php if($filter_range == 'custom') { echo 'selected'; } ?>>Custom</option>
                                <option value="3m" <?php if($filter_range == '3m') { echo 'selected'; } ?>>Last 3 Months</option>
                                <option value="6m" <?php if($filter_range == '6m') { echo 'selected'; } ?>>Last 6 Months</option>
                                <option value="12m" <?php if($filter_range == '12m') { echo 'selected'; } ?>>Last 12 Months</option>
                            </select>
                        </td>
                        <td><input type="text" name="filter_from_date" class="form-control date filter_date" value="<?php echo $filter_from_date; ?>" data-date-format="YYYY-MM-DD" /></td>
                        <td><input type="text" name="filter_to_date" class="form-control date filter_date" value="<?php echo $filter_to_date; ?>" data-date-format="YYYY-MM-DD" /></td>
                        <td><a class="btn btn-info" id="button-filter">Filter</a></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-hover" id="clv">
                <thead>
                    <tr><td class="text-center"><?php echo $column_date_added; ?></td>
                        <td class="text-center"><?php echo $column_name; ?></td>
                        <td class="text-center"><?php echo $column_email; ?></td>
                        <td class="text-center"><?php echo $column_telephone; ?></td>
                        <td class="text-center"><?php echo $column_latest_order_date; ?></td>
                        <td class="text-center"><?php echo $column_value; ?></td>
                        <td class="text-center"><?php echo $column_units; ?></td>
                        <td class="text-center"><?php echo $column_order_count; ?></td>
                        <td class="text-center"><?php echo $column_ticket_size; ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($clv_data)) { ?>
                        <?php foreach($clv_data AS $data) { ?>
                            <tr>
                                <td><?php echo $data['date_added']; ?></td>
                                <td><a href="<?php echo $data['href']; ?>"><?php echo $data['name']; ?></a></td>
                                <td><?php echo $data['email']; ?></td>
                                <td class="text-center"><?php echo $data['telephone']; ?></td>
                                <td class="text-center"><?php echo $data['latest_order_date']; ?></td>
                                <td class="text-center"><?php echo $data['order_value']; ?></td>
                                <td class="text-center"><?php echo $data['order_units']; ?></td>
                                <td class="text-center"><?php echo $data['order_count']; ?></td>
                                <td class="text-center"><?php echo $data['ticket_size']; ?></td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="9"><?php echo $text_empty; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#clv').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'csv', 'excel'
            ],
            lengthMenu: [
                [ 50, 100, 200, 500, -1 ],
                [ '50','100','200', '500', 'Show all' ]
            ],
            "pagingType": "full_numbers",
            "order": [[ 5 , "desc" ]]
        });
    });
    $('.date').datetimepicker({
	    pickTime: false,
        maxDate: moment()
    });
    $('#filter_range').change(function() {
        if($(this).val() == 'custom') {
            $('.filter_date').removeAttr('disabled');
        } else {
            $('.filter_date').val('');
            $('.filter_date').attr('disabled', 'disabled');
        }
    });
    $('#button-filter').click(function() {
        url = 'index.php?route=analytics/clv&token=<?php echo $token; ?>';

    	var filter_range = $('select[name=\'filter_range\']').val();
    
    	if (filter_range) {
    		url += '&filter_range=' + encodeURIComponent(filter_range);
    	}
    	
    	if(filter_range == 'custom') {
    	    var filter_from_date = $('input[name=\'filter_from_date\']').val();
    	    var filter_to_date = $('input[name=\'filter_to_date\']').val();
    	    if(filter_from_date.length == 10 && filter_to_date.length == 10) {
    	        url += '&filter_from_date=' + encodeURIComponent(filter_from_date);
    	        url += '&filter_to_date=' + encodeURIComponent(filter_to_date);
    	    } else {
    	        if(filter_from_date.length != '10') {
    	            alert('Please select from date!');
    	        }
        	    if(filter_to_date.length != '10') {
        	        alert('Please select to date!');
        	    }
    	        return false;
    	    }
    	}
    	
    	location = url;
    });
</script>