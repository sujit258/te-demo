<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="mp-content">
	<?php echo $mpblogmenu; ?>
	<div class="mpblog-body">
		<div class="page-header">
			<div class="container-fluid">
				<div class="pull-right">
					<button type="submit" id="button-save" form="form-setting" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				</div>
				<h1><?php echo $heading_title; ?></h1>
				<ul class="breadcrumb">
					<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="container-fluid">
			<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<?php if ($configuration) { ?>
			<div class="alert alert-info"><i class="fa fa-check-circle"></i> <?php echo $configuration; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
					<div class="pull-right">
						<span><?php echo $text_store; ?></span>
						<button type="button" data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle"><span><?php echo $store_name; ?> &nbsp; &nbsp; </span> <i class="fa fa-angle-down"></i></button>
						<ul class="dropdown-menu pull-right">
							<li><a href="index.php?route=mpblog/mpblog&token=<?php echo $token; ?>&store_id=0"><?php echo $text_default; ?></a></li>
							<?php foreach($stores as $store) { ?>
							<li><a href="index.php?route=mpblog/mpblog&token=<?php echo $token; ?>&store_id=<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="panel-body">
					<div class="bs-callout bs-callout-info"> 
						<h4>Module Points Blog</h4> 
						<p><center><strong>M-Blog Version 1.0 .</strong></center> <br/> Add beautiful blog system into your opencart store. Blog Is one of best media for internet user to stay tunes with your site. Blogs are also helpful for your upcoming announcements and keep users excited with upcomming products. One of the feature of blog is that blogs are useful to website <strong>SEO</strong>. Using blogs articles you can promote your website. </p> 
					</div>
					<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-setting" class="form-horizontal">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab-setting" data-toggle="tab"> <i class="fa fa-cog"></i> <?php echo $tab_setting; ?></a></li>
							<li><a href="#tab-mpblogcategory" data-toggle="tab"> <i class="fa fa-list"></i> <?php echo $tab_mpblogcategory; ?></a></li>
							<li><a href="#tab-mpblogpost" data-toggle="tab"> <i class="fa fa-file-text-o"></i> <?php echo $tab_mpblogpost; ?></a></li>
							<li><a href="#tab-mpblogmodule" data-toggle="tab"> <i class="fa fa fa-puzzle-piece fw"></i> <?php echo $tab_mpblogmodule; ?></a></li>
							<li><a href="#tab-mpblogcomments" data-toggle="tab"> <i class="fa fa-comments"></i> <?php echo $tab_mpblogcomments; ?></a></li>
							<li><a href="#tab-mpblogrss" data-toggle="tab"> <i class="fa fa-rss"></i> <?php echo $tab_mpblogrss; ?></a></li>
							<li><a href="#tab-mpsupport" data-toggle="tab"> <i class="fa fa-thumbs-up"></i> <?php echo $tab_mpsupport; ?></a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab-setting">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog_status"><?php echo $entry_status; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog_status" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_status" type="checkbox" <?php if ($mpblog_status) { echo 'checked="checked"'; } ?> />
										<input type="hidden" name="mpblog_status" value="<?php echo $mpblog_status; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog_blog_viewwishlist"><?php echo $text_blog_show_viewwishlist; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog_blog_viewwishlist" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_viewwishlist" type="checkbox" <?php if ($mpblog_blog_viewwishlist) { echo 'checked="checked"'; } ?> />
										<input type="hidden" name="mpblog_blog_viewwishlist" value="<?php echo $mpblog_blog_viewwishlist; ?>">
									</div>
								</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_author"><?php echo $text_blog_show_author; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_author" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_author" type="checkbox" <?php if ($mpblog_blog_author) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_author" value="<?php echo $mpblog_blog_author; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_show_rating"><?php echo $text_blog_show_rating; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_show_rating" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_show_rating" type="checkbox" <?php if ($mpblog_blog_show_rating) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_show_rating" value="<?php echo $mpblog_blog_show_rating; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_allow_rating"><?php echo $text_blog_allow_rating; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_allow_rating" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_allow_rating" type="checkbox" <?php if ($mpblog_blog_allow_rating) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_allow_rating" value="<?php echo $mpblog_blog_allow_rating; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_approve_rating"><?php echo $text_blog_approve_rating; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_approve_rating" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_approve_rating" type="checkbox" <?php if ($mpblog_blog_approve_rating) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_approve_rating" value="<?php echo $mpblog_blog_approve_rating; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_guest_rating"><?php echo $text_blog_guest_rating; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_guest_rating" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_guest_rating" type="checkbox" <?php if ($mpblog_blog_guest_rating) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_guest_rating" value="<?php echo $mpblog_blog_guest_rating; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_show_comment"><?php echo $text_blog_show_comment; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_show_comment" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_show_comment" type="checkbox" <?php if ($mpblog_blog_show_comment) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_show_comment" value="<?php echo $mpblog_blog_show_comment; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_use_comment"><?php echo $text_blog_use_comment; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_use_comment" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_use_comment" type="checkbox" <?php if ($mpblog_blog_use_comment) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_use_comment" value="<?php echo $mpblog_blog_use_comment; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_viewcount"><?php echo $text_blog_show_viewcount; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_viewcount" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_viewcount" type="checkbox" <?php if ($mpblog_blog_viewcount) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_viewcount" value="<?php echo $mpblog_blog_viewcount; ?>">
										</div>
									</div>
									<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_show_tags"><?php echo $text_blog_show_tags; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_show_tags" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_show_tags" type="checkbox" <?php if ($mpblog_blog_show_tags) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_show_tags" value="<?php echo $mpblog_blog_show_tags; ?>">
													</div>
												</div>			
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_date"><?php echo $text_blog_show_date; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_date" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_date" type="checkbox" <?php if ($mpblog_blog_date) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_date" value="<?php echo $mpblog_blog_date; ?>">
										</div>
									</div>
									<div class="form-group mpblog_show_date" style="<?php if ($mpblog_blog_date==0) { echo 'display: none'; } ?>">
										<label class="col-sm-2 control-label" for="input-blog_date_format"><?php echo $text_blog_date_format; ?></label>
										<div class="col-sm-10">
											<input id="input-blog_date_format" type="text" name="mpblog_blog_date_format" value="<?php echo $mpblog_blog_date_format; ?>" placeholder="<?php echo $text_blog_date_format; ?>" class="form-control" />
										</div>
									</div>
								
							</div>
							<div class="tab-pane" id="tab-mpblogcategory">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog_home_category"><span data-toggle="tooltip" title="<?php echo $help_category_home; ?>"><?php echo $text_category_home; ?></span></label>
									<div class="col-sm-10">
										<select class="form-control" name="mpblog_home_category">
											<option value=""><?php echo $text_select; ?></option>
											<?php foreach($mpblog_categories as $mpblog_category) { 
											$sel = '';
											if($mpblog_category['mpblogcategory_id'] == $mpblog_home_category) {
											$sel = 'selected="selected"';
											}
											?>
											<option <?php echo $sel; ?> value="<?php echo $mpblog_category['mpblogcategory_id']; ?>"><?php echo $mpblog_category['name']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog_category_post_count"><?php echo $text_category_post_count; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog_category_post_count" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_category_post_count" type="checkbox" <?php if ($mpblog_category_post_count) { echo 'checked="checked"'; } ?> />
										<input type="hidden" name="mpblog_category_post_count" value="<?php echo $mpblog_category_post_count; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog_category_image"><?php echo $text_category_show_image; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog_category_image" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_category_image" type="checkbox" <?php if ($mpblog_category_image) { echo 'checked="checked"'; } ?> />
										<input type="hidden" name="mpblog_category_image" value="<?php echo $mpblog_category_image; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog_category_description"><?php echo $text_category_show_description; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog_category_description" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_category_description" type="checkbox" <?php if ($mpblog_category_description) { echo 'checked="checked"'; } ?> />
										<input type="hidden" name="mpblog_category_description" value="<?php echo $mpblog_category_description; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-blog_page_limit"><?php echo $text_category_page_limit; ?></label>
									<div class="col-sm-10">
										<input type="text" name="mpblog_category_page_limit" value="<?php echo $mpblog_category_page_limit; ?>" placeholder="<?php echo $text_category_page_limit; ?>" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog_category_view"><?php echo $text_category_view; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog_category_view" class="switch-toggle" data-on-text="<?php echo $text_grid; ?>" data-off-text="<?php echo $text_list; ?>" data-on-value="GRID" data-off-value="LIST" data-target="mpblog_category_view" type="checkbox" <?php if ($mpblog_category_view=='GRID') { echo 'checked="checked"'; } ?> />
										<input type="hidden" name="mpblog_category_view" value="<?php echo $mpblog_category_view; ?>">
									</div>
								</div>
								<fieldset class="mpblog_category_view_grid" <?php if ($mpblog_category_view=='LIST') { echo 'style="display: none;"'; }?>>
									<legend><?php echo $legend_design; ?></legend>
									<div class="bs-callout bs-callout-info"> 
										<h4><?php echo $text_column_title; ?></h4> 
										<p><?php echo $text_column_defination; ?></p>
									</div>
									<div id="mpblog_category_design">
										<div class="blog_design form-group">
										<?php $blog_category_design_row = 0; ?>
										<?php foreach ($mpblog_category_designs as $mpblog_category_design) { ?>
										<div class="clearfix" id="input-mpblog_category_design-<?php echo $blog_category_design_row; ?>">
											<label class="col-sm-2 control-label" for="input-mpblog_category_design-<?php echo $blog_category_design_row; ?>"><?php echo $text_category_design; ?></label>
											<div class="col-sm-10 switch-buttons six-cols">
												<div class="input-group">
												<div class="btns">
													<?php for($i=1;$i<=6;$i++) { 
													$sel = '';
													if($i==5) { continue; }
													if($mpblog_category_design == $i) {
													$sel = 'checked="checked"';
													}
													?>
													<label class="radio-inline">
														<input type="radio" name="mpblog_category_design[<?php echo $blog_category_design_row; ?>]" id="input-mpblog_category_design-<?php echo $blog_category_design_row; ?>" value="<?php echo $i; ?>" <?php echo $sel; ?>  />
	                    			<span class="btn"><?php echo $i .'  ' .$text_column ; ?></span>
													</label>
													<?php } ?>
												</div>
												</div>
											</div>
										</div>
										<?php $blog_category_design_row++; ?>
										<?php } ?>
										</div>
									</div>
								</fieldset>
								<fieldset>
									<legend><?php echo $legend_image_sizes; ?></legend>
									<div class="bs-callout bs-callout-info"> 
										<h4><?php echo $text_cimg_size_title; ?></h4> 
										<p><?php echo $text_cimg_size_defination; ?></p>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-image-category-width"><?php echo $entry_image_category; ?></label>
										<div class="col-sm-10">
											<div class="row">
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_category_width" value="<?php echo $mpblog_image_category_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-image-category-width" class="form-control" />
												</div>
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_category_height" value="<?php echo $mpblog_image_category_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
												</div>
											</div>
											<?php if ($error_image_category) { ?>
											<div class="text-danger"><?php echo $error_image_category; ?></div>
											<?php } ?>
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-image-category-thumb-width"><?php echo $entry_image_category_thumb; ?></label>
										<div class="col-sm-10">
											<div class="row">
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_category_thumb_width" value="<?php echo $mpblog_image_category_thumb_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-image-category-thumb-width" class="form-control" />
												</div>
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_category_thumb_height" value="<?php echo $mpblog_image_category_thumb_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
												</div>
											</div>
											<?php if ($error_image_category_thumb) { ?>
											<div class="text-danger"><?php echo $error_image_category_thumb; ?></div>
											<?php } ?>
										</div>
									</div>
								</fieldset>
							</div>
							<div class="tab-pane" id="tab-mpblogpost">
								<div class="row">
									<div class="col-sm-3">
										<ul class="nav nav-pills nav-stacked ostab" id="mpblogs">
											<li class="active"><a class="text-left" href="#tab-mpblogs-view" data-toggle="tab"><i class="fa fa-comment" aria-hidden="true"></i> <?php echo $tab_mpblogs_view; ?></a></li>
											<li><a class="text-left" href="#tab-mpblogs-listing" data-toggle="tab"><i class="fa fa-comment" aria-hidden="true"></i> <?php echo $tab_mpblogs_listing; ?></a></li>
										</ul>
									</div>  
									<div class="col-sm-9">
										<div class="tab-content">
											<div class="tab-pane" id="tab-mpblogs-listing">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_view"><?php echo $text_blog_view; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_view" class="switch-toggle" data-on-text="<?php echo $text_grid; ?>" data-off-text="<?php echo $text_list; ?>"  data-on-value="GRID" data-off-value="LIST" data-target="mpblog_blog_view" type="checkbox" <?php if ($mpblog_blog_view=='GRID') { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_view" value="<?php echo $mpblog_blog_view; ?>">
													</div>
												</div>
											<fieldset class="mpblog_blog_view_grid" <?php if ($mpblog_blog_view=='LIST') { echo 'style="display: none;"'; }?> >
												<legend><?php echo $legend_design; ?></legend>
												<div class="bs-callout bs-callout-info"> 
													<h4><?php echo $text_column_title; ?></h4> 
													<p><?php echo $text_column_defination; ?></p>
												</div>
												<div id="mpblog_blog_design">
													<div class="blog_design form-group">
													<?php $blog_design_row = 0; ?>
													<?php foreach ($mpblog_blog_designs as $mpblog_blog_design) { ?>
													<div class="clearfix" id="input-mpblog_blog_design-<?php echo $blog_design_row; ?>">
														<label class="col-sm-2 control-label" for="input-mpblog_blog_design-<?php echo $blog_design_row; ?>"><?php echo $text_blog_design; ?></label>
														<div class="col-sm-10 switch-buttons six-cols">
															<div class="input-group">
															<div class="btns">
																<?php for($i=1;$i<=6;$i++) { 
																if($i==5) { continue; }
																$sel = '';
																if($mpblog_blog_design == $i) {
																$sel = 'checked="checked"';
																}
																?>
																<label class="radio-inline">
																 <input type="radio" name="mpblog_blog_design[<?php echo $blog_design_row; ?>]" id="input-mpblog_blog_design-<?php echo $blog_design_row; ?>" value="<?php echo $i; ?>" <?php echo $sel; ?> /> <span class="btn"><?php echo $i .' '. $text_column; ?></span>
																</label>
																<?php } ?>
															</div>
															</div>
														</div>
													</div>
													<?php $blog_design_row++; ?>
													<?php } ?>
													</div>
												</div>
											</fieldset>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_sdescription"><?php echo $text_blog_show_sdescription; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_sdescription" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_sdescription" type="checkbox" <?php if ($mpblog_blog_sdescription) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_sdescription" value="<?php echo $mpblog_blog_sdescription; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-blog_sdescription_length"><?php echo $text_blog_sdescription_length; ?></label>
													<div class="col-sm-10">
														<input type="text" name="mpblog_blog_sdescription_length" value="<?php echo $mpblog_blog_sdescription_length; ?>" placeholder="<?php echo $text_blog_sdescription_length; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-blog_page_limit"><?php echo $text_blog_page_limit; ?></label>
													<div class="col-sm-10">
														<input type="text" name="mpblog_blog_page_limit" value="<?php echo $mpblog_blog_page_limit; ?>" placeholder="<?php echo $text_blog_page_limit; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_show_readmore"><?php echo $text_blog_show_readmore; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_show_readmore" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_show_readmore" type="checkbox" <?php if ($mpblog_blog_show_readmore) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_show_readmore" value="<?php echo $mpblog_blog_show_readmore; ?>">
													</div>
												</div>
											</div>
											<div class="tab-pane active" id="tab-mpblogs-view">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_image"><?php echo $text_blog_show_image; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_image" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_image" type="checkbox" <?php if ($mpblog_blog_image) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_image" value="<?php echo $mpblog_blog_image; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_image_popup"><?php echo $text_blog_show_image_popup; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_image_popup" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_image_popup" type="checkbox" <?php if ($mpblog_blog_image_popup) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_image_popup" value="<?php echo $mpblog_blog_image_popup; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_description"><?php echo $text_blog_show_description; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_description" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_description" type="checkbox" <?php if ($mpblog_blog_description) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_description" value="<?php echo $mpblog_blog_description; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_nextprev"><?php echo $text_blog_show_nextprev; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_nextprev" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_nextprev" type="checkbox" <?php if ($mpblog_blog_nextprev) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_nextprev" value="<?php echo $mpblog_blog_nextprev; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_nextprev_title"><?php echo $text_blog_show_nextprev_title; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_nextprev_title" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_nextprev_title" type="checkbox" <?php if ($mpblog_blog_nextprev_title) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_nextprev_title" value="<?php echo $mpblog_blog_nextprev_title; ?>">
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>	
								<fieldset>
									<legend><?php echo $legend_image_sizes; ?></legend>
									<div class="bs-callout bs-callout-info"> 
										<h4><?php echo $text_img_size_title; ?></h4> 
										<p><?php echo $text_img_size_defination; ?></p>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-image-post-width"><?php echo $entry_image_post; ?></label>
										<div class="col-sm-10">
											<div class="row">
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_width" value="<?php echo $mpblog_image_post_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-image-post-width" class="form-control" />
												</div>
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_height" value="<?php echo $mpblog_image_post_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
												</div>
											</div>
											<?php if ($error_image_post) { ?>
											<div class="text-danger"><?php echo $error_image_post; ?></div>
											<?php } ?>
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-image-post-additional-width"><?php echo $entry_image_post_additional; ?></label>
										<div class="col-sm-10">
											<div class="row">
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_additional_width" value="<?php echo $mpblog_image_post_additional_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-image-post-additional-width" class="form-control" />
												</div>
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_additional_height" value="<?php echo $mpblog_image_post_additional_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
												</div>
											</div>
											<?php if ($error_image_post_additional) { ?>
											<div class="text-danger"><?php echo $error_image_post_additional; ?></div>
											<?php } ?>
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-image-post-popup-width"><?php echo $entry_image_post_popup; ?></label>
										<div class="col-sm-10">
											<div class="row">
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_popup_width" value="<?php echo $mpblog_image_post_popup_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-image-post-popup-width" class="form-control" />
												</div>
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_popup_height" value="<?php echo $mpblog_image_post_popup_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
												</div>
											</div>
											<?php if ($error_image_post_popup) { ?>
											<div class="text-danger"><?php echo $error_image_post_popup; ?></div>
											<?php } ?>
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-image-post-related"><?php echo $entry_image_post_related; ?></label>
										<div class="col-sm-10">
											<div class="row">
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_related_width" value="<?php echo $mpblog_image_post_related_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-image-post-related" class="form-control" />
												</div>
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_related_height" value="<?php echo $mpblog_image_post_related_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
												</div>
											</div>
											<?php if ($error_image_post_related) { ?>
											<div class="text-danger"><?php echo $error_image_post_related; ?></div>
											<?php } ?>
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-image-post-thumb-width"><?php echo $entry_image_post_thumb; ?></label>
										<div class="col-sm-10">
											<div class="row">
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_thumb_width" value="<?php echo $mpblog_image_post_thumb_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-image-post-thumb-width" class="form-control" />
												</div>
												<div class="col-sm-6">
													<input type="text" name="mpblog_image_post_thumb_height" value="<?php echo $mpblog_image_post_thumb_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
												</div>
											</div>
											<?php if ($error_image_post_thumb) { ?>
											<div class="text-danger"><?php echo $error_image_post_thumb; ?></div>
											<?php } ?>
										</div>
									</div>
								</fieldset>		
							</div>
							<div class="tab-pane" id="tab-mpblogmodule">
								<div class="bs-callout bs-callout-info"> 
									<h4><?php echo $text_socialmedia_title; ?></h4> 
									<p><?php echo $text_socialmedia_defination; ?></p>
								</div>
								<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_viewsocial"><?php echo $text_blog_show_viewsocial; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_viewsocial" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_viewsocial" type="checkbox" <?php if ($mpblog_blog_viewsocial) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_viewsocial" value="<?php echo $mpblog_blog_viewsocial; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_sociallocation"><?php echo $text_blog_show_sociallocation; ?></label>
										<div class="col-sm-4 switch-buttons">
											<div class="btns">
											<label class="checkbox-inline">
												<?php if (in_array('TOP', $mpblog_blog_sociallocation)) { ?>
												<input type="checkbox" name="mpblog_blog_sociallocation[]" value="TOP" checked="checked" />
												<span class="btn"><?php echo $text_top; ?></span>
												<?php } else { ?>
												<input type="checkbox" name="mpblog_blog_sociallocation[]" value="TOP" />
												<span class="btn"><?php echo $text_top; ?></span>
												<?php } ?>
											</label>
											<label class="checkbox-inline">
												<?php if (in_array('BOTTOM', $mpblog_blog_sociallocation)) { ?>
												<input type="checkbox" name="mpblog_blog_sociallocation[]" value="BOTTOM" checked="checked" />
												<span class="btn"><?php echo $text_bottom; ?></span>
												<?php } else { ?>
												<input type="checkbox" name="mpblog_blog_sociallocation[]" value="BOTTOM" />
												<span class="btn"><?php echo $text_bottom; ?></span>
												<?php } ?>
											</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-mpblog_blog_sharethis"><?php echo $text_blog_show_sharethis; ?></label>
										<div class="col-sm-10">
											<input id="input-mpblog_blog_sharethis" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_sharethis" type="checkbox" <?php if ($mpblog_blog_sharethis) { echo 'checked="checked"'; } ?> />
											<input type="hidden" name="mpblog_blog_sharethis" value="<?php echo $mpblog_blog_sharethis; ?>">
										</div>
									</div>
								<div class="table-responsive">
									<table id="mpblog-social" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td class="text-left"><?php echo $entry_social_icon; ?></td>
												<td class="text-left"><?php echo $entry_social_href; ?></td>
												<td class="text-left"><?php echo $entry_social_name; ?></td>
												<td class="text-right"><?php echo $entry_sort_order; ?></td>
												<td></td>
											</tr>
										</thead>
										<tbody>
											<?php $mpblog_social_row = 0; ?>
											<?php foreach ($mpblog_socials as $mpblog_social) { ?>
											<tr id="mpblog-social-row<?php echo $mpblog_social_row; ?>">
												<td class="text-left"><input type="text" name="mpblog_social[<?php echo $mpblog_social_row; ?>][icon]" value="<?php echo $mpblog_social['icon']; ?>" placeholder="<?php echo $placeholder_social_icon; ?>" class="form-control" /></td>
												<td class="text-left"><input type="text" name="mpblog_social[<?php echo $mpblog_social_row; ?>][href]" value="<?php echo $mpblog_social['href']; ?>" placeholder="<?php echo $placeholder_social_href; ?>" class="form-control" /></td>
												<td class="text-left">
													<?php foreach ($languages as $language) { ?>
							                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
							                        <input type="text" name="mpblog_social[<?php echo $mpblog_social_row; ?>][name][<?php echo $language['language_id']; ?>]" value="<?php echo isset($mpblog_social['name'][$language['language_id']]) ?  $mpblog_social['name'][$language['language_id']] : ''; ?>" placeholder="<?php echo $placeholder_social_name; ?>" class="form-control" />
							                        </div>
							                        <?php } ?>
							                      </td>
												<td class="text-right"><input type="text" name="mpblog_social[<?php echo $mpblog_social_row; ?>][sort_order]" value="<?php echo $mpblog_social['sort_order']; ?>" placeholder="<?php echo $placeholder_sort_order; ?>" class="form-control" /></td>
												<td class="text-left"><button type="button" onclick="$('#mpblog-social-row<?php echo $mpblog_social_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
											</tr>
											<?php $mpblog_social_row++; ?>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="4"></td>
												<td class="text-left"><button type="button" onclick="addMpBlogSocial();" data-toggle="tooltip" title="<?php echo $button_social_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="tab-mpblogcomments">
								<div class="bs-callout bs-callout-info"> 
									<h4><?php echo $text_comment_title; ?></h4> 
									<p><?php echo $text_comment_defination; ?></p>
								</div>
								<fieldset>
									<legend><?php echo $legend_comment; ?></legend>
								
									
									
								<div class="row">
									<div class="col-sm-3">
										<ul class="nav nav-pills nav-stacked ostab" id="mpblogcomments">
											<li class="active"><a class="text-left" href="#tab-mpblogcomments-default" data-toggle="tab"><i class="fa fa-comment" aria-hidden="true"></i> <?php echo $tab_mpblogcomments_default; ?></a></li>
											<li><a class="text-left" href="#tab-mpblogcomments-facebook" data-toggle="tab"><i class="fa fa-facebook-official" aria-hidden="true"></i> <?php echo $tab_mpblogcomments_facebook; ?></a></li>
											<li><a class="text-left" href="#tab-mpblogcomments-google" data-toggle="tab"><i class="fa fa-google-plus-square" aria-hidden="true"></i> <?php echo $tab_mpblogcomments_google; ?></a></li>
											<li><a class="text-left" href="#tab-mpblogcomments-disqus" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i> <?php echo $tab_mpblogcomments_disqus; ?></a></li>
										</ul>
									</div>  
									<div class="col-sm-9">
										<div class="tab-content">
											<div class="tab-pane active" id="tab-mpblogcomments-default">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_comments_default"><?php echo $text_comment_default; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_comments_default" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_comments_default" type="checkbox" <?php if ($mpblog_comments_default) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_comments_default" value="<?php echo $mpblog_comments_default; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_allow_comment"><?php echo $text_blog_allow_comment; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_allow_comment" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_allow_comment" type="checkbox" <?php if ($mpblog_blog_allow_comment) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_allow_comment" value="<?php echo $mpblog_blog_allow_comment; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_approve_comment"><?php echo $text_blog_approve_comment; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_approve_comment" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_approve_comment" type="checkbox" <?php if ($mpblog_blog_approve_comment) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_approve_comment" value="<?php echo $mpblog_blog_approve_comment; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_blog_captcha_comment"><span data-toggle="tooltip" title="<?php echo $help_blog_captcha_comment; ?>"><?php echo $text_blog_captcha_comment; ?></span></label>
													<div class="col-sm-10">
														<input id="input-mpblog_blog_captcha_comment" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_blog_captcha_comment" type="checkbox" <?php if ($mpblog_blog_captcha_comment) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_blog_captcha_comment" value="<?php echo $mpblog_blog_captcha_comment; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_comments_default_guest"><?php echo $text_comment_default_guest; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_comments_default_guest" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_comments_default_guest" type="checkbox" <?php if ($mpblog_comments_default_guest) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_comments_default_guest" value="<?php echo $mpblog_comments_default_guest; ?>">
													</div>
												</div>

											</div>
											<div class="tab-pane" id="tab-mpblogcomments-facebook">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_comments_facebook"><?php echo $text_comment_facebook; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_comments_facebook" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_comments_facebook" type="checkbox" <?php if ($mpblog_comments_facebook) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_comments_facebook" value="<?php echo $mpblog_comments_facebook; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-facebook_appid"><?php echo $text_facebook_appid; ?></label>
													<div class="col-sm-10">
														<input id="input-facebook_appid" type="text" name="mpblog_facebook_appid" value="<?php echo $mpblog_facebook_appid; ?>" placeholder="<?php echo $text_facebook_appid; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-facebook_nocomment"><?php echo $text_facebook_nocomment; ?></label>
													<div class="col-sm-10">
														<input id="input-facebook_nocomment" type="text" name="mpblog_facebook_nocomment" value="<?php echo $mpblog_facebook_nocomment; ?>" placeholder="<?php echo $text_facebook_nocomment; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-facebook_color"><?php echo $text_facebook_color; ?></label>
													<div class="col-sm-10">
														<select id="input-facebook_color" type="text" name="mpblog_facebook_color" class="form-control">
															<option <?php if($mpblog_facebook_color=='light') { echo 'selected="selected"'; } ?> value="light"><?php echo $text_facebook_colorlight; ?></option>
															<option <?php if($mpblog_facebook_color=='dark') { echo 'selected="selected"'; } ?> value="dark"><?php echo $text_facebook_colordark; ?></option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-facebook_order"><?php echo $text_facebook_order; ?></label>
													<div class="col-sm-10">
														<select id="input-facebook_order" type="text" name="mpblog_facebook_order" class="form-control">
															<option <?php if($mpblog_facebook_order=='social') { echo 'selected="selected"'; } ?> value="social"><?php echo $text_facebook_ordersocial; ?></option>
															<option <?php if($mpblog_facebook_order=='reverse_time') { echo 'selected="selected"'; } ?> value="reverse_time"><?php echo $text_facebook_orderreverse_time; ?></option>
															<option <?php if($mpblog_facebook_order=='time') { echo 'selected="selected"'; } ?> value="time"><?php echo $text_facebook_ordertime; ?></option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-facebook_width"><?php echo $text_facebook_width; ?></label>
													<div class="col-sm-10">
														<input id="input-facebook_width" type="text" name="mpblog_facebook_width" value="<?php echo $mpblog_facebook_width; ?>" placeholder="<?php echo $text_facebook_width; ?>" class="form-control" />
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab-mpblogcomments-google">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_comments_google"><?php echo $text_comment_google; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_comments_google" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_comments_google" type="checkbox" <?php if ($mpblog_comments_google) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_comments_google" value="<?php echo $mpblog_comments_google; ?>">
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab-mpblogcomments-disqus">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog_comments_disqus"><?php echo $text_comment_disqus; ?></label>
													<div class="col-sm-10">
														<input id="input-mpblog_comments_disqus" class="switch-toggle" data-on-text="<?php echo $text_yes; ?>" data-off-text="<?php echo $text_no; ?>" data-target="mpblog_comments_disqus" type="checkbox" <?php if ($mpblog_comments_disqus) { echo 'checked="checked"'; } ?> />
														<input type="hidden" name="mpblog_comments_disqus" value="<?php echo $mpblog_comments_disqus; ?>">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog-comment-disqus-code"><?php echo $text_comment_disqus_code; ?></label>
													<div class="col-sm-10">
														<textarea row="8" id="input-mpblog-comment-disqus-code" type="text" name="mpblog_comment_disqus_code" placeholder="<?php echo $text_comment_disqus_code; ?>" class="form-control"><?php echo $mpblog_comment_disqus_code; ?></textarea>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="input-mpblog-comment-disqus-count"><?php echo $text_comment_disqus_count; ?></label>
													<div class="col-sm-10">
														<textarea row="8" id="input-mpblog-comment-disqus-count" type="text" name="mpblog_comment_disqus_count" placeholder="<?php echo $text_comment_disqus_count; ?>" class="form-control" ><?php echo $mpblog_comment_disqus_count; ?></textarea>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>

								</fieldset>
							</div>
							<div class="tab-pane" id="tab-mpblogrss">
								<div class="bs-callout bs-callout-info"> 
									<h4><?php echo $info_title_rssfeed; ?></h4> 
									<p><?php echo $info_text_rssfeed; ?></p> 
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog-rssfeed_title"><?php echo $text_rssfeed_title; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog-rssfeed_title" placeholder="<?php echo $text_rssfeed_title; ?>" name="mpblog_rssfeed_title" class="form-control" value="<?php echo $mpblog_rssfeed_title; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog-rssfeed_description"><?php echo $text_rssfeed_description; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog-rssfeed_description" placeholder="<?php echo $text_rssfeed_description; ?>" name="mpblog_rssfeed_description" class="form-control" value="<?php echo $mpblog_rssfeed_description; ?>" />
									</div>  
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog-rssfeed_format"><?php echo $text_rssfeed_format; ?></label>
									<div class="col-sm-10">
										<select id="input-mpblog-rssfeed_format" name="mpblog_rssfeed_format" class="form-control">
											<option <?php if($mpblog_rssfeed_format=='Atom') { echo 'selected="selected"'; } ?> value="Atom">Atom</option>
											<option <?php if($mpblog_rssfeed_format=='RSS 2.0') { echo 'selected="selected"'; } ?> value="RSS 2.0">RSS 2.0</option>
											<option <?php if($mpblog_rssfeed_format=='RSS 1.0') { echo 'selected="selected"'; } ?> value="RSS 1.0">RSS 1.0</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog-rssfeed_limit"><?php echo $text_rssfeed_limit; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog-rssfeed_limit" placeholder="<?php echo $text_rssfeed_limit; ?>" name="mpblog_rssfeed_limit" class="form-control" value="<?php echo $mpblog_rssfeed_limit; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog-rssfeed_web_master"><?php echo $text_rssfeed_web_master; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog-rssfeed_web_master" placeholder="<?php echo $text_rssfeed_web_master; ?>" name="mpblog_rssfeed_web_master" class="form-control" value="<?php echo $mpblog_rssfeed_web_master; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="input-mpblog-rssfeed_copy_write"><?php echo $text_rssfeed_copy_write; ?></label>
									<div class="col-sm-10">
										<input id="input-mpblog-rssfeed_copy_write" placeholder="<?php echo $text_rssfeed_copy_write; ?>" name="mpblog_rssfeed_copy_write" class="form-control" value="<?php echo $mpblog_rssfeed_copy_write; ?>" />
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab-mpsupport">
								<fieldset id="module-points">
									<legend class="text-mpsuccess">Module Points</legend>
									<div class="form-group">
										<div class="col-md-12 col-xs-12">
											
											<h4 class="text-mpsuccess text-center"><i class="fa fa-thumbs-up fa-mp" aria-hidden="true"></i> Thanks For Choosing Our Extension</h4>
											 <ul class="list-group list-hover">
												<li class="list-group-item clearfix">Installed Version <span class="badge"><i class="fa fa-gg" aria-hidden="true"></i> V.1.0</span></li>
											</ul>
											<h4 class="text-mpsuccess text-center"><i class="fa fa-phone fa-mp" aria-hidden="true"></i> Please Contact Us In Case Any Issue Or Give Feedback!</h4>
											<ul class="list-group">
												<li class="list-group-item clearfix">support@modulepoints.com <span class="badge"><a href="mailto:support@modulepoints.com?Subject=Request Support: Blog Module &body=Module ID: https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=30958"><i class="fa fa-envelope"></i> Email To Support</a></span></li>
											</ul>
										</div>
									</div>
								</fieldset>
							</div>
						</div>  
					</form>  
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript"><!--

$('input[name="mpblog_blog_date"]').on('change', function() {
	if($(this).val() == 1) {
		$('.mpblog_show_date').slideDown('slow');
	} else {
		$('.mpblog_show_date').slideUp('slow');
	}
});

$('input[name="mpblog_blog_view"]').on('change', function() {
	if($(this).val() == 'GRID') {
		$('.mpblog_blog_view_grid').slideDown('slow');
	} else {
		$('.mpblog_blog_view_grid').slideUp('slow');
	}
});

$('input[name="mpblog_category_view"]').on('change', function() {
	if($(this).val() == 'GRID') {
		$('.mpblog_category_view_grid').slideDown('slow');
	} else {
		$('.mpblog_category_view_grid').slideUp('slow');
	}
});

var mpblog_social_row = <?php echo $mpblog_social_row; ?>;

function addMpBlogSocial() {
	html  = '<tr id="mpblog-social-row' + mpblog_social_row + '">';
	html += '  <td class="text-left"><input type="text" name="mpblog_social[' + mpblog_social_row + '][icon]" value="" placeholder="<?php echo $placeholder_social_icon; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><input type="text" name="mpblog_social[' + mpblog_social_row + '][href]" value="" placeholder="<?php echo $placeholder_social_href; ?>" class="form-control" /></td>';

	html += '<td class="text-left">';
					<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>';
	html += '<input type="text" name="mpblog_social[' + mpblog_social_row + '][name][<?php echo $language['language_id']; ?>]" value="" placeholder="<?php echo $placeholder_social_name; ?>" class="form-control" />';
	html += '</div>';
					<?php } ?>
	html += '</td>';

	html += '  <td class="text-right"><input type="text" name="mpblog_social[' + mpblog_social_row + '][sort_order]" value="" placeholder="<?php echo $placeholder_sort_order; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#mpblog-social-row' + mpblog_social_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#mpblog-social > tbody').append(html);

	mpblog_social_row++;
}


$('.switch-toggle').bootstrapSwitch({
	onSwitchChange : function(event, state) {
		// console.log(this); // DOM element
		// console.log(event); // jQuery event
		// console.log(state); // true | false

		var on_value = $(this).attr('data-on-value') || 1;
		var off_value = $(this).attr('data-off-value') || 0;

		$('input[name="'+ $(this).attr('data-target') +'"]').val(state==true ? on_value : off_value).trigger('click').trigger('change');	
	}
});
//--></script>
</div><?php echo $footer; ?>