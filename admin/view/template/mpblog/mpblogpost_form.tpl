<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="mp-content">
  <?php echo $mpblogmenu; ?>
  <div class="mpblog-body">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-mpblogpost" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo $text_form; ?> <sup> <i class="fa fa-eye"></i> (<?php echo $viewed; ?>)</sup></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-mpblogpost" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
            <li><a href="#tab-links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
            <li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
            <li><a href="#tab-desc" data-toggle="tab"><?php echo $content; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="mpblogpost_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($mpblogpost_description[$language['language_id']]) ? $mpblogpost_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control"/>
                      <?php if (isset($error_name[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sdescription<?php echo $language['language_id']; ?>"><?php echo $entry_sdescription; ?></label>
                    <div class="col-sm-10">
                      <textarea rows="5" name="mpblogpost_description[<?php echo $language['language_id']; ?>][sdescription]" placeholder="<?php echo $entry_sdescription; ?>" id="input-sdescription<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($mpblogpost_description[$language['language_id']]) ? $mpblogpost_description[$language['language_id']]['sdescription'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="mpblogpost_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($mpblogpost_description[$language['language_id']]) ? $mpblogpost_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="mpblogpost_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($mpblogpost_description[$language['language_id']]) ? $mpblogpost_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="mpblogpost_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($mpblogpost_description[$language['language_id']]) ? $mpblogpost_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                    <div class="col-sm-10">
                      <textarea name="mpblogpost_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($mpblogpost_description[$language['language_id']]) ? $mpblogpost_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-tag<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="<?php echo $help_tag; ?>"><?php echo $entry_tag; ?></span></label>
                    <div class="col-sm-10">
                      <input type="text" name="mpblogpost_description[<?php echo $language['language_id']; ?>][tag]" value="<?php echo isset($mpblogpost_description[$language['language_id']]) ? $mpblogpost_description[$language['language_id']]['tag'] : ''; ?>" placeholder="<?php echo $entry_tag; ?>" id="input-tag<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="tab-pane" id="tab-data">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-author"><?php echo $entry_author; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="author" value="<?php echo $author; ?>" placeholder="<?php echo $entry_author; ?>" id="input-author" class="form-control" />

                  <?php if ($error_author) { ?>
                  <div class="text-danger"><?php echo $error_author; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><?php echo $entry_image; ?></label>
                <div class="col-sm-10">
                  <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                  <?php if ($error_keyword) { ?>
                  <div class="text-danger"><?php echo $error_keyword; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-video"><span data-toggle="tooltip" title="<?php echo $help_video; ?>"><?php echo $entry_video; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="video" value="<?php echo $video; ?>" placeholder="<?php echo $entry_video; ?>" id="input-video" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-date-available"><?php echo $entry_date_available; ?></label>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <input type="text" name="date_available" value="<?php echo $date_available; ?>" placeholder="<?php echo $entry_date_available; ?>" data-date-format="YYYY-MM-DD" id="input-date-available" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-posttype"><span data-toggle="tooltip" title="<?php echo $help_posttype; ?>"><?php echo $entry_posttype; ?></span></label>
                <div class="col-sm-10">
                  <select name="posttype" id="input-posttype" class="form-control">
                    
                    <option value="ARTICAL" <?php if($posttype=='ARTICAL') { echo 'selected="selected"'; } ?>><?php echo $text_post_artical; ?></option>
                    <option value="VIDEO" <?php if($posttype=='VIDEO') { echo 'selected="selected"'; } ?>><?php echo $text_post_video; ?></option>
                    <option value="IMAGES" <?php if($posttype=='IMAGES') { echo 'selected="selected"'; } ?>><?php echo $text_post_images; ?></option>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="status" id="input-status" class="form-control">
                    <?php if ($status) { ?>
                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-links">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-mpblogcategory"><span data-toggle="tooltip" title="<?php echo $help_mpblogcategory; ?>"><?php echo $entry_mpblogcategory; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="mpblogcategory" value="" placeholder="<?php echo $entry_mpblogcategory; ?>" id="input-mpblogcategory" class="form-control" />
                  <div id="mpblogpost-mpblogcategory" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($mpblogpost_mpblogcategories as $mpblogpost_mpblogcategory) { ?>
                    <div id="mpblogpost-mpblogcategory<?php echo $mpblogpost_mpblogcategory['mpblogcategory_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $mpblogpost_mpblogcategory['name']; ?>
                      <input type="hidden" name="mpblogpost_mpblogcategory[]" value="<?php echo $mpblogpost_mpblogcategory['mpblogcategory_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $mpblogpost_store)) { ?>
                        <input type="checkbox" name="mpblogpost_store[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="mpblogpost_store[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $mpblogpost_store)) { ?>
                        <input type="checkbox" name="mpblogpost_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="mpblogpost_store[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                  <div id="mpblogpost-related" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($mpblogpost_relateds as $mpblogpost_related) { ?>
                    <div id="mpblogpost-related<?php echo $mpblogpost_related['mpblogpost_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $mpblogpost_related['name']; ?>
                      <input type="hidden" name="mpblogpost_related[]" value="<?php echo $mpblogpost_related['mpblogpost_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-relatedcategory"><span data-toggle="tooltip" title="<?php echo $help_relatedcategory; ?>"><?php echo $entry_relatedcategory; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="relatedcategory" value="" placeholder="<?php echo $entry_relatedcategory; ?>" id="input-relatedcategory" class="form-control" />
                  <div id="mpblogpost-relatedcategory" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($mpblogpost_relatedcategories as $mpblogpost_relatedcategory) { ?>
                    <div id="mpblogpost-relatedcategory<?php echo $mpblogpost_relatedcategory['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $mpblogpost_relatedcategory['name']; ?>
                      <input type="hidden" name="mpblogpost_relatedcategory[]" value="<?php echo $mpblogpost_relatedcategory['category_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-relatedproduct"><span data-toggle="tooltip" title="<?php echo $help_relatedproduct; ?>"><?php echo $entry_relatedproduct; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="relatedproduct" value="" placeholder="<?php echo $entry_relatedproduct; ?>" id="input-relatedproduct" class="form-control" />
                  <div id="mpblogpost-relatedproduct" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($mpblogpost_relatedproducts as $mpblogpost_relatedproduct) { ?>
                    <div id="mpblogpost-relatedproduct<?php echo $mpblogpost_relatedproduct['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $mpblogpost_relatedproduct['name']; ?>
                      <input type="hidden" name="mpblogpost_relatedproduct[]" value="<?php echo $mpblogpost_relatedproduct['product_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
              
              <div class="tab-pane" id="tab-desc">
              <div class="table-responsive">
                <table id="images" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_additional_image; ?></td>
                      <td class="text-right">desc</td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $desc_row = 0; ?>
                    <?php foreach ($mpblogpost_contents as $mpblogpost_content) { ?>
                    <tr id="desc-row<?php echo $desc_row; ?>">
                      <td class="text-left"><a href="" id="desc-image<?php echo $desc_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $mpblogpost_content['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="mpblogpost_content[<?php echo $desc_row; ?>][image]" value="<?php echo $mpblogpost_content['image']; ?>" id="input-image<?php echo $desc_row; ?>" /></td>
                      <!-- <td class="text-right"><input type="text" name="mpblogpost_content[<?php echo $desc_row; ?>][desc]" value="<?php echo $mpblogpost_content['desc']; ?>" placeholder=" " class="form-control" /></td> -->
                      <td class="text-right"><textarea type="textarea" name="mpblogpost_content[<?php echo $desc_row; ?>][desc]" placeholder="description " class="form-control"><?php echo $mpblogpost_content['desc']; ?></textarea></td>
                      <td class="text-left"><button type="button" onclick="$('#desc-row<?php echo $desc_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $desc_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="descImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>

            <div class="tab-pane" id="tab-image">
              <div class="table-responsive">
                <table id="images" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_additional_image; ?></td>
                      <td class="text-right"><?php echo $entry_sort_order; ?></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $image_row = 0; ?>
                    <?php foreach ($mpblogpost_images as $mpblogpost_image) { ?>
                    <tr id="image-row<?php echo $image_row; ?>">
                      <td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $mpblogpost_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="mpblogpost_image[<?php echo $image_row; ?>][image]" value="<?php echo $mpblogpost_image['image']; ?>" id="input-image<?php echo $image_row; ?>" /></td>
                      <td class="text-right"><input type="text" name="mpblogpost_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $mpblogpost_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                      <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $image_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
  <script type="text/javascript"><!--
// Category
$('input[name=\'mpblogcategory\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=mpblog/mpblogcategory/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['mpblogcategory_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'mpblogcategory\']').val('');

		$('#mpblogpost-mpblogcategory' + item['value']).remove();

		$('#mpblogpost-mpblogcategory').append('<div id="mpblogpost-mpblogcategory' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="mpblogpost_mpblogcategory[]" value="' + item['value'] + '" /></div>');
	}
});

$('#mpblogpost-mpblogcategory').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Related
$('input[name=\'related\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=mpblog/mpblogpost/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['mpblogpost_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'related\']').val('');

    $('#mpblogpost-related' + item['value']).remove();

    $('#mpblogpost-related').append('<div id="mpblogpost-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="mpblogpost_related[]" value="' + item['value'] + '" /></div>');
  }
});

$('#mpblogpost-related').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});

// Related Category
$('input[name=\'relatedcategory\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=mpblog/mpblogpost/autocompleteCategory&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'relatedcategory\']').val('');

		$('#mpblogpost-relatedcategory' + item['value']).remove();

		$('#mpblogpost-relatedcategory').append('<div id="mpblogpost-relatedcategory' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="mpblogpost_relatedcategory[]" value="' + item['value'] + '" /></div>');
	}
});

$('#mpblogpost-relatedcategory').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});


// Related Product
$('input[name=\'relatedproduct\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=mpblog/mpblogpost/autocompleteProduct&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'relatedproduct\']').val('');

    $('#mpblogpost-relatedproduct' + item['value']).remove();

    $('#mpblogpost-relatedproduct').append('<div id="mpblogpost-relatedproduct' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="mpblogpost_relatedproduct[]" value="' + item['value'] + '" /></div>');
  }
});

$('#mpblogpost-relatedproduct').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});

//--></script>
 <script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
  html  = '<tr id="image-row' + image_row + '">';
  html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="mpblogpost_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
  html += '  <td class="text-right"><input type="text" name="mpblogpost_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
  html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
  html += '</tr>';

  $('#images tbody').append(html);

  image_row++;
}
//--></script>
<script type="text/javascript">
var desc_row = <?php echo $desc_row; ?>;

function descImage() {
  html  = '<tr id="desc-row' + desc_row + '">';
  html += '  <td class="text-left"><a href="" id="desc-image' + desc_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="mpblogpost_content[' + desc_row + '][image]" value="" id="input-image' + desc_row + '" /></td>';
  html += '  <td class="text-right"><textarea type="text" name="mpblogpost_content[' + desc_row + '][desc]" placeholder="description" class="form-control"><?php echo $mpblogpost_content['thumb']; ?></textarea></td>';
  html += '  <td class="text-left"><button type="button" onclick="$(\'#desc-row' + desc_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
  html += '</tr>';

  $('#tab-desc tbody').append(html);
   desc_row++;
}
</script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.time').datetimepicker({
  pickDate: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});
//--></script>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div></div>
<?php echo $footer; ?>