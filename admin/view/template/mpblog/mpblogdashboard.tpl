<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="mp-content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<?php echo $mpblogmenu; ?>
	<div class="mpblog-body">
		<div class="container-fluid">
			<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<div class="row">
				<div class="col-sm-3">
					<div class="tile">
						<a href="<?php echo $mpblogs; ?>">
						<div class="tile-heading"><?php echo $text_total_blogs; ?> </div>
						<div class="tile-body">
							<h2> <i class="fa fa-file-text-o"></i> <?php echo $total_blogs; ?></h2>
						</div>
						<div class="tile-footer"><?php echo $text_view_blogs; ?></div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="tile">
						<a href="<?php echo $mpcategory; ?>">
						<div class="tile-heading"><?php echo $text_total_categories; ?> </div>
						<div class="tile-body">
							<h2> <i class="fa fa-list"></i> <?php echo $total_categories; ?></h2>
						</div>
						<div class="tile-footer"><?php echo $text_view_categories; ?></div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="tile">
						<a href="<?php echo $mpcomment; ?>">
						<div class="tile-heading"><?php echo $text_total_comments; ?> </div>
						<div class="tile-body">
							<h2> <i class="fa fa-comments"></i> <?php echo $total_comments; ?></h2>
						</div>
						<div class="tile-footer"><?php echo $text_view_comments; ?></div>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="tile">
						<a href="<?php echo $mprating; ?>">
						<div class="tile-heading"><?php echo $text_total_ratings; ?> </div>
						<div class="tile-body">
							<h2> <i class="fa fa-star"></i> <?php echo $total_ratings; ?></h2>
						</div>
						<div class="tile-footer"><?php echo $text_view_ratings; ?></div>
						</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="panel panel-default">
					  <div class="panel-heading">
					    <h3 class="panel-title"><i class="fa fa-pencil-square-o"></i> <?php echo $text_latest_blogs; ?></h3>
					  </div>
					  <div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<td><?php echo $column_image; ?></td>
									<td><?php echo $column_blog; ?></td>
									<?php /*<td><?php echo $column_author; ?></td>*/ ?>
									<td><?php echo $column_published; ?></td>
									<td><?php echo $column_date_available; ?></td>
									<td class="text-right"><?php echo $column_action; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($latest_blogs)) { foreach($latest_blogs as $latest_blog) { ?>
								<tr>
									<td><img src="<?php echo $latest_blog['thumb']; ?>" alt="<?php echo $latest_blog['name']; ?>" /></td>
									<td><a href="<?php echo $latest_blog['href']; ?>"><?php echo $latest_blog['name']; ?></a></td>
									<?php /*<td><?php echo $latest_blog['author']; ?></td>*/ ?>
									<td><?php echo $latest_blog['status']; ?></td>
									<td><?php echo $latest_blog['date_available']; ?></td>
									<td class="text-right"><a href="<?php echo $latest_blog['href']; ?>" data-toggle="tooltip" title="<?php echo $text_edit; ?>" class="btn btn-info" data-original-title="<?php echo $text_edit; ?>"><i class="fa fa-eye"></i></a></td>
								</tr>
								<?php } } else { ?>
								<tr>
									<td colspan="6" align="center"><?php echo $text_no_blogs; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					  </div>					  		
					</div>
					<div class="buttons text-right">
						<a class="btn btn-primary" href="<?php echo $all_blogs; ?>"><?php echo $button_viewall; ?></a>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="panel panel-default">
					  <div class="panel-heading">
					    <h3 class="panel-title"><i class="fa fa-comments"></i> <?php echo $text_latest_comments; ?></h3>
					  </div>
					  <div class="table-responsive">
					    <table class="table">
					      <thead>
					        <tr>
					          <td><?php echo $column_blog; ?></td>
					          <td><?php echo $column_commentby; ?></td>
					          <td><?php echo $column_comment; ?></td>
					          <td><?php echo $column_date_added; ?></td>

					          <td class="text-right"><?php echo $column_action; ?></td>
					        </tr>
					      </thead>
					      <tbody>
					          <?php if(!empty($latest_comments)) { foreach($latest_comments as $latest_comment) { ?>
								<tr>
									<td><?php echo $latest_comment['name']; ?></td>
									<td><a href="<?php echo $latest_comment['href']; ?>"><?php echo $latest_comment['author']; ?></a></td>
									<td><?php echo $latest_comment['text']; ?></td>
									<td><?php echo $latest_comment['date_added']; ?></td>

									<td class="text-right"><a href="<?php echo $latest_comment['href']; ?>" data-toggle="tooltip" title="<?php echo $text_edit; ?>" class="btn btn-info" data-original-title="<?php echo $text_edit; ?>"><i class="fa fa-eye"></i></a></td>
								</tr>
								<?php } } else { ?>
								<tr>
									<td colspan="6"><?php echo $text_no_comments; ?></td>
								</tr>
								<?php } ?>
					      </tbody>
					    </table>
					  </div>
					</div>
					<div class="buttons text-right">
						<a class="btn btn-primary" href="<?php echo $all_comments; ?>"><?php echo $button_viewall; ?></a>
					</div>
				</div>
			</div> <?php /*
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="panel panel-default">
					  <div class="panel-heading">
					    <h3 class="panel-title"><i class="fa fa-star"></i> <?php echo $text_top_rated_blogs; ?></h3>
					  </div>
					  <div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<td><?php echo $column_image; ?></td>
									<td><?php echo $column_blog; ?></td>
									<td><?php echo $column_author; ?></td>
									<td><?php echo $column_published; ?></td>
									<td><?php echo $column_date_available; ?></td>
									<td class="text-right"><?php echo $column_action; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($toprated_blogs)) { foreach($toprated_blogs as $toprated_blog) { ?>
								<tr>
									<td><img src="<?php echo $toprated_blog['thumb']; ?>" alt="<?php echo $toprated_blog['name']; ?>" /></td>
									<td><a href="<?php echo $toprated_blog['href']; ?>"><?php echo $toprated_blog['name']; ?> <sup><i class="fa fa-star"></i><?php echo $toprated_blog['rating']; ?></sup></a></td>
									<td><?php echo $toprated_blog['author']; ?></td>
									<td><?php echo $toprated_blog['status']; ?></td>
									<td><?php echo $toprated_blog['date_available']; ?></td>
									<td class="text-right"><a href="<?php echo $toprated_blog['href']; ?>" data-toggle="tooltip" title="<?php echo $text_edit; ?>" class="btn btn-info" data-original-title="<?php echo $text_edit; ?>"><i class="fa fa-eye"></i></a></td>
								</tr>
								<?php } } else { ?>
								<tr>
									<td colspan="6"><?php echo $text_no_blogs; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					  </div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="panel panel-default">
					  <div class="panel-heading">
					    <h3 class="panel-title"><i class="fa fa-eye"></i> <?php echo $text_top_viewed_blogs; ?></h3>
					  </div>
					  <div class="table-responsive">
					    <table class="table">
					      	<thead>
								<tr>
									<td><?php echo $column_image; ?></td>
									<td><?php echo $column_blog; ?></td>
									<td><?php echo $column_author; ?></td>
									<td><?php echo $column_published; ?></td>
									<td><?php echo $column_date_available; ?></td>
									<td class="text-right"><?php echo $column_action; ?></td>
								</tr>
							</thead>
					      	<tbody>
								<?php if(!empty($mostviewed_blogs)) { foreach($mostviewed_blogs as $mostviewed_blog) { ?>
								<tr>
									<td><img src="<?php echo $mostviewed_blog['thumb']; ?>" alt="<?php echo $mostviewed_blog['name']; ?>" /></td>
									<td><a href="<?php echo $mostviewed_blog['href']; ?>"><?php echo $mostviewed_blog['name']; ?><sup><i class="fa fa-eye"></i><?php echo $mostviewed_blog['viewed']; ?></sup></a></td>
									<td><?php echo $mostviewed_blog['author']; ?></td>
									<td><?php echo $mostviewed_blog['status']; ?></td>
									<td><?php echo $mostviewed_blog['date_available']; ?></td>
									<td class="text-right"><a href="<?php echo $mostviewed_blog['href']; ?>" data-toggle="tooltip" title="<?php echo $text_edit; ?>" class="btn btn-info" data-original-title="<?php echo $text_edit; ?>"><i class="fa fa-eye"></i></a></td>
								</tr>
								<?php } } else { ?>
								<tr>
									<td colspan="6"><?php echo $text_no_blogs; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					  </div>
					</div>
				</div>
			</div> */ ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>
