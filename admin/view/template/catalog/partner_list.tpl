<?php echo $header; ?><?php echo $column_left; ?>


<style>
   .container-fluid {
        padding-left: 70px;
        padding-right: 22px;
    }
    .panel {
        overflow-x:hidden;
    }
    
    #example span{
      display:none;
    }
    
     .table-striped tbody td a:nth-child(3) {
        background-color: #228B22;
    }
      
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
      color: white !important;
      border: none;
      background-color: #585858;
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #585858), color-stop(100%, #111));
      /* Chrome,Safari4+ */
      background: -webkit-linear-gradient(top, #585858 0%, #111 100%);
      /* Chrome10+,Safari5.1+ */
      background: -moz-linear-gradient(top, #585858 0%, #111 100%);
      /* FF3.6+ */
      background: -ms-linear-gradient(top, #585858 0%, #111 100%);
      /* IE10+ */
      background: -o-linear-gradient(top, #585858 0%, #111 100%);
      /* Opera 11.10+ */
      background: linear-gradient(to bottom, #585858 0%, #111 100%);
      /* W3C */
    }
    
    table.dataTable tbody th, table.dataTable tbody td {
    
        border: 1px solid #ddd;
    
    }
    
    .dataTables_wrapper .dataTables_length {
        float: right !important;
        margin-bottom: 8px !important;
    
    }
    .dataTables_wrapper .dataTables_filter {
        float: left !important;
        text-align: left !important;
    }
    tfoot {
        display: table-header-group !important;
    }

    .ui-state-default input{
        width: 100% !important;
    }
    
    
    @media(min-width: 780px) {
       
        .dataTables_wrapper .dataTables_filter input {
            margin-left: 0.5em;
            height: 26px;
            width: 370px;
        }
    }
    
    .pull-right {
       margin: 1%;
    }
    
    input, button, select, textarea {
        color:#000 !important;   
    }
</style>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-partner').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-partner">
          <div >
            <table cellspacing="50" style="border-spacing: 0px 0px;" width="100%" id="leads"class="table table-striped table-bordered" >
               <tfoot>
            <tr> 
                <td></td>
                <th class="text-left"><?php echo $column_name; ?></th>
				<th class="text-left">Email</th>
				<th class="text-left">Mobile</th>
				<th class="text-center"><?php echo $column_city; ?></th>
				<th class="text-center"><?php echo $column_scope; ?></th>
				<td class="text-center"><?php echo $column_request_date; ?></td>
                <td></td>
            </tr>
        </tfoot>
        <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php echo $column_name; ?></td>
				  <td class="text-left">Email</th>
				  <td class="text-left">Mobile</th>
				  <td class="text-center"><?php echo $column_city; ?></td>
				  <td class="text-center"><?php echo $column_scope; ?></td>
				  <td class="text-center"><?php echo $column_request_date; ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($partner_list) { ?>
                <?php foreach ($partner_list as $list) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($list['partner_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $list['partner_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $list['partner_id']; ?>" />
                    <?php } ?>
                    </td>
                  <td class="text-left"><?php echo $list['name']; ?></td>
                  <td class="text-left"><?php echo $list['email']; ?></td>
                  <td class="text-left"><?php echo $list['mobile']; ?></td><!--Here to Change -->
                  <td class="text-center"><?php echo $list['city']; ?></td>
                  <td class="text-center"><?php echo $list['scope']; ?></td>
                  
                   <?php   $date_added = strtotime($list['request_date']);
                            $new_date_added = date('d-M-Y',$date_added); 
                            
                    ?>

                    <td class="center" data-sort="<?php echo $date_added; ?>"> <?php echo $new_date_added; ?></td>
                  <td class="text-right"><a href="<?php echo $list['view']; ?>" data-toggle="tooltip" title="<?php echo "ViEW DETAILS"; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a><span style="display:none"><?php echo $list['phone']; ?></span></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <!--<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>-->
      </div>
    </div>
  </div>
</div>


<script>
  $('#leads tfoot th').each( function () {
        var title = $(this).text();
        var id = $(this).attr("id");
        $(this).html( '<input type="text" id="id-'+id+'" placeholder="Search '+title+'" />' );
    } );
    
    $(document).ready(function() {
        var table = $('#leads').DataTable( {
            dom: 'lBfrtip',
            buttons: [
                'csv', 'excel'
            ],
            language: {
                searchPlaceholder: "Universal Search"
            },
            lengthMenu: [
                [ 50, 100, 200, -1 ],
                [ '50','100','200', 'Show all' ]
            ],
            /* Disable initial sort */
            "aaSorting": [],
            "pagingType":"full_numbers",
            initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;
                      $( 'input', this.footer() ).on( 'keyup change clear', function () {
                                if ( that.search() !== this.value ) {
                                    that
                                        .search( this.value )
                                        .draw();
                                }
                            } );
                    } );
                }
        } );
    
   $('#lead_typeselect').on('change', function(){
       table.search(this.value).draw();   
    });
    
    $('#statusselect').on('change', function(){
       table.search(this.value).draw();   
    });
    
    
} );
</script>
<?php echo $footer; ?>