<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-coupon"><?php echo $entry_coupon; ?></label>
                <input type="text" name="filter_coupon" value="<?php echo $filter_coupon; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-coupon" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-homepage"><?php echo $entry_homepage; ?></label>
                <select name="filter_homepage" id="input-homepage" class="form-control">
                  <?php if ($filter_homepage == '1') { ?>
                    <option value="*"></option>
                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                  <?php } else if ($filter_homepage == '0') { ?>
                    <option value="*"></option>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                    <option value="*" selected="selected"></option>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-sku"><?php echo $entry_sku; ?></label>
                <select name="filter_sku" id="input-sku" class="form-control">
                    <option value=""></option>
                    <?php foreach($sku_list AS $sku) { ?>
                        <?php if ($filter_sku == $sku['option_value_id']) { ?>
                            <option value="<?php echo $sku['option_value_id']; ?>" selected="selected"><?php echo $sku['name']; ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $sku['option_value_id']; ?>"><?php echo $sku['name']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" data-date-format="YYYY-MM-DD" placeholder="<?php echo $entry_date_start; ?>" id="input-date-start" class="form-control date" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" data-date-format="YYYY-MM-DD" placeholder="<?php echo $entry_date_end; ?>" id="input-date-end" class="form-control date" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-price"><?php echo $entry_price; ?></label>
                <input type="text" name="filter_price" value="<?php echo $filter_price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
              </div>
            </div>
            <div class="com-sm-4">
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form action="" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  
                  <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                    
                  <td class="text-right"><?php if ($sort == 'd.price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                    <?php } ?></td>
                    
                  <td class="text-left"><?php echo $column_sku; ?></td>
                  
                  <td class="text-right"><?php if ($sort == 'd.homepage') { ?>
                    <a href="<?php echo $sort_homepage; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_homepage; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_homepage; ?>"><?php echo $column_homepage; ?></a>
                    <?php } ?></td>
                    
                  <td class="text-left"><?php if ($sort == 'd.coupon') { ?>
                    <a href="<?php echo $sort_coupon; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_coupon; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_coupon; ?>"><?php echo $column_coupon; ?></a>
                    <?php } ?></td>
                    
                  <td class="text-left"><?php if ($sort == 'd.date_start') { ?>
                    <a href="<?php echo $sort_date_start; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_start; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_start; ?>"><?php echo $column_date_start; ?></a>
                    <?php } ?></td>
                    
                  <td class="text-left"><?php if ($sort == 'd.date_end') { ?>
                    <a href="<?php echo $sort_date_end; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_end; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_end; ?>"><?php echo $column_date_end; ?></a>
                    <?php } ?></td>
                    
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($deals) { ?>
                <?php foreach ($deals as $deal) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($deal['deal_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $deal['deal_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $deal['deal_id']; ?>" />
                    <?php } ?></td>
                    
                  <td class="text-left"><?php echo $deal['name'],"<br/><b>Discount: </b>",$deal['discount'],"%"; ?></td>
                  <td class="text-right"><?php echo $deal['price']; ?></td>
                  <td class="text-left"><?php echo $deal['sku']; ?></td>
                  <td class="text-left"><?php echo $deal['homepage']; ?></td>
                  <td class="text-left"><?php echo $deal['coupon']; ?></td>
                  <td class="text-left"><?php echo $deal['date_start']; ?></td>
                  <td class="text-left"><?php echo $deal['date_end']; ?></td>
                  <td class="text-right"><a href="<?php echo $deal['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
    <script tye="text/javascript">
    $('#input-name, #input-coupon, #input-price, #input-date-start, #input-date-end').on('keyup', function (e) {
        if (e.keyCode == 13) {
            $("#button-filter").click(); 
        }
    });
  </script>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/deals&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_coupon = $('input[name=\'filter_coupon\']').val();

	if (filter_coupon) {
		url += '&filter_coupon=' + encodeURIComponent(filter_coupon);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}
	
	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}

	var filter_sku = $('select[name=\'filter_sku\']').val();

	if (filter_sku != '') {
		url += '&filter_sku=' + encodeURIComponent(filter_sku);
	}

    var filter_homepage = $('select[name=\'filter_homepage\']').val();

    if (filter_homepage != '') {
        url += '&filter_homepage=' + encodeURIComponent(filter_homepage);
    }

	location = url;
});
</script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});
</script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<script type="text/javascript"><!--
    $('.date').datetimepicker({
	    pickTime: false
    });
</script>

</div>
<?php echo $footer; ?>