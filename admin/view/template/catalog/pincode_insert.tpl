<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-pincode" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="?route=extension/module/pincode&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if (isset($success)) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>Insert Pincode</h3>
		<div class="pull-right">
		
      </div> </div>
      <div class="panel-body">
		<ul class="nav nav-tabs">
            <li class="active"><a href="#tab-insert" data-toggle="tab">Pincode Insert</a></li>
            <li><a href="#tab-upload" data-toggle="tab">Pincode Upload</a></li>
        </ul>
		<div class="tab-content">
			<div class="tab-pane active in" id = "tab-insert">
				<form action="?route=catalog/pincode/insert&token=<?php echo $_GET['token']; ?>" method="post" enctype="multipart/form-data" id="form-pincode">
				  <div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<td class="left">Pincode:</td>
								<td class="left">Services:</td>
								<td class="left">Delivery Options:</td>
								<td class="left"></td>
							</tr>
						</thead>
						<tbody id="module-row">
							<tr id = "row1">
								<td class="left"><input type = "text" name = "data[1][pin]"></td>
								<td class="left">
									<select name = "data[1][service]">
										<option value = "0">Prepaid</option>
										<option value = "1">Cash on Delivery</option>
									</select>
								</td>
								<td class="left">
									<select name = "data[1][delivery]">
										<?php foreach($delivery_time as $delivery){ ?>
											<option value = "<?php echo $delivery['id'] ; ?>"><?php echo $delivery['delivery_time'] ; ?></option>
										<?php } ?>
									</select>
								</td>
								<td class="left">
									<a style = "background: rgba(255, 0, 0, 0) !important;" title = "Remove" onclick="$('#row1').remove();" class="button"><i class="fa fa-minus-circle fa-2x" style="color:red;"></i></a>
									<input type = "hidden" id = "counter" value = "2">
								</td>					  
							</tr>
							<tr id = "last_row">
								<td colspan="3"></td>
								<td class="left"><a  title = "Add Row" class="button" onclick="addrows();" ><i class="fa fa-plus-circle fa-2x" style="color:green;"></i></a>
							</tr> 
						</tbody>
					</table>
				  </div>
				</form>
			</div>
			<div class="tab-pane fade" id = "tab-upload">
				<form action="?route=catalog/pincode/upload&token=<?php echo $_GET['token']; ?>" method="post" enctype="multipart/form-data" id="form_upload">
				  <div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<td class="left">Upload CSV File:</td>
								<td class="center">Services:</td>
								<td class="center">Delivery Option:</td>
								<td class="left"></td>
							</tr>
						</thead>
						<tbody id="module-row">
							<tr>
								<td class="left"><input type="file" name="file_up" id="file" style="float:left;">  <span style="float:right"><a  title="Download" data-toggle="tooltip" class="btn btn-primary" href = "sample.csv" style = "margin-top:-8px;">Download Tamplate File</a></span></td>
								<td align = 'left'>
									<select name = "service">
										<option value = "0">Prepaid</option/>
										<option value = "1">Cash on Delivery</option/>
									</select>
								</td>
								<td class="center">
									<select name = "delivery">
										<?php foreach($delivery_time as $delivery){ ?>
											<option value = "<?php echo $delivery['id'] ; ?>"><?php echo $delivery['delivery_time'] ; ?></option>
										<?php } ?>
									</select>
								</td>
								<td align="center">
									<div><a title = "Upload" style = "color:white;" onclick="$('#form_upload').submit();" class="btn btn-primary">Upload</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				  </div>
				</form>
			</div>
		</div>
      </div>
    </div>
  </div>

<script>
	function addrows(){ 
		var counter = $('#counter').val();
		var setvalue = parseInt(counter)+1;
		$('#counter').val(setvalue);
		row_string='<tr id = "row'+counter+'">';
		row_string+='<td class="left"><input type = "text" name = "data['+counter+'][pin]"></td>';
		row_string+='<td class="left"><select name = "data['+counter+'][service]"><option value = "0">Prepaid</option/><option value = "1">Cash on Delivery</option/></select></td>';
		row_string+='<td class="left"><select name = "data['+counter+'][delivery]"><?php foreach($delivery_time as $delivery){ ?><option value = "<?php echo $delivery["id"]  ?>"><?php echo $delivery["delivery_time"] ?></option><?php } ?></select></td>';
		
		row_string+='<td class="left"><a style = "background: rgba(255, 0, 0, 0) !important;" title = "Remove" onclick="removerow('+counter+');" class="button"><i class="fa fa-minus-circle fa-2x" style="color:red;"></i></td>';
		$('#last_row').before(row_string);
	}
	function removerow(counter){
		$("#row"+counter).remove();
	}
</script>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>