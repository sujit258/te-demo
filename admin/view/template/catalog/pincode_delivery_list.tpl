<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
		<a href="?route=catalog/pincode/Insert_delivery&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Insert" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <a href="?route=extension/module/pincode&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a>
		<button type="button" data-toggle="tooltip" title="Delete" class="btn btn-danger" onclick="$('#form-pincode').submit();"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if (isset($success)) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>Pincode Delivery Options List</h3>
      </div>
      <div class="panel-body">
        <form action="?route=catalog/pincode/delivery_delete&token=<?php echo $_GET['token']; ?>" method="post" enctype="multipart/form-data" id="form-pincode">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
						<td>Delivery Option</td>
						<td class="text-right">Action</td>
					<tr>
				</thead>
				<tbody>
					<?php foreach($deliveries as $delivery){ ?>
						<tr>
							<td><input type="checkbox" name="selected[]" value="<?php echo $delivery['id']; ?>" /></td>
							<td><?php echo $delivery['delivery_time']; ?></td>
							<td class="text-right"><a href="?route=catalog/pincode/delivery_edit&token=<?php echo $_GET['token']; ?>&delivery_id=<?php echo $delivery['id']; ?>" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		  </div>
		</form>
		<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>