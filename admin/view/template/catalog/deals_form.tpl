<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_permission) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_permission; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } else if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <div class="tab-content">
                <div class="tab-pane active">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name,$text_help; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                      <input type="hidden" name="product_id" value="<?php echo $product_id ? $product_id : 0; ?>" class="form-control" />
                      <input type="hidden" name="option_id" value="<?php echo $option_id; ?>" />
                      <input type="hidden" name="option_value_id" value="<?php echo $option_value_id; ?>" />
                      <?php if ($error_product) { ?>
                            <div class="text-danger"><?php echo $error_product; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-discount"><?php echo $entry_discount; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="discount" value="<?php echo $discount; ?>" placeholder="<?php echo $entry_discount; ?>" id="input-discount" class="form-control" onchange="updatePrice(this.value);" />
                      <?php if ($error_discount) { ?>
                            <div class="text-danger"><?php echo $error_discount; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div id="option">
                    <?php if($options) { ?>
                        <fieldset>
                            <legend>Options</legend>
                            <?php foreach($options AS $option) { ?>
				                <?php if ($option['type'] == 'select') { ?>
					                <div class="form-group required">
					                    <label class="col-sm-2 control-label" for="input-option<?php echo $option['option_id']; ?>"><?php echo $option['name']; ?></label>
					                    <div class="col-sm-10">
					                        <select name="option['<?php echo $option['option_id']; ?>']" id="input-option<?php echo $option['option_id']; ?>" class="form-control" onchange="updateOptionData('<?php echo $option['option_id']; ?>',this.value);">
					                            <option value="">Choose</option>
                                                <?php foreach($option['product_option_value'] AS $option_value) { ?>
                                                    <?php if($option_value['option_value_id'] == $option_value_id) { ?>
						                                <option value="<?php echo $option_value['option_value_id']; ?>" data-price="<?php echo $option_value['price']; ?>" selected><?php echo $option_value['name']; ?></option>
						                            <?php } else { ?>
						                                <option value="<?php echo $option_value['option_value_id']; ?>" data-price="<?php echo $option_value['price']; ?>" ><?php echo $option_value['name']; ?></option>
						                            <?php } ?>
						                        <?php } ?>
					                        </select>
					                    </div>
					                </div>
				                <?php } ?>
				                
				                <?php if ($option['type'] == 'radio') { ?>
					                <div class="form-group required">
					                    <label class="col-sm-2 control-label" for="input-option<?php echo $option['option_id']; ?>"><?php echo $option['name']; ?></label>
					                    <div class="col-sm-10">
					                        <select name="option['<?php echo $option['option_id']; ?>']" id="input-option<?php echo $option['option_id']; ?>" class="form-control" onchange="updateOptionData('<?php echo $option['option_id']; ?>',this.value);">
					                            <option value="">Choose</option>
                                                <?php foreach($option['product_option_value'] AS $option_value) { ?>
                                                    <?php if($option_value['option_value_id'] == $option_value_id) { ?>
						                                <option value="<?php echo $option_value['option_value_id']; ?>" data-price="<?php echo $option_value['price']; ?>" selected><?php echo $option_value['name']; ?></option>
						                            <?php } else { ?>
						                                <option value="<?php echo $option_value['option_value_id']; ?>" data-price="<?php echo $option_value['price']; ?>"><?php echo $option_value['name']; ?></option>
						                            <?php } ?>
						                        <?php } ?>
					                        </select>
					                    </div>
					                </div>
				                <?php } ?>
                            <?php } ?>
			            </fieldset>
			            <input type="hidden" name="has_option" value="1" />
                    <?php } ?>
                    <?php if ($error_option) { ?>
                        <div class="text-danger"><?php echo $error_option; ?></div>
                     <?php } ?>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="price" value="<?php echo $price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" readonly />
                      <input type="hidden" name="product_mrp" id="product-mrp" value="<?php echo $product_mrp ? $product_mrp : 0; ?> " />
                      <?php if ($error_price) { ?>
                        <div class="text-danger"><?php echo $error_price; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-coupon"><?php echo $entry_coupon,$text_help; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="coupon" value="<?php echo $coupon; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-coupon" class="form-control" />
                      <input type="hidden" name="coupon_id" value="<?php echo $coupon_id ? $coupon_id : 0; ?>" />
                      <?php if ($error_coupon) { ?>
                        <div class="text-danger"><?php echo $error_coupon; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-homepage"><?php echo $entry_homepage; ?></label>
                    <div class="col-sm-10">
                      <select name="homepage" id="input-homepage" class="form-control">
                          <?php if($homepage == '1') { ?>
                            <option value=""></option>
                            <option value="1" selected><?php echo $text_yes; ?></option>
                            <option value="0"><?php echo $text_no; ?></option>
                          <?php } else if ($homepage == '0') { ?>
                            <option value=""></option>
                            <option value="1"><?php echo $text_yes; ?></option>
                            <option value="0" selected><?php echo $text_no; ?></option>
                          <?php } else { ?>
                            <option value="" selected></option>
                            <option value="1"><?php echo $text_yes; ?></option>
                            <option value="0"><?php echo $text_no; ?></option>
                          <?php } ?>
                      </select>
                      <?php if ($error_homepage) { ?>
                        <div class="text-danger"><?php echo $error_homepage; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="date_start" value="<?php echo $date_start; ?>" data-date-format="YYYY-MM-DD" placeholder="<?php echo $entry_date_start; ?>" id="input-date-start" class="form-control date" onchange="setDateEnd();" />
                      <?php if ($error_date_start) { ?>
                        <div class="text-danger"><?php echo $error_date_start; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="date_end" value="<?php echo $date_end; ?>" data-date-format="YYYY-MM-DD" placeholder="<?php echo $entry_date_end; ?>" id="input-date-end" class="form-control" readonly />
                      <?php if ($error_date_end) { ?>
                        <div class="text-danger"><?php echo $error_date_end; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
$('input[name=\'name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id'],
						option: item['option'],
						mrp: item['mrp']
					}
				}));
			}
		});
	},
	'select': function(item) {
	    $('input[name=\'option_id\']').val('');
	    $('input[name=\'option_value_id\']').val('');
		$('input[name=\'name\']').val(item['label']);
		$('input[name=\'product_id\']').val(item['value']);
		$('input[name=\'price\']').val(Math.round(item['mrp']));
		$("#product-mrp").val(Math.round(item['mrp']));

		if (item['option'] != '') {
 			html  = '<fieldset>';
            html += '  <legend>Options</legend>';

			for (i = 0; i < item['option'].length; i++) {
				option = item['option'][i];

				if (option['type'] == 'select') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['option_id'] + ']" id="input-option' + option['option_id'] + '" class="form-control" onchange="updateOptionData(' + option['option_id'] + ',this.value);">';
					html += '      <option value="">Choose</option>';

					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];

						html += '<option value="' + option_value['option_value_id'] + '" data-price="' + parseInt(option_value['price'].replace(/[^\d.]/g, '')) + '">' + option_value['name'];

						html += '</option>';
					}

					html += '    </select>';
					html += '  </div>';
					html += '</div>';
				}

				if (option['type'] == 'radio') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['option_id'] + ']" id="input-option' + option['option_id'] + '" class="form-control" onchange="updateOptionData(' + option['option_id'] + ',this.value);">';
					html += '      <option value="">Choose</option>';

					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];

						html += '<option value="' + option_value['option_value_id'] + '" data-price="' + parseInt(option_value['mrp'].replace(/[^\d.]/g, '')) + '">' + option_value['name'];
						html += '</option>';
					}

					html += '    </select>';
					html += '  </div>';
					html += '</div>';
				}
			}

			html += '</fieldset>';
			html += '<input type="hidden" name="has_option" value="1" />';

			$('#option').html(html);
		} else {
			$('#option').html('');
		}
	}
});

function updateOptionData(option_id, option_value_id) {
    if(option_id > 0) {
        $('input[name=\'option_id\']').val(option_id);
    } else {
        $('input[name=\'option_id\']').val('');
    }
    
    if(option_value_id > 0) {
        $('input[name=\'option_value_id\']').val(option_value_id);
    } else {
         $('input[name=\'option_value_id\']').val('');
    }
    
    var discount = $("#input-discount").val();
    
    var mrp = $("#option select").find(':selected').data("price");
    var price = mrp - (mrp * discount / 100);
    $("#input-price").val(Math.round(price));
}

function updatePrice(discount) {
    var mrp = $("#product-mrp").val();
    var price = mrp - (mrp * discount / 100);
    $("#input-price").val(Math.round(price));
}

</script>
<script type="text/javascript"><!--
$('a:first').tab('show');
</script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

function setDateEnd() {
    var date_start = new Date($('#input-date-start').val());
    date_start.setDate(date_start.getDate()+7);
    var day = date_start.getDate();
    var month = date_start.getMonth() + 1;
    var year = date_start.getFullYear();
    if(day < 10) {
        day = "0" + day;
    }
    if(month < 10) {
        month = "0" + month;
    }
    $('#input-date-end').val(year + "-" + month + "-" + day);
}
</script>
<script>
$('input[name=\'coupon\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=marketing/coupon/autocomplete&token=<?php echo $token; ?>&filter_code=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['code'],
						value: item['coupon_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
	    $('input[name=\'coupon_id\']').val(item['value']);
	    $('input[name=\'coupon\']').val(item['label']);
	}
});
</script>
<?php echo $footer; ?>