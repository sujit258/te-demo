<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-pincode" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="?route=catalog/pincode/getlist&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if (isset($success)) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>Edit Pincode</h3>
      </div>
      <div class="panel-body">
        <form action="?route=catalog/pincode/update&token=<?php echo $_GET['token']; ?>" method="post" enctype="multipart/form-data" id="form-pincode">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="left">Pincode:</td>
						<td class="left">Services:</td>
						<td class="left">Delivery Options:</td>
					</tr>
				</thead>
				<tbody id="module-row">
					<tr id = "row1">
						<td class="left"><input type = "text" name = "pincode" value = "<?php echo $pincode['pincode'];?>"></td>
						<td class="left">
							<select name = "service">
								<option value = "0" <?php if($pincode['service_available'] == 0){ echo "selected"; }?>>Prepaid</option>
								<option value = "1" <?php if($pincode['service_available'] == 1){ echo "selected"; }?>>Cash on Delivery</option>
							</select>
						</td>
						<td class="left">
							<select name = "delivery">
								<?php foreach($delivery_time as $delivery){ ?>
									<option  value = "<?php echo $delivery['id'] ; ?>" <?php if($pincode['delivery_option'] == $delivery['id']){ echo "selected"; } ?> ><?php echo $delivery['delivery_time'] ; ?></option>
								<?php } ?>
							</select>
							<input type = "hidden" name = "pincode_id" value = "<?php echo $_GET['pincode_id']; ?>">
						</td>
					</tr>
				</tbody>
			</table>
		  </div>
		</form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>