<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-delivery" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="?route=catalog/pincode/delivery_getlist&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if (isset($success)) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>Edit Delivery Option</h3>
      </div>
      <div class="panel-body">
        <form action="?route=catalog/pincode/delivery_update&token=<?php echo $_GET['token']; ?>" method="post" enctype="multipart/form-data" id="form-delivery">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="left">Delivery Options:</td>
					</tr>
				</thead>
				<tbody id="module-row">
					<tr id = "row1">
						<td class="left">
							<input type = "text" name = "delivery" value = "<?php echo $delivery['delivery_time']; ?>">
							<input type = "hidden" name = "delivery_id" value = "<?php echo $delivery['id']; ?>">
						</td>
					</tr>
				</tbody>
			</table>
		  </div>
		</form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>