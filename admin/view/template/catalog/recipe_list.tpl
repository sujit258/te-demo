<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $insert; ?>" data-toggle="tooltip" title="<?php echo "Add - Recipies"; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo "Delete_Recipies"; ?>" class="btn btn-danger" onclick="confirm('<?php echo "Are You Sure..?" ?>') ? $('#form-recipe').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
	 
      <div class="panel-body">
          <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-title">Recipe Name:</label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="input-title" class="form-control" />
              </div>			  
			               
            </div>			
			<div class="col-sm-4">
				<!--<label class="control-label" for="input-category"><?php echo $filter_category_name; ?></label>	    
				<select style = "width:316px;" name="filter_category" id="input-category" class="form-control">	
				<option value="*" ><?php echo $filter_category_placeholder; ?></option>
				<?php foreach($category_list as $result) { ?>					
				<option value = "<?php $result['category_id'] ?>" disabled><?php echo $result['name']; ?></option>				
				<?php } ?>
				<!-- keep this code in comment for future work<?php if(!is_null($filter_category)) { ?>
				<option value="<?php echo $filter_category; ?>" selected="selected"><?php echo $category_name; ?></option>
				<?php } ?> 
                </select> -->
                 <div class="form-group">
                <label class="control-label" for="input-status">Status</label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"><?php echo "Select Status"; ?></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_status && !is_null($filter_status)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
				
			</div>
		<div class="col-sm-4"><br>
            <label class="control-label" for="input-category"><?php echo $filter_category_name; ?></label>    
            <select style = "width:316px;" name="filter_category" id="input-category" class="form-control">
                    <option value="*" ><?php echo $filter_category_placeholder; ?></option>
                    <?php if($filter_category) { ?>
                    <?php foreach($category_list as $result) { ?>
                    <?php if($filter_category == $result['category_id']) { ?>
                    <option value = "<?php echo $result['category_id'] ?>" selected="selected"><?php echo $result['name']; ?></option>
                    <?php } else { ?>
                    <option value = "<?php echo $result['category_id'] ?>" ><?php echo $result['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                    <?php } else { ?>
                    <?php foreach($category_list as $result) { ?>
                    <option value = "<?php echo $result['category_id'] ?>" ><?php echo $result['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
               </select><br><br><br>
            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
        </div>

		
			
		</div>
		</div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-recipe">	
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[titl  e*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'id.title') { ?>
                    <a href="<?php echo $sort_by_title; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_title; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_by_title; ?>"><?php echo $column_title; ?></a>
                    <?php } ?></td>
					<td style="color:#3366ff" class="text-center">Category</td>					
                  <td class="text-right"><?php if ($sort == 'i.sort_order') { ?>
                    <a href="<?php echo $sort_by_sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_by_sort_order; ?>"><?php echo $column_sort_order; ?></a>
                    <?php } ?></td>
                    <td><?php if ($sort == 'i.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>">Status</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>">Status</a>
                    <?php } ?></td>
                  <td class="text-right" style = "color:#3366ff"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($recipies) { ?>
                <?php foreach ($recipies as $recipe) { ?>
                <tr>
                  <td class="text-center"><?php if(in_array($recipe['recipe_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $recipe['recipe_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $recipe['recipe_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $recipe['title']; ?></td>
				  <td class="text-center" ><?php echo $recipe['category']; ?></td>
                  <td class="text-right"><?php echo $recipe['sort_order']; ?></td>
                  <td class="text-left"><?php echo $recipe['status']; ?></td>
                  <td class="text-right"><a href="<?php echo $recipe['edit']; ?>" data-toggle="tooltip" title="<?php echo "Edit Recipie"; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div> 
	  
    </div>
  </div>
</div>

<script tye="text/javascript">
    $('#input-title').on('keyup', function (e) {
        if (e.keyCode == 13) {
            $("#button-filter").click(); 
        }
    });
  </script>
  
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/recipe&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
	
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	var filter_category = $('select[name=\'filter_category\']').val();
	if (filter_category != '*') {
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}
	
	
	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/recipe/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['title'],
						value: item['recipe_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

//--></script>

<?php echo $footer; ?>