<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <style>
    .test_button {
        margin-top: 10px;
    }
</style>
  <div class="container-fluid">
      <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-pencil"></i> Form</h3>
        </div>
      </div>
      <div class="panel-body">
        <form class="form-horizontal">
          <fieldset>
            <legend>Product</legend>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-product">Product Name</label>
              <div class="col-sm-10">
                    <input type="text" name="product" value="" id="input-product" class="form-control" />
                    <input type="hidden" name="product_id" value="" />
                    <input type="hidden" name="option_id" value="" />
                    <input type="hidden" name="option_value_id" value="" />
              </div>
            </div>
            <div id="option"></div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-coupon">Coupon</label>
                <div class="col-sm-10">
                    <input type="text" name="coupon" value="" id="input-coupon" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                <div class="text-center">
                    <button type="button" id="button-generate" data-loading-text="Generating.." class="btn btn-primary" onclick="generateLink();"><i class="fa fa-plus-circle"></i> Generate Link</button>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-link">Product Coupon Link</label>
                <div class="col-sm-10">
                    <input type="text" name="link" value="" id="input-link" class="form-control" />
                </div>
            </div>
          </fieldset>
        </form>
      </div>
  </div>
<script type="text/javascript">
$('input[name=\'product\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id'],
						option: item['option']
					}
				}));
			}
		});
	},
	'select': function(item) {
	    $('input[name=\'link\']').val('');
	    $('.test_button').remove();
	    $('input[name=\'option_id\']').val('');
	    $('input[name=\'option_value_id\']').val('');
		$('input[name=\'product\']').val(item['label']);
		$('input[name=\'product_id\']').val(item['value']);

		if (item['option'] != '') {
 			html  = '<fieldset>';
            html += '  <legend>Options</legend>';

			for (i = 0; i < item['option'].length; i++) {
				option = item['option'][i];

				if (option['type'] == 'select') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['option_id'] + ']" id="input-option' + option['option_id'] + '" class="form-control" onchange="updateOptionData(' + option['option_id'] + ',this.value);">';
					html += '      <option value="">Choose</option>';

					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];

						html += '<option value="' + option_value['option_value_id'] + '">' + option_value['name'];

						html += '</option>';
					}

					html += '    </select>';
					html += '  </div>';
					html += '</div>';
				}

				if (option['type'] == 'radio') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['option_id'] + ']" id="input-option' + option['option_id'] + '" class="form-control" onchange="updateOptionData(' + option['option_id'] + ',this.value);">';
					html += '      <option value="">Choose</option>';

					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];

						html += '<option value="' + option_value['option_value_id'] + '">' + option_value['name'];
						html += '</option>';
					}

					html += '    </select>';
					html += '  </div>';
					html += '</div>';
				}
			}

			html += '</fieldset>';

			$('#option').html(html);
		} else {
			$('#option').html('');
		}
	}
});

function updateOptionData(option_id, option_value_id) {
    if(option_id > 0) {
        $('input[name=\'option_id\']').val(option_id);
    } else {
        $('input[name=\'option_id\']').val('');
    }
    
    if(option_value_id > 0) {
        $('input[name=\'option_value_id\']').val(option_value_id);
    } else {
         $('input[name=\'option_value_id\']').val('');
    }
}

function generateLink() {
    var option_id = $('input[name=\'option_id\']').val();
    var option_value_id = $('input[name=\'option_value_id\']').val();
    var product_id = $('input[name=\'product_id\']').val();
    var coupon = $('input[name=\'coupon\']').val();
    var link = '';
    
    if(product_id > 0) {
        link += "https://www.true-elements.com/index.php?route=checkout/cart&product_id=" + product_id;
        
        if(option_id > 0 && option_value_id > 0) {
            link += "&option_id="+option_id+"&option_value_id="+option_value_id;
        }
    
        if(coupon != "") {
            link += "&coupon="+coupon;
        }
    }
    
    if(link == '') {
        alert("Please select product.");
    } else {
        $('input[name=\'link\']').val(link);
        $('.test_button').remove();
        $('input[name=\'link\']').after("<a class='btn btn-success test_button' href='" + link + "' target='_blank'>Test Link</a>");
    }
}
</script>
<?php echo $footer; ?>