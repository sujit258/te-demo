<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
		<a href="?route=catalog/pincode/insert&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Insert" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <a href="<?php echo $csv_link;?>" data-toggle="tooltip" title="Download All Pincodes " class="btn btn-primary"><i class="fa fa-csv"></i></a>
		<a href="?route=extension/module/pincode&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a>
		<button type="button" data-toggle="tooltip" title="Delete Selected Pincode" class="btn btn-danger" onclick="confirm('Are you sure do you want to perform this action') ? $('#form-pincode').submit() : false;"">Delete Selected Pincode</button>
		
	 </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if (isset($success)) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>Pincode List</h3>
      
	  <div class="pull-right">
	  <a href="?route=catalog/pincode/deleteallpincodes&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Delete All Pincodes" class="btn btn-danger" id = "de" style = "margin-top:-8px;" onclick=" return confirm('Are you sure you want to delete all pincodes');">Delete All Pincodes</i></a>
      </div> </div>
	  <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-pincode">Pincode</label>
                <input type="text" name="filter_pincode" value="<?php if(isset($_GET['filter_pincode'])){echo $_GET['filter_pincode'];} ?>" placeholder="Pincode" id="input-pincode" class="form-control" />
              </div>
			</div>
			<div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-service">Service</label>
                <select name="filter_service" class="form-control">
					<option value = "">Select Service</option>
					<option value = "0" <?php if(isset($_GET['filter_service']) && $_GET['filter_service'] == 0){echo "SELECTED";} ?>>Prepaid</option>
					<option value = "1" <?php if(isset($_GET['filter_service']) && $_GET['filter_service'] == 1){echo "SELECTED";} ?>>Cash on Delivery</option>
				</select>
			  </div>
			</div>
			<div class="col-sm-4">
			  <div class="form-group">
                <label class="control-label" for="input-delivery">Delivery Options</label>
                <select name="filter_delivery" class="form-control">
					<option value = "">Delivery Options</option>
					<?php foreach($delivery_time as $delivery){ ?>
						<option value = "<?php echo $delivery['id'] ?>"<?php if(isset($_GET['filter_delivery']) && $_GET['filter_delivery'] == $delivery['id']){echo "SELECTED";} ?>><?php echo $delivery['delivery_time'] ?></option>
					<?php } ?>
				</select>
			  </div>
			  <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i>Filter</button>
            </div>
           </div>
        </div>
		<form action="?route=catalog/pincode/delete&token=<?php echo $_GET['token']; ?>" method="post" enctype="multipart/form-data" id="form-pincode">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
						<td>Pincode</td>
						<td>Prepaid</td>
						<td>Cash on Delivery</td>
						<td>Delivery Option</td>
						<td class="text-right">Action</td>
					<tr>
				</thead>
				<tbody>
					<?php foreach($pincodes as $pincode){ ?>
						<tr>
							<td><input type="checkbox" name="selected[]" value="<?php echo $pincode['id']; ?>" /></td>
							<td><?php echo $pincode['pincode']; ?></td>
							<?php if($pincode['service_available'] == 0){ ?>
								<td>Yes</td><td>No</td>
							<?php } elseif($pincode['service_available'] == 1){ ?>
								<td>Yes</td><td>Yes</td>
							<?php } ?>
							<td><?php echo $pincode['delivery_time']; ?></td>
							<td class="text-right"><a href="?route=catalog/pincode/edit&token=<?php echo $_GET['token']; ?>&pincode_id=<?php echo $pincode['id']; ?>" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		  </div>
		</form>
		<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
	.fa-csv:before{content:"\f063"}
</style>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/pincode/getlist&token=<?php echo $_GET['token']; ?>';

	var filter_pincode = $('input[name=\'filter_pincode\']').val();
	
	if (filter_pincode) {
		url += '&filter_pincode=' + encodeURIComponent(filter_pincode);
	}

	var filter_service = $('select[name=\'filter_service\']').val();

	if (filter_service) {
		url += '&filter_service=' + encodeURIComponent(filter_service);
	}

	var filter_delivery = $('select[name=\'filter_delivery\']').val();

	if (filter_delivery) {
		url += '&filter_delivery=' + encodeURIComponent(filter_delivery);
	}
	
	location = url;
});
//--></script> 
<?php echo $footer; ?>