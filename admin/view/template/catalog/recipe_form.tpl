<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-recipe" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-recipe" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
            <li><a href="#tab-contest" data-toggle="tab">Contest</a></li>
            <li><a href="#tab-link" data-toggle="tab">Links</a></li>
          </ul>
          
		  <div class="tab-content">		  
		  <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
               <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  
				  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
				  
					<!-- Date Added Start -->
			<div class="form-group">
            <label class="col-sm-2 control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
            <div class="col-sm-3">
              <div class="input-group datetime">
                <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][date_added]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['date_added'] : ''; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD HH:mm:ss" id="input-date-added" class="form-control" readonly />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span>
              </div>
            </div>
          </div>
					<!-- Date Added End   -->
				  
				 <!-- Category Edit   -->				  
				<div class="form-group ">
                <label class="col-sm-2 control-label" for="input-category_id"><?php echo $entry_category; ?></label>               
			    <div class="col-sm-3">
				<select name="category_id" style = "width:500px;" id="input-recipe-category" class="form-control">
                    <?php foreach ($recipe_categories as $recipe_category) { ?>
                    <?php if ($recipe_category['category_id'] == $category_id) { ?>
                    <option value="<?php echo $recipe_category['category_id']; ?>" selected="selected"><?php echo $recipe_category['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $recipe_category['category_id']; ?>"><?php echo $recipe_category['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                  </div>
              </div>		  
			  <!-- End Category -->
			  
			    <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-prep-time<?php echo $language['language_id']; ?>">Preparation Time<br/><small>(in Minutes)</small></label>
                    <div class="col-sm-10">
                      <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][prep_time]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['prep_time'] : ''; ?>" placeholder="Preparation Time" id="input-prep-time<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                </div>
				  
				  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="recipe_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['description'] : ''; ?></textarea>
                      <?php if (isset($error_description[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
				  
				   <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-directions<?php echo $language['language_id']; ?>"><?php echo $entry_directions; ?></label>
                    <div class="col-sm-10">
                      <textarea name="recipe_description[<?php echo $language['language_id']; ?>][directions]" placeholder="<?php echo $entry_directions; ?>" id="input-directions<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['directions'] : ''; ?></textarea>
                      <?php if (isset($error_description[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
				  
				   <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-ingredients<?php echo $language['language_id']; ?>"><?php echo $entry_ingredients; ?></label>
                    <div class="col-sm-10">
                      <textarea name="recipe_description[<?php echo $language['language_id']; ?>][ingredients]" placeholder="<?php echo $entry_ingredients; ?>" id="input-ingredients<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['ingredients'] : ''; ?></textarea>
                      <?php if (isset($error_description[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="recipe_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                    <div class="col-sm-10">
                      <textarea name="recipe_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-tag<?php echo $language['language_id']; ?>">Tag</label>
                    <div class="col-sm-10">
                      <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][tag]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['tag'] : ''; ?>" placeholder="Tag" id="input-tag<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  
				  </div>
                <?php } ?>
              </div>
            </div>   
			
			
			<div class="tab-pane" id="tab-data">
				
				<!-- Edited for image -->
				<div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_image; ?> (1080 x 1080)</label>
                <div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label">Home Page Image (1100 x 520)</label>
                <div class="col-sm-10"><a href="" id="thumb-imagee" data-toggle="image" class="img-thumbnail"><img src="<?php echo $homethumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="homepage_image" value="<?php echo $homepage_image; ?>" id="input-imagee" />
                </div>
              </div>
				<!-- End Edit -->
			
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="input-author<?php echo $language['language_id']; ?>"><?php echo $entry_author; ?></label>
                    <div class="col-sm-10">
                       <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][author]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['author'] : ''; ?>" placeholder="<?php echo $entry_author; ?>" id="input-author<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_description[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
			
                <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="Replace spaces with -"><?php echo $entry_keyword; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo "Keyword"; ?>" id="input-keyword" class="form-control" />
                  <?php if ($error_keyword) { ?>
                  <div class="text-danger"><?php echo $error_keyword; ?></div>
                  <?php } ?>
                </div>
              </div>
              
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-youtube-url<?php echo $language['language_id']; ?>">Youtube Video URL</label>
                    <div class="col-sm-10">
                        <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][youtube_url]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['youtube_url'] : ''; ?>" placeholder="Youtube Video URL" id="input-youtube-url<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                </div>
			  
               <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="recipe_description[<?php echo $language['language_id']; ?>][status]" id="input-status" class="form-control">
                    <?php if ($status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
			  
			  
			  <div class="form-group" >
                    <label class="col-sm-2 control-label" for="input-sort-order<?php echo $language['language_id']; ?>"><?php echo $entry_sort_order; ?></label>
					
                    <div class="col-sm-10">
                      <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][sort_order]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['sort_order'] : ''; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order<?php echo $language['language_id']; ?>" class="form-control" />
					  
                      <?php if (isset($error_description[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>                  
				  </div>                   
		   </div>
		   
		   <div class="tab-pane" id="tab-contest">
              	<div class="form-group">
                    <label class="col-sm-2 control-label" for="input-is-winner">Winner?</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="recipe_description[<?php echo $language['language_id']; ?>][is_winner]" value="1"  id="input-is-winner" class="form-control" <?php if($recipe_description[$language['language_id']]['is_winner'] == 1) { echo 'checked'; } ?> />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-contest-name"><span data-toggle="tooltip" title="If winner checkbox is checked then enter contest name here in proper case.">Contest Name</span></label>
                    <div class="col-sm-10">
                        <input type="text" name="recipe_description[<?php echo $language['language_id']; ?>][contest_name]" value="<?php echo isset($recipe_description[$language['language_id']]) ? $recipe_description[$language['language_id']]['contest_name'] : ''; ?>" placeholder="Enter Contest Name in 2 Words only" id="input-contest-name" class="form-control" />
                        <?php if (isset($error_contest_name[$language['language_id']])) { ?>
                            <div class="text-danger"><?php echo $error_contest_name[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                </div>
            </div> 
            
		  
		    <!--SOF Recipe Related Product-->
		    <div class="tab-pane" id="tab-link">
              	<div class="form-group">
                    <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                    <div class="col-sm-10">
                        <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                        <div id="recipe-related" class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php foreach ($recipe_relateds as $recipe_related) { ?>
                                <div id="recipe-related<?php echo $recipe_related['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $recipe_related['name']; ?>
                                <input type="hidden" name="recipe_related[]" value="<?php echo $recipe_related['product_id']; ?>" />
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-related-recipe"><span data-toggle="tooltip" title="<?php echo $help_related_recipe; ?>"><?php echo $entry_related_recipe; ?></span></label>
                    <div class="col-sm-10">
                        <input type="text" name="related_recipe" value="" placeholder="<?php echo $entry_related_recipe; ?>" id="input-related-recipe" class="form-control" />
                        <div id="related_recipe" class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php foreach ($related_recipes as $related_recipe) { ?>
                                <div id="related_recipe<?php echo $related_recipe['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $related_recipe['title']; ?>
                                <input type="hidden" name="related_recipe[]" value="<?php echo $related_recipe['related_recipe_id']; ?>" />
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- filters -->
                <div class="form-group">
                <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="<?php echo $help_filter; ?>"><?php echo $entry_filter; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="filter" value="" placeholder="<?php echo $entry_filter; ?>" id="input-filter" class="form-control" />
                  <div id="recipe-filter" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($recipe_filters as $recipe_filter) { ?>
                    <div id="recipe-filter<?php echo $recipe_filter['filter_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $recipe_filter['name']; ?>
                      <input type="hidden" name="recipe_filter[]" value="<?php echo $recipe_filter['filter_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
                <!-- filters -->
            </div>
            <!--EOF Recipe Related Product-->
            
            
		   </div>
		  
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>  
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>

<script type = "text/javascript">
// Category
$('input[name=\'category\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/recipe/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'category\']').val('');

		$('#recipe-category' + item['value']).remove();

		$('#recipe-category').append('<div id="recipe-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="recipe_category[]" value="' + item['value'] + '" /></div>');
	}
});

$('#recipe-category').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

</script>
<script type = "text/javascript">

$('input[name=\'related\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'related\']').val('');

		$('#recipe-related' + item['value']).remove();

		$('#recipe-related').append('<div id="recipe-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="recipe_related[]" value="' + item['value'] + '" /></div>');
	}
});

$('input[name=\'related_recipe\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/recipe/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['title'],
						value: item['recipe_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'related_recipe\']').val('');

		$('#related_recipe' + item['value']).remove();

		$('#related_recipe').append('<div id="related_recipe' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="related_recipe[]" value="' + item['value'] + '" /></div>');
	}
});

$('#recipe-related, #related_recipe').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});


// Filter
$('input[name=\'filter\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['filter_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter\']').val('');

		$('#recipe-filter' + item['value']).remove();

		$('#recipe-filter').append('<div id="recipe-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="recipe_filter[]" value="' + item['value'] + '" /></div>');
	}
});

$('#recipe-filter').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
</script>
<?php echo $footer; ?>