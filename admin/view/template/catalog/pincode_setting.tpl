<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-setting" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="?route=extension/module/pincode&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php //echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>Settings</h3>
      </div>
      <div class="panel-body">
		<form action="?route=catalog/pincode/setting&token=<?php echo $_GET['token']; ?>" method="post" enctype="multipart/form-data" id="form-setting">
			<div class="table-responsive">
				<table class="table table-bordered">
					<tbody>
							<tr align = "center"><td  align = "left"><span data-toggle="tooltip" title="Are you want to stop CHECKOUT for Unserviceable PINCODE." id = "shippingpin" >On checkout Page.</span> </td>
								<td  align = "left">
									<?php 
										$pincode_checkout_status = $pincode_checkout_status;
										if(isset($pincode_checkout_status)){ 
											if($pincode_checkout_status == 1){ ?>
												<select class="onj_width"  name = "pincodesetting_checkout_status" >
													<option value = "1">Enable</option/>
													<option value = "0">Disable</option/>
												</select>
											<?php
											}
											else{ ?>
												<select class="onj_width"  name = "pincodesetting_checkout_status">
													<option value = "0">Disable</option/>
													<option value = "1">Enable</option/>
												</select>
											<?php
											}
										}
										else { ?>
											<select class="onj_width"  name = "pincodesetting_checkout_status">
													<option value = "0">Disable</option/>
													<option value = "1">Enable</option/>
											</select>
										<?php
										}
									?>
								</td>
								</tr>
							<tr align = "center">
								<td  align = "left"><span data-toggle="tooltip" title="Enable it that means COD/other payment method will show when billing and shipping(delivery) post code inout field must be same." id = "shippingpin" >Pincode Must Be Same.</span> </td>
								<td  align = "left">
									<?php 
										$pincodesetting_pincodemustbesame = $pincodesetting_pincodemustbesame;
										if(isset($pincodesetting_pincodemustbesame)){ 
											if($pincodesetting_pincodemustbesame == 1){ ?>
												<select  class="onj_width"  name = "pincodesetting_pincodemustbesame" >
													<option value = "1">Enable</option/>
													<option value = "0">Disable</option/>
												</select>
											<?php
											}
											else{ ?>
												<select class="onj_width"  name = "pincodesetting_pincodemustbesame">
													<option value = "0">Disable</option/>
													<option value = "1">Enable</option/>
												</select>
											<?php
											}
										}
										else { ?>
											<select class="onj_width"  name = "pincodesetting_pincodemustbesame">
													<option value = "0">Disable</option/>
													<option value = "1">Enable</option/>
											</select>
										<?php
										}
									?>
								</td>
							</tr>
							<tr align = "center">
								<td  align = "left">Show On Product Detail Page</td>
								<?php $product_page_status = $pincode_product_page_status; ?>
								<td align = "left">
									<select class="onj_width"  name = "pincodesetting_product_page_status">
										<?php if(!isset($product_page_status)){ ?>
											<option value = "0">No</option>
											<option value = "1">Yes</option>
										<?php } elseif($product_page_status == '1'){ ?>
											<option value = "1" selected>Yes</option>
											<option value = "0">No</option>
										<?php }elseif($product_page_status == '0'){ ?>
											<option value = "0" selected>No</option>
											<option value = "1">Yes</option>
										<?php }?>
									</select>
								</td>
							</tr>
							<tr align = "center" ><td  align = "left">No Service Available Message</td>
								<td  align = "left"> 
								<?php if(isset($pincode_no_service_msg)){ ?>
									<input type="text"  class="onj_width" name="pincodesetting_no_service_msg" id="no_service_msg" value="<?php echo $pincode_no_service_msg; ?>">
									<?php } else { ?>
										<input type="text" class="onj_width"  name="pincodesetting_no_service_msg" id="no_service_msg" value="Service is not Available at your location yet">
										<?php } ?>
								</td>
							</tr>
							<tr align = "center" ><td  align = "left">COD Message</td>
								<td  align = "left">
								<?php if(isset($pincode_cod_msg)){ ?>
									<input type="text" class="onj_width" name="pincodesetting_cod_msg" id="cod_msg" value="<?php echo $pincode_cod_msg; ?>" >
									<?php } else { ?>
										<input type="text" class="onj_width" name="pincodesetting_cod_msg" id="cod_msg" value="COD Service Available at this Location.">
									<?php } ?>
								</td>
							</tr>
							<tr align = "center" ><td  align = "left">Prepaid Message</td>
								<td  align = "left">
								<?php if(isset($pincode_prepaid_msg)){ ?>
									<input type="text" name="pincodesetting_prepaid_msg" class="onj_width" id="prepaid_msg" value="<?php echo $pincode_prepaid_msg; ?>">
								<?php } else { ?>
									<input type="text" name="pincodesetting_prepaid_msg"  class="onj_width" id="prepaid_msg" value="Only Prepaid Service Available" > 
								<?php } ?>
								</td>
							</tr>
							<!--<tr align = "center" ><td  align = "left">COD Error Message</td>
								<td  align = "left">
								<?php if(isset($pincode_cod_error)){ ?>
									 <textarea name="pincodesetting_cod_error" id="cod_error"><?php echo $pincode_cod_error; ?></textarea>
								 <?php } else { ?>
									 <textarea name="pincodesetting_cod_error" id="cod_error">Error occurred in COD Service</textarea>
								 <?php } ?>
								</td>
							</tr>-->
							<tr align = "center"><td  align = "left">Message text Color</td>
								<td  align = "left">
									<?php 
									$text_color = $pincode_text_color;
									if(isset($text_color)){ ?>
										<input class="color onj_width" name = "pincodesetting_text_color" value = "<?php echo $text_color; ?>" size = "50">
									<?php }
									else { ?>
										<input class="color onj_width" name = "pincodesetting_text_color" value = "FF2D0D" size = "50">
									<?php
										}
									?>
								</td>
							</tr>
						</tbody>
					</table>
			</div>
		</form>
      </div>
    </div>
  </div>
  <style>
  #shippingpin:after {
    font-family: FontAwesome;
    color: #1E91CF;
    content: "\f059";
    margin-left: 4px;
}
.onj_width {width:100%;}
  </style>
  <script type="text/javascript" src="view/javascript/jquery/jscolor/jscolor.js"></script>
	<script type="text/javascript"><!--
		$('#no_service_msg').summernote({
			height: 100
		});
		$('#cod_msg').summernote({
			height: 100
		});
		$('#cod_msg').summernote({
			height: 100
		});
		$('#prepaid_msg').summernote({
			height: 100
		});
		$('#cod_error').summernote({
			height: 100
		});
		//--></script>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>