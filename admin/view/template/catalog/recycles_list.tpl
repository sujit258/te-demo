<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <!-- <div class="pull-right"> 
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-recycles').submit() : false;"><i class="fa fa-trash-o"></i></button> 
      </div>	Delete Button-->
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
	  <!-- Filter -->
	  <div class="well">
      <div class="row">
      <div class="col-sm-4">
      <div class="form-group">
      <label class="control-label" for="input-name"><?php echo $column_name; ?></label>
      <input type="text" name="filter_name" placeholder="<?php echo $filter_name_placeholder; ?>" id="input-name" class="form-control" /><br>
	  <label class="control-label" for="input-mobile"><?php echo $column_mobile; ?></label>
      <input type="text" name="filter_mobile" placeholder="<?php echo $filter_mobile_placeholder; ?>" id="input-mobile" class="form-control" />
      
      </div> 
	  </div>

			<div class="col-sm-4"><br>
				<label class="control-label" for="input-status"><?php echo $column_status; ?></label>	    
				<select style = "width:316px;" name="filter_status" id="input-status" class="form-control">	
				<option value="*" ><?php echo $filter_status_placeholder; ?></option>
				<?php foreach($status_list as $result) { ?>					
				<option value = "<?php echo $result['status_id']; ?>" ><?php echo $result['name']; ?></option>				
				<?php } ?>				
                </select><br><br>		
				<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
			</div>
	 </div>			
	 </div>
	  <!-- End Filter -->
        
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-recycles">
        <div class="table-responsive">
        <table class="table table-bordered table-hover">
        <thead>
        <tr>
        <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
        <td class="text-left"><?php echo $column_name; ?></td>
		<td class="text-left"><?php echo $column_mobile; ?></td>
		<td class="text-center"><?php echo $column_no_of_items; ?></td>
		<td class="text-center"><?php echo $column_status; ?></td>
		<td class="text-center"><?php echo $column_request_date; ?></td>
        <td class="text-right"><?php echo $column_action; ?></td>
        </tr>
        </thead>
        <tbody>
        
    <?php if ($recycles_list) { ?>
                <?php foreach ($recycles_list as $list) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($list['recycle_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $list['recycle_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $list['recycle_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $list['name']; ?></td>
				  <td class="text-left"><?php echo $list['mobile']; ?></td>
                  <td class="text-center"><?php echo $list['total_packets']; ?></td>
                  <td class="text-center"><?php echo $list['status']; ?></td>
                  
                  <td class="text-center"><?php echo $list['request_date']; ?></td>
                  <td class="text-right"><a href="<?php echo $list['edit']; ?>" data-toggle="tooltip" title="<?php echo "EDIT DETAILS"; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/recycles&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {		
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_mobile = $('input[name=\'filter_mobile\']').val();

	if (filter_mobile) {		
		url += '&filter_mobile=' + encodeURIComponent(filter_mobile);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}	

	location = url;
});
//--></script>

 <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/recycles/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['recycle_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_mobile\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/recycles/autocomplete&token=<?php echo $token; ?>&filter_mobile=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['mobile'],
						value: item['recycle_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_mobile\']').val(item['label']);
	}
});

//--></script>


</div>
<?php echo $footer; ?>