<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
	   <button type="submit" form="form-recycles" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
       <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo "Cancel"; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo "Customer recycle Details"; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-recycles" class="form-horizontal">
		<?php foreach($recycles as $recycle) { ?>
          <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $recycle['name']; ?>" id="input-name" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_mobile; ?></label>
            <div class="col-sm-10">
              <input type="text" name="mobile" value="<?php echo $recycle['mobile']; ?>" id="input-mobile" class="form-control" />              
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_email; ?></label>
            <div class="col-sm-10">
            <input type="text" name="email" value="<?php echo $recycle['email']; ?>"  id="input-email" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_address; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address" value="<?php echo $recycle['address']; ?>" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_no_of_items; ?></label>
            <div class="col-sm-10">
              <input type="text" name="total_packets" value="<?php echo $recycle['total_packets']; ?>"  id="input-author" class="form-control" />
            </div>
          </div>		        
          <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_request_date; ?></label>
            <div class="col-sm-3">
              <div class="input-group datetime">
                <input type="text" name="request_date" value="<?php echo $recycle['request_date']; ?>"  data-date-format="YYYY-MM-DD HH:mm:ss" id="input-request_date" class="form-control" disabled/>                
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_pickup_date; ?></label>
            <div class="col-sm-3">
              <div class="input-group date">
                <input type="text" name="pickup_date" value="<?php echo $recycle['pickup_date']; ?>" data-date-format="YYYY-MM-DD HH:mm:ss" id="input-pickup_date" class="form-control" disabled/>                
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_pickup_slot; ?></label>
            <div class="col-sm-3">
              <div class="input-group">
                <?php echo $recycle['pickup_slot']; ?>               
              </div>
            </div>
          </div>
		  <div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $tag_status; ?></label>
				<div class="col-sm-3">
				<select style = "width:277px;" name="recycle_status" class="form-control">				
				<?php foreach($status_list as $result) { ?>
				<?php if($recycle['status_id'] == $result['status_id']) { ?>
				<option value = "<?php echo $result['status_id']; ?>" selected><?php echo $result['name']; ?></option>		<?php } else {?>
				<option value = "<?php echo $result['status_id']; ?>"><?php echo $result['name']; ?></option>			
				<?php } ?>
				<?php } ?>
                </select>				
				</div>				
			</div>			
		  <?php } ?>		 
        </form>
      </div>
    </div>
  </div>
  
<?php echo $footer; ?>