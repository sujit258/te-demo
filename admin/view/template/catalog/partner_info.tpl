<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
       <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo "Back"; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo "Partner Details"; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-partner" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" disabled/>
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_mobile; ?></label>
            <div class="col-sm-10">
              <input type="text" name="mobile" value="<?php echo $mobile; ?>" id="input-mobile" class="form-control" disabled/>              
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_email; ?></label>
            <div class="col-sm-10">
            <input type="text" name="email" value="<?php echo $email; ?>"  id="input-email" class="form-control" disabled/>
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_city; ?></label>
            <div class="col-sm-10">
              <input type="text" name="city" value="<?php echo $city; ?>" class="form-control" disabled/>
            </div>
          </div>
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_scope; ?></label>
            <div class="col-sm-10">
              <input type="text" name="scope" value="<?php echo $scope; ?>"  id="input-author" class="form-control" disabled/>
            </div>
          </div>
		        
          <div class="form-group">
            <label class="col-sm-2 control-label" ><?php echo $tag_request_date; ?></label>
            <div class="col-sm-3">
              <div class="input-group datetime">
                <input type="text" name="request_date" value="<?php echo $request_date; ?>"  data-date-format="YYYY-MM-DD HH:mm:ss" id="input-request_date" class="form-control" disabled/>
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span>
              </div>
            </div>
          </div>
         
        </form>
      </div>
    </div>
  </div>

<?php echo $footer; ?>