<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
               <a href="?route=extension/module/pincode&token=<?php echo $_GET['token']; ?>" data-toggle="tooltip" title="Back" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      
    </div>
  </div>
  <div class="container-fluid">
    <?php if (isset($error_warning)) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if (isset($success)) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-life-ring"></i> Welcome to our ONJECTION family!</h3>
		<div class="pull-right">
		
      </div> </div>
      <div class="panel-body">
		<p>Purchasing this great module brought you to become part of our family. In ONJECTION, our mantra is set to deliver great products with fanatically awesome customer service</p>
		<h2>support</h2>
		<p>ONJECTION team is taking support very seriously. No matter what issue or question you have we will address it. Most of the times we answer within couple of hours, as sometimes it can take a day. Please click the button below to Contact Us.</p>
		<div><a href="http://www.onjection.com/contact-us/" target="_blank" class="btn btn-primary ">CONTACT US</a></div>
	    <h2 id = "show_install" >Installation Step:</h2>
				<div id = "install">
					<ol>
					     <li>Extract the zip file first name : unzip_me-Postcode_ServiceAvailability_Check.zip You will see 2 files</li>
                        <li>Upload_me.ocmod.zip and Documentaion file </li>
						<li>Go to admin->Extensions->Extension Installer.</li> 
                        <li>Click on Upload button and select the zip File Name is : Upload_me.ocmod.zip</li>
                        <li>After the progress bar has been completed with the success message underneath it then go to Extensions > Modifications and hit the blue "Refresh" button in the upper right corner under the notification icon. Please make sure that you click the blue refresh button and not the browser refresh button.</li>
						<li>Then Go to admin => Extensions => Extensions => Modules and then install Pincode Module.</li>
						<li>After installation click on edit of “Pincode Module”.</li>
                        <li>Now you can see the list. You can  perform following operations:-
							<ul>
								<li>♦ Click on “INSERT DELIVERY OPTIONS”. You can add multiple delivery options by clicking on “+”.</li>
								<li>♦ Click on “Insert Pincode” to add New Pin Code.</li>
								<li>♦ In it you can add individually, and, multiple according to Services and Delivery Time. For Multiple you can click on “+” sign, and for removing one of them while inserting click on “-” sign.</li>
								<li>♦ Also you can upload “CSV file” containing Pin Codes (according to Services and Delivery Time).</li>
								<li>♦ Click on “List Pincode” for viewing the Pincodes.</li>
								<li>♦ You can also download the “CSV file” of Pincodes.</li>
								<li>♦ You can also have list of DELIVERY by clicking on “DELIVERY OPTIONS LIST”.</li>
								<li>♦ Pincode on Product Page can be viewed by clicking on “Setting”. Select “YES \ NO” on the list of “Show On Product Detail Page”.</li>
								<li>♦ Pincode on Checkout Page can be viewed by clicking on “Setting”. Select “Enable \ Disable” on the list of “Are you want to stop CHECKOUT for Unserviceable PINCODE”.</li>
								<li>♦ The message displayed on front end will be declared here.</li>
							</ul>
						</li>
						<li>At last you have click enable on the list of “Status” occurred firstly on “PINCODE” module and click “Save” button.
						</li>
						
					</ol>
				</div>
				<h2 >Onjection News:</h2><br />
				<div id="onj_news">
					<iframe src="http://shipway.in/newupdates/updates.php?extension=size_chart_2.0&oc_version=2.0" scrolling="no"  style="border:none;width:100%;min-height:200px" > </iframe><br />
					<div style = "float:left"><a href = "http://www.opencart.com/index.php?route=extension/extension&filter_username=vikasgarg40" class = "btn btn-primary" >MORE EXTENSIONS</a></div><br/>
				</div>


      </div>
    </div>
  </div>

</div>
<?php echo $footer; ?>