<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-right"><?php echo $column_order_id; ?></td>
        <td class="text-left"><?php echo $column_customer; ?></td>
        <td class="text-left">Order Total</td>
        <td class="text-left">Order Status</td>
        <td class="text-left"><?php echo $column_date_added; ?></td>
        <td class="text-left">view order</td>
      </tr>
    </thead>
    <tbody>
      <?php if ($histories) { ?>
      <?php foreach ($histories as $history) { ?>
      <tr>
        <td class="text-right"><?php echo $history['order_id']; ?></td>
        <td class="text-left"><?php echo $history['customer']; ?></td>
        <td class="text-left"><?php echo $history['total']; ?></td>
        <td class="text-left"><?php echo $history['order_status']; ?></td>
        <td class="text-left"><?php echo $history['date_added']; ?></td>
        <td class="text-left"><a href="<?php echo $history['view']; ?>" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="View" target="_blank"><i class="fa fa-eye"></i></a></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
