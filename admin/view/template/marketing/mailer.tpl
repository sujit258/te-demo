<?php 
/*** jitendra kumar sharma (vxinfosystem.com) jks0586@gmail.com, vxinfosystem@gmail.com ****/
?>
<?php echo $header; ?><?php echo $column_left; ?>
<style>
    #content{
        margin-left:4%;
    } 
    
    table {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }
    
    table td, table th {
      border: 1px solid #ddd;
      padding: 8px;
    }
    
    table tr:nth-child(even){background-color: #f2f2f2;}
    
    table tr:hover {background-color: #ddd;}
    
    table th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #4CAF50;
      color: white;
    }
    
    .buttons {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        border-radius: 7px;
        margin-top: 10%;
    }
</style>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
            <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            
            <a href="<?php echo $send_mailer; ?>" data-toggle="tooltip" title="Mailer" class="buttons">Mailer</a> 
        </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	 <?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
    <?php } ?>
	<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
	  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
	  <div class="panel-body">
			<form action="" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
				  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description"><?php echo $text_import; ?></label>
                    <div class="col-sm-10">
                       <input type="file"  style="height:33px;" name="import" value=""/>
                    </div>
                  </div>
			</form>
			
		
		    <table>
                      <thead>
                          <tr>
                              <th>A</th>
                              <th>B</th>
                              <th>C</th>
                              <th>D</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>Customer ID</td>
                              <td>Email</td>
                              <td>Customer Name</td>
                              <td>Customer Type</td>
                          </tr>
                      </tbody>
                  </table>	
	   </div>
	</div>
   </div>
</div>
<?php echo $footer; ?>