<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $export; ?>" data-toggle="tooltip" title="<?php echo $button_export; ?>" class="btn btn-success"><i class="fa fa-download"></i></a> <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-coupon').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
          
          					<div class="well">
						  <div class="row">
							<div class="col-sm-4">
							  <div class="form-group">
								<label class="control-label" for="input-coupon-name"><?php echo $entry_coupon_name; ?></label>
								<input type="text" name="filter_coupon_name" value="<?php echo $filter_coupon_name; ?>" placeholder="<?php echo $entry_coupon_name; ?>" id="input-coupon-name" class="form-control" />
							  </div>
							  <div class="form-group">
								<label class="control-label" for="input-code"><?php echo $entry_code; ?></label>
								<input type="text" name="filter_code" value="<?php echo $filter_code; ?>" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control" />
							  </div>
							</div>
							<div class="col-sm-4">
							  <div class="form-group">
								<label class="control-label" for="input-coupon-status"><?php echo $entry_coupon_status; ?></label>
								<select name="filter_coupon_status" id="input-coupon-status" class="form-control">
								  <option value="*"></option>
								  <?php if ($filter_status) { ?>
								  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								  <?php } else { ?>
								  <option value="1"><?php echo $text_enabled; ?></option>
								  <?php } ?>
								  <?php if (!$filter_status && !is_null($filter_status)) { ?>
								  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								  <?php } else { ?>
								  <option value="0"><?php echo $text_disabled; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="form-group">
								<label class="control-label" for="input-discount"><?php echo $entry_discount; ?></label>
								<input type="text" name="filter_discount" value="<?php echo $filter_discount; ?>" placeholder="<?php echo $entry_discount; ?>" id="input-discount" class="form-control" />
							  </div>
							</div>
							<div class="col-sm-4">
							  <div class="form-group">
								<label class="control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
								<div class="input-group date">
								  <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
								  <span class="input-group-btn">
								  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
								  </span></div>
							  </div>
							  <div class="form-group">
								<label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
								<div class="input-group date">
								  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
								  <span class="input-group-btn">
								  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
								  </span></div>
							  </div>
							  <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
							</div>
						  </div>
					</div>
					
					
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-coupon">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                    <!-- S-coupon -->
                  <td class="text-left">Default</td>
			<!-- kupon -->
                  <td class="text-left"><?php if ($sort == 'code') { ?>
                    <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'discount') { ?>
                    <a href="<?php echo $sort_discount; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_discount; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_discount; ?>"><?php echo $column_discount; ?></a>
                    <?php } ?></td>
                    <!--order total-->
                    <td> Order Total</td>
                    <!--order total-->
                  <td class="text-left"><?php if ($sort == 'date_start') { ?>
                    <a href="<?php echo $sort_date_start; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_start; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_start; ?>"><?php echo $column_date_start; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'date_end') { ?>
                    <a href="<?php echo $sort_date_end; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_end; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_end; ?>"><?php echo $column_date_end; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($coupons) { ?>
                <?php foreach ($coupons as $coupon) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($coupon['coupon_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $coupon['coupon_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $coupon['coupon_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $coupon['name']; ?></td>
                  <!-- S-coupon -->
                  <td class="text-left">
                    <?php if ($coupon['default1'] == '1') { ?>
                    YES
                    <?php } else { ?>
                    NO
                    <?php } ?>
                  </td>
			<!-- S-coupon -->
                  <td class="text-left"><?php echo $coupon['code']; ?></td>
                  <td class="text-right"><?php echo $coupon['discount']; ?></td>
                  <td class="text-right"><?php echo $coupon['ordertotal']; ?> (Redeemption:<?php echo $coupon['orders']; ?>)</td>
                  <td class="text-left"><?php echo $coupon['date_start']; ?></td>
                  <td class="text-left"><?php echo $coupon['date_end']; ?></td>
                  <td class="text-left"><?php echo $coupon['status']; ?></td>
                  <td class="text-right"><a href="<?php echo $coupon['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
  <script tye="text/javascript">
    $('#input-coupon-name, #input-code, #input-discount, #input-date-added, #input-date-end').on('keyup', function (e) {
        if (e.keyCode == 13) {
            $("#button-filter").click(); 
        }
    });
  </script>
<script type="text/javascript"><!--
	$('#button-filter').on('click', function() {
	url = 'index.php?route=marketing/coupon&token=<?php echo $token; ?>';

	    var filter_coupon_name = $('input[name=\'filter_coupon_name\']').val();
        	if (filter_coupon_name) {
    			url += '&filter_coupon_name=' + encodeURIComponent(filter_coupon_name);
    		}

	    var filter_discount = $('input[name=\'filter_discount\']').val();
            if (filter_discount) {
				url += '&filter_discount=' + encodeURIComponent(filter_discount);
			}

		var filter_coupon_status = $('select[name=\'filter_coupon_status\']').val();

			if (filter_coupon_status != '*') {
			    url += '&filter_coupon_status=' + encodeURIComponent(filter_coupon_status);
			}	

			var filter_code = $('input[name=\'filter_code\']').val();
                if (filter_code) {
					url += '&filter_code=' + encodeURIComponent(filter_code);
				}	

			var filter_date_added = $('input[name=\'filter_date_added\']').val();

				if (filter_date_added) {
					url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
				}

			var filter_date_end = $('input[name=\'filter_date_end\']').val();
                if (filter_date_end) {
					url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
				}

				location = url;
	});
//--></script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<script type="text/javascript"><!--
		$('.date').datetimepicker({
				pickTime: false
		});
//--></script>

<?php echo $footer; ?>