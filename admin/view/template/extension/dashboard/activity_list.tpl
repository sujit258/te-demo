<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
       
      <h1>All Activities</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo $heading_title; ?></h3>
  </div>
  
  <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                  <td class="text-left">Comment</td>
                  <td class="text-left">Added On</td>
                </tr>
            </thead>
            <tbody>
                <?php if ($activities) { ?>
                    <?php foreach ($activities as $activity) { ?>
                <tr>
                  <td class="text-left"><?php echo $activity['comment']; ?></td>
                  <td class="text-left"><small class="text-muted"><i class="fa fa-clock-o"></i> <?php echo $activity['date_added']; ?></small></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <br>
    <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
    </div>
  
</div>
</div>
</div>


<?php echo $footer; ?> 