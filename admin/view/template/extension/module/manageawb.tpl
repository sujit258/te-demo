<?php echo $header ; ?><?php echo $column_left ; ?>

<div id="content">

	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $download_awb ; ?>">
					<button data-toggle="tooltip" title="<?php echo $button_download_awb ; ?>" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; <?php echo $button_download_awb ; ?></button>
				</a>
			</div>
		    <h1><?php echo $heading_title ; ?></h1>
		    <ul class="breadcrumb">
			   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
		    </ul>
		</div>
	</div>

	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning ; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success ; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
		
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title ; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $filter_action ; ?>" method="get" enctype="multipart/form-data" id="filter_forms">
					<input type="hidden" name="route" value="extension/module/manageawb" />
					<input type="hidden" name="token" value="<?php echo $token ; ?>" />
					<div class="well">
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-awb"><?php echo $column_awb ; ?></label>
									<input type="text" name="filter_awb" value="<?php echo $filter_awb ; ?>" placeholder="<?php echo $column_awb ; ?>" id="input-awb" class="form-control" />
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-shipment_id"><?php echo $column_shipment_id ; ?></label>
									<input type="text" name="filter_shipment_id" value="<?php echo $filter_shipment_id ; ?>" placeholder="<?php echo $column_shipment_id ; ?>" id="input-shipment_id" class="form-control" />
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-shipment_to"><?php echo $column_shipment_to ; ?></label>
									<input type="text" name="filter_shipment_to" value="<?php echo $filter_shipment_to ; ?>" placeholder="<?php echo $column_shipment_to ; ?>" id="input-shipment_to" class="form-control" />
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-state"><?php echo $column_state ; ?></label>
									<select name="filter_state" id="input-state" class="form-control">
										<option value=""></option>
										<?php foreach ($data_state as $manage_state) { ?>
											<?php if ($filter_state == $manage_state['values']) { ?>
												<option value="<?php echo $manage_state['values'] ; ?>" selected="selected"><?php echo $manage_state['label'] ; ?></option>
											<?php } else { ?>
												<option value="<?php echo $manage_state['values'] ; ?>"><?php echo $manage_state['label'] ; ?></option>
										<?php } ?>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-status"><?php echo $entry_status ; ?></label>
									<select name="filter_status" id="input-status" class="form-control">
										<option value=""></option>
										<?php foreach ($status as $manage_status) { ?>
										
											<?php if ($filter_status == $manage_status['values']) { ?>
												<option value="<?php echo $manage_status['values'] ; ?>" selected="selected"><?php echo $manage_status['values'] ; ?></option>
											<?php } else { ?>
												<option value="<?php echo $manage_status['values'] ; ?>"><?php echo $manage_status['values'] ; ?></option>
										<?php } ?>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-status_type"><?php echo $column_status_type ; ?></label>
									<select name="filter_status_type" id="input-status_type" class="form-control">
										<option value=""></option>
										<?php foreach ($data_status_type as $manage_stype) { ?>
										
											<?php if ($filter_status_type == $manage_stype['values']) { ?>
												<option value="<?php echo $manage_stype['values'] ; ?>" selected="selected"><?php echo $manage_stype['values'] ; ?></option>
											<?php } else { ?>
												<option value="<?php echo $manage_stype['values'] ; ?>"><?php echo $manage_stype['values'] ; ?></option>
										<?php } ?>
										<?php } ?>
										
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-orderid"><?php echo $column_orderid ; ?></label>
									<input type="text" name="filter_order_increment_id" value="<?php echo $filter_order_increment_id ; ?>" placeholder="<?php echo $column_orderid ; ?>" id="input-orderid" class="form-control" />
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="form-group">
								<br />
								<button type="submit" id="button-filter" class="btn btn-primary"><i class="fa fa-filter"></i> <?php echo $button_filter ; ?></button>
								
								
								<a href="<?php echo $reset_filter ; ?>">
									<button type="button" id="button-filter" class="btn btn-primary">Reset Filter</button>
								</a>
								
								</div>
							</div>
							
						</div>
					</div>
				</form>
				
				<form action="<?php echo $massaction ; ?>" method="post" enctype="multipart/form-data" id="form-manageawbs" target="">
					<div class="">
						<div class="row">
							
							<div class="col-sm-3">
								<div class="form-group">
									<select name="massactions" id="input-massactions" class="form-control">
										<option value="">Actions</option>
										<option value="submit_manifest"><?php echo $text_submit_manifest ; ?></option>
										<option value="update_awb_status"><?php echo $text_update_awb_status ; ?></option>
										<option value="cancel_package"><?php echo $text_cancel_package ; ?></option>
										<option value="print_shipping_label"><?php echo $text_print_shipping_label ; ?></option>
									</select>
								</div>
							</div>
							<div class="col-sm-3 manifestsLocation" style="display:none;">
								<div class="form-group">
									<select name="manifest_location" id="manifest_location" class="form-control">
										<option value="">Select Location</option>
										<?php foreach ($location_datas as $loct) { ?>
											<option value="<?php echo $loct['values'] ; ?>" ><?php echo $loct['label'] ; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<button type="button" id="button-submit" class="btn btn-primary massactionBtn"><i class="fa fa-save"></i> Submit </button>
								</div>
							</div>
							
						</div>
					</div>
					
					  <div class="table-responsive">
						<table class="table table-bordered table-hover">
						  <thead>
							<tr>
								<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
								<td class="text-left">
									<?php if ($sort == 'entity_id') { ?>
										<a href="<?php echo $sort_entity_id ; ?>" class="<?php echo $order|lower ; ?>"><?php echo $column_entity_id ; ?></a>
									<?php } else { ?>
										<a href="<?php echo $sort_entity_id ; ?>"><?php echo $column_entity_id ; ?></a>
								<?php } ?>
								</td>
								<td class="text-left"><?php echo $column_awb ; ?></td>
								<td class="text-left"><?php echo $column_shipment_id ; ?></td>
								<td class="text-left"><?php echo $column_shipment_to ; ?></td>
								<td class="text-left"><?php echo $column_state ; ?></td>
								<td class="text-left"><?php echo $column_status ; ?></td>
								<td class="text-left"><?php echo $column_status_type ; ?></td>
								<td class="text-left"><?php echo $column_orderid ; ?></td>
								<td class="text-right"><?php echo $column_action ; ?></td>
							</tr>
						  </thead>
						  <tbody>
							<?php if ($delhiveryLastmile_manageAwb) { ?>
							<?php foreach ($delhiveryLastmile_manageAwb as $manage_awb) { ?>
							<tr>
								<td class="text-center">
								<?php if ($manage_awb['entity_id'] == selected) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $manage_awb['entity_id'] ; ?>" checked="checked" />
									<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $manage_awb['entity_id'] ; ?>" />
								<?php } ?>
								</td>
								<td class="text-left"><?php echo $manage_awb['entity_id'] ; ?></td>
								<td class="text-left"><?php echo $manage_awb['awb'] ; ?></td>
								<td class="text-left"><?php echo $manage_awb['shipment_id'] ; ?></td>
								<td class="text-left"><?php echo $manage_awb['shipment_to'] ; ?></td>
								<td class="text-left"><?php echo $manage_awb['state'] ; ?></td>
								<td class="text-left"><?php echo $manage_awb['status'] ; ?></td>
								<td class="text-left"><?php echo $manage_awb['status_type'] ; ?></td>
								<td class="text-left"><?php echo $manage_awb['order_increment_id'] ; ?></td>
								<td class="text-right">
									<a href="<?php echo $manage_awb['edit'] ; ?>" data-toggle="tooltip" title="<?php echo $button_edit ; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
								</td>
							</tr>
							<?php } ?>
							<?php } else { ?>
							<tr>
							  <td class="text-center" colspan="10"><?php echo $text_no_results ; ?></td>
							</tr>
						<?php } ?>
						  </tbody>
						</table>
					  </div>
					</form>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination ; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results ; ?></div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>
<!-- onclick="confirm('<?php echo $text_confirm ; ?>') ? $('#form-mpblogcategory').submit() : false;" -->
<script type="text/javascript"><!--
$('#input-massactions').on('change', function() {
if($('#input-massactions').val()=='print_shipping_label'){
		$('#form-manageawbs').attr("target","_blank");
	}else{
		$('#form-manageawbs').attr("target","");
	}
	if($(this).val()=='submit_manifest'){
		$(".manifestsLocation").show();
		$("#manifest_location").val("");
	}else{
		$(".manifestsLocation").hide();
		$("#manifest_location").val("");
	}
});
//--></script>
<script type="text/javascript"><!--
$('.massactionBtn').on('click', function() {

	
	if($('#input-massactions').val()=='submit_manifest'){
		if($("#manifest_location").val()){
			$('#form-manageawbs').submit();
		}else{
			alert("Please select location.");
		}
	}else if($('#input-massactions').val()){
		$('#form-manageawbs').submit();
	}else{
		alert("Please select action.");
	}
});
//--></script>
<?php echo $footer ; ?>