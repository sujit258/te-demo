<?php 
   $reviewmail_name = $moduleName.'[ReviewMail]['.$reviewmail['id'].']';
   $reviewmail_data = (isset($moduleData['ReviewMail'][$reviewmail['id']])) ? $moduleData['ReviewMail'][$reviewmail['id']] : array();
?>
<div id="reviewmail_<?php echo $reviewmail['id']; ?>" class="tab-pane reviews" style="width:99%;overflow:hidden;">
   <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#general_settings_<?php echo $reviewmail['id'] ?>">General</a></li>
        <li><a data-toggle="tab" href="#conditions_<?php echo $reviewmail['id'] ?>">Configuration</a></li>
        <li><a data-toggle="tab" href="#email_template_<?php echo $reviewmail['id'] ?>">Email Template</a></li>
        <li><a data-toggle="tab" href="#discount_<?php echo $reviewmail['id'] ?>">Discount Settings</a></li>
        <li id="discountMailTab_<?php echo $reviewmail['id']; ?>"><a data-toggle="tab" href="#discount_email_template_<?php echo $reviewmail['id'] ?>">Discount Email Template</a></li>
    </ul>

    <div class="tab-content">
        <div id="general_settings_<?php echo $reviewmail['id'] ?>" class="tab-pane fade in active">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_general.php'); ?>
        </div>
        <div id="conditions_<?php echo $reviewmail['id'] ?>" class="tab-pane fade in">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_conditions.php'); ?>
        </div>
        <div id="discount_<?php echo $reviewmail['id'] ?>" class="tab-pane fade in">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_discount.php'); ?>
        </div>
        <div id="email_template_<?php echo $reviewmail['id'] ?>" class="tab-pane fade in">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_email.php'); ?>
        </div>
         <div id="discount_email_template_<?php echo $reviewmail['id'] ?>" class="tab-pane fade in">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_discount_email.php'); ?>
        </div>
        
    </div>
    <?php if (isset($newAddition) && $newAddition==true) { ?>
   <script type="text/javascript">
      <?php foreach ($languages as $language) { ?>
         $('#message_<?php echo $reviewmail['id']; ?>_<?php echo $language['language_id']; ?>').summernote({
               height: 320
         });
      <?php } ?>

       <?php foreach ($languages as $discount_language) { ?>
         $('#messageD_<?php echo $reviewmail['id']; ?>_<?php echo $discount_language['language_id']; ?>').summernote({
               height: 320
         });
      <?php } ?>
      selectorsForDiscount();
   </script>
   <?php } ?>
</div>		
</script>
