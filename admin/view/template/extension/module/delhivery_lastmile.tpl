<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
            <button type="submit" form="form-convead" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
			   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
		    </ul>
		</div>
	</div>

	<div class="container-fluid">
		<!-- <?php if (curl_exec == false ) { ?>
			<div class="alert alert-warning">
				<i class="fa fa-check-circle"></i>
				<?php echo $curl_disable; ?>
			</div>
		<?php } ?> -->
		<?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
		<?php if ($success_msg ) { ?>
			<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success_msg; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-convead" class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
							<select name="delhivery_lastmile_status" id="input-status" class="form-control">
								<?php if ($delhivery_lastmile_status ) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>			
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-is_production"><?php echo $entry_is_production; ?></label>
						<div class="col-sm-10">
							<select name="delhivery_lastmile_is_production" id="input-is_production" class="form-control">
								<?php if ($delhivery_lastmile_is_production ) { ?>
									<option value="1" selected="selected"><?php echo $text_yes; ?></option>
									<option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_yes; ?></option>
									<option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
							</select>
						</div>			
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-method_name"><?php echo $entry_method_name; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_method_name" id="input-method_name" class="form-control" type="text" value="<?php echo $delhivery_lastmile_method_name; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-carrier_title"><?php echo $entry_carrier_title; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_carrier_title" id="input-carrier_title" class="form-control" type="text" value="<?php echo $delhivery_lastmile_carrier_title; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-login_id"><?php echo $entry_login_id; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_login_id" id="input-login_id" class="form-control" type="text" value="<?php echo $delhivery_lastmile_login_id; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-client_id"><?php echo $entry_client_id; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_client_id" id="input-client_id" class="form-control" type="text" value="<?php echo $delhivery_lastmile_client_id; ?>">                
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-license_key"><?php echo $entry_license_key; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_license_key" id="input-license_key" class="form-control" type="text" value="<?php echo $delhivery_lastmile_license_key; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-auto_manifest"><?php echo $entry_auto_manifest; ?></label>
						<div class="col-sm-10">
							<select name="delhivery_lastmile_auto_manifest" id="input-auto_manifest" class="form-control">
								<?php if ($delhivery_lastmile_auto_manifest ) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>			
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-debug_mode"><?php echo $entry_debug_mode; ?></label>
						<div class="col-sm-10">
							<select name="delhivery_lastmile_debug_mode" id="input-debug_mode" class="form-control">
								<?php if ($delhivery_lastmile_debug_mode ) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>			
					</div>
					
					
					<!-- <div class="form-group">
						<label class="col-sm-2 control-label" for="input-error_message"><?php echo $entry_error_message; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_error_message" id="input-error_message" class="form-control" type="text" value="<?php echo $delhivery_lastmile_error_message; ?>">                
						</div>
					</div>
					-->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-cod_method"><?php echo $entry_cod_method; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_cod_method" id="input-cod_method" class="form-control" type="text" value="<?php echo $delhivery_lastmile_cod_method; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-return_address"><?php echo $entry_return_address; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_return_address" id="input-return_address" class="form-control" type="text" value="<?php echo $delhivery_lastmile_return_address; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="entry-store_name"> Store Name </label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_store_name" id="entry-store_name" class="form-control" type="text" value="<?php echo $delhivery_lastmile_store_name; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-store_address"> Store Address</label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_store_address" id="input-store_address" class="form-control" type="text" value="<?php echo $delhivery_lastmile_store_address; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-gst_no"><?php echo $entry_gst_no; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_gst_no" id="input-gst_no" class="form-control" type="text" value="<?php echo $delhivery_lastmile_gst_no; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-consignee_tin"><?php echo $entry_consignee_tin; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_consignee_tin" id="input-consignee_tin" class="form-control" type="text" value="<?php echo $delhivery_lastmile_consignee_tin; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-cst_no"><?php echo $entry_cst_no; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_cst_no" id="input-cst_no" class="form-control" type="text" value="<?php echo $delhivery_lastmile_cst_no; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-sale_tax_form"><?php echo $entry_sale_tax_form; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_sale_tax_form" id="input-sale_tax_form" class="form-control" type="text" value="<?php echo $delhivery_lastmile_sale_tax_form; ?>">                
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-heavy_shipment"><?php echo $entry_heavy_shipment; ?></label>
						<div class="col-sm-10">
							<input name="delhivery_lastmile_heavy_shipment" id="input-heavy_shipment" class="form-control" type="text" value="<?php echo $delhivery_lastmile_heavy_shipment; ?>">                
						</div>
					</div>
					
					
				</form>
			</div>
		</div>
	</div>

</div>

<?php echo $footer; ?>