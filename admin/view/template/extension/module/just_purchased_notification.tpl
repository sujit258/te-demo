<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-jpn" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
	  </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($update) { ?>
    <div class="alert alert-info"><i class="fa fa-information-circle"></i> <?php echo $update; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-jpn" class="form-horizontal">
			<ul class="nav nav-tabs" id="tabs">
				<li class="active"><a href="#tab-setting" data-toggle="tab"><i class="fa fa-fw fa-wrench"></i> <?php echo $tab_general; ?></a></li>
				<li><a href="#tab-message" data-toggle="tab"><i class="fa fa-fw fa-file-text-o"></i> <?php echo $tab_message; ?></a></li>
				<li><a href="#tab-notification-options" data-toggle="tab"><i class="fa fa-fw fa-check-square-o"></i> <?php echo $tab_notification_options; ?></a></li>
				<li><a href="#tab-help" data-toggle="tab"><i class="fa fa-fw fa-question"></i> <?php echo $tab_help; ?></a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tab-setting"> 
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
							<select name="just_purchased_notification_status" id="input-status" class="form-control">
								<?php if ($just_purchased_notification_status) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>	
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-limit"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_limit; ?>"><?php echo $entry_limit; ?></span></label>
						<div class="col-sm-10">
							<input type="text" name="just_purchased_notification_limit" value="<?php echo $just_purchased_notification_limit; ?>" placeholder="<?php echo $entry_limit; ?>" id="input-limit" class="form-control" />
							  <?php if ($error_limit) { ?>
							  <div class="text-danger"><?php echo $error_limit; ?></div>
							  <?php } ?>						
						</div>
					</div>	
					<div class="form-group required">
						  <label class="col-sm-2 control-label" for="order_status"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_order_status; ?>"><?php echo $entry_order_status; ?></span></label>
						  <div class="col-sm-10">
							<div class="well well-sm" style="height: 150px; overflow: auto;">
							  <?php foreach ($order_statuses as $order_status) { ?>
							  <div class="checkbox">
								<label>
								  <?php if (in_array($order_status['order_status_id'], $just_purchased_notification_order_status)) { ?>
								  <input type="checkbox" name="just_purchased_notification_order_status[]" value="<?php echo $order_status['order_status_id']; ?>" checked="checked" />
								  <?php echo $order_status['name']; ?>
								  <?php } else { ?>
								  <input type="checkbox" name="just_purchased_notification_order_status[]" value="<?php echo $order_status['order_status_id']; ?>" />
								  <?php echo $order_status['name']; ?>
								  <?php } ?>
								</label>
							  </div>
							  <?php } ?>
							</div>
							<?php if ($error_order_status) { ?>
							<div class="text-danger"><?php echo $error_order_status; ?></div>
							<?php } ?>
						  </div>
					</div>					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-shuffle"><?php echo $entry_shuffle; ?></label>
						<div class="col-sm-10">
							<select name="just_purchased_notification_shuffle" id="input-shuffle" class="form-control">
								<?php if ($just_purchased_notification_shuffle) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>					
					<div class="form-group cache-area">
						<label class="col-sm-2 control-label" for="input-cache"><?php echo $entry_cache; ?></label>
						<div class="col-sm-10">
							<select name="just_purchased_notification_cache" id="input-cache" class="form-control">
								<?php if ($just_purchased_notification_cache) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
							<br />
							<a id="clear-cache" class="btn btn-block btn-primary" data-loading-text="<?php echo $text_loading ; ?>"><i class="fa fa-fw fa-trash-o"></i> <?php echo $button_clear_cache; ?></a>
						</div>
					</div>	
				</div>			
				
				<div class="tab-pane" id="tab-message">
					<div class="tab-content">
						<ul class="nav nav-tabs no-promote" id="languages">
							<?php foreach ($languages as $language) { ?>
							<li><a data-toggle="tab" href="#language-<?php echo $language['language_id']; ?>"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
							<?php } ?>
						</ul>
							
						<?php foreach ($languages as $language) { ?>
						<div id="language-<?php echo $language['language_id']; ?>" class="tab-pane">
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="input-message-<?php echo $language['language_id']; ?>"><?php echo $entry_message; ?></span></label>
								<div class="col-sm-10">
									<textarea name="just_purchased_notification_localisation[<?php echo $language['language_id']; ?>][message]" placeholder="<?php echo $entry_message; ?>" id="input-message-<?php echo $language['language_id']; ?>" data-toggle="popover" data-html="true" data-placement="top" title="<?php echo $help_message_title; ?>" data-content="<?php echo $help_message; ?>" data-trigger="focus" class="form-control"><?php echo isset($just_purchased_notification_localisation[$language['language_id']]) ? $just_purchased_notification_localisation[$language['language_id']]['message'] : ''; ?></textarea>
									<?php if (isset($error_message[$language['language_id']])) { ?>
									<div class="text-danger"><?php echo $error_message[$language['language_id']]; ?></div>
									<?php } ?>
								</div>
							</div>
							<div class="form-group required time-ago">
								<label class="col-sm-2 control-label" for="input-time-ago-minute-<?php echo $language['language_id']; ?>"><?php echo $entry_time_ago_minute; ?></label>
								<div class="col-sm-10">
									<input name="just_purchased_notification_localisation[<?php echo $language['language_id']; ?>][time_ago_minute]" placeholder="<?php echo $entry_time_ago_minute; ?>" id="input-time-ago-minute-<?php echo $language['language_id']; ?>" value="<?php echo isset($just_purchased_notification_localisation[$language['language_id']]) ? $just_purchased_notification_localisation[$language['language_id']]['time_ago_minute'] : ''; ?>" data-toggle="popover" data-html="true" data-placement="top" data-content="<?php echo $help_time_ago_minute; ?>" data-trigger="focus" class="form-control" />
									<?php if (isset($error_time_ago_minute[$language['language_id']])) { ?>
									<div class="text-danger"><?php echo $error_time_ago_minute[$language['language_id']]; ?></div>
									<?php } ?>
								</div>
							</div>
							<div class="form-group required time-ago">
								<label class="col-sm-2 control-label" for="input-time-ago-hour-<?php echo $language['language_id']; ?>"><?php echo $entry_time_ago_hour; ?></label>
								<div class="col-sm-10">
									<input name="just_purchased_notification_localisation[<?php echo $language['language_id']; ?>][time_ago_hour]" placeholder="<?php echo $entry_time_ago_hour; ?>" id="input-time-ago-hour-<?php echo $language['language_id']; ?>" value="<?php echo isset($just_purchased_notification_localisation[$language['language_id']]) ? $just_purchased_notification_localisation[$language['language_id']]['time_ago_hour'] : ''; ?>" data-toggle="popover" data-html="true" data-placement="top" data-content="<?php echo $help_time_ago_hour; ?>" data-trigger="focus" class="form-control" />
									<?php if (isset($error_time_ago_hour[$language['language_id']])) { ?>
									<div class="text-danger"><?php echo $error_time_ago_hour[$language['language_id']]; ?></div>
									<?php } ?>
								</div>
							</div>	
							<div class="form-group required time-ago">
								<label class="col-sm-2 control-label" for="input-time-ago-day-<?php echo $language['language_id']; ?>"><?php echo $entry_time_ago_day; ?></label>
								<div class="col-sm-10">
									<input name="just_purchased_notification_localisation[<?php echo $language['language_id']; ?>][time_ago_day]" placeholder="<?php echo $entry_time_ago_day; ?>" id="input-time-ago-day-<?php echo $language['language_id']; ?>" value="<?php echo isset($just_purchased_notification_localisation[$language['language_id']]) ? $just_purchased_notification_localisation[$language['language_id']]['time_ago_day'] : ''; ?>" data-toggle="popover" data-html="true" data-placement="top" data-content="<?php echo $help_time_ago_day; ?>" data-trigger="focus" class="form-control" />
									<?php if (isset($error_time_ago_day[$language['language_id']])) { ?>
									<div class="text-danger"><?php echo $error_time_ago_day[$language['language_id']]; ?></div>
									<?php } ?>
								</div>
							</div>							
						</div>
						<?php } ?>	
							
						<div class="form-group time-ago">
							<label class="col-sm-2 control-label" for="input-hide-older"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_hide_older; ?>"><?php echo $entry_hide_older; ?></span></label>
							<div class="col-sm-10">
								<div class="input-group">
									<input type="text" name="just_purchased_notification_hide_older" value="<?php echo $just_purchased_notification_hide_older; ?>" placeholder="<?php echo $entry_hide_older; ?>" id="input-hide-older" class="form-control" />
									<span class="input-group-addon"><?php echo $text_hours; ?></span>
								</div>						
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-time-ago"><?php echo $entry_time_ago; ?></label>
							<div class="col-sm-10">
								<select name="just_purchased_notification_time_ago" id="input-time-ago" class="form-control">
									<?php if ($just_purchased_notification_time_ago) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
									<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>						
					</div>
				</div>					
				
				<div class="tab-pane" id="tab-notification-options">
					<div class="tab-content">
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
							<div class="col-sm-10">
							  <div class="row">
								<div class="col-sm-6">
								  <input type="text" name="just_purchased_notification_image_width" placeholder="<?php echo $entry_width; ?>" value="<?php echo $just_purchased_notification_image_width; ?>"  id="input-image-width" class="form-control" />
								</div>
								<div class="col-sm-6">
								  <input type="text" name="just_purchased_notification_image_height" placeholder="<?php echo $entry_height; ?>" value="<?php echo $just_purchased_notification_image_height; ?>" id="input-image-height" class="form-control" />
								</div>
							  </div>
							  <?php if ($error_image) { ?>
							  <div class="text-danger"><?php echo $error_image; ?></div>
							  <?php } ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-alert-type"><?php echo $entry_alert_type; ?></label>
							<div class="col-sm-10">
								<select name="just_purchased_notification_alert_type" id="input-alert-type" class="form-control">
									<option value="success" <?php echo ($just_purchased_notification_alert_type == 'success') ? 'selected="selected"' : ''; ?>><?php echo $text_alert_success; ?></option>
									<option value="info" <?php echo ($just_purchased_notification_alert_type == 'info') ? 'selected="selected"' : ''; ?>><?php echo $text_alert_info; ?></option>
									<option value="warning" <?php echo ($just_purchased_notification_alert_type == 'warning') ? 'selected="selected"' : ''; ?>><?php echo $text_alert_warning; ?></option>
									<option value="danger" <?php echo ($just_purchased_notification_alert_type == 'danger') ? 'selected="selected"' : ''; ?>><?php echo $text_alert_danger; ?></option>
									<option value="custom" <?php echo ($just_purchased_notification_alert_type == 'custom') ? 'selected="selected"' : ''; ?>><?php echo $text_alert_custom; ?></option>
								</select>
							</div>
						</div>	
						<div class="form-group required alert-custom">
							<label class="col-sm-2 control-label" for="input-background-color"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_background_color; ?>"><?php echo $entry_background_color; ?></span></label>
							<div class="col-sm-10">
								<div class="input-group">
									<input type="text" name="just_purchased_notification_background_color" value="<?php echo $just_purchased_notification_background_color; ?>" placeholder="<?php echo $entry_background_color; ?>" id="input-background-color" class="form-control choose-color" />
									<span class="input-group-addon"></span>
								</div>
							    <?php if ($error_background_color) { ?>
							    <div class="text-danger"><?php echo $error_background_color; ?></div>
							    <?php } ?>								
							</div>
						</div>
						<div class="form-group required alert-custom">
							<label class="col-sm-2 control-label" for="input-border-color"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_border_color; ?>"><?php echo $entry_border_color; ?></span></label>
							<div class="col-sm-10">
								<div class="input-group">
									<input type="text" name="just_purchased_notification_border_color" value="<?php echo $just_purchased_notification_border_color; ?>" placeholder="<?php echo $entry_border_color; ?>" id="input-border-color" class="form-control choose-color" />
									<span class="input-group-addon"></span>
								</div>
							    <?php if ($error_border_color) { ?>
							    <div class="text-danger"><?php echo $error_border_color; ?></div>
							    <?php } ?>								
							</div>
						</div>
						<div class="form-group required alert-custom">
							<label class="col-sm-2 control-label" for="input-text-color"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_text_color; ?>"><?php echo $entry_text_color; ?></span></label>
							<div class="col-sm-10">
								<div class="input-group">
									<input type="text" name="just_purchased_notification_text_color" value="<?php echo $just_purchased_notification_text_color; ?>" placeholder="<?php echo $entry_text_color; ?>" id="input-text-color" class="form-control choose-color" />
									<span class="input-group-addon"></span>
								</div>	
							    <?php if ($error_text_color) { ?>
							    <div class="text-danger"><?php echo $error_text_color; ?></div>
							    <?php } ?>								
							</div>
						</div>	
						<div class="form-group required alert-custom">
							<label class="col-sm-2 control-label" for="input-text-color"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_link_color; ?>"><?php echo $entry_link_color; ?></span></label>
							<div class="col-sm-10">
								<div class="input-group">
									<input type="text" name="just_purchased_notification_link_color" value="<?php echo $just_purchased_notification_link_color; ?>" placeholder="<?php echo $entry_link_color; ?>" id="input-link-color" class="form-control choose-color" />
									<span class="input-group-addon"></span>
								</div>	
								<?php if ($error_link_color) { ?>
							    <div class="text-danger"><?php echo $error_link_color; ?></div>
							    <?php } ?>
							</div>
						</div>	
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-placement"><?php echo $entry_placement; ?></label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-6">
										<select name="just_purchased_notification_placement_from" id="input-placement-from" class="form-control">
											<?php if ($just_purchased_notification_placement_from == 'top') { ?>
											<option value="top" selected="selected"><?php echo $text_top; ?></option>
											<option value="bottom"><?php echo $text_bottom; ?></option>
											<?php } else { ?>
											<option value="top"><?php echo $text_top; ?></option>
											<option value="bottom" selected="selected"><?php echo $text_bottom; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-sm-6">
										<select name="just_purchased_notification_placement_align" id="input-placement-align" class="form-control">
											<?php if ($just_purchased_notification_placement_align == 'right') { ?>
											<option value="left"><?php echo $text_left; ?></option>
											<option value="center"><?php echo $text_center; ?></option>
											<option value="right" selected="selected"><?php echo $text_right; ?></option>
											<?php } elseif ($just_purchased_notification_placement_align == 'center') { ?>
											<option value="left"><?php echo $text_left; ?></option>
											<option value="center" selected="selected"><?php echo $text_center; ?></option>
											<option value="right"><?php echo $text_right; ?></option>
											<?php } else { ?>
											<option value="left" selected="selected"><?php echo $text_left; ?></option>
											<option value="center"><?php echo $text_center; ?></option>
											<option value="right"><?php echo $text_right; ?></option>											
											<?php } ?>
										</select>
									</div>									
								</div>	
							</div>
						</div>	
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-allow-dismiss"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_allow_dismiss; ?>"><?php echo $entry_allow_dismiss; ?></span></label>
							<div class="col-sm-10">
								<select name="just_purchased_notification_allow_dismiss" id="input-allow-dismiss" class="form-control">
									<?php if ($just_purchased_notification_allow_dismiss) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
									<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-show-progressbar"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_show_progressbar; ?>"><?php echo $entry_show_progressbar; ?></span></label>
							<div class="col-sm-10">
								<select name="just_purchased_notification_show_progressbar" id="input-show-progressbar" class="form-control">
									<?php if ($just_purchased_notification_show_progressbar) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
									<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>					
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-animate-enter"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_animate_enter; ?>"><?php echo $entry_animate_enter; ?></span></label>
							<div class="col-sm-10">
								<select name="just_purchased_notification_animate_enter" id="input-animate-enter" class="form-control">
									<?php foreach($animations_enter as $animate_enter) { ?>
									<?php if ($just_purchased_notification_animate_enter == $animate_enter) { ?>
									<option value="<?php echo $animate_enter; ?>" selected="selected"><?php echo $animate_enter; ?></option>
									<?php } else { ?>
									<option value="<?php echo $animate_enter; ?>"><?php echo $animate_enter; ?></option>
									<?php } ?>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-animate-exit"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_animate_exit; ?>"><?php echo $entry_animate_exit; ?></span></label>
							<div class="col-sm-10">
								<select name="just_purchased_notification_animate_exit" id="input-animate-exit" class="form-control">
									<?php foreach($animations_exit as $animate_exit) { ?>
									<?php if ($just_purchased_notification_animate_exit == $animate_exit) { ?>
									<option value="<?php echo $animate_exit; ?>" selected="selected"><?php echo $animate_exit; ?></option>
									<?php } else { ?>
									<option value="<?php echo $animate_exit; ?>"><?php echo $animate_exit; ?></option>
									<?php } ?>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-delay"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_delay; ?>"><?php echo $entry_delay; ?></span></label>
							<div class="col-sm-10">
								<div class="input-group">	
									<input type="text" name="just_purchased_notification_delay" value="<?php echo $just_purchased_notification_delay; ?>" placeholder="<?php echo $entry_delay; ?>" id="input-delay" class="form-control" />
									<span class="input-group-addon"><?php echo $text_seconds; ?></span>
								</div>	
								<?php if ($error_delay) { ?>
								<div class="text-danger"><?php echo $error_delay; ?></div>
								<?php } ?>						
							</div>
						</div>	
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-delay"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_timeout; ?>"><?php echo $entry_timeout; ?></span></label>
							<div class="col-sm-10">
								<div class="input-group">	
									<input type="text" name="just_purchased_notification_timeout" value="<?php echo $just_purchased_notification_timeout; ?>" placeholder="<?php echo $entry_timeout; ?>" id="input-timeout" class="form-control" />
									<span class="input-group-addon"><?php echo $text_seconds; ?></span>
								</div>	
								<?php if ($error_timeout) { ?>
								<div class="text-danger"><?php echo $error_timeout; ?></div>
								<?php } ?>						
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="input-zindex"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_zindex; ?>"><?php echo $entry_zindex; ?></span></label>
							<div class="col-sm-10">
								<input type="text" name="just_purchased_notification_zindex" value="<?php echo $just_purchased_notification_zindex; ?>" placeholder="<?php echo $entry_zindex; ?>" id="input-zindex" class="form-control" />						
							</div>
						</div>							
					</div>
				</div>					
				
				<div class="tab-pane" id="tab-help">
					<div class="tab-content">
						Change Log and HELP Guide is available : <a href="http://www.oc-extensions.com/Just-Purchased-Notification-Opencart-2.x" target="blank">HERE</a><br /><br />
						If you need support email us at <strong>support@oc-extensions.com</strong> (Please first read help guide) 				
					</div>
				</div>
			</div>
		</form>	
    </div>
  </div>
<script type="text/javascript"><!--
initLivePreviewLayout();

$('.choose-color').colorpicker().on('changeColor', function(ev){
	colorLivePreview($(this), ev.color.toHex());
});

function colorLivePreview(object, color) {
	color = typeof(color) != 'undefined' ? color : object.val();
	
	// set input value => color hex
	object.val(color);
	
	// mark span with choosed color
	object.next().css('background-color', color);
}

function initLivePreviewLayout() {
	$('.choose-color').each(function() {
		colorLivePreview($(this));
	});
}

$('#clear-cache').on('click', function(){
	$.ajax({
		url: 'index.php?route=module/just_purchased_notification/clearcache&token=<?php echo $token; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#clear-cache').button('loading');
		},
		success: function(json) {
			$('.cache-area .alert').remove();
			
			$('#clear-cache').button('reset');

			if (json['success']) {
				$('#clear-cache').before('<div class="alert alert-success"><i class="fa fa-fw fa-check-circle"></i> ' + json['success'] + '</div>');
				
				setTimeout(function(){
					$('.cache-area .alert').remove();
				}, 3000)
			}
		}
	});
});

$('select[name=\'just_purchased_notification_cache\']').on('change', function(){
	if ($(this).val() == 1) {
		$('#clear-cache').show();
	} else {
		$('#clear-cache').hide();
	}
});

$('select[name=\'just_purchased_notification_cache\']').trigger('change');

$('select[name=\'just_purchased_notification_time_ago\']').on('change', function(){
	if ($(this).val() == 1) {
		$('.time-ago').show();
	} else {
		$('.time-ago').hide();
	}
});

$('select[name=\'just_purchased_notification_time_ago\']').trigger('change');

$('select[name=\'just_purchased_notification_alert_type\']').on('change', function(){
	if ($(this).val() == 'custom') {
		$('.alert-custom').show();
	} else {
		$('.alert-custom').hide();
	}
});

$('select[name=\'just_purchased_notification_alert_type\']').trigger('change');

$('#languages li:first-child a').tab('show');

$('[data-toggle="popover"]').popover();
//--></script></div>
<?php echo $footer; ?>