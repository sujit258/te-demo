<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-pincode" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		 <a href="<?php echo $support; ?>" data-toggle="tooltip" title="<?php echo $button_support; ?>" class="btn btn-default"><i class="fa fa-life-ring fa-lg"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>Pincode Module</h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-pincode" class="form-horizontal">
         <!-- <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="pincode_status" id="input-status" class="form-control">
                <?php if ($pincode_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>-->
        </form>
		
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6" style = "width:20% !important;">
				<div class="tile">
					<div class="tile-heading"><?php echo $text_insert_pincode?></div>
					<div class="tile-body" align="center"><a href = '<?php echo $insert; ?>'><i class="fa fa-insert" style = "font-size: 101px; !important;"></i></div>
					<div class="tile-footer"><a href="<?php echo $insert; ?>"><?php echo $text_insert_pincode?>...</a></div>
				</div>
			</div>
		 <div class="col-lg-3 col-md-3 col-sm-6" style = "width:20% !important;">
			  <div class="tile">
				  <div class="tile-heading"><?php echo $text_list_pincode?></div>
				  <div class="tile-body" align="center"><a href = '<?php echo $getlist; ?>'><i class="fa fa-list" style = "font-size: 101px; !important;"></i></a></div>
				  <div class="tile-footer"><a href="<?php echo $getlist; ?>"><?php echo $text_list_pincode?>...</a></div>
			  </div>
		</div>
        <div class="col-lg-3 col-md-3 col-sm-6" style = "width:20% !important;">
			<div class="tile">
			  <div class="tile-heading"><?php echo $text_insert_delivery_options?></div>
			  <div class="tile-body" align="center"><a href = '<?php echo $Insert_delivery; ?>'><i class="fa fa-truck" style = "font-size: 101px; !important;"></a></i></div>
			  <div class="tile-footer"><a href = '<?php echo $Insert_delivery; ?>'><?php echo $text_insert_delivery_options?>...</a></div>
			</div>
		</div>
        <div class="col-lg-3 col-md-3 col-sm-6" style = "width:20% !important;">
			<div class="tile">
				<div class="tile-heading"><?php echo $text_delhivery_option_list?></div>
				<div class="tile-body" align="center"><a href = '<?php echo $delivery_getlist; ?>'><i class="fa fa-list" style = "font-size: 101px; !important;"></i></a></div>
				<div class="tile-footer"><a href = '<?php echo $delivery_getlist; ?>'><?php echo $text_delhivery_option_list?>...</a></div>
			</div>
		</div>
		 <div class="col-lg-3 col-md-3 col-sm-6" style = "width:20% !important;">
			  <div class="tile">
				  <div class="tile-heading"><?php echo $text_setting?></div>
				  <div class="tile-body" align="center"><a href = '<?php echo $setting; ?>'><i class="fa fa-setting" style = "font-size: 101px; !important;"></i></a></div>
				  <div class="tile-footer"><a href="<?php echo $setting; ?>"><?php echo $text_setting?>...</a></div>
			  </div>
		</div>
     </div>
	</div>
    </div>
  </div>
</div>
<style>
	.fa-insert:before{content:"\f196"}
	.fa-list:before{content:"\f03a"}
	.fa-setting:before{content:"\f0ad"}

</style>
<?php echo $footer; ?>