<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-parcelamento" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-parcelamento" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $entry_text_list; ?></label>
                        <div class="col-sm-9">
                            <textarea name="joseanmatias_preco_cupom_text_list" rows="2" cols="100" class="form-control"><?php echo $joseanmatias_preco_cupom_text_list; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $entry_text_page; ?></label>
                        <div class="col-sm-9">
                            <textarea name="joseanmatias_preco_cupom_text_page" rows="2" cols="100" class="form-control"><?php echo $joseanmatias_preco_cupom_text_page; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $entry_showlist; ?></label>
                        <div class="col-sm-9">
                            <div class="btn-group" data-toggle="buttons">
                                <?php if ($joseanmatias_preco_cupom_showlist) { ?>
                                    <label class="btn btn-default active">
                                        <input type="radio" name="joseanmatias_preco_cupom_showlist" value="1" checked="checked" /> <?php echo $text_yes; ?>
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="joseanmatias_preco_cupom_showlist" value="0" /> <?php echo $text_no; ?>
                                    </label>
                                <?php } else { ?>
                                    <label class="btn btn-default">
                                        <input type="radio" name="joseanmatias_preco_cupom_showlist" value="1" /> <?php echo $text_yes; ?>
                                    </label>
                                    <label class="btn btn-default active">
                                        <input type="radio" name="joseanmatias_preco_cupom_showlist" value="0" checked="checked" /> <?php echo $text_no; ?>
                                    </label>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $entry_outofstock; ?></label>
                        <div class="col-sm-9">
                            <div class="btn-group" data-toggle="buttons">
                                <?php if ($joseanmatias_preco_cupom_outofstock) { ?>
                                    <label class="btn btn-default active">
                                        <input type="radio" name="joseanmatias_preco_cupom_outofstock" value="1" checked="checked" /> <?php echo $text_yes; ?>
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="joseanmatias_preco_cupom_outofstock" value="0" /> <?php echo $text_no; ?>
                                    </label>
                                <?php } else { ?>
                                    <label class="btn btn-default">
                                        <input type="radio" name="joseanmatias_preco_cupom_outofstock" value="1" /> <?php echo $text_yes; ?>
                                    </label>
                                    <label class="btn btn-default active">
                                        <input type="radio" name="joseanmatias_preco_cupom_outofstock" value="0" checked="checked" /> <?php echo $text_no; ?>
                                    </label>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $entry_status; ?></label>
                        <div class="col-sm-9">
                            <div class="btn-group" data-toggle="buttons">
                                <?php if ($joseanmatias_preco_cupom_status) { ?>
                                    <label class="btn btn-default active">
                                        <input type="radio" name="joseanmatias_preco_cupom_status" value="1" checked="checked" /> <?php echo $text_enabled; ?>
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="joseanmatias_preco_cupom_status" value="0" /> <?php echo $text_disabled; ?>
                                    </label>
                                <?php } else { ?>
                                    <label class="btn btn-default">
                                        <input type="radio" name="joseanmatias_preco_cupom_status" value="1" /> <?php echo $text_enabled; ?>
                                    </label>
                                    <label class="btn btn-default active">
                                        <input type="radio" name="joseanmatias_preco_cupom_status" value="0" checked="checked" /> <?php echo $text_disabled; ?>
                                    </label>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>