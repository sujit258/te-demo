<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-account" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $export_format; ?></label>
                        <div class="col-sm-10">
                            <select name="export_format" id="input-export-format" class="form-control">
                                <option value="csv" selected="selected">CSV</option>
                                <option value="txt">TXT</option>
                            </select>

                        </div>
                    </div>

                        <div style="display: none" class="form-group separator-field">
                            <label class="col-sm-2 control-label" for="input-name1"><?php echo $separator_text; ?></label>
                            <div class="col-sm-10">
                                <input name="export_separator" value=";" placeholder="<?php echo $separator_text; ?>" id="input-export-separator" class="form-control" type="text">
                            </div>
                        </div>

                        <div style="text-align: right; margin-top: 50px;" class="col-sm-12">
                            <button type="button" id="button-export" data-loading-text="Export..." class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo $export_btn_text; ?> </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div style="text-align: center;">
            <b>Author:</b> Vladislav Emashov<br/>
            <b>Skype:</b> vlad@emashov.me<br/>
            <b>Email:</b> <a href="mailto:vlademashov@gmail.com">vlademashov@gmail.com</a><br/>
        </div>
    </div>
</div>



<script>

    var use_custom_separator = false;

    $('#input-export-format').on('change', function (e) {

        if($("#input-export-format").val() == "txt"){
            $('.separator-field').show();
            use_custom_separator = true;
        }else{
            $('.separator-field').hide();
            use_custom_separator = false;
        }

    });

    $("#button-export").click(function () {
        var url = "<?php echo $iframe_url; ?>";

        url += "&format="+$("#input-export-format").val();

        if(use_custom_separator == true) url += "&separator="+$("#input-export-separator").val();

       $('<iframe style="display: none" src="'+url+'" frameborder="0" scrolling="no" id="myFrame"></iframe>').appendTo('body');

    });
</script>
<?php echo $footer; ?>