<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			  <h1><img src="view/image/download.jpg" alt="" /> <?php echo $heading_title; ?></h1>
			 <div class="pull-right"><a onclick="$('#form').submit();" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></a><a href="<?php echo $cancel;?>" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Cancel"><i class="fa fa-reply"></i></a></div>
				<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			  </ul> 
		</div>
	  </div>
	  
	   <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?> </h3>
		</div>
    
	<?php if (isset($product_counter) || isset($product_failed) ) {
		$count=$product_counter+$product_failed;
    ?>
	<div class="success"><?php echo $product_counter ." Products are uploaded out of " .$count; ?></div>
	<?php } ?>
	
    <div class="panel-body">


    <!--<div class="heading">
      <h1><img src="../image/data/cod.jpg" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a style = "background-color: #36C705;"onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a style = "background-color: #F80642;" href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>-->
 
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
         <tr>
              <td><?php echo $entry_category; ?></td>
              <td><input type="text" name="category" value="" id="input-category" class="form-control" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                  <?php  foreach ($product_categories as $product_category) { ?>
                  <div id="product-category<?php echo $product_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_category['name']; ?>
                    <input type="hidden" name="product_cod_product_category[]" value="<?php echo $product_category['category_id']; ?>" />
                  </div>
                  <?php } ?>
                </div></td>
            </tr> 
		  <tr>
            <td><?php echo $entry_product; ?></td>
            <td><input type="text" name="product" value="" id="input-product" class="form-control" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><div id="featured-product" class="well well-sm" style="height: 150px; overflow: auto;">
                <?php foreach ($products as $product) { ?>
             
                <div id="featured-product<?php echo $product['product_id']; ?>" ><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                  <input type="hidden" value="<?php echo $product['product_id']; ?>" />
                </div>
                <?php } ?>
              </div>
              <input type="hidden" name="product_cod" value="<?php echo $product_cod; ?>" /></td>
          </tr>
		  <tr>
			<td><font color = "#F80642">COD Not Available Message : </font></td>
			<td><textarea style="margin: 1.8181817531585693px; height: 145px; width: 344px;" name = "cod_product_error_message" rows = "10" cols = "30"><?php echo $cod_product_error_message; ?></textarea> </td>
		  </tr>
        </table>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('#featured-product' + item['value']).remove();
		
		$('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" value="' + item['value'] + '" /></div>');	
	
		data = $.map($('#featured-product input'), function(element) {
			return $(element).attr('value');
		});
				$('input[name=\'product_cod\']').attr('value', data.join());		
		
	}	
});

$('#featured-product').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();

	data = $.map($('#featured-product input'), function(element) {
		return $(element).attr('value');
	});
					
	$('input[name=\'product_cod\']').attr('value', data.join());	
});
//--></script> 
<script>

$('input[name=\'category\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'category\']').val('');
		
		$('#product-category' + item['value']).remove();
		
		$('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_cod_product_category[]" value="' + item['value'] + '" /></div>');	
	}
});

$('#product-category').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

</script>
<?php echo $footer; ?>