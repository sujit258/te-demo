<title>Print Label</title>
<?php if ($delhiveryLastmile_manageLabel) { ?>
<?php foreach ($delhiveryLastmile_manageLabel as $manage_lb) { ?>
	<?php if ($manage_lb['page_break']) { ?>
		<P style="page-break-before: always">
	<?php } ?>
	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<td width="162" align="center"><img src="view/image/logo.png" height="30px"></td>
			<td width="206" align="center"><img src="<?php echo $manage_lb['delhivery_logo']; ?>" height="30px"></td>
		</tr>
		<tr>
			<td><img src="<?php echo $manage_lb['barcode']; ?>" height="60px"/></td>
			<td>Order#:&nbsp;<?php echo $manage_lb['order_id']; ?></td>
		</tr>
		<tr>
			<td><strong>Shipping Address :</strong><br><?php echo $manage_lb['shipping_address']; ?></td>
			<td align="center"><strong><?php echo $manage_lb['methodcode']; ?></strong></td>
		</tr>
		<tr>
			<td><strong>Seller:</strong>&nbsp; <?php echo $manage_lb['store_name']; ?><br><?php echo $manage_lb['store_address']; ?></td>
			<td>GSTIN: <?php echo $manage_lb['gst']; ?><br>
			TIN: <?php echo $manage_lb['tin']; ?><br>
			CST: <?php echo $manage_lb['cst']; ?><br>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<table border="1" width="100%" cellspacing="0" cellpadding="3">
				<tr>
					<td><strong>Product</strong></td>
					<td><strong>Price</strong></td>
					<td><strong>Qty</strong></td>
					<td><strong>Total</strong></td>
				</tr>
				<?php foreach ($manage_lb['items'] as $manage_items) { ?>
					<tr>
						<td><?php echo $manage_items['name']; ?></td>
						<td><?php echo $manage_items['price']; ?></td>
						<td><?php echo $manage_items['qty']; ?></td>
						<td><?php echo $manage_items['total']; ?></td>
					</tr>
				<?php } ?>
				<?php foreach ($manage_lb['O_totals'] as $manage_total) { ?>
					<tr>
						<td colspan="3" align="right"><strong><?php echo $manage_total['title']; ?>&nbsp;</strong></td>
						<td>&nbsp;<?php echo $manage_total['value']; ?></td>
					</tr>
				<?php } ?>

			</table>
			</td>
		</tr>
		<tr>
			<td>Return Address:&nbsp;<?php echo $manage_lb['return_address']; ?></td>
			<td><img src="<?php echo $manage_lb['barcode']; ?>" height="60px"/></td>
		</tr>
	</table>
<?php } ?>
<?php } ?>


<style type = "text/css">
table {
  border-collapse: collapse;
  width:450px;
  
}
table, th, td {
  border: 1px solid black;
}
   <!--
      @media screen {
         table { font-family:verdana, arial, sans-serif; margin:auto; }
      }

      @media print {
          table { font-family:verdana, arial, sans-serif; float:left; }
      }
   -->
</style>

<script type="text/javascript">
<!--
window.print();
//-->
</script>