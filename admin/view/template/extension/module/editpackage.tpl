<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

	<div class="page-header">
		<div class="container-fluid">
		    <h1><?php echo $heading_title; ?></h1>
		    <ul class="breadcrumb">
			   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
		    </ul>
		</div>
	</div>

	<div class="container-fluid">
			<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
			<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $form_data.action; ?>" method="post" enctype="multipart/form-data" id="filter_forms">
					<input type="hidden" name="route" value="extension/module/manageawb" />
					<input type="hidden" name="user_token" value="<?php echo $user_token; ?>" />
					<div class="well">
						<div class="row">
							<div class="col-sm-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label" for="input-awb">WayBill No.</label>
									<input type="text" name="awb" value="<?php echo $form_data.awb; ?>" placeholder="WayBill No." id="input-awb" class="form-control" required/>
								</div>
							</div>
							<div class="col-sm-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label" for="input-phone">Phone</label>
									<input type="text" name="phone" value="<?php echo $form_data['phone']; ?>" placeholder="Phone" id="input-phone" class="form-control" />
								</div>
							</div>
							<div class="col-sm-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label" for="input-name">Name</label>
									<input type="text" name="name" value="<?php echo $form_data['name']; ?>" placeholder="Name" id="input-shipment_to" class="form-control" />
								</div>
							</div>
							<div class="col-sm-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label" for="input-address">Address</label>
									<textarea  name="address" id="input-address" class="form-control"><?php echo $form_data['address']; ?></textarea>
								</div>
							</div>
							<div class="col-sm-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label" for="input-shipment_length">Shipment Length</label>
									<input type="text" name="shipment_length" value="<?php echo $form_data['shipment_length']; ?>" placeholder="Shipment Length" id="input-shipment_length" class="form-control" />
								</div>
							</div>
							<div class="col-sm-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label" for="input-shipment_width">Shipment Width</label>
									<input type="text" name="shipment_width" value="<?php echo $form_data['shipment_width']; ?>" placeholder="Shipment Width" id="input-shipment_width" class="form-control" />
								</div>
							</div>
							<div class="col-sm-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label" for="input-shipment_height">Shipment Height</label>
									<input type="text" name="shipment_height" value="<?php echo $form_data['shipment_height']; ?>" placeholder="Shipment Height" id="input-shipment_height" class="form-control" />
								</div>
							</div>
							<div class="col-sm-8 col-md-offset-2">
								<div class="form-group">
									<label class="control-label" for="input-weight">Weight</label>
									<input type="text" name="weight" value="" placeholder="Weight" id="input-weight" class="form-control" />
								</div>
							</div>
							<div class="col-sm-8 col-md-offset-5">
								<div class="form-group">
								<br />
								<button type="submit" id="button-filter" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
								</div>
							</div>
							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>