<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $download_pincode; ?>">
					<button data-toggle="tooltip" title="<?php echo $button_download_pincode; ?>" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; <?php echo $button_download_pincode; ?></button>
				</a>
			</div>
		    <h1>Manage Pincodes</h1>
		    <ul class="breadcrumb">
			   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
		    </ul>
		</div>
	</div>

	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> Manage Pincodes</h3>
			</div>
			<div class="panel-body">
				
				<form action="<?php echo $filter_action; ?>" method="get" enctype="multipart/form-data" id="filter_forms">
					<input type="hidden" name="route" value="extension/module/managepincode" />
					<input type="hidden" name="token" value="<?php echo $token; ?>" />
					<div class="well">
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-district"><?php echo $column_district; ?></label>
									<input type="text" name="filter_district" value="<?php echo $filter_district; ?>" placeholder="<?php echo $column_district; ?>" id="input-district" class="form-control" />
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label" for="input-pin"><?php echo $column_pin; ?></label>
									<input type="text" name="filter_pin" value="<?php echo $filter_pin; ?>" placeholder="<?php echo $column_pin; ?>" id="input-pin" class="form-control" />
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="form-group">
								<br />
								<button type="submit" id="button-filter" class="btn btn-primary"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
								
								
								<a href="<?php echo $reset_filter; ?>">
									<button type="button" id="button-filter" class="btn btn-primary">Reset Filter</button>
								</a>
								
								</div>
							</div>
							
						</div>
					</div>
				</form>
				
				<form action="" method="post" enctype="multipart/form-data" id="form-mpblogcategory">
					  <div class="table-responsive">
						<table class="table table-bordered table-hover">
						  <thead>
							<tr>
								<td class="text-left">
									<?php if ($sort == 'pincode_id') { ?>
										<a href="<?php echo $sort_pincode_id; ?>" class="<?php echo $order|lower; ?>"><?php echo $column_pincode_id; ?></a>
									<?php } else { ?>
										<a href="<?php echo $sort_pincode_id; ?>"><?php echo $column_pincode_id; ?></a>
									<?php } ?>
								</td>
								<td class="text-left"><?php echo $column_district; ?></td>
								<td class="text-left"><?php echo $column_pin; ?></td>
								<td class="text-left"><?php echo $column_pre_paid; ?></td>
								<td class="text-left"><?php echo $column_cash; ?></td>
								<td class="text-left"><?php echo $column_pickup; ?></td>
								<td class="text-left"><?php echo $column_cod; ?></td>
								<td class="text-left"><?php echo $column_state_code; ?></td>
							</tr>
						  </thead>
						  <tbody>
							<?php if ($delhiveryLastmile_managePincode) { ?>
							<?php foreach ($delhiveryLastmile_managePincode as $manage_pincode) { ?>
							<tr>
								<td class="text-left"><?php echo $manage_pincode['pincode_id']; ?></td>
								<td class="text-left"><?php echo $manage_pincode['district']; ?></td>
								<td class="text-left"><?php echo $manage_pincode['pin']; ?></td>
								<td class="text-left"><?php echo $manage_pincode['pre_paid']; ?></td>
								<td class="text-left"><?php echo $manage_pincode['cash']; ?></td>
								<td class="text-left"><?php echo $manage_pincode['pickup']; ?></td>
								<td class="text-left"><?php echo $manage_pincode['cod'] ?></td>
								<td class="text-left"><?php echo $manage_pincode['state_code']; ?></td>
							</tr>
							<?php } ?>
							<?php } else { ?>
							<tr>
							  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
							</tr>
							<?php } ?>
						  </tbody>
						</table>
					  </div>
					</form>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
				
				
			</div>
		</div>
	</div>

</div>

<?php echo $footer; ?>