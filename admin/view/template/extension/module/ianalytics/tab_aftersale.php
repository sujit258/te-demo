<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" id="afterSaleTabs">               
				<li><a href="#customer-funnel" data-toggle="tab"><i class="fa fa-filter"></i>&nbsp;&nbsp;<?php echo $text_customers_funnel; ?> </a></li>
                <li><a href="#sales-report" data-toggle="tab" data-chart="sales_report"><i class="fa fa-money"></i>&nbsp;&nbsp;<?php echo $text_sales_report; ?></a></li>
                <li><a href="#ordered-products" data-toggle="tab" data-chart="products_report"><i class="fa fa-download"></i>&nbsp;&nbsp;<?php echo $text_most_ordered_product; ?></a></li>
                <li><a href="#customer-orders" data-toggle="tab" data-chart="customers_report"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo $text_customer_order; ?></a></li>
                <li><a href="#truecash-report" data-toggle="tab" data-chart="truecash_report"><i class="fa fa-trophy"></i>&nbsp;&nbsp;<?php echo $text_truecash_report; ?></a></li>
                <li><a href="#citywise-report" data-toggle="tab" data-chart="citywise_report"><i class="fa fa-building"></i>&nbsp;&nbsp;<?php echo $text_citywise_report; ?></a></li>
                <!--li><a href="#new-old-customer-report" data-toggle="tab" data-chart="new_old_customer_report"><i class="fa fa-user"></i>&nbsp;&nbsp;<?php echo $text_new_old_customer_report; ?></a></li-->
            </ul>
        </div>
        <div class="col-md-9">
			<div class="tab-content">
                <div id="customer-funnel" class="tab-pane fade">
					<div style="float:right;overflow:hidden;"><?php require('element_filter.php'); ?></div>
					<h3><?php echo $text_customers_funnel; ?></h3>
					<div class="help"><?php echo $text_co_help; ?></div>
                    <br />
                    <div class="row">
                      <div class="col-md-8">
                      	<center><div id="funnel"></div></center>
                        <?php if (isset($iAnalyticsFunnelData[2][0])) { ?>
							<?php $stage[0]=0; $stage[1]=0; $stage[2]=0; $stage[3]=0; $stage[4]=0; $stage[5]=0; $stage[6]=0; ?>
                            <?php foreach($iAnalyticsFunnelData as $j => $k): 
                                    if ($j<1) { 
                                    } else { 
                                          if ($k[0]==0) $stage[0]=$k[1];
                                          else if ($k[0]==1) $stage[1]=$k[1];
                                          else if ($k[0]==2) $stage[2]=$k[1];
                                          else if ($k[0]==3) $stage[3]=$k[1];
                                          else if ($k[0]==4) $stage[4]=$k[1]; 
                                          else if ($k[0]==5) $stage[5]=$k[1]; 
                                          else if ($k[0]==6) $stage[6]=$k[1];
                                    } 
                                endforeach; ?>
                            <script type="text/javascript">
                                var funnelData = [
                                    [<?php echo $text_first_visit; ?>, <?php echo $stage[0]; ?>],
                                    [<?php echo $text_add_to_cart; ?>, <?php echo $stage[1] ?>], 
                                    [<?php echo $text_login_register; ?>, <?php echo $stage[2] ?>], 
                                    [<?php echo $text_delivery_method; ?>, <?php echo $stage[3] ?>], 
                                    [<?php echo $text_payment_method; ?>, <?php echo $stage[4] ?>], 
                                    [<?php echo $text_confirm_order; ?>, <?php echo $stage[5] ?>],  
                                    [<?php echo $text_successful_order; ?>, <?php echo $stage[6] ?>]];
                                var chart = new FunnelChart(funnelData, 480, 500, 1/3);
                                chart.draw('#funnel', 2.5);
                            </script>
                         <?php } else {
							echo $text_no_chart_data; } ?>
                      	</div>
                      <div class="col-md-4">
                      	<div class="panel panel-info">
                        	<div class="panel-heading">
                            	<?php $var = 0; $min = 0; $max = 0;
								if (isset($iAnalyticsFunnelData) && sizeof($iAnalyticsFunnelData)>1) 									
								{
									foreach ($iAnalyticsFunnelData as $fData) {
										if ($fData[0] == 0)	$min = $fData[1];
										else if ($fData[0] == 6) $max = $fData[1];							
									}
									if ($min>0 && $max>0) {
										$var = ($max/$min)*(100); $var = number_format($var, 2);
									}
								}
								?>
                            	<h3 style="margin-top:10px;"><i class="fa fa-retweet"></i>&nbsp;<strong><?php echo $text_conversion_rate; ?> <span id="rate"><?php echo ($var>0) ? $var.'%' : 'N/A'; ?></span></strong></h3>
                                
                            </div>
                    		<div class="panel-body">
								<h6><?php echo $text_conversion_rate_bo; ?></h6>
                              	<div class="row">
                                    <div class="col-xs-5">
                                        <select id="firstConversion" class="form-control">
                                          <option value="0"><?php echo $text_first_visit; ?></option>
                                          <option value="1"><?php echo $text_add_to_cart; ?></option>
                                          <option value="2"><?php echo $text_login_register; ?></option>
                                          <option value="3"><?php echo $text_delivery_method; ?></option>
                                          <option value="4"><?php echo $text_payment_method; ?></option>
                                          <option value="5"><?php echo $text_confirm_order; ?></option>
                                          <option value="6"><?php echo $text_successful_order; ?></option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2" style="text-align:center;">
                                    	<p style="font-weight:700;padding-top:6px;font-size:15px;">vs.</p>
                                    </div>
                                    <div class="col-xs-5" style="padding-left:0">
                                        <select id="secondConversion" class="form-control">
                                          <option value="0"><?php echo $text_first_visit; ?></option>
                                          <option value="1"><?php echo $text_add_to_cart; ?></option>
                                          <option value="2"><?php echo $text_login_register; ?></option>
                                          <option value="3"><?php echo $text_delivery_method; ?></option>
                                          <option value="4"><?php echo $text_payment_method; ?></option>
                                          <option value="5"><?php echo $text_confirm_order; ?></option>
                                          <option value="6" selected='selected'><?php echo $text_successful_order; ?></option>
                                        </select>
                                    </div>
								</div>
                            </div>
                        </div>
							<script>
							$(function() {
							  $("#firstConversion, #secondConversion").change(function() {
	var result = parseFloat(parseInt($('#'+$('#secondConversion').val()).text()) * 100) / parseInt($('#'+$('#firstConversion').val()).text()).toFixed(2);
								$('#rate').text(result.toFixed(2) + '%');
							  })
							});
                            </script>
                      	<table class="table-hover table table-striped">
						<?php foreach($iAnalyticsFunnelData as $j => $k): ?>
                            <?php if ($j<1) { ?>
                                <thead>
                                    <tr>
                                      <th><?php echo $k[0]?></th>
                                      <th><?php echo $k[1]?></th>
                                    </tr>
                                </thead>
                            <?php } else { ?>
                            <tr>
                                <td><?php
                                  if ($k[0]==0) echo $text_first_visit;
                                  else if ($k[0]==1) echo $text_add_to_cart; 
                                  else if ($k[0]==2) echo $text_login_register;
                                  else if ($k[0]==3) echo $text_delivery_method; 
                                  else if ($k[0]==4) echo $text_payment_method;
                                  else if ($k[0]==5) echo $text_confirm_order;
                                  else if ($k[0]==6) echo $text_successful_order;
                                  ?></td>
                                 <td><?php
                                  	   if ($k[0]==0) echo "<div id='".$k[0]."'>".$k[1]."</div>";
                                  else if ($k[0]==1) echo "<span id='".$k[0]."'>".$k[1]."</span>";
                                  else if ($k[0]==2) echo "<span id='".$k[0]."'>".$k[1]."</span>";
                                  else if ($k[0]==3) echo "<span id='".$k[0]."'>".$k[1]."</span>"; 
                                  else if ($k[0]==4) echo "<span id='".$k[0]."'>".$k[1]."</span>";
                                  else if ($k[0]==5) echo "<span id='".$k[0]."'>".$k[1]."</span>";
                                  else if ($k[0]==6) echo "<span id='".$k[0]."'>".$k[1]."</span>";
                                  ?></td>
                            </tr>
                            <?php } ?>
                        <?php endforeach; ?>
                        </table>
                        
                      </div>
                    </div>					
					<div class="clearfix"></div>
				</div>
				<div id="sales-report" class="tab-pane fade">
                    <div style="float:right;overflow:hidden;"><?php require('report_sales_filter.php'); ?></div>
                    <h3><?php echo $text_sales; ?></h3>
                    <div class="help"><?php echo $text_sales_help; ?></div>
                    <br />
                  	<?php require_once(DIR_APPLICATION.'view/template/'.$modulePath.'/report_sales.php'); ?>   
					<div class="clearfix"></div>
				</div>
				<div id="ordered-products" class="tab-pane fade">
					<div style="float:right;overflow:hidden;"><?php require('report_product_purchased_filter.php'); ?></div>
					<h3><?php echo $text_products; ?></h3>
                    <div class="help"><?php echo $text_products_help; ?></div>
                    <br />
                  	<?php require_once(DIR_APPLICATION.'view/template/'.$modulePath.'/report_product_purchased.php'); ?>   
					<div class="clearfix"></div>
				</div>
                <div id="customer-orders" class="tab-pane fade">
					<div style="float:right;overflow:hidden;"><?php require('report_product_purchased_filter.php'); ?></div>
					<h3><?php echo $text_customers; ?></h3>
                    <div class="help"><?php echo $text_customers_help; ?></div>
                    <br />
                  	<?php require_once(DIR_APPLICATION.'view/template/'.$modulePath.'/report_customer_orders.php'); ?>   
					<div class="clearfix"></div>
				</div>
    				<div id="truecash-report" class="tab-pane fade">
					<div style="float:right;overflow:hidden;"><?php require('report_product_purchased_filter.php'); ?></div>
					<h3><?php echo $text_truecash; ?></h3>
                    <div class="help"><?php echo $text_truecash_help; ?></div>
                    <br />
                  	<?php require_once(DIR_APPLICATION.'view/template/'.$modulePath.'/report_truecash.php'); ?>   
					<div class="clearfix"></div>
				</div>
				<div id="citywise-report" class="tab-pane fade">
					<div style="float:right;overflow:hidden;"><?php require('report_product_purchased_filter.php'); ?></div>
					<h3><?php echo $text_citywise; ?></h3>
                    <div class="help"><?php echo $text_citywise_help; ?></div>
                    <br />
                  	<?php require_once(DIR_APPLICATION.'view/template/'.$modulePath.'/report_citywise.php'); ?>   
					<div class="clearfix"></div>
				</div>
				<!--<div id="new-old-customer-report" class="tab-pane fade">
					<div style="float:right;overflow:hidden;">
					    <?php require('report_product_purchased_filter.php'); ?>
					    <table cellspacing="0" cellpadding="0" style="margin-top: 10px; margin-left: 20px;">
                            <tbody>
                                <tr>
    	                            <td class="form-inline">
    	                                <label>Customer Type: </label>
					                    <select name="filterCustomertype" class="filterCustomertype form-control">
					                        <?php $filterCustomertype = (isset($_GET['filterCustomertype'])) ? $_GET['filterCustomertype'] : '0'; ?>
					                        <option value="1" <?php if($filterCustomertype == 0 || $filterCustomertype == 1) { echo 'selected'; } ?>>New Customers</option>
					                        <option value="2" <?php if($filterCustomertype == 2) { echo 'selected'; } ?>>Repeaters</option>
		                                </select>
		                            </td>
		                        </tr>
		                    </tbody>
		                </table>
					</div>
					<h3><?php echo $text_new_old_customer; ?></h3>
                    <div class="help"><?php echo $text_new_old_customer_help; ?></div>
                    <br />
                  	<?php require_once(DIR_APPLICATION.'view/template/'.$modulePath.'/report_new_old_customer.php'); ?>   
					<div class="clearfix"></div>
				</div>-->
			</div>
        </div>
    </div>
</div>