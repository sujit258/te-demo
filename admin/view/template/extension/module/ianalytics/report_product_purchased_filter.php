<table cellpadding="0" cellspacing="0" class="iAnalyticsDateFilter">
    <tr>
    	<td>
            <select class="form-control iAnalyticsSelectBox">
                <option value="custom"<?php if (!$iAnalyticsSelectData['enable'][0]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][0]) echo (' selected="selected"'); ?>><?php echo $text_custom; ?></option>
                <option value="last-week"<?php if (!$iAnalyticsSelectData['enable'][1]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][1]) echo (' selected="selected"'); ?>><?php echo $text_week; ?></option>
                <option value="last-month"<?php if (!$iAnalyticsSelectData['enable'][2]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][2]) echo (' selected="selected"'); ?>><?php echo $text_month; ?></option>
            </select>
   		</td>
        <td class="form-inline">
        <label><?php echo $text_from; ?></label>
        <input value="<?php echo $iAnalyticsFromDate;?>" class="fromDate form-control" style="width:100px;padding:10px;" type="text" maxlength="10" />
        </td>
        <td class="form-inline">
        <label><?php echo $text_to; ?></label>
        <input value="<?php echo $iAnalyticsToDate;?>" class="toDate form-control" style="width:100px;padding:10px;" type="text" maxlength="10" />
    	</td>
        <td class="form-inline">
        <select name="filterOrders" class="form-control">
              <option value="0"><?php echo $text_all_statuses; ?></option>
              <?php $filter_order_status_id = (isset($_GET['filterOrders'])) ? $_GET['filterOrders'] : '0'; ?>
              <?php foreach ($order_statuses as $order_status) { ?>
              <?php if ($order_status['order_status_id'] == $filter_order_status_id) { ?>
              <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
              <?php } ?>
              <?php } ?>
		</select>
       	</td>
        <td>
        <button type="button" class="btn btn-warning dateFilterButton"><i class="fa fa-filter"></i>&nbsp; <?php echo $text_filter; ?></button>
        </td>
    </tr>
</table>
<div class="clearfix"></div>