<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <h5><span class="required">* </span><strong><?php echo $text_module_status; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i> <?php echo $text_module_status_help; ?></span>
        </div>
        <div class="col-md-4">
            <select id="Checker" name="<?php echo $moduleName; ?>[Enabled]" class="form-control">
                <option value="yes" <?php echo (!empty($moduleData['Enabled']) && $moduleData['Enabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                <option value="no"  <?php echo (empty($moduleData['Enabled']) || $moduleData['Enabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <h5><strong><?php echo $text_co_after_sale; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i> <?php echo $text_co_after_sale_help; ?></span>
        </div>
        <div class="col-md-4">
            <select name="<?php echo $moduleName; ?>[AfterSaleData]" class="form-control">
                <option value="yes" <?php echo (!empty($moduleData['AfterSaleData']) && $moduleData['AfterSaleData'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                <option value="no"  <?php echo (empty($moduleData['AfterSaleData']) || $moduleData['AfterSaleData']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <h5><strong><?php echo $text_google_analytics; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i> <?php echo $text_google_analyt_help; ?></span>
        </div>
        <div class="col-md-4">
            <select name="<?php echo $moduleName; ?>[GoogleAnalytics]" id="GA_ecommerce" class="form-control">
                <option value="yes" <?php echo (!empty($moduleData['GoogleAnalytics']) && $moduleData['GoogleAnalytics'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                <option value="no"  <?php echo (empty($moduleData['GoogleAnalytics']) || $moduleData['GoogleAnalytics']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <div class="custom_settings">
   		<hr />
    </div>
    <div class="row custom_settings">
        <div class="col-md-3">
            <h5><strong><?php echo $text_google_account_nr; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i> <?php echo $text_google_an_help; ?></span>
        </div>
        <div class="col-md-4">
            <div class="form-group">
          	  <input type="text" class="form-control" placeholder="UA-XXXXXXX-XX" name="<?php echo $moduleName; ?>[GoogleAnalyticsIDNumber]"  id="moduleGA_id" value="<?php echo !empty($moduleData['GoogleAnalyticsIDNumber']) ? $moduleData['GoogleAnalyticsIDNumber'] : ''?>">
            </div>
        </div>
        <div class="col-md-4">
			<script language="javascript" type="text/javascript">
				function popitup(url) {
					newwindow=window.open(url,'name','height=750,width=950,toolbar=no,menubar=no,scrollbars=no,location=no,directories=no');
					if (window.focus) { newwindow.focus(); }
					return false;
				}
            </script>
            <a class="btn btn-primary" onclick="return popitup('https://www.google.com/analytics/web/#report/conversions-ecommerce-overview/')"><?php echo $text_ecommerce_trac_button; ?></a>
        </div>
    </div>
    <div class="row custom_settings">
        <hr />
        <div class="col-md-3">
            <h5><strong><?php echo $text_tracking_code; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i> <?php echo $text_tracking_code_help; ?></span>
        </div>
        <div class="col-md-4">
       		<select name="<?php echo $moduleName; ?>[GoogleAnalyticsTracking]" id="GoogleAnalyticsTracking" class="form-control">
                <option value="regular_tracking" <?php echo (!empty($moduleData['GoogleAnalyticsTracking']) && $moduleData['GoogleAnalyticsTracking'] == 'regular_tracking') ? 'selected=selected' : '' ?>><?php echo $text_regular_tracking; ?></option>
                <option value="ec_tracking"  <?php echo (empty($moduleData['GoogleAnalyticsTracking']) || $moduleData['GoogleAnalyticsTracking']== 'ec_tracking') ? 'selected=selected' : '' ?>><?php echo $text_enhanced_tracking; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <h5><strong><?php echo $text_clear_analytics; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i> <?php echo $text_clear_analytics_help; ?></span>
        </div>
        <div class="col-md-4">
       		<a class="btn btn-default" onclick="return confirm('Are you sure you wish to delete all analytics data?');" href="index.php?route=module/ianalytics/deleteanalyticsdata&token=<?php echo $token; ?>"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp; <?php echo $text_claer_all; ?></a>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <h5><strong><?php echo $text_balcklisted; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i> <?php echo $text_balcklisted_help; ?></span>
        </div>
        <div class="col-md-4">
        	<textarea style="width: 400px; height: 100px;" class="form-control" name="<?php echo $moduleName; ?>[BlacklistedIPs]"><?php echo !empty($moduleData['BlacklistedIPs']) ? $moduleData['BlacklistedIPs'] : ''; ?></textarea>
        </div>
    </div>
</div>