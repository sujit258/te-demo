<table cellpadding="0" cellspacing="0" class="iAnalyticsDateFilter">
  <tr>
    <td>
      <select class="form-control iAnalyticsSelectBox">
        <option value="custom"<?php if (!$iAnalyticsSelectData['enable'][0]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][0]) echo (' selected="selected"'); ?>><?php echo $text_custom; ?></option>
        <option value="last-week"<?php if (!$iAnalyticsSelectData['enable'][1]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][1]) echo (' selected="selected"'); ?>><?php echo $text_week; ?></option>
        <option value="last-month"<?php if (!$iAnalyticsSelectData['enable'][2]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][2]) echo (' selected="selected"'); ?>> <?php echo $text_month; ?></option>
      </select>
    </td>
    <td class="form-inline">
      <label><?php echo $text_from; ?></label>
      <input value="<?php echo $iAnalyticsFromDate;?>" class="fromDate form-control" data-date-format="YYYY-MM-DD" style="width:100px;padding:10px;" type="text" maxlength="10" />
    </td>
    <td class="form-inline">
      <label><?php echo $text_to; ?></label>
      <input value="<?php echo $iAnalyticsToDate;?>" class="toDate form-control" data-date-format="YYYY-MM-DD" style="width:100px;padding:10px;" type="text" maxlength="10" />
    </td>
    <td>
      <button type="button" class="btn btn-warning dateFilterButton"><i class="fa fa-filter"></i>&nbsp; Filter</button>
    </td>
  </tr>
</table>
<div class="clearfix"></div>