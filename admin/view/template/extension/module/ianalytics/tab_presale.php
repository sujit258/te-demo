<?php $searchvar = 'search'; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked" id="preSaleTabs">
				<li><a href="#search-queries1" data-toggle="tab" data-chart="search_queries"><i class="fa fa-search"></i>&nbsp;&nbsp;<?php echo $text_search_queries; ?></a></li>
                <li><a href="#search-keywords" data-toggle="tab"><i class="fa fa-font"></i>&nbsp;&nbsp;<?php echo $text_search_keywords; ?></a></li>
                
                <li><a href="#search-keywords2" data-toggle="tab" data-chart="search_queries"><i class="fa fa-circle-thin"></i>&nbsp;&nbsp;0 result search </a></li>
				
                <li><a href="#most-searched-products" data-toggle="tab"><i class="fa fa-tags"></i>&nbsp;&nbsp;<?php echo $text_m_searched_prod; ?></a></li>
                <li><a href="#opened-products" data-toggle="tab"><i class="fa fa-eye"></i>&nbsp;&nbsp;<?php echo $text_opened_prod; ?></a></li>
                <li><a href="#added-to-cart" data-toggle="tab"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;<?php echo $text_added_to_c_prod; ?></a></li>
                <li><a href="#added-to-wishlist" data-toggle="tab"><i class="fa fa-heart"></i>&nbsp;&nbsp;<?php echo $text_added_to_wishlist; ?></a></li>
				<li><a href="#compared-products" data-toggle="tab"><i class="fa fa-random"></i>&nbsp;&nbsp;<?php echo $text_compared_prod; ?></a></li>            
            </ul>
        </div>
        <div class="col-md-9">
			<div style="float:right;overflow:hidden;"><?php require('element_filter.php'); ?></div>
			<div class="tab-content">
                <div id="search-queries1" class="tab-pane fade">
                    <h3><?php echo $text_search_query_graph; ?></h3>
                    <div class="help"><?php echo $text_search_query_graph_h; ?></div>
                    <div class="iModuleFields">
						<br />
                        <div style="position: relative; padding-bottom: 30%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
                            <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                <canvas id="search_queries_canvas" style="max-width:100%; max-height:100%;"></canvas>
                            </div>
                        </div>
                        <script type="text/javascript">
                            iAnalytics.charts.search_queries = {
                                instance: null,
								name: 'search_queries',
								canvas: 'search_queries_canvas',	
								type: 'Line',
								data: {
									labels : [
                                        <?php if (count($iAnalyticsMonthlySearchesTable) == 2) { echo "'#',"; } ?>
                                        <?php foreach($iAnalyticsMonthlySearchesTable as $index => $day) { if ($index<1) { } else { ?> '<?php echo $day[0]?>', <?php } } ?>
                                    ],
									datasets : [{
										label: "Successful Search Queries",
										fillColor : "rgba(1,159,215,0.2)",
										strokeColor : "rgba(1,159,215,1)",
										pointColor : "rgba(1,159,215,1)",
										pointStrokeColor : "#fff",
										pointHighlightFill : "#fff",
										pointHighlightStroke : "rgba(1,159,215,1)",
										data : [
                                            <?php if (count($iAnalyticsMonthlySearchesTable) == 2) { echo "'0',"; } ?>
                                            <?php foreach($iAnalyticsMonthlySearchesTable as $index => $day) { if ($index<1) { } else { ?> '<?php echo $day[2]?>', <?php } } ?>
                                        ]
									},
									{
										label: "Zero-Results Search Queries",
										fillColor: "rgba(157,157,157,0.2)",
										strokeColor: "rgba(157,157,157,1)",
										pointColor: "rgba(157,157,157,1)",
										pointStrokeColor : "#fff",
										pointHighlightFill : "#fff",
										pointHighlightStroke: "rgba(151,187,205,1)",
										data : [
                                            <?php if (count($iAnalyticsMonthlySearchesTable) == 2) { echo "'0',"; } ?>
                                            <?php foreach($iAnalyticsMonthlySearchesTable as $index => $day) { if ($index<1) { } else { ?> '<?php echo $day[3]?>', <?php } } ?>
                                        ]
									}]
								}
                            }
                        </script>
                        <div id="search_queries_legend"></div>
                        <br />
                        <h3>Zero search result keyword - History</h3>
                        <table class="table-hover table table-striped">
                            <?php foreach($iAnalyticsMonthlySearchesTable as $index => $day): ?>
							<?php if ($index<1) { ?>
                                <thead>
                                    <tr>
                                      <th><?php echo $day[0]?></th>
                                      <th><?php echo $day[1]?></th>
                                      <th><?php echo $day[2]?></th>
                                      <th><?php echo $day[3]?></th>
                                    </tr>
                                </thead>
                            <?php } else { ?>
                                <tr><td><?php echo $day[0]?></td><td><?php echo $day[1]?></td><td><?php echo $day[2]?></td><td><?php echo $day[3]?></td></tr>
                            <?php } ?>
                            <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <div id="search-keywords" class="tab-pane fade">
                    <div class="iModuleFields">
                        <h3><?php echo $text_keywords_search; ?></h3>
                        <div class="help"><?php echo $text_keywords_search_help; ?></div><br />
                        <table class="table-hover table table-striped">
                        <?php foreach($iAnalyticsKeywordSearchHistory as $index => $k): ?>
						<?php if ($index<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
								  <th><?php echo $k[2]?></th>
                                  <th><?php echo $k[3]?></th>
                                  <th><?php echo $k[4]?></th>
                                  <th><?php echo $k[5]?></th>
                                  <th></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
                            <tr><td><?php echo strlen($k[0]) > 30 ? substr($k[0], 0, 30) . '...' : $k[0];?></td><td><?php echo $k[1]?></td><td><?php echo $k[2]?></td><td><?php echo $k[3]?></td><td><?php echo $k[4]?></td><td><?php echo $k[5]?></td>
                            <td style="text-align:right;"><?php if ($index > 0) : ?><a class="btn btn-default" onclick="return confirm('Are you sure you wish to delete the record?');" href="index.php?route=module/ianalytics/deletesearchkeyword&token=<?php echo $token; ?>&searchValue=<?php echo $k[6]; ?>"><i class="fa fa-minus" aria-hidden="true"></i>&nbsp;<?php echo $text_delete_record; ?></a>&nbsp;&nbsp;<a class="btn btn-default" onclick="return confirm('Are you sure you wish to delete all of the searches of this keyword?');" href="index.php?route=module/ianalytics/deleteallsearchkeyword&token=<?php echo $token; ?>&searchValue=<?php echo $k[0]; ?>"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;<?php echo $text_delete_keyword; ?></a><?php endif; ?></td>
                            </tr>
                            <?php } ?>
                        <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <!-- start -->
                 <div id="search-keywords2" class="tab-pane fade">
                    <div class="iModuleFields">
                        <h3><?php echo $text_keywords_search; ?></h3>
                        <div class="help"><?php echo $text_keywords_search_help; ?></div><br />
                        <table class="table-hover table table-striped">
                        <?php foreach($iAnalyticszeroKeywordSearchHistory as $index => $k): ?>
						<?php if ($index<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
								  <th><?php echo $k[2]?></th>
                                  <th><?php echo $k[3]?></th>
                                  <th><?php echo $k[4]?></th>
                                  <th><?php echo $k[5]?></th>
                                  <th></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
                            <tr>
                                <td><?php echo strlen($k[0]) > 30 ? substr($k[0], 0, 30) . '...' : $k[0];?></td>
                                <td><?php echo $k[1]?></td>
                                <td><?php echo $k[2]?></td>
                                <td><?php echo $k[3]?></td>
                                <td><?php echo $k[4]?></td>
                                <td><?php echo $k[5]?></td>
                                
                            <td style="text-align:right;"><?php if ($index > 0) : ?><a class="btn btn-default" onclick="return confirm('Are you sure you wish to delete the record?');" href="index.php?route=module/ianalytics/deletesearchkeyword&token=<?php echo $token; ?>&searchValue=<?php echo $k[6]; ?>"><i class="fa fa-minus" aria-hidden="true"></i>&nbsp;<?php echo $text_delete_record; ?></a>&nbsp;&nbsp;<a class="btn btn-default" onclick="return confirm('Are you sure you wish to delete all of the searches of this keyword?');" href="index.php?route=module/ianalytics/deleteallsearchkeyword&token=<?php echo $token; ?>&searchValue=<?php echo $k[0]; ?>"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;<?php echo $text_delete_keyword; ?></a><?php endif; ?></td>
                            </tr>
                            <?php } ?>
                        <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <!-- end -->
                
                <div id="most-searched-products" class="tab-pane fade">
                    <h3><?php echo $text_most_searched; ?></h3>
                    <div class="help"><?php echo $text_most_searched_help; ?></div>
                    <br />
                    <table class="table-hover table table-striped">
                        <?php foreach($iAnalyticsMostSearchedKeywords as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                  <th></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
                            <tr><td><?php echo strlen($k[0]) > 30 ? substr($k[0], 0, 30) . '...' : $k[0];?></td><td><?php echo $k[1]?></td><td align="right"><?php if ($j > 0) {  ?><div><a href="../index.php?route=product/search&<?php echo $searchvar; ?>=<?php echo $k[0]?>" target="_blank" class="btn btn-default"><i class="  fa   fa-eye" aria-hidden="true"></i>&nbsp; <?php echo $text_preview; ?></a></div><?php } ?></td></tr>
						<?php } ?>
                        <?php endforeach; ?>
                    </table>
                    <div class="iModuleFields">
                        <div class="clearfix"></div>
                    </div>
                 </div>
                 
                 <div id="opened-products" class="tab-pane fade">
					<h3><?php echo $text_opened_prod; ?></h3>
					<div class="help"><?php echo $text_opened_prod_help; ?></div>
                    <br />
					<table class="table-hover table table-striped">
					<?php foreach($iAnalyticsMostOpenedProducts as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
						<tr><td><?php echo $k[0]?></td><td><?php echo $k[1]?></td></tr>
						<?php } ?>
					<?php endforeach; ?>
					</table>
					<div class="clearfix"></div>
				</div>
                
                <div id="added-to-cart" class="tab-pane fade">
					<h3><?php echo $text_most_added_to_c; ?></h3>
					<div class="help"><?php echo $text_most_added_to_c_help; ?></div>
                    <br />
					<table class="table-hover table table-striped">
					<?php foreach($iAnalyticsMostAddedtoCartProducts as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
						<tr><td><?php echo $k[0]?></td><td><?php echo $k[1]?></td></tr>
						<?php } ?>
					<?php endforeach; ?>
					</table>
					<div class="clearfix"></div>
				</div>
                
				<div id="added-to-wishlist" class="tab-pane fade">
					<h3><?php echo $text_most_added_to_wish; ?></h3>
					<div class="help"><?php echo $text_most_added_to_wish_help; ?></div>
                    <br />
					<table class="table-hover table table-striped">
					<?php foreach($iAnalyticsMostAddedtoWishlistProducts as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
						<tr><td><?php echo $k[0]?></td><td><?php echo $k[1]?></td></tr>
						<?php } ?>
					<?php endforeach; ?>
					</table>
					<div class="clearfix"></div>
				</div>
                
                <div id="compared-products" class="tab-pane fade">
                    <h3><?php echo $text_compared_prod; ?></h3>
                    <div class="help"><?php echo $text_compare_prod_help; ?></div>
                    <br />
                    <table class="table-hover table table-striped">
						<?php foreach($iAnalyticsMostComparedProducts as $j => $k): ?>
							<?php if ($j<1) { ?>
                                <thead>
                                    <tr>
                                      <th><?php echo $k[0]?></th>
                                      <th><?php echo $k[1]?></th>
                                    </tr>
                                </thead>
                    		<?php } else { ?>
                      			<tr><td><?php echo $k[0]?></td><td><?php echo $k[1]?></td></tr>
                    		<?php } ?>
                   		 <?php endforeach; ?>
                    </table>
                    <div class="clearfix"></div>
				</div>
			</div>
        </div>
    </div>
    <hr />
</div>