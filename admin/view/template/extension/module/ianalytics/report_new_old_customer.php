<?php
if (isset($_GET['fromDate'])) { $filter_date_start = $_GET['fromDate']; } else { $filter_date_start = $iAnalyticsFromDate; }
if (isset($_GET['toDate'])) {	$filter_date_end = $_GET['toDate']; } else { $filter_date_end = $iAnalyticsToDate; }
if (isset($_GET['filterOrders'])) { $filter_order_status_id = $_GET['filterOrders']; } else { $filter_order_status_id = 0; }
if (isset($_GET['filterCustomertype'])) { $filter_customer_type = $_GET['filterCustomertype']; } else { $filter_customer_type = 1; }
if (isset($_GET['page'])) { $page = $_GET['page']; } else {	$page = 1; }

$new_old_customers_data = array();
$new_old_customers_filters = array(
	'filter_date_start'	  		=> $filter_date_start, 
	'filter_date_end'			=> $filter_date_end, 
	'filter_order_status_id' 	=> $filter_order_status_id,
	'filter_customer_type'      => $filter_customer_type,
	'start'                  	=> ($page - 1) * $limit,
	'limit'                  	=> $limit,
	'store_id'                  => $store_id
);

$result = $model_module_ianalitycs->getNewOldCustomersData($new_old_customers_filters);
$new_avg_ticket_size = $result['new_customer_sales'] / $result['new_order_count'];
$old_avg_ticket_size = $result['old_customer_sales'] / $result['old_order_count'];

$new_old_customers_data = array(
	'new_customer_count'    => $result['new_customer_count'],
	'new_order_count'       => $result['new_order_count'],
	'new_customer_sales'    => $currency_lib->format($result['new_customer_sales'], $currency),
	'new_avg_ticket_size'   => $currency_lib->format($new_avg_ticket_size, $currency),
	'old_customer_count'    => $result['old_customer_count'],
	'old_order_count'       => $result['old_order_count'],
	'old_customer_sales'    => $currency_lib->format($result['old_customer_sales'], $currency),
	'old_avg_ticket_size'   => $currency_lib->format($old_avg_ticket_size, $currency)
);

$url = '';
if (isset($_GET['fromDate'])) {	$url .= '&fromDate=' . $_GET['fromDate']; }
if (isset($_GET['toDate'])) {	$url .= '&toDate=' . $_GET['toDate']; }
if (isset($_GET['filterOrders'])) {	$url .= '&filterOrders=' . $_GET['filterOrders']; }
if (isset($_GET['filterCustomertype'])) {	$url .= '&filterCustomertype=' . $_GET['filterCustomertype']; }

?>
<b>Note: Default data is based on Shipped & Complete order status.</b>
<table class="table-hover table table-striped">
    <thead>
      <tr>
        <th style="width:20%;">Customer Type</th>
        <th style="width:20%;">Number of Customers</th>
        <th style="width:20%;">Number of Orders</th>
        <th style="width:20%;">Sales</th>
        <th style="width:20%;">Average Ticket Size</th>
    </thead>
    <tbody>
        <tr>
            <td>New Customer</td>
            <td><?php echo $new_old_customers_data['new_customer_count']; ?></td>
            <td><?php echo $new_old_customers_data['new_order_count']; ?></td>
            <td><?php echo $new_old_customers_data['new_customer_sales']; ?></td>
            <td><?php echo $new_old_customers_data['new_avg_ticket_size']; ?></td>
        </tr>
        <tr>
            <td>Repeat Customer</td>
            <td><?php echo $new_old_customers_data['old_customer_count']; ?></td>
            <td><?php echo $new_old_customers_data['old_order_count']; ?></td>
            <td><?php echo $new_old_customers_data['old_customer_sales']; ?></td>
            <td><?php echo $new_old_customers_data['old_avg_ticket_size']; ?></td>
        </tr>
    </tbody>
</table>