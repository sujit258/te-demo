<table cellpadding="0" cellspacing="0" class="iAnalyticsDateFilter">
  <tr>
    <td style="padding-left:10px;">
      <label style="display:block;text-align:left; padding:0;margin:0;">Range</label>
      <select class="form-control iAnalyticsSelectBox">
        <option value="custom"<?php if (!$iAnalyticsSelectData['enable'][0]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][0]) echo (' selected="selected"'); ?>><?php echo $text_custom; ?></option>
        <option value="last-week"<?php if (!$iAnalyticsSelectData['enable'][1]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][1]) echo (' selected="selected"'); ?>><?php echo $text_week; ?></option>
        <option value="last-month"<?php if (!$iAnalyticsSelectData['enable'][2]) echo (' disabled="disabled" class="optionDisabled"'); if ($iAnalyticsSelectData['select'][2]) echo (' selected="selected"'); ?>><?php echo $text_month; ?></option>
      </select>
    </td>
    <td class="form-inline" style="padding-left:10px;text-align:right">
      <label style="display:block;text-align:left; padding:0;margin:0;">From</label>
      <input value="<?php echo $iAnalyticsFromDate;?>" class="fromDate form-control" style="width:100px;padding:10px;" type="text" maxlength="10" />
    </td>
    <td class="form-inline" style="padding-left:10px;">
      <label style="display:block;text-align:left; padding:0;margin:0;">To</label>
      <input value="<?php echo $iAnalyticsToDate;?>" class="toDate form-control" style="width:100px;padding:10px;" type="text" maxlength="10" />
    </td>
    <td class="form-inline" style="padding-left:10px;">
      <label style="display:block;text-align:left; padding:0;margin:0;">Status</label>
      <select name="filterOrders" class="form-control" style="width:135px">
        <option value="0"><?php echo $text_all_statuses; ?></option>
        <?php $filter_order_status_id = (isset($_GET['filterOrders'])) ? $_GET['filterOrders'] : '0'; ?>
        <?php foreach ($order_statuses as $order_status) { ?>
          <?php if ($order_status['order_status_id'] == $filter_order_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
          <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
          <?php } ?>
        <?php } ?>
      </select>
    </td>
    <td class="form-inline" style="padding-left:10px;">
      <label style="display:block;text-align:left; padding:0;margin:0;">Group by</label>
      
      <?php $filterGroup = (isset($_GET['filterGroup'])) ? $_GET['filterGroup'] : 'day'; ?>
      <select name="filterGroup" id="input-group" class="form-control">
        <option value="day"  <?php echo $filterGroup == 'day' ? 'selected="selected"' : ''; ?>>Days</option>
        <option value="week" <?php echo $filterGroup == 'week' ? 'selected="selected"' : ''; ?>>Weeks</option>
        <option value="month" <?php echo $filterGroup == 'month' ? 'selected="selected"' : ''; ?>>Months</option>
        <option value="year" <?php echo $filterGroup == 'year' ? 'selected="selected"' : ''; ?>>Years</option>
      </select>
    </td>
    <td>
      <label style="display:block;text-align:left; padding:0;margin:0;">&nbsp;</label>
      <button type="button" class="btn btn-warning dateFilterButton"><i class="fa fa-filter"></i>&nbsp; <?php echo $text_filter; ?></button>
    </td>
  </tr>
</table>
<div class="clearfix"></div>