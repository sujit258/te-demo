<?php
if (isset($_GET['fromDate'])) { $filter_date_start = $_GET['fromDate']; } else { $filter_date_start = $iAnalyticsFromDate; }
if (isset($_GET['toDate'])) {	$filter_date_end = $_GET['toDate']; } else { $filter_date_end = $iAnalyticsToDate; }
if (isset($_GET['filterOrders'])) { $filter_order_status_id = $_GET['filterOrders']; } else { $filter_order_status_id = 0; }
if (isset($_GET['page'])) { $page = $_GET['page']; } else {	$page = 1; }

$citywise_data = array();
$citywise_filters = array(
	'filter_date_start'	  		=> $filter_date_start, 
	'filter_date_end'			=> $filter_date_end, 
	'filter_order_status_id' 	=> $filter_order_status_id,
	'start'                  	=> ($page - 1) * $limit,
	'limit'                  	=> $limit,
	'store_id'                  => $store_id
);

$citywise_total = $model_module_ianalitycs->getTotalCitywiseData($citywise_filters);
$citywise_results = $model_module_ianalitycs->getCitywiseData($citywise_filters);
$total_sales_orders = $model_module_ianalitycs->getCitywiseTotalSalesOrders($citywise_filters);

foreach ($citywise_results as $city) {
	$citywise_data[] = array(
		'shipping_city' => $city['shipping_city'],
		'total_orders'  => $city['total_orders'],
		'sales'         => $currency_lib->format($city['sales'], $currency)
	);
}

$url = '';
if (isset($_GET['fromDate'])) {	$url .= '&fromDate=' . $_GET['fromDate']; }
if (isset($_GET['toDate'])) {	$url .= '&toDate=' . $_GET['toDate']; }
if (isset($_GET['filterOrders'])) {	$url .= '&filterOrders=' . $_GET['filterOrders']; }

$pagination					= new Pagination();
$pagination->total			= $citywise_total;
$pagination->page			= $page;
$pagination->limit			= $limit; 
$pagination->url			= $pagination->url = $url_link->link($modulePath, 'store_id=' . $store_id . '&token=' . $token . $url . '&page={page}');
$pagination_citywise		= $pagination->render();
$results_citywise			= sprintf($language->get('text_pagination'), ($pagination->total) ? (($page - 1) * $pagination->limit) + 1 : 0, ((($page - 1) * $pagination->limit) > ($pagination->total - $pagination->limit)) ? $pagination->total : ((($page - 1) * $pagination->limit) + $pagination->limit), $pagination->total, ceil($pagination->total / $pagination->limit));

?>
<b>Note: Default data is based on Shipped & Complete order status.</b>
<table class="table-hover table table-striped">
    <thead>
      <tr>
        <th style="width:25%;"><?php echo $text_total_sales; ?></th>
        <th style="width:25%;"><?php echo $text_total_orders; ?></th>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $currency_lib->format($total_sales_orders['total_sales'], $currency); ?></td>
            <td><?php echo $total_sales_orders['total_orders']; ?></td>
        </tr>
    </tbody>
</table>
<table class="table-hover table table-striped">
    <thead>
      <tr>
        <th style="width:25%;"><?php echo $text_city; ?></th>
        <th style="width:25%;"><?php echo $text_total_orders; ?></th>
        <th style="width:25%;"><?php echo $text_total_sales; ?></th>
        <th style="width:25%;"><?php echo $text_contribution; ?></th>
      </tr>
    </thead>
<?php if ($citywise_results) { ?>
     <?php foreach ($citywise_results as $result) { ?>
          <tr>
            <td style="text-transform: capitalize;"><?php echo $result['shipping_city']; ?></td>
            <td><?php echo $result['total_orders']; ?></td>
            <td><?php echo $currency_lib->format($result['sales'], $currency); ?></td>
            <td><?php echo round($result['sales'] / $total_sales_orders['total_sales'] * 100,2); ?>%</td>
          </tr>
     <?php } ?>
<?php } else { ?>
	<tr>
		<td class="center" colspan="4"><?php echo $text_no_results; ?></td>
	</tr>
<?php } ?>
</table>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination_citywise; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results_citywise; ?></div>
</div>