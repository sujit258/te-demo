<?php
if (isset($_GET['fromDate'])) { $filter_date_start = $_GET['fromDate']; } else { $filter_date_start = $iAnalyticsFromDate; }
if (isset($_GET['toDate'])) {	$filter_date_end = $_GET['toDate']; } else { $filter_date_end = $iAnalyticsToDate; }
if (isset($_GET['filterOrders'])) { $filter_order_status_id = $_GET['filterOrders']; } else { $filter_order_status_id = 0; }
if (isset($_GET['page'])) { $page = $_GET['page']; } else {	$page = 1; }

$tc_data = array();
$tc_filters = array(
	'filter_date_start'	  		=> $filter_date_start, 
	'filter_date_end'			=> $filter_date_end, 
	'filter_order_status_id' 	=> $filter_order_status_id,
	'start'                  	=> ($page - 1) * $limit,
	'limit'                  	=> $limit
);

//$tc_total = $model_module_ianalitycs->getTotalTrueCashData($tc_filters);
$tc_results = $model_module_ianalitycs->getTrueCashData($tc_filters);

$total_earned = 0;
$total_used = 0;
foreach ($tc_results as $tc_result) {
	$tc_data[] = array(
		'customer_id'   => $tc_result['customer_id'],
		'email'         => $tc_result['email'],
		'earned'        => $currency_lib->format($tc_result['earned'], $currency),
		'used'          => $currency_lib->format($tc_result['used'], $currency),
		'href'          => $tc_result['href']
	);
	$total_earned += $tc_result['earned'];
    $total_used += $tc_result['used'];
}

$url = '';
if (isset($_GET['fromDate'])) {	$url .= '&fromDate=' . $_GET['fromDate']; }
if (isset($_GET['toDate'])) {	$url .= '&toDate=' . $_GET['toDate']; }
if (isset($_GET['filterOrders'])) {	$url .= '&filterOrders=' . $_GET['filterOrders']; }

$pagination					= new Pagination();
$pagination->total			= count($tc_data);
$pagination->page			= $page;
$pagination->limit			= count($tc_data);//$limit; 
$pagination->url			= $pagination->url = $url_link->link($modulePath, 'store_id=' . $store_id . '&token=' . $token . $url . '&page={page}');
$pagination_tc		= $pagination->render();
$results_tc			= sprintf($language->get('text_pagination'), ($pagination->total) ? (($page - 1) * $pagination->limit) + 1 : 0, ((($page - 1) * $pagination->limit) > ($pagination->total - $pagination->limit)) ? $pagination->total : ((($page - 1) * $pagination->limit) + $pagination->limit), $pagination->total, ceil($pagination->total / $pagination->limit));

?>
<b>Note: Default data is based on Shipped & Complete order status.</b>
<table class="table-hover table table-striped">
    <thead>
        <tr>
            <th style="width:25%;"><?php echo $text_total_earned; ?></th>
            <th style="width:25%;"><?php echo $text_total_used; ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $currency_lib->format($total_earned, $currency); ?></td>
            <td><?php echo $currency_lib->format($total_used, $currency); ?></td>
        </tr>
    </tbody>
</table>
<table class="table-hover table table-striped">
    <thead>
      <tr>
        <th style="width:33.33%;"><?php echo $text_email; ?></th>
        <th style="width:33.33%;"><?php echo $text_earned; ?></th>
        <th style="width:33.33%;"><?php echo $text_used; ?></th>
      </tr>
    </thead>
<?php if ($tc_data) { ?>
     <?php foreach ($tc_data as $tc) { ?>
          <tr>
            <td><a href="<?php echo $tc['href']; ?>" target="_blank"><?php echo $tc['email']; ?></a></td>
            <td><?php echo $tc['earned']; ?></td>
            <td><?php echo $tc['used']; ?></td>
          </tr>
     <?php } ?>
<?php } else { ?>
	<tr>
		<td class="center" colspan="3"><?php echo $text_no_results; ?></td>
	</tr>
<?php } ?>
</table>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination_tc; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results_tc; ?></div>
</div>