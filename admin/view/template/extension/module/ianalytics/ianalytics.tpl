<?php echo $header; ?>
<script>
var iAnalyticsMinDate = '<?php echo date("Y-m-d", strtotime("-3 MONTHS")); ?>';
//var iAnalyticsMinDate = '<?php echo $iAnalyticsMinDate; ?>';
var token = '<?php echo $token; ?>';
var iAnalyticsColors = ['019FD7','9ECC3C','FFDB1A','CC3C3C','9D9D9D','FF9900','242858'];
iAnalytics = {
	charts: new Object()	
};
Chart.defaults.global.responsive = true;
Chart.defaults.global.animationSteps = 30;
</script>
<?php echo $column_left; ?>
<div id="content" class="iAnalytics">
 <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
 
    <?php if ($success) { ?>
        <div class="alert alert-success autoSlideUp"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <script>$('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php } ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><span style="vertical-align:middle;font-weight:bold;"><?php echo $text_module_settings ?></span></h3>
         	<div class="storeSwitcherWidget">
            	<div class="form-group" style="padding:0px;">
                	<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><?php echo $store['name']; if($store['store_id'] == 0) echo " <strong>".$text_default."</strong>"; ?>&nbsp;<span class="caret"></span><span class="sr-only"><?php echo $text_toggle_dropdown ?></span></button>
                	<ul class="dropdown-menu" role="menu">
                    	<?php foreach ($stores as $st) { ?>
                            <li><a href="index.php?route=<?php echo $modulePath; ?>&store_id=<?php echo $st['store_id'];?>&token=<?php echo $token; ?>"><?php echo $st['name']; ?></a></li>
                    	<?php } ?> 
                	</ul>
            	</div>
            </div>
        </div>
        <div class="panel-body">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form"> 
				<input type="hidden" name="<?php echo $moduleName; ?>_status" value="1" />
				<input type="hidden" name="store_id" value="<?php echo $store_id; ?>" />
                <div class="tabbable">
                    <div class="tab-navigation form-inline">
                        <ul class="nav nav-tabs mainMenuTabs" id="mainTabs">
                            <li class="active"><a href="#dashboard1" data-toggle="tab"><?php echo $text_dashboard ?></a></li>
                            <li><a href="#presale" data-toggle="tab" data-chart="search_queries"><?php echo $text_presale ?></a></li>
                            <li><a href="#aftersale" data-toggle="tab"><?php echo $text_aftersale ?></a></li>
                            <li><a href="#visitors" data-toggle="tab"><?php echo $text_visitors ?></a></li>
                            <li><a href="#controlpanel" data-toggle="tab"><?php echo $text_settings ?></a></li>
                          
                        </ul>
                        <div class="tab-buttons">
                            <div class="btn-group">
                                <button type="button" onClick="javascript:void(0)" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bar-chart"></i>&nbsp;&nbsp;Data&nbsp; <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <?php if ((isset($ianalyticsStatus["ianalyticsStatus"]) && $ianalyticsStatus["ianalyticsStatus"] == 'run')){ ?>
                                    <li><a href="javascript:void(0)" onclick="document.location='index.php?route=<?php echo $modulePath; ?>/pausegatheringdata&store_id=<?php echo $store_id; ?>&token=<?php echo $_GET['token']; ?>'"><i class="fa fa-pause"></i>&nbsp;<?php echo $text_pause_data ?></a></li>
                                    <?php } else { ?>
                                    <li><a href="javascript:void(0)" onclick="document.location='index.php?route=<?php echo $modulePath; ?>/resumegatheringdata&store_id=<?php echo $store_id; ?>&token=<?php echo $_GET['token']; ?>'"><i class="fa fa-play"></i>&nbsp;<?php echo $text_resume_data ?></a></li>
                                    <?php } ?>
                                    <li class="divider"></li>
                                    <li><a onclick="return confirm('Are you sure you wish to delete all analytics data?');" href="index.php?route=<?php echo $modulePath; ?>/deleteanalyticsdata&store_id=<?php echo $store_id; ?>&token=<?php echo $token; ?>"><i class="fa fa-trash"></i>&nbsp;<?php echo $text_clear_all_data ?></a></li>
                                </ul>
                            </div>
                            <button type="submit" class="btn btn-success save-changes"><i class="fa fa-check"></i>&nbsp;<?php echo $text_save_changes ?></button>
                            <a onclick="location = '<?php echo $cancel; ?>'" class="btn btn-warning"><i class="fa fa-times"></i>&nbsp;<?php echo $text_cancel ?></a>
                        </div> 
                    </div><!-- /.tab-navigation --> 
                    <div class="tab-content">
                    	<?php
                        if (!function_exists('modification_vqmod')) {
                        	function modification_vqmod($file) {
                        		if (class_exists('VQMod')) {
                       				return VQMod::modCheck(modification($file), $file);
                        		} else {
                        			return modification($file);
                       			}
                        	}
                        }
						?>
                        <div id="dashboard1" class="tab-pane active">
                          <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_dashboard.php'); ?>                        
                        </div>
                        <div id="presale" class="tab-pane">
                          <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_presale.php'); ?>                        
                        </div>
                        <div id="aftersale" class="tab-pane">
                          <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_aftersale.php'); ?>                        
                        </div>
                        <div id="visitors" class="tab-pane">
                          <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_visitors.php'); ?>                        
                        </div>
                        <div id="controlpanel" class="tab-pane">
                          <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_controlpanel.php'); ?>                        
                        </div>
                    
                    </div> <!-- /.tab-content --> 
                </div><!-- /.tabbable -->
            </form>
        </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
function showHideStuff($typeSelector, $toggleArea, $selectStatus) {
	if ($typeSelector.val() === $selectStatus) {
		$toggleArea.show(); 
	} else {
		$toggleArea.hide(); 
	}
    $typeSelector.change(function(){
        if ($typeSelector.val() === $selectStatus) {
            $toggleArea.show(300); 
        }
        else {
            $toggleArea.hide(300); 
        }
    });
}
$(function() {
	showHideStuff($('#GA_ecommerce'), $('.custom_settings'), 'yes');
});
</script>
<?php echo $footer; ?>