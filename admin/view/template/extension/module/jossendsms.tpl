<script type="text/javascript" src="view/javascript/checkLength.js"></script>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
    <div class="container-fluid">
    	<div class="pull-right">
        <a href="<?php echo $setting; ?>" data-toggle="tooltip" title="<?php echo $button_setting; ?>" class="btn btn-primary"><i class="fa fa-gear"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	  <?php if ($error_warning) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	  <?php } ?>
	  <?php if ($success) { ?>
	  <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	  <?php } ?>
	  <?php if ($error) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	  <?php } ?>
	  <div class="panel panel-default">
	  	<div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $heading_title; ?></h3>
      </div>
	    <div class="panel-body">
	      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formsend" class="form-horizontal">
	      	
	      	<div id="sms" class="form">
		      	<div class="form-group">
		      		<label class="col-sm-2 control-label" for="input-gateway">Default Gateway</label>
		      		<div class="col-sm-4">
				        <div class="alert alert-info"><?php echo $gateway; ?></div>
				      </div>
		      	</div>
		      	<div class="form-group required">
				    	<label class="col-sm-2 control-label" for="input-to"><?php echo $entry_or; ?></label>
				      <div class="col-sm-10">
				        <select name="to" id="input-to" class="form-control">
	            		<option value=""><?php echo $text_none; ?></option>
		            	<option value="nohp"><?php echo $text_nohp; ?></option>
		              <option value="newsletter"><?php echo $text_newsletter; ?></option>
		              <option value="customer_all"><?php echo $text_customer_all; ?></option>
		              <option value="customer_group"><?php echo $text_customer_group; ?></option>
		              <option value="customer"><?php echo $text_customer; ?></option>
		              <option value="affiliate_all"><?php echo $text_affiliate_all; ?></option>
		              <option value="affiliate"><?php echo $text_affiliate; ?></option>
		              <option value="product"><?php echo $text_product; ?></option>
	              </select>
	              <?php if ($error_nohp) { ?>
		            <div class="text-danger"><?php echo $error_nohp; ?></div>
		            <?php } ?>
				      </div>
				    </div>
				    
				    <div id="to-nohp" class="to">
				    	<div class="form-group">
			      		<label class="col-sm-2 control-label" for="input-nohp"><?php echo $entry_nohp; ?></label>
			      		<div class="col-sm-10">
					        <input type="text" name="nohp" value="" placeholder="Mobile Phone" id="input-nohp" onkeyup="this.value = this.value.replace (/\D+/, '')" class="form-control" />
					      </div>
			      	</div>
				    </div>
				    <div id="to-customer-group" class="to">
				    	<div class="form-group">
			      		<label class="col-sm-2 control-label" for="input-nohp"><?php echo $entry_customer_group; ?></label>
			      		<div class="col-sm-10">
					        <select name="customer_group_id" class="form-control">
	                  <?php foreach ($customer_groups as $customer_group) { ?>
	                  <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
	                  <?php } ?>
	                </select>
					      </div>
			      	</div>
				    </div>
				    <div id="to-customer" class="to">
				    	<div class="form-group">
			      		<label class="col-sm-2 control-label" for="input-customer"><?php echo $entry_customer; ?></label>
			      		<div class="col-sm-10">
					        <input type="text" name="customers" value="" placeholder="Customer" id="input-customer" class="form-control" />
					        <div id="customer" class="scrollbox"></div>
					        <div class="well well-sm" style="height: 150px; overflow: auto;"></div>
					      </div>
			      	</div>
				    </div>
				    <div id="to-affiliate" class="to">
				    	<div class="form-group">
			      		<label class="col-sm-2 control-label" for="input-affiliate"><?php echo $entry_affiliate; ?></label>
			      		<div class="col-sm-10">
					        <input type="text" name="affiliates" value="" placeholder="Affiliate" id="input-affiliate" class="form-control" />
					        <div class="well well-sm" style="height: 150px; overflow: auto;"></div>
					      </div>
			      	</div>
				    </div>
				    <div id="to-product" class="to">
				    	<div class="form-group">
			      		<label class="col-sm-2 control-label" for="input-product"><?php echo $entry_product; ?></label>
			      		<div class="col-sm-10">
					        <input type="text" name="products" value="" placeholder="Products" id="input-product" class="form-control" />
					        <div class="well well-sm" style="height: 150px; overflow: auto;"></div>
					      </div>
			      	</div>
				    </div>
				    
				    <div class="form-group required">
			      	<label class="col-sm-2 control-label" for="input-nohp"><?php echo $entry_message; ?></label>
			      	<div class="col-sm-10">
					      <textarea name="message" rows="8" onChange="updateTextBoxCounter(this.form);" onKeyPress="updateTextBoxCounter(this.form);" onKeyUp="updateTextBoxCounter(this.form);" onKeyDown="updateTextBoxCounter(this.form);" class="form-control"></textarea>
	          			<DIV ID="InfoCharCounter"></DIV>
		              <?php if ($error_message) { ?>
		              <div class="text-danger"><?php echo $error_message; ?></div>
		              <?php } ?>
		              <br />
              		<input type="submit" id="button-button-send" class="btn btn-primary" value="<?php echo $button_send; ?>">
					    </div>
			      </div>
				    
			    </div>
	      </form>
	    </div>
	  </div>
  </div>
</div>

<script type="text/javascript"><!--	
$('select[name=\'to\']').bind('change', function() {
	$('#sms .to').hide();
	
	$('#sms #to-' + $(this).prop('value').replace('_', '-')).show();
});

$('select[name=\'to\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--
// Customers
$('input[name=\'customers\']').autocomplete({
	//delay: 500,
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {	
				response($.map(json, function(item) {
					return {
						//category: item.customer_group,
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
		
	}, 
	'select': function(item) {
		$('input[name=\'customers\']').val('');
		
		$('#input-customer' + item['value']).remove();

		$('#input-customer').parent().find('.well').append('<div id="customer' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="customer[]" value="' + item['value'] + '" /></div>');
				
		
	}
});

$('#input-customer').parent().find('.well').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script> 

<script type="text/javascript"><!--	
	// Affiliates
$('input[name=\'affiliates\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});		
	}, 
	'select': function(item) {
		$('input[name=\'affiliates\']').val('');

		$('#input-affiliate' + item['value']).remove();

		$('#input-affiliate').parent().find('.well').append('<div id="affiliate' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="affiliate[]" value="' + item['value'] + '" /></div>');
	}
});

$('#input-affiliate').parent().find('.well').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Products
$('input[name=\'products\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	}, 
	'select': function(item) {
		$('input[name=\'products\']').val('');

		$('#input-product' + item['value']).remove();

		$('#input-product').parent().find('.well').append('<div id="product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');
	}
});

$('#input-product').parent().find('.well').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

function send(url) { 
	$('textarea[name=\'message\']').html($('textarea[name=\'message\']').val());
	
	$.ajax({
		url: url,
		type: 'post',
		data: $('select, input, textarea'),		
		dataType: 'json',
		beforeSend: function() {
			$('#button-send').button('loading');
		},
		complete: function() {
			$('#button-send').button('reset');
		},				
		success: function(json) {
			$('.alert, .text-danger').remove();
			
			if (json['error']) {
				if (json['error']['warning']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');
				}
				
				if (json['error']['subject']) {
					$('input[name=\'subject\']').after('<div class="text-danger">' + json['error']['subject'] + '</div>');
				}	
				
				if (json['error']['message']) {
					$('textarea[name=\'message\']').parent().append('<div class="text-danger">' + json['error']['message'] + '</div>');
				}									
			}			
			
			if (json['next']) {
				if (json['success']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  ' + json['success'] + '</div>');
					
					send(json['next']);
				}		
			} else {
				if (json['success']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				}					
			}				
		}
	});
}
//--></script> 
<?php echo $footer; ?>