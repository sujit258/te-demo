<table class="table">

<tr>
	<td class="col-xs-3"><h5><strong><?php echo $text_button_name; ?></strong></h5></td>
    <td>
      <div class="col-xs-4">
         <?php foreach ($languages as $language) : ?>
            <div class="input-group" style="margin:10px auto;">
              <div class="input-group-addon"><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>" /></div>
              <input placeholder="<?php echo $text_pre_order; ?>" class="form-control" type="text" name="preorder[ButtonName][<?php echo $language['language_id']; ?>]" value="<?php echo !empty($data['preorder']['ButtonName'][$language['language_id']]) ? $data['preorder']['ButtonName'][$language['language_id']] : $text_pre_order; ?>" />
            </div>
          <?php endforeach; ?>
      </div>
    </td>
</tr>
<tr>
	<td class="col-xs-3"><h5><strong><?php echo $text_date_note; ?></strong></h5> 
    	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_date_note_help; ?></span>
    </td>
    <td>
      <div class="col-xs-8">
         <?php foreach ($languages as $language) : ?>
            <div class="input-group" style="margin:10px auto;">
              <div class="input-group-addon"><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>" /></div>
              <input placeholder="<?php echo $text_date_note_example; ?>" class="form-control" type="text" name="preorder[DateNote][<?php echo $language['language_id']; ?>]" value="<?php echo !empty($data['preorder']['DateNote'][$language['language_id']]) ? $data['preorder']['DateNote'][$language['language_id']] : ''; ?>" />
            </div>
          <?php endforeach; ?>
      </div>
    </td>
</tr>
 <tr id="custom_settings" class="customCssForm">
	<td class="col-xs-3"><h5><strong><?php echo $text_custom_colors; ?></strong></h5> 
    	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_custom_colors_help; ?></span>
    </td>
    <td class="col-xs-8">
        <div class="col-xs-5">
            <div class="customDesignColors form-group">
                <div class="input-group" id="po_font_color">            	
                    <span class="input-group-addon"><?php echo $text_text;?></span>
                    <input name="preorder[FontColor]" class="form-control colorp" type="text" value="<?php echo (!empty($data['preorder']['FontColor'])) ? $data['preorder']['FontColor'] : '#fff'  ?>">
                	<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
                </div>
            </div>
            <div class="customDesignColors form-group"> 
                <div class="input-group" id="po_bg_color">
                    <span class="input-group-addon"><?php echo $text_background;?></span>
                    <input name="preorder[BackgroundColor]" class="form-control colorp" type="text" value="<?php echo (!empty($data['preorder']['BackgroundColor'])) ? $data['preorder']['BackgroundColor'] : '#19BDF0'  ?>">
               		<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
                </div>
            </div>
            <div class="customDesignColors form-group">
                <div class="input-group" id="po_border_color">
                    <span class="input-group-addon"><?php echo $text_border;?></span>
                    <input name="preorder[BorderColor]" class="form-control colorp" type="text" value="<?php echo (!empty($data['preorder']['BorderColor'])) ? $data['preorder']['BorderColor'] : '#19BDF0'  ?>">
                	<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
                </div>
            </div>
        </div>
    </td>
</tr>
  <tr>
    <td class="col-xs-3">
      <h5><strong><?php echo $text_admin_notification; ?></strong></h5>
      <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_admin_notification_help; ?></span>
    </td>
    <td>
      <div class="col-xs-4">
          <select name="preorder[Notifications]" class="preorderNotifications form-control">
              <option value="yes" <?php echo ((isset($data['preorder']['Notifications']) && $data['preorder']['Notifications'] == 'yes')) ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
             <option value="no" <?php echo ((isset($data['preorder']['Notifications']) && $data['preorder']['Notifications'] == 'no')) ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
          </select>
      </div>
    </td>
  </tr>  
  
  <tr>
    <td class="col-xs-3">
      <h5><strong><?php echo $text_image_size; ?></strong></h5>
      <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_image_size_help; ?></span>
    </td>
    <td>
     <div class="form-group"> 
            <div class="input-group col-xs-5">
                <span class="input-group-addon"><?php echo $text_width;?></span>
                <input name="preorder[ImageWidth]" class="form-control" type="text" value="<?php echo (!empty($data['preorder']['ImageWidth'])) ? $data['preorder']['ImageWidth'] : '200'  ?>">
                <span class="input-group-addon">px</span>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group col-xs-5">
                <span class="input-group-addon"><?php echo $text_height;?></span>
                <input name="preorder[ImageHeight]" class="form-control" type="text" value="<?php echo (!empty($data['preorder']['ImageHeight'])) ? $data['preorder']['ImageHeight'] : '200'  ?>">
                <span class="input-group-addon">px</span>
            </div>
        </div>
    </td>
  </tr>  
   <tr>
<td class="col-xs-3">
    <h5><strong><?php echo $text_email; ?></strong></h5>
    <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_email_help; ?></span>
</td>
<td>
<div class="col-xs-12">
      <ul class="nav nav-tabs">
          <?php $i=0; foreach ($languages as $language) { ?>
            <li <?php if ($i==0) echo 'class="active"'; ?>><a href="#emailtab-<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="<?php echo $language['flag_url']; ?>"/> <?php echo $language['name']; ?></a></li>
          <?php $i++; }?>
      </ul>
      <div class="tab-content">
        <?php $i=0; foreach ($languages as $language) { ?>
                <div id="emailtab-<?php echo $language['language_id']; ?>" language-id="<?php echo $language['language_id']; ?>" class="row-fluid tab-pane language <?php if ($i==0) echo 'active'; ?>">
                    <div class="row">
                        <div class="col-md-8">
                            <h5><strong><?php echo $text_email_subject; ?></strong></h5>
                       
                            <input name="preorder[EmailSubject][<?php echo $language['code']; ?>]" class="input-xxlarge form-control" type="text" value="<?php echo (isset($data['preorder']['EmailSubject'][$language['code']])) ? $data['preorder']['EmailSubject'][$language['code']] : $text_pre_order ?>" />
                         </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <textarea id="email_text_<?php echo $language['code']; ?>" name="preorder[EmailText][<?php echo $language['code']; ?>]" style="height:80px;"  class="preorderEmailText form-control summernote"><?php echo (isset($data['preorder']['EmailText'][$language['code']])) ?        $data['preorder']['EmailText'][$language['code']] : $text_email_body; ?>
                          </textarea>
                        </div>
                    </div>
                </div>
            <?php $i++; } ?>
      </div>
  </div>
 
</td>
</tr>
<tr>
<td class="col-xs-3"><h5><strong><?php echo $text_custom_css; ?></strong></h5></td>
<td>
  <div class="col-xs-4">
      <textarea name="preorder[CustomCSS]" class="preorderCustomCSS form-control"><?php echo (isset($data['preorder']['CustomCSS'])) ? $data['preorder']['CustomCSS'] : '' ?>
      </textarea>
  </div>
</td>
</tr>
</table>
<?php $token = $_GET['token']; ?>
<script type="text/javascript"><!--

var colorpicker_change = function(input, box, color) {
    if (input !== false) {
      $(input).val(color);
    }

    $(box).css('background-color', color);
  }

  $('#po_font_color').ColorPicker({
    color: $('#po_font_color').find('input').val(),
    onChange : function(hsb, hex, rgb) {
      colorpicker_change($('#po_font_color').find('input'), $('#po_font_color').find('.colorpicker_box_color'), '#' + hex);
    } 
  });

  $('#po_bg_color').ColorPicker({
    color: $('#po_bg_color').find('input').val(),
    onChange : function(hsb, hex, rgb) {
      colorpicker_change($('#po_bg_color').find('input'), $('#po_bg_color').find('.colorpicker_box_color'), '#' + hex);
    }
  });

  $('#po_border_color').ColorPicker({
    color: $('#po_border_color').find('input').val(),
    onChange : function(hsb, hex, rgb) {
      colorpicker_change($('#po_border_color').find('input'), $('#po_border_color').find('.colorpicker_box_color'), '#' + hex);
    }
  });
  
  <?php if (!empty($data['preorder']['FontColor'])) : ?>
    colorpicker_change($('#po_font_color').find('input'), $('#po_font_color').find('.colorpicker_box_color'), '<?php echo $data['preorder']['FontColor']; ?>');
  <?php endif; ?>

  <?php if (!empty($data['preorder']['BackgroundColor'])) : ?>
    colorpicker_change($('#po_bg_color').find('input'), $('#po_bg_color').find('.colorpicker_box_color'), '<?php echo $data['preorder']['BackgroundColor']; ?>');
  <?php endif; ?>
  
   <?php if (!empty($data['preorder']['BorderColor'])) : ?>
    colorpicker_change($('#po_border_color').find('input'), $('#po_border_color').find('.colorpicker_box_color'), '<?php echo $data['preorder']['BorderColor']; ?>');
  <?php endif; ?>

  $('#cp_color, #cp_bgcolor').find('input').change(function() {
    colorpicker_change(false, $(this).closest('.colorpicker-component').find('.colorpicker_box_color'), $(this).val());
  });

  $('input, textarea').focus(function() {
    $('#cp_color, #cp_bgcolor').ColorPickerHide();
    $(this).trigger('click'); 
  })
  
//--></script>