<table class="table">
   <tr>
    <td class="col-xs-3">
      <h5><strong><?php echo $text_module_settings; ?></strong></h5>
      <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_module_settings_help; ?></span>
    </td>
    <td>
      <div class="col-xs-4">
          <select name="preorder[Enabled]" class="preorderEnabled form-control">
              <option value="yes" <?php echo (!empty($data['preorder']['Enabled']) && $data['preorder']['Enabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
             <option value="no" <?php echo (empty($data['preorder']['Enabled']) || $data['preorder']['Enabled'] == 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
          </select>
      </div>
   </td>
  </tr>
  <tr>
    <td class="col-xs-3">
      <h5><strong><?php echo $text_module_status ?></strong></h5>
      <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_module_status_help; ?></span>
    </td>
    <td>
      <div class="col-xs-6">
      <?php foreach($stock_statuses as $stock_status) { ?>
      
          <div class="checkbox">
                    <label>
                      <input type="checkbox" name="preorder[<?php echo $stock_status['stock_status_id']?>]" value="yes" <?php echo isset($data['preorder'][$stock_status['stock_status_id']]) ? 'checked="checked"' : ''; ?>/> <?php echo $stock_status['name'] ?>
                    </label>
           </div>
       <?php } ?>
      </div>
   </td>
  </tr>
</table>
