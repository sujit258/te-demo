<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-jossms" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $sendsms; ?>" data-toggle="tooltip" title="<?php echo $button_sendsms; ?>" class="btn btn-warning"><i class="fa fa-envelope"></i></a>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	  <?php if ($success) { ?>
	  <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	  <?php } ?>
	  <?php if ($error_warning) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	  <?php } ?>
  <div class="panel panel-default">
    <div class="panel-heading">
    	<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
    </div>
    <div class="panel-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-jossms" class="form-horizontal">
      	
	      	<!-- new layout -->
	      <div id="setgate">
	      	<div class="form-group required">
	      		<label class="col-sm-3 control-label" for="input-gateway"><?php echo $entry_gateway; ?></label>
	          <div class="col-sm-9">
	            <select name="jossms_gateway" id="input-gateway" class="form-control">
	            		<option value=""><?php echo $text_none; ?></option>
	            		<?php if ($jossms_gateway=="amd") { ?>
	            			<option value="amd" selected="selected">AMD Telecom - www.amdtelecom.net</option>
	            		<?php }else{ ?>
	            			<option value="amd">AMD Telecom - www.amdtelecom.net</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="smsglobal") { ?>
	            			<option value="smsglobal" selected="selected">Bulk SMS Global - www.bulksmsglobal.in</option>
	            		<?php }else{ ?>
	            			<option value="smsglobal">Bulk SMS Global - www.bulksmsglobal.in</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="clickatell") { ?>
	            			<option value="clickatell" selected="selected">Clickatell - www.clickatell.com</option>
	            		<?php }else{ ?>
	            			<option value="clickatell">Clickatell - www.clickatell.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="liveall") { ?>
	            			<option value="liveall" selected="selected">LiveAll - www.liveall.eu</option>
	            		<?php }else{ ?>
	            			<option value="liveall">LiveAll - www.liveall.eu</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="malath") { ?>
	            			<option value="malath" selected="selected">Malath SMS - sms.malath.net.sa</option>
	            		<?php }else{ ?>
	            			<option value="malath">Malath SMS - sms.malath.net.sa</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="mobily") { ?>
	            			<option value="mobily" selected="selected">mobily.ws - www.mobily.ws</option>
	            		<?php }else{ ?>
	            			<option value="mobily">mobily.ws - www.mobily.ws</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="msegat") { ?>
	            			<option value="msegat" selected="selected">Msegat.com - www.msegat.com</option>
	            		<?php }else{ ?>
	            			<option value="msegat">Msegat.com - www.msegat.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="msg91") { ?>
	            			<option value="msg91" selected="selected">MSG91 - www.msg91.com</option>
	            		<?php }else{ ?>
	            			<option value="msg91">MSG91 - www.msg91.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="mvaayoo") { ?>
	            			<option value="mvaayoo" selected="selected">mVaayoo - www.mvaayoo.com</option>
	            		<?php }else{ ?>
	            			<option value="mvaayoo">mVaayoo - www.mvaayoo.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="mysms") { ?>
	            			<option value="mysms" selected="selected">MySms.com.gr - www.mysms.com.gr</option>
	            		<?php }else{ ?>
	            			<option value="mysms">MySms.com.gr - www.mysms.com.gr</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="nexmo") { ?>
	            			<option value="nexmo" selected="selected">Nexmo - www.nexmo.com</option>
	            		<?php }else{ ?>
	            			<option value="nexmo">Nexmo - www.nexmo.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="netgsm") { ?>
	            			<option value="netgsm" selected="selected">Netgsm.com.tr - www.netgsm.com.tr</option>
	            		<?php }else{ ?>
	            			<option value="netgsm">Netgsm.com.tr - www.netgsm.com.tr</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="oneway") { ?>
	            			<option value="oneway" selected="selected">One Way SMS - www.onewaysms.com.my</option>
	            		<?php }else{ ?>
	            			<option value="oneway">One Way SMS - www.onewaysms.com.my</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="openhouse") { ?>
	            			<option value="openhouse" selected="selected">Openhouse IMI Mobile - www.openhouse.imimobile.com</option>
	            		<?php }else{ ?>
	            			<option value="openhouse">Openhouse IMI Mobile - www.openhouse.imimobile.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="redsms") { ?>
	            			<option value="redsms" selected="selected">Red SMS - www.redsms.in</option>
	            		<?php }else{ ?>
	            			<option value="redsms">Red SMS - www.redsms.in</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="routesms") { ?>
	            			<option value="routesms" selected="selected">Routesms - www.routesms.com</option>
	            		<?php }else{ ?>
	            			<option value="routesms">Routesms - www.routesms.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="smsboxcom") { ?>
	            			<option value="smsboxcom" selected="selected">SMSBox.com - www.smsbox.com</option>
	            		<?php }else{ ?>
	            			<option value="smsboxcom">SMSBox.com - www.smsbox.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="smsgatewayhub") { ?>
	            			<option value="smsgatewayhub" selected="selected">SMS GATEWAYHUB - www.smsgatewayhub.com</option>
	            		<?php }else{ ?>
	            			<option value="smsgatewayhub">SMS GATEWAYHUB - www.smsgatewayhub.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="smslane") { ?>
	            			<option value="smslane" selected="selected">SMS Lane - www.smslane.com</option>
	            		<?php }else{ ?>
	            			<option value="smslane">SMS Lane - www.smslane.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="smslaneg") { ?>
	            			<option value="smslaneg" selected="selected">SMSLane Global SMS - www.world.smslane.com</option>
	            		<?php }else{ ?>
	            			<option value="smslaneg">SMSLane Global SMS - www.world.smslane.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="smsnetgr") { ?>
	            			<option value="smsnetgr" selected="selected">SMS.net.gr - www.sms.net.gr</option>
	            		<?php }else{ ?>
	            			<option value="smsnetgr">SMS.net.gr - www.sms.net.gr</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="topsms") { ?>
	            			<option value="topsms" selected="selected">TOP SMS - www.topsms.mobi</option>
	            		<?php }else{ ?>
	            			<option value="topsms">TOP SMS - www.topsms.mobi</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="velti") { ?>
	            			<option value="velti" selected="selected">Velti - www.velti.com</option>
	            		<?php }else{ ?>
	            			<option value="velti">Velti - www.velti.com</option>
	            		<?php } ?>
	            		
	            		<?php if ($jossms_gateway=="zenziva") { ?>
	            			<option value="zenziva" selected="selected">Zenziva - www.zenziva.com</option>
	            		<?php }else{ ?>
	            			<option value="zenziva">Zenziva - www.zenziva.com</option>
	            		<?php } ?>
	            		
	              </select>
	              <?php if ($error_gateway) { ?>
	              <div class="text-danger"><?php echo $error_gateway; ?></div>
	              <?php } ?>
	          </div>
	      	</div>
	      
		      <div id="gateway-zenziva" class="gateway">
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey"><?php echo $entry_userkey; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey" value="<?php echo $jossms_userkey; ?>" placeholder="Userkey" id="input-userkey" class="form-control" />
				          <?php if ($error_userkey) { ?>
				            <div class="text-danger"><?php echo $error_userkey; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey"><?php echo $entry_passkey; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey" value="<?php echo $jossms_passkey; ?>" placeholder="Passkey" id="input-passkey" class="form-control" />
				          <?php if ($error_passkey) { ?>
				            <div class="text-danger"><?php echo $error_passkey; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi"><?php echo $entry_httpapi; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi" value="<?php echo $jossms_httpapi; ?>" placeholder="HTTP API URL" id="input-httpapi" class="form-control" /><?php echo $httpapi_example; ?>
				          <?php if ($error_httpapi) { ?>
				            <div class="text-danger"><?php echo $error_httpapi; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			    </div>
			    
			    <div id="gateway-clickatell" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-apiid-clickatell"><?php echo $entry_apiid_clickatell; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_apiid_clickatell" value="<?php echo $jossms_apiid_clickatell; ?>" placeholder="API ID" id="input-apiid-clickatell" class="form-control" />
				          <?php if ($error_apiid_clickatell) { ?>
				            <div class="text-danger"><?php echo $error_apiid_clickatell; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-clickatell"><?php echo $entry_userkey_clickatell; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_clickatell" value="<?php echo $jossms_userkey_clickatell; ?>" placeholder="User" id="input-userkey-clickatell" class="form-control" />
				          <?php if ($error_userkey_clickatell) { ?>
				            <div class="text-danger"><?php echo $error_userkey_clickatell; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-clickatell"><?php echo $entry_passkey_clickatell; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_clickatell" value="<?php echo $jossms_passkey_clickatell; ?>" placeholder="Password" id="input-passkey-clickatell" class="form-control" />
				          <?php if ($error_passkey_clickatell) { ?>
				            <div class="text-danger"><?php echo $error_passkey_clickatell; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-clickatell"><?php echo $entry_httpapi_clickatell; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_clickatell" value="<?php echo $jossms_httpapi_clickatell; ?>" placeholder="HTTP API URL" id="input-httpapi-clickatell" class="form-control" /><?php echo $httpapi_example_clickatell; ?>
				          <?php if ($error_httpapi_clickatell) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_clickatell; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-senderid-clickatell"><?php echo $entry_senderid_clickatell; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_clickatell" value="<?php echo $jossms_senderid_clickatell; ?>" placeholder="Sender ID" id="input-senderid-clickatell" class="form-control" />
				      </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-httpapi-clickatell"><?php echo $entry_unicode_clickatell; ?></label>
			        <div class="col-sm-9">
			        	<?php if ($jossms_config_unicode_clickatell) { ?>
                <input type="radio" name="jossms_config_unicode_clickatell" value="1" checked="checked" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_clickatell" value="0" /> <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="jossms_config_unicode_clickatell" value="1" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_clickatell" value="0" checked="checked" /> <?php echo $text_no; ?>
                <?php } ?>
			        </div>
			      </div>
			    </div>
			    
			    <div id="gateway-amd" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-amd"><?php echo $entry_userkey_amd; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_amd" value="<?php echo $jossms_userkey_amd; ?>" placeholder="Userkey" id="input-userkey-amd" class="form-control" />
				          <?php if ($error_userkey_amd) { ?>
				            <div class="text-danger"><?php echo $error_userkey_amd; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-amd"><?php echo $entry_passkey_amd; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_amd" value="<?php echo $jossms_passkey_amd; ?>" placeholder="Passkey" id="input-passkey-amd" class="form-control" />
				          <?php if ($error_passkey_amd) { ?>
				            <div class="text-danger"><?php echo $error_passkey_amd; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-amd"><?php echo $entry_httpapi_amd; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_amd" value="<?php echo $jossms_httpapi_amd; ?>" placeholder="HTTP API URL" id="input-httpapi-amd" class="form-control" /><?php echo $httpapi_example_amd; ?>
				          <?php if ($error_httpapi_amd) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_amd; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-amd"><?php echo $entry_senderid_amd; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_amd" value="<?php echo $jossms_senderid_amd; ?>" placeholder="Sender ID" id="input-senderid-amd" class="form-control" />
			          <?php if ($error_senderid_amd) { ?>
				          <div class="text-danger"><?php echo $error_senderid_amd; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-liveall" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-liveall"><?php echo $entry_userkey_liveall; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_liveall" value="<?php echo $jossms_userkey_liveall; ?>" placeholder="Username" id="input-userkey-liveall" class="form-control" />
				          <?php if ($error_userkey_liveall) { ?>
				            <div class="text-danger"><?php echo $error_userkey_liveall; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-liveall"><?php echo $entry_passkey_liveall; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_liveall" value="<?php echo $jossms_passkey_liveall; ?>" placeholder="Password" id="input-passkey-liveall" class="form-control" />
				          <?php if ($error_passkey_liveall) { ?>
				            <div class="text-danger"><?php echo $error_passkey_liveall; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-liveall"><?php echo $entry_httpapi_liveall; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_liveall" value="<?php echo $jossms_httpapi_liveall; ?>" placeholder="HTTP API URL" id="input-httpapi-liveall" class="form-control" /><?php echo $httpapi_example_liveall; ?>
				          <?php if ($error_httpapi_liveall) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_liveall; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-liveall"><?php echo $entry_senderid_liveall; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_liveall" value="<?php echo $jossms_senderid_liveall; ?>" placeholder="Sender ID" id="input-senderid-liveall" class="form-control" />
			          <?php if ($error_senderid_liveall) { ?>
				          <div class="text-danger"><?php echo $error_senderid_liveall; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-smsglobal" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-smsglobal"><?php echo $entry_userkey_smsglobal; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_smsglobal" value="<?php echo $jossms_userkey_smsglobal; ?>" placeholder="Username" id="input-userkey-smsglobal" class="form-control" />
				          <?php if ($error_userkey_smsglobal) { ?>
				            <div class="text-danger"><?php echo $error_userkey_smsglobal; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-smsglobal"><?php echo $entry_passkey_smsglobal; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_smsglobal" value="<?php echo $jossms_passkey_smsglobal; ?>" placeholder="Password" id="input-passkey-smsglobal" class="form-control" />
				          <?php if ($error_passkey_smsglobal) { ?>
				            <div class="text-danger"><?php echo $error_passkey_smsglobal; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smsglobal"><?php echo $entry_httpapi_smsglobal; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_smsglobal" value="<?php echo $jossms_httpapi_smsglobal; ?>" placeholder="HTTP API URL" id="input-httpapi-smsglobal" class="form-control" /><?php echo $httpapi_example_smsglobal; ?>
				          <?php if ($error_httpapi_smsglobal) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_smsglobal; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-smsglobal"><?php echo $entry_senderid_smsglobal; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_smsglobal" value="<?php echo $jossms_senderid_smsglobal; ?>" placeholder="Sender ID" id="input-senderid-smsglobal" class="form-control" />
			          <?php if ($error_senderid_smsglobal) { ?>
				          <div class="text-danger"><?php echo $error_senderid_smsglobal; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-malath" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-malath"><?php echo $entry_userkey_malath; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_malath" value="<?php echo $jossms_userkey_malath; ?>" placeholder="Username" id="input-userkey-malath" class="form-control" />
				          <?php if ($error_userkey_malath) { ?>
				            <div class="text-danger"><?php echo $error_userkey_malath; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-malath"><?php echo $entry_passkey_malath; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_malath" value="<?php echo $jossms_passkey_malath; ?>" placeholder="Password" id="input-passkey-malath" class="form-control" />
				          <?php if ($error_passkey_malath) { ?>
				            <div class="text-danger"><?php echo $error_passkey_malath; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-malath"><?php echo $entry_httpapi_malath; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_malath" value="<?php echo $jossms_httpapi_malath; ?>" placeholder="HTTP API URL" id="input-httpapi-malath" class="form-control" /><?php echo $httpapi_example_malath; ?>
				          <?php if ($error_httpapi_malath) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_malath; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-senderid-malath"><?php echo $entry_senderid_malath; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_malath" value="<?php echo $jossms_senderid_malath; ?>" placeholder="Sender ID" id="input-senderid-malath" class="form-control" />
				      	<?php if ($error_senderid_malath) { ?>
				            <div class="text-danger"><?php echo $error_senderid_malath; ?></div>
				            <?php } ?>
				      </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-httpapi-malath"><?php echo $entry_unicode_malath; ?></label>
			        <div class="col-sm-9">
			        	<?php if ($jossms_config_unicode_malath) { ?>
                <input type="radio" name="jossms_config_unicode_malath" value="1" checked="checked" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_malath" value="0" /> <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="jossms_config_unicode_malath" value="1" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_malath" value="0" checked="checked" /> <?php echo $text_no; ?>
                <?php } ?>
			        </div>
			      </div>
			    </div>
			    
			    <div id="gateway-mobily" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-mobily"><?php echo $entry_userkey_mobily; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_mobily" value="<?php echo $jossms_userkey_mobily; ?>" placeholder="Mobile" id="input-userkey-mobily" class="form-control" />
				          <?php if ($error_userkey_mobily) { ?>
				            <div class="text-danger"><?php echo $error_userkey_mobily; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-mobily"><?php echo $entry_passkey_mobily; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_mobily" value="<?php echo $jossms_passkey_mobily; ?>" placeholder="Password" id="input-passkey-mobily" class="form-control" />
				          <?php if ($error_passkey_mobily) { ?>
				            <div class="text-danger"><?php echo $error_passkey_mobily; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-mobily"><?php echo $entry_httpapi_mobily; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_mobily" value="<?php echo $jossms_httpapi_mobily; ?>" placeholder="HTTP API URL" id="input-httpapi-mobily" class="form-control" /><?php echo $httpapi_example_mobily; ?>
				          <?php if ($error_httpapi_mobily) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_mobily; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-mobily"><?php echo $entry_senderid_mobily; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_mobily" value="<?php echo $jossms_senderid_mobily; ?>" placeholder="Sender Name" id="input-senderid-mobily" class="form-control" />
			          <?php if ($error_senderid_mobily) { ?>
				          <div class="text-danger"><?php echo $error_senderid_mobily; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-msegat" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-msegat"><?php echo $entry_userkey_msegat; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_msegat" value="<?php echo $jossms_userkey_msegat; ?>" placeholder="Username" id="input-userkey-msegat" class="form-control" />
				          <?php if ($error_userkey_msegat) { ?>
				            <div class="text-danger"><?php echo $error_userkey_msegat; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-msegat"><?php echo $entry_passkey_msegat; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_msegat" value="<?php echo $jossms_passkey_msegat; ?>" placeholder="Password" id="input-passkey-msegat" class="form-control" />
				          <?php if ($error_passkey_msegat) { ?>
				            <div class="text-danger"><?php echo $error_passkey_msegat; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-msegat"><?php echo $entry_httpapi_msegat; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_msegat" value="<?php echo $jossms_httpapi_msegat; ?>" placeholder="HTTP API URL" id="input-httpapi-msegat" class="form-control" /><?php echo $httpapi_example_msegat; ?>
				          <?php if ($error_httpapi_msegat) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_msegat; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-msegat"><?php echo $entry_senderid_msegat; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_msegat" value="<?php echo $jossms_senderid_msegat; ?>" placeholder="Sender ID" id="input-senderid-msegat" class="form-control" />
			          <?php if ($error_senderid_msegat) { ?>
				          <div class="text-danger"><?php echo $error_senderid_msegat; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-msg91" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-msg91"><?php echo $entry_userkey_msg91; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_msg91" value="<?php echo $jossms_userkey_msg91; ?>" placeholder="Authentication Key" id="input-userkey-msg91" class="form-control" />
				          <?php if ($error_userkey_msg91) { ?>
				            <div class="text-danger"><?php echo $error_userkey_msg91; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-route-msg91"><?php echo $entry_route_msg91; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_route_msg91" value="<?php echo $jossms_route_msg91; ?>" placeholder="Route" id="input-route-msg91" class="form-control" />
				          <?php if ($error_route_msg91) { ?>
				            <div class="text-danger"><?php echo $error_route_msg91; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-msg91"><?php echo $entry_httpapi_msg91; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_msg91" value="<?php echo $jossms_httpapi_msg91; ?>" placeholder="HTTP API URL" id="input-httpapi-msg91" class="form-control" /><?php echo $httpapi_example_msg91; ?>
				          <?php if ($error_httpapi_msg91) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_msg91; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-msg91"><?php echo $entry_senderid_msg91; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_msg91" value="<?php echo $jossms_senderid_msg91; ?>" placeholder="Sender ID" id="input-senderid-msg91" class="form-control" />
			          <?php if ($error_senderid_msg91) { ?>
				          <div class="text-danger"><?php echo $error_senderid_msg91; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-mvaayoo" class="gateway">
			    	<div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-userkey-mvaayoo"><?php echo $entry_term_mvaayoo; ?></label>
			        <div class="col-sm-9">
			        	<div class="alert alert-warning"><?php echo $text_term_mvaayoo; ?></div>
			        </div>
			      </div>
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-mvaayoo"><?php echo $entry_userkey_mvaayoo; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_mvaayoo" value="<?php echo $jossms_userkey_mvaayoo; ?>" placeholder="Username" id="input-userkey-mvaayoo" class="form-control" />
				          <?php if ($error_userkey_mvaayoo) { ?>
				            <div class="text-danger"><?php echo $error_userkey_mvaayoo; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-mvaayoo"><?php echo $entry_passkey_mvaayoo; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_mvaayoo" value="<?php echo $jossms_passkey_mvaayoo; ?>" placeholder="Password" id="input-passkey-mvaayoo" class="form-control" />
				          <?php if ($error_passkey_mvaayoo) { ?>
				            <div class="text-danger"><?php echo $error_passkey_mvaayoo; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-mvaayoo"><?php echo $entry_httpapi_mvaayoo; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_mvaayoo" value="<?php echo $jossms_httpapi_mvaayoo; ?>" placeholder="HTTP API URL" id="input-httpapi-mvaayoo" class="form-control" /><?php echo $httpapi_example_mvaayoo; ?>
				          <?php if ($error_httpapi_mvaayoo) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_mvaayoo; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-mvaayoo"><?php echo $entry_senderid_mvaayoo; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_mvaayoo" value="<?php echo $jossms_senderid_mvaayoo; ?>" placeholder="Sender ID" id="input-senderid-mvaayoo" class="form-control" />
			          <?php if ($error_senderid_mvaayoo) { ?>
				          <div class="text-danger"><?php echo $error_senderid_mvaayoo; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-mysms" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-mysms"><?php echo $entry_userkey_mysms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_mysms" value="<?php echo $jossms_userkey_mysms; ?>" placeholder="Username" id="input-userkey-mysms" class="form-control" />
				          <?php if ($error_userkey_mysms) { ?>
				            <div class="text-danger"><?php echo $error_userkey_mysms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-mysms"><?php echo $entry_passkey_mysms; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_mysms" value="<?php echo $jossms_passkey_mysms; ?>" placeholder="Password" id="input-passkey-mysms" class="form-control" />
				          <?php if ($error_passkey_mysms) { ?>
				            <div class="text-danger"><?php echo $error_passkey_mysms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-mysms"><?php echo $entry_httpapi_mysms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_mysms" value="<?php echo $jossms_httpapi_mysms; ?>" placeholder="HTTP API URL" id="input-httpapi-mysms" class="form-control" /><?php echo $httpapi_example_mysms; ?>
				          <?php if ($error_httpapi_mysms) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_mysms; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-mysms"><?php echo $entry_senderid_mysms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_mysms" value="<?php echo $jossms_senderid_mysms; ?>" placeholder="Sender ID" id="input-senderid-mysms" class="form-control" />
			          <?php if ($error_senderid_mysms) { ?>
				          <div class="text-danger"><?php echo $error_senderid_mysms; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-nexmo" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-nexmo"><?php echo $entry_userkey_nexmo; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_nexmo" value="<?php echo $jossms_userkey_nexmo; ?>" placeholder="Key" id="input-userkey-nexmo" class="form-control" />
				          <?php if ($error_userkey_nexmo) { ?>
				            <div class="text-danger"><?php echo $error_userkey_nexmo; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-nexmo"><?php echo $entry_passkey_nexmo; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_nexmo" value="<?php echo $jossms_passkey_nexmo; ?>" placeholder="Secret" id="input-passkey-nexmo" class="form-control" />
				          <?php if ($error_passkey_nexmo) { ?>
				            <div class="text-danger"><?php echo $error_passkey_nexmo; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-nexmo"><?php echo $entry_httpapi_nexmo; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_nexmo" value="<?php echo $jossms_httpapi_nexmo; ?>" placeholder="HTTP API URL" id="input-httpapi-nexmo" class="form-control" /><?php echo $httpapi_example_nexmo; ?>
				          <?php if ($error_httpapi_nexmo) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_nexmo; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-senderid-nexmo"><?php echo $entry_senderid_nexmo; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_nexmo" value="<?php echo $jossms_senderid_nexmo; ?>" placeholder="Sender ID" id="input-senderid-nexmo" class="form-control" />
				      	<?php if ($error_senderid_nexmo) { ?>
				            <div class="text-danger"><?php echo $error_senderid_nexmo; ?></div>
				            <?php } ?>
				      </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-httpapi-nexmo"><?php echo $entry_unicode_nexmo; ?></label>
			        <div class="col-sm-9">
			        	<?php if ($jossms_config_unicode_nexmo) { ?>
                <input type="radio" name="jossms_config_unicode_nexmo" value="1" checked="checked" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_nexmo" value="0" /> <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="jossms_config_unicode_nexmo" value="1" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_nexmo" value="0" checked="checked" /> <?php echo $text_no; ?>
                <?php } ?>
			        </div>
			      </div>
			    </div>
			    
			    <div id="gateway-netgsm" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-netgsm"><?php echo $entry_userkey_netgsm; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_netgsm" value="<?php echo $jossms_userkey_netgsm; ?>" placeholder="Username" id="input-userkey-netgsm" class="form-control" />
				          <?php if ($error_userkey_netgsm) { ?>
				            <div class="text-danger"><?php echo $error_userkey_netgsm; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-netgsm"><?php echo $entry_passkey_netgsm; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_netgsm" value="<?php echo $jossms_passkey_netgsm; ?>" placeholder="Password" id="input-passkey-netgsm" class="form-control" />
				          <?php if ($error_passkey_netgsm) { ?>
				            <div class="text-danger"><?php echo $error_passkey_netgsm; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-netgsm"><?php echo $entry_httpapi_netgsm; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_netgsm" value="<?php echo $jossms_httpapi_netgsm; ?>" placeholder="HTTP API URL" id="input-httpapi-netgsm" class="form-control" /><?php echo $httpapi_example_netgsm; ?>
				          <?php if ($error_httpapi_netgsm) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_netgsm; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-netgsm"><?php echo $entry_senderid_netgsm; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_netgsm" value="<?php echo $jossms_senderid_netgsm; ?>" placeholder="Sender ID" id="input-senderid-netgsm" class="form-control" />
			          <?php if ($error_senderid_netgsm) { ?>
				          <div class="text-danger"><?php echo $error_senderid_netgsm; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-oneway" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-oneway"><?php echo $entry_userkey_oneway; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_oneway" value="<?php echo $jossms_userkey_oneway; ?>" placeholder="API Username" id="input-userkey-oneway" class="form-control" />
				          <?php if ($error_userkey_oneway) { ?>
				            <div class="text-danger"><?php echo $error_userkey_oneway; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-oneway"><?php echo $entry_passkey_oneway; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_oneway" value="<?php echo $jossms_passkey_oneway; ?>" placeholder="API Password" id="input-passkey-oneway" class="form-control" />
				          <?php if ($error_passkey_oneway) { ?>
				            <div class="text-danger"><?php echo $error_passkey_oneway; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-oneway"><?php echo $entry_httpapi_oneway; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_oneway" value="<?php echo $jossms_httpapi_oneway; ?>" placeholder="MT URL" id="input-httpapi-oneway" class="form-control" /><?php echo $httpapi_example_oneway; ?>
				          <?php if ($error_httpapi_oneway) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_oneway; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-oneway"><?php echo $entry_senderid_oneway; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_oneway" value="<?php echo $jossms_senderid_oneway; ?>" placeholder="Sender ID" id="input-senderid-oneway" class="form-control" />
			          <?php if ($error_senderid_oneway) { ?>
				          <div class="text-danger"><?php echo $error_senderid_oneway; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-openhouse" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-openhouse"><?php echo $entry_userkey_openhouse; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_openhouse" value="<?php echo $jossms_userkey_openhouse; ?>" placeholder="Key" id="input-userkey-openhouse" class="form-control" />
				          <?php if ($error_userkey_openhouse) { ?>
				            <div class="text-danger"><?php echo $error_userkey_openhouse; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-openhouse"><?php echo $entry_passkey_openhouse; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_passkey_openhouse" value="<?php echo $jossms_passkey_openhouse; ?>" placeholder="Sender Address" id="input-passkey-openhouse" class="form-control" />
				          <?php if ($error_passkey_openhouse) { ?>
				            <div class="text-danger"><?php echo $error_passkey_openhouse; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-openhouse"><?php echo $entry_senderid_openhouse; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_openhouse" value="<?php echo $jossms_senderid_openhouse; ?>" placeholder="Sender ID" id="input-senderid-openhouse" class="form-control" />
			          <?php if ($error_senderid_openhouse) { ?>
				          <div class="text-danger"><?php echo $error_senderid_openhouse; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-redsms" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-redsms"><?php echo $entry_userkey_redsms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_redsms" value="<?php echo $jossms_userkey_redsms; ?>" placeholder="Username" id="input-userkey-redsms" class="form-control" />
				          <?php if ($error_userkey_redsms) { ?>
				            <div class="text-danger"><?php echo $error_userkey_redsms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-redsms"><?php echo $entry_passkey_redsms; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_redsms" value="<?php echo $jossms_passkey_redsms; ?>" placeholder="Password" id="input-passkey-redsms" class="form-control" />
				          <?php if ($error_passkey_redsms) { ?>
				            <div class="text-danger"><?php echo $error_passkey_redsms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-redsms"><?php echo $entry_httpapi_redsms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_redsms" value="<?php echo $jossms_httpapi_redsms; ?>" placeholder="HTTP API" id="input-httpapi-redsms" class="form-control" /><?php echo $httpapi_example_redsms; ?>
				          <?php if ($error_httpapi_redsms) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_redsms; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-redsms"><?php echo $entry_senderid_redsms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_redsms" value="<?php echo $jossms_senderid_redsms; ?>" placeholder="Sender ID" id="input-senderid-redsms" class="form-control" />
			          <?php if ($error_senderid_redsms) { ?>
				          <div class="text-danger"><?php echo $error_senderid_redsms; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-routesms" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-routesms"><?php echo $entry_userkey_routesms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_routesms" value="<?php echo $jossms_userkey_routesms; ?>" placeholder="Username" id="input-userkey-routesms" class="form-control" />
				          <?php if ($error_userkey_routesms) { ?>
				            <div class="text-danger"><?php echo $error_userkey_routesms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-routesms"><?php echo $entry_passkey_routesms; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_routesms" value="<?php echo $jossms_passkey_routesms; ?>" placeholder="Password" id="input-passkey-routesms" class="form-control" />
				          <?php if ($error_passkey_routesms) { ?>
				            <div class="text-danger"><?php echo $error_passkey_routesms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-routesms"><?php echo $entry_httpapi_routesms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_routesms" value="<?php echo $jossms_httpapi_routesms; ?>" placeholder="HTTP API" id="input-httpapi-routesms" class="form-control" /><?php echo $httpapi_example_routesms; ?>
				          <?php if ($error_httpapi_routesms) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_routesms; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-routesms"><?php echo $entry_senderid_routesms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_routesms" value="<?php echo $jossms_senderid_routesms; ?>" placeholder="Sender ID" id="input-senderid-routesms" class="form-control" />
			          <?php if ($error_senderid_routesms) { ?>
				          <div class="text-danger"><?php echo $error_senderid_routesms; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-smsboxcom" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-apiid-smsboxcom"><?php echo $entry_apiid_smsboxcom; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_apiid_smsboxcom" value="<?php echo $jossms_apiid_smsboxcom; ?>" placeholder="Customer ID" id="input-apiid-smsboxcom" class="form-control" />
				          <?php if ($error_apiid_smsboxcom) { ?>
				            <div class="text-danger"><?php echo $error_apiid_smsboxcom; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-smsboxcom"><?php echo $entry_userkey_smsboxcom; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_smsboxcom" value="<?php echo $jossms_userkey_smsboxcom; ?>" placeholder="Username" id="input-userkey-smsboxcom" class="form-control" />
				          <?php if ($error_userkey_smsboxcom) { ?>
				            <div class="text-danger"><?php echo $error_userkey_smsboxcom; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-smsboxcom"><?php echo $entry_passkey_smsboxcom; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_smsboxcom" value="<?php echo $jossms_passkey_smsboxcom; ?>" placeholder="Password" id="input-passkey-smsboxcom" class="form-control" />
				          <?php if ($error_passkey_smsboxcom) { ?>
				            <div class="text-danger"><?php echo $error_passkey_smsboxcom; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smsboxcom"><?php echo $entry_httpapi_smsboxcom; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_smsboxcom" value="<?php echo $jossms_httpapi_smsboxcom; ?>" placeholder="HTTP API URL" id="input-httpapi-smsboxcom" class="form-control" /><?php echo $httpapi_example_smsboxcom; ?>
				          <?php if ($error_httpapi_smsboxcom) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_smsboxcom; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-senderid-smsboxcom"><?php echo $entry_senderid_smsboxcom; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_smsboxcom" value="<?php echo $jossms_senderid_smsboxcom; ?>" placeholder="Sender ID" id="input-senderid-smsboxcom" class="form-control" />
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-smsgatewayhub" class="gateway">
			    	<div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-userkey-smsgatewayhub"><?php echo $entry_term_smsgatewayhub; ?></label>
			        <div class="col-sm-9">
			        	<div class="alert alert-warning"><?php echo $text_term_smsgatewayhub; ?></div>
			        </div>
			      </div>
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-smsgatewayhub"><?php echo $entry_userkey_smsgatewayhub; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_smsgatewayhub" value="<?php echo $jossms_userkey_smsgatewayhub; ?>" placeholder="User" id="input-userkey-smsgatewayhub" class="form-control" />
				          <?php if ($error_userkey_smsgatewayhub) { ?>
				            <div class="text-danger"><?php echo $error_userkey_smsgatewayhub; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-smsgatewayhub"><?php echo $entry_passkey_smsgatewayhub; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_smsgatewayhub" value="<?php echo $jossms_passkey_smsgatewayhub; ?>" placeholder="Password" id="input-passkey-smsgatewayhub" class="form-control" />
				          <?php if ($error_passkey_smsgatewayhub) { ?>
				            <div class="text-danger"><?php echo $error_passkey_smsgatewayhub; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smsgatewayhub"><?php echo $entry_httpapi_smsgatewayhub; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_smsgatewayhub" value="<?php echo $jossms_httpapi_smsgatewayhub; ?>" placeholder="HTTP API" id="input-httpapi-smsgatewayhub" class="form-control" /><?php echo $httpapi_example_smsgatewayhub; ?>
				          <?php if ($error_httpapi_smsgatewayhub) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_smsgatewayhub; ?></div>
				          <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-senderid-smsgatewayhub"><?php echo $entry_senderid_smsgatewayhub; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_smsgatewayhub" value="<?php echo $jossms_senderid_smsgatewayhub; ?>" placeholder="Sender ID" id="input-senderid-smsgatewayhub" class="form-control" />
			          <?php if ($error_senderid_smsgatewayhub) { ?>
				          <div class="text-danger"><?php echo $error_senderid_smsgatewayhub; ?></div>
				        <?php } ?>
				      </div>
			      </div>
			    </div>
			    
			    <div id="gateway-smslane" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-smslane"><?php echo $entry_userkey_smslane; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_smslane" value="<?php echo $jossms_userkey_smslane; ?>" placeholder="Username" id="input-userkey-smslane" class="form-control" />
				          <?php if ($error_userkey_smslane) { ?>
				            <div class="text-danger"><?php echo $error_userkey_smslane; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-smslane"><?php echo $entry_passkey_smslane; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_smslane" value="<?php echo $jossms_passkey_smslane; ?>" placeholder="Password" id="input-passkey-smslane" class="form-control" />
				          <?php if ($error_passkey_smslane) { ?>
				            <div class="text-danger"><?php echo $error_passkey_smslane; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smslane"><?php echo $entry_httpapi_smslane; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_smslane" value="<?php echo $jossms_httpapi_smslane; ?>" placeholder="HTTP API URL" id="input-httpapi-smslane" class="form-control" /><?php echo $httpapi_example_smslane; ?>
				          <?php if ($error_httpapi_smslane) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_smslane; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-senderid-smslane"><?php echo $entry_senderid_smslane; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_smslane" value="<?php echo $jossms_senderid_smslane; ?>" placeholder="Sender ID" id="input-senderid-smslane" class="form-control" />
				      	<?php if ($error_senderid_smslane) { ?>
				            <div class="text-danger"><?php echo $error_senderid_smslane; ?></div>
				            <?php } ?>
				      </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smslane"><?php echo $entry_transactional_smslane; ?></label>
			        <div class="col-sm-9">
			        	<?php if ($jossms_config_transactional_smslane) { ?>
                <input type="radio" name="jossms_config_transactional_smslane" value="1" checked="checked" /> <?php echo $text_transactional_smslane; ?>&nbsp;
                <input type="radio" name="jossms_config_transactional_smslane" value="0" /> <?php echo $text_promotional_smslane; ?>
                <?php } else { ?>
                <input type="radio" name="jossms_config_transactional_smslane" value="1" /> <?php echo $text_transactional_smslane; ?>&nbsp;
                <input type="radio" name="jossms_config_transactional_smslane" value="0" checked="checked" /> <?php echo $text_promotional_smslane; ?>
                <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smslane"><?php echo $entry_unicode_smslane; ?></label>
			        <div class="col-sm-9">
			        	<?php if ($jossms_config_unicode_smslane) { ?>
                <input type="radio" name="jossms_config_unicode_smslane" value="1" checked="checked" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_smslane" value="0" /> <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="jossms_config_unicode_smslane" value="1" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_smslane" value="0" checked="checked" /> <?php echo $text_no; ?>
                <?php } ?>
			        </div>
			      </div>
			    </div>
			    
			    <div id="gateway-smslaneg" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-smslaneg"><?php echo $entry_userkey_smslaneg; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_smslaneg" value="<?php echo $jossms_userkey_smslaneg; ?>" placeholder="Username" id="input-userkey-smslaneg" class="form-control" />
				          <?php if ($error_userkey_smslaneg) { ?>
				            <div class="text-danger"><?php echo $error_userkey_smslaneg; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-smslaneg"><?php echo $entry_passkey_smslaneg; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_smslaneg" value="<?php echo $jossms_passkey_smslaneg; ?>" placeholder="Password" id="input-passkey-smslaneg" class="form-control" />
				          <?php if ($error_passkey_smslaneg) { ?>
				            <div class="text-danger"><?php echo $error_passkey_smslaneg; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smslaneg"><?php echo $entry_httpapi_smslaneg; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_smslaneg" value="<?php echo $jossms_httpapi_smslaneg; ?>" placeholder="HTTP API URL" id="input-httpapi-smslaneg" class="form-control" /><?php echo $httpapi_example_smslaneg; ?>
				          <?php if ($error_httpapi_smslaneg) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_smslaneg; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-senderid-smslaneg"><?php echo $entry_senderid_smslaneg; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_smslaneg" value="<?php echo $jossms_senderid_smslaneg; ?>" placeholder="Sender ID" id="input-senderid-smslaneg" class="form-control" />
				      	<?php if ($error_senderid_smslaneg) { ?>
				            <div class="text-danger"><?php echo $error_senderid_smslaneg; ?></div>
				            <?php } ?>
				      </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smslaneg"><?php echo $entry_unicode_smslaneg; ?></label>
			        <div class="col-sm-9">
			        	<?php if ($jossms_config_unicode_smslaneg) { ?>
                <input type="radio" name="jossms_config_unicode_smslaneg" value="1" checked="checked" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_smslaneg" value="0" /> <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="jossms_config_unicode_smslaneg" value="1" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_smslaneg" value="0" checked="checked" /> <?php echo $text_no; ?>
                <?php } ?>
			        </div>
			      </div>
			    </div>
			    
			    <div id="gateway-smsnetgr" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-apiid-smsnetgr"><?php echo $entry_apiid_smsnetgr; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_apiid_smsnetgr" value="<?php echo $jossms_apiid_smsnetgr; ?>" placeholder="API Token" id="input-apiid-smsnetgr" class="form-control" />
				          <?php if ($error_apiid_smsnetgr) { ?>
				            <div class="text-danger"><?php echo $error_apiid_smsnetgr; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-smsnetgr"><?php echo $entry_userkey_smsnetgr; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_smsnetgr" value="<?php echo $jossms_userkey_smsnetgr; ?>" placeholder="Username" id="input-userkey-smsnetgr" class="form-control" />
				          <?php if ($error_userkey_smsnetgr) { ?>
				            <div class="text-danger"><?php echo $error_userkey_smsnetgr; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-smsnetgr"><?php echo $entry_passkey_smsnetgr; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_smsnetgr" value="<?php echo $jossms_passkey_smsnetgr; ?>" placeholder="Password" id="input-passkey-smsnetgr" class="form-control" />
				          <?php if ($error_passkey_smsnetgr) { ?>
				            <div class="text-danger"><?php echo $error_passkey_smsnetgr; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smsnetgr"><?php echo $entry_httpapi_smsnetgr; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_smsnetgr" value="<?php echo $jossms_httpapi_smsnetgr; ?>" placeholder="HTTP API URL" id="input-httpapi-smsnetgr" class="form-control" /><?php echo $httpapi_example_smsnetgr; ?>
				          <?php if ($error_httpapi_smsnetgr) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_smsnetgr; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-senderid-smsnetgr"><?php echo $entry_senderid_smsnetgr; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_smsnetgr" value="<?php echo $jossms_senderid_smsnetgr; ?>" placeholder="Sender ID" id="input-senderid-smsnetgr" class="form-control" />
				      </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-httpapi-smsnetgr"><?php echo $entry_unicode_smsnetgr; ?></label>
			        <div class="col-sm-9">
			        	<?php if ($jossms_config_unicode_smsnetgr) { ?>
                <input type="radio" name="jossms_config_unicode_smsnetgr" value="1" checked="checked" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_smsnetgr" value="0" /> <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="jossms_config_unicode_smsnetgr" value="1" /> <?php echo $text_yes; ?>
                <input type="radio" name="jossms_config_unicode_smsnetgr" value="0" checked="checked" /> <?php echo $text_no; ?>
                <?php } ?>
			        </div>
			      </div>
			    </div>
			    
			    <div id="gateway-topsms" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-topsms"><?php echo $entry_userkey_topsms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_topsms" value="<?php echo $jossms_userkey_topsms; ?>" placeholder="Username" id="input-userkey-topsms" class="form-control" />
				          <?php if ($error_userkey_topsms) { ?>
				            <div class="text-danger"><?php echo $error_userkey_topsms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-topsms"><?php echo $entry_passkey_topsms; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_topsms" value="<?php echo $jossms_passkey_topsms; ?>" placeholder="Password" id="input-passkey-topsms" class="form-control" />
				          <?php if ($error_passkey_topsms) { ?>
				            <div class="text-danger"><?php echo $error_passkey_topsms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-topsms"><?php echo $entry_httpapi_topsms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_topsms" value="<?php echo $jossms_httpapi_topsms; ?>" placeholder="HTTP API URL" id="input-httpapi-topsms" class="form-control" /><?php echo $httpapi_example_topsms; ?>
				          <?php if ($error_httpapi_topsms) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_topsms; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-senderid-topsms"><?php echo $entry_senderid_topsms; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_senderid_topsms" value="<?php echo $jossms_senderid_topsms; ?>" placeholder="Sender ID" id="input-senderid-topsms" class="form-control" />
				      	<?php if ($error_senderid_topsms) { ?>
				            <div class="text-danger"><?php echo $error_senderid_topsms; ?></div>
				            <?php } ?>
				      </div>
			      </div>
			      <div class="form-group">
			      	<label class="col-sm-3 control-label" for="input-httpapi-topsms"><?php echo $entry_lang_topsms; ?></label>
			        <div class="col-sm-9">
			        	<?php if ($jossms_config_lang_topsms) { ?>
                <input type="radio" name="jossms_config_lang_topsms" value="1" checked="checked" /> <?php echo $text_ar; ?>
                <input type="radio" name="jossms_config_lang_topsms" value="0" /> <?php echo $text_en; ?>
                <?php } else { ?>
                <input type="radio" name="jossms_config_lang_topsms" value="1" /> <?php echo $text_ar; ?>
                <input type="radio" name="jossms_config_lang_topsms" value="0" checked="checked" /> <?php echo $text_en; ?>
                <?php } ?>
			        </div>
			      </div>
			    </div>
			    
			    <div id="gateway-velti" class="gateway">
			    	<div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-userkey-velti"><?php echo $entry_userkey_velti; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_userkey_velti" value="<?php echo $jossms_userkey_velti; ?>" placeholder="AID" id="input-userkey-velti" class="form-control" />
				          <?php if ($error_userkey_velti) { ?>
				            <div class="text-danger"><?php echo $error_userkey_velti; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-passkey-velti"><?php echo $entry_passkey_velti; ?></label>
			        <div class="col-sm-9">
			          <input type="password" name="jossms_passkey_velti" value="<?php echo $jossms_passkey_velti; ?>" placeholder="PIN" id="input-passkey-velti" class="form-control" />
				          <?php if ($error_passkey_velti) { ?>
				            <div class="text-danger"><?php echo $error_passkey_velti; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			      <div class="form-group required">
			      	<label class="col-sm-3 control-label" for="input-httpapi-velti"><?php echo $entry_httpapi_velti; ?></label>
			        <div class="col-sm-9">
			          <input type="text" name="jossms_httpapi_velti" value="<?php echo $jossms_httpapi_velti; ?>" placeholder="HTTP API" id="input-httpapi-velti" class="form-control" /><?php echo $httpapi_example_velti; ?>
				          <?php if ($error_httpapi_velti) { ?>
				            <div class="text-danger"><?php echo $error_httpapi_velti; ?></div>
				            <?php } ?>
			        </div>
			      </div>
			    </div>
			    <!--
			    <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-smslimit"><?php echo $entry_smslimit; ?></label>
			      <div class="col-sm-1">
			        <input type="text" name="jossms_smslimit" value="<?php echo $jossms_smslimit; ?>" placeholder="" id="input-smslimit" class="form-control" />
			      </div>
			    </div>
			    -->
			    <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-message_reg"><?php echo $entry_alert_reg; ?></label>
			      <div class="col-sm-9">
			        <textarea name="jossms_message_reg" rows="8" class="form-control" id="input-message_reg" ><?php echo $jossms_message_reg; ?></textarea><?php echo $entry_alert_blank; ?>
			      </div>
			    </div>
			    
			    <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-message_order"><?php echo $entry_alert_order; ?></label>
			      <div class="col-sm-9">
			        <textarea name="jossms_message_order" rows="8" class="form-control" id="input-message_order" ><?php echo $jossms_message_order; ?></textarea><?php echo $entry_alert_blank; ?><br /><?php echo $entry_parsing; ?>
			      </div>
			    </div>
			    
			    <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-status"><?php echo $entry_status_order_alert; ?></label>
			      <div class="col-sm-9">
			        <select name="jossms_status" id="input-status" class="form-control">
            		<option value=""><?php echo $text_status_none; ?></option>
            		
            		<?php foreach ($order_statuses as $order_status) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                
              </select>
			      </div>
			    </div>
			    
			    <?php foreach ($order_statuses as $order_status) { ?>
	          <div id="status-<?php echo $order_status['order_status_id'] ?>" class="status">
	          	<div class="form-group">
					    	<label class="col-sm-3 control-label" for="input-message_changestate_<?php echo $order_status['order_status_id'] ?>"><?php echo $order_status['name'].' '.$entry_alert_changestate; ?></label>
					      <div class="col-sm-9">
					        <textarea name="jossms_message_changestate_<?php echo $order_status['order_status_id'] ?>" rows="8" class="form-control" id="input-message_changestate_<?php echo $order_status['order_status_id'] ?>" ><?php echo ${"jossms_message_changestate_" . $order_status['order_status_id']}; ?></textarea><?php echo $entry_alert_blank; ?>
					      </div>
					    </div>
			      </div>
          <?php } ?>
          
          <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-status"><?php echo $entry_status_return_alert; ?></label>
			      <div class="col-sm-9">
			        <select name="jossms_return_status" id="input-status" class="form-control">
            		<option value=""><?php echo $text_status_none; ?></option>
            		
            		<?php foreach ($return_statuses as $return_status) { ?>
                <option value="<?php echo $return_status['return_status_id']; ?>"><?php echo $return_status['name']; ?></option>
                <?php } ?>
                
              </select>
			      </div>
			    </div>
			    
			    <?php foreach ($return_statuses as $return_status) { ?>
	          <div id="return-status-<?php echo $return_status['return_status_id'] ?>" class="return-status">
	          	<div class="form-group">
					    	<label class="col-sm-3 control-label" for="input-message_returnstate_<?php echo $return_status['return_status_id'] ?>"><?php echo $return_status['name'].' '.$entry_alert_returnstate; ?></label>
					      <div class="col-sm-9">
					        <textarea name="jossms_message_returnstate_<?php echo $return_status['return_status_id'] ?>" rows="8" class="form-control" id="input-message_returnstate_<?php echo $return_status['return_status_id'] ?>" ><?php echo ${"jossms_message_returnstate_" . $return_status['return_status_id']}; ?></textarea><?php echo $entry_alert_blank; ?>
					      </div>
					    </div>
			      </div>
          <?php } ?>
          
          <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-config_alert_sms"><?php echo $entry_alert_sms; ?></label>
			      <div class="col-sm-9">
			        <textarea name="jossms_config_alert_sms" rows="8" class="form-control" id="input-config_alert_sms" ><?php echo $jossms_config_alert_sms; ?></textarea><?php echo $entry_alert_blank; ?><br /><?php echo $entry_parsing; ?>
			      </div>
			    </div>
			    
			    <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-config_account_sms"><?php echo $entry_account_sms; ?></label>
			      <div class="col-sm-9">
			        <textarea name="jossms_config_account_sms" rows="8" class="form-control" id="input-config_account_sms" ><?php echo $jossms_config_account_sms; ?></textarea><?php echo $entry_alert_blank; ?>
			      </div>
			    </div>
			    
			    <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-config_return_sms"><?php echo $entry_return_sms; ?></label>
			      <div class="col-sm-9">
			        <textarea name="jossms_config_return_sms" rows="8" class="form-control" id="input-config_return_sms" ><?php echo $jossms_config_return_sms; ?></textarea><?php echo $entry_alert_blank; ?><br /><?php echo $entry_parsing_return; ?>
			      </div>
			    </div>
			    
			    <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-message_alert"><?php echo $entry_additional_alert; ?></label>
			      <div class="col-sm-9">
			        <textarea name="jossms_message_alert" rows="8" class="form-control" id="input-message_alert" ><?php echo $jossms_message_alert; ?></textarea><?php echo $entry_alert_blank; ?>
			      </div>
			    </div>
			    
			    <div class="form-group">
			    	<label class="col-sm-3 control-label" for="input-verify"><?php echo $text_enable_verify; ?></label>
			      <div class="col-sm-9">
			        <select name="jossms_verify" id="input-verify" class="form-control">
            		<?php if ($jossms_verify==0) { ?>
            			<option value="0" selected="selected"><?php echo $text_no; ?></option>
            		<?php }else{ ?>
            			<option value="0"><?php echo $text_no; ?></option>
            		<?php } ?>
            		
            		<?php if ($jossms_verify==1) { ?>
            			<option value="1" selected="selected"><?php echo $text_yes; ?></option>
            		<?php }else{ ?>
            			<option value="1"><?php echo $text_yes; ?></option>
            		<?php } ?>
                
              </select>
			      </div>
			    </div>
			    
			    <div id="verify-1" class="verify">
			    	<div class="form-group">
				    	<label class="col-sm-3 control-label" for="input-order_verify"><?php echo $text_verify_checkout; ?></label>
				      <div class="col-sm-9">
				        <select name="jossms_order_verify" id="input-order_verify" class="form-control">
	            		<?php if ($jossms_order_verify==0) { ?>
		            		<option value="0" selected="selected"><?php echo $text_no; ?></option>
		            	<?php }else{ ?>
		            		<option value="0"><?php echo $text_no; ?></option>
		            	<?php } ?>
		            	
		            	<?php if ($jossms_order_verify==1) { ?>
		            		<option value="1" selected="selected"><?php echo $text_yes; ?></option>
		            	<?php }else{ ?>
		            		<option value="1"><?php echo $text_yes; ?></option>
		            	<?php } ?>	                
	              </select>
				      </div>
				    </div>
				    <div id="order_verify-1" class="order_verify">
					    <div class="form-group">
					    	<label class="col-sm-3 control-label" for="input-skip_payment_method"><?php echo $entry_skip_payment_method; ?></label>
					      <div class="col-sm-4">
					        <select name="jossms_skip_payment_method[]" multiple="multiple" size="5" id="input-skip_payment_method" class="form-control">
		            		<option id="null" onClick="$('.nulls').removeAttr('selected')" value="none" <?php if (in_array('none', $jossms_skip_payment_method)) echo "selected";?>>None</option>
		                <?php foreach ($payment_methods as $payment_method) { ?>
		                <option class="nulls" onClick="$('#null').removeAttr('selected')" value="<?php echo $payment_method['code']; ?>" <?php if (in_array($payment_method['code'],$jossms_skip_payment_method)) echo "selected";?>><?php echo $payment_method['title']; ?>
		                <?php } ?>
		              </select><?php echo $entry_skip_payment_method_help; ?>
					      </div>
					    </div>
					  </div>
					  
					  <div id="order_verify-1" class="order_verify">
					    <div class="form-group">
					    	<label class="col-sm-3 control-label" for="input-skip_group_id"><?php echo $entry_skip_group; ?></label>
					      <div class="col-sm-4">
					        <select name="jossms_skip_group_id[]" multiple="multiple" size="5" id="input-skip_group_id" class="form-control">
		            		<option id="none" onClick="$('.nones').removeAttr('selected')" value=0 <?php if (in_array(0,$jossms_skip_group_id)) echo 'selected="selected"';?>>None</option>
		                  <?php foreach ($customer_groups as $customer_group) { ?>
		                  <!--<option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>-->
		                  <option class="nones" onClick="$('#none').removeAttr('selected')" value="<?php echo $customer_group['customer_group_id']; ?>" <?php if (in_array($customer_group['customer_group_id'],$jossms_skip_group_id)) echo 'selected="selected"';?>><?php echo $customer_group['name']; ?>
		                  <?php } ?>
		              </select><?php echo $entry_skip_group_help; ?>
					      </div>
					    </div>
					  </div>
					  
					  <div class="form-group">
				    	<label class="col-sm-3 control-label" for="input-register_verify"><?php echo $text_verify_register; ?></label>
				      <div class="col-sm-9">
				        <select name="jossms_register_verify" id="input-register_verify" class="form-control">
	            		<?php if ($jossms_register_verify==0) { ?>
		            		<option value="0" selected="selected"><?php echo $text_no; ?></option>
		            	<?php }else{ ?>
		            		<option value="0"><?php echo $text_no; ?></option>
		            	<?php } ?>
		            	
		            	<?php if ($jossms_register_verify==1) { ?>
		            		<option value="1" selected="selected"><?php echo $text_yes; ?></option>
		            	<?php }else{ ?>
		            		<option value="1"><?php echo $text_yes; ?></option>
		            	<?php } ?>
	              </select>
				      </div>
				    </div>
				    
				    <div class="form-group">
				    	<label class="col-sm-3 control-label" for="input-forgotten_verify"><?php echo $text_verify_forgotten; ?></label>
				      <div class="col-sm-9">
				        <select name="jossms_forgotten_verify" id="input-forgotten_verify" class="form-control">
	            		<?php if ($jossms_forgotten_verify==0) { ?>
		            		<option value="0" selected="selected"><?php echo $text_no; ?></option>
		            	<?php }else{ ?>
		            		<option value="0"><?php echo $text_no; ?></option>
		            	<?php } ?>
		            	
		            	<?php if ($jossms_forgotten_verify==1) { ?>
		            		<option value="1" selected="selected"><?php echo $text_yes; ?></option>
		            	<?php }else{ ?>
		            		<option value="1"><?php echo $text_yes; ?></option>
		            	<?php } ?>
	              </select>
				      </div>
				    </div>
				    
				    <div class="form-group required">
				    	<label class="col-sm-3 control-label" for="input-code_digit"><?php echo $entry_code_digit; ?></label>
				      <div class="col-sm-2">
				        <input type="text" name="jossms_code_digit" value="<?php echo $jossms_code_digit; ?>" placeholder="" id="input-code_digit" class="form-control" onkeyup="this.value = this.value.replace (/\D+/, '')" />
				        <?php if ($error_code_digit) { ?>
		            <div class="text-danger"><?php echo $error_code_digit; ?></div>
		            <?php } ?>
				      </div>
				    </div>
				    
				    <div class="form-group">
				    	<label class="col-sm-3 control-label" for="input-max_retry"><?php echo $entry_max_retry; ?></label>
				      <div class="col-sm-2">
				        <input type="text" name="jossms_max_retry" value="<?php echo $jossms_max_retry; ?>" placeholder="" id="input-max_retry" class="form-control" onkeyup="this.value = this.value.replace (/\D+/, '')" />
				        <?php echo $entry_limit_blank; ?>
				      </div>
				    </div>
				    
				    <div class="form-group required">
				    	<label class="col-sm-3 control-label" for="input-message_code_verification"><?php echo $entry_verify_code; ?></label>
				      <div class="col-sm-9">
				        <textarea name="jossms_message_code_verification" rows="8" class="form-control" id="input-message_code_verification" ><?php echo $jossms_message_code_verification; ?></textarea><?php echo $entry_alert_blank; ?>
				        <?php if ($error_message_code_verification) { ?>
		            <div class="text-danger"><?php echo $error_message_code_verification; ?></div>
		            <?php } ?>
				      </div>
				    </div>				    
			    </div>
			    
	      </div>
      	<!-- end new layout -->
      </form>
    </div>
  </div>
</div>
</div>

<script type="text/javascript"><!--	
$('select[name=\'jossms_gateway\']').bind('change', function() {
	$('#setgate .gateway').hide();
	
	$('#setgate #gateway-' + $(this).prop('value').replace('_', '-')).show();
});

$('select[name=\'jossms_gateway\']').trigger('change');


$('select[name=\'jossms_status\']').bind('change', function() {
	$('#setgate .status').hide();
	
	$('#setgate #status-' + $(this).prop('value').replace('_', '-')).show();
});

$('select[name=\'jossms_status\']').trigger('change');

$('select[name=\'jossms_return_status\']').bind('change', function() {
	$('#setgate .return-status').hide();
	
	$('#setgate #return-status-' + $(this).prop('value').replace('_', '-')).show();
});
$('select[name=\'jossms_return_status\']').trigger('change');


$('select[name=\'jossms_verify\']').bind('change', function() {
	$('#setgate .verify').hide();
	
	$('#setgate #verify-' + $(this).prop('value').replace('_', '-')).show();
});

$('select[name=\'jossms_verify\']').trigger('change');

$('select[name=\'jossms_order_verify\']').bind('change', function() {
	$('#setgate .order_verify').hide();
	
	$('#setgate #order_verify-' + $(this).prop('value').replace('_', '-')).show();
});

$('select[name=\'jossms_order_verify\']').trigger('change');

//--></script>
<?php echo $footer; ?>