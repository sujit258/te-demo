<?php echo $header; ?><?php echo $column_left; ?>

        <?php foreach ($photos as $photo) { ?>
              <div class="col-xs-3 col-md-2 js-photos-item-<?php echo $photo['shortcode']; ?>">
                <div data-toggle="tooltip" title="<?php echo $text_photo_manage; ?>">
                  <img src="<?php echo $photo['image_thumb']; ?>" class="img-responsive" style="cursor:pointer" data-toggle="modal" data-target="#js-photos-modal" data-isl-photo='<?php echo $photo['data']; ?>'>
                </div>
                
                <div class="text-center" style="margin-top:5px">
                  <span class="label label-default js-photos-approve-<?php echo $photo['shortcode']; ?>" data-toggle="tooltip" title="<?php echo $text_approval_status; ?>"><?php echo $photo['approve']; ?></span>
                  <span class="label label-primary js-photos-relproduct-<?php echo $photo['shortcode']; ?>" data-toggle="tooltip" title="<?php echo $text_related_product; ?>" style="padding-left:8px;padding-right:8px;"><?php echo $photo['related_product']; ?></span>
                <span class="label label-danger js-photos-remove-<?php echo $photo['shortcode']; ?> cursor-pointer" data-isg-remove="<?php echo $photo['shortcode']; ?>" data-toggle="tooltip" title="<?php echo $text_remove_db; ?>" style="padding-left:8px;padding-right:8px;<?php echo !($photo['remove'])  ? 'display:none' : 'display:none' ?>">x</span>
                
                
                
                </div>
                
              </div>
        <?php } ?>
 

<?php echo $footer; ?>