<div class="row">
  <div class="col-md-9">

    <div class="form-group">
      <label class="col-sm-3 control-label" for="input-photos-status"><?php echo $entry_global_status; ?></label>
      <div class="col-sm-6 col-md-3">
        <select name="<?php echo $module_setting; ?>[status]" id="input-photos-status" class="form-control">
          <option value="1" <?php echo ($setting['status'] == 1) ? 'selected' : ''; ?>><?php echo $text_enabled;?></option>
          <option value="0" <?php echo ($setting['status'] == 0) ? 'selected' : ''; ?>><?php echo $text_disabled;?></option>
        </select>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-3 control-label" for="input-hashtag"><?php echo $entry_hashtag; ?></label>
      <div class="col-sm-6">
        <div class="input-group">
          <div class="input-group-addon">#</div>
          <input type="text" id="input-hashtag" name="<?php echo $module_setting; ?>[hashtag]" value="<?php echo $setting['hashtag']; ?>" placeholder="Instagram hashtag" class="form-control">
        </div>
      </div>
    </div>

  </div>

  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-heading"><?php echo $text_information; ?></div>
      <div class="panel-body">
        <ul class="isl-list">
          <?php foreach ($text_info_setting as $item) { ?>
            <li><?php echo $item; ?></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <hr class="isl-hr">
</div>

<div class="row">

  <div class="col-md-9 clearfix">
    <h3 style="display:inline-block"><?php echo $text_moderation; ?></h3>
    <div class="pull-right">
      <select id="js-fetch-source" class="form-control input-sm" style="width:150px; margin-right:15px; display:inline-block;">
        <option value="instagram">Instagram</option>
        <option value="database"><?php echo $text_saved_photos; ?></option>
      </select>
      <a class="btn btn-success btn-sm js-fetch" data-isl-fetch style="margin-right:15px"><?php echo $text_fetch; ?></a>
    </div>

    <div class="js-photos-container">
       
    </div>
    <div class="js-photos-notification" style="margin:15px">
      <div class="text-center"><?php echo $text_fetch_info; ?></div>
    </div>
    <div class="text-center">
      <a class="btn btn-default btn-info btn-sm js-fetch-more" data-isl-fetch style="margin-bottom:25px; display:none"><?php echo $text_load_more; ?></a>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-heading"><?php echo $text_information; ?></div>
      <div class="panel-body">
        <ul class="isl-list">
          <?php foreach ($text_info_setting_moderation as $item) { ?>
            <li><?php echo $item; ?></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>

</div>


<div id="js-photos-modal" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius:0;">
      <button type="button" class="close" data-dismiss="modal" style="position:absolute; right:5px; top:2px; z-index:2000;"><span>&times;</span></button>
      <div class="js-modal-container"><div class="text-center" style="padding:100px 20px"><i class="fa fa-spinner fa-spin"></i><?php echo $text_loading; ?></div></div>
    </div>
  </div>
</div>


<script>
$(document).ready(function()
{
  var notyTpl = '<div class="text-center">{content}</div>',
      hashtag = $('#input-hashtag').val();

  // Fetch option
  $('#js-fetch-source').on('change', function() {
    $('.js-fetch-more').data('isl-fetch', '')
  });

  // Get photos
  $('[data-isl-fetch]').on('click', function() {
    var page = $(this).data('isl-fetch');

    $.ajax({
      url: 'index.php?route=<?php echo $module['path']; ?>/fetch&token=<?php echo $token; ?>&_='+ new Date().getTime(),
      type: 'POST',
      dataType: 'json',
      cache: false,
      data: {
        source  : $('#js-fetch-source').val(),
        hashtag : $('#input-hashtag').val(),
        page    : page
      },
      beforeSend: function() {
        if (page == '') {
          $('.js-photos-container').html('');
          $('.js-fetch-more').hide();
        }
        $('.js-photos-notification').html(notyTpl.replace('{content}', '<i class="fa fa-spinner fa-spin"></i> Processing..'));
      },
      success: function(data) {
        if (data.error) {
          $('.js-photos-notification').html(notyTpl.replace('{content}', '<div class="alert alert-warning" style="margin:0">'+data.output+'</div>'));
        } else {
          $('.js-photos-notification').html('');
          $('.js-photos-container').append(data.output);

          if (data.page_info.has_next_page) {
            $('.js-fetch-more').fadeIn();
            $('.js-fetch-more').data('isl-fetch', data.page_info.end_cursor);
          } else {
            $('.js-fetch-more').fadeOut();
          }
        }
      }
    });
  });

  // Automatically fetch
  if (hashtag != '') {
    $('.js-fetch').trigger('click');
  }
  $('#input-hashtag').on('focusout', function() {
    var hashtag = $('#input-hashtag').val();
    if (hashtag != '') {
      $('.js-fetch').trigger('click');
    }
  });
  $('#input-hashtag').on('keydown', function(e) {
    if (e.which == 13) { // enter
      e.preventDefault();

      var hashtag = $('#input-hashtag').val();
      if (hashtag != '') {
        $('.js-fetch').trigger('click');
      }
    }
  });

  // Remove photos from database
  $('.js-photos-container').on('click', '[data-isg-remove]', function() {
    var shortcode = $(this).data('isg-remove');

    $.ajax({
        
      url: 'index.php?route=<?php echo $module['path']; ?>/remove&token=<?php echo $token; ?>&_='+ new Date().getTime(),
      
      type: 'POST',
      dataType: 'json',
      cache: false,
      data: {
        source    : $('#js-fetch-source').val(),
        shortcode : shortcode,
      },
      beforeSend: function() {
        $('.js-photos-remove-' + shortcode).addClass('label-muted');
      },
      success: function(data) {
        if (!data.error) {
          if (data.remove_photo) {
            $('.js-photos-item-' + data.shortcode).remove();
          } else {
            $('.js-photos-remove-' + data.shortcode).removeClass('label-muted');
            $('.js-photos-remove-' + data.shortcode).hide();
          }
        }
      }
    });
  });

  // Load photos form when modal is open
  $('#js-photos-modal').on('show.bs.modal', function (e) {
    var el = $(e.relatedTarget),
        params = el.data('isl-photo');

    $('[data-toggle="tooltip"], .tooltip').tooltip('hide');
    $('#js-photos-modal .js-modal-container').load('index.php?route=<?php echo $module[path]; ?>/modalForm&token=<?php echo $token; ?>&_='+ new Date().getTime(), params);
  });
  $('#js-photos-modal').on('hidden.bs.modal', function (e) {
    $('#js-photos-modal .js-modal-container').html('<div class="text-center" style="padding:100px 20px"><i class="fa fa-spinner fa-spin"></i> Loading..</div>');
  });

  // Save modal
  $('.js-modal-container').on('click', '.js-modal-save', function(e) {
    $.ajax({
      url: 'index.php?route=<?php echo $module[path]; ?>/modalSave&token=<?php echo $token; ?>&_='+ new Date().getTime(),
      type: 'POST',
      dataType: 'json',
      cache: false,
      data: $('#form-modal').serialize(),
      beforeSend: function() {
        $('.js-modal-noty').show().html('<i class="fa fa-spinner fa-spin"></i> Saving..');
      },
      success: function(data) {
        if (!data.error) {
          $('.js-modal-noty').html('<b class="text-success">Successfully saved!</b>');
          $('.js-photos-approve-' + data.shortcode).html(data.approve);
          $('.js-photos-relproduct-' + data.shortcode).html(data.related_product);
          $('.js-photos-remove-' + data.shortcode).show();

          setTimeout(function() {
            $('#js-photos-modal').modal('hide');
          }, 250);
        }
      }
    });
  });
});
</script>