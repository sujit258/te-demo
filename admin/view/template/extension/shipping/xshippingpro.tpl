<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
 <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="button" id="save_form" form="form-xshippingpro" value="save" name="action" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a id="quick_save" onclick="return false;" data-toggle="tooltip" title="<?php echo $button_save_continue; ?>" id="quick_save" class="btn btn-info"><i class="fa fa-clipboard"></i></a>&nbsp;
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
 </div>
<div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
          </div>
          <div class="panel-body">
           <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-xshippingpro" class="form-horizontal">
             <input type="file" class="import-csv" accept="text/csv" name="file" />
             <div class="row">
                    <div class="col-sm-3">
                      <ul id="method-list" class="nav nav-pills nav-stacked draggable-container">
                        <li class="active" draggable="false"><a href="#tab-general" draggable="false" class="global" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <?php foreach($methods as $no_of_tab => $name) { ?>
                          <li draggable="true" class="draggable"><a draggable="false" class="tab<?php echo $no_of_tab;?>" href="#shipping-<?php echo $no_of_tab; ?>"  data-toggle="tab"><?php echo $name;?></a><i class="fa fas fa-arrows-alt"></i></li>
                        <?php } ?>
                      </ul>
                      
                      <button class="btn btn-success add-new" data-toggle="tooltip" form="form-xshippingpro" type="button"  data-placement="bottom"  data-original-title="<?php echo $text_add_new_method?>"><i class="fa fa-plus"></i></button>
                    </div>
	                
                  <div class="col-sm-9">
                    <div id="shipping-container" class="tab-content">
                     <div class="tab-pane active global-tab-content" id="tab-general">
                        <ul class="nav nav-tabs" id="language-heading">
                            <?php $active_class=''; foreach ($languages as $language) { ?>
                             <li <?php if(!$active_class) echo 'active'; $active_class='1'; ?>><a href="#language<?php echo $language['language_id']; ?>heading" data-toggle="tab"><img src="<?php echo $language_dir.$language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                            <?php } ?>
                         </ul>
                         
                         <div class="tab-content global-heading">
                            <?php $active_class=''; foreach ($languages as $language) { ?>
                              <div class="tab-pane<?php if(!$active_class) echo ' active'; $active_class='1'; ?>" id="language<?php echo $language['language_id']; ?>heading">                         
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label" for="lang-heading<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="<?php echo $tip_heading; ?>"><?php echo $text_heading; ?> </span></label>
                                    <div class="col-sm-9">
                                      <input type="text" name="xshippingpro_heading[<?php echo $language['language_id']; ?>]" value="<?php echo isset($xshippingpro_heading[$language['language_id']])?$xshippingpro_heading[$language['language_id']]:'Shipping Options'; ?>" placeholder="<?php echo $text_heading; ?>" id="lang-heading<?php echo $language['language_id']; ?>" class="form-control" />
                                    </div>
                                  </div>
                               </div> 
                            <?php } ?>
                          </div>
                         
                          <ul class="nav nav-tabs">
                             <li class="active"><a href="#global-general" data-toggle="tab"><?php echo $tab_general_general?></a></li>
                             <li><a href="#global-group" data-toggle="tab"><?php echo $tab_general_global?></a></li>
                             <li><a href="#global-export" data-toggle="tab"><?php echo $tab_import_export?></a></li>
                             <li class="nav-item"><a href="#global-estimator" class="nav-link" data-toggle="tab"><?php echo $text_estimator_tab?></a></li>
                             <li><a href="#global-help" data-toggle="tab"><?php echo $text_help?></a></li>
                          </ul>
                         
                         <div class="tab-content"> 
                           <div class="tab-pane active" id="global-general">
                            <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-sort-order"><span data-toggle="tooltip" title="<?php echo $tip_sorting_global; ?>"><?php echo $entry_sort_order; ?> </span></label>
                              <div class="col-sm-9">
                                <input type="text" name="xshippingpro_sort_order" value="<?php echo $xshippingpro_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                              </div>
                           </div>
                           <div class="form-group">
                            <label for="xshippingpro_error" class="col-sm-3 control-label"><span data-toggle="tooltip" title="<?php echo $text_product_error_help; ?>"><?php echo $text_product_error; ?></span></label>
                             <div class="col-sm-9">
                                 <input <?php if(isset($xshippingpro_error) && $xshippingpro_error) echo 'checked';?> type="checkbox" name="xshippingpro_error" value="1" id="xshippingpro_error" />
                              </div>
                           </div>
                           <div class="form-group">
                            <label for="xshippingpro_desc_mail" class="col-sm-3 control-label"><?php echo $text_description; ?> </label>
                             <div class="col-sm-9">
                                 <input <?php if(isset($xshippingpro_desc_mail) && $xshippingpro_desc_mail) echo 'checked';?> type="checkbox" name="xshippingpro_desc_mail" value="1" id="xshippingpro_desc_mail" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-map-api"><span data-toggle="tooltip" title="<?php echo $text_google_map_api_key_help; ?>"><?php echo $text_google_map_api_key; ?> </span></label>
                              <div class="col-sm-9">
                                <input type="text" name="xshippingpro_map_api" value="<?php echo $xshippingpro_map_api; ?>" placeholder="<?php echo $text_google_map_api_key; ?>" id="input-map-api" class="form-control" />
                              </div>
                           </div>
                           <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-group-sorting"><span data-toggle="tooltip" title="<?php echo $tip_text_sort_type; ?>"><?php echo $text_sort_type; ?></span></label>
                            <div class="col-sm-9">
                              <select name="xshippingpro_sorting" id="input-group-sorting" class="form-control">
                               		 <?php
                   
                  					 foreach($sort_options as $sort_option_key=>$sort_option_value){
                    					$selected=(isset($xshippingpro_sorting) && $xshippingpro_sorting==$sort_option_key)?'selected':'';
                  					?>
                                   <option value="<?php echo $sort_option_key?>" <?php echo $selected?>><?php echo $sort_option_value?></option>
                                  <?php } ?>
                              </select>
                             </div>
                          </div> 
                             <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-status"><span data-toggle="tooltip" title="<?php echo $tip_status_global; ?>"><?php echo $module_status; ?></span></label>
                            <div class="col-sm-9">
                              <select name="xshippingpro_status" id="input-status" class="form-control">
                                <?php if ($xshippingpro_status) { ?>
                                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                  <option value="0"><?php echo $text_disabled; ?></option>
                                  <?php } else { ?>
                                  <option value="1"><?php echo $text_enabled; ?></option>
                                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                              </select>
                             </div>
                          </div>
                            <div class="form-group">
                            <label class="col-sm-3 control-label" for="xshippingpro_debug"><span data-toggle="tooltip" title="<?php echo $tip_debug; ?>"><?php echo $text_debug; ?></span></label>
                            <div class="col-sm-9">
                              <select name="xshippingpro_debug" id="xshippingpro_debug" class="form-control">
                               <?php if ($xshippingpro_debug) { ?>
                                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                  <option value="0"><?php echo $text_disabled; ?></option>
                                  <?php } else { ?>
                                  <option value="1"><?php echo $text_enabled; ?></option>
                                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                  <?php } ?>
                              </select>
                             </div>
                          </div>
                         </div>
                         <div class="tab-pane" id="global-export">
                            <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-import"><span data-toggle="tooltip" title="<?php echo $tip_import; ?>"><?php echo $text_import; ?></span></label>
                              <div class="col-sm-6">
                                 <input type="file" class="form-control" id="input-import" accept="text/txt" name="file_import" />
                              </div>
                              <div class="col-sm-3">
                                <button type="submit" form="form-xshippingpro" data-toggle="tooltip" name="action" title="<?php echo $text_import; ?>" value="import" class="btn btn-primary"><i class="fa fa-upload"></i></button>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-export"><span data-toggle="tooltip" title="<?php echo $tip_export; ?>"><?php echo $text_export; ?></span></label>
                              <div class="col-sm-9">
                                 <a href="<?php echo $export;?>" target="_blank" class="btn btn-primary"><?php echo $text_export; ?></a>
                              </div>
                           </div>
                         </div>
                         <div class="tab-pane" id="global-help">
                          <div class="form-group">
                            <div class="col-sm-12">
                               <div class="help-data">
                                <a class="btn btn-info" href="http://opencartmart.com/docs/xshippingpro/index.html" target="_blank" role="button"><?php echo $text_documentation;?></a> <br />
                                <a class="btn btn-danger" href="javascript:debugBrowser();" role="button"><?php echo $text_debug_button;?></a>
                              </div>
                            </div>
                          </div>
                        </div> 
                        <div class="tab-pane" id="global-estimator">
                             <div class="form-group">
                              <label for="estimator-status" class="col-sm-6 control-label col-form-label"><?php echo $text_estimator_enable; ?></label>
                              <div class="col-sm-6">
                                <input <?php if (isset($xshippingpro_estimator['status'])) echo 'checked' ?> type="checkbox" name="xshippingpro_estimator[status]" value="1" id="estimator-status" />
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-3 control-label col-form-label"><?php echo $text_estimator_store; ?></label>
                              <div class="col-sm-9">
                                 <div class="checkbox xshipping-checkbox">
                                   <?php foreach($stores as $store) { ?>
                                      <label><input <?php if (isset($xshippingpro_estimator['store']) && in_array($store['store_id'], $xshippingpro_estimator['store'])) echo 'checked' ?> name="xshippingpro_estimator[store][]" type="checkbox" value="<?php echo $store['store_id']; ?>" />&nbsp; <?php echo $store['name']; ?></label>
                                   <?php } ?>
                                 </div>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-3 control-label col-form-label"><?php echo $text_estimator_fields; ?></label>
                              <div class="col-sm-9">
                                 <div class="checkbox xshipping-checkbox">
                                     <label><input <?php if (isset($xshippingpro_estimator['country'])) echo 'checked' ?> name="xshippingpro_estimator[country]" type="checkbox" value="1" />&nbsp; <?php echo $text_estimator_country; ?></label>
                                     <label><input <?php if (isset($xshippingpro_estimator['zone'])) echo 'checked' ?> name="xshippingpro_estimator[zone]" type="checkbox" value="1" />&nbsp; <?php echo $text_estimator_zone; ?></label>
                                     <label><input <?php if (isset($xshippingpro_estimator['postal'])) echo 'checked' ?> name="xshippingpro_estimator[postal]" type="checkbox" value="1" />&nbsp; <?php echo $text_estimator_postal; ?></label>
                                 </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-sm-3 control-label col-form-label" for="xshippingpro_estimator_type"><?php echo $estimator_type;?></label>
                              <div class="col-sm-9">
                                <select name="xshippingpro_estimator[type]" id="xshippingpro_estimator_type" class="form-control">
                                    <option value="method" <?php  if ($xshippingpro_estimator['type'] == 'method') { ?> selected <?php } ?>><?php echo $estimator_type_method;?></option>
                                    <option value="avail" <?php  if ($xshippingpro_estimator['type'] == 'avail') { ?> selected <?php } ?>><?php echo $estimator_type_avail;?></option>
                                </select>
                              </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-3 control-label col-form-label" for="input-estimator-selector"><span data-toggle="tooltip" title="<?php echo $text_estimator_selector_help; ?>"><?php echo $text_estimator_selector; ?></span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="xshippingpro_estimator[selector]" value="<?php echo isset($xshippingpro_estimator['selector']) ? $xshippingpro_estimator['selector'] : '' ; ?>" id="input-estimator-selector" class="form-control" />
                                </div>
                             </div>
                             <div class="form-group">
                               <label class="col-sm-3 control-label"><span data-toggle="tooltip" title="<?php echo $text_custom_css_help; ?>"><?php echo $text_estimator_css; ?></span></label>
                               <div class="col-sm-9">
                                  <textarea rows="5" class="form-control" name="xshippingpro_estimator[css]"><?php echo isset($xshippingpro_estimator['css']) ? $xshippingpro_estimator['css'] : '' ; ?></textarea>
                                </div>  
                              </div> 
                        </div>
                         <div class="tab-pane" id="global-group">
                        <div class="form-group">
                        <div class="col-sm-12"> 
                          <label class="control-label" for="input-method-group"><span data-toggle="tooltip" title="<?php echo $tip_method_group; ?>"> <?php echo $text_method_group;?> </span><br><br></label>
                         <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover">
                            <thead>
                              <tr>
                                <td class="text-left">
                                    <?php echo $text_method_id;?>
                                  </td>
                                  <td class="text-left">
                                    <?php echo $text_group_type;?>
                                </td>
                                <td class="text-left">
                                    <span data-toggle="tooltip" title="<?php echo $tip_group_limit; ?>"><?php echo $text_group_limit;?></span>
                                </td>
                                <td class="text-left">
                                    <span data-toggle="tooltip" title="<?php echo $tip_group_name; ?>"><?php echo $entry_group_name;?></span>
                                </td>
                                <td class="text-left">
                                    <span data-toggle="tooltip" title="<?php echo $tip_group_desc; ?>"><?php echo $text_group_desc;?></span>
                                </td>
                               </tr>    
                            </thead>
                           <tbody>
                            <?php
                      
                              for($sg=1; $sg<=$xshippingpro_sub_groups_count;$sg++) {
                                  $current_method_mode = 'no_group';
                            ?>
                             <tr>
                                <td class="text-left">Group<?php echo $sg;?></td>
                                <td class="text-left">
                                  <select class="xshippingpro_sub_group<?php echo $sg;?>" name="xshippingpro_sub_group[<?php echo $sg;?>]">
                                  <?php
                                    foreach($group_options as $type=>$name) {
                                      $selected=(isset($xshippingpro_sub_group[$sg]) && $xshippingpro_sub_group[$sg]==$type)?'selected':'';
                                        $current_method_mode = (isset($xshippingpro_sub_group[$sg]) && $xshippingpro_sub_group[$sg]==$type)? $type: $current_method_mode; 
                                ?>
                                  <option value="<?php echo $type?>" <?php echo $selected?>><?php echo $name?></option>
                                <?php } ?>
                              </select>
                              </td>
                              <td class="text-left"> 
                                    <select <?php if($current_method_mode!='lowest' && $current_method_mode!='highest') echo 'style="display:none;"'; ?> class="xshippingpro_sub_group_limit<?php echo $sg;?>" name="xshippingpro_sub_group_limit[<?php echo $sg;?>]">
                                  <?php
                                      for($gi=1; $gi<=5; $gi++){
                                        $selected=(isset($xshippingpro_sub_group_limit[$sg]) && $xshippingpro_sub_group_limit[$sg]==$gi)?'selected':'';
                                      ?>
                                      <option value="<?php echo $gi?>" <?php echo $selected?>><?php echo $gi?></option>
                                  <?php } ?>
                                </select>
                                </td>
                                <td class="text-left"> 
                                     <input type="text" name="xshippingpro_sub_group_name[<?php echo $sg;?>]" value="<?php echo isset($xshippingpro_sub_group_name[$sg])?$xshippingpro_sub_group_name[$sg]:''; ?>" placeholder="<?php echo $entry_name; ?>" class="form-control" />
                                </td>
                                <td class="text-left"> 
                                     <input type="text" name="xshippingpro_sub_group_desc[<?php echo $sg;?>]" value="<?php echo isset($xshippingpro_sub_group_desc[$sg])?$xshippingpro_sub_group_desc[$sg]:''; ?>" placeholder="<?php echo $text_group_desc; ?>" class="form-control" />
                                </td>
                            </tr> 
                            <?php } ?> 
                            </tbody>
                             </table>
                             </div>
                         </div>
                     </div>
            		       <div class="form-group">
                            <label class="col-sm-3 control-label" for="input-group"><span data-toggle="tooltip" title="<?php echo $tip_grouping; ?>"><?php echo $text_group_shipping_mode; ?></span></label>
                            <div class="col-sm-9">
                              <select name="xshippingpro_group" id="input-group" class="form-control xshippingpro_group">
                                <?php
                                   foreach($group_options as $type=>$name){
                                   if($type =='and') continue;
                                    $selected=(isset($xshippingpro_group) && $xshippingpro_group==$type)?'selected':'';
                                  ?>
                                  <option value="<?php echo $type?>" <?php echo $selected?>><?php echo $name?></option>
                                  <?php } ?>
                              </select>
                             </div>
                          </div>
                          <div <?php if($xshippingpro_group!='lowest' && $xshippingpro_group!='highest') echo 'style="display:none;"'; ?> id="group-limit" class="form-group">
                            <label class="col-sm-3 control-label" for="input-group-limit"><span data-toggle="tooltip" title="<?php echo $tip_group_limit; ?>"><?php echo $text_group_limit; ?></span></label>
                            <div class="col-sm-9">
                              <select name="xshippingpro_group_limit" id="input-group-limit" class="form-control">
                                <?php
                                   for($gi=1; $gi<=5; $gi++){
                                    $selected=(isset($xshippingpro_group_limit) && $xshippingpro_group_limit==$gi)?'selected':'';
                                  ?>
                                  <option value="<?php echo $gi?>" <?php echo $selected?>><?php echo $gi?></option>
                                  <?php } ?>
                              </select>
                             </div>
                          </div>
                         </div>   
                        </div>   
                       </div> <!--end of tab general-->
                       <?php  echo $form_data;?>
                   </div>
                 </div>
               </div>      
            </form>
      </div>
    </div>
  </div>

<style type="text/css">
.xshipping-checkbox label{ margin-right: 15px;}
.xshipping-checkbox label input {
        margin-right: 5px;
}
.well {
    margin-bottom: 5px;
}
.well-sm{ display:none;}
.well-days, .well-desc, .product-category, .product-product, .product-option, .product-manufacturer, .hide-methods, .product-location, .hide-methods-inactive{ display:block;}
div.category, div.product, div.option, div.postal-option, div.range-option, div.coupon-option, div.manufacturer, div.dimensional-option, div.city-option, div.location{ display:none;}
div.checkbox-selection-wrap{
    display: none;
}
div.checkbox-selection-wrap a {
    float: right;
    margin-left: 10px;
  }
label.any-class {
    margin-top: 4px;
 }
div.range-option textarea{height: 45px;}
.tbl-wrapper{ width:99%;}
.import-btn-wrapper{width:99%; height:auto; overflow:hidden; margin-bottom:10px;}
.import-btn-wrapper a.btn{ float:right;margin-right: 5px;}
input.import-csv[type="file"]{ display:none;}
div.shipping {
    position: relative;
}
.action-btn {
    position: absolute;
    right: 10px;
    top: 0;
}
.action-btn button {
    margin-left: 5px;
}
button.add-new {
    margin-top: 10px;
}

div.tooltip div.tooltip-inner{ font-weight:normal !important; text-align:left !important;}
div.tooltip div.tooltip-inner b{ display:block !important;}
.global-waiting{display:block;position:fixed; width:124px; height:34px; text-align:center;font-size:16px;font-weight:bold; color:#ffffff;background-color:#D96E7C; border-radius:5px;padding-top:5px;}
.fa-minus-circle{ cursor:pointer;}
/* End of new*/

/*tab related*/
.global-tab-content{margin-top:15px;}
#shipping-container {
  border: 1px solid #ddd;
  padding: 0 15px 15px;
}
.global-heading {
    margin-bottom: 30px;
    border-bottom: 1px solid #efefef;
    padding-bottom: 15px;
}
#method-list > li > a.global{
  text-transform: uppercase;
  background-color: #c7c7c7;
  color: #555;
  font-weight: bold;
}
#method-list > li > a {
    background-color: #f7f7f7;
    border-bottom: 1px solid #ddd;
    border-top: 1px solid #ddd;
    margin-bottom: 2px;
    border-right:1px solid #ccc;
}
#method-list > li.active > a, #method-list > li > a:hover {
  background-color: #1e91cf;
  color: #fff;
}
#method-list > li.active a.global {background-color: #FF5722 !important;}
.display-name-row {
    background: #f7f7f7;
    margin-bottom: 11px !important;
    border-bottom: 1px solid #d7d7d7;
}
.method-tab {
  background: #f7f7f7;
   margin-bottom: 0;
}
.method-content {
    border: 1px solid #ddd;
    padding: 15px;
    border-top: none;
}

.help-data{
      text-align: center;
    }
.help-data a{
   margin-bottom: 10px !important;
 }
 .dropable {
      border: 2px dotted #f38733 !important;
}
.dragging {
 opacity: 0.5;
}
 .remove-selection{
  margin-left: 12px;
}
.sub-option {
  border: 1px solid #ccc;
  background: #fbfbfb;
  padding: 10px 75px 10px 10px;
  margin-bottom: 15px;
  position: relative;
}
.sub-option .sub-option-action {
  position: absolute;
  top: 40px;
  right: 5px;
  width: 50px;
}
.sub-option table {
  width: 100%;
}
.sub-option table td {
  padding: 5px;
}
.sub-options-btn {
  text-align: right;
}
.drag-sub-option {
  cursor: move;
}
#method-list i {
  position: absolute;
  right: 8px;
  top: 12px;
  cursor: move;
  display: none;
  color: #f1f1f1;
}
#method-list li:hover i {
  display: inline-block;
}
</style>
<script type="text/javascript"><!--
var current_tab=1; 
var save_type = '';   
var range ='<tr>'; 
    range += '    <td class="text-left"><input size="15" type="text" name="xshippingpro[rate_start][]" class="form-control" value="__VALUE_START__" /></td>';
    range += '    <td class="text-left"><input size="15" type="text" name="xshippingpro[rate_end][]" class="form-control" value="__VALUE_END__" /></td>';
    range += '    <td class="text-left"><input size="15" type="text" name="xshippingpro[rate_total][]" class="form-control" value="__VALUE_COST__" /></td>';
    range += '    <td class="text-left"><input size="6" type="text" name="xshippingpro[rate_block][]" class="form-control" value="__VALUE_PG__" /></td>';
	range += '    <td class="text-left"><select name="xshippingpro[rate_partial][]"><option __VALUE_PA2__ value="0"><?php echo $text_no;?></option><option __VALUE_PA1__ value="1"><?php echo $text_yes;?></option></select></td>';
    range += '    <td class="text-right"><a class="btn btn-danger remove-row"><?php echo $text_remove;?></a></td>';
    range += '  </tr>';
       
 var tmp = <?php echo json_encode($tpl); ?>;
 var sub_option_tmp =  <?php echo json_encode($tpl_sub_option); ?>; 
 var shipping_methods = [];
 var auto_complete_cat={
				'source': function(request, response) {
					$.ajax({
						url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
						dataType: 'json',			
						success: function(json) {
							response($.map(json, function(item) {
								return {
									label: item['name'],
									value: item['category_id']
								}
							}));
						}
					});
				},
				'select': function(item) {
					
					var my_method_area=$('#shipping-'+current_tab);
					$('input[name=\'category\']',my_method_area).val('');
					$('.product-category' + item['value'],my_method_area).remove();
					$('.product-category',my_method_area).append('<div class="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="xshippingpro[product_category][]" value="' + item['value'] + '" /></div>');	
					
				}
	};
	
 var auto_complete_prod={
	 'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	 'select': function(item) {
		 
		var my_method_area=$('#shipping-'+current_tab); 
		$('input[name=\'product\']', my_method_area).val('');
		$('.product-product' + item['value'], my_method_area).remove();
		$('.product-product', my_method_area).append('<div class="product-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="xshippingpro[product_product][]" value="' + item['value'] + '" /></div>');	
	  }	
  };
  
  var auto_complete_option={
	 'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=extension/shipping/xshippingpro/getOption&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['option_value_id']
					}
				}));
			}
		});
	},
	 'select': function(item) {
		 
		var my_method_area=$('#shipping-'+current_tab); 
		$('input[name=\'option\']', my_method_area).val('');
		$('.product-option' + item['value'], my_method_area).remove();
		$('.product-option', my_method_area).append('<div class="product-option' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="xshippingpro[product_option][]" value="' + item['value'] + '" /></div>');	
	  }	
  };

  var auto_complete_manufacturer={
   'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',     
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['manufacturer_id']
          }
        }));
      }
    });
  },
   'select': function(item) {
     
    var my_method_area=$('#shipping-'+current_tab); 
    $('input[name=\'input_manufacturer\']', my_method_area).val('');
    $('.product-manufacturer' + item['value'], my_method_area).remove();
    $('.product-manufacturer', my_method_area).append('<div class="product-manufacturer' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="xshippingpro[manufacturer][]" value="' + item['value'] + '" /></div>'); 
    } 
  };

  
 var auto_complete_methods = {
   'source': function(request, response) {
    var methods = [];
    $('#shipping-container').find('div.shipping').each(function(){
             var name,
                 tab_id,
                 $this = $(this);
             tab_id = $this.attr('id');
             tab_id = parseInt(tab_id.replace('shipping-',''));
             name = $this.find('input.display-name').val();
             if (!name) name = 'Untitled Method'+tab_id;
             if (current_tab != tab_id && name.toLowerCase().indexOf(request) != -1) {
                  methods.push({
                  label: name,
                  value: tab_id
                });
              }
      });
    response(methods);
  },
   'select': function(item) {

      var hide_class = 'hide-method',
          hide_wrapper = 'hide-methods',
          field_name = 'hide';
      if ($(this).hasClass('hide-inactive')) {
         hide_class = 'hide-method-inactive';
         hide_wrapper = 'hide-methods-inactive';
         field_name = 'hide_inactive';
      }

     
      var my_method_area=$('#shipping-'+current_tab); 
      $('input.hide-shipping', my_method_area).val('');
      $('.'+hide_class + item['value'], my_method_area).remove();
      $('.'+hide_wrapper, my_method_area).append('<div class="'+hide_class + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="xshippingpro['+field_name+'][]" value="' + item['value'] + '" /></div>'); 
    }
 };

 var auto_complete_location={
   'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=extension/shipping/xshippingpro/getLocation&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',     
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['id']
          }
        }));
      }
    });
  },
   'select': function(item) {
     
      var my_method_area=$('#shipping-'+current_tab); 
      $('input[name=\'input_location\']', my_method_area).val('');
      $('.product-location' + item['value'], my_method_area).remove();
      $('.product-location', my_method_area).append('<div class="product-location' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="xshippingpro[location][]" value="' + item['label'] + '" /></div>');  
    } 
  };

   
$(document).ready(function () {		
 
	 /* Enable tab*/
	 $('#method-list a:first').tab('show');
	 $('#language-heading a:first').tab('show');
	 
	 $("#method-list").on("click","li",function(){
		 current_tab=$(this).find('a').attr('href').replace('#shipping-','');  
	 });

   makedropzone();

   $('.date').datetimepicker({
      pickTime: false
   });
	 
	 /* Creating New method*/
	 $('.add-new').on('click',function(e) {
		  e.preventDefault();
		  $this=$(this);
		  var no_of_tab=$('#shipping-container').find('div.shipping').length;
		  no_of_tab=parseInt(no_of_tab)+1;
		  //finding qnique id
		  while($('#shipping-'+no_of_tab).length!=0)
		   {
		     no_of_tab++;
		   }
		  var tab_html=tmp;
		  tab_html=tab_html.replace('__ID__','shipping-'+no_of_tab);
		  tab_html=tab_html.replace(/__INDEX__/g, no_of_tab);
		  $('#shipping-container').append(tab_html);
		  
		  $('#method-list').append('<li draggable="true" class="draggable"><a draggable="false" data-toggle="tab" class="tab'+no_of_tab+'" href="#shipping-'+no_of_tab+'">Untitled Method '+no_of_tab+'</a><i class="fa fas fa-arrows-alt"></i></li>');
		   enableEvents(no_of_tab); 
		   current_tab=no_of_tab;
    });
	 /* End of creating new*/

   /* Sub- Options */
    $("#shipping-container").on('click','a.add-sub-option',function(){
        var sub_options = [0];
        $('#shipping-' + current_tab + ' .sub-options-container .sub-option').each(function() {
            sub_options.push(parseInt($(this).attr('index')));
        });
        var counter = Math.max.apply(null, sub_options);
        counter++;
        var sub_option_html = sub_option_tmp.replace(/__INDEX__/g, counter);
        $('#shipping-' + current_tab + ' .sub-options-container').append(sub_option_html);
        $('#shipping-' + current_tab + ' .sub-option-'+counter+' [data-toggle="tooltip"]').tooltip();
    });
    $("#shipping-container").on('click','a.delete-sub-option',function() {
        $(this).closest('.sub-option').remove();
    });
    /* End of Sub-options */
	 
	 $("#shipping-container").on('click','button.btn-delete', function() { 
		  if(confirm('Are you sure to delete this method?')){
		              deleteMethod(current_tab);
					  $('a.tab'+current_tab).remove();
					  $('#shipping-'+current_tab).remove();
					  $('#method-list a:first').tab('show');
				}
	 });

   $("#shipping-container").on('click','a.toggle-box', function(e) { 
        e.preventDefault();
        var rel = $(this).attr('rel');
        if ($(rel).css('display') == 'none' || !$(rel).css('display')) {
           $(rel).show();
        } else {
          $(rel).hide();
        }
   });

   $("#shipping-container").on('change','.country input[type="checkbox"]', function(e) { 
        e.preventDefault();
        fetchZone();
   });
	 
	 $("#shipping-container").on('click','button.btn-copy', function() { 
		  if(confirm('Are you sure to copy this method?')){
					  copyMethod(current_tab);
				}
	 });
	 
	 
	   $('.xshippingpro_group').change(function(){
		   if($(this).val()=='lowest' || $(this).val()=='highest') {
			  $('#group-limit').show();  
		   }else{
			  $('#group-limit').hide();     
		   }  
		});
		
		$("select[class^='xshippingpro_sub_group']").change(function(){
		   var groupid = $(this).attr('class').replace('xshippingpro_sub_group','');
		   if($(this).val()=='lowest' || $(this).val()=='highest') {
			  $('.xshippingpro_sub_group_limit'+parseInt(groupid)).show();  
		   }else{
			   $('.xshippingpro_sub_group_limit'+parseInt(groupid)).hide();     
		   }  
		
		});
	   
       
	   $("#shipping-container").on('keyup','input.display-name', function() {		
		  var method_name=$(this).val();
		  if(method_name=='') method_name='Untitled Method '+current_tab;
		  $('a.tab'+current_tab).html(method_name);
	   }); 
	   
	   
	    $("#shipping-container").on('change','select.selection', function() {
		    
			var relto=$(this).attr('rel');
			 if($(this).val()=='1'){
			    $(this).closest('div.form-group').siblings('div.'+relto).hide();  
			 }else{
			    $(this).closest('div.form-group').siblings('div.'+relto).show();  
			 }
		 }); 
		 
		 $('#shipping-container').delegate('.fa-minus-circle', 'click', function() {
			  $(this).parent().remove();
	     });
		
		  // Category selection 
		  $('input[name=\'category\']').autocomplete(auto_complete_cat);
		  $('input[name=\'product\']').autocomplete(auto_complete_prod);
		  $('input[name=\'option\']').autocomplete(auto_complete_option);
      $('input[name=\'input_manufacturer\']').autocomplete(auto_complete_manufacturer);
      $('input[name=\'input_location\']').autocomplete(auto_complete_location);
		  $('input.hide-shipping').autocomplete(auto_complete_methods);

      $("#shipping-container").on('click', '.check-all', function(e) {
           e.preventDefault();
           $(this).closest('div.form-group').find('div.checkbox input[type="checkbox"]').prop( "checked",true);;
      });

      $("#shipping-container").on('click', '.uncheck-all', function(e) {
           e.preventDefault();
           $(this).closest('div.form-group').find('div.checkbox input[type="checkbox"]').prop( "checked",false);;
      });

      $("#shipping-container").on('click', '.remove-selection', function(e) {
           e.preventDefault();
           var rel = $(this).attr('rel');
           $(this).closest('div.form-group').find('div.' + rel).html('');
      });
			
		  $("#shipping-container").on('click', '.choose-any', function() {
				 if($(this).prop('checked')){
					  $(this).parent().next('div.well-sm').slideUp(); 
            $(this).parent().siblings('div.checkbox-selection-wrap').hide(); 
				 }else{
					  $(this).parent().next('div.well-sm').slideDown();
            $(this).parent().siblings('div.checkbox-selection-wrap').show();
				}
			});
			
		  $("#shipping-container").on('click', '.choose-any-with', function() {
                var relto=$(this).attr('rel');
				  
				  if(relto=='manufacturer-option') {
					 if($(this).prop('checked')){
						 $(this).parent().next('div.well-sm').slideUp();  
					 }else{
						 $(this).parent().next('div.well-sm').slideDown();
					 }
				  }
				 	
				 if($(this).prop('checked')){
					 $(this).closest('div.form-group').siblings('div.'+relto).hide();  
				 }else{
					$(this).closest('div.form-group').siblings('div.'+relto).show();  
				}
			});
                
		
    $("#shipping-container").on('change', 'select.rate-selection', function() {
		      
      			if($(this).val()=='flat'){
      			   $(this).closest('div.form-group').siblings('div.single-option').show();
               $(this).closest('div.form-group').siblings('div.range-option').hide();
      			   $(this).closest('div.form-group').siblings('div.dimensional-option').hide();
      			   
      			}
      		   else if($(this).val()=='dimensional' || $(this).val()=='volumetric'){
      			   $(this).closest('div.form-group').siblings('div.single-option').hide();
      			   $(this).closest('div.form-group').siblings('div.dimensional-option').show();
      			   $(this).closest('div.form-group').siblings('div.range-option').show();
      			  
      			 }
      		   else {
      			   $(this).closest('div.form-group').siblings('div.single-option').hide();
      			   $(this).closest('div.form-group').siblings('div.dimensional-option').hide();
               $(this).closest('div.form-group').siblings('div.range-option').show();
      			 }

            /* additional ranges */
            if ($(this).val() =='quantity') {
              $(this).closest('div.method-content').find('div.additional-weight').show();
              $(this).closest('div.method-content').find('div.additional-total').show();
              $(this).closest('div.method-content').find('div.additional-quantity').hide();
            }
            else if ($(this).val() =='weight') {
              $(this).closest('div.method-content').find('div.additional-total').show();
              $(this).closest('div.method-content').find('div.additional-quantity').show();
              $(this).closest('div.method-content').find('div.additional-weight').hide();
            }
            else if ($(this).val() =='total'
              || $(this).val() =='sub'
              || $(this).val() =='total_coupon'
              || $(this).val() =='grand_shipping'
              || $(this).val() =='grand') {
              $(this).closest('div.method-content').find('div.additional-weight').show();
              $(this).closest('div.method-content').find('div.additional-quantity').show();
              $(this).closest('div.method-content').find('div.additional-total').hide();
            } else {
              $(this).closest('div.method-content').find('div.additional-weight').show();
              $(this).closest('div.method-content').find('div.additional-quantity').show();
              $(this).closest('div.method-content').find('div.additional-total').show();
            }
		});
                
                
     /* Range Options */
	 $("#shipping-container").on('click','a.add-row',function(){
	          
	          var tpl=range;
			  tpl=tpl.replace(/__INDEX__/g, current_tab);
			  tpl=tpl.replace(/__VALUE_START__/g, '0');
			  tpl=tpl.replace(/__VALUE_END__/g, '0');
			  tpl=tpl.replace(/__VALUE_COST__/g, '0');
			  tpl=tpl.replace(/__VALUE_PG__/g, '0');
			  tpl=tpl.replace(/__VALUE_PA1__/g, '');
			  tpl=tpl.replace(/__VALUE_PA2__/g, '');
             
			  $(this).closest('table').find('tbody tr.no-row').remove();     
		      $(this).closest('table').find('tbody').append(tpl);		  
        });
		
		 $("#shipping-container").on('click','a.remove-row',function(){									  
	        $(this).closest('tr').remove();	
			 if($('#shipping-'+current_tab).find('.tbl-wrapper tbody tr').length==0){
			  $('#shipping-'+current_tab).find('.tbl-wrapper tbody').append('<tr class="no-row"><td colspan="6"><?php echo $no_unit_row;?></td></tr>');     	
		    }									  
        });
        
        $("#shipping-container").on('click','a.delete-all',function(){									  
	       $(this).closest('div.tbl-wrapper').find('tbody > tr').remove();	
		   if($('#shipping-'+current_tab).find('.tbl-wrapper tbody tr').length==0) {
			  $('#shipping-'+current_tab).find('.tbl-wrapper tbody').append('<tr class="no-row"><td colspan="6"><?php echo $no_unit_row;?></td></tr>');     	
		    }										  
        });
        
		/* CSV upload*/
		$("#shipping-container").on('click', 'a.import-btn', function() {
		    $('input.import-csv:file').trigger('click');      
		});
		
		$("input.import-csv:file").change(function (){
		    var file = $('input.import-csv:file').get(0).files[0];
			
           var fd = new FormData();
           fd.append('file', file);
		    $.ajax({
					url: 'index.php?route=extension/shipping/xshippingpro/csv_upload&token=<?php echo $token; ?>',
					dataType: 'json',
                  	data:fd,
				    processData: false,
                  	contentType: false,
                  	type:'POST',
				    beforeSend: function(){
				      showLoader();
					},
					success: function(json) {		
					  
					   if (json['data']) {
							   for(i=0;i<json['data'].length;i++) {
								  var data=json['data'][i];
								  var tpl=range;
								  var pa1=1, pa2=0;
								  if(data.pa=='1') pa1='selected';
								  if(data.pa=='0') pa2='selected';
								  tpl=tpl.replace(/__INDEX__/g, current_tab);
								  tpl=tpl.replace(/__VALUE_START__/g, data.start);
								  tpl=tpl.replace(/__VALUE_END__/g, data.end);
								  tpl=tpl.replace(/__VALUE_COST__/g, data.cost);
								  tpl=tpl.replace(/__VALUE_PG__/g, data.pg);
								  tpl=tpl.replace(/__VALUE_PA1__/g, pa1);
								  tpl=tpl.replace(/__VALUE_PA2__/g, pa2);
								  $('#shipping-'+current_tab).find('.tbl-wrapper tbody').append(tpl);	
							  }
                if ($('#shipping-'+current_tab).find('.tbl-wrapper tbody tr').length > 1) {
                   $('#shipping-'+current_tab).find('.tbl-wrapper tbody tr.no-row').remove();
                }
              }
					    if (json['error']) {
                  alert(json['error']);
              }
              $('.global-waiting').remove();	  
					}
			 }); 
		});
          
        /* End of CSV upload*/
         
        /*Quick save*/
        $('#quick_save, #save_form').click(function(){
    			
    			save_type = $(this).attr('id');
    			
    			$('#method-list').find('li.draggable a').each(function(sort_order){
    			   var data,
    			   	   tab_id,
    			   	   $this = $(this);
    			   tab_id = $this.attr('href');
    			   tab_id = tab_id.replace('#shipping-','');
    			   data = $('#shipping-container #shipping-'+tab_id).find(':input').serialize();
    			   shipping_methods.push({
    			     tab_id:tab_id,
               sort_order: sort_order,
    			     data: data
    			   });
    			});
    			
    			var general_data = $('#tab-general :input').serialize();
    			var is_redirect = (save_type === 'save_form' && shipping_methods.length == 0);
          saveGeneral(general_data, is_redirect);
    			
    			if(shipping_methods.length > 0) {
			    	showLoader();
			    	saveMethod();
			    }
		});           
       
	    
 });
 
 
 function saveGeneral(data, is_redirect) {
   
   if(data) {
   
  		$.ajax({
			url: 'index.php?route=extension/shipping/xshippingpro/save_general&token=<?php echo $token; ?>',
			dataType: 'json',
        	data:data,
        	type:'POST',
			success: function(json) {		
			     if (json['error']) {
                 alert(json['error']);
                 $('.global-waiting').remove();
           }
           if (is_redirect) {
             location = '<?php echo htmlspecialchars_decode($cancel); ?>';
           } 
			}
		});
	}
 }
 
 function saveMethod() {
   
   if(shipping_methods.length > 0) {
   
   		var method_data = shipping_methods.pop();
  		$.ajax({
			url: 'index.php?route=extension/shipping/xshippingpro/quick_save&token=<?php echo $token; ?>&tab_id='+method_data.tab_id + '&sort_order='+method_data.sort_order,
			dataType: 'json',
        	data:method_data.data,
        	type:'POST',
			success: function(json) {		
			  if (json['error']) {
                 alert(json['error']);
                 $('.global-waiting').remove();
               }
               else {
               	 if (shipping_methods.length === 0) {
               	   $('.global-waiting').remove();
               	   
               	   if(save_type === 'save_form') {
               	     location = '<?php echo htmlspecialchars_decode($cancel); ?>';
               	   }
               	   	
               	 } else {
               	   saveMethod();
               	 }
               }   
			}
		});
	}
 }
 
 function deleteMethod(tab_id) {
   
   if(tab_id) {
   		showLoader();
  		$.ajax({
			url: 'index.php?route=extension/shipping/xshippingpro/delete&token=<?php echo $token; ?>&tab_id='+tab_id,
			dataType: 'json',
        	type:'GET',
			success: function(json) {		
			  if (json['error']) {
                 alert(json['error']);
               } 
               $('.global-waiting').remove();	  
			}
		});
	}
 }
 
 function showLoader() {
   $('body').append('<div class="global-waiting">Processing...</div>');
   $('.global-waiting').css({top:'50%',left:'50%',marginTop:'-40px',marginLeft:'-75px'});
 }
 
 function enableEvents(no_of_tab){ 
		  /* new */
		  var my_method_area=$('#shipping-'+no_of_tab);
         $('#method-list a.tab'+no_of_tab).trigger('click');
		  $('#language'+no_of_tab+' li:first-child').trigger('click');
		  $('#method-tab-'+no_of_tab+' li:first-child').trigger('click');
		  $("[data-toggle='tooltip']",my_method_area).tooltip(); 
		  $('input[name=\'category\']', my_method_area).autocomplete(auto_complete_cat);
		  $('input[name=\'product\']', my_method_area).autocomplete(auto_complete_prod);
		  $('input[name=\'option\']', my_method_area).autocomplete(auto_complete_option); 
      $('input[name=\'input_manufacturer\']', my_method_area).autocomplete(auto_complete_manufacturer); 
      $('input[name=\'input_location\']').autocomplete(auto_complete_location);
		  $('input.hide-shipping', my_method_area).autocomplete(auto_complete_methods);

      $('.date').datetimepicker({
        pickTime: false
      });
 }
 
 function copyMethod(tabId) {
   
	      var no_of_tab=$('#shipping-container').find('div.shipping').length;
	      no_of_tab=parseInt(no_of_tab)+1;
	      //finding qnique id
	      while($('#shipping-'+no_of_tab).length!=0)
	      {
		   no_of_tab++;
	      }
          
          var tab_item=$('#shipping-'+tabId).clone()
          var tab_html='<div id="shipping-'+no_of_tab+'" class="tab-pane shipping">'+tab_item.html()+'</div>';
          
		   
 		  tab_html = tab_html.replace(/xshippingpro\[([a-z_]+)\]/igm, 'xshippingpro[$1]');
 		  tab_html = tab_html.replace(/_(\d+)/g, '_'+no_of_tab); 
          
		  $('#shipping-container').append(tab_html); 
		  
		  var inputs_text = $('#shipping-'+tabId+' input[type="text"]');
		  var inputs_text_new = $('#shipping-'+no_of_tab+' input[type="text"]');
		  
		  var inputs_checkboxes = $('#shipping-'+tabId+' input[type="checkbox"]');
		  var inputs_checkboxes_new = $('#shipping-'+no_of_tab+' input[type="checkbox"]');
		  
		  var inputs_selects = $('#shipping-'+tabId+' select');
		  var inputs_selects_new = $('#shipping-'+no_of_tab+' select');
		  
		  inputs_text.each(function(index) {
              inputs_text_new.eq(index).val($(this).val());
          });
          
          inputs_selects.each(function(index) {
              inputs_selects_new.eq(index).val($(this).val());
          });
          
          inputs_checkboxes.each(function(index) {
              if($(this).prop('checked')) {
                inputs_checkboxes_new.eq(index).prop('checked','checked');
              } else {
                inputs_checkboxes_new.eq(index).removeAttr('checked');
              }   
          });
	
	
		  $('#method-list').append('<li draggable="true" class="draggable"><a draggable="false" data-toggle="tab" class="tab'+no_of_tab+'" href="#shipping-'+no_of_tab+'">'+$('a.tab'+tabId).html()+'</a><i class="fa fas fa-arrows-alt"></i></li>');
		  enableEvents(no_of_tab);
		  current_tab=no_of_tab;
 }

 function makedropzone() {
     var dragged,
         container;
     
     $("#form-xshippingpro").on('drag', '.draggable-container', function(e) { 
         e.preventDefault();
     }); 
     $("#form-xshippingpro").on('dragstart', '.draggable-container', function(e) {
         if (e.target.nodeName.toLowerCase() == 'input') {
            e.preventDefault();
            return;
         }
         var $this = $(e.target).closest('.draggable');
         dragged = $this[0];
         container = $(e.target).closest('.draggable-container')[0];
         $this.removeClass('dragging').addClass('dragging');
     });
     $("#form-xshippingpro").on('dragend', '.draggable-container', function(e) { 
         var $this = $(e.target).closest('.draggable');
         $this.removeClass('dragging');
     });
     $("#form-xshippingpro").on('dragover', '.draggable-container', function(e) { 
          e.preventDefault();
     });
     $("#form-xshippingpro").on('dragenter', '.draggable-container', function(e) { 
         e.preventDefault();
         var $this = $(e.target).closest('.draggable');
         var _container = $(e.target).closest('.draggable-container')[0];
         if ($this[0] !== dragged && container === _container) {
             $this.removeClass('dropable').addClass('dropable');
         }
     });
     $("#form-xshippingpro").on('dragleave', '.draggable-container', function(e) { 
          e.preventDefault();
          var _container = $(e.target).closest('.draggable-container')[0];
          var $this = $(e.target).closest('.draggable');
          if ($this[0] !== dragged && container === _container) {
            $this.removeClass("dropable");
          }
     });
     $("#form-xshippingpro").on('drop', '.draggable-container', function(e) {
          var $this = $(e.target).closest('.draggable');
          var _container = $(e.target).closest('.draggable-container')[0];
          if ($this[0] !== dragged && container === _container) { 
             var from = $(dragged);
             var to = $(e.target).closest('.draggable');

             var from_dest = from.prev('.draggable')[0] || from[0];
             var to_dest = to.prev('.draggable')[0] || to[0];

             if (to_dest === from_dest) {
                if (from.prev()[0] === to[0]) from.after(to);
                else from.before(to);
             } else {
                $(to_dest).after(from);
                $(from_dest).after(to);
             }
          }
          $this.parent().find('.draggable').removeClass('dropable dragging');
     });
 }

 
 function categoryBrowser() {
    var html;
    $('#modal-category').remove();

    html  = '<div class="modal fade" id="modal-category" role="dialog">'
    html += '  <div class="modal-dialog" style="width: 500px;">'
    html += '  <div class="modal-content">'
    html += '  <div class="modal-header">'
    html += '  <button type="button" class="close" data-dismiss="modal">&times;</button>'
    html += '  <h4 class="modal-title"><?php echo $cat_selection ?></h4>'
    html += '  </div>'
    html += '  <div class="modal-body">'
    html += '  <div style="margin-bottom: 14px;" class="fiter-panel">'
    html += '   <input type="text" id="filter_category" onkeyup="onCategoryFilter()" name="filter_category" value="" placeholder="<?php echo $cat_search ?>" class="form-control" />'
    html += '  </div>'
    
    html += '  <div class="category-content">'
    html += '<table class="table table-bordered table-hover">'
    html += '   <thead>'
    html += '     <tr>'
    html += '      <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$(\'input[name*=xselected]\').prop(\'checked\', this.checked);" /></td>'
    html += '      <td class="text-center"><?php echo $text_admin_name ?></td>'
    html += '    </tr>'
    html += '  </thead>'
    html += '  <tbody>'
    html += '  </tbody>'
    html += '   </table>'
    html += '  </div>'

    html += '  </div>'
    html += '  <div class="modal-footer">'
    html += '  <label style="margin-right: 10px;"><input type="checkbox" name="inc_child">&nbsp; <?php echo $cat_inc_sub ?></label>'
    html += '  <button type="button" class="btn btn-primary" onclick="chooseBatchCategory()">Choose Selected</button>'
    html += '  </div>'
    html += '  </div>'
    html += '  </div>'
    html += '  </div>';
    
    $('body').append(html);
    $('#modal-category').modal('show');
    onCategoryFilter();
}

function onCategoryFilter() {
   $.ajax({
     url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token ?>&filter_name=' +  encodeURIComponent($('#filter_category').val()),
     dataType: 'json',
     type:'GET',
     success: function(json) {
        var out = '';
        for (var i = 0; i < json.length; i++) {
            out += '<tr>'
            out += '  <td style="width: 1px;" class="text-center"><input type="checkbox" name="xselected[]" value="'+json[i].category_id+'" /></td>'
            out += '<td class="text-left">' + json[i].name + '</td>'
            out += '</tr>'
        }

        if (!out) {
           out = '<tr><td colspan="2" class="text-center"><?php echo $cat_filter_no_data ?></td></tr>';
        }
        $('#modal-category tbody').html(out);
     }
   });
}

function chooseBatchCategory() {
   var data = $('#modal-category').find(':input').serialize();
   var url = 'index.php?route=extension/shipping/xshippingpro/fetchCategoy&token=<?php echo $token ?>';
   $('#modal-category').modal('hide');
   $.post(url, data, function(json) {
       for (var i = 0; i < json.length; i++) {
          var item = json[i];
          var my_method_area=$('#shipping-'+current_tab);
          $('.product-category' + item['category_id'],my_method_area).remove();
          $('.product-category',my_method_area).append('<div class="product-category' + item['category_id'] + '"><i class="fa fa-minus-circle"></i> ' + item['name'] + '<input type="hidden" name="xshippingpro[product_category][]" value="' + item['category_id'] + '" /></div>'); 
       }
   }, 'json');
}

var debugInternal;
function debugBrowser() {
    var html;
    $('#modal-debug').remove();

    html  = '<div class="modal fade" id="modal-debug" role="dialog">'
    html += '  <div class="modal-dialog" style="width: 500px;">'
    html += '  <div class="modal-content">'
    html += '  <div class="modal-header">'
    html += '  <h4 style="display:inline-block" class="modal-title">Live Debug Log</h4>'
    html += '  <button type="button" class="close" onclick="closeDebug()">&times;</button>'
    html += '  </div>'
    html += '  <div class="modal-body" style="height: 300px;overflow-y: auto;">'
    html += '  </div>'
    html += '  </div>'
    html += '  </div>'
    html += '  </div>';
    
    $('body').append(html);
    $('#modal-debug').modal('show');
    debugInternal = setInterval(fetchDebug, 2000);
}

function fetchDebug() {
   var url = 'index.php?route=extension/shipping/xshippingpro/giveDebug&token=<?php echo $token ?>';
   $.get(url, function(logs) {
       $('#modal-debug .modal-body .text-danger, #modal-debug .modal-body .text-success').remove();
       var current_logs = $('#modal-debug .modal-body').html();
       if (!current_logs.trim() && !logs) {
             logs = '<div class="text-success"><?php echo $text_debug_hint; ?></div>';
       }
       if (logs) {
          $('#modal-debug .modal-body').html(logs + current_logs);
          $('#modal-debug .modal-body').scrollTop(0);
       }
   });
}

function closeDebug() {
   clearInterval(debugInternal);
   $('#modal-debug').modal('hide');
}

$("#shipping-container").on('keyup','.quick-search', function(e) {
    e.preventDefault();
    var list = $(this).next('.xshipping-checkbox').find('label');
    var keyword = $(this).val().toLowerCase();
    list.each(function() {
        if (keyword && $(this).text().toLowerCase().indexOf(keyword) == -1) {
            $(this).hide();
        } else {
           $(this).show();
        }
    });
});

function fetchZone() {
   var my_method_area=$('#shipping-'+current_tab);
   var data = $('.country input[type="checkbox"]:checked',my_method_area).serialize();
   var current = $('.zone input[type="checkbox"]:checked',my_method_area).map(function(){
                  return $(this).val();
                }).toArray();
   console.log(current);
   var url = 'index.php?route=extension/shipping/xshippingpro/fetchZone&token=<?php echo $token ?>';
   $.post(url, data, function(json) {
       var html = '';
       for (var i = 0; i < json.length; i++) {
          var item = json[i];
          var checked = current.indexOf(item['zone_id']) !== -1 ? 'checked' : '';
          html +='<label><input type="checkbox" '+checked+' name="xshippingpro[zone][]" value="'+item['zone_id']+'" />'+item['name']+'</label>';
       }
       $('.zone .xshipping-checkbox',my_method_area).html(html);
   }, 'json');
}
 
//--></script>
</div>
<?php echo $_v; ?>
<?php echo $footer; ?>