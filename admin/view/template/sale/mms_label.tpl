<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
<style>
    tr {
        text-align: center;
    }
    .center {
         text-align: center;
    }
    .bold {
        font-weight: bold;
    }
    .border-bottom {
        border-bottom: 1px dotted #000;
    }
    .fssai {
        height: 35px;
        width: 100px;
        display: table;
        margin-left: auto;
        margin-right: auto;
    }
    .left {
        text-align: left;
    }
    .col-md-12, .col-xs-12, .col-sm-12, .col-md-7, .col-xs-7, .col-sm-7, .col-md-5, .col-xs-5, .col-sm-5, .col-md-6, .col-xs-6, .col-sm-6 {
        padding-right: 5px;
        padding-left: 5px;
    }
    .size-5pt {
        font-size: 7pt!important;
    }
    .size-4-7pt {
        font-size: 6.7pt!important;
    }
    .size-9pt {
        font-size: 11pt!important;
    }
    .size-8pt {
        font-size: 10pt!important;
    }
    .size-7pt {
        font-size: 9pt!important;
    }
    .size-7-5pt {
        font-size: 9.5pt!important;
    }
    .size-6pt {
        font-size: 8pt!important;
    }
    .size-20pt {
        font-size: 25pt!important;
    }
    p {
        margin: 5px 0 5px !important;
    }
    .table > tbody > tr > td {
        padding: 3px!important;
    }
    .table {
        margin-bottom: 5px!important;
    }
    .table-bordered td {
        border: 1px solid #000!important;
    }
    body {
        color: #000!important;
    }
    .barcode {
        height: 50px;
        width: 100px;
        float: right;
    }
</style>
</head>
<body>
<div class="container">
  <?php foreach ($labels as $order_label) { ?>
        <?php foreach($order_label['label'] as $label) { ?>
            <div style="page-break-after: always;">
                <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                    <img src="/image/te-png-logo.png" style="width: 110px;" />
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <h4 class="center size-20pt" style="margin-top: 5px; font-weight: bold;"><?php echo $label['name']; ?></h4>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 border-bottom">
                    <div class="col-md-7 col-xs-7 col-sm-7" style="border-right: 1px dotted #000;">
                        <p class="center bold size-5pt">NUTRITIONAL INFORMATION (Per 100g - approx)</p>
                        <table class="table table-bordered size-5pt">
                            <tbody>
                                <?php if($label['energy'] > 0) { ?>
                                    <tr>
                                        <td>Energy</td>
                                        <td><?php echo $label['energy']; ?> Kcal</td>
                                    </tr>
                                <?php } ?>
                                <?php if($label['protein'] > 0) { ?>
                                    <tr>
                                        <td>Protein</td>
                                        <td><?php echo $label['protein']; ?> g</td>
                                    </tr>
                                <?php } ?>
                                <?php if($label['carbohydrates'] > 0) { ?>
                                    <tr>
                                        <td>Carbohydrates</td>
                                        <td><?php echo $label['carbohydrates']; ?> g</td>
                                    </tr>
                                <?php } ?>
                                <?php if($label['dietary_fibres'] > 0) { ?>
                                    <tr>
                                        <td>Dietary Fibres</td>
                                        <td><?php echo $label['dietary_fibres']; ?> g</td>
                                    </tr>
                                <?php } ?>
                                <?php if($label['fats'] > 0) { ?>
                                    <tr>
                                        <td>Fats</td>
                                        <td><?php echo $label['fats']; ?> g</td>
                                    </tr>
                                <?php } ?>
                                <?php if($label['sodium'] > 0) { ?>
                                    <tr>
                                        <td>Sodium</td>
                                        <td><?php echo $label['sodium']; ?> g</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5 col-xs-5 col-sm-5" class="top-right">
                        <p class="center bold size-5pt">INGREDIENTS</p>
                        <p class="center size-4-7pt"><?php echo $label['ingredients']; ?>, Natural Soy Lecithin.</p>
                        <p class="center bold size-5pt">"No Artificial Colour, Flavours and Preservatives"</p>
                        <p class="center bold size-5pt">"Ingredients in the pack are not mentioned in any ordered sequence"</p>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 border-bottom size-9pt" style="line-height: 10px!important;">
                    <p class="center">For Feedback or queries, Call on : +91 8767 120 120</p>
                    <p class="center">or write to us on : care@true-elements.com</p>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 border-bottom size-7pt" style="line-height: 12px!important;">
                    <p class="center bold">Manufactured & Marketed by : HW WELLNESS SOLUTIONS PVT. LTD.</p>
                    <p class="center">S. NO. 246/6, Phase 2 Road, Next to KTA Spindle, Hinjawadi, Pune - 411 057 (MH).</p>
                    <p class="center"><b>FSSAI Licence No:</b> 11517036000562</p>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 border-bottom">
                    <div class="col-md-6 col-xs-6 col-sm-6 size-5pt" style="border-right: 1px dotted #000;">
                        <p class="center bold">HOW TO USE</p>
                        <p class="center">These True Elements Snacks can be consumed directly or sprinkled over breakfast and soup or ground into powder and added to food.</p>
                    </div>
                    <div class="col-md-6 col-xs-6 col-sm-6 size-5pt">
                        <p class="center bold">ALLERGEN INFORMATION :</p>
                        <p class="center">Contains <span class="bold">Soy<?php if($allergy_almonds == 1) { ?> & Almonds<?php } ?></span>. Packed in a facility that also handles <span class="bold">Tree nuts</span> and Cereals containing <span class="bold">Gluten</span>.</p>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 size-5pt border-bottom">
                    <?php if($label['regular_packaging'] == 0) { ?>
                        <p class="center"><span class="bold">STORAGE INFORMATION : </span>This being plastic free packaging. Transfer it to airtight container when received. Store airtight container in cool, dry and dark place away from moisture to avoid contamination. Best consumed within 15 days of opening.</p>
                    <?php } else { ?>
                        <p class="center"><span class="bold">STORAGE INFORMATION : </span>Store in a cool, dry and dark place away from moisture and direct sunlight to avoid infestation. Best consumed within 15 days of opening.</p>
                    <?php } ?>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 size-5pt border-bottom">
                    <p class="center"><span class="bold">This pack contains oxygen absorber. Remove it after opening. Do Not Eat.</span></p>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 border-bottom">
                    <p class="bold size-8pt" style="border: 1px solid #000; width: 100%; display: table; padding: 2px; text-align: center;">BEST BEFORE 1 MONTH FROM THE DATE OF MANUFACTURE</p>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top: 3px;">
                    <div class="col-md-7 col-xs-7 col-sm-7">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="left bold size-7-5pt" style="padding-right: 30px;">Net Weight</td>
                                    <td class="size-7-5pt"><?php echo round($label['weight'],0); ?> g (<?php echo round($label['weight'] / 28.35,2); ?> oz)</td>
                                </tr>
                                <tr>
                                    <td class="left bold size-7-5pt" style="padding-right: 30px; line-height: 10px;">MRP<br/><span class="size-6pt" style="font-weight: normal!important;">(Incl. of all Taxes)</size></td>
                                    <td class="size-7-5pt">Rs.<?php echo $label['price']; ?></td>
                                </tr>
                                <tr>
                                    <td class="left bold size-7-5pt" style="padding-right: 30px;">BATCH NO.</td>
                                    <td class="size-7-5pt"><?php echo $todays_batch; ?></td>
                                </tr>
                                <tr>
                                    <td class="left bold size-7-5pt" style="padding-right: 30px;">MFG DATE.</td>
                                    <td class="size-7-5pt"><?php echo date('d-m-Y'); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-5">
                        <img src="https://www.true-elements.com/image/barcode-8906112661783.png" class="barcode" />
                    </div>
                </div>
            </div>
        <?php } ?>
  <?php } ?>
</div>
</body>
</html>