<title>Delhivery Label</title>
<?php if ($delhiveryLastmile_manageLabel) { ?>
<?php foreach ($delhiveryLastmile_manageLabel as $manage_lb) { ?>
	<?php if ($manage_lb['page_break']) { ?>
		<P style="page-break-before: always">
	<?php } ?>
	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<td width="162" align="center"><img src="view/image/logo.png" height="30px"></td>
			<td width="206" align="center"><img src="<?php echo $manage_lb['delhivery_logo']; ?>" height="30px"></td>
		</tr>
		<tr>
			<td><img style="width: 100%;" src="<?php echo $manage_lb['barcode']; ?>" height="60px"/></td>
			<td>Order#:&nbsp;<?php echo $manage_lb['order_id']; ?><br>
			<strong>Payment Mode:<?php echo $manage_lb['methodcode']; ?></strong></td>
		</tr>
		<tr>
		    <td ><strong>Seller:</strong><br> 
		            <?php echo $manage_lb['store_name']; ?><br>
		            <?php echo $manage_lb['store_address']; ?><br>
    		        GSTIN: <?php echo $manage_lb['gst']; ?><br>
    			    <?php  if($manage_lb['tin'] !=''){TIN: echo $manage_lb['tin']; }?><br>
    			    <?php  if($manage_lb['cst'] !=''){CST: echo $manage_lb['cst']; }?><br> 
			</td>
			<td><strong>Shipping Address :</strong><br>
			<?php echo $manage_lb['shipping_address']; ?></td>
			
		</tr>
		<tr>
			<td colspan="2">
			<table border="1" width="100%" cellspacing="0" cellpadding="3" style="width: 100%;">
				<tr>
					<td><strong>Product</strong></td>
					<td><strong>Price</strong></td>
					<td><strong>Qty</strong></td>
					<td><strong>Total</strong></td>
				</tr>
				<?php foreach ($manage_lb['items'] as $manage_items) { ?>
					<tr>
						<td><?php echo $manage_items['name']; ?></td>
						<td><?php echo $manage_items['price']; ?></td>
						<td><?php echo $manage_items['qty']; ?></td>
						<td><?php echo $manage_items['total']; ?></td>
					</tr>
				<?php } ?>
				<?php foreach ($manage_lb['O_totals'] as $manage_total) { ?>
					<tr>
						<td colspan="3" align="right"><strong><?php echo $manage_total['title']; ?>&nbsp;</strong></td>
						<td>&nbsp;<?php echo $manage_total['value']; ?></td>
					</tr>
				<?php } ?>

			</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" >Return Address:&nbsp;<?php echo $manage_lb['return_address']; ?></td>
		</tr>
	</table>
<?php } ?>
<?php } ?>


<style type = "text/css">
table {
  border-collapse: collapse;
  width: 100%;
  
}
table, th, td {
  border: 1px solid black;
  font-size: 9px;
}
   <!--
      @media screen {
         table { font-family:verdana, arial, sans-serif; margin:auto; }
      }

      @media print {
          table { font-family:verdana, arial, sans-serif; float:left; }
      }
   -->
</style>

<script type="text/javascript">
<!--
window.print();
//-->
</script>