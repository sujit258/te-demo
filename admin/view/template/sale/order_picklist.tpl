<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<body>
<div class="container">
  <h3 class="center"><?php echo $text_picklist; ?></h3>
  <div style="page-break-after: always;">
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_product; ?></b></td>
          <td class="text-center"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right" style="width: 10%;"></td>
          <td class="text-right" style="width: 10%;"></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($merged_products as $product) { ?>
        <tr>
          <td><?php echo $product['name']; ?><b><?php echo $product['pack']; ?></b>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php if ($option['weight'] > 0) { echo $option['value'], " (", round($option['weight']), "gm)"; } else { echo $option['value']; } ?></small>
            <?php } ?></td>
          <td class="text-center" style="font-weight: bold;<?php if($product['quantity'] > 1) { echo 'font-size: 16px;'; } ?>"><?php echo round($product['quantity']); ?></td>
          <td class="text-right"></td>
          <td class="text-right"></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
</body>
</html>