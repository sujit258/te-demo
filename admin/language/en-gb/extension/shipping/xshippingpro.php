<?php
// Heading
$_['heading_title']    = 'X-Shipping Pro';
$_['tab_general']    = 'Global Setting';
$_['tab_rate']    = 'Method Setting';
$_['module_status']    = 'Module Status';
$_['text_debug']    = 'Debugging:';
$_['text_description']    = 'Show Shipping Description in Order Email:';
$_['text_desc_estimate_popup']    = 'Estimate Shipping & Taxes';
$_['text_desc_delivery_method']    = 'Delivery Method Step';
$_['text_desc_confirmation']    = 'Confirmation Step';
$_['text_desc_site_order_detail']    = 'Customer Order Detail';
$_['text_desc_admin_order_detail']    = 'Admin Order Detail';
$_['text_desc_order_email']    = 'Email';
$_['text_desc_order_invoice']    = 'Invoice';
$_['text_edit']        = 'Edit X-Shipping Pro Shipping';

// Text 
$_['text_extension']  = 'Extensions';
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified X-shipping Pro!';

$_['text_select_all']    = 'Select All';
$_['text_unselect_all']    = 'Unselect All';
$_['text_any']    = 'For Any';
$_['text_heading']    = 'Heading';
$_['no_unit_row']    = 'There is no unit range for this method!';

//group
$_['text_group_shipping_mode']    = 'Group in groups';
$_['entry_group']    = 'Select Group';
$_['entry_group_tip']    = 'Select Group';
$_['text_group_none']    = 'None';
$_['text_no_grouping']    = 'No Grouping';
$_['text_lowest']    = 'Lowest';
$_['text_highest']    = 'Highest';
$_['text_average']    = 'Average';
$_['text_sum']    = 'Sum';
$_['text_and']    = 'AND';
$_['text_group_type']    = 'Group Type';
$_['text_group_limit']    = 'Number of methods to show';
$_['text_group_name']    = 'Group Name';
$_['text_method_group']    = 'Group in methods';
$_['tip_method_group']    = 'You can divide your shipping methods up to 10 groups and can be applied different operation to the group. In case of highest/lowest operation, you can set limit the number of methods that you want to show. Here are available group mode <br /><b>Lowest</b>: Only shipping method with lowest cost would be shown .<br /><b>Highest</b>: Only Shipping method with highest cost would be shown.<br /><b>Average</b>: All shipping costs would be averaged.<br /><b>Sum</b>: All shipping costs would be summed.';
$_['tab_general_global']    = 'Group Option';
$_['tab_general_general']    = 'General';


$_['text_add_new_method']    = 'Add New Method';
$_['text_remove']    = 'Remove';
$_['text_general']    = 'General';
$_['text_criteria_setting']    = 'Criteria Setting';
$_['text_category_product']    = 'Category/Product';
$_['text_price_setting']    = 'Price Setting';
$_['text_others']    = 'Others';
$_['text_zip_postal']    = 'Zip/Postal';
$_['text_enter_zip']    = 'Enter Zip/Postal Code';
$_['text_zip_rule']    = 'Zip/Postal Rule';
$_['text_zip_rule_inclusive']    = 'Only for entered zip/postal codes';
$_['text_zip_rule_exclusive']    = 'For all except entered zip/postal codes';
$_['text_group_limit']    = 'No. of showing method:';
$_['text_method_remove']    = 'Remove method';
$_['text_method_copy']    = 'Copy this method';

$_['text_coupon']    = 'Coupon';
$_['text_enter_coupon']    = 'Enter Coupon Code';
$_['text_coupon_rule']    = 'Coupon Rule';
$_['text_coupon_rule_inclusive']    = 'Only for entered coupon codes';
$_['text_coupon_rule_exclusive']    = 'For all except entered coupon codes';

$_['text_rate_type']    = 'Shipping By';
$_['text_rate_flat']    = 'Flat Amount';
$_['text_rate_quantity']    = 'Quantity';
$_['text_rate_weight']    = 'Weight';
$_['text_rate_volume']    = 'Volume';
$_['text_rate_total']    = 'SubTotal +  Tax of SubTotal';
$_['text_grand_total']    = 'Grand Total';
$_['text_rate_total_coupon']    = 'SubTotal + Tax of SubTotal - Coupon/Reward Value';

$_['text_rate_sub_total']    = 'SubTotal';
$_['text_unit_range']    = 'Unit Range';
$_['text_delete_all']    = 'Delete All';
$_['text_csv_import']    = 'CSV Import';
$_['text_start']    = 'Start';
$_['text_end']    = 'End';
$_['text_cost']    = 'Cost';
$_['text_qnty_block']    = 'Per Unit Block';
$_['text_add_new']    = 'Add New';
$_['text_final_cost']    = 'Final Cost';
$_['text_final_single']    = 'Single';
$_['text_final_cumulative']    = 'Cumulative';
$_['text_percentage_related']    = 'Percentage related to';
$_['text_percent_sub_total']    = 'SubTotal';
$_['text_percent_total']    = 'SubTotal + Tax of SubTotal';
$_['text_percent_shipping']    = 'Shipping Cost';
$_['text_percent_sub_total_shipping']    = 'SubTotal + Shipping Cost';
$_['text_percent_total_shipping']    = 'SubTotal + Tax of SubTotal + Shipping Cost';
$_['text_price_adjustment']    = 'Price Adjustment';
$_['text_price_min']    = 'Min';
$_['text_price_max']    = 'Max';
$_['text_price_add']    = 'Modifier e.g. +5';
$_['text_days_week']    = 'Days of Week';
$_['text_time_period']    = 'Time Period';
$_['text_sunday']    = 'Sunday';
$_['text_monday']    = 'Monday';
$_['text_tuesday']    = 'Tuesday';
$_['text_wednesday']    = 'Wednesday';
$_['text_thursday']    = 'Thursday';
$_['text_friday']    = 'Friday';
$_['text_saturday']    = 'Saturday';
$_['text_logo']    = 'Logo URL';

$_['entry_all']    = 'Exact';
$_['entry_any']    = 'Any';
$_['text_mask_price']    = 'Show entered text instead of showing cost during checkout. Available only in Shipping selection step.';
$_['text_mask_title']    = 'Price Text Masking';

// Entry 
$_['entry_weight_include']       = 'Include Weight in the Name:';
$_['entry_cost']       = 'Shipping Cost:';
$_['entry_name']       = 'Method Name:';
$_['entry_desc']       = 'Description:';
$_['entry_order_total']       = 'Product Total Range:';
$_['entry_order_weight']       = 'Weight Range:';
$_['entry_quantity']       = 'Quantity Range:';
$_['entry_to']       = 'to';
$_['entry_order_hints']       = 'Please enter 0 (zero) if not applicable';
$_['entry_tax']        = 'Tax Class:';
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';
$_['entry_customer_group'] = 'Customer Group:';
$_['entry_store'] = 'Store:';
$_['entry_manufacturer'] = 'Manufacturer:';
$_['store_default'] = 'Default';
$_['text_all'] = 'Any';
$_['text_category'] = 'Category Rule';
$_['text_multi_category'] = 'Multi-Categories Rule';
$_['text_category_any'] = 'For any categories';
$_['text_category_all'] = 'Must have selected categories with others';
$_['text_category_least'] = 'Any of the selected categories';
$_['text_category_least_with_other'] = 'Any of the selected categories with others';
$_['text_category_except'] = 'Except the selected categories';
$_['text_category_exact'] = 'Must have selected categories';
$_['text_category_except_other'] = 'Except the selected categories with others';

$_['entry_category']     = 'Categories';
$_['text_product'] = 'Product Rule';
$_['text_product_any'] = 'For any products';
$_['text_product_all'] = 'Must have selected products with others';
$_['text_product_least'] = 'Any of the selected products';
$_['text_product_least_with_other'] = 'Any of the selected products with others';
$_['text_product_exact'] = 'Must have selected products';
$_['text_product_except'] = 'Except the selected products';
$_['text_product_except_other'] = 'Except the selected products with others';
$_['entry_product']     = 'Products';
$_['text_geo_address']     = 'GEO zone address type';
$_['text_delivery']     = 'Shipping Address';
$_['text_payment']     = 'Payment Address';
$_['text_no_of_category']     = 'No. of Categories';
$_['text_no_of_manufacturers']     = 'No. of Manufacturers';


$_['text_manufacturer_rule'] = 'Manufacturer Rule';
$_['text_manufacturer_any'] = 'For any manufacturers';
$_['text_manufacturer_all'] = 'Must have selected manufacturers with others';
$_['text_manufacturer_least'] = 'Any of the selected manufacturers';
$_['text_manufacturer_least_with_other'] = 'Any of the selected manufacturers with others';
$_['text_manufacturer_exact'] = 'Must have selected manufacturers';
$_['text_manufacturer_except'] = 'Except the selected manufacturers';
$_['text_manufacturer_except_other'] = 'Except the selected manufacturers with others';

$_['text_dimensional_weight'] = 'Dimensional Weight';
$_['text_volumetric_weight'] = 'Volumetric Weight';
$_['text_dimensional_factor'] = 'Factor Value';
$_['text_dimensional_overrule'] = 'Consider Actual Weight if it is greater';
$_['text_weight_eq'] = 'Note: Xshippingpro consider these equations. For Dimensional Weight = (volume / factor) x weight. For Volumetric Weight = (volume / factor)';

$_['button_save_continue'] = 'Save and Stay';
$_['ignore_modifier'] = 'Ignore/Discard Price Adjustment until <i>Per unit block</i> taken into account';

$_['entry_group_name']       = 'Group Name (Optional)';
$_['text_admin_name'] = 'Name';
$_['text_admin_name_tip'] = 'For admin use only. It helps you to remember your shipping method by an alternative name';
$_['text_name_tip'] = 'This name will be appeared during checkout';
$_['text_hide'] = 'Hide selected methods if this method become active:';
$_['text_hide_placeholder'] = 'Type Method Name to hide';
$_['text_hide_inactive'] = 'Hide selected methods if this method become inactive';
$_['entry_location'] = 'Product Location';
$_['text_location_rule'] = 'Location Rule';
$_['text_location_any'] = 'For any locations';
$_['text_location_all'] = 'Must have selected locations with others';
$_['text_location_least'] = 'Any of the selected locations';
$_['text_location_least_with_other'] = 'Any of the selected locations with others';
$_['text_location_exact'] = 'Must have selected locations';
$_['text_location_except'] = 'Except the selected locations';
$_['text_location_except_other'] = 'Except the selected locations with others';
$_['check_all'] = 'Check all';
$_['uncheck_all'] = 'Uncheck all';
$_['select_multiple'] = 'Bulk Selection';
$_['grand_total_before_shiping'] = 'Summed Total Before Shipping';
$_['text_no_of_location'] = 'No. of Locations';


$_['text_city']    = 'City';
$_['text_city_enter']    = 'Enter City';
$_['tip_city']    = 'Please select applicable cities';
$_['text_city_enter_tip']    = 'Multiple cities are supported. Newline/Comma Separated';
$_['text_city_rule']    = 'City Rule';
$_['text_city_rule_inclusive']    = 'Only for entered cities';
$_['text_city_rule_exclusive']    = 'For all except entered cities';
$_['text_coupon_tip']    = 'Multiple Coupon allowed. Newline/Comma(,) Separated';
$_['text_country_tip']    = 'Please select applicable country';

/* tooltip */
$_['tip_group_name']       = 'If provided any name, It would show group name instead of method name during checkout. It is possible to place original method name and prices in the group name. To put name, price and name-price pair, you have to put @, $ and @$ respectively.';
$_['tip_group_desc']       = 'If provided any description, It would show this description instead of method description during checkout. It is possible to place original method name and prices in the description. To put name, price and name-price pair, you have to put @, $ and @$ respectively.';
$_['tip_group_limit']       = 'You can limit number of highest/lowest methods to show. By default it would show only one method';
$_['tip_weight_include']       = 'It would show cart weight beside shipping method name';
$_['tip_sorting_own']       = 'Sorting order with respective to x-shipping methods';
$_['tip_status_own']       = 'Enable/Disable this particular method only';
$_['tip_store']       = 'Please Select Stores for which this shipping method will work';
$_['tip_geo']       = 'Please Select Geo Zones for which this shipping method will work';
$_['tip_manufacturer']       = 'Please Select Manufacturer for which this shipping method will work';
$_['tip_customer_group']       = 'Please Select customer groups for which this shipping method will work';
$_['tip_zip']       = 'Please enter zip/postal for which this shipping method will work';
$_['tip_coupon']       = 'Please enter coupon code for which this shipping method will work';
$_['tip_category']       = '<b>For any Categories</b>: Valid for any categories.<br /><b>Any of the selected categories</b>: Shopping cart must contain at least one of the selected categories. Other categories are not allowed.<br /><b>Any of the selected categories with others</b>: Shopping cart must contain at least one of the selected categories with  other categories.<b>Must have selected categories</b>: Shopping cart must contain the selected categories. Other categories are not allowed.<br /><b>Must have selected categories with others</b>: Shopping cart must contain the selected categories with  others.<br /><br /><b>Except the selected product</b>: Shopping cart must not contain any of the selected categories. Only non-selected categories are allowed .<br /><b>Except the selected categories with others</b>: Shopping cart might have selected categories if and only if shopping cart contain at least non-selected categories';
$_['tip_multi_category']       = 'Define how categories would be treated if a product is assigned or belong to more than one category. If you choose this rule is <i>Any</i>, then you don\'t need to select all the categories of a product at the below category selection box because xshippingpro will automatically find out all applicable categories for you.  If you choose <i>Exact</i>, then xshippingpro will just consider provided categories. For example: a product belongs to category A, B and C and. Now if you choose this rule to Any and choose only one category A here, then xshippingpro automatically will fetch other categories B and C as well and final categoryies would be A, B and C. But if you choose <b>Exact</b>, then xshippingpro won\'t take any other categories into account other than provided ones';

$_['tip_product']       = '<b>For any Products</b>: Valid for any products.<br /><b>Must have selected products</b>: Shopping cart must contain the selected products. Other products are not allowed.<br /><b>Must have selected products with others</b>: Shopping cart must contain the selected products with  others.<br /><b>Any of the selected products</b>: Shopping cart must contain at least one of the selected products. Other products are not allowed.<br /><b>Any of the selected products with others</b>: Shopping cart must contain at least one of the selected products with  other products.<br /><b>Except the selected product</b>: Shopping cart must not contain any of the selected products. Only non-selected products are allowed .<br /><b>Except the selected products with others</b>: Shopping cart might have selected products if and only if shopping cart contain at least non-selected products';
$_['tip_manufacturer_rule']       = '<b>Must have selected manufacturers</b>: Shopping cart must contain the selected manufacturers. Other manufacturers are not allowed.<br /><b>Must have selected manufacturers with others</b>: Shopping cart must contain the selected manufacturers with  others.<br /><b>Any of the selected manufacturers</b>: Shopping cart must contain at least one of the selected manufacturers. Other manufacturers are not allowed.<br /><b>Any of the selected manufacturers with others</b>: Shopping cart must contain at least one of the selected manufacturers with  other manufacturers.<br /><b>Except the selected product</b>: Shopping cart must not contain any of the selected manufacturers. Only non-selected manufacturers are allowed .<br /><b>Except the selected manufacturers with others</b>: Shopping cart might have selected manufacturers if and only if shopping cart contain at least non-selected manufacturers';
$_['tip_rate_type']       = '<b>Flat Amount</b>: Only a single flat value.<br /><b>Quantity</b>: Total quantities of the shopping cart.<br /><b>Weight</b>: Total weight of the shopping cart.<br /><b>Volume</b>: Total volume of the shopping cart.<br /><b>SubTotal + Tax of SubTotal</b>: Cart Sub-total with tax.<br /><b>SubTotal</b>: Cart Sub-total without tax. <br /><b>Grand Total</b>: Grand Total except shipping cost.';
$_['tip_cost']       = 'Percentage is allowed. e.g. 5%, 99.99 etc.';
$_['tip_unit_start']       = 'Counting will start from this value';
$_['tip_unit_end']       = 'Counting will end in this value';
$_['tip_unit_price']       = 'If \'<i>Per Unit Block</i>\' is 0, then Price will be for the selected range of this Row. <br /> If \'<i>Per Unit Block</i>\' is greater than 0, then price willl be for the \'Per unit block\'. Percentage is allowed e.g. 5, 7% etc.';
$_['tip_unit_ppu']       = '<b>Per unit block</b>: If you want to calculate shipping cost based on per unit block you can use this feature. For example, if you want to charge $5 for every 3 items up to total quanity is 10. So you will have to set: Start=0, End=10, Cost=5 and Per unit block=3. Simply <i>Per Unit Block</i> means price for every nth unit(s)';
$_['tip_single_commulative']       = '<b>Single</b>: It considers only one unit row which within shopping cart value fall under. <br /> <b>Cumulative</b>: It accrues/cumulate the value of the unit rows from starting to the row which within shopping cart value fall under. Note: Shopping cart value means either Weight or Total or Volume etc depending on `Shipping By` above';
$_['tip_percentage']       = 'Select the value how percentage would be calculated of. <i> Cart Unit Value idicates </i> <i> Shipping Cost, Sub-total with shipping and total with shipping are available in price adjustment section only.</i>';
$_['tip_price_adjust']       = 'You can adjust final price. <br /><b>Min</b>: If final price is less than Min, then Min will be considered . <br /><b>Max</b>: If final price is greater than Max, then Max will be considered. <br /><b>Modifier</b>: Any value with an operator(+,-,*,/) which will be added/multiplied/subtracted/divided in the final price depends on operator. e.g: +5.2 means 5.2 will be added in the final price';
$_['tip_day']       = 'Please select the day(s) for which this shipping method will be valid';
$_['tip_time']       = 'Please set time period for this shipping method. Note: Server time will be considered. Server time now: %s';
$_['tip_heading']       = 'Heading of the shipping section in the site';
$_['tip_status_global']       = 'Global Status of the module';
$_['tip_sorting_global']       = 'Global Sorting Order with respective to other shipping modules';
$_['tip_grouping']       = 'Rules are as same as `Group in methods` but this grouping would work like group within groups. Shipping methods having same <i>Sort Order</i> value would be considered as a group. Please keep default value if you are not sure what are you doing.';
$_['tip_debug']       = 'It would show the cause why a particular shipping method does not appear while checkout';
$_['tip_desc']       = 'Optional field. You could put a supportive small description under the shipping method name in the site.';
$_['tip_import']       = '<b>For CSV import:</b> CSV will should be like <br /><br /> 1,10,4.99,0,0 <br />11,20,5.99,2,0<br />21,30,6.99,0,0';
$_['tip_text_logo'] = 'Optional field.  Image URL or font-awesoem icon class is allowed. This logo/icon would appear right before shipping title. Note it may not work in all checkout modules.';

$_['tip_postal_code']='Newline/Comma Separated. Wildcards support (*, ?) and Range Support. <br /><b>Example:</b><br />12345,443300-443399,9843*,875*22,45433?,S3432?2 <br /><b>Explanation</b>:<br /> 12345: A single Postal Code <br /> 443300-443399: Postal Code start from 443300 to 443399<br /> 9843*: Any code that starts with 9843 <br />875*22:  Any code that starts with 875 and ends by 22 <br /> 45433?: Any code that start with 45433 and ends by any single alpha-numeric char. <br /> SE-1-10: Postal Code start from 1 to 10 with prefix SE i.e SE9 <br /> PA-1-10-NK: Postal Code start from 1 to 10 with prefix PK and suffix NK i.e PA9NK';

$_['text_partial']    = 'Allow Partial';
$_['tip_partial'] ='It would count partial block pricing if applicable. For example, if you set `Per Unit Block` 6 but user chose 3 only, it would charge for half block i.e. 3 units proportionally. If you set `Allow Partial` to false, customer would always charge for a complete block.';
$_['text_yes']    = 'Yes';
$_['text_no']    = 'No';
$_['text_additional']    = 'Additional Price';
$_['text_additional_till']    = 'Max Limit.';
$_['tip_additional'] ='If the shopping cart value (i.e weight or quantity etc. depending on Shipping By) fell outside of the above ranges, Xshippingpro would calculate shipping cost for additional items based on these fields. If you don\'t want to auto calculate, keep it blank OR enter 0. Default Max Limit is unlimited.';

$_['text_cart_value']    = 'Shopping Cart Modifier';
$_['tip_cart_value'] ='If you want to add/substruct/multiply/divide your shopping cart value right before calculating shipping cost, enter the value with an arithmetic operator. For example, if you want to add additional 200g packaging weight to the shopping cart weight, enter +200. Or if you want to deduct 200g, you will have to enter -200. Percentage is allowed. e.g +10%';

$_['text_sort_type']    = 'Order By';
$_['text_sort_manual']    = 'Manual';
$_['text_sort_price_asc']    = 'Ascending by Price';
$_['text_sort_price_desc']    = 'Descending by Price';
$_['text_sort_name_asc']    = 'Ascending by name';
$_['text_sort_name_desc']    = 'Descending by name';
$_['tip_text_sort_type'] ='Orders of the Shipping methods in the site. Manual order will be based on the <b>Sort Order</b> input value.';

$_['tip_weight']       = 'Inclusive value. Additional weight range option. Note that this rule does not respect product rules defined in the Product/category tab i.e it would consider full cart weight.';
$_['tip_total']       = 'Inclusive value. Additional Total range option. Note that this rule does not respect product rules defined in the Product/category tab i.e it would consider full cart total.';
$_['tip_quantity']       = 'Inclusive value. Additional quantity range option. Note that this rule does not respect product rules defined in the Product/category tab i.e it would consider total cart quantity.';

$_['text_export']    = 'Export';
$_['tip_export'] = 'Export all shipping data';
$_['text_import']    = 'Import';
$_['tip_import'] = 'Define your `Shipping Cost` vs `Range` based on what you have chosen `Shipping By` above. Warning! importing from csv file will overwrite your existing ranges.';
$_['tab_import_export']    = 'Import/Export';

$_['text_country']    = 'Country';
$_['text_help']    = 'Help';
$_['text_documentation']    = 'Documentation';
$_['text_update']    = 'Check For Update';

$_['text_method_specific']    = 'Only take the products into account as the per rules defined in Category/Product tab while calculating shipping cost';
$_['text_cat_product_ignore']    = 'Don\'t check these rules to validate shipping method. Just use these product rules for calculating shipping cost only.';
$_['text_cat_product_ignore_tip']    = 'If you check this option, xshippingpro will ignore these product/category/manufacturer/option/location rules to validate this method. In other words, these rules will no more remain active for this method, these rules only be used for estimating shipping cost.';
$_['text_batch_select']= 'Batch Selection';
$_['text_search']= 'Enter keyword to search';
$_['text_name']= 'Name';
$_['text_selection_mode']= 'Selection Mode:';
$_['text_selection_mode_exact']= 'Selected items only';
$_['text_selection_mode_exact_sub']= 'Selected items along with all sub-items';
$_['text_selection_mode_except']= 'All other rather than selected items';
$_['text_selection_select']= 'Select';


$_['text_equation']    = 'Final Equation';
$_['text_eq_placeholder']    = 'e.g. {subTotal}-5*{quantity}';
$_['tip_equation']    = 'Any arithmetic equation. If provided, final shipping cost will be calculated based on result returned by this equation. If the equation returned negetive shipping cost, shipping method would be disappeared. Ternary Operation is allowed';

$_['text_eq_cart_total'] = 'Cost of all cart\'s products';
$_['text_eq_cart_weight'] = 'Weight of all cart\'s products';
$_['text_eq_cart_qnty'] = 'Quantity of all cart\'s products';
$_['text_eq_cart_vol'] = 'Volume of all cart\'s products';
$_['text_eq_method_total'] = 'Cost of the products those are valid according to the products rule.';
$_['text_eq_method_weight'] = 'Weight of the products those are valid according to the products rule.';
$_['text_eq_method_qnty'] = 'Quantity of the products those are valid according to the products rule.';
$_['text_eq_method_vol'] = 'Volume of the products those are valid according to the products rule.';
$_['text_eq_shipping'] = 'Evaluated shipping cost of this method before equation';
$_['text_eq_modifier'] = 'Value of the Modifier of the price adjustment section';
$_['text_eq_no_man'] = 'Number of different Product Manufacturers';
$_['text_eq_no_loc'] = 'Number of different Product Locations';
$_['text_eq_no_cat'] = 'Number of different Category';
$_['text_eq_coupon'] = 'Applied Coupon Value';
$_['text_eq_reward'] = 'Applied Reward Amount';
$_['text_eq_ref_price'] = 'You can also insert price of other xshippingpro\'s method by using the method ID. For example, ID of this method is %s and you can place this {shipping%s} placeholder into the equation field of the other shipping method.';

$_['text_eq_cart_total_tax'] = 'Cost of all cart\'s products including tax';
$_['text_eq_method_total_tax'] = 'Cost of the products including tax which are valid according to the products rule';
$_['text_method_id'] = 'Method ID';
$_['text_group_id'] = 'Group ID';
$_['text_group_desc'] = 'Group Desc';
$_['text_cache'] = 'Use Cache';
$_['text_cache_tip'] = 'You must keep cache enabled for better performace.';

$_['text_option'] = 'Product Option Rule';
$_['text_option_any'] = 'For any Product Option';
$_['text_option_all'] = 'Must have selected options with others';
$_['text_option_least'] = 'Any of the selected options';
$_['text_option_least_with_other'] = 'Any of the selected options with others';
$_['text_option_exact'] = 'Must have selected options';
$_['text_option_except'] = 'Except the selected options';
$_['text_option_except_other'] = 'Except the selected options with others';
$_['entry_option']     = 'Product Options';
$_['tip_option']       = '<b>For any Product Options</b>: Valid for any options.<br /><b>Must have selected options</b>: Shopping cart must contain the selected options. Other options are not allowed.<br /><b>Must have selected options with others</b>: Shopping cart must contain the selected options with  others.<br /><b>Any of the selected options</b>: Shopping cart must contain at least one of the selected options. Other options are not allowed.<br /><b>Any of the selected options with others</b>: Shopping cart must contain at least one of the selected options with  other options.<br /><b>Except the selected product</b>: Shopping cart must not contain any of the selected options. Only non-selected options are allowed .<br /><b>Except the selected options with others</b>: Shopping cart might have selected options if and only if shopping cart contain at least non-selected options';
$_['entry_payment']  = 'Payment Method:';
$_['tip_payment']       = 'Please Select payment methods for which this method will be valid. Be Noted that this rule does not work in all checkout modules.';

$_['text_or_mode']  = 'Treat these rules as Logical OR operand. In other words, make this method valid if any of the product rules is valid';
$_['text_or_mode_tip']   = 'By default xshippingpro treats all rules as Logical AND operand. If you tick this checkbox, all these rules (product/category/manufacturer/option/location) would be treated as logical OR operand instead of doing default AND operand.';


$_['text_currency']    = 'Currency';
$_['text_currency_tip']    = 'Select applicable currencies for those this method will be available';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify X-shipping pro!';
$_['error_filetype'] = 'Invalid file type. Only CSV allowed';
$_['error_upload'] = 'Error! unable to upload';
$_['error_no_data'] = 'No data found to import';
$_['error_partial'] = 'Partial uploading error. Please check your upload size limit.';
$_['error_import'] = 'Please select a valid file to import';

$_['text_date_range']    = 'Date Range';
$_['entry_date_start']    = 'Any Date';
$_['entry_date_end']    = 'Any Date';
$_['text_date_tip']    = 'If you want to restrict shipping method by date range, please choose applicable dates. Note: Server date will be considered. Server Date: %s';
$_['rate_individual_product']    = 'For Each Individual Product';
$_['rate_individual_weight']    = 'Product Total Weight';
$_['rate_individual_volume']    = 'Product Total Volume';
$_['rate_individual_price']    = 'Product Total Price';
$_['rate_individual_quantity']    = 'Product Total Quantity';
$_['rate_individual_weight_self']    = 'Product Actual Weight';
$_['rate_individual_volume_self']    = 'Product Actual Volume';
$_['rate_individual_price_self']    = 'Product Actual Price';
$_['rate_any_product']    = 'If Any Cart Product';
$_['rate_any_price']    = 'Product Total Price';
$_['rate_any_quantity']    = 'Product Total Quantity';
$_['rate_any_weight']    = 'Product Total Weight';
$_['rate_any_volume']    = 'Product Total Volume';
$_['rate_any_price_self']    = 'Product Actual Price';
$_['rate_any_weight_self']    = 'Product Actual Weight';
$_['rate_any_volume_self']    = 'Product Actual Volume';
$_['rate_any_width_self']    = 'Product Actual Width';
$_['rate_any_height_self']    = 'Product Actual Height';
$_['rate_any_length_self']    = 'Product Actual Length';
$_['text_estimator_tab'] = 'Shipping Estimator';
$_['text_estimator_enable']    = 'Show Shipping Estimator on the product detail page';
$_['text_estimator_fields']    = 'Estimator Fields:';
$_['text_estimator_country']    = 'Country';
$_['text_estimator_zone']    = 'Region / State';
$_['text_estimator_postal']    = 'Postcode';
$_['text_estimator_css']    = 'Custom CSS';
$_['text_custom_css_help']  = 'Don\'t put inside style tag. Just enter your css rule. e.g .xshippingpro-box {background:#2f96c9;}';
$_['text_estimator_selector']    = 'Enter CSS selecotr';
$_['text_estimator_selector_help']    = 'Please enter a CSS selector where xshippingpro would append estimation box';

$_['text_sub_options'] = 'Sub Options';
$_['text_sub_option_name'] = 'Name:';
$_['text_sub_option_cost'] = 'Cost:';
$_['text_sub_option_cost_help'] = 'Additional cost for the option. You can add or subtract with main shipping cost. You also overwrite the main shipping cost. For example: +10 or -10 or 10. If you don\'t have price, left it empty.';
$_['text_sub_option_placeholder'] = 'e.g. +10 or -10 or 10';
$_['text_option_free'] = 'Make option free when shipping cost is free.';

$_['entry_search']    = 'Type to search';
$_['entry_zone']    = 'Region / State';
$_['text_zone_tip']    = 'Please select country first to display country wise zones';
$_['estimator_type']    = 'Estimator Type';
$_['estimator_type_avail']    = 'Show Shipping Availablity Only';
$_['estimator_type_method']    = 'Show Shipping Methods With Cost';
$_['text_type_equation']    = 'Equation Value';
$_['cat_filter_no_data']    = 'There is nothing to show.';
$_['cat_selection']    = 'Category Selection';
$_['cat_inc_sub']    = 'Include Sub Categories';
$_['text_chooose_selected']    = 'Choose Selected';
$_['text_remove_all']    = 'Remove All';
$_['cat_search']    = 'Category Search';
$_['text_placeholders']    = 'Placeholders';
$_['text_guest_checkout']    = 'Guest Checkout';
$_['text_debug_hint']    = 'Please try to checkout in the site. Waiting for logs...';
$_['text_debug_button']    = 'Live Debug Log';
$_['text_debug_enable_warn']    = 'Please enable Debug mode under global tab of xshippingpro and click on <b><i>Save and Stay Button</i></b> for saving debug state. Then try to checkout in the site to get rules names which are restricting a method from appearing.';
$_['text_product_error']    = 'Display non-deliverable product list in case of shipping error';
$_['text_product_error_help']    = 'It will show non-deliverable product list if no shipping method is available to the delivery address and shipping method is linked with products. It may not work on some checkout modules.';
$_['text_estimator_store']    = 'Display on Stores:';
$_['text_rate_type_distance']    = 'Distance (KM)';
$_['text_google_map_api_key']    = 'Google map API key(Optional):';
$_['text_google_map_api_key_help']    = 'Only need if you want to set shipping cost by distance. You must set Geocode on your store setting as well.';
$_['text_price_without_tax']    = 'Show Price without Tax/vat';
$_['text_price_without_tax_help']    = 'Price will be displayed without vat/tax on the method list';
$_['text_convert_negetive_zero']    = 'Convert Negetive values to zero';
$_['text_convert_negetive_zero_help']    = 'By Default if the equation returned negetive value, Xshippingpro would not display that method. But if you checked this option, negetive value would be converted to 0 and method would be appeared regardless of the returned value.';
?>