<?php
// Heading
$_['heading_title']            = 'Just Purchased Notification';

// Tab
$_['tab_general']              = 'General';
$_['tab_message']              = 'Message';
$_['tab_notification_options'] = 'Notification Options';
$_['tab_help']                 = 'Help';

// Button
$_['button_clear_cache']       = 'Clear Just Purchased Notification Cache';

// Text
$_['text_module']              = 'Modules';
$_['text_success']             = 'Success: You have modified module Just Purchased Notification!';
$_['text_cache_success']       = 'Success: Just Purchased Notification cache was cleared!';
$_['text_edit']                = 'Edit Just Purchased Notification';

$_['text_seconds']             = 'seconds';
$_['text_hours']               = 'hours';
$_['text_alert_success']       = 'Success';
$_['text_alert_info']          = 'Info';
$_['text_alert_warning']       = 'Warning';
$_['text_alert_danger']        = 'Danger';
$_['text_alert_custom']        = 'Custom';

$_['text_top']                 = 'Top';
$_['text_bottom']              = 'Bottom';
$_['text_left']                = 'Left';
$_['text_center']              = 'Center';
$_['text_right']               = 'Right';

// Entry
$_['entry_status']             = 'Status';
$_['entry_limit']              = 'Limit'; 
$_['entry_order_status']       = 'Order Status'; 
$_['entry_shuffle']            = 'Shuffle Notifications';
$_['entry_cache']              = 'Use cache';

$_['entry_message']            = 'Notification message';
$_['entry_time_ago']           = 'Show "Time ago"';
$_['entry_time_ago_minute']    = 'Minutes ago';
$_['entry_time_ago_hour']      = 'Hours ago';
$_['entry_time_ago_day']       = 'Days ago';
$_['entry_hide_older']         = 'Hide old "Time ago"';

$_['entry_image']              = 'Image Dimension';
$_['entry_width']              = 'Width';
$_['entry_height']             = 'Height';
$_['entry_alert_type']         = 'Notification Type';
$_['entry_background_color']   = 'Background Color';
$_['entry_border_color']       = 'Border Color';
$_['entry_text_color']         = 'Text Color';
$_['entry_link_color']         = 'Link Color';
$_['entry_placement']          = 'Placement';
$_['entry_allow_dismiss']      = 'Allow dismiss';
$_['entry_show_progressbar']   = 'Show Progress Bar';
$_['entry_timeout']            = 'Delay between';
$_['entry_delay']              = 'Display time';
$_['entry_animate_enter']      = 'Enter Animation';
$_['entry_animate_exit']       = 'Exit Animation';
$_['entry_zindex']             = 'Z-index';


// Help
$_['help_limit']               = 'Show notification about last x products purchased'; 
$_['help_order_status']        = 'Products from orders with selected status'; 

$_['help_message_title']       = 'Keywords';
$_['help_message']             = '{country} = customer country<br />{zone} = customer zone/state<br />{city} = customer city<br />{quantity} = product quantity <br />{product_with_link} = product name with link';
$_['help_time_ago_older']      = 'Hide "time ago" text if order is older than x hours';
$_['help_time_ago_minute']     = 'Ex: {time_counter} minutes ago';
$_['help_time_ago_hour']       = 'Ex: {time_counter} hours ago';
$_['help_time_ago_day']        = 'Ex: {time_counter} days ago';
$_['help_hide_older']          = 'IF is older than X HOURS';

$_['help_background_color']    = 'default color: #F1F2F0';
$_['help_border_color']        = 'default color: #DDDDDD';
$_['help_text_color']          = 'default color: #666666';
$_['help_link_color']          = 'default color: #666666';
$_['help_placement_from']      = 'This controls where if the notification will be placed at the top or bottom of your page.';
$_['help_placement_align']     = 'This controls if the notification will be placed in the left, center or right side of the page.';
$_['help_allow_dismiss']       = 'Alow or not to show close button';
$_['help_show_progressbar']    = 'Determine if the notification should display a progress bar or not';
$_['help_animate_enter']       = 'This will control the animation used to bring generated notification on screen';
$_['help_animate_exit']        = 'This will control the animation used to remove notification from screen';
     
$_['help_timeout']             = 'Time between 2 notifications';
$_['help_delay']               = 'After how many seconds active notification is hidden';
$_['help_zindex']              = 'This sets the css property z-index for the notification. You may have to raise this number if you have other elements overlapping the notification.'; 


// Error
$_['error_permission']         = 'Warning: You do not have permission to modify module Just Purchased Notification!';
$_['error_in_tab']             = 'Please check again. Found error in tab %s!';

$_['error_limit']              = 'Limit is required!';
$_['error_order_status']       = 'Please choose at least one order status!';
$_['error_message']            = 'Notification message format is required!';
$_['error_time_ago_minute']    = 'Time ago - minutes is required! Keyword {time_counter} need to be added in text. EX: {time_counter} minutes ago';
$_['error_time_ago_hour']      = 'Time ago - hours is required! Keyword {time_counter} need to be added in text. EX: {time_counter} minutes ago';
$_['error_time_ago_day']       = 'Time ago - days is required! Keyword {time_counter} need to be added in text. EX: {time_counter} minutes ago';
$_['error_hide_older']         = 'Incorrect value. Good Value example: 24, 30 etc!';
$_['error_image']              = 'Image width &amp; height dimensions required!';
$_['error_color']              = 'Color value - 7 chars required. Ex: #E7C4AB';
$_['error_timeout']            = 'Delay between notifications is required';
$_['error_delay']              = 'Display time is required';
?>