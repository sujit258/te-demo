<?php
// Heading
$_['heading_title']    = '<font color = "#0099FF"><b>Postcode Service Availability Checker</b></font>';

$_['heading_title1']    = 'Postcode Service Availability Checker';
$_['heading_title2']    = 'Pincode';
//$_['heading_title']    = 'Pincode';

// Text
$_['text_module']     					 = 'Modules';
$_['text_success']    					 = 'Success: You have modified Postcode Service Availability Checker module!';
$_['text_edit']   						 = 'Edit Pincode Module';
$_['text_insert_pincode'] 				 = 'Insert Pincode';
$_['text_list_pincode']     	  		 = 'List Pincode';
$_['text_insert_delivery_options']	 	= 'Insert Delivery Options';
$_['text_delhivery_option_list']  		= 'Delivery Options List';
$_['text_setting']   					= 'Settings';


// Entry
$_['entry_status']     = 'Status';


// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Postcode Service Availability Checker module!';