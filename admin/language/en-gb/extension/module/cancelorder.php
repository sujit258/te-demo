<?php
/**
 * @copyright   Copyright (C) 2009 - 2016 Open Source Technologies, Inc. All rights reserved
 * @license     GNU General Public License version 2 or later; see License.pdf
 */
// Heading
$_['heading_title'] = 'Cancel Order';

$_['text_module'] = 'Modules';
$_['text_success'] = 'Success: You have modified Cancel Order module!';
$_['text_edit'] = 'Edit Cancel Order Module';

// Entry
$_['entry_status'] = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Cancel Order module!';
