<?php
// Heading
$_['heading_title']       		= 'jOS SMS Setting';
$_['intruction_title']    		= 'Instruction';

// Text
$_['text_module']         		= 'Modules';
$_['text_success']        		= 'Success: You have modified module jOS SMS!';
$_['text_content_top']    		= 'Content Top';
$_['text_content_bottom'] 		= 'Content Bottom';
$_['text_column_left']    		= 'Column Left';
$_['text_column_right']   		= 'Column Right';
$_['text_yes']      					= 'Yes';
$_['text_no']      						= 'No';
$_['text_none']      					= '-- Select Gateway --';
$_['text_status_none']      	= '-- Select Status --';
$_['text_limit']      				= 'SMS/day';
$_['text_enable_verify']      = '<span title="Send Verification mobile phone when customers do order and/or register." data-toggle="tooltip">Enable Verification by SMS?</span>';
$_['text_verify_checkout']    = '<span title="Verification mobile phone when customers do order." data-toggle="tooltip">Order Verification?</span>';
$_['text_verify_register']    = '<span title="Verification mobile phone when customers create account." data-toggle="tooltip">Register Verification?</span>';
$_['text_verify_forgotten']   = '<span title="Verification mobile phone when customers forgot password." data-toggle="tooltip">Forgotten Password Verification?</span>';

// Gateway
$_['entry_gateway']       		= '<span title="Select gateway provider.<br />For the list of countries supported by gateways, you can visit the each gateway site." data-toggle="tooltip">Gateway</span>';

// Zenziva
$_['entry_userkey']       		= '<span title="Register or Login at zenziva.net go to SETTING > API SETTING copy &amp; paste the <b>userkey</b> into the text box." data-toggle="tooltip">Userkey</span>';
$_['entry_passkey']       		= '<span title="Register or Login at zenziva.com go to SETTING > API SETTING create new passkey if not any and copy &amp; paste the <b>passkey</b> into the text box." data-toggle="tooltip">Passkey</span>';
$_['entry_httpapi']       		= '<span title="Register or Login at zenziva.com go to SETTING > API SETTING copy &amp; paste the <b>http API</b> into the text box." data-toggle="tooltip">HTTP API</span>';
$_['httpapi_example']					= '<span class="help"><b>Use this for HTTP API-></b> http://reguler.zenziva.net/apps/smsapi.php</span>';

// AMD Telecom
$_['entry_userkey_amd']   		= '<span title="Register or Login at amdtelecom.net use your username for the <b>username</b>." data-toggle="tooltip">Username</span>';
$_['entry_passkey_amd']   		= '<span title="Register or Login at amdtelecom.net use your password for the <b>password</b>." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_amd']   		= '<span title="Register or Login at amdtelecom.net go to Help menu &amp; download the API Doc for documentation about <b>HTTP API</b> url." data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_amd']			= '<span title="Max. 11 characters, alphanumeric without spacing." data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_amd']			= '<span class="help"><b>Use this for HTTP API-></b> https://api2.amdtelecom.net/</span>';

// Bulk SMS Global
$_['entry_userkey_smsglobal']   		= '<span title="Register or Login at bulksmsglobal.in use your username for the <b>username</b>." data-toggle="tooltip">Username</span>';
$_['entry_passkey_smsglobal']   		= '<span title="Register or Login at bulksmsglobal.in use your password for the <b>password</b>." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_smsglobal']   		= '<span title="Register or Login at bulksmsglobal.in go to Developer Tool menu." data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_smsglobal']			= '<span title="Max. 11 characters, alphanumeric without spacing." data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_smsglobal']			= '<span class="help"><b>Use this for HTTP API-></b> login.bulksmsglobal.in/sendhttp.php</span>';

// Clickatell
$_['entry_userkey_clickatell']   		= '<span title="Clickatell Account Username." data-toggle="tooltip">User</span>';
$_['entry_passkey_clickatell']   		= '<span title="Clickatell Account Password." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_clickatell']   		= '<span title="Clickatell HTTP Api <b>api.clickatell.com/http/sendmsg</b>" data-toggle="tooltip">HTTP API</span>';
$_['entry_apiid_clickatell']				= '<span title="Clickatell API ID." data-toggle="tooltip">API ID</span>';
$_['entry_senderid_clickatell']			= '<span title="Your Sender ID which has been added in clickatell and approved." data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_clickatell']		= '<span class="help"><b>Use this for HTTP API-></b> api.clickatell.com/http/sendmsg</span>';
$_['entry_unicode_clickatell']			= '<span title="Send all message text as unicode." data-toggle="tooltip">Unicode</span>';

// LiveAll
$_['entry_userkey_liveall']   = '<span title="username of your account at liveall.eu." data-toggle="tooltip">Username</span>';
$_['entry_passkey_liveall']   = '<span title="password of the above username." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_liveall']   = 'HTTP API:';
$_['entry_senderid_liveall']	= '<span title="Sender name. Maximum 11 characters. Phone number is not allowed." data-toggle="tooltip">Sender</span>';
$_['httpapi_example_liveall']	= '<span class="help"><b>Use this for HTTP API-></b> http://www.liveall.eu/webservice/sms/sendSMSHTTP.php</span>';

// Malath
$_['entry_userkey_malath']   	= '<span title="This is your account username given by Malath SMS" data-toggle="tooltip">Username</span>';
$_['entry_passkey_malath']   	= '<span title="This is password of your account." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_malath']   	= '<span title="This is HTTP API URL of Malath SMS" data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_malath']		= '<span title="This is sender name. This can be only in English and only up to 11 characters.<br />Must be active your sender name before do any test." data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_malath']	= '<span class="help"><b>Use this for HTTP API-></b> sms.malath.net.sa/httpSmsProvider.aspx</span>';
$_['entry_unicode_malath']		= '<span title="Send all message text as unicode." data-toggle="tooltip">Unicode</span>';

// mobily
$_['entry_userkey_mobily']   = '<span title="Mobile number, which represents the user name" data-toggle="tooltip">Mobile</span>';
$_['entry_passkey_mobily']   = '<span title="Password for the account in mobily.ws" data-toggle="tooltip">Password</span>';
$_['entry_httpapi_mobily']   = 'HTTP API:';
$_['entry_senderid_mobily']	 = '<span title="If the sending to the numbers of Saudi mobile networks, it must send through the active names only to you." data-toggle="tooltip">Sender Name</span>';
$_['httpapi_example_mobily'] = '<span class="help"><b>Use this for HTTP API-></b> www.mobily.ws/api/msgSend.php</span>';

// msegat
$_['entry_userkey_msegat']   = '<span title="username for the account at Msegat.com" data-toggle="tooltip">Username</span>';
$_['entry_passkey_msegat']   = '<span title="userPassword for the account at Msegat.com" data-toggle="tooltip">Password</span>';
$_['entry_httpapi_msegat']   = 'HTTP API:';
$_['entry_senderid_msegat']	 = '<span title="sender name , should be activated from Msegat.com" data-toggle="tooltip">Sender Name</span>';
$_['httpapi_example_msegat'] = '<span class="help"><b>Use this for HTTP API-></b> msegat.com/gw/</span>';

// MSG91
$_['entry_userkey_msg91']   = '<span title="MSG91 Authentication Key" data-toggle="tooltip">Auth Key</span>';
$_['entry_route_msg91']   	= '<span title="Fill with 1 or 4" data-toggle="tooltip">Route</span>';
$_['entry_httpapi_msg91']   = 'HTTP API:';
$_['entry_senderid_msg91']	= '<span title="Your MSG91 Sender ID" data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_msg91'] = '<span class="help"><b>Use this for HTTP API-></b> control.msg91.com/api/sendhttp.php</span>';

// mVaayoo
$_['entry_userkey_mvaayoo']   = '<span title="Register or Login at mVaayoo.com click API Settings menu at top right." data-toggle="tooltip">Username</span>';
$_['entry_passkey_mvaayoo']   = '<span title="Register or Login at mVaayoo.com click API Settings menu at top right." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_mvaayoo']   = '<span title="Register or Login at mVaayoo.com On Compose page, click on the API Tab to see the URL & other details." data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_mvaayoo']	= 'Sender ID:';
$_['httpapi_example_mvaayoo']	= '<span class="help"><b>Use this for HTTP API-></b> http://api.mVaayoo.com/mvaayooapi/MessageCompose</span>';
$_['entry_term_mvaayoo']   		= 'Terms:';
$_['text_term_mvaayoo']   		= '<span class="help">Just <strong><i>Transactional route</i></strong> will work with this module.<br />Make sure you have registered Templates and approved by mVaayoo before you use message templates in this module.</span>';

// mysms
$_['entry_userkey_mysms']   = '<span title="Register or Login at www.mysms.com.gr" data-toggle="tooltip">Username</span>';
$_['entry_passkey_mysms']   = '<span title="Register or Login at www.mysms.com.gr" data-toggle="tooltip">Password</span>';
$_['entry_httpapi_mysms']   = '<span title="Register or Login at www.mysms.com.gr and see the documentation." data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_mysms']	= '<span title="11 Latin characters, without spaces, numbers or other symbols" data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_mysms']	= '<span class="help"><b>Use this for HTTP API-></b>www.mysms.com.gr/api.php</span>';

// Nexmo
$_['entry_userkey_nexmo']   	= '<span title="Register or Login at nexmo.com click API Settings menu at top right." data-toggle="tooltip">Key</span>';
$_['entry_passkey_nexmo']   	= '<span title="Register or Login at nexmo.com click API Settings menu at top right." data-toggle="tooltip">Secret</span>';
$_['entry_httpapi_nexmo']   	= '<span title="Register or Login at nexmo.com go to API menu &amp; see the documentation about <b>HTTP API</b> Base URL. (Use json response)" data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_nexmo']		= '<span title="Max. 11 characters, alphanumeric without spacing" data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_nexmo']		= '<span class="help"><b>Use this for HTTP API-></b> https://rest.nexmo.com/sms/json</span>';
$_['entry_unicode_nexmo']			= '<span title="Send all message text as unicode." data-toggle="tooltip">Unicode</span>';

// Netgsm
$_['entry_userkey_netgsm']   = '<span title="netgsm received from the user name" data-toggle="tooltip">Username</span>';
$_['entry_passkey_netgsm']   = '<span title="is netgsm from received user password" data-toggle="tooltip">Password</span>';
$_['entry_httpapi_netgsm']   = '<span title="netgsm api url" data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_netgsm']	 = '<span title="Max. 11 characters." data-toggle="tooltip">Sender Name</span>';
$_['httpapi_example_netgsm'] = '<span class="help"><b>Use this for HTTP API-></b>api.netgsm.com.tr/bulkhttppost.asp</span>';

// One Way SMS
$_['entry_userkey_oneway']   	= '<span title="Register or Login at onewaysms.com.my go to menu Account -> API at top right." data-toggle="tooltip">API Username</span>';
$_['entry_passkey_oneway']   	= '<span title="Register or Login at onewaysms.com.my go to menu Account -> API at top right." data-toggle="tooltip">API Password</span>';
$_['entry_httpapi_oneway']   	= '<span title="Register or Login at onewaysms.com.my go to menu Account -> API at top right." data-toggle="tooltip">MT URL</span>';
$_['entry_senderid_oneway']		= '<span title="Max. 11 characters, alphanumeric." data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_oneway']		= '<span class="help"><b>Example MT URL-></b> http://gateway.onewaysms.com.my:10001/api.aspx</span>';

// Openhouse IMI Mobile
$_['entry_userkey_openhouse'] = '<span title="It is the Service Provider\'s unique access key" data-toggle="tooltip">Key</span>';
$_['entry_passkey_openhouse'] = '<span title="It is the short code set to the application to receive SMS responses." data-toggle="tooltip">Sender Address</span>';
$_['entry_senderid_openhouse']		= '<span title="The name of the sender to appear on the terminal is the address to whom a responding SMS may be sent." data-toggle="tooltip">Sender Name</span>';

// Redsms India
$_['entry_userkey_redsms']   	= '<span title="Username" data-toggle="tooltip">Username</span>';
$_['entry_passkey_redsms']   	= '<span title="Password" data-toggle="tooltip">Password</span>';
$_['entry_httpapi_redsms']   	= '<span title="HTTP API" data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_redsms']		= '<span title="Red SMS Approved Sender ID<br />Letters only and min 3 and max 20" data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_redsms']	= '<span class="help"><b>Use this for HTTP API-></b> http://login.redsms.in/API/SendMessage.ashx</span>';

// Routesms
$_['entry_userkey_routesms']   	= '<span title="Your Routesms Username" data-toggle="tooltip">Username</span>';
$_['entry_passkey_routesms']   	= '<span title="Your Routesms Password" data-toggle="tooltip">Password</span>';
$_['entry_httpapi_routesms']   	= '<span title="Your Routesms API URL server" data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_routesms']		= '<span title="Max Length of 18 if Only Numeric<br />Max Length of 11 if Alpha Numeric" data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_routesms']	= '<span class="help"><strong>https://<font color="red">&#60;server&#62;</font>.routesms.com/bulksms/bulksms</strong><br /><i><strong>ex:</strong> https://smsplus2.routesms.com/bulksms/bulksms</i><br />If not sure, ask your server to the routesms support</span>';

// smsgatewayhub
$_['entry_userkey_smsgatewayhub']   = '<span title="Your user name." data-toggle="tooltip">User</span>';
$_['entry_passkey_smsgatewayhub']   = '<span title="Your Password." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_smsgatewayhub']   = '<span title="Transactional SMS API Url." data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_smsgatewayhub']	= 'Sender ID:';
$_['httpapi_example_smsgatewayhub']	= '<span class="help"><b>Use this for HTTP API-></b> api.smsgatewayhub.com/smsapi/pushsms.aspx</span>';
$_['entry_term_smsgatewayhub']   		= 'Terms:';
$_['text_term_smsgatewayhub']   		= '<span class="help">Just <strong><i>Transactional Route</i></strong> will work with this module.<br />Make sure you have registered Templates and approved transactional template SMS before you use message templates in this module.</span>';

// SMSBox.com
$_['entry_userkey_smsboxcom']   		= '<span title="smsbox.com Account Username." data-toggle="tooltip">Username</span>';
$_['entry_passkey_smsboxcom']   		= '<span title="smsbox.com Account Password." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_smsboxcom']   		= '<span title="smsbox.com HTTP Api" data-toggle="tooltip">HTTP API</span>';
$_['entry_apiid_smsboxcom']					= '<span title="smsbox.com Customer ID." data-toggle="tooltip">Customer ID</span>';
$_['entry_senderid_smsboxcom']			= '<span title="Your Sender Text which has been added in smsbox.com and approved." data-toggle="tooltip">Sender Text</span>';
$_['httpapi_example_smsboxcom']			= '<span class="help"><b>Use this for HTTP API-></b> www.smsbox.com/smsgateway/Services/Messaging.asmx/Http_SendSMS</span>';

// smslane
$_['entry_userkey_smslane']   	= '<span title="Username" data-toggle="tooltip">Username</span>';
$_['entry_passkey_smslane']   	= '<span title="Password" data-toggle="tooltip">Password</span>';
$_['entry_httpapi_smslane']   	= '<span title="HTTP API" data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_smslane']		= 'Sender ID:';
$_['httpapi_example_smslane']		= '<span class="help"><b>Use this for HTTP API-></b> http://smslane.com/vendorsms/pushsms.aspx</span>';
$_['entry_unicode_smslane']			= '<span title="Send all message text as unicode." data-toggle="tooltip">Unicode</span>';
$_['entry_transactional_smslane']			= '<span title="Choose your SMS Route. For Transactional Route, make sure you pass valid and approved template in message text." data-toggle="tooltip">Route</span>';
$_['text_promotional_smslane'] 	= 'Promotional';
$_['text_transactional_smslane']= 'Transactional';

// smslane global
$_['entry_userkey_smslaneg']   	= '<span title="Client username for GlobalSMS system login." data-toggle="tooltip">Username</span>';
$_['entry_passkey_smslaneg']   	= '<span title="Client password for GlobalSMS system login." data-toggle="tooltip">Password</span>';
$_['entry_httpapi_smslaneg']   	= '<span title="GlobalSMS HTTP API url" data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_smslaneg']		= '<span title="Dynamic message sender ID Max. 11 characters." data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_smslaneg']	= '<span class="help"><b>Use this for HTTP API-></b> http://world.smslane.com/vendorsms/GlobalPush.aspx</span>';
$_['entry_unicode_smslaneg']		= '<span title="Send all message text as unicode." data-toggle="tooltip">Unicode</span>';

// smsnetgr
$_['entry_userkey_smsnetgr']   		= '<span title="Your portal login." data-toggle="tooltip">Username</span>';
$_['entry_passkey_smsnetgr']   		= '<span title="Not same as portal password. Find API Password in Setting menu." data-toggle="tooltip">API Password</span>';
$_['entry_httpapi_smsnetgr']   		= '<span title="HTTP API <b>api.clickatell.com/http/sendmsg</b>" data-toggle="tooltip">HTTP API</span>';
$_['entry_apiid_smsnetgr']			= '<span title="Secure token. Find API Token in Setting menu." data-toggle="tooltip">API Token</span>';
$_['entry_senderid_smsnetgr']			= '<span title="Sender ID, can be empty, then default value will be used (sms.net.gr)" data-toggle="tooltip">Sender ID</span>';
$_['httpapi_example_smsnetgr']		= '<span class="help"><b>Use this for HTTP API-></b> https://sms.net.gr/index.php/api/do</span>';
$_['entry_unicode_smsnetgr']			= '<span title="Send all message text as unicode." data-toggle="tooltip">Unicode</span>';

// topsms
$_['entry_userkey_topsms']   	= '<span title="Username" data-toggle="tooltip">Username</span>';
$_['entry_passkey_topsms']   	= '<span title="Password" data-toggle="tooltip">Password</span>';
$_['entry_httpapi_topsms']   	= '<span title="HTTP API URL" data-toggle="tooltip">HTTP API</span>';
$_['entry_senderid_topsms']		= '<span title="Sender Name." data-toggle="tooltip">Sender</span>';
$_['httpapi_example_topsms']	= '<span class="help"><b>Use this for HTTP API-></b> topsms.mobi/sendsms.php</span>';
$_['entry_lang_topsms']				= '<span title="Choice Lang (English or Arabic)." data-toggle="tooltip">Language</span>';
$_['text_en']      						= 'English';
$_['text_ar']      						= 'Arabic';

// Velti India
$_['entry_userkey_velti']   	= '<span title="AID" data-toggle="tooltip">AID</span>';
$_['entry_passkey_velti']   	= '<span title="PIN" data-toggle="tooltip">PIN</span>';
$_['entry_httpapi_velti']   	= '<span title="HTTP API" data-toggle="tooltip">HTTP API</span>';
$_['httpapi_example_velti']		= '<span class="help"><b>Use this for HTTP API-></b> https://luna.a2wi.co.in/failsafe/HttpLink</span>';

// Entry
$_['entry_layout']        		= 'Layout:';
$_['entry_position']      		= 'Position:';
$_['entry_status']        		= 'Status:';
$_['entry_sort_order']    		= 'Sort Order:';
$_['entry_nohp']    					= 'Mobile Number:';
$_['entry_message']    				= 'Message:';
$_['entry_smslimit']    			= '<span title="You can limit the message who can sending of visitor for a day. Leave blank to disable limit." data-toggle="tooltip">SMS Limit/Day</span>';
$_['entry_alert_reg']   			= '<span title="You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{telephone}</i> and <i>{storename}</i> in the message. <b>eg</b>: <i>Dear {firstname} {lastname}, Thanks for sign up on {storename}.</i>" data-toggle="tooltip">Register Message Alert</span>';
$_['entry_alert_order']   		= '<span title="You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{orderid}</i>, <i>{orderstatus}</i>, <i>{shippingmethod}</i>, <i>{paymentmethod}</i>, <i>{total}</i>, <i>{storename}</i> in the message." data-toggle="tooltip">Place Order Message Alert</span>';
$_['entry_alert_changestate'] = '<span title="You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{orderid}</i>, <i>{orderstatus}</i>, <i>{shippingmethod}</i>, <i>{invoiceno}</i>, <i>{total}</i>, <i>{comment}</i>, <i>{storename}</i> in the message." data-toggle="tooltip">Order Alert</span>';
$_['entry_alert_returnstate'] = '<span title="You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{orderid}</i>, <i>{order_date}</i>, <i>{returnid}</i>, <i>{return_status}</i>, <i>{return_action}</i>, <i>{return_reason}</i>, <i>{email}</i>, <i>{product}</i>, <i>{model}</i>, <i>{quantity}</i>, <i>{comment}</i>, <i>{storename}</i> in the message." data-toggle="tooltip">Order Alert</span>';
$_['entry_additional_alert']	= '<span title="Any additional admin mobile number (with Country Code) you want to receive the register and order alert sms. (comma separated). e.g: 1803015xxx,4475258xxx,6285220xxx" data-toggle="tooltip">Additional Alert SMS</span>';
$_['entry_alert_sms']    			= '<span title="Send a SMS to the store owner when a new order is created. You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{orderid}</i>, <i>{orderstatus}</i> and etc (<strong>Variable accepted</strong>) in the message." data-toggle="tooltip">New Order Alert SMS</span>';
$_['entry_account_sms']  			= '<span title="Send a SMS to the store owner when a new account is registered. You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{telephone}</i> and <i>{storename}</i> in the message." data-toggle="tooltip">New Account Alert SMS</span>';
$_['entry_return_sms']  			= '<span title="Send a SMS to the store owner when a new request product return. You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{orderid}</i>, <i>{order_date}</i>, <i>{returnid}</i>, <i>{return_status}</i>, <i>{return_action}</i>, <i>{return_reason}</i>, <i>{email}</i>, <i>{product}</i>, <i>{model}</i>, <i>{quantity}</i>, <i>{comment}</i>, <i>{storename}</i> in the message." data-toggle="tooltip">Product Return Request Alert SMS</span>';
$_['entry_alert_blank']    		= '<span class="help">* Leave blank to disable this alert</span>';
$_['entry_status_order_alert']= '<span title="Select order status" data-toggle="tooltip">Change Status Order Alert</span>';
$_['entry_status_return_alert']= '<span title="Select return status" data-toggle="tooltip">Change Status Return Alert</span>';
$_['entry_verify_code']				= '<span title="Add <i>{code}</i> for verification code variable in message.<br />e.g: Your verification code is: {code}" data-toggle="tooltip">Verification code message</span>';
$_['entry_skip_group']				= '<span title="When Order Verification is active, Customer who have login and is in group selected will not required mobile phone verification." data-toggle="tooltip">Skip verification for Groups</span>';
$_['entry_skip_group_help']   = '<span class="help">Hold Ctrl key for multiple select.</span>';
$_['entry_code_digit']   			= 'Length of Verification Code digit:';
$_['entry_max_retry']   			= '<span title="The maximum customers can resend verification code." data-toggle="tooltip">Maximum retry</span>';
$_['entry_limit_blank']    		= '<span class="help">Leave blank to disable limit.</span>';
$_['entry_parsing']    				= '<span class="help"><strong>Variable accepted:</strong> {order_date},{products_ordered},{firstname},{lastname},{email},{telephone},{orderid},{orderstatus},{shippingmethod},{shipping_firstname},{shipping_lastname},{shipping_company},{shipping_address},{shipping_city},{shipping_postcode},{shipping_state},{shipping_country},{paymentmethod},{payment_firstname},{payment_lastname},{payment_company},{payment_address},{payment_city},{payment_postcode},{payment_state},{payment_country},{total},{comment},{storename}</span>';
$_['entry_parsing_return']    				= '<span class="help"><strong>Variable accepted:</strong> {firstname}, {lastname}, {orderid}, {order_date}, {returnid}, {return_status},{return_action}, {return_reason}, {email}, {product}, {model}, {quantity}, {comment}, {storename}</span>';
$_['entry_skip_payment_method']				= '<span title="Select Payment Method that do not require verification." data-toggle="tooltip">Skip verification for Payment Method</span>';
$_['entry_skip_payment_method_help']  = '<span class="help">Hold Ctrl key for multiple select.</span>';

// Button
$_['button_save']							= 'Save';
$_['button_cancel']						= 'Cancel';
$_['button_add_module']				= 'Add Module';
$_['button_remove']						= 'Remove';
$_['button_sendsms']					= 'Send SMS';

// Error
$_['error_permission']    		= 'Warning: You do not have permission to modify module jOS SMS!';
$_['error_gateway']       		= 'Gateway required';
$_['error_userkey']       		= 'This field required';
$_['error_passkey']       		= 'This field required';
$_['error_httpapi']       		= 'This field required';
$_['error_senderid']       		= 'This field required';
$_['error_apiid']       			= 'This field required';
$_['error_nohp']       				= 'Phone number required';
$_['error_message']       		= 'Text message required';
$_['error_gateway_null']   		= 'Gateway not found! Please set the default gateway';

// Anounce
$_['text_instruction']				= '<span class="help">You can add <b>jOS SMS</b> module to enable your web visitor can Sending Message for free from your web. This can increase visitor traffic of your web.<br />Click <b>Add Module</b> button and set the configuration of module.<br/><br/><b>Layout</b>: On the page where the module will be displayed.<br/><b>Position</b>: Position of module.<br/><b>Status</b>: You can enable or disable the module.<br/><b>Sort Order</b>: Order position of the module.</span>';
?>