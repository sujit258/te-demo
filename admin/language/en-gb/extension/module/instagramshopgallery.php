<?php
$_['heading_title']              = 'InstagramShopGallery';

$_['text_main_setting']          = 'Main Settings';
$_['text_module']                = 'Module';
$_['text_page']                  = 'Page';
$_['text_support']               = 'Support';

$_['text_modules']               = 'Modules';
$_['text_enabled']               = 'Enabled';
$_['text_disabled']              = 'Disabled';
$_['button_cancel']              = 'Cancel';
$_['text_default']               = 'Default';
$_['text_close']                 = 'Close';
$_['text_save']                  = 'Save';
$_['text_no']                    = 'No';
$_['text_yes']                   = 'Yes';
$_['text_likes']                 = 'Likes';
$_['text_comments']              = 'Comments';

$_['entry_title']                = 'Title';
$_['entry_status']               = 'Status';
$_['entry_global_status']        = 'Global Status';
$_['entry_hashtag']              = 'Instagram Hashtag';
$_['entry_visibility']           = 'Photo Visibility';
$_['entry_photo_limit']          = 'Photo Limit';
$_['entry_extra_image']          = 'Extra Image';
$_['entry_extra_link']           = 'Extra Image Link';
$_['entry_banner_image']         = 'Banner Image';
$_['entry_banner_link']          = 'Banner Image Link';
$_['entry_navbar']               = 'Main Navigation';
$_['entry_custom_css']           = 'Custom CSS';
$_['entry_seo_options']          = 'SEO Options';
$_['entry_meta_title']           = 'Meta Title';
$_['entry_meta_desc']            = 'Meta Description';
$_['entry_meta_keywords']        = 'Meta Keywords';
$_['entry_url_alias']            = 'URL Alias';
$_['entry_ig_user_id']           = 'IG User Id';
$_['entry_username']             = 'Username';
$_['entry_post_time']            = 'Post Time';
$_['entry_approve']              = 'Approve';
$_['entry_products']             = 'Products';

$_['entry_extra_image_help']     = 'Extra image added to the end of photo list. For best result, size at least 480px x 480px';
$_['entry_banner_image_help']    = 'Banner image placed at top of the page. For best result, width size at least 1920px';
$_['entry_extra_link_help']      = 'Full URL address, including \'http(s)://\' protocol.';
$_['entry_navbar_help']          = 'Display in the top main navigation bar.';

$_['text_moderation']            = 'Moderation';
$_['text_saved_photos']          = 'Saved Photos';
$_['text_information']           = 'Information';
$_['text_fetch']                 = 'Fetch';
$_['text_fetch_info']            = 'Click <code>Fetch</code> to view Instagram photos.';
$_['text_load_more']             = 'Load more';
$_['text_processing']            = 'Processing..';
$_['text_loading']               = 'Loading..';
$_['text_show_all_photos']       = 'Show all photos';
$_['text_only_approved']         = 'Only approved';
$_['text_have_related']          = 'Have related product';
$_['text_approve_have_related']  = 'Approved and related product';

$_['text_aliasing']              = 'Aliasing';
$_['text_photo_manage']          = 'Click photo to manage approval and related product';
$_['text_approval_status']       = 'Approval status';
$_['text_related_product']       = 'Total related product';
$_['text_remove_db']             = 'Remove from database';

$_['text_info_setting']          = array('Instagram hashtag globally used for Module and Page.');
$_['text_info_setting_moderation'] = array(
    'Click image to edit photo approval and related products.',
    'Change fetching source from the <code>dropdown</code>',
    '<code>Instagram Fetch</code> gathers recently published photos directly from Instagram feed based on hashtag.',
    '<code>Load more</code> button will appear if next photos feed is available to fetch.',
    '<code>Saved Photos Fetch</code> will load saved photos information in database.',
);
$_['text_info_module']          = array(
    'This is a helper section containing the module settings. Please make sure you add the module into <code>Design > Layouts</code> so it displays on the page you want to.',
    'Visibility setting:
        <ul class="isl-list">
          <li><code>Show all photos</code> fetch recent photos from Instagram.</li>
          <li><code>Only approved</code> displays photos that have been approved. Regardless of whether they have related product or not.</li>
          <li><code>Have related product</code> shows only posts that have related products. Does not matter whether the post is approved or not.</li>
          <li><code>Approved and related product</code> shows only posts which are approved and have related products added to them.</li>
        </ul>',
    'If Instagram feed not responding, photo information will be automatically gathered from the saved photos.',
);
$_['text_info_page']          = array(
    'This is a helper section containing the custom page settings.',
    'Visibility setting:
        <ul class="isl-list">
          <li><code>Show all photos</code> fetch recent photos from Instagram.</li>
          <li><code>Only approved</code> displays photos that have been approved. Regardless of whether they have related product or not.</li>
          <li><code>Have related product</code> shows only posts that have related products. Does not matter whether the post is approved or not.</li>
          <li><code>Approved and related product</code> shows only posts which are approved and have related products added to them.</li>
        </ul>',
    'If Instagram feed not responding, photo information will be automatically gathered from the saved photos.',
);

// Tab Support
$_['text_your_license']          = 'Your license';
$_['text_please_enter_the_code'] = "Please enter your product purchase license code";
$_['text_activate_license']      = "Activate License";
$_['text_not_having_a_license']  = "Don't have a code? Get it from here.";
$_['text_license_holder']        = "License Holder";
$_['text_registered_domains']    = "Registered domains";
$_['text_expires_on']            = "License Expires on";
$_['text_valid_license']         = "VALID LICENSE";
$_['text_get_support']           = 'Get Support';
$_['text_community']             = "Community";
$_['text_ask_our_community']     = "Ask the community about your issue on the iSenseLabs forum.";
$_['text_tickets']               = 'Tickets';
$_['text_open_a_ticket']         = 'Want to communicate one-to-one with our tech people? Then open a support ticket.';
$_['text_pre_sale']              = 'Pre-sale';
$_['text_pre_sale_desc']         = 'Have a brilliant idea for your webstore? Our team of top-notch developers can make it real.';
$_['text_browse_forums']         = 'Browse forums';
$_['text_open_ticket_for_real']  = 'Open a ticket';
$_['text_bump_the_sales']        = 'Bump the sales';

// // Notification
$_['text_success']               = 'Success: You have modified module InstagramShopGallery!';
$_['text_success_save']          = 'Successfully saved!';

$_['error_permission']           = 'Warning: You do not have permission to modify module InstagramShopGallery!';
$_['error_general']              = 'Error occured, please try again later!';
$_['error_hashtag_required']     = 'Instagram hashtag required!';
$_['error_ig_not_respond']       = 'Instagram server is not responding, please try again later!';
$_['error_empty_database']       = 'No photos found in database!';
