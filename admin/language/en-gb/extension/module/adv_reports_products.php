<?php
// Heading
$_['heading_title']							= '<img src="view/image/adv_reports/adv_icon_small.png"> <span style="color:#4170bd; font-weight:bold">ADV Products Report</span>';
$_['heading_title_main']					= 'ADV Products Report';

// Text
$_['text_edit']        						= 'Edit ADV Products Report';
$_['text_extension']   						= 'Extensions';
$_['text_asking_help']						= 'Read before requesting for support';
$_['text_help_request']						= 'Requesting for support';
$_['text_terms']							= 'Terms & Conditions';
$_['text_success']							= 'Success: You have modified module ADV Products Report!';

//Tab
$_['tab_about']            					= 'About';

// Button
$_['button_documentation']            		= 'Documentation';

// Error
$_['error_permission']    					= 'Warning: You do not have permission to modify module ADV Products Report!';