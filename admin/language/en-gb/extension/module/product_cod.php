<?php
// Heading
$_['heading_title_1']       = 'COD Based On Product & Categories';
$_['heading_title']       = '<b><font color = "#0CABFB">COD Based On Product & Categories</font></b>';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module COD Based on Product!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_product']       = '<font color = "#F80642">COD Disable Products:</font><br /><span class="help">(Autocomplete)</span>';
$_['entry_category']       = '<font color = "#F80642">COD Disable Category:</font><br /><span class="help">(Autocomplete)</span>';

// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module COD Based on Product!';
?>