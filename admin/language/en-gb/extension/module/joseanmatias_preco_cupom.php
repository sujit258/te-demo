<?php
// Heading
$_['heading_title']    = 'Price with Coupon';
$_['heading_title_inner']    = 'Price with Coupon';

// Text
$_['text_module']     = 'Modules';
$_['text_success']     = 'Price with Coupon updated successfully!';
$_['parcelamento_text_list_default']     = 'Com cupom <br><strong class="price-new">[PRECO]</strong> <br><div class="coupon-code">Cupom: [CUPOM]</div>';
$_['parcelamento_text_page_default']     = 'Price with coupon: <strong class = "price-new"> [PRICE] </ strong> <br> <div class = "coupon-';
$_['text_edit']        = 'Edit Price with Coupon';

// Entry
$_['entry_text_page']     = '<span data-toggle = "tooltip" title = "Constants [CUPOM] and [PRECO] are essential for perfect operation."> Text on product page </ span>';
$_['entry_text_list']     = '<span data-toggle = "tooltip" title = "Constants [CUPOM] and [PRECO] are essential for perfect operation."> Text in other areas of the store </ span>';
$_['entry_showlist']     = '<span data-toggle = "tooltip" title = "Display value and coupon in store lists."> Display in listings </ span>';
$_['entry_outofstock']     = '<span data-toggle = "tooltip" title = "Display value and coupon on non-stock products."> Display on products without stock </ span>';
$_['entry_status']     = 'Situation:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to edit the Price with Coupon module!';
?>