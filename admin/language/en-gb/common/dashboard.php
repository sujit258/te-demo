<?php
// Heading
$_['heading_title'] = 'TE-Dashboard';
$_['heading_mtd']   = 'MTD Sales';
$_['heading_products_mtd']   = 'MTD Products Count';

// Text
$_['text_view']        = 'View more...';

// Error
$_['error_install'] = 'Warning: Install folder still exists and should be deleted for security reasons!';