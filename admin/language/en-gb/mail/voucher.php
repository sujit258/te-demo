<?php
// Text
$_['text_subject']  = 'Hurray! %s sent you a Gift Voucher';
$_['text_greeting'] = 'Congratulations...! You have received a %s Gift Voucher worth %s/-';
$_['text_from']     = 'This Gift Voucher has been sent to you by %s';
$_['text_message']  = 'With a message saying';

$_['text_redeem']   = 'Redeem your Gift by using the code (<b>%s</b>)';

$_['text_redeem_1']   = 'While we always wish to see you and your familys #True smiles flashing on your faces, a small yet heartfelt gesture from True Elements is enough to make up for all the false truths and lies around you.';
$_['text_redeem_2']   = 'Just like your smile, pamper yourself with Food that DOES NOT lie to you, because we love seeing the #True YOU :)';

$_['text_footer']   = ' ';


$_['text_subject1']  = 'Hurray! %s sent you a Gift Voucher';
$_['text_greeting1'] = 'Congratulations...! You have received a Gift Voucher worth %s/-';
$_['text_from1']     = 'This Gift Voucher has been sent to you by %s';
$_['text_message1']  = 'With a message saying';

$_['text_redeem1']   = 'Click <a href="https://www.true-elements.com/index.php?route=account/account"> Here </a> To know more';
