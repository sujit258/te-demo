<?php
// Text
$_['text_approve_subject']      = '%s - Your Account has been activated!';
$_['text_approve_welcome']      = 'Welcome and thank you for registering at %s!';
$_['text_approve_login']        = 'Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_approve_services']     = 'Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.';
$_['text_approve_thanks']       = 'Thanks,';
$_['text_transaction_subject']  = '%s - Account Credit';
$_['text_transaction_received'] = 'You have received %s credit!';
$_['text_transaction_total']    = 'Your total amount of credit is now %s.' . "\n\n" . 'Your account credit will be automatically deducted from your next purchase.';
$_['text_reward_subject']       = 'Congrats! You have Earned True Elements True Cash';
$_['text_reward_received']      = 'Greetings from True Elements!.' . "\n\n" . 'When you are obsessed with tasty and natural health food, True Elements is the right place for you.' . "\n\n" . 'Congratulations on your purchase. You have just earned %s True Cash.';
$_['text_reward_total']         = 'Your current balance is %s. You can redeem the True Cash on your next purchase.' . "\n\n" . 'Till then, Happy shopping and Thank you for visiting True Elements. We appreciate your time.';

