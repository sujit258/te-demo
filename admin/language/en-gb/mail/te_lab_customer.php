<?php
// Text
$_['text_approve_subject']      = '%s - Your Account has been approved for TE Lab Program!';

$_['text_hello_te_lab']        = 'Hello %s';
$_['text_thanks_te_lab']        = 'Thank you for applying for TE Lab Program at True Elements';
$_['text_approved_te_lab']        = 'We have approved your application for trying out the sample of our upcoming products';

$_['text_link_te_lab']        = 'To access the new launch products for tasting before you are ready to purchase, click here.'; 
$_['text_approve_thanks']       = 'Thanks,'; 