<?php
$_['heading_title'] = 'Delhivery Lastmile | Manage AWB';
$_['text_delhivery_lastmile'] = 'Delhivery Lastmile';
$_['text_module']         = 'Modules';
$_['text_edit']           = 'Module settings';
$_['text_success'] = 'Details saved successfully.';





$_['text_yes']        = 'Yes';
$_['text_no']       = 'No';

$_['text_enabled']        = 'Enabled';
$_['text_disabled']       = 'Disabled';
$_['button_save']         = 'Save';
$_['button_cancel']       = 'Cancel';
$_['curl_disable']        = 'Attention! On the hosting is not enabled CURL support. It is necessary for the operation of the module.';


// Text
$_['text_new_subject']          = '%s - Order %s';
$_['text_new_greeting']         = "Thanks a lot for letting True Elements be a part of your story. It means a lot to us and we are truly delighted to serve you. We will try our best to dispatch the order within the next 48 working hours, if not earlier.";
$_['text_new_received']         = 'You have received an order.';
$_['text_new_link']             = 'To view your order click on the link below:';
$_['text_new_order_detail']     = 'Order Details';
$_['text_new_instruction']      = 'Instructions';
$_['text_new_order_id']         = 'Order ID:';
$_['text_new_date_added']       = 'Date Added:';
$_['text_new_order_status']     = 'Order Status:';
$_['text_new_payment_method']   = 'Payment Method:';
$_['text_new_shipping_method']  = 'Shipping Method:';
$_['text_new_email']            = 'E-mail:';
$_['text_new_telephone']        = 'Telephone:';
$_['text_new_ip']               = 'IP Address:';
$_['text_new_payment_address']  = 'Payment Address';
$_['text_new_shipping_address'] = 'Shipping Address';
$_['text_new_products']         = 'Products';
$_['text_new_product']          = 'Product';
$_['text_new_model']            = 'Model';
$_['text_new_quantity']         = 'Quantity';
$_['text_new_price']            = 'Price';
$_['text_new_order_total']      = 'Order Totals';
$_['text_new_total']            = 'Total';
$_['text_new_download']         = 'Once your payment has been confirmed you can click on the link below to access your downloadable products:';
$_['text_new_comment']          = 'The comments for your order are:';
$_['text_new_footer']           = '<p style="color:#000000;">Committed to enhancing your quality of life, True Elements aims to bring positivity, hope & confidence through food that is packed with the goodness of nature & taste that will keep you wanting for more.<br />
<i>True to Nature. True to You</i><br />
<img src="http://www.true-elements.com/image/catalog/brands/Main-logo1.png" alt="<?php echo $store_name; ?>" width="100" style="margin-bottom: 20px; border: none;" /><br />
Note: This invoice is just for information purpose, we will share the original copy of your order invoice along with order delivery</p>
<br />
Please reply to this email if you have any questions.<br />  ';
$_['text_update_subject']       = '%s - Order Update %s';
$_['text_update_order']         = 'Order ID:';
$_['text_update_date_added']    = 'Date Ordered:';
$_['text_update_order_status']  = 'Your order has been updated to the following status:';
$_['text_update_comment']       = 'The comments for your order are:';
$_['text_update_link']          = 'To view your order click on the link below:';
$_['text_update_footer']        = 'Please reply to this email if you have any questions.<br />  <b>True Elements</b><br /> True to Nature. True to You';
