<?php
// Heading
$_['heading_title']     			= 'Blog Settings';

// Text
$_['text_success']      			= 'Success: You Have Updated Blog Settings!';
$_['text_configuration']      		= 'Configuration: You Need To Hit Save Button Once To Configure Blog Into Your System. This Is One Time Only. All Required Filled With Default Values. You Can Change Them As Per Your Requirement.';

$_['text_edit']         			= 'Edit Blog Settings';

$_['text_store']         			= 'Store';
$_['text_column']         			= ' - Column';
$_['text_top']         				= 'Top';
$_['text_bottom']         			= 'Bottom';
$_['text_grid']         			= 'Grid';
$_['text_list']         			= 'List';
$_['text_mpblog']	 		 		= 'Blog';
$_['text_mpblog_category']			= 'Blog Category';
$_['text_mpblog_post']	 	 		= 'Blog Post';
$_['text_mpblog_comment']	 		= 'Blog Comment';
$_['text_mpblog_review']	 		= 'Blog Rating';

// how column works
$_['text_column_title']         	= 'How Blogs Column Per Row Works?';
$_['text_column_defination']        = 'Select How Many Blogs To Be Show Per Row. For Example 1 Blog Per Row. 3 Blog Per Row. 6 Blog Per Row. Still Not Clear?. Let\'s Make Different Combiniations: <br> 1 Blog Per Row <br> 3 Blog Per Row <br> 2 Blog Per Row <br> 6 Blog Per Row.';

$_['text_img_size_title']         	= 'Blog Image Sizes';
$_['text_img_size_defination']      = 'Give Blog Image Size Dimensions Width X Height . E.g: 200 X 120';
$_['text_cimg_size_title']         	= 'Category Image Sizes';
$_['text_cimg_size_defination']     = 'Give Category Blog Image Size Dimensions Width X Height . E.g: 200 X 120';

$_['text_socialmedia_title']        = 'Social Media Links';
$_['text_socialmedia_defination']   = 'Add Social Media Links To Be Show In Each Blog. Select If You Want To Show Social Media Information On Blog Top Or Bottom. Add Social Media As Per Your Choice. You Can Also Share Particular Blog Using Sharethis Plugin.';

$_['text_comment_title']        	= 'Blog Comments System';
$_['text_comment_defination']   	= 'Add Comments System Into Blog. Select Type Of Commenting System For Your Blog. You Have Multiple Choice Of Commenting System To Fit Best Into Your Needs.';


$_['text_category_home']       		= 'Blog Home Menu';
$_['text_category_post_count'] 		= 'Show Category Post Count';
$_['text_category_show_image'] 		= 'Show Category Main Image';
$_['text_category_show_description']= 'Show Category Description';
$_['text_category_page_limit'] 		= 'Category Page Limit';
$_['text_category_design']         	= 'Blog Per Row';

$_['text_blog_show_image']         	= 'Show Blog Main Image';
$_['text_blog_show_image_popup']    = 'Show Blog Main Image In Popup';
$_['text_blog_show_description']    = 'Show Description';
$_['text_blog_show_sdescription']   = 'Show Short Description';
$_['text_blog_sdescription_length'] = 'Short Description Length';
$_['text_blog_page_limit']          = 'Page Limit';
$_['text_blog_show_author']         = 'Show Author';
$_['text_blog_show_date']         	= 'Show Publish Date';
$_['text_blog_date_format']         = 'Date Format';
$_['text_blog_show_comment']        = 'Show Comments';
$_['text_blog_use_comment']        	= 'Allow Comments On Blog';
$_['text_blog_allow_comment']       = 'Allow Comments';
$_['text_blog_approve_comment']     = 'Approve Comments';
$_['text_blog_captcha_comment']     = 'Captcha On Comment';
$_['text_blog_show_rating']         = 'Show Rating';
$_['text_blog_allow_rating']        = 'Allow Rating';
$_['text_blog_approve_rating']      = 'Auto Approve Rating';
$_['text_blog_guest_rating']      	= 'Allow Guest Rating';
$_['text_blog_show_readmore']       = 'Show Readmore Link';
$_['text_blog_show_viewcount']      = 'Show Total Blog View';
$_['text_blog_show_sharethis']      = 'Show Social Share Buttons';
$_['text_blog_show_nextprev']      	= 'Show Next Previous Blog';
$_['text_blog_show_nextprev_title'] = 'Show Next Previous Blog Title';
$_['text_blog_view']      			= 'Blog Listing View';
$_['text_category_view']      		= 'Blogs View On Category Page';
$_['text_blog_show_tags']      		= 'Show Blog Tags';
$_['text_blog_show_viewsocial']     = 'Show Social Media Icons';
$_['text_blog_show_sociallocation'] = 'Social Media Position';
$_['text_blog_show_viewwishlist']   = 'Show Blog Favorite (Bookmark)';
$_['text_blog_design']         		= 'Blog Column Per Row';

$_['text_comment_default']         	= 'Use Default Comments';
$_['text_comment_default_guest']    = 'Allow Guest Comments';

$_['text_comment_facebook']       	= 'Use Facebook Comments';
$_['text_facebook_appid']       	= 'Facebook APP ID';
$_['text_facebook_nocomment']      	= 'Show No. Of Comments';
$_['text_facebook_color']       	= 'Color Scheme';
$_['text_facebook_colorlight']     	= 'Light';
$_['text_facebook_colordark']      	= 'Dark';
$_['text_facebook_order']       	= 'Comments Order';
$_['text_facebook_ordersocial']    	= 'Social';
$_['text_facebook_orderreverse_time']= 'Reverse Time';
$_['text_facebook_ordertime']      	= 'Time';
$_['text_facebook_width']       	= 'Width';

$_['text_comment_google']         	= 'Use Google Comments';

$_['text_comment_disqus']         	= 'Use Disqus Comments';
$_['text_comment_disqus_code']      = 'Disqus Universal Code';
$_['text_comment_disqus_count']     = 'Disqus Display Comment Count';


$_['text_rssfeed_title']     		= 'Feed Title';
$_['text_rssfeed_description']     	= 'Feed Description';
$_['text_rssfeed_format']     		= 'Default Feed Format';

$_['text_rssfeed_limit']     		= 'Feed Per Page';
$_['text_rssfeed_web_master']     	= 'Feed Web Master';
$_['text_rssfeed_copy_write']     	= 'Feed Copy Write';

// Legend

$_['legend_blogpage']     			= 'Blog View Page';
$_['legend_bloglist']     			= 'Blog Listing Page';
$_['legend_design']         		= 'Blog Per Row';
$_['legend_image_sizes']        	= 'Image Sizes';
$_['legend_comment']        		= 'Comments';

// Info

$_['info_title_rssfeed']     		= 'RSS Feed';
$_['info_text_rssfeed']     		= 'RSS (Really Simple Syndication) To Syndicate Or Subscribe To The Feed Of A Website, Blog.  Many News-related Sites, Weblogs And Other Online Publishers Syndicate Their Content As An Rss Feed To Whoever Wants It. Configure The Rss Feed Setting.';

// Entry
$_['entry_support']      			= 'Support';
$_['entry_status']       			= 'Status';
$_['entry_social_icon']       		= 'Icon Class';
$_['entry_social_href']       		= 'Social Media Link';
$_['entry_social_name']       		= 'Name';
$_['entry_sort_order']       		= 'Sort Order';
$_['placeholder_social_icon']      	= 'fa fa-facebook-official';
$_['placeholder_social_href']       = 'https://www.facebook.com/';
$_['placeholder_social_name']       = 'Facebook';
$_['placeholder_sort_order']       	= '5';

$_['entry_width']        			= 'Width';
$_['entry_height']       			= 'Height';

$_['entry_image_category']       	= 'Blog Category Image Size';
$_['entry_image_category_thumb']    = 'Blog Sub-Categories Image Size';

$_['entry_image_post']       	 	= 'Blog Main Image Size';
$_['entry_image_post_thumb']        = 'Blog Each Listing Image Size';
$_['entry_image_post_popup']        = 'Blog Image Popup Size';
$_['entry_image_post_additional']   = 'Blog Additional Images Size';
$_['entry_image_post_related']      = 'Blog Related Image Size';


// Button
$_['button_support']      			= 'Email To Support';
$_['button_design_add']      		= 'Add Row';
$_['button_social_add']      		= 'Add Social Media';
$_['button_design_remove']      	= 'Remove Row';


// Tab
$_['tab_setting']      	  			= 'Settings';
$_['tab_mpblogcategory']  			= 'Blog Categories';
$_['tab_mpblogpost']      			= 'Blog Page';
$_['tab_mpblogmodule']      		= 'Social Media';
$_['tab_mpblogcomments']      		= 'Comments';

$_['tab_mpblogcomments_default']  	= 'Default Comments';
$_['tab_mpblogcomments_facebook']  	= 'Facebook Comments';
$_['tab_mpblogcomments_google']  	= 'Google+ Comments';
$_['tab_mpblogcomments_disqus']  	= 'Disqus Comments';

$_['tab_mpblogrss']  				= 'RSS Feed';
$_['tab_mpblogdoc']  				= 'Documentation';
$_['tab_mpsupport']  				= 'Support';
$_['tab_mpblogs_view']  			= 'Blog Full View Page';
$_['tab_mpblogs_listing']  			= 'Blogs Listing Page';

// Help
$_['help_category_home']  			= 'Set Home Category To Show In Blog Home Page. This Is Entry Point Into Blogs Using Blog Category.';
$_['help_blog_captcha_comment']  	= 'Display Captcha On Blog Comment Section. You Need To Enable Captcha From Extension and Settings From Backend. If Captcha Settings Are Not Enabled Then Captcha Will Not Work.';

// Error
$_['error_permission']  			= 'Warning: You Do Not Have Permission To Modify Blog Rating!';