<?php
// Heading
$_['heading_title']         = 'Customer True Cash Report';

// Text
$_['text_list']             = 'Customer True Cash List';

// Column
$_['column_customer']       = 'Customer Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Customer Group';
$_['column_status']         = 'Status';
$_['column_points']         = 'True Cash';
$_['column_orders']         = 'No. Orders';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';

// Entry
$_['entry_date_start']      = 'Date Start';
$_['entry_date_end']        = 'Date End';
$_['entry_customer']		= 'Customer';