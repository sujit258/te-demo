<?php
// Heading
$_['heading_title']     = '<b><i>Media Articles</i></b>';
$_['page_title']     = 'Media Articles';

// Text  
$_['text_success']      = 'Success: You have modified Media Articles!';
$_['text_list']      = 'Media Articles list';
$_['text_form']      = 'Media Articles form';
$_['text_edit']      = 'Edit';
$_['text_testimonial'] = 'Media Articles';
$_['text_enabled']      = 'Enabled';
$_['text_disabled']     = 'Disabled';

// Column
$_['column_name']       = 'Media Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_status']     = 'Status';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Media Name:';
$_['entry_testimonial_limit'] = "Default Items per Page";
$_['entry_testimonial_image'] = "Media Article Image Size";
$_['entry_image']        = 'Image File:';
$_['entry_description'] = 'Content:';
$_['entry_status']      = 'Status:';
$_['entry_url']         = 'Url:';
$_['entry_sort_order']  = 'Sort Order:';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Media Articles!';
$_['error_name']       		 = 'Media Name must be greater than 3 and less than 255 characters!';
$_['error_description'] 	 = 'Content must exceed 3 characters!';
$_['error_image_testimonial'] 	 = 'Media Article Image dimensions required!';
$_['error_testimonial_limit'] 	 = 'Media Article Limit in Page required!';