<?php
// Heading
$_['heading_title']     = 'Recipe Filters';

// Text
$_['text_success']      = 'Success: You have modified recipe filters!';
$_['text_list']         = 'Recipe Filter List';
$_['text_add']          = 'Add Recipe Filter';
$_['text_edit']         = 'Edit Recipe Filter';

// Column
$_['column_group']      = 'Recipe Filter Group';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_group']       = 'Recipe Filter Group Name';
$_['entry_name']        = 'Recipe Filter Name';
$_['entry_sort_order']  = 'Sort Order';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify recipe filters!';
$_['error_group']       = 'Recipe Filter Group Name must be between 1 and 64 characters!';
$_['error_name']        = 'Recipe Filter Name must be between 1 and 64 characters!';