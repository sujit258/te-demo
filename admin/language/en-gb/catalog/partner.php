<?php
// Heading
$_['heading_title']     = 'Partner With Us';


// Column
$_['column_name']     = 'Name';
$_['column_city']     = 'City';
$_['column_scope']    = 'Scope/Purpose';
$_['column_request_date'] = 'Request Date';
$_['column_action']     = 'Action';

	//Tag
	$_['tag_name']      = 'Name : ';
	$_['tag_mobile']      = 'Mobile No :  : ';
	$_['tag_email']      = 'Email : ';
	$_['tag_city']      = 'city : ';
	$_['tag_scope']      = 'Scope/Purpose : ';
	$_['tag_request_date']      = 'Request Date : ';

//text
$_['text_view']         = 'View Details';
$_['text_list']         = "Partner's List";

?>