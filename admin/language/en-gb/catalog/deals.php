<?php
// Heading
$_['heading_title']          = 'Deals';

// Text
$_['text_success']           = 'Success: You have modified deals!';
$_['text_home']              = 'Home';
$_['text_list']              = 'Deals List';
$_['text_add']               = 'Add Deal';
$_['text_edit']              = 'Edit Deal';
$_['text_default']           = 'Default';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_yes']               = 'Yes';
$_['text_no']                = 'No';
$_['text_help']              = '<br/><small>(Autocomplete)</small>';

// Column
$_['column_name']            = 'Product Name';
$_['column_price']           = 'Price';
$_['column_sku']             = 'SKU';
$_['column_homepage']        = 'Homepage';
$_['column_coupon']          = 'Coupon';
$_['column_date_start']      = 'Date Start';
$_['column_date_end']        = 'Date End';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Product Name';
$_['entry_price']            = 'Price';
$_['entry_discount']         = 'Discount(%)';
$_['entry_sku']              = 'SKU';
$_['entry_image']            = 'Image';
$_['entry_homepage']         = 'Homepage';
$_['entry_coupon']           = 'Coupon';
$_['entry_date_start']       = 'Date Start';
$_['entry_date_end']         = 'Date End';

//Tab
$_['tab_general']            = 'General';

//Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify deals!';
$_['error_product']          = 'Please select product.';
$_['error_discount']         = 'Discount should be greater than 0';
$_['error_option']           = 'Please choose product again & select sku from the list.';
$_['error_price']            = 'Price should be greater than 0';
$_['error_coupon']           = 'Please choose coupon from autocomplete box.';
$_['error_homepage']         = 'Please choose whether to display deal on homepage or not.';
$_['error_date_start']       = 'Please choose date start.';
$_['error_date_end']         = 'Please choose date start to get date end.';
?>