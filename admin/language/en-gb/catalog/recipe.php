<?php
// Heading
$_['heading_title']     = 'Recipe';

// Text
$_['text_success']           = 'Success: You have modified recipe..!';
$_['text_list']              = 'Recipe List';
$_['text_add']               = 'Add Recipe';
$_['text_edit']              = 'Edit Recipe';
$_['text_default']           = 'Default';

// Column
$_['column_title']           = 'Recipe Title';
$_['column_sort_order']	     = 'Sort Order';
$_['column_action']          = 'Action';
$_['column_date_added']      = 'Date Added';

// Filter
$_['filter_name']				  = 'Recipe Name';
$_['filter_status']				  = 'status';
$_['entry_filter']                = 'Filters';
$_['filter_category']			  = 'Category';
$_['filter_recipe_placeholder']   = 'Enter Recipe name here'; 
$_['filter_category_placeholder'] = 'Select Category';

// Entry
$_['entry_title']                 = 'Recipe Title:';
$_['entry_related']               = 'Related Products:';
$_['entry_related_recipe']        = 'Related Recipes:';
$_['entry_date_added']            = 'Date Added:';
$_['entry_description']           = 'Description:';
$_['entry_directions']            = 'Directions:';
$_['entry_ingredients']           = 'Ingredients:';
$_['entry_meta_title'] 	          = 'Meta Tag Title';
$_['entry_meta_keyword'] 	      = 'Meta Tag Keywords';
$_['entry_meta_description']      = 'Meta Tag Description';
$_['entry_image'] 	              = 'Image:';
$_['entry_category']              = 'Category:';
$_['entry_store']                 = 'Stores:';
$_['entry_keyword']               = 'SEO Keyword:';
$_['entry_bottom']                = 'Bottom:<br/><span class="help">Display in the bottom footer.</span>';
$_['entry_status']                = 'Status:';
$_['entry_sort_order']            = 'Sort Order:';
$_['entry_author']	              = 'Author:';
$_['entry_layout']                = 'Layout Override:';

// Help
$_['help_related']           = '(Autocomplete)';
$_['help_related_recipe']           = '(Autocomplete)';

// Error 
$_['error_warning']               = 'Warning: Please check the form carefully for errors!';
$_['error_permission']            = 'Warning: You do not have permission to modify Recipe!';  
$_['error_title']                 = 'Recipe Title must be between 3 and 64 characters!';
$_['error_description']           = 'Description must be more than 3 characters!';
$_['error_meta_title']           = 'Meta Title must be more than 3 characters!';
$_['error_meta_keyword']         = 'Meta Keyword must be more than 3 characters!';
$_['error_meta_description']     = 'Meta Description must be more than 3 characters!';
$_['error_account']               = 'Warning: This Recipe page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']              = 'Warning: This Recipe page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']             = 'Warning: This Recipe page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_store']                 = 'Warning: This Recipe page cannot be deleted as its currently used by %s stores!';
?>