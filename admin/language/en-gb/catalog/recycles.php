<?php

// Heading
$_['heading_title']     = 'Recycles';

// Column
$_['column_name']         = 'Customer Name';
$_['column_mobile']       = 'Mobile';
$_['column_no_of_items']  = 'No Of Items';
$_['column_status']   	  = 'Status';
$_['column_request_date'] = 'Request Date';
$_['column_action']       = 'Action';

//Tag
$_['tag_name']        = 'Customer Name : ';
$_['tag_mobile']      = 'Mobile No :  : ';
$_['tag_email']       = 'Email : ';
$_['tag_address']     = 'Address : ';
$_['tag_no_of_items'] = 'No Of Items : ';
$_['tag_request_date']= 'Request Date : ';
$_['tag_pickup_date'] = 'Pickup Date : ';
$_['tag_pickup_slot'] = 'Pickup Slot : ';
$_['tag_status']      = 'Status : ';

//text
$_['text_no_results'] = 'No match / List found';
$_['text_view']     = 'View Details';
$_['text_list']     = "Customer's List";
$_['text_confirm']  = 'Are You Sure..?';

// Button
$_['button_save']   = 'Save';

// Filter
$_['filter_name']   = 'Type Customer Name';
$_['filter_status'] = '- Select Status -';
$_['filter_mobile'] = 'Type Mobile Number';

?>