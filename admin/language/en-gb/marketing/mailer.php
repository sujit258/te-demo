<?php
/*** jitendra kumar sharma (vxinfosystem.com) jks0586@gmail.com, vxinfosystem@gmail.com ****/

// Heading
$_['heading_title']          = 'Import Reviews';

// Text
$_['text_success']           = 'Success: You have modified reviews!';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse';
$_['text_clear']             = 'Clear';
$_['text_import']            = 'Xlsx,Xls file Import here';
$_['text_success']    		 = 'Success: You have modified %s review!';
$_['text_warnings']    		 = 'Warning: %s E-Mail Address is already registered!';
$_['text_form']    			 = 'Import Review';
$_['text_customers'] 		 = 'Reviews';

$_['button_save']            = 'Save';
$_['button_cancel']          = 'Cancel';


// Error 
$_['error_warning']          = 'Warning: Please import Excel file!';
$_['text_no_data']           = 'Warning: No data Found!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
?>