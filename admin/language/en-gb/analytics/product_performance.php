<?php
// Heading
$_['heading_title']     = 'Analysis Report';

// Text
$_['text_weekly_units_summary']     = 'Summary By Quantity (Weekly)';
$_['text_monthly_units_summary']    = 'Summary By Quantity (Monthly)';
$_['text_monthly_sales_summary']    = 'Summary By Value (Monthly)';

// Column
$_['column_category_name']  = 'Category Name';
$_['column_product_name']   = 'Product Name';

// Entry
$_['entry_email_to']    = 'Enter Email id&quot;s seperated by comma(,)';