<?php
// Heading
$_['heading_title']     = 'Customer Lifetime Value';

// Column
$_['column_date_added'] = 'Registered On';
$_['column_name']       = 'Customer Name';
$_['column_email']      = 'Email';
$_['column_telephone']  = 'Telephone';
$_['column_latest_order_date']  = 'Recently Ordered On';
$_['column_value']      = 'Total Value';
$_['column_units']      = 'Total Units';
$_['column_order_count']    = 'Total Orders';
$_['column_ticket_size']    = 'Ticket Size';
?>