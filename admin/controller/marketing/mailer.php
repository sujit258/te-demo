<?php 
/*** jitendra kumar sharma (vxinfosystem.com) jks0586@gmail.com, vxinfosystem@gmail.com ****/
include DIR_SYSTEM.'library/PHPExcel.php';
class ControllerMarketingMailer extends Controller {
	private $error = array();
	
	public function index(){
		$this->language->load('marketing/mailer');
		$this->load->model('marketing/mailer');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['text_form'] = $this->language->get('text_form');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_import'] = $this->language->get('text_import');
		
		$data['cancel'] = $this->url->link('marketing/mailer','token='.$this->session->data['token'],'SSL');
		
		$data['send_mailer'] = $this->url->link('marketing/mailer/send_mailer','token='.$this->session->data['token'],'SSL');
		
		$url = '';
		
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
		);
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_customers'),
			'href'      => $this->url->link('marketing/mailer', 'token=' . $this->session->data['token'], 'SSL'),
		);

		
		////Import Coding Start
		if(($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if($this->request->files) {
			$file = basename($this->request->files['import']['name']);
			move_uploaded_file($this->request->files['import']['tmp_name'], $file);
			$inputFileName = $file;
			
			
			if(isset($this->request->post['approve'])){
				$approved = $this->request->post['approve'];
			}else{
				$approved = 0;
			}
			
			$extension = pathinfo($inputFileName);
			if($extension['basename']) {
				if($extension['extension']=='xlsx' || $extension['extension']=='xls') {
					try{
						$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					}catch(Exception $e){
						die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					}
					$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
					$i=0;
					$email_exist = 0;
					$sucess_import = 0;
					foreach($allDataInSheet as $value) {
						if($value['A']!="" && $value['B']!="" && $value['D']!="") {
							if($i!=0){
								$customer_id  = $value['A'];
								$email = $value['B'];
								$name   	 = $value['C'];
								$type  		 = $value['D'];
								$rating 	 = $value['E'];
								$status 	 = $value['F'];
								
								$filter_data=array(
									'customer_id' => $customer_id,
									'email'	=> $email,
									'name'		=> $name,
									'type'	=> $type,
									'rating'	=> $rating,
									'status'	=> $status,
								);
							
								$return_data = $this->model_marketing_mailer->addreview($filter_data);
								// if return_data returns 1 it means already exists,, otherwise success 
								if($return_data==2){
									$sucess_import += 1;
								}
								
							}
						}
						$i++;
					}
					if($sucess_import){
						$this->session->data['success'] = sprintf($this->language->get('text_success'),$sucess_import);
					}
					
					if(!$sucess_import && !$email_exist){
						$this->session->data['error_warning'] = $this->language->get('text_no_data');
					}
				} else{
					$this->session->data['error_warning'] = $this->language->get('error_warning');
				}
			}else{
				$this->session->data['error_warning'] = $this->language->get('error_warning');
			}
			if($inputFileName){
				unlink($inputFileName);
			}
			
		  }else{
			$this->session->data['error_warning'] = $this->language->get('error_warning');
		  }
		}
		if(isset($this->session->data['success'])){
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}else{
			$data['success'] = '';	
		}
		
		if(isset($this->session->data['error_warning'])){
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}else{
			$data['error_warning'] = '';	
		}
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/mailer.tpl', $data));
	}
	
	public function send_mailer() {
	    
		$this->load->language('marketing/contact');
		//$this->load->model('marketing/contact');
		$this->load->model('marketing/mailer');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_default'] = $this->language->get('text_default');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_customer_all'] = $this->language->get('text_customer_all');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_customer_group'] = $this->language->get('text_customer_group');
		$data['text_affiliate_all'] = $this->language->get('text_affiliate_all');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_to'] = $this->language->get('entry_to');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_affiliate'] = $this->language->get('entry_affiliate');
		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_subject'] = $this->language->get('entry_subject');
		$data['entry_message'] = $this->language->get('entry_message');

		$data['help_customer'] = $this->language->get('help_customer');
		$data['help_affiliate'] = $this->language->get('help_affiliate');
		$data['help_product'] = $this->language->get('help_product');

		$data['button_send'] = $this->language->get('button_send');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['token'] = $this->session->data['token'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/mailer/send_mailer', 'token=' . $this->session->data['token'], true)
		);

		$data['cancel'] = $this->url->link('marketing/mailer/send_mailer', 'token=' . $this->session->data['token'], true);
		 
		$data['importer'] = $this->url->link('marketing/mailer','token='.$this->session->data['token'],'SSL');

		$this->load->model('setting/store');

		$data['stores'] = $this->model_setting_store->getStores();
		
		$data['types'] = $this->model_marketing_mailer->getTypes();

		$this->load->model('customer/customer_group');

		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/send_mailer', $data));
	}
	
	public function send() {
		$this->load->language('marketing/contact');
        	$this->load->model('marketing/mailer');
        	
		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if (!$this->user->hasPermission('modify', 'marketing/mailer')) {
				$json['error']['warning'] = $this->language->get('error_permission');
			}

			if (!$this->request->post['subject']) {
				$json['error']['subject'] = $this->language->get('error_subject');
			}

			if (!$this->request->post['message']) {
				$json['error']['message'] = $this->language->get('error_message');
			}

			if (!$json) {
				$this->load->model('setting/store');

				$store_info = $this->model_setting_store->getStore($this->request->post['store_id']);

				if ($store_info) {
					$store_name = $store_info['name'];
				} else {
					$store_name = $this->config->get('config_name');
				}
				
				$this->load->model('setting/setting');
				$setting = $this->model_setting_setting->getSetting('config', $this->request->post['store_id']);
				$store_email = isset($setting['config_email']) ? $setting['config_email'] : $this->config->get('config_email');

				$this->load->model('customer/customer');

				$this->load->model('customer/customer_group');

				$this->load->model('marketing/affiliate');

				$this->load->model('sale/order');

				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}

				$email_total = 0;

				$emails = array();
				
				if($this->request->post['customer_group_id']){
				    
				    $customer_data = array(
						'filter_type' => $this->request->post['customer_group_id'],
						'start'                    => ($page - 1) * 10,
						'limit'                    => 10
					);

					$email_total = $this->model_marketing_mailer->getTotalCustomers($customer_data);

					$results = $this->model_marketing_mailer->getCustomers($customer_data);

    				foreach ($results as $result) {
    					$emails[] = $result['email'];
    				}
				}
				
				if ($emails) {
					$json['success'] = $this->language->get('text_success');

					$start = ($page - 1) * 10;
					$end = $start + 10;

					if ($end < $email_total) {
						$json['success'] = sprintf($this->language->get('text_sent'), $start, $email_total);
					}

					if ($end < $email_total) {
						$json['next'] = str_replace('&amp;', '&', $this->url->link('marketing/mailer/send', 'token=' . $this->session->data['token'] . '&page=' . ($page + 1), true));
					} else {
						$json['next'] = '';
					}

					$message  = '<html dir="ltr" lang="en">' . "\n";
					$message .= '  <head>' . "\n";
					$message .= '    <title>' . $this->request->post['subject'] . '</title>' . "\n";
					$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
					$message .= '  </head>' . "\n";
					$message .= '  <body>' . html_entity_decode($this->request->post['message'], ENT_QUOTES, 'UTF-8') . '</body>' . "\n";
					$message .= '</html>' . "\n";
                    $i = 0;
					foreach ($emails as $email) {
						if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
						    $i++;
						    if($i%200 == 0) {
        		                sleep(2);
        		            }
							$mail = new Mail();
							$mail->protocol = $this->config->get('config_mail_protocol');
							$mail->parameter = $this->config->get('config_mail_parameter');
							$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
							$mail->smtp_username = $this->config->get('config_mail_smtp_username');
							$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
							$mail->smtp_port = $this->config->get('config_mail_smtp_port');
							$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

							$mail->setTo($email);
							$mail->setFrom($store_email);
							$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
							$mail->setSubject(html_entity_decode($this->request->post['subject'], ENT_QUOTES, 'UTF-8'));
							$mail->setHtml($message);
							//$mail->send();
						}
					}
				} else {
					$json['error']['email'] = $this->language->get('error_email');
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>