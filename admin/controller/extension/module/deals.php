<?php
class ControllerExtensionModuleDeals extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/deals');

		$this->document->setTitle($this->language->get('heading_title')); 

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('deals', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_registration'] = $this->language->get('entry_registration');
		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_firstorder'] = $this->language->get('entry_firstorder');
		$data['entry_reviews'] = $this->language->get('entry_reviews');
		$data['entry_unsubscribe'] = $this->language->get('entry_unsubscribe');
		$data['entry_status'] = $this->language->get('entry_status');
		
		$data['help_registration'] = $this->language->get('help_registration');
		$data['help_newsletter'] = $this->language->get('help_newsletter');
		$data['help_firstorder'] = $this->language->get('help_firstorder');
		$data['help_reviews'] = $this->language->get('help_reviews');
		$data['help_unsubscribe'] = $this->language->get('help_unsubscribe');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/deals', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/module/deals', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);
		
		

		if (isset($this->request->post['deals_status'])) {
			$data['deals_status'] = $this->request->post['deals_status'];
		} else {
			$data['deals_status'] = $this->config->get('deals_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/deals', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/deals')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
