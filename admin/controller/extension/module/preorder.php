<?php
class ControllerExtensionModulepreorder extends Controller {
	private $data = array();
	private $error = array(); 
	private $version = '2.9.2';
		
	public function index() {   
		$this->language->load('extension/module/preorder');
        $this->load->model('extension/module/preorder');
		$this->load->model('catalog/product');
		$this->load->model('setting/store');
		$this->load->model('setting/setting');
		$this->load->model('localisation/language');

		$this->document->setTitle($this->language->get('heading_title').' '.$this->version);
		$this->document->addStyle('view/stylesheet/preorder.css');
		
		$this->document->addStyle('view/javascript/preorder/colorpicker/css/colorpicker.css');
		$this->document->addScript('view/javascript/preorder/colorpicker/js/colorpicker.js');
					
		if(!isset($this->request->get['store_id'])) {
           $this->request->get['store_id'] = 0; 
        }
		
		$catalogURL = $this->getCatalogURL();
		$store = $this->getCurrentStore($this->request->get['store_id']);
			
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {
			if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
				$this->request->post['preorder']['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
			}
			if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
				$this->request->post['preorder']['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']),true);
			}
			$store = $this->getCurrentStore($this->request->post['store_id']);
			$this->model_setting_setting->editSetting('preorder', $this->request->post, $this->request->post['store_id']);	
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module/preorder', 'store_id='.$this->request->post['store_id'] . '&token=' . $this->session->data['token']. '&type=module', true));
		}
		
		$languages = $this->model_localisation_language->getLanguages();
		$this->data['languages'] = $languages;
		
		foreach ($this->data['languages'] as $key => $value) {
			$this->data['languages'][$key]['flag_url'] = 'language/'.$this->data['languages'][$key]['code'].'/'.$this->data['languages'][$key]['code'].'.png"';
		}
		
		$firstLanguage = array_shift($languages);
		$this->data['firstLanguageCode'] = $firstLanguage['code'];
		
		$languageVariables = array(
			'text_default',
			'button_cancel',
			'text_disabled',
            'text_enabled',
            'text_module',
            'text_module_settings',
            'text_module_settings_help',
            'default_notification',
            'text_success',
            'text_success_activation',
            'text_button_name',
            'text_pre_order',
            'text_pre_order_note',
            'text_pre_order_note_help',
            'pre_order_note',
            'text_admin_notification',
            'text_admin_notification_help',
			'text_email',
			'text_email_help',
            'text_email_subject',
            'text_email_body',
            'text_custom_css',
            'text_customer_email',
			'text_customer_name',
            'text_product',
			'text_date',
            'text_language',
			'text_actions',
			'text_remove',
			'text_remove_all',
			'text_module_status',
			'text_module_status_help',
			'error_allow_checkout',
			'notifywhenavailable_enabled',
			'text_date_note',
			'text_date_note_help',
			'text_date_note_example',
			'text_custom_colors',
			'text_custom_colors_help',
			'text_text',
			'text_background',
			'text_border',
			'text_image_size',
			'text_image_size_help',
			'text_width',
			'text_height'
        ); 

        foreach ($languageVariables as $languageVariable) {
           $this->data[$languageVariable] = $this->language->get($languageVariable);
        }
		
		$this->data['heading_title'] = $this->language->get('heading_title').' '.$this->version;

		
 		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token']. '&type=module', true),
      		'separator' => false
   		);
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token']. '&type=module', true),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/preorder', 'token=' . $this->session->data['token']. '&type=module', true),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('extension/module/preorder', 'token=' . $this->session->data['token']. '&type=module', true);
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token']. '&type=module', true);
		if (isset($this->request->post['preorder'])) {
			foreach ($this->request->post['preorder'] as $key => $value) {
				$this->data['data']['preorder'][$key] = $this->request->post['preorder'][$key];
			}
		} else {
			$configValue = $this->config->get('preorder');
			$this->data['data']['preorder'] = $configValue;		
		}

		$stats	= $this->model_extension_module_preorder->getStatistics($store['store_id']);
			
		$this->data['products'] = array();
		
		if (isset($stats->rows)) {
			foreach ($stats->rows as $row) {
				$this->data['products'][$row['product_id']][$row['notify']][$row['order_id']] = 1;
			}
		}

		$this->load->model('localisation/stock_status');

		$this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

		if($this->config->get('config_stock_checkout') == 0) {
			$this->data['allow_checkout'] = false;	
		} else {
			$this->data['allow_checkout'] = true;
		}

		$this->data['stores'] 				= array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' (' . $this->data['text_default'].')', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());	
		$this->data['store']				= $store;
		$this->data['data']					= $this->model_setting_setting->getSetting('preorder', $store['store_id']);
        $this->data['modules']				= $this->model_setting_setting->getSetting('preorder_module', $store['store_id']);
		$this->data['product_info']  		= $this->model_catalog_product;
		$this->data['token'] 				= $this->session->data['token'];
		$this->data['header']				= $this->load->controller('common/header');
		$this->data['column_left']			= $this->load->controller('common/column_left');
		$this->data['footer']				= $this->load->controller('common/footer');
		
		//Check if NotifyWhenAvailable is installed and enabled		
		$this->data['notifywhenavailable']	= $this->model_setting_setting->getSetting('notifywhenavailable', $store['store_id']);

        $this->response->setOutput($this->load->view('extension/module/preorder.tpl', $this->data));
	}

 	public function install() {
        $this->load->model('extension/module/preorder');
        $this->model_extension_module_preorder->install();	
	}

    public function uninstall() {
    	$this->load->model('setting/setting');
		$this->load->model('setting/store');
		$this->model_setting_setting->deleteSetting('preorder_module',0);
		$stores=$this->model_setting_store->getStores();
		foreach ($stores as $store) {
			$this->model_setting_setting->deleteSetting('preorders', $store['store_id']);
		}
        $this->load->model('extension/module/preorder');
        $this->model_extension_module_preorder->uninstall();
    }
	
	private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        } 
        return $storeURL;
    }

    private function getServerURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_SERVER;
        } else {
            $storeURL = HTTP_SERVER;
        } 
        return $storeURL;
    }

    private function getCurrentStore($store_id) {    
        if($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL(); 
        }
        return $store;
    }

	 public function getcustomers() {
        if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
        } else {
            $page = '1';    
        }

		if (!empty($this->request->get['store_id'])) {
            $store_id = (int) $this->request->get['store_id'];
        } else {
        	$store_id = 0;
        }
		$this->language->load('extension/module/preorder');
			$languageVariables = array(
			'text_default',
			'button_cancel',
			'text_disabled',
            'text_enabled',
            'text_module',
            'text_module_settings',
            'text_module_settings_help',
            'default_notification',
            'text_success',
            'text_success_activation',
            'text_button_name',
            'text_pre_order',
            'text_pre_order_note',
            'text_pre_order_note_help',
            'pre_order_note',
            'text_admin_notification',
            'text_admin_notification_help',
			'text_email',
			'text_email_help',
            'text_email_subject',
            'text_email_body',
            'text_custom_css',
            'text_customer_email',
			'text_customer_name',
            'text_product',
			'text_date',
            'text_language',
			'text_actions',
			'text_remove',
			'text_remove_all',
			'text_module_status',
			'text_module_status_help',
			
        ); 

        foreach ($languageVariables as $languageVariable) {
           $this->data[$languageVariable] = $this->language->get($languageVariable);
        }
		
		$this->data['heading_title'] = $this->language->get('heading_title').' '.$this->version;

        $this->load->model('extension/module/preorder');
		
		$this->data['store_id']		= $store_id;
		$this->data['token']		= $this->session->data['token'];
		$this->data['limit']		= 8;
		$this->data['total']		= $this->model_extension_module_preorder->getTotalCustomers($this->data['store_id']);
		
	    $pagination					= new Pagination();
        $pagination->total			= $this->data['total'];
        $pagination->page			= $page;
        $pagination->limit			= $this->data['limit']; 
        $pagination->url			= $this->url->link('extension/module/preorder/getcustomers','token=' . $this->session->data['token'].'&page={page}&store_id='.$this->data['store_id'], 'SSL');
		$this->data['pagination']	= $pagination->render();
        $this->data['sources']		= $this->model_extension_module_preorder->viewcustomers($page, $this->data['limit'], $this->data['store_id']);

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($this->data['total']) ? (($page - 1) * $this->data['limit']) + 1 : 0, ((($page - 1) * $this->data['limit']) > ($this->data['total'] - $this->data['limit'])) ? $this->data['total'] : ((($page - 1) * $this->data['limit']) + $this->data['limit']), $this->data['total'], ceil($this->data['total'] / $this->data['limit']));
		
        $this->template       	    = 'extension/module/preorder/viewcustomers.tpl';
		
        $this->response->setOutput($this->load->view($this->template, $this->data));
    }
	
	public function getarchive() {
        if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
        }

        if (!empty($this->request->get['store_id'])) {
            $store_id = (int) $this->request->get['store_id'];
        } else {
        	$store_id = 0;
        }
$this->language->load('extension/module/preorder');
	$languageVariables = array(
			'text_default',
			'button_cancel',
			'text_disabled', 
            'text_enabled',
            'text_module',
            'text_module_settings',
            'text_module_settings_help',
            'default_notification',
            'text_success',
            'text_success_activation',
            'text_button_name',
            'text_pre_order',
            'text_pre_order_note',
            'text_pre_order_note_help',
            'pre_order_note',
            'text_admin_notification',
            'text_admin_notification_help',
			'text_email',
			'text_email_help',
            'text_email_subject',
            'text_email_body',
            'text_custom_css',
            'text_customer_email',
			'text_customer_name',
            'text_product',
			'text_date',
            'text_language',
			'text_actions',
			'text_remove',
			'text_remove_all',
			'text_module_status',
			'text_module_status_help',
			'text_notified_customers',
			'text_notified_customers_help',
			
        ); 

        foreach ($languageVariables as $languageVariable) {
           $this->data[$languageVariable] = $this->language->get($languageVariable);
        }
		
		$this->data['heading_title'] = $this->language->get('heading_title').' '.$this->version;

		$this->load->model('extension/module/preorder');
		$this->data['store_id']		= $store_id;
		$this->data['token']		= $this->session->data['token'];
		$this->data['limit']		= 8;
		$this->data['total']		= $this->model_extension_module_preorder->getTotalNotifiedCustomers($this->data['store_id']);
		
	    $pagination					= new Pagination();
        $pagination->total			= $this->data['total'];
        $pagination->page			= $page;
        $pagination->limit			= $this->data['limit']; 
        $pagination->url			= $this->url->link('extension/module/preorder/getarchive','token=' . $this->session->data['token'].'&page={page}&store_id='.$this->data['store_id'], 'SSL');
		$this->data['pagination']	= $pagination->render();
        $this->data['sources']		= $this->model_extension_module_preorder->viewnotifiedcustomers($page, $this->data['limit'], $this->data['store_id']);

		$this->data['results'] 		= sprintf($this->language->get('text_pagination'), ($this->data['total']) ? (($page - 1) * $this->data['limit']) + 1 : 0, ((($page - 1) * $this->data['limit']) > ($this->data['total'] - $this->data['limit'])) ? $this->data['total'] : ((($page - 1) * $this->data['limit']) + $this->data['limit']), $this->data['total'], ceil($this->data['total'] / $this->data['limit']));
		
        $this->template       	    = 'extension/module/preorder/archive.tpl';
		
        $this->response->setOutput($this->load->view($this->template, $this->data));
    }
	
	public function removecustomer() {
		if (isset($_POST['preorder_id'])) {
				$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "preorder` WHERE `preorder_id`=".(int)$_POST['preorder_id']);
				if ($run_query) echo "Success!";
			}
	}
		
	public function removeallcustomers() {
		if (isset($_POST['remove']) && ($_POST['remove']==true)) {
				$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "preorder`");
				if ($run_query) echo "Success!";
			}
	}
		
	public function removeallarchive() { 
		$run_query = $this->db->query("DELETE p FROM `" . DB_PREFIX . "preorder` p
		LEFT JOIN `" . DB_PREFIX . "order_history` oh ON oh.order_id = p.order_id
		WHERE oh.notify=1");
		if ($run_query) echo "Success!"; 
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'extension/module/preorder')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}
?>