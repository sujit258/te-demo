<?php
class ControllerExtensionModuleIanalytics extends Controller {
	private $moduleName;
    private $modulePath;
	private $moduleModel;
	private $moduleVersion;
    private $extensionsLink;
    private $callModel;
    private $error = array(); 
    private $data = array();

    public function __construct($registry) {
        parent::__construct($registry);
        
        // Config Loader
        $this->config->load('isenselabs/ianalytics');
        
        // Module Constants
        $this->moduleName           = $this->config->get('ianalytics_name');
        $this->callModel            = $this->config->get('ianalytics_model');
        $this->modulePath           = $this->config->get('ianalytics_path');
        $this->moduleVersion        = $this->config->get('ianalytics_version');        
        $this->extensionsLink       = $this->url->link($this->config->get('ianalytics_link'), 'token=' . $this->session->data['token'].$this->config->get('ianalytics_link_params'), 'SSL');

        // Load Language
        $this->load->language($this->modulePath);
        
        // Load Model
        $this->load->model($this->modulePath);
        
        // Model Instance
        $this->moduleModel          = $this->{$this->callModel};
        
        // Global Variables      
        $this->data['moduleName']   = $this->moduleName;
        $this->data['modulePath']   = $this->modulePath;
        
        $this->data['limit']        = 15;
    }
    
	public function index() {   
		$this->load->model('setting/setting');
		$this->load->model('localisation/order_status');
		$this->load->model('report/sale');
		$this->load->model('report/product');
		$this->load->model('report/customer');
        $this->load->model('setting/store');
		
        $this->document->addStyle('view/stylesheet/' . $this->moduleName . '/' . $this->moduleName . '.css');
		$this->document->addStyle('view/javascript/' . $this->moduleName . '/jquery/css/ui-lightness/jquery-ui-1.9.2.custom.min.css');
		$this->document->addScript('view/javascript/' . $this->moduleName . '/jquery/js/jquery-ui-1.9.2.custom.min.js');
		$this->document->addScript('view/javascript/' . $this->moduleName . '/charts/Chart.js');
		$this->document->addScript('view/javascript/' . $this->moduleName . '/d3.v2.min.js');
		$this->document->addScript('view/javascript/' . $this->moduleName . '/d3-funnel-charts.min.js');
        $this->document->addScript('view/javascript/' . $this->moduleName . '/' . $this->moduleName . '.js');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$store_id = $this->getStoreId();

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) { 	
            if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
                $this->request->post[$this->moduleName]['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
            }
            if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
                $this->request->post[$this->moduleName]['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']), true);
            }

        	$this->model_setting_setting->editSetting($this->moduleName, $this->request->post, $store_id);
            
            $this->session->data['success'] = $this->language->get('text_success');
            
            $this->response->redirect($this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL'));
        }
		
		$this->data['breadcrumbs']    = array();
   		$this->data['breadcrumbs'][]  = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
   		$this->data['breadcrumbs'][]  = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->extensionsLink
        );
   		$this->data['breadcrumbs'][]  = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL')
        );
	
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
			
		  // Languages
        $this->language->load($this->modulePath);
		$language_strings = $this->language->load($this->modulePath);
        foreach ($language_strings as $code => $languageVariable) {
			$this->data[$code] = $languageVariable;
		}
        $this->data['heading_title']            .= ' '.$this->moduleVersion;
		
		$this->data['token']                  	= $this->session->data['token'];
        $this->data['action']                 	= $this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel']                 	= $this->extensionsLink;
        
		$this->data['moduleSettings']           = $this->model_setting_setting->getSetting($this->moduleName, $store_id);
        $this->data['moduleData']               = (isset($this->data['moduleSettings'][$this->moduleName])) ? $this->data['moduleSettings'][$this->moduleName] : array();
		$this->data[$this->moduleName.'Status']	= $this->model_setting_setting->getSetting($this->moduleName.'Status', $store_id);

		$this->data['order_statuses']           = $this->model_localisation_order_status->getOrderStatuses();
		$this->data['currency']					= $this->config->get('config_currency');
        $this->data['stores']                   = array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' ' . $this->data['text_default'].' ', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());

        $this->data['store']                    = $this->getCurrentStore($store_id);
        $this->data['store_id']                 = $store_id;
		
		$this->data['model_sale_order']         = $this->model_report_sale;
		$this->data['model_report_product']     = $this->model_report_product;
		$this->data['model_report_customer']    = $this->model_report_customer;
		$this->data['model_module_ianalitycs']	= $this->{$this->callModel};
		$this->data['url_link']					= $this->url;
		$this->data['language']					= $this->language;
		$this->data['currency_lib']				= $this->currency;
		
		$this->data = $this->moduleModel->getAnalyticsData($this->data, $store_id);
		
		if ($this->config->get($this->moduleName.'_status')) {
			$this->data[$this->moduleName.'_status'] = $this->config->get($this->moduleName.'_status');
		} else {
			$this->data[$this->moduleName.'_status'] = '0';
		}

		$this->data['column_left']				    = $this->load->controller('common/column_left');
		$this->data['footer']						= $this->load->controller('common/footer');
        $this->data['header']						= $this->load->controller('common/header');

		$this->response->setOutput($this->load->view($this->modulePath . '/'.$this->moduleName.'.tpl', $this->data));
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', $this->modulePath)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	
	public function pausegatheringdata() {
		$this->load->model('setting/setting');
		
		$store_id = $this->getStoreId();
		
		$this->model_setting_setting->editSetting($this->moduleName.'Status', array($this->moduleName.'Status' => 'pause'), $store_id);
		
        $this->session->data['success'] = 'iAnalytics data gathering is now <strong>paused</strong>.';
		
        $this->response->redirect($this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL'));
	}

	public function resumegatheringdata() {
		$this->load->model('setting/setting');

		$store_id = $this->getStoreId();
		
		$this->model_setting_setting->editSetting($this->moduleName.'Status', array($this->moduleName.'Status' => 'run'), $store_id);
		$this->session->data['success'] = 'iAnalytics data gathering is now <strong>resumed</strong>.';
		$this->response->redirect($this->url->link($this->modulePath, 'store_id=' .$store_id . '&token=' . $this->session->data['token'], 'SSL'));
	}
	
	public function deletesearchkeyword() {
		$store_id = $this->getStoreId();

		if (!$this->validateForm()) { 
			$this->response->redirect($this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL'));
		}
		
		if (!empty($_GET['searchValue'])) {
			$this->moduleModel->deleteSearchKeyword($_GET['searchValue'], $store_id);
			
			$this->session->data['success'] = $this->language->get('deleted_keyword');
		}
		
		$this->response->redirect($this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'] . '&tab=1&searchTab=1', 'SSL'));
	}
	
	public function deleteallsearchkeyword() {
		$store_id = $this->getStoreId();

		if (!$this->validateForm()) { 
			$this->redirect($this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL'));
		}
		
		if (!empty($_GET['searchValue'])) {
			$this->moduleModel->deleteAllSearchKeyword($_GET['searchValue'], $store_id);
			
			$this->session->data['success'] = $this->language->get('deleted_keyword');
		}
		
		$this->response->redirect($this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL'));
	}
	
	public function deleteanalyticsdata() {
		$store_id = $this->getStoreId();
		
		if (!$this->validateForm()) { 
			$this->response->redirect($this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL'));
		}

		$this->moduleModel->deleteAnalyticsData($store_id);
		
		$this->session->data['success'] = $this->language->get('deleted_analytics_data');
		
		$this->response->redirect($this->url->link($this->modulePath, 'store_id=' . $store_id . '&token=' . $this->session->data['token'], 'SSL'));
	}
	
	public function install() {
		$this->load->model('setting/setting');
		$this->load->model('setting/store');
		
        $this->data['stores']	= array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . '(Default)', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());

		foreach($this->data['stores'] as $store) {
			$this->model_setting_setting->editSetting($this->moduleName.'Status', array($this->moduleName.'Status' => 'run'), $store['store_id']);
		}
		
	    $this->moduleModel->install();
    }
    
    public function uninstall() {
		$this->load->model('setting/setting');

		$store_id = $this->getStoreId();

		$this->model_setting_setting->deleteSetting($this->moduleName, $store_id);
		$this->model_setting_setting->deleteSetting($this->moduleName.'Status', $store_id);
       
        $this->moduleModel->uninstall();
	}
	
    private function getStoreId() {
        $store_id = 0;
        
        if (isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id'];
		} elseif (isset($this->request->post['store_id'])) {
			$store_id = $this->request->post['store_id'];
		} else {
			$store_id = 0;
		}
        
        return $store_id;
    }
    
	private function getCurrentStore($store_id) {    
        if($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL(); 
        }
        return $store;
    }
	
	private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        } 
        return $storeURL;
    }

}