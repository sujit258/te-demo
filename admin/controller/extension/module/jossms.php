<?php
class ControllerExtensionModuleJosSMS extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('extension/module/jossms');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$prefix = array(
					'jossms_AD' => 376,'jossms_AE' => 971,'jossms_AF' => 93,'jossms_AG' => 1268,'jossms_AI' => 1264,'jossms_AL' => 355,'jossms_AM' => 374,'jossms_AN' => 599,'jossms_AO' => 244,
					'jossms_AQ' => 672,'jossms_AR' => 54,'jossms_AS' => 1684,'jossms_AT' => 43,'jossms_AU' => 61,'jossms_AW' => 297,'jossms_AX' => '','jossms_AZ' => 994,'jossms_BA' => 387,
					'jossms_BB' => 1246,'jossms_BD' => 880,'jossms_BE' => 32,'jossms_BF' => 226,'jossms_BG' => 359,'jossms_BH' => 973,'jossms_BI' => 257,'jossms_BJ' => 229,'jossms_BL' => 590,'jossms_BM' => 1441,
					'jossms_BN' => 673,'jossms_BO' => 591,'jossms_BR' => 55,'jossms_BS' => 1242,'jossms_BT' => 975,'jossms_BV' => '','jossms_BW' => 267,'jossms_BY' => 375,'jossms_BZ' => 501,
					'jossms_CA' => 1,'jossms_CC' => 61,'jossms_CD' => 242,'jossms_CF' => 236,'jossms_CG' => 243,'jossms_CH' => 41,'jossms_CI' => 225,'jossms_CK' => 682,'jossms_CL' => 56,'jossms_CM' => 237,
					'jossms_CN' => 86,'jossms_CO' => 57,'jossms_CR' => 506,'jossms_CU' => 53,'jossms_CV' => 238,'jossms_CX' => 61,'jossms_CY' => 357,'jossms_CZ' => 420,'jossms_DE' => 49,'jossms_DJ' => 253,
					'jossms_DK' => 45,'jossms_DM' => 1767,'jossms_DO' => 1809,'jossms_DZ' => 213,'jossms_EC' => 593,'jossms_EE' => 372,'jossms_EG' => 20,'jossms_EH' => '','jossms_ER' => 291,'jossms_ES' => 34,
					'jossms_ET' => 251,'jossms_FI' => 358,'jossms_FJ' => 679,'jossms_FK' => 500,'jossms_FM' => 691,'jossms_FO' => 298,'jossms_FR' => 33,'jossms_GA' => 241,'jossms_GB' => 44,'jossms_GD' => 1473,
					'jossms_GE' => 995,'jossms_GF' => 594,'jossms_GG' => 44,'jossms_GH' => 233,'jossms_GI' => 350,'jossms_GL' => 299,'jossms_GM' => 220,'jossms_GN' => 224,'jossms_GP' => 590,'jossms_GQ' => 240,
					'jossms_GR' => 30,'jossms_GS' => 500,'jossms_GT' => 502,'jossms_GU' => 1671,'jossms_GW' => 245,'jossms_GY' => 592,'jossms_HK' => 852,'jossms_HM' => '','jossms_HN' => 504,'jossms_HR' => 385,
					'jossms_HT' => 509,'jossms_HU' => 36,'jossms_ID' => 62,'jossms_IE' => 353,'jossms_IL' => 972,'jossms_IM' => 44,'jossms_IN' => 91,'jossms_IO' => 1284,'jossms_IQ' => 964,'jossms_IR' => 98,
					'jossms_IS' => 354,'jossms_IT' => 39,'jossms_JE' => 44,'jossms_JM' => 1876,'jossms_JO' => 962,'jossms_JP' => 81,'jossms_KE' => 254,'jossms_KG' => 996,'jossms_KH' => 855,'jossms_KI' => 686,
					'jossms_KM' => 269,'jossms_KN' => 1869,'jossms_KP' => 850,'jossms_KR' => 82,'jossms_KW' => 965,'jossms_KY' => 1345,'jossms_KZ' => 7,'jossms_LA' => 856,'jossms_LB' => 961,'jossms_LC' => 1758,
					'jossms_LI' => 423,'jossms_LK' => 94,'jossms_LR' => 231,'jossms_LS' => 266,'jossms_LT' => 370,'jossms_LU' => 352,'jossms_LV' => 371,'jossms_LY' => 218,'jossms_MA' => 212,'jossms_MC' => 377,
					'jossms_MD' => 373,'jossms_ME' => 382,'jossms_MF' => 1599,'jossms_MG' => 261,'jossms_MH' => 692,'jossms_MK' => 389,'jossms_ML' => 223,'jossms_MM' => 95,'jossms_MN' => 976,'jossms_MO' => 853,
					'jossms_MP' => 1670,'jossms_MQ' => 596,'jossms_MR' => 222,'jossms_MS' => 1664,'jossms_MT' => 356,'jossms_MU' => 230,'jossms_MV' => 960,'jossms_MW' => 265,'jossms_MX' => 52,'jossms_MY' => 60,
					'jossms_MZ' => 258,'jossms_NA' => 264,'jossms_NC' => 687,'jossms_NE' => 227,'jossms_NF' => 672,'jossms_NG' => 234,'jossms_NI' => 505,'jossms_NL' => 31,'jossms_NO' => 47,'jossms_NP' => 977,
					'jossms_NR' => 674,'jossms_NU' => 683,'jossms_NZ' => 64,'jossms_OM' => 968,'jossms_PA' => 507,'jossms_PE' => 51,'jossms_PF' => 689,'jossms_PG' => 675,'jossms_PH' => 63,'jossms_PK' => 92,
					'jossms_PL' => 48,'jossms_PM' => 508,'jossms_PN' => 870,'jossms_PR' => 1,'jossms_PS' => 970,'jossms_PT' => 351,'jossms_PW' => 680,'jossms_PY' => 595,'jossms_QA' => 974,'jossms_RE' => 262,
					'jossms_RO' => 40,'jossms_RS' => 381,'jossms_RU' => 7,'jossms_RW' => 250,'jossms_SA' => 966,'jossms_SB' => 677,'jossms_SC' => 248,'jossms_SD' => 249,'jossms_SE' => 46,'jossms_SG' => 65,
					'jossms_SI' => 386,'jossms_SJ' => '','jossms_SK' => 421,'jossms_SL' => 232,'jossms_SM' => 378,'jossms_SN' => 221,'jossms_SO' => 252,'jossms_SR' => 597,'jossms_ST' => 239,'jossms_SV' => 503,
					'jossms_SY' => 963,'jossms_SZ' => 268,'jossms_TC' => 1649,'jossms_TD' => 235,'jossms_TF' => '','jossms_TG' => 228,'jossms_TH' => 66,'jossms_TJ' => 992,'jossms_TK' => 690,'jossms_TL' => 670,
					'jossms_TM' => 993,'jossms_TN' => 216,'jossms_TO' => 676,'jossms_TR' => 90,'jossms_TT' => 1868,'jossms_TV' => 688,'jossms_TW' => 886,'jossms_TZ' => 255,'jossms_UA' => 380,'jossms_UG' => 256,
					'jossms_US' => 1,'jossms_UY' => 598,'jossms_UZ' => 998,'jossms_VA' => 379,'jossms_VC' => 1784,'jossms_VE' => 58,'jossms_VG' => 1284,'jossms_VI' => 1340,'jossms_VN' => 84,'jossms_VU' => 678,
					'jossms_WF' => 681,'jossms_WS' => 685,'jossms_YE' => 967,'jossms_YT' => 262,'jossms_ZA' => 27,'jossms_ZM' => 260,'jossms_ZW' => 263
			);
			$this->model_setting_setting->editSetting('jossms', array_merge($this->request->post,$prefix));
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module/jossms', 'token=' . $this->session->data['token'], true));
		}
				
		$data['heading_title'] = $this->language->get('heading_title');
		$data['intruction_title'] = $this->language->get('intruction_title');
		
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_content_top'] = $this->language->get('text_content_top');
		$data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$data['text_column_left'] = $this->language->get('text_column_left');
		$data['text_column_right'] = $this->language->get('text_column_right');
		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_status_none'] = $this->language->get('text_status_none');
		$data['text_enable_verify'] = $this->language->get('text_enable_verify');
		$data['text_verify_checkout'] = $this->language->get('text_verify_checkout');
		$data['text_verify_register'] = $this->language->get('text_verify_register');
		$data['text_verify_forgotten'] = $this->language->get('text_verify_forgotten');
		
		$data['entry_gateway'] = $this->language->get('entry_gateway');
		$data['entry_userkey'] = $this->language->get('entry_userkey');
		$data['entry_passkey'] = $this->language->get('entry_passkey');
		$data['entry_httpapi'] = $this->language->get('entry_httpapi');
		$data['httpapi_example'] = $this->language->get('httpapi_example');
		
		$data['entry_userkey_amd'] = $this->language->get('entry_userkey_amd');
		$data['entry_passkey_amd'] = $this->language->get('entry_passkey_amd');
		$data['entry_httpapi_amd'] = $this->language->get('entry_httpapi_amd');
		$data['entry_senderid_amd'] = $this->language->get('entry_senderid_amd');
		$data['httpapi_example_amd'] = $this->language->get('httpapi_example_amd');
		
		$data['entry_userkey_smsglobal'] = $this->language->get('entry_userkey_smsglobal');
		$data['entry_passkey_smsglobal'] = $this->language->get('entry_passkey_smsglobal');
		$data['entry_httpapi_smsglobal'] = $this->language->get('entry_httpapi_smsglobal');
		$data['entry_senderid_smsglobal'] = $this->language->get('entry_senderid_smsglobal');
		$data['httpapi_example_smsglobal'] = $this->language->get('httpapi_example_smsglobal');
		
		$data['entry_userkey_clickatell'] = $this->language->get('entry_userkey_clickatell');
		$data['entry_passkey_clickatell'] = $this->language->get('entry_passkey_clickatell');
		$data['entry_httpapi_clickatell'] = $this->language->get('entry_httpapi_clickatell');
		$data['entry_apiid_clickatell'] = $this->language->get('entry_apiid_clickatell');
		$data['entry_senderid_clickatell'] = $this->language->get('entry_senderid_clickatell');
		$data['httpapi_example_clickatell'] = $this->language->get('httpapi_example_clickatell');
		$data['entry_unicode_clickatell'] = $this->language->get('entry_unicode_clickatell');
		
		$data['entry_userkey_liveall'] = $this->language->get('entry_userkey_liveall');
		$data['entry_passkey_liveall'] = $this->language->get('entry_passkey_liveall');
		$data['entry_httpapi_liveall'] = $this->language->get('entry_httpapi_liveall');
		$data['entry_senderid_liveall'] = $this->language->get('entry_senderid_liveall');
		$data['httpapi_example_liveall'] = $this->language->get('httpapi_example_liveall');
		
		$data['entry_userkey_malath'] = $this->language->get('entry_userkey_malath');
		$data['entry_passkey_malath'] = $this->language->get('entry_passkey_malath');
		$data['entry_httpapi_malath'] = $this->language->get('entry_httpapi_malath');
		$data['entry_senderid_malath'] = $this->language->get('entry_senderid_malath');
		$data['httpapi_example_malath'] = $this->language->get('httpapi_example_malath');
		$data['entry_unicode_malath'] = $this->language->get('entry_unicode_malath');
		
		$data['entry_userkey_mobily'] = $this->language->get('entry_userkey_mobily');
		$data['entry_passkey_mobily'] = $this->language->get('entry_passkey_mobily');
		$data['entry_httpapi_mobily'] = $this->language->get('entry_httpapi_mobily');
		$data['entry_senderid_mobily'] = $this->language->get('entry_senderid_mobily');
		$data['httpapi_example_mobily'] = $this->language->get('httpapi_example_mobily');
		
		$data['entry_userkey_mvaayoo'] = $this->language->get('entry_userkey_mvaayoo');
		$data['entry_passkey_mvaayoo'] = $this->language->get('entry_passkey_mvaayoo');
		$data['entry_httpapi_mvaayoo'] = $this->language->get('entry_httpapi_mvaayoo');
		$data['entry_senderid_mvaayoo'] = $this->language->get('entry_senderid_mvaayoo');
		$data['httpapi_example_mvaayoo'] = $this->language->get('httpapi_example_mvaayoo');
		$data['entry_term_mvaayoo'] = $this->language->get('entry_term_mvaayoo');
		$data['text_term_mvaayoo'] = $this->language->get('text_term_mvaayoo');
		
		$data['entry_userkey_msegat'] = $this->language->get('entry_userkey_msegat');
		$data['entry_passkey_msegat'] = $this->language->get('entry_passkey_msegat');
		$data['entry_httpapi_msegat'] = $this->language->get('entry_httpapi_msegat');
		$data['entry_senderid_msegat'] = $this->language->get('entry_senderid_msegat');
		$data['httpapi_example_msegat'] = $this->language->get('httpapi_example_msegat');
		
		$data['entry_userkey_msg91'] = $this->language->get('entry_userkey_msg91');
		$data['entry_route_msg91'] = $this->language->get('entry_route_msg91');
		$data['entry_httpapi_msg91'] = $this->language->get('entry_httpapi_msg91');
		$data['entry_senderid_msg91'] = $this->language->get('entry_senderid_msg91');
		$data['httpapi_example_msg91'] = $this->language->get('httpapi_example_msg91');
		
		$data['entry_userkey_mysms'] = $this->language->get('entry_userkey_mysms');
		$data['entry_passkey_mysms'] = $this->language->get('entry_passkey_mysms');
		$data['entry_httpapi_mysms'] = $this->language->get('entry_httpapi_mysms');
		$data['entry_senderid_mysms'] = $this->language->get('entry_senderid_mysms');
		$data['httpapi_example_mysms'] = $this->language->get('httpapi_example_mysms');
		
		$data['entry_userkey_nexmo'] = $this->language->get('entry_userkey_nexmo');
		$data['entry_passkey_nexmo'] = $this->language->get('entry_passkey_nexmo');
		$data['entry_httpapi_nexmo'] = $this->language->get('entry_httpapi_nexmo');
		$data['entry_senderid_nexmo'] = $this->language->get('entry_senderid_nexmo');
		$data['httpapi_example_nexmo'] = $this->language->get('httpapi_example_nexmo');
		$data['entry_unicode_nexmo'] = $this->language->get('entry_unicode_nexmo');
		
		$data['entry_userkey_netgsm'] = $this->language->get('entry_userkey_netgsm');
		$data['entry_passkey_netgsm'] = $this->language->get('entry_passkey_netgsm');
		$data['entry_httpapi_netgsm'] = $this->language->get('entry_httpapi_netgsm');
		$data['entry_senderid_netgsm'] = $this->language->get('entry_senderid_netgsm');
		$data['httpapi_example_netgsm'] = $this->language->get('httpapi_example_netgsm');
		
		$data['entry_userkey_oneway'] = $this->language->get('entry_userkey_oneway');
		$data['entry_passkey_oneway'] = $this->language->get('entry_passkey_oneway');
		$data['entry_httpapi_oneway'] = $this->language->get('entry_httpapi_oneway');
		$data['entry_senderid_oneway'] = $this->language->get('entry_senderid_oneway');
		$data['httpapi_example_oneway'] = $this->language->get('httpapi_example_oneway');
		
		$data['entry_userkey_openhouse'] = $this->language->get('entry_userkey_openhouse');
		$data['entry_passkey_openhouse'] = $this->language->get('entry_passkey_openhouse');
		$data['entry_senderid_openhouse'] = $this->language->get('entry_senderid_openhouse');
		
		$data['entry_userkey_redsms'] = $this->language->get('entry_userkey_redsms');
		$data['entry_passkey_redsms'] = $this->language->get('entry_passkey_redsms');
		$data['entry_httpapi_redsms'] = $this->language->get('entry_httpapi_redsms');
		$data['entry_senderid_redsms'] = $this->language->get('entry_senderid_redsms');
		$data['httpapi_example_redsms'] = $this->language->get('httpapi_example_redsms');
		
		$data['entry_userkey_routesms'] = $this->language->get('entry_userkey_routesms');
		$data['entry_passkey_routesms'] = $this->language->get('entry_passkey_routesms');
		$data['entry_httpapi_routesms'] = $this->language->get('entry_httpapi_routesms');
		$data['entry_senderid_routesms'] = $this->language->get('entry_senderid_routesms');
		$data['httpapi_example_routesms'] = $this->language->get('httpapi_example_routesms');
		
		$data['entry_userkey_smsboxcom'] = $this->language->get('entry_userkey_smsboxcom');
		$data['entry_passkey_smsboxcom'] = $this->language->get('entry_passkey_smsboxcom');
		$data['entry_httpapi_smsboxcom'] = $this->language->get('entry_httpapi_smsboxcom');
		$data['entry_apiid_smsboxcom'] = $this->language->get('entry_apiid_smsboxcom');
		$data['entry_senderid_smsboxcom'] = $this->language->get('entry_senderid_smsboxcom');
		$data['httpapi_example_smsboxcom'] = $this->language->get('httpapi_example_smsboxcom');
		
		$data['entry_userkey_smsgatewayhub'] = $this->language->get('entry_userkey_smsgatewayhub');
		$data['entry_passkey_smsgatewayhub'] = $this->language->get('entry_passkey_smsgatewayhub');
		$data['entry_httpapi_smsgatewayhub'] = $this->language->get('entry_httpapi_smsgatewayhub');
		$data['entry_senderid_smsgatewayhub'] = $this->language->get('entry_senderid_smsgatewayhub');
		$data['httpapi_example_smsgatewayhub'] = $this->language->get('httpapi_example_smsgatewayhub');
		$data['entry_term_smsgatewayhub'] = $this->language->get('entry_term_smsgatewayhub');
		$data['text_term_smsgatewayhub'] = $this->language->get('text_term_smsgatewayhub');
		
		$data['entry_userkey_smslane'] = $this->language->get('entry_userkey_smslane');
		$data['entry_passkey_smslane'] = $this->language->get('entry_passkey_smslane');
		$data['entry_httpapi_smslane'] = $this->language->get('entry_httpapi_smslane');
		$data['entry_senderid_smslane'] = $this->language->get('entry_senderid_smslane');
		$data['httpapi_example_smslane'] = $this->language->get('httpapi_example_smslane');
		$data['entry_unicode_smslane'] = $this->language->get('entry_unicode_smslane');
		$data['entry_transactional_smslane'] = $this->language->get('entry_transactional_smslane');
		$data['text_promotional_smslane'] = $this->language->get('text_promotional_smslane');
		$data['text_transactional_smslane'] = $this->language->get('text_transactional_smslane');
		
		$data['entry_userkey_smslaneg'] = $this->language->get('entry_userkey_smslaneg');
		$data['entry_passkey_smslaneg'] = $this->language->get('entry_passkey_smslaneg');
		$data['entry_httpapi_smslaneg'] = $this->language->get('entry_httpapi_smslaneg');
		$data['entry_senderid_smslaneg'] = $this->language->get('entry_senderid_smslaneg');
		$data['httpapi_example_smslaneg'] = $this->language->get('httpapi_example_smslaneg');
		$data['entry_unicode_smslaneg'] = $this->language->get('entry_unicode_smslaneg');
		
		$data['entry_userkey_smsnetgr'] = $this->language->get('entry_userkey_smsnetgr');
		$data['entry_passkey_smsnetgr'] = $this->language->get('entry_passkey_smsnetgr');
		$data['entry_httpapi_smsnetgr'] = $this->language->get('entry_httpapi_smsnetgr');
		$data['entry_apiid_smsnetgr'] = $this->language->get('entry_apiid_smsnetgr');
		$data['entry_senderid_smsnetgr'] = $this->language->get('entry_senderid_smsnetgr');
		$data['httpapi_example_smsnetgr'] = $this->language->get('httpapi_example_smsnetgr');
		$data['entry_unicode_smsnetgr'] = $this->language->get('entry_unicode_smsnetgr');
		
		$data['entry_userkey_topsms'] = $this->language->get('entry_userkey_topsms');
		$data['entry_passkey_topsms'] = $this->language->get('entry_passkey_topsms');
		$data['entry_httpapi_topsms'] = $this->language->get('entry_httpapi_topsms');
		$data['entry_senderid_topsms'] = $this->language->get('entry_senderid_topsms');
		$data['httpapi_example_topsms'] = $this->language->get('httpapi_example_topsms');
		$data['entry_lang_topsms'] = $this->language->get('entry_lang_topsms');
		$data['text_en'] = $this->language->get('text_en');
		$data['text_ar'] = $this->language->get('text_ar');
		
		$data['entry_userkey_velti'] = $this->language->get('entry_userkey_velti');
		$data['entry_passkey_velti'] = $this->language->get('entry_passkey_velti');
		$data['entry_httpapi_velti'] = $this->language->get('entry_httpapi_velti');
		$data['httpapi_example_velti'] = $this->language->get('httpapi_example_velti');
		
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_position'] = $this->language->get('entry_position');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_smslimit'] = $this->language->get('entry_smslimit');
		$data['entry_alert_reg'] = $this->language->get('entry_alert_reg');
		$data['entry_alert_blank'] = $this->language->get('entry_alert_blank');
		$data['entry_alert_order'] = $this->language->get('entry_alert_order');
		$data['entry_alert_changestate'] = $this->language->get('entry_alert_changestate');
		$data['entry_alert_returnstate'] = $this->language->get('entry_alert_returnstate');
		$data['entry_additional_alert'] = $this->language->get('entry_additional_alert');
		$data['entry_alert_sms'] = $this->language->get('entry_alert_sms');
		$data['entry_account_sms'] = $this->language->get('entry_account_sms');
		$data['entry_return_sms'] = $this->language->get('entry_return_sms');
		$data['entry_status_order_alert'] = $this->language->get('entry_status_order_alert');
		$data['entry_status_return_alert'] = $this->language->get('entry_status_return_alert');
		$data['entry_verify_code'] = $this->language->get('entry_verify_code');
		$data['entry_skip_group'] = $this->language->get('entry_skip_group');
		$data['entry_skip_group_help'] = $this->language->get('entry_skip_group_help');
		$data['entry_code_digit'] = $this->language->get('entry_code_digit');
		$data['entry_max_retry'] = $this->language->get('entry_max_retry');
		$data['entry_limit_blank'] = $this->language->get('entry_limit_blank');
		$data['entry_parsing'] = $this->language->get('entry_parsing');
		$data['entry_parsing_return'] = $this->language->get('entry_parsing_return');
		$data['entry_skip_payment_method'] = $this->language->get('entry_skip_payment_method');
		$data['entry_skip_payment_method_help'] = $this->language->get('entry_skip_payment_method_help');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_add_module'] = $this->language->get('button_add_module');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		$this->load->model('localisation/return_status');
		$data['return_statuses'] = $this->model_localisation_return_status->getReturnStatuses();	
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['code_digit'])) {
			$data['error_code_digit'] = $this->error['code_digit'];
		} else {
			$data['error_code_digit'] = '';
		}
		
		if (isset($this->error['message_code_verification'])) {
			$data['error_message_code_verification'] = $this->error['message_code_verification'];
		} else {
			$data['error_message_code_verification'] = '';
		}		
		
		if (isset($this->error['gateway'])) {
			$data['error_gateway'] = $this->error['gateway'];
		} else {
			$data['error_gateway'] = '';
		}
		
		// Zenziva
		if (isset($this->error['userkey'])) {
			$data['error_userkey'] = $this->error['userkey'];
		} else {
			$data['error_userkey'] = '';
		}
		
		if (isset($this->error['passkey'])) {
			$data['error_passkey'] = $this->error['passkey'];
		} else {
			$data['error_passkey'] = '';
		}
		
		if (isset($this->error['httpapi'])) {
			$data['error_httpapi'] = $this->error['httpapi'];
		} else {
			$data['error_httpapi'] = '';
		}
		
		// AMD Telecom
		if (isset($this->error['userkey_amd'])) {
			$data['error_userkey_amd'] = $this->error['userkey_amd'];
		} else {
			$data['error_userkey_amd'] = '';
		}
		
		if (isset($this->error['passkey_amd'])) {
			$data['error_passkey_amd'] = $this->error['passkey_amd'];
		} else {
			$data['error_passkey_amd'] = '';
		}
		
		if (isset($this->error['httpapi_amd'])) {
			$data['error_httpapi_amd'] = $this->error['httpapi_amd'];
		} else {
			$data['error_httpapi_amd'] = '';
		}
		
		if (isset($this->error['senderid_amd'])) {
			$data['error_senderid_amd'] = $this->error['senderid_amd'];
		} else {
			$data['error_senderid_amd'] = '';
		}
		// ---
		
		// Bulk SMS Global
		if (isset($this->error['userkey_smsglobal'])) {
			$data['error_userkey_smsglobal'] = $this->error['userkey_smsglobal'];
		} else {
			$data['error_userkey_smsglobal'] = '';
		}
		
		if (isset($this->error['passkey_smsglobal'])) {
			$data['error_passkey_smsglobal'] = $this->error['passkey_smsglobal'];
		} else {
			$data['error_passkey_smsglobal'] = '';
		}
		
		if (isset($this->error['httpapi_smsglobal'])) {
			$data['error_httpapi_smsglobal'] = $this->error['httpapi_smsglobal'];
		} else {
			$data['error_httpapi_smsglobal'] = '';
		}
		
		if (isset($this->error['senderid_smsglobal'])) {
			$data['error_senderid_smsglobal'] = $this->error['senderid_smsglobal'];
		} else {
			$data['error_senderid_smsglobal'] = '';
		}
		// ---
		
		// Clickatell
		if (isset($this->error['userkey_clickatell'])) {
			$data['error_userkey_clickatell'] = $this->error['userkey_clickatell'];
		} else {
			$data['error_userkey_clickatell'] = '';
		}
		
		if (isset($this->error['passkey_clickatell'])) {
			$data['error_passkey_clickatell'] = $this->error['passkey_clickatell'];
		} else {
			$data['error_passkey_clickatell'] = '';
		}
		
		if (isset($this->error['httpapi_clickatell'])) {
			$data['error_httpapi_clickatell'] = $this->error['httpapi_clickatell'];
		} else {
			$data['error_httpapi_clickatell'] = '';
		}
		
		if (isset($this->error['apiid_clickatell'])) {
			$data['error_apiid_clickatell'] = $this->error['apiid_clickatell'];
		} else {
			$data['error_apiid_clickatell'] = '';
		}
		// ---
		
		// LiveAll
		if (isset($this->error['userkey_liveall'])) {
				$data['error_userkey_liveall'] = $this->error['userkey_liveall'];
			} else {
				$data['error_userkey_liveall'] = '';
			}			
			if (isset($this->error['passkey_liveall'])) {
				$data['error_passkey_liveall'] = $this->error['passkey_liveall'];
			} else {
				$data['error_passkey_liveall'] = '';
			}			
			if (isset($this->error['httpapi_liveall'])) {
				$data['error_httpapi_liveall'] = $this->error['httpapi_liveall'];
			} else {
				$data['error_httpapi_liveall'] = '';
			}			
			if (isset($this->error['senderid_liveall'])) {
				$data['error_senderid_liveall'] = $this->error['senderid_liveall'];
			} else {
				$data['error_senderid_liveall'] = '';
			}
			// ---
		
		// Malath
		if (isset($this->error['userkey_malath'])) {
			$data['error_userkey_malath'] = $this->error['userkey_malath'];
		} else {
			$data['error_userkey_malath'] = '';
		}
		
		if (isset($this->error['passkey_malath'])) {
			$data['error_passkey_malath'] = $this->error['passkey_malath'];
		} else {
			$data['error_passkey_malath'] = '';
		}
		
		if (isset($this->error['httpapi_malath'])) {
			$data['error_httpapi_malath'] = $this->error['httpapi_malath'];
		} else {
			$data['error_httpapi_malath'] = '';
		}
		
		if (isset($this->error['senderid_malath'])) {
			$data['error_senderid_malath'] = $this->error['senderid_malath'];
		} else {
			$data['error_senderid_malath'] = '';
		}
		// ---
		
		// mobily
		if (isset($this->error['userkey_mobily'])) {
			$data['error_userkey_mobily'] = $this->error['userkey_mobily'];
		} else {
			$data['error_userkey_mobily'] = '';
		}
		
		if (isset($this->error['passkey_mobily'])) {
			$data['error_passkey_mobily'] = $this->error['passkey_mobily'];
		} else {
			$data['error_passkey_mobily'] = '';
		}
		
		if (isset($this->error['httpapi_mobily'])) {
			$data['error_httpapi_mobily'] = $this->error['httpapi_mobily'];
		} else {
			$data['error_httpapi_mobily'] = '';
		}
		
		if (isset($this->error['senderid_mobily'])) {
			$data['error_senderid_mobily'] = $this->error['senderid_mobily'];
		} else {
			$data['error_senderid_mobily'] = '';
		}
		// ---
		
		// msegat
		if (isset($this->error['userkey_msegat'])) {
			$data['error_userkey_msegat'] = $this->error['userkey_msegat'];
		} else {
			$data['error_userkey_msegat'] = '';
		}
		
		if (isset($this->error['passkey_msegat'])) {
			$data['error_passkey_msegat'] = $this->error['passkey_msegat'];
		} else {
			$data['error_passkey_msegat'] = '';
		}
		
		if (isset($this->error['httpapi_msegat'])) {
			$data['error_httpapi_msegat'] = $this->error['httpapi_msegat'];
		} else {
			$data['error_httpapi_msegat'] = '';
		}
		
		if (isset($this->error['senderid_msegat'])) {
			$data['error_senderid_msegat'] = $this->error['senderid_msegat'];
		} else {
			$data['error_senderid_msegat'] = '';
		}
		// ---
		
		// msg91
		if (isset($this->error['userkey_msg91'])) {
			$data['error_userkey_msg91'] = $this->error['userkey_msg91'];
		} else {
			$data['error_userkey_msg91'] = '';
		}
		
		if (isset($this->error['route_msg91'])) {
			$data['error_route_msg91'] = $this->error['route_msg91'];
		} else {
			$data['error_route_msg91'] = '';
		}
		
		if (isset($this->error['httpapi_msg91'])) {
			$data['error_httpapi_msg91'] = $this->error['httpapi_msg91'];
		} else {
			$data['error_httpapi_msg91'] = '';
		}
		
		if (isset($this->error['senderid_msg91'])) {
			$data['error_senderid_msg91'] = $this->error['senderid_msg91'];
		} else {
			$data['error_senderid_msg91'] = '';
		}
		// ---
		
		// mVaayoo
		if (isset($this->error['userkey_mvaayoo'])) {
			$data['error_userkey_mvaayoo'] = $this->error['userkey_mvaayoo'];
		} else {
			$data['error_userkey_mvaayoo'] = '';
		}
		
		if (isset($this->error['passkey_mvaayoo'])) {
			$data['error_passkey_mvaayoo'] = $this->error['passkey_mvaayoo'];
		} else {
			$data['error_passkey_mvaayoo'] = '';
		}
		
		if (isset($this->error['httpapi_mvaayoo'])) {
			$data['error_httpapi_mvaayoo'] = $this->error['httpapi_mvaayoo'];
		} else {
			$data['error_httpapi_mvaayoo'] = '';
		}
		
		if (isset($this->error['senderid_mvaayoo'])) {
			$data['error_senderid_mvaayoo'] = $this->error['senderid_mvaayoo'];
		} else {
			$data['error_senderid_mvaayoo'] = '';
		}
		// ---
		
		// mysms
		if (isset($this->error['userkey_mysms'])) {
			$data['error_userkey_mysms'] = $this->error['userkey_mysms'];
		} else {
			$data['error_userkey_mysms'] = '';
		}
		
		if (isset($this->error['passkey_mysms'])) {
			$data['error_passkey_mysms'] = $this->error['passkey_mysms'];
		} else {
			$data['error_passkey_mysms'] = '';
		}
		
		if (isset($this->error['httpapi_mysms'])) {
			$data['error_httpapi_mysms'] = $this->error['httpapi_mysms'];
		} else {
			$data['error_httpapi_mysms'] = '';
		}
		
		if (isset($this->error['senderid_mysms'])) {
			$data['error_senderid_mysms'] = $this->error['senderid_mysms'];
		} else {
			$data['error_senderid_mysms'] = '';
		}
		// ---
		
		// Nexmo
		if (isset($this->error['userkey_nexmo'])) {
			$data['error_userkey_nexmo'] = $this->error['userkey_nexmo'];
		} else {
			$data['error_userkey_nexmo'] = '';
		}
		
		if (isset($this->error['passkey_nexmo'])) {
			$data['error_passkey_nexmo'] = $this->error['passkey_nexmo'];
		} else {
			$data['error_passkey_nexmo'] = '';
		}
		
		if (isset($this->error['httpapi_nexmo'])) {
			$data['error_httpapi_nexmo'] = $this->error['httpapi_nexmo'];
		} else {
			$data['error_httpapi_nexmo'] = '';
		}
		
		if (isset($this->error['senderid_nexmo'])) {
			$data['error_senderid_nexmo'] = $this->error['senderid_nexmo'];
		} else {
			$data['error_senderid_nexmo'] = '';
		}
		// ---
		
		// Netgsm
		if (isset($this->error['userkey_netgsm'])) {
			$data['error_userkey_netgsm'] = $this->error['userkey_netgsm'];
		} else {
			$data['error_userkey_netgsm'] = '';
		}
		
		if (isset($this->error['passkey_netgsm'])) {
			$data['error_passkey_netgsm'] = $this->error['passkey_netgsm'];
		} else {
			$data['error_passkey_netgsm'] = '';
		}
		
		if (isset($this->error['httpapi_netgsm'])) {
			$data['error_httpapi_netgsm'] = $this->error['httpapi_netgsm'];
		} else {
			$data['error_httpapi_netgsm'] = '';
		}
		
		if (isset($this->error['senderid_netgsm'])) {
			$data['error_senderid_netgsm'] = $this->error['senderid_netgsm'];
		} else {
			$data['error_senderid_netgsm'] = '';
		}
		// ---
		
		// One Way SMS
		if (isset($this->error['userkey_oneway'])) {
			$data['error_userkey_oneway'] = $this->error['userkey_oneway'];
		} else {
			$data['error_userkey_oneway'] = '';
		}
		
		if (isset($this->error['passkey_oneway'])) {
			$data['error_passkey_oneway'] = $this->error['passkey_oneway'];
		} else {
			$data['error_passkey_oneway'] = '';
		}
		
		if (isset($this->error['httpapi_oneway'])) {
			$data['error_httpapi_oneway'] = $this->error['httpapi_oneway'];
		} else {
			$data['error_httpapi_oneway'] = '';
		}
		
		if (isset($this->error['senderid_oneway'])) {
			$data['error_senderid_oneway'] = $this->error['senderid_oneway'];
		} else {
			$data['error_senderid_oneway'] = '';
		}
		// ---
		
		// Openhouse
		if (isset($this->error['userkey_openhouse'])) {
			$data['error_userkey_openhouse'] = $this->error['userkey_openhouse'];
		} else {
			$data['error_userkey_openhouse'] = '';
		}
		
		if (isset($this->error['passkey_openhouse'])) {
			$data['error_passkey_openhouse'] = $this->error['passkey_openhouse'];
		} else {
			$data['error_passkey_openhouse'] = '';
		}
		
		if (isset($this->error['senderid_openhouse'])) {
			$data['error_senderid_openhouse'] = $this->error['senderid_openhouse'];
		} else {
			$data['error_senderid_openhouse'] = '';
		}
		// ---
		
		// Redsms
		if (isset($this->error['userkey_redsms'])) {
			$data['error_userkey_redsms'] = $this->error['userkey_redsms'];
		} else {
			$data['error_userkey_redsms'] = '';
		}
		
		if (isset($this->error['passkey_redsms'])) {
			$data['error_passkey_redsms'] = $this->error['passkey_redsms'];
		} else {
			$data['error_passkey_redsms'] = '';
		}
		
		if (isset($this->error['httpapi_redsms'])) {
			$data['error_httpapi_redsms'] = $this->error['httpapi_redsms'];
		} else {
			$data['error_httpapi_redsms'] = '';
		}
		
		if (isset($this->error['senderid_redsms'])) {
			$data['error_senderid_redsms'] = $this->error['senderid_redsms'];
		} else {
			$data['error_senderid_redsms'] = '';
		}
		// ---
		
		// Routesms
		if (isset($this->error['userkey_routesms'])) {
			$data['error_userkey_routesms'] = $this->error['userkey_routesms'];
		} else {
			$data['error_userkey_routesms'] = '';
		}
		
		if (isset($this->error['passkey_routesms'])) {
			$data['error_passkey_routesms'] = $this->error['passkey_routesms'];
		} else {
			$data['error_passkey_routesms'] = '';
		}
		
		if (isset($this->error['httpapi_routesms'])) {
			$data['error_httpapi_routesms'] = $this->error['httpapi_routesms'];
		} else {
			$data['error_httpapi_routesms'] = '';
		}
		
		if (isset($this->error['senderid_routesms'])) {
			$data['error_senderid_routesms'] = $this->error['senderid_routesms'];
		} else {
			$data['error_senderid_routesms'] = '';
		}
		// ---
		
		// smsboxcom
		if (isset($this->error['userkey_smsboxcom'])) {
			$data['error_userkey_smsboxcom'] = $this->error['userkey_smsboxcom'];
		} else {
			$data['error_userkey_smsboxcom'] = '';
		}
		
		if (isset($this->error['passkey_smsboxcom'])) {
			$data['error_passkey_smsboxcom'] = $this->error['passkey_smsboxcom'];
		} else {
			$data['error_passkey_smsboxcom'] = '';
		}
		
		if (isset($this->error['httpapi_smsboxcom'])) {
			$data['error_httpapi_smsboxcom'] = $this->error['httpapi_smsboxcom'];
		} else {
			$data['error_httpapi_smsboxcom'] = '';
		}
		
		if (isset($this->error['apiid_smsboxcom'])) {
			$data['error_apiid_smsboxcom'] = $this->error['senderid_smsboxcom'];
		} else {
			$data['error_apiid_smsboxcom'] = '';
		}
		// ---
		
		// SMS GATEWAYHUB
		if (isset($this->error['userkey_smsgatewayhub'])) {
			$data['error_userkey_smsgatewayhub'] = $this->error['userkey_smsgatewayhub'];
		} else {
			$data['error_userkey_smsgatewayhub'] = '';
		}
		
		if (isset($this->error['passkey_smsgatewayhub'])) {
			$data['error_passkey_smsgatewayhub'] = $this->error['passkey_smsgatewayhub'];
		} else {
			$data['error_passkey_smsgatewayhub'] = '';
		}
		
		if (isset($this->error['httpapi_smsgatewayhub'])) {
			$data['error_httpapi_smsgatewayhub'] = $this->error['httpapi_smsgatewayhub'];
		} else {
			$data['error_httpapi_smsgatewayhub'] = '';
		}
		
		if (isset($this->error['senderid_smsgatewayhub'])) {
			$data['error_senderid_smsgatewayhub'] = $this->error['senderid_smsgatewayhub'];
		} else {
			$data['error_senderid_smsgatewayhub'] = '';
		}
		// ---
		
		// SMS Lane
		if (isset($this->error['userkey_smslane'])) {
			$data['error_userkey_smslane'] = $this->error['userkey_smslane'];
		} else {
			$data['error_userkey_smslane'] = '';
		}
		
		if (isset($this->error['passkey_smslane'])) {
			$data['error_passkey_smslane'] = $this->error['passkey_smslane'];
		} else {
			$data['error_passkey_smslane'] = '';
		}
		
		if (isset($this->error['httpapi_smslane'])) {
			$data['error_httpapi_smslane'] = $this->error['httpapi_smslane'];
		} else {
			$data['error_httpapi_smslane'] = '';
		}
		
		if (isset($this->error['senderid_smslane'])) {
			$data['error_senderid_smslane'] = $this->error['senderid_smslane'];
		} else {
			$data['error_senderid_smslane'] = '';
		}
		// ---
		
		// SMS Lane Global
		if (isset($this->error['userkey_smslaneg'])) {
			$data['error_userkey_smslaneg'] = $this->error['userkey_smslaneg'];
		} else {
			$data['error_userkey_smslaneg'] = '';
		}
		
		if (isset($this->error['passkey_smslaneg'])) {
			$data['error_passkey_smslaneg'] = $this->error['passkey_smslaneg'];
		} else {
			$data['error_passkey_smslaneg'] = '';
		}
		
		if (isset($this->error['httpapi_smslaneg'])) {
			$data['error_httpapi_smslaneg'] = $this->error['httpapi_smslaneg'];
		} else {
			$data['error_httpapi_smslaneg'] = '';
		}
		
		if (isset($this->error['senderid_smslaneg'])) {
			$data['error_senderid_smslaneg'] = $this->error['senderid_smslaneg'];
		} else {
			$data['error_senderid_smslaneg'] = '';
		}
		// ---
		
		// SMS.net.gr
		if (isset($this->error['userkey_smsnetgr'])) {
			$data['error_userkey_smsnetgr'] = $this->error['userkey_smsnetgr'];
		} else {
			$data['error_userkey_smsnetgr'] = '';
		}
		
		if (isset($this->error['passkey_smsnetgr'])) {
			$data['error_passkey_smsnetgr'] = $this->error['passkey_smsnetgr'];
		} else {
			$data['error_passkey_smsnetgr'] = '';
		}
		
		if (isset($this->error['httpapi_smsnetgr'])) {
			$data['error_httpapi_smsnetgr'] = $this->error['httpapi_smsnetgr'];
		} else {
			$data['error_httpapi_smsnetgr'] = '';
		}
		
		if (isset($this->error['apiid_smsnetgr'])) {
			$data['error_apiid_smsnetgr'] = $this->error['apiid_smsnetgr'];
		} else {
			$data['error_apiid_smsnetgr'] = '';
		}
		// ---
		
		// topsms
		if (isset($this->error['userkey_topsms'])) {
			$data['error_userkey_topsms'] = $this->error['userkey_topsms'];
		} else {
			$data['error_userkey_topsms'] = '';
		}
		
		if (isset($this->error['passkey_topsms'])) {
			$data['error_passkey_topsms'] = $this->error['passkey_topsms'];
		} else {
			$data['error_passkey_topsms'] = '';
		}
		
		if (isset($this->error['httpapi_topsms'])) {
			$data['error_httpapi_topsms'] = $this->error['httpapi_topsms'];
		} else {
			$data['error_httpapi_topsms'] = '';
		}
		
		if (isset($this->error['senderid_topsms'])) {
			$data['error_senderid_topsms'] = $this->error['senderid_topsms'];
		} else {
			$data['error_senderid_topsms'] = '';
		}
		// ---
		
		// Velti
		if (isset($this->error['userkey_velti'])) {
			$data['error_userkey_velti'] = $this->error['userkey_velti'];
		} else {
			$data['error_userkey_velti'] = '';
		}
		
		if (isset($this->error['passkey_velti'])) {
			$data['error_passkey_velti'] = $this->error['passkey_velti'];
		} else {
			$data['error_passkey_velti'] = '';
		}
		
		if (isset($this->error['httpapi_velti'])) {
			$data['error_httpapi_velti'] = $this->error['httpapi_velti'];
		} else {
			$data['error_httpapi_velti'] = '';
		}
		// ---
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
      	'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], true),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/jossms', 'token=' . $this->session->data['token'], true),
      		'separator' => ' :: '
   		);
		$data['button_sendsms'] = $this->language->get('button_sendsms');
		$data['action'] = $this->url->link('extension/module/jossms', 'token=' . $this->session->data['token'], true);
		$data['sendsms'] = $this->url->link('extension/module/jossendsms', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'], true);
		
		if (isset($this->request->post['jossms_gateway'])) {
			$data['jossms_gateway'] = $this->request->post['jossms_gateway'];
		} else {
			$data['jossms_gateway'] = $this->config->get('jossms_gateway');
		}
		
		if (isset($this->request->post['jossms_smslimit'])) {
			$data['jossms_smslimit'] = $this->request->post['jossms_smslimit'];
		} else {
			$data['jossms_smslimit'] = $this->config->get('jossms_smslimit');
		}
		
		if (isset($this->request->post['jossms_message_order'])) {
			$data['jossms_message_order'] = $this->request->post['jossms_message_order'];
		} else {
			$data['jossms_message_order'] = $this->config->get('jossms_message_order');
		}
		
		if (isset($this->request->post['jossms_message_reg'])) {
			$data['jossms_message_reg'] = $this->request->post['jossms_message_reg'];
		} else {
			$data['jossms_message_reg'] = $this->config->get('jossms_message_reg');
		}
		
		if (isset($this->request->post['jossms_message_alert'])) {
			$data['jossms_message_alert'] = $this->request->post['jossms_message_alert'];
		} else {
			$data['jossms_message_alert'] = $this->config->get('jossms_message_alert');
		}
		
		foreach ($data['order_statuses'] as $order_status) {
			if (isset($this->request->post['jossms_message_changestate'.$order_status['order_status_id']])) {
				$data['jossms_message_changestate_'.$order_status['order_status_id']] = $this->request->post['jossms_message_changestate_'.$order_status['order_status_id']];
			} else {
				$data['jossms_message_changestate_'.$order_status['order_status_id']] = $this->config->get('jossms_message_changestate_'.$order_status['order_status_id']);
			}
		}
		
		foreach ($data['return_statuses'] as $return_status) {
			if (isset($this->request->post['jossms_message_returnstate'.$return_status['return_status_id']])) {
				$data['jossms_message_returnstate_'.$return_status['return_status_id']] = $this->request->post['jossms_message_returnstate_'.$return_status['return_status_id']];
			} else {
				$data['jossms_message_returnstate_'.$return_status['return_status_id']] = $this->config->get('jossms_message_returnstate_'.$return_status['return_status_id']);
			}
		}
		
		if (isset($this->request->post['jossms_config_alert_sms'])) {
			$data['jossms_config_alert_sms'] = $this->request->post['jossms_config_alert_sms'];
		} else {
			$data['jossms_config_alert_sms'] = $this->config->get('jossms_config_alert_sms');
		}
		
		if (isset($this->request->post['jossms_config_account_sms'])) {
			$data['jossms_config_account_sms'] = $this->request->post['jossms_config_account_sms'];
		} else {
			$data['jossms_config_account_sms'] = $this->config->get('jossms_config_account_sms');
		}
		
		if (isset($this->request->post['jossms_config_return_sms'])) {
			$data['jossms_config_return_sms'] = $this->request->post['jossms_config_return_sms'];
		} else {
			$data['jossms_config_return_sms'] = $this->config->get('jossms_config_return_sms');
		}
		
		if (isset($this->request->post['jossms_verify'])) {
			$data['jossms_verify'] = $this->request->post['jossms_verify'];
		} else {
			$data['jossms_verify'] = $this->config->get('jossms_verify');
		}
		
		if (isset($this->request->post['jossms_order_verify'])) {
			$data['jossms_order_verify'] = $this->request->post['jossms_order_verify'];
		} else {
			$data['jossms_order_verify'] = $this->config->get('jossms_order_verify');
		}
		
		if (isset($this->request->post['jossms_register_verify'])) {
			$data['jossms_register_verify'] = $this->request->post['jossms_register_verify'];
		} else {
			$data['jossms_register_verify'] = $this->config->get('jossms_register_verify');
		}
		
		if (isset($this->request->post['jossms_forgotten_verify'])) {
			$data['jossms_forgotten_verify'] = $this->request->post['jossms_forgotten_verify'];
		} else {
			$data['jossms_forgotten_verify'] = $this->config->get('jossms_forgotten_verify');
		}
		
		if (isset($this->request->post['jossms_code_digit'])) {
			$data['jossms_code_digit'] = $this->request->post['jossms_code_digit'];
		} else {
			$data['jossms_code_digit'] = $this->config->get('jossms_code_digit');
		}
		
		if (isset($this->request->post['jossms_max_retry'])) {
			$data['jossms_max_retry'] = $this->request->post['jossms_max_retry'];
		} else {
			$data['jossms_max_retry'] = $this->config->get('jossms_max_retry');
		}
		
		if (isset($this->request->post['jossms_message_code_verification'])) {
			$data['jossms_message_code_verification'] = $this->request->post['jossms_message_code_verification'];
		} else {
			$data['jossms_message_code_verification'] = $this->config->get('jossms_message_code_verification');
		}
		
		if (isset($this->request->post['jossms_skip_group_id'])) {
			$data['jossms_skip_group_id'] = $this->request->post['jossms_skip_group_id'];
		} elseif ($this->config->get('jossms_skip_group_id')) {
			$data['jossms_skip_group_id'] = $this->config->get('jossms_skip_group_id');
		} else {
			$data['jossms_skip_group_id'] = array(0);
		}
		
		if (isset($this->request->post['jossms_skip_payment_method'])) {
			$data['jossms_skip_payment_method'] = $this->request->post['jossms_skip_payment_method'];
		} elseif ($this->config->get('jossms_skip_payment_method')) {
			$data['jossms_skip_payment_method'] = $this->config->get('jossms_skip_payment_method');
		} else {
			$data['jossms_skip_payment_method'] = array('none');
		}
		
		$model_file  = DIR_APPLICATION . 'model/customer/customer_group.php';
		if (file_exists($model_file)) {
			$this->load->model('customer/customer_group');
			$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups(0);
		} else {
			$this->load->model('sale/customer_group');
			$data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups(0);
		}
		
		$this->load->model('extension/extension');
		$payments = $this->model_extension_extension->getInstalled('payment');
		$payments_files = glob(DIR_APPLICATION . 'controller/extension/payment/*.php');
		
		if ($payments_files) {
			foreach ($payments_files as $file) {
				$payment = basename($file, '.php');
				$this->load->language('extension/payment/' . $payment);
				if (in_array($payment, $payments)) {
					$data['payment_methods'][] = array(
						'title' => $this->language->get('heading_title'),
						'code' => $payment
					);
				}
			}
		}
		
		// Zenziva
		if (isset($this->request->post['jossms_userkey'])) {
			$data['jossms_userkey'] = $this->request->post['jossms_userkey'];
		} else {
			$data['jossms_userkey'] = $this->config->get('jossms_userkey');
		}
		
		if (isset($this->request->post['jossms_passkey'])) {
			$data['jossms_passkey'] = $this->request->post['jossms_passkey'];
		} else {
			$data['jossms_passkey'] = $this->config->get('jossms_passkey');
		}
		
		if (isset($this->request->post['jossms_httpapi'])) {
			$data['jossms_httpapi'] = $this->request->post['jossms_httpapi'];
		} else {
			$data['jossms_httpapi'] = $this->config->get('jossms_httpapi');
		}
		
		// AMD Telecom
		if (isset($this->request->post['jossms_userkey_amd'])) {
			$data['jossms_userkey_amd'] = $this->request->post['jossms_userkey_amd'];
		} else {
			$data['jossms_userkey_amd'] = $this->config->get('jossms_userkey_amd');
		}
		
		if (isset($this->request->post['jossms_passkey_amd'])) {
			$data['jossms_passkey_amd'] = $this->request->post['jossms_passkey_amd'];
		} else {
			$data['jossms_passkey_amd'] = $this->config->get('jossms_passkey_amd');
		}
		
		if (isset($this->request->post['jossms_httpapi_amd'])) {
			$data['jossms_httpapi_amd'] = $this->request->post['jossms_httpapi_amd'];
		} else {
			$data['jossms_httpapi_amd'] = $this->config->get('jossms_httpapi_amd');
		}
		
		if (isset($this->request->post['jossms_senderid_amd'])) {
			$data['jossms_senderid_amd'] = $this->request->post['jossms_senderid_amd'];
		} else {
			$data['jossms_senderid_amd'] = $this->config->get('jossms_senderid_amd');
		}
		// ---
		
		// Bulk SMS Global
		if (isset($this->request->post['jossms_userkey_smsglobal'])) {
			$data['jossms_userkey_smsglobal'] = $this->request->post['jossms_userkey_smsglobal'];
		} else {
			$data['jossms_userkey_smsglobal'] = $this->config->get('jossms_userkey_smsglobal');
		}
		
		if (isset($this->request->post['jossms_passkey_smsglobal'])) {
			$data['jossms_passkey_smsglobal'] = $this->request->post['jossms_passkey_smsglobal'];
		} else {
			$data['jossms_passkey_smsglobal'] = $this->config->get('jossms_passkey_smsglobal');
		}
		
		if (isset($this->request->post['jossms_httpapi_smsglobal'])) {
			$data['jossms_httpapi_smsglobal'] = $this->request->post['jossms_httpapi_smsglobal'];
		} else {
			$data['jossms_httpapi_smsglobal'] = $this->config->get('jossms_httpapi_smsglobal');
		}
		
		if (isset($this->request->post['jossms_senderid_smsglobal'])) {
			$data['jossms_senderid_smsglobal'] = $this->request->post['jossms_senderid_smsglobal'];
		} else {
			$data['jossms_senderid_smsglobal'] = $this->config->get('jossms_senderid_smsglobal');
		}
		// ---
		
		// Cilckatell
		if (isset($this->request->post['jossms_userkey_clickatell'])) {
			$data['jossms_userkey_clickatell'] = $this->request->post['jossms_userkey_clickatell'];
		} else {
			$data['jossms_userkey_clickatell'] = $this->config->get('jossms_userkey_clickatell');
		}
		
		if (isset($this->request->post['jossms_passkey_clickatell'])) {
			$data['jossms_passkey_clickatell'] = $this->request->post['jossms_passkey_clickatell'];
		} else {
			$data['jossms_passkey_clickatell'] = $this->config->get('jossms_passkey_clickatell');
		}
		
		if (isset($this->request->post['jossms_httpapi_clickatell'])) {
			$data['jossms_httpapi_clickatell'] = $this->request->post['jossms_httpapi_clickatell'];
		} else {
			$data['jossms_httpapi_clickatell'] = $this->config->get('jossms_httpapi_clickatell');
		}
		
		if (isset($this->request->post['jossms_apiid_clickatell'])) {
			$data['jossms_apiid_clickatell'] = $this->request->post['jossms_apiid_clickatell'];
		} else {
			$data['jossms_apiid_clickatell'] = $this->config->get('jossms_apiid_clickatell');
		}
		
		if (isset($this->request->post['jossms_senderid_clickatell'])) {
			$data['jossms_senderid_clickatell'] = $this->request->post['jossms_senderid_clickatell'];
		} else {
			$data['jossms_senderid_clickatell'] = $this->config->get('jossms_senderid_clickatell');
		}
		
		if (isset($this->request->post['jossms_config_unicode_clickatell'])) {
			$data['jossms_config_unicode_clickatell'] = $this->request->post['jossms_config_unicode_clickatell'];
		} else {
			$data['jossms_config_unicode_clickatell'] = $this->config->get('jossms_config_unicode_clickatell');
		}
		// ---
		
		// LiveAll
		if (isset($this->request->post['jossms_userkey_liveall'])) {     
			$data['jossms_userkey_liveall'] = $this->request->post['jossms_userkey_liveall'];    
		} else {     
			$data['jossms_userkey_liveall'] = $this->config->get('jossms_userkey_liveall');    
		}        
		if (isset($this->request->post['jossms_passkey_liveall'])) {     
			$data['jossms_passkey_liveall'] = $this->request->post['jossms_passkey_liveall'];    
		} else {     
			$data['jossms_passkey_liveall'] = $this->config->get('jossms_passkey_liveall');    
		}        
		if (isset($this->request->post['jossms_httpapi_liveall'])) {     
			$data['jossms_httpapi_liveall'] = $this->request->post['jossms_httpapi_liveall'];    
		} else {     
			$data['jossms_httpapi_liveall'] = $this->config->get('jossms_httpapi_liveall');    
		}        
		if (isset($this->request->post['jossms_senderid_liveall'])) {     
			$data['jossms_senderid_liveall'] = $this->request->post['jossms_senderid_liveall'];    
		} else {     
			$data['jossms_senderid_liveall'] = $this->config->get('jossms_senderid_liveall');    
		}
		// ---
		
		// Malath
		if (isset($this->request->post['jossms_userkey_malath'])) {
			$data['jossms_userkey_malath'] = $this->request->post['jossms_userkey_malath'];
		} else {
			$data['jossms_userkey_malath'] = $this->config->get('jossms_userkey_malath');
		}
		
		if (isset($this->request->post['jossms_passkey_malath'])) {
			$data['jossms_passkey_malath'] = $this->request->post['jossms_passkey_malath'];
		} else {
			$data['jossms_passkey_malath'] = $this->config->get('jossms_passkey_malath');
		}
		
		if (isset($this->request->post['jossms_httpapi_malath'])) {
			$data['jossms_httpapi_malath'] = $this->request->post['jossms_httpapi_malath'];
		} else {
			$data['jossms_httpapi_malath'] = $this->config->get('jossms_httpapi_malath');
		}
		
		if (isset($this->request->post['jossms_senderid_malath'])) {
			$data['jossms_senderid_malath'] = $this->request->post['jossms_senderid_malath'];
		} else {
			$data['jossms_senderid_malath'] = $this->config->get('jossms_senderid_malath');
		}
		
		if (isset($this->request->post['jossms_config_unicode_malath'])) {
			$data['jossms_config_unicode_malath'] = $this->request->post['jossms_config_unicode_malath'];
		} else {
			$data['jossms_config_unicode_malath'] = $this->config->get('jossms_config_unicode_malath');
		}
		// ---
		
		// mobily
		if (isset($this->request->post['jossms_userkey_mobily'])) {
			$data['jossms_userkey_mobily'] = $this->request->post['jossms_userkey_mobily'];
		} else {
			$data['jossms_userkey_mobily'] = $this->config->get('jossms_userkey_mobily');
		}
		
		if (isset($this->request->post['jossms_passkey_mobily'])) {
			$data['jossms_passkey_mobily'] = $this->request->post['jossms_passkey_mobily'];
		} else {
			$data['jossms_passkey_mobily'] = $this->config->get('jossms_passkey_mobily');
		}
		
		if (isset($this->request->post['jossms_httpapi_mobily'])) {
			$data['jossms_httpapi_mobily'] = $this->request->post['jossms_httpapi_mobily'];
		} else {
			$data['jossms_httpapi_mobily'] = $this->config->get('jossms_httpapi_mobily');
		}
		
		if (isset($this->request->post['jossms_senderid_mobily'])) {
			$data['jossms_senderid_mobily'] = $this->request->post['jossms_senderid_mobily'];
		} else {
			$data['jossms_senderid_mobily'] = $this->config->get('jossms_senderid_mobily');
		}
		// ---
		
		// msegat
		if (isset($this->request->post['jossms_userkey_msegat'])) {
			$data['jossms_userkey_msegat'] = $this->request->post['jossms_userkey_msegat'];
		} else {
			$data['jossms_userkey_msegat'] = $this->config->get('jossms_userkey_msegat');
		}
		
		if (isset($this->request->post['jossms_passkey_msegat'])) {
			$data['jossms_passkey_msegat'] = $this->request->post['jossms_passkey_msegat'];
		} else {
			$data['jossms_passkey_msegat'] = $this->config->get('jossms_passkey_msegat');
		}
		
		if (isset($this->request->post['jossms_httpapi_msegat'])) {
			$data['jossms_httpapi_msegat'] = $this->request->post['jossms_httpapi_msegat'];
		} else {
			$data['jossms_httpapi_msegat'] = $this->config->get('jossms_httpapi_msegat');
		}
		
		if (isset($this->request->post['jossms_senderid_msegat'])) {
			$data['jossms_senderid_msegat'] = $this->request->post['jossms_senderid_msegat'];
		} else {
			$data['jossms_senderid_msegat'] = $this->config->get('jossms_senderid_msegat');
		}
		// ---
		
		// msg91
		if (isset($this->request->post['jossms_userkey_msg91'])) {
			$data['jossms_userkey_msg91'] = $this->request->post['jossms_userkey_msg91'];
		} else {
			$data['jossms_userkey_msg91'] = $this->config->get('jossms_userkey_msg91');
		}
		
		if (isset($this->request->post['jossms_route_msg91'])) {
			$data['jossms_route_msg91'] = $this->request->post['jossms_route_msg91'];
		} else {
			$data['jossms_route_msg91'] = $this->config->get('jossms_route_msg91');
		}
		
		if (isset($this->request->post['jossms_httpapi_msg91'])) {
			$data['jossms_httpapi_msg91'] = $this->request->post['jossms_httpapi_msg91'];
		} else {
			$data['jossms_httpapi_msg91'] = $this->config->get('jossms_httpapi_msg91');
		}
		
		if (isset($this->request->post['jossms_senderid_msg91'])) {
			$data['jossms_senderid_msg91'] = $this->request->post['jossms_senderid_msg91'];
		} else {
			$data['jossms_senderid_msg91'] = $this->config->get('jossms_senderid_msg91');
		}
		// ---
		
		// mVaayoo
		if (isset($this->request->post['jossms_userkey_mvaayoo'])) {
			$data['jossms_userkey_mvaayoo'] = $this->request->post['jossms_userkey_mvaayoo'];
		} else {
			$data['jossms_userkey_mvaayoo'] = $this->config->get('jossms_userkey_mvaayoo');
		}
		
		if (isset($this->request->post['jossms_passkey_mvaayoo'])) {
			$data['jossms_passkey_mvaayoo'] = $this->request->post['jossms_passkey_mvaayoo'];
		} else {
			$data['jossms_passkey_mvaayoo'] = $this->config->get('jossms_passkey_mvaayoo');
		}
		
		if (isset($this->request->post['jossms_httpapi_mvaayoo'])) {
			$data['jossms_httpapi_mvaayoo'] = $this->request->post['jossms_httpapi_mvaayoo'];
		} else {
			$data['jossms_httpapi_mvaayoo'] = $this->config->get('jossms_httpapi_mvaayoo');
		}
		
		if (isset($this->request->post['jossms_senderid_mvaayoo'])) {
			$data['jossms_senderid_mvaayoo'] = $this->request->post['jossms_senderid_mvaayoo'];
		} else {
			$data['jossms_senderid_mvaayoo'] = $this->config->get('jossms_senderid_mvaayoo');
		}
		// ---
		
		// mysms
		if (isset($this->request->post['jossms_userkey_mysms'])) {
			$data['jossms_userkey_mysms'] = $this->request->post['jossms_userkey_mysms'];
		} else {
			$data['jossms_userkey_mysms'] = $this->config->get('jossms_userkey_mysms');
		}
		
		if (isset($this->request->post['jossms_passkey_mysms'])) {
			$data['jossms_passkey_mysms'] = $this->request->post['jossms_passkey_mysms'];
		} else {
			$data['jossms_passkey_mysms'] = $this->config->get('jossms_passkey_mysms');
		}
		
		if (isset($this->request->post['jossms_httpapi_mysms'])) {
			$data['jossms_httpapi_mysms'] = $this->request->post['jossms_httpapi_mysms'];
		} else {
			$data['jossms_httpapi_mysms'] = $this->config->get('jossms_httpapi_mysms');
		}
		
		if (isset($this->request->post['jossms_senderid_mysms'])) {
			$data['jossms_senderid_mysms'] = $this->request->post['jossms_senderid_mysms'];
		} else {
			$data['jossms_senderid_mysms'] = $this->config->get('jossms_senderid_mysms');
		}
		// ---
		
		// Nexmo
		if (isset($this->request->post['jossms_userkey_nexmo'])) {
			$data['jossms_userkey_nexmo'] = $this->request->post['jossms_userkey_nexmo'];
		} else {
			$data['jossms_userkey_nexmo'] = $this->config->get('jossms_userkey_nexmo');
		}
		
		if (isset($this->request->post['jossms_passkey_nexmo'])) {
			$data['jossms_passkey_nexmo'] = $this->request->post['jossms_passkey_nexmo'];
		} else {
			$data['jossms_passkey_nexmo'] = $this->config->get('jossms_passkey_nexmo');
		}
		
		if (isset($this->request->post['jossms_httpapi_nexmo'])) {
			$data['jossms_httpapi_nexmo'] = $this->request->post['jossms_httpapi_nexmo'];
		} else {
			$data['jossms_httpapi_nexmo'] = $this->config->get('jossms_httpapi_nexmo');
		}
		
		if (isset($this->request->post['jossms_senderid_nexmo'])) {
			$data['jossms_senderid_nexmo'] = $this->request->post['jossms_senderid_nexmo'];
		} else {
			$data['jossms_senderid_nexmo'] = $this->config->get('jossms_senderid_nexmo');
		}
		
		if (isset($this->request->post['jossms_config_unicode_nexmo'])) {
			$data['jossms_config_unicode_nexmo'] = $this->request->post['jossms_config_unicode_nexmo'];
		} else {
			$data['jossms_config_unicode_nexmo'] = $this->config->get('jossms_config_unicode_nexmo');
		}
		// ---
		
		// netgsm
		if (isset($this->request->post['jossms_userkey_netgsm'])) {
			$data['jossms_userkey_netgsm'] = $this->request->post['jossms_userkey_netgsm'];
		} else {
			$data['jossms_userkey_netgsm'] = $this->config->get('jossms_userkey_netgsm');
		}
		
		if (isset($this->request->post['jossms_passkey_netgsm'])) {
			$data['jossms_passkey_netgsm'] = $this->request->post['jossms_passkey_netgsm'];
		} else {
			$data['jossms_passkey_netgsm'] = $this->config->get('jossms_passkey_netgsm');
		}
		
		if (isset($this->request->post['jossms_httpapi_netgsm'])) {
			$data['jossms_httpapi_netgsm'] = $this->request->post['jossms_httpapi_netgsm'];
		} else {
			$data['jossms_httpapi_netgsm'] = $this->config->get('jossms_httpapi_netgsm');
		}
		
		if (isset($this->request->post['jossms_senderid_netgsm'])) {
			$data['jossms_senderid_netgsm'] = $this->request->post['jossms_senderid_netgsm'];
		} else {
			$data['jossms_senderid_netgsm'] = $this->config->get('jossms_senderid_netgsm');
		}
		// ---
		
		// One Way SMS
		if (isset($this->request->post['jossms_userkey_oneway'])) {
			$data['jossms_userkey_oneway'] = $this->request->post['jossms_userkey_oneway'];
		} else {
			$data['jossms_userkey_oneway'] = $this->config->get('jossms_userkey_oneway');
		}
		
		if (isset($this->request->post['jossms_passkey_oneway'])) {
			$data['jossms_passkey_oneway'] = $this->request->post['jossms_passkey_oneway'];
		} else {
			$data['jossms_passkey_oneway'] = $this->config->get('jossms_passkey_oneway');
		}
		
		if (isset($this->request->post['jossms_httpapi_oneway'])) {
			$data['jossms_httpapi_oneway'] = $this->request->post['jossms_httpapi_oneway'];
		} else {
			$data['jossms_httpapi_oneway'] = $this->config->get('jossms_httpapi_oneway');
		}
		
		if (isset($this->request->post['jossms_senderid_oneway'])) {
			$data['jossms_senderid_oneway'] = $this->request->post['jossms_senderid_oneway'];
		} else {
			$data['jossms_senderid_oneway'] = $this->config->get('jossms_senderid_oneway');
		}
		// ---
		
		// Openhouse
		if (isset($this->request->post['jossms_userkey_openhouse'])) {
			$data['jossms_userkey_openhouse'] = $this->request->post['jossms_userkey_openhouse'];
		} else {
			$data['jossms_userkey_openhouse'] = $this->config->get('jossms_userkey_openhouse');
		}
		
		if (isset($this->request->post['jossms_passkey_openhouse'])) {
			$data['jossms_passkey_openhouse'] = $this->request->post['jossms_passkey_openhouse'];
		} else {
			$data['jossms_passkey_openhouse'] = $this->config->get('jossms_passkey_openhouse');
		}
		
		if (isset($this->request->post['jossms_senderid_openhouse'])) {
			$data['jossms_senderid_openhouse'] = $this->request->post['jossms_senderid_openhouse'];
		} else {
			$data['jossms_senderid_openhouse'] = $this->config->get('jossms_senderid_openhouse');
		}
		// ---
		
		// Redsms
		if (isset($this->request->post['jossms_userkey_redsms'])) {
			$data['jossms_userkey_redsms'] = $this->request->post['jossms_userkey_redsms'];
		} else {
			$data['jossms_userkey_redsms'] = $this->config->get('jossms_userkey_redsms');
		}
		
		if (isset($this->request->post['jossms_passkey_redsms'])) {
			$data['jossms_passkey_redsms'] = $this->request->post['jossms_passkey_redsms'];
		} else {
			$data['jossms_passkey_redsms'] = $this->config->get('jossms_passkey_redsms');
		}
		
		if (isset($this->request->post['jossms_httpapi_redsms'])) {
			$data['jossms_httpapi_redsms'] = $this->request->post['jossms_httpapi_redsms'];
		} else {
			$data['jossms_httpapi_redsms'] = $this->config->get('jossms_httpapi_redsms');
		}
		
		if (isset($this->request->post['jossms_senderid_redsms'])) {
			$data['jossms_senderid_redsms'] = $this->request->post['jossms_senderid_redsms'];
		} else {
			$data['jossms_senderid_redsms'] = $this->config->get('jossms_senderid_redsms');
		}
		// ---
		
		// Routesms
		if (isset($this->request->post['jossms_userkey_routesms'])) {
			$data['jossms_userkey_routesms'] = $this->request->post['jossms_userkey_routesms'];
		} else {
			$data['jossms_userkey_routesms'] = $this->config->get('jossms_userkey_routesms');
		}
		
		if (isset($this->request->post['jossms_passkey_routesms'])) {
			$data['jossms_passkey_routesms'] = $this->request->post['jossms_passkey_routesms'];
		} else {
			$data['jossms_passkey_routesms'] = $this->config->get('jossms_passkey_routesms');
		}
		
		if (isset($this->request->post['jossms_httpapi_routesms'])) {
			$data['jossms_httpapi_routesms'] = $this->request->post['jossms_httpapi_routesms'];
		} else {
			$data['jossms_httpapi_routesms'] = $this->config->get('jossms_httpapi_routesms');
		}
		
		if (isset($this->request->post['jossms_senderid_routesms'])) {
			$data['jossms_senderid_routesms'] = $this->request->post['jossms_senderid_routesms'];
		} else {
			$data['jossms_senderid_routesms'] = $this->config->get('jossms_senderid_routesms');
		}
		// ---
		
		// smsboxcom
		if (isset($this->request->post['jossms_userkey_smsboxcom'])) {
			$data['jossms_userkey_smsboxcom'] = $this->request->post['jossms_userkey_smsboxcom'];
		} else {
			$data['jossms_userkey_smsboxcom'] = $this->config->get('jossms_userkey_smsboxcom');
		}
		
		if (isset($this->request->post['jossms_passkey_smsboxcom'])) {
			$data['jossms_passkey_smsboxcom'] = $this->request->post['jossms_passkey_smsboxcom'];
		} else {
			$data['jossms_passkey_smsboxcom'] = $this->config->get('jossms_passkey_smsboxcom');
		}
		
		if (isset($this->request->post['jossms_httpapi_smsboxcom'])) {
			$data['jossms_httpapi_smsboxcom'] = $this->request->post['jossms_httpapi_smsboxcom'];
		} else {
			$data['jossms_httpapi_smsboxcom'] = $this->config->get('jossms_httpapi_smsboxcom');
		}
		
		if (isset($this->request->post['jossms_apiid_smsboxcom'])) {
			$data['jossms_apiid_smsboxcom'] = $this->request->post['jossms_apiid_smsboxcom'];
		} else {
			$data['jossms_apiid_smsboxcom'] = $this->config->get('jossms_apiid_smsboxcom');
		}
		
		if (isset($this->request->post['jossms_senderid_smsboxcom'])) {
			$data['jossms_senderid_smsboxcom'] = $this->request->post['jossms_senderid_smsboxcom'];
		} else {
			$data['jossms_senderid_smsboxcom'] = $this->config->get('jossms_senderid_smsboxcom');
		}
		// ---
		
		// SMS GATEWAYHUB
		if (isset($this->request->post['jossms_userkey_smsgatewayhub'])) {
			$data['jossms_userkey_smsgatewayhub'] = $this->request->post['jossms_userkey_smsgatewayhub'];
		} else {
			$data['jossms_userkey_smsgatewayhub'] = $this->config->get('jossms_userkey_smsgatewayhub');
		}
		
		if (isset($this->request->post['jossms_passkey_smsgatewayhub'])) {
			$data['jossms_passkey_smsgatewayhub'] = $this->request->post['jossms_passkey_smsgatewayhub'];
		} else {
			$data['jossms_passkey_smsgatewayhub'] = $this->config->get('jossms_passkey_smsgatewayhub');
		}
		
		if (isset($this->request->post['jossms_httpapi_smsgatewayhub'])) {
			$data['jossms_httpapi_smsgatewayhub'] = $this->request->post['jossms_httpapi_smsgatewayhub'];
		} else {
			$data['jossms_httpapi_smsgatewayhub'] = $this->config->get('jossms_httpapi_smsgatewayhub');
		}
		
		if (isset($this->request->post['jossms_senderid_smsgatewayhub'])) {
			$data['jossms_senderid_smsgatewayhub'] = $this->request->post['jossms_senderid_smsgatewayhub'];
		} else {
			$data['jossms_senderid_smsgatewayhub'] = $this->config->get('jossms_senderid_smsgatewayhub');
		}
		// ---
		
		// SMS Lane
		if (isset($this->request->post['jossms_userkey_smslane'])) {
			$data['jossms_userkey_smslane'] = $this->request->post['jossms_userkey_smslane'];
		} else {
			$data['jossms_userkey_smslane'] = $this->config->get('jossms_userkey_smslane');
		}
		
		if (isset($this->request->post['jossms_passkey_smslane'])) {
			$data['jossms_passkey_smslane'] = $this->request->post['jossms_passkey_smslane'];
		} else {
			$data['jossms_passkey_smslane'] = $this->config->get('jossms_passkey_smslane');
		}
		
		if (isset($this->request->post['jossms_httpapi_smslane'])) {
			$data['jossms_httpapi_smslane'] = $this->request->post['jossms_httpapi_smslane'];
		} else {
			$data['jossms_httpapi_smslane'] = $this->config->get('jossms_httpapi_smslane');
		}
		
		if (isset($this->request->post['jossms_senderid_smslane'])) {
			$data['jossms_senderid_smslane'] = $this->request->post['jossms_senderid_smslane'];
		} else {
			$data['jossms_senderid_smslane'] = $this->config->get('jossms_senderid_smslane');
		}
		
		if (isset($this->request->post['jossms_config_unicode_smslane'])) {
			$data['jossms_config_unicode_smslane'] = $this->request->post['jossms_config_unicode_smslane'];
		} else {
			$data['jossms_config_unicode_smslane'] = $this->config->get('jossms_config_unicode_smslane');
		}
		
		if (isset($this->request->post['jossms_config_transactional_smslane'])) {
			$data['jossms_config_transactional_smslane'] = $this->request->post['jossms_config_transactional_smslane'];
		} else {
			$data['jossms_config_transactional_smslane'] = $this->config->get('jossms_config_transactional_smslane');
		}
		// ---
		
		// SMS Lane Global
		if (isset($this->request->post['jossms_userkey_smslaneg'])) {
			$data['jossms_userkey_smslaneg'] = $this->request->post['jossms_userkey_smslaneg'];
		} else {
			$data['jossms_userkey_smslaneg'] = $this->config->get('jossms_userkey_smslaneg');
		}
		
		if (isset($this->request->post['jossms_passkey_smslaneg'])) {
			$data['jossms_passkey_smslaneg'] = $this->request->post['jossms_passkey_smslaneg'];
		} else {
			$data['jossms_passkey_smslaneg'] = $this->config->get('jossms_passkey_smslaneg');
		}
		
		if (isset($this->request->post['jossms_httpapi_smslaneg'])) {
			$data['jossms_httpapi_smslaneg'] = $this->request->post['jossms_httpapi_smslaneg'];
		} else {
			$data['jossms_httpapi_smslaneg'] = $this->config->get('jossms_httpapi_smslaneg');
		}
		
		if (isset($this->request->post['jossms_senderid_smslaneg'])) {
			$data['jossms_senderid_smslaneg'] = $this->request->post['jossms_senderid_smslaneg'];
		} else {
			$data['jossms_senderid_smslaneg'] = $this->config->get('jossms_senderid_smslaneg');
		}
		
		if (isset($this->request->post['jossms_config_unicode_smslaneg'])) {
			$data['jossms_config_unicode_smslaneg'] = $this->request->post['jossms_config_unicode_smslaneg'];
		} else {
			$data['jossms_config_unicode_smslaneg'] = $this->config->get('jossms_config_unicode_smslaneg');
		}
		// ---
		
		// SMS.net.gr
		if (isset($this->request->post['jossms_userkey_smsnetgr'])) {
			$data['jossms_userkey_smsnetgr'] = $this->request->post['jossms_userkey_smsnetgr'];
		} else {
			$data['jossms_userkey_smsnetgr'] = $this->config->get('jossms_userkey_smsnetgr');
		}
		
		if (isset($this->request->post['jossms_passkey_smsnetgr'])) {
			$data['jossms_passkey_smsnetgr'] = $this->request->post['jossms_passkey_smsnetgr'];
		} else {
			$data['jossms_passkey_smsnetgr'] = $this->config->get('jossms_passkey_smsnetgr');
		}
		
		if (isset($this->request->post['jossms_httpapi_smsnetgr'])) {
			$data['jossms_httpapi_smsnetgr'] = $this->request->post['jossms_httpapi_smsnetgr'];
		} else {
			$data['jossms_httpapi_smsnetgr'] = $this->config->get('jossms_httpapi_smsnetgr');
		}
		
		if (isset($this->request->post['jossms_apiid_smsnetgr'])) {
			$data['jossms_apiid_smsnetgr'] = $this->request->post['jossms_apiid_smsnetgr'];
		} else {
			$data['jossms_apiid_smsnetgr'] = $this->config->get('jossms_apiid_smsnetgr');
		}
		
		if (isset($this->request->post['jossms_senderid_smsnetgr'])) {
			$data['jossms_senderid_smsnetgr'] = $this->request->post['jossms_senderid_smsnetgr'];
		} else {
			$data['jossms_senderid_smsnetgr'] = $this->config->get('jossms_senderid_smsnetgr');
		}
		
		if (isset($this->request->post['jossms_config_unicode_smsnetgr'])) {
			$data['jossms_config_unicode_smsnetgr'] = $this->request->post['jossms_config_unicode_smsnetgr'];
		} else {
			$data['jossms_config_unicode_smsnetgr'] = $this->config->get('jossms_config_unicode_smsnetgr');
		}
		// ---
		
		// topsms
		if (isset($this->request->post['jossms_userkey_topsms'])) {
			$data['jossms_userkey_topsms'] = $this->request->post['jossms_userkey_topsms'];
		} else {
			$data['jossms_userkey_topsms'] = $this->config->get('jossms_userkey_topsms');
		}
		
		if (isset($this->request->post['jossms_passkey_topsms'])) {
			$data['jossms_passkey_topsms'] = $this->request->post['jossms_passkey_topsms'];
		} else {
			$data['jossms_passkey_topsms'] = $this->config->get('jossms_passkey_topsms');
		}
		
		if (isset($this->request->post['jossms_httpapi_topsms'])) {
			$data['jossms_httpapi_topsms'] = $this->request->post['jossms_httpapi_topsms'];
		} else {
			$data['jossms_httpapi_topsms'] = $this->config->get('jossms_httpapi_topsms');
		}
		
		if (isset($this->request->post['jossms_senderid_topsms'])) {
			$data['jossms_senderid_topsms'] = $this->request->post['jossms_senderid_topsms'];
		} else {
			$data['jossms_senderid_topsms'] = $this->config->get('jossms_senderid_topsms');
		}
		
		if (isset($this->request->post['jossms_config_lang_topsms'])) {
			$data['jossms_config_lang_topsms'] = $this->request->post['jossms_config_lang_topsms'];
		} else {
			$data['jossms_config_lang_topsms'] = $this->config->get('jossms_config_lang_topsms');
		}
		// ---
		
		// Velti
		if (isset($this->request->post['jossms_userkey_velti'])) {
			$data['jossms_userkey_velti'] = $this->request->post['jossms_userkey_velti'];
		} else {
			$data['jossms_userkey_velti'] = $this->config->get('jossms_userkey_velti');
		}
		
		if (isset($this->request->post['jossms_passkey_velti'])) {
			$data['jossms_passkey_velti'] = $this->request->post['jossms_passkey_velti'];
		} else {
			$data['jossms_passkey_velti'] = $this->config->get('jossms_passkey_velti');
		}
		
		if (isset($this->request->post['jossms_httpapi_velti'])) {
			$data['jossms_httpapi_velti'] = $this->request->post['jossms_httpapi_velti'];
		} else {
			$data['jossms_httpapi_velti'] = $this->config->get('jossms_httpapi_velti');
		}
		// ---

		$data['modules'] = array();
		
		if (isset($this->request->post['jossms_module'])) {
			$data['modules'] = $this->request->post['jossms_module'];
		} elseif ($this->config->get('jossms_module')) { 
			$data['modules'] = $this->config->get('jossms_module');
		}	
		
		$this->load->model('design/layout');
		
		$data['layouts'] = $this->model_design_layout->getLayouts();
						
		//$this->template = 'module/jossms.tpl';
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
				
		$this->response->setOutput($this->load->view('extension/module/jossms.tpl', $data));
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/jossms')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ($this->request->post['jossms_verify'] == 1) {
			if (!$this->request->post['jossms_code_digit']) {
				$this->error['code_digit'] = $this->language->get('error_userkey');
			}
			
			if (!$this->request->post['jossms_message_code_verification']) {
				$this->error['message_code_verification'] = $this->language->get('error_userkey');
			}
		}
		
		if (!$this->request->post['jossms_gateway']) {
			$this->error['gateway'] = $this->language->get('error_gateway');
		} else {
			
			if($this->request->post['jossms_gateway'] == "zenziva"){
				// Zenziva
				if (!$this->request->post['jossms_userkey']) {
					$this->error['userkey'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey']) {
					$this->error['passkey'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi']) {
					$this->error['httpapi'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi'] = str_replace(' ', '', $this->request->post['jossms_httpapi']);
				}
			}

			if($this->request->post['jossms_gateway'] == "amd"){	
				// AMD Telecom
				if (!$this->request->post['jossms_userkey_amd']) {
					$this->error['userkey_amd'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_amd']) {
					$this->error['passkey_amd'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_amd']) {
					$this->error['httpapi_amd'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_amd'] = str_replace(' ', '', $this->request->post['jossms_httpapi_amd']);
				}
				
				if (!$this->request->post['jossms_senderid_amd']) {
					$this->error['senderid_amd'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "smsglobal"){	
				// Bulk SMS Global
				if (!$this->request->post['jossms_userkey_smsglobal']) {
					$this->error['userkey_smsglobal'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_smsglobal']) {
					$this->error['passkey_smsglobal'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_smsglobal']) {
					$this->error['httpapi_smsglobal'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_smsglobal'] = str_replace(' ', '', $this->request->post['jossms_httpapi_smsglobal']);
				}
				
				if (!$this->request->post['jossms_senderid_smsglobal']) {
					$this->error['senderid_smsglobal'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "clickatell"){	
				// Clickatell
				if (!$this->request->post['jossms_userkey_clickatell']) {
					$this->error['userkey_clickatell'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_clickatell']) {
					$this->error['passkey_clickatell'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_clickatell']) {
					$this->error['httpapi_clickatell'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_clickatell'] = str_replace(' ', '', $this->request->post['jossms_httpapi_clickatell']);
				}
				
				if (!$this->request->post['jossms_apiid_clickatell']) {
					$this->error['apiid_clickatell'] = $this->language->get('error_apiid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "liveall"){
				// LiveAll
				if (!$this->request->post['jossms_userkey_liveall']) {
					$this->error['userkey_liveall'] = $this->language->get('error_userkey');
				}
				if (!$this->request->post['jossms_passkey_liveall']) {
					$this->error['passkey_liveall'] = $this->language->get('error_passkey');
				}
				if (!$this->request->post['jossms_httpapi_liveall']) {
					$this->error['httpapi_liveall'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_liveall'] = str_replace(' ', '', $this->request->post['jossms_httpapi_liveall']);
				}
				if (!$this->request->post['jossms_senderid_liveall']) {
					$this->error['senderid_liveall'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "malath"){	
				// Malath
				if (!$this->request->post['jossms_userkey_malath']) {
					$this->error['userkey_malath'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_malath']) {
					$this->error['passkey_malath'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_malath']) {
					$this->error['httpapi_malath'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_malath'] = str_replace(' ', '', $this->request->post['jossms_httpapi_malath']);
				}
				
				if (!$this->request->post['jossms_senderid_malath']) {
					$this->error['senderid_malath'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "mobily"){	
				// mobily
				if (!$this->request->post['jossms_userkey_mobily']) {
					$this->error['userkey_mobily'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_mobily']) {
					$this->error['passkey_mobily'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_mobily']) {
					$this->error['httpapi_mobily'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_mobily'] = str_replace(' ', '', $this->request->post['jossms_httpapi_mobily']);
				}
				
				if (!$this->request->post['jossms_senderid_mobily']) {
					$this->error['senderid_mobily'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "msegat"){	
				// msegat
				if (!$this->request->post['jossms_userkey_msegat']) {
					$this->error['userkey_msegat'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_msegat']) {
					$this->error['passkey_msegat'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_msegat']) {
					$this->error['httpapi_msegat'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_msegat'] = str_replace(' ', '', $this->request->post['jossms_httpapi_msegat']);
				}
				
				if (!$this->request->post['jossms_senderid_msegat']) {
					$this->error['senderid_msegat'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "msg91"){	
				// msg91
				if (!$this->request->post['jossms_userkey_msg91']) {
					$this->error['userkey_msg91'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_route_msg91']) {
					$this->error['route_msg91'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_msg91']) {
					$this->error['httpapi_msg91'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_msg91'] = str_replace(' ', '', $this->request->post['jossms_httpapi_msg91']);
				}
				
				if (!$this->request->post['jossms_senderid_msg91']) {
					$this->error['senderid_msg91'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "mvaayoo"){	
				// mVaayoo
				if (!$this->request->post['jossms_userkey_mvaayoo']) {
					$this->error['userkey_mvaayoo'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_mvaayoo']) {
					$this->error['passkey_mvaayoo'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_mvaayoo']) {
					$this->error['httpapi_mvaayoo'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_mvaayoo'] = str_replace(' ', '', $this->request->post['jossms_httpapi_mvaayoo']);
				}
				
				if (!$this->request->post['jossms_senderid_mvaayoo']) {
					$this->error['senderid_mvaayoo'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "mysms"){	
				// mysms
				if (!$this->request->post['jossms_userkey_mysms']) {
					$this->error['userkey_mysms'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_mysms']) {
					$this->error['passkey_mysms'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_mysms']) {
					$this->error['httpapi_mysms'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_mysms'] = str_replace(' ', '', $this->request->post['jossms_httpapi_mysms']);
				}
				
				if (!$this->request->post['jossms_senderid_mysms']) {
					$this->error['senderid_mysms'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "nexmo"){	
				// Nexmo
				if (!$this->request->post['jossms_userkey_nexmo']) {
					$this->error['userkey_nexmo'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_nexmo']) {
					$this->error['passkey_nexmo'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_nexmo']) {
					$this->error['httpapi_nexmo'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_nexmo'] = str_replace(' ', '', $this->request->post['jossms_httpapi_nexmo']);
				}
				
				if (!$this->request->post['jossms_senderid_nexmo']) {
					$this->error['senderid_nexmo'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "netgsm"){	
				// netgsm
				if (!$this->request->post['jossms_userkey_netgsm']) {
					$this->error['userkey_netgsm'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_netgsm']) {
					$this->error['passkey_netgsm'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_netgsm']) {
					$this->error['httpapi_netgsm'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_netgsm'] = str_replace(' ', '', $this->request->post['jossms_httpapi_netgsm']);
				}
				
				if (!$this->request->post['jossms_senderid_netgsm']) {
					$this->error['senderid_netgsm'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "oneway"){	
				// One Way SMS
				if (!$this->request->post['jossms_userkey_oneway']) {
					$this->error['userkey_oneway'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_oneway']) {
					$this->error['passkey_oneway'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_oneway']) {
					$this->error['httpapi_oneway'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_oneway'] = str_replace(' ', '', $this->request->post['jossms_httpapi_oneway']);
				}
				
				if (!$this->request->post['jossms_senderid_oneway']) {
					$this->error['senderid_oneway'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "openhouse"){	
				// Openhouse
				if (!$this->request->post['jossms_userkey_openhouse']) {
					$this->error['userkey_openhouse'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_openhouse']) {
					$this->error['passkey_openhouse'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_senderid_openhouse']) {
					$this->error['senderid_openhouse'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "redsms"){	
				// Redsms
				if (!$this->request->post['jossms_userkey_redsms']) {
					$this->error['userkey_redsms'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_redsms']) {
					$this->error['passkey_redsms'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_redsms']) {
					$this->error['httpapi_redsms'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_redsms'] = str_replace(' ', '', $this->request->post['jossms_httpapi_redsms']);
				}
				
				if (!$this->request->post['jossms_senderid_redsms']) {
					$this->error['senderid_redsms'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "routesms"){	
				// Routesms
				if (!$this->request->post['jossms_userkey_routesms']) {
					$this->error['userkey_routesms'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_routesms']) {
					$this->error['passkey_routesms'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_routesms']) {
					$this->error['httpapi_routesms'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_routesms'] = str_replace(' ', '', $this->request->post['jossms_httpapi_routesms']);
				}
				
				if (!$this->request->post['jossms_senderid_routesms']) {
					$this->error['senderid_routesms'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "smsboxcom"){	
				// smsboxcom
				if (!$this->request->post['jossms_userkey_smsboxcom']) {
					$this->error['userkey_smsboxcom'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_smsboxcom']) {
					$this->error['passkey_smsboxcom'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_smsboxcom']) {
					$this->error['httpapi_smsboxcom'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_smsboxcom'] = str_replace(' ', '', $this->request->post['jossms_httpapi_smsboxcom']);
				}
				
				if (!$this->request->post['jossms_apiid_smsboxcom']) {
					$this->error['apiid_smsboxcom'] = $this->language->get('error_apiid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "smsgatewayhub"){	
				// SMS GATEWAYHUB
				if (!$this->request->post['jossms_userkey_smsgatewayhub']) {
					$this->error['userkey_smsgatewayhub'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_smsgatewayhub']) {
					$this->error['passkey_smsgatewayhub'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_smsgatewayhub']) {
					$this->error['httpapi_smsgatewayhub'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_smsgatewayhub'] = str_replace(' ', '', $this->request->post['jossms_httpapi_smsgatewayhub']);
				}
				
				if (!$this->request->post['jossms_senderid_smsgatewayhub']) {
					$this->error['senderid_smsgatewayhub'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "smslane"){	
				// SMSLane
				if (!$this->request->post['jossms_userkey_smslane']) {
					$this->error['userkey_smslane'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_smslane']) {
					$this->error['passkey_smslane'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_smslane']) {
					$this->error['httpapi_smslane'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_smslane'] = str_replace(' ', '', $this->request->post['jossms_httpapi_smslane']);
				}
				
				if (!$this->request->post['jossms_senderid_smslane']) {
					$this->error['senderid_smslane'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "smslaneg"){	
				// SMSLane Global
				if (!$this->request->post['jossms_userkey_smslaneg']) {
					$this->error['userkey_smslaneg'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_smslaneg']) {
					$this->error['passkey_smslaneg'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_smslaneg']) {
					$this->error['httpapi_smslaneg'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_smslaneg'] = str_replace(' ', '', $this->request->post['jossms_httpapi_smslaneg']);
				}
				
				if (!$this->request->post['jossms_senderid_smslaneg']) {
					$this->error['senderid_smslaneg'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "smsnetgr"){	
				// SMS.net.gr
				if (!$this->request->post['jossms_userkey_smsnetgr']) {
					$this->error['userkey_smsnetgr'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_smsnetgr']) {
					$this->error['passkey_smsnetgr'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_smsnetgr']) {
					$this->error['httpapi_smsnetgr'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_smsnetgr'] = str_replace(' ', '', $this->request->post['jossms_httpapi_smsnetgr']);
				}
				
				if (!$this->request->post['jossms_apiid_smsnetgr']) {
					$this->error['apiid_smsnetgr'] = $this->language->get('error_apiid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "topsms"){	
				// topsms
				if (!$this->request->post['jossms_userkey_topsms']) {
					$this->error['userkey_topsms'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_topsms']) {
					$this->error['passkey_topsms'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_topsms']) {
					$this->error['httpapi_topsms'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_topsms'] = str_replace(' ', '', $this->request->post['jossms_httpapi_topsms']);
				}
				
				if (!$this->request->post['jossms_senderid_topsms']) {
					$this->error['senderid_topsms'] = $this->language->get('error_senderid');
				}
				// ---
			}
			
			if($this->request->post['jossms_gateway'] == "velti"){	
				// Velti
				if (!$this->request->post['jossms_userkey_velti']) {
					$this->error['userkey_velti'] = $this->language->get('error_userkey');
				}
				
				if (!$this->request->post['jossms_passkey_velti']) {
					$this->error['passkey_velti'] = $this->language->get('error_passkey');
				}
				
				if (!$this->request->post['jossms_httpapi_velti']) {
					$this->error['httpapi_velti'] = $this->language->get('error_httpapi');
				} else {
					$this->request->post['jossms_httpapi_velti'] = str_replace(' ', '', $this->request->post['jossms_httpapi_velti']);
				}
				// ---
			}
		}
		
		return !$this->error;
	}
}
?>