<?php
class ControllerExtensionModulePincode extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/pincode');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_setting_setting->editSetting('pincode', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title1');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_insert_pincode'] = $this->language->get('text_insert_pincode');
		$data['text_list_pincode'] = $this->language->get('text_list_pincode');
		$data['text_insert_delivery_options'] = $this->language->get('text_insert_delivery_options');
		$data['text_delhivery_option_list'] = $this->language->get('text_delhivery_option_list');
		$data['text_setting'] = $this->language->get('text_setting');

		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_support'] = $this->language->get('support');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title1'),
			'href' => $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$data['support'] = $this->url->link('catalog/pincode/support', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['pincode_status'])) {
			$data['pincode_status'] = $this->request->post['pincode_status'];
		} else {
			$data['pincode_status'] = $this->config->get('pincode_status');
		}
		$data['insert'] = $this->url->link('catalog/pincode/insert', 'token=' . $this->session->data['token'], 'SSL');
		$data['getlist'] = $this->url->link('catalog/pincode/getlist', 'token=' . $this->session->data['token'], 'SSL');
		$data['Insert_delivery'] = $this->url->link('catalog/pincode/Insert_delivery', 'token=' . $this->session->data['token'], 'SSL');
		$data['delivery_getlist'] = $this->url->link('catalog/pincode/delivery_getlist', 'token=' . $this->session->data['token'], 'SSL');
		$data['setting'] = $this->url->link('catalog/pincode/setting', 'token=' . $this->session->data['token'], 'SSL');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/pincode.tpl', $data));
	}
	
	public function install() {
		
       $this->load->model('catalog/pincode');
       $this->model_catalog_pincode->createTable(); 
	   $this->load->model('setting/setting');
       //$this->model_setting_setting->editSetting('pincode', array('pincode_status'=>1));
	   $this->load->model('user/user_group');
	   $this->model_user_user_group->addPermission($this->user->getId(), 'access', 'catalog/pincode');
	   $this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'catalog/pincode');
	  }
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/pincode')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}