<?php

class ControllerExtensionModuleJoseanMatiasPrecoCupom extends Controller {
    private $error = array();

    public function index() {
        $this->language->load('extension/module/joseanmatias_preco_cupom');

        $this->document->setTitle($this->language->get('heading_title_inner'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->model_setting_setting->editSetting('joseanmatias_preco_cupom', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
        }

        $data['heading_title'] = $this->language->get('heading_title_inner');

        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_edit'] = $this->language->get('text_edit');

        $data['entry_text_page'] = $this->language->get('entry_text_page');
        $data['entry_text_list'] = $this->language->get('entry_text_list');
        $data['entry_showlist'] = $this->language->get('entry_showlist');
        $data['entry_outofstock'] = $this->language->get('entry_outofstock');
        $data['entry_status'] = $this->language->get('entry_status');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title_inner'),
            'href' => $this->url->link('extension/module/joseanmatias_preco_cupom', 'token=' . $this->session->data['token'], true)
        );

        $data['action'] = $this->url->link('extension/module/joseanmatias_preco_cupom', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

        if (isset($this->request->post['joseanmatias_preco_cupom_text_list'])) {
            $data['joseanmatias_preco_cupom_text_list'] = $this->request->post['joseanmatias_preco_cupom_text_list'];
        } elseif ($this->config->get('joseanmatias_preco_cupom_text_list')) {
            $data['joseanmatias_preco_cupom_text_list'] = $this->config->get('joseanmatias_preco_cupom_text_list');
        } else {
            $data['joseanmatias_preco_cupom_text_list'] = $this->language->get('parcelamento_text_list_default');
        }

        if (isset($this->request->post['joseanmatias_preco_cupom_text_page'])) {
            $data['joseanmatias_preco_cupom_text_page'] = $this->request->post['joseanmatias_preco_cupom_text_page'];
        } elseif ($this->config->get('joseanmatias_preco_cupom_text_page')) {
            $data['joseanmatias_preco_cupom_text_page'] = $this->config->get('joseanmatias_preco_cupom_text_page');
        } else {
            $data['joseanmatias_preco_cupom_text_page'] = $this->language->get('parcelamento_text_page_default');
        }
        
        if (isset($this->request->post['joseanmatias_preco_cupom_showlist'])) {
            $data['joseanmatias_preco_cupom_showlist'] = $this->request->post['joseanmatias_preco_cupom_showlist'];
        } else {
            $data['joseanmatias_preco_cupom_showlist'] = $this->config->get('joseanmatias_preco_cupom_showlist');
        }

        if (isset($this->request->post['joseanmatias_preco_cupom_outofstock'])) {
            $data['joseanmatias_preco_cupom_outofstock'] = $this->request->post['joseanmatias_preco_cupom_outofstock'];
        } else {
            $data['joseanmatias_preco_cupom_outofstock'] = $this->config->get('joseanmatias_preco_cupom_outofstock');
        }

        if (isset($this->request->post['joseanmatias_preco_cupom_status'])) {
            $data['joseanmatias_preco_cupom_status'] = $this->request->post['joseanmatias_preco_cupom_status'];
        } else {
            $data['joseanmatias_preco_cupom_status'] = $this->config->get('joseanmatias_preco_cupom_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/joseanmatias_preco_cupom', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/joseanmatias_preco_cupom')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}
?>