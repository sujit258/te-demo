<?php
class ControllerExtensionModuleInstagramShopGallery extends Controller
{
    private $data   = array();
    private $module = array();
    private $error  = array();

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->config->load('isenselabs/instagramshopgallery');
        $this->module = $this->config->get('instagramshopgallery');

        $this->load->model('setting/setting');
        $this->load->model('extension/module/instagramshopgallery');
        
        $this->load->model($this->module['path']);

        // === ===

        $this->module['token'] = $this->session->data['token'];
        $this->module['model'] = $this->{$this->module['model']};

        if (isset($this->request->post['store_id'])) {
            $this->module['store_id'] = (int)$this->request->post['store_id'];
        } elseif (isset($this->request->get['store_id'])) {
            $this->module['store_id'] = (int)$this->request->get['store_id'];
        }

        // Module setting
        $setting = $this->model_setting_setting->getSetting($this->module['code'], $this->module['store_id']);
        $this->module['setting'] = array_replace_recursive(
            $this->module['setting'],
            !empty($setting[$this->module['code'] . '_setting']) ? $setting[$this->module['code'] . '_setting'] : array()
        );

        // Template variables
        $this->data['store_id']    = $this->module['store_id'];
        $this->data['setting']     = $this->module['setting'];
        $this->data['token']       = $this->module['token'];
        $this->data['link_ext']    = $this->url->link($this->module['ext_link'], 'token=' . $this->module['token'] . $this->module['ext_type'], 'SSL');
        $this->data['link_module'] = $this->url->link($this->module['path'], 'store_id=' . $this->module['store_id'] . '&token=' . $this->module['token'], 'SSL');
        $this->data['module']      = array(
            'name'  => $this->module['name'],
            'path'  => $this->module['path']
        );

        $language_vars = $this->load->language($this->module['path'], $this->module['name']);
        //$this->data = array_replace_recursive($this->data,$language_vars[$this->module['name']]->all() );
    }

    public function index()
    {
        $this->load->model('setting/store');
        $this->load->model('tool/image');
        $this->load->model('localisation/language');
        $this->load->model('extension/module/instagramshopgallery');

        $this->document->setTitle($this->module['title']);

        $data      = $this->data;
        $languages = $this->model_localisation_language->getLanguages();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            //$this->module['model']->setupEvent();

            if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
                $this->request->post[$this->module['code'] . '_setting']['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
            }
            if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
                $this->request->post[$this->module['code'] . '_setting']['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']), true);
            }

            $post = array_replace_recursive(
                $this->request->post,
                array($this->module['code'] . '_status' => $this->request->post[$this->module['code'] . '_setting']['status'])
            );

            $this->model_setting_setting->editSetting($this->module['code'], $post, $this->module['store_id']);

            $this->session->data['success'] = $data['text_success'];
            $this->response->redirect($data['link_module']);
        }

        // === ===

        $data['heading_title']  = $this->module['title'] . ' ' . $this->module['version'];
        
        $data['heading_title1']  =  model_extension_module_instagramshopgallery;

        $data['breadcrumbs']    = array();
        $data['breadcrumbs'][]  = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/dashboard', 'token=' . $this->module['token'], 'SSL')
        );
        $data['breadcrumbs'][]  = array(
            'text'      => $data['text_modules'],
            'href'      => $data['link_ext']
        );
        $data['breadcrumbs'][]  = array(
            'text'      => $this->module['title'],
            'href'      => $data['link_module']
        );

        $data['error_warning'] = '';
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        }
        $data['success'] = '';
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        }

        // === ===

        $data['action']         = $data['link_module'];
        $data['cancel']         = $data['link_ext'];

        $data['site_url']       = HTTPS_CATALOG;
        $data['module_setting'] = $this->module['code'] . '_setting';

        $store_default = array(
            'store_id' => '0',
            'name'     => $this->config->get('config_name') . $this->language->get('text_default'),
            'url'      => HTTPS_CATALOG
        );
        $data['store']          = $this->module['store_id'] != 0 ? $this->model_setting_store->getStore($this->module['store_id']) : $store_default;
        $data['stores']         = array_merge(
            array(0 => $store_default),
            $this->model_setting_store->getStores()
        );

        $data['languages']  = $languages;
        foreach ($data['languages'] as $key => $value) {
            $data['languages'][$key]['flag_url'] = 'language/'.$data['languages'][$key]['code'].'/'.$data['languages'][$key]['code'].'.png';
        }
        
        $data['no_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        $data['setting']['module']['extra_thumb'] = $data['no_image'];
        if ($data['setting']['module']['extra_image'] && is_file(DIR_IMAGE . $data['setting']['module']['extra_image'])) {
            $data['setting']['module']['extra_thumb'] = $this->model_tool_image->resize($data['setting']['module']['extra_image'], 100, 100);
        }

        $data['setting']['page']['banner_thumb'] = $data['no_image'];
        if ($data['setting']['page']['banner'] && is_file(DIR_IMAGE . $data['setting']['page']['banner'])) {
            $data['setting']['page']['banner_thumb'] = $this->model_tool_image->resize($data['setting']['page']['banner'], 100, 100);
        }

        // Support

        
        $data['licenseDataBase64'] = !empty($this->module['setting']['License']) ? base64_encode(json_encode($this->module['setting']['License'])) : '';
        

        // ===
        
        $data['heading_title']   = $this->language->get('heading_title');
        $data['text_main_setting']   = $this->language->get('text_main_setting');
        $data['text_module']   = $this->language->get('text_module');
        $data['text_page']   = $this->language->get('text_page');
        $data['text_support']   = $this->language->get('text_support');
        $data['text_modules']   = $this->language->get('text_modules');
        $data['text_enabled']   = $this->language->get('text_enabled');
        $data['text_disabled']   = $this->language->get('text_disabled');
        $data['button_cancel']   = $this->language->get('button_cancel');
        $data['text_default']   = $this->language->get('entry_global_status');
        
        $data['text_close']   = $this->language->get('text_close');
        $data['text_save']   = $this->language->get('text_save');
        $data['text_no']   = $this->language->get('text_no');
        $data['text_yes']   = $this->language->get('text_yes');
        $data['text_likes']   = $this->language->get('text_likes');
        $data['entry_title']   = $this->language->get('entry_title');
        $data['entry_status']   = $this->language->get('entry_status');
        $data['entry_global_status']   = $this->language->get('entry_global_status');
        $data['entry_hashtag']   = $this->language->get('entry_hashtag');
        $data['entry_visibility']   = $this->language->get('entry_visibility');
        $data['entry_photo_limit']   = $this->language->get('entry_photo_limit');
        $data['entry_extra_image']   = $this->language->get('entry_extra_image');
        $data['entry_extra_link']   = $this->language->get('entry_extra_link');
        $data['entry_banner_image']   = $this->language->get('entry_banner_image');
        $data['entry_banner_link']   = $this->language->get('entry_banner_link');
        $data['entry_navbar']   = $this->language->get('entry_navbar');
        $data['entry_custom_css']   = $this->language->get('entry_custom_css');
        $data['entry_seo_options']   = $this->language->get('entry_seo_options');
        $data['entry_meta_title']   = $this->language->get('entry_meta_title');
        $data['entry_meta_desc']   = $this->language->get('entry_meta_desc');
        $data['entry_meta_keywords']   = $this->language->get('entry_meta_keywords');
        $data['entry_url_alias']   = $this->language->get('entry_url_alias');
        $data['entry_ig_user_id']   = $this->language->get('entry_ig_user_id');
        $data['entry_username']   = $this->language->get('entry_username');
        $data['entry_post_time']   = $this->language->get('entry_post_time');
        $data['entry_approve']   = $this->language->get('entry_approve');
        $data['entry_products']   = $this->language->get('entry_products');
        $data['entry_extra_image_help']   = $this->language->get('entry_extra_image_help');
        $data['entry_banner_image_help']   = $this->language->get('entry_banner_image_help');
        $data['entry_extra_link_help']   = $this->language->get('entry_extra_link_help');
        $data['entry_navbar_help']   = $this->language->get('entry_navbar_help');
        $data['text_information']   = $this->language->get('text_information');
        
        $data['text_moderation']   = $this->language->get('text_moderation');
        $data['text_saved_photos']   = $this->language->get('text_saved_photos');
        $data['text_fetch']   = $this->language->get('text_fetch');
        $data['text_fetch_info']   = $this->language->get('text_fetch_info');
        $data['text_load_more']   = $this->language->get('text_load_more');
        $data['text_processing']   = $this->language->get('text_processing');
        $data['text_loading']   = $this->language->get('text_loading');
        $data['text_show_all_photos']   = $this->language->get('text_show_all_photos');
        $data['text_only_approved']   = $this->language->get('text_only_approved');
        $data['text_have_related']   = $this->language->get('text_have_related');
        $data['text_approve_have_related']   = $this->language->get('text_approve_have_related');
        $data['text_aliasing']   = $this->language->get('text_aliasing');
        $data['text_photo_manage']   = $this->language->get('text_photo_manage');
        $data['text_approval_status']   = $this->language->get('text_approval_status');
        $data['text_related_product']   = $this->language->get('text_related_product');
        $data['text_remove_db']   = $this->language->get('text_remove_db');
        $data['text_info_setting']   = $this->language->get('text_info_setting');
        $data['text_info_setting_moderation']   = $this->language->get('text_info_setting_moderation');
        $data['text_info_module']   = $this->language->get('text_info_module');
        $data['text_info_page']   = $this->language->get('text_info_page');
        $data['text_success']   = $this->language->get('text_success');
        $data['text_success_save']   = $this->language->get('text_success_save');
        $data['error_permission']   = $this->language->get('error_permission');
        $data['error_general']   = $this->language->get('error_general');
        $data['error_hashtag_required']   = $this->language->get('error_hashtag_required');
        $data['error_ig_not_respond']   = $this->language->get('error_ig_not_respond');
        $data['error_empty_database']   = $this->language->get('error_empty_database');
        


        $data['tab_main_setting']   = $this->load->view($this->module['path'] .'/tab_setting', $data);
        $data['tab_module_setting'] = $this->load->view($this->module['path'] .'/tab_module', $data);
        $data['tab_page_setting']   = $this->load->view($this->module['path'] .'/tab_page', $data);
        $data['tab_support']        = $this->load->view($this->module['path'] .'/tab_support', $data);

        $data['header']             = $this->load->controller('common/header');
        $data['column_left']        = $this->load->controller('common/column_left');
        $data['footer']             = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view($this->module['path'], $data));
    }

   /**
     * Fetch photo from source: instagram or database
     *
     * @return string   JSON format
     */
    public function fetch()
    {
        $param = array_replace_recursive(array(
            'source'  => 'instagram',
            'hashtag' => 'trueelements',
            'page'    => ''
        ), $this->request->post);
        $response = array(
            'error'  => true,
            'output' => '',
            'param'  => $param
        );
        
        if (!empty($param['hashtag']) && !empty($param['source'])) {
            if ($param['source'] == 'instagram') {
                $response = $this->fetchInstagram($param);
            } elseif ($param['source'] == 'database') {
                $response = $this->fetchDatabase($param);
            }
        } else {
            $response['output'] = $this->data['error_hashtag_required'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
    }

    /**
     * Get photo from Instagram server
     *
     * @return  array
     */
    private function fetchInstagram($param)
    {
        $data           = $this->data;
        $data['photos'] = array();
        $photosFeed     = array();
        $photosMedia    = array();
        $photosPageInfo = array();

        try {
            @$photosFeed = json_decode(file_get_contents('https://www.instagram.com/explore/tags/'.$param['hashtag'].'/?__a=1&max_id=' . $param['page']), true);
        } catch (Exception $e) {
            // silent
        }
        
        
        //https://instagram.com/graphql/query/?query_id=17888483320059182&id=3133781313&first=12&after=$param['page']

        if (!empty($photosFeed)) {
            $photosMedia    = $photosFeed['graphql']['hashtag']['edge_hashtag_to_media'];
            $photosPageInfo = $photosMedia['page_info'];
            $photosItems    = $photosMedia['edges'];
            $photosCount    = count($photosItems);
            $photosMeta     = $this->module['model']->getPhotosSummary();

            for ($i=0; $i < $photosCount; $i++) {
                $caption = isset($photosItems[$i]['node']['edge_media_to_caption']['edges'][0]['node']['text']) ? $photosItems[$i]['node']['edge_media_to_caption']['edges'][0]['node']['text'] : '';
                $caption = str_replace(array('“', '”', "`", "'"), '', $caption);
                
                //if($photosItems[$i]['node']['owner']['id'] == '3133781313'){ 
                
                $data['photos'][$i] = array(
                    'owner'           => $photosItems[$i]['node']['owner']['id'],
                    'image_original'  => $photosItems[$i]['node']['display_url'],
                    'image_large'     => $photosItems[$i]['node']['thumbnail_src'], // 640 x 640
                    'image_thumb'     => $photosItems[$i]['node']['thumbnail_resources'][3]['src'], // 480 x 480
                    'shortcode'       => $photosItems[$i]['node']['shortcode'],
                    'timestamp'       => $photosItems[$i]['node']['taken_at_timestamp'],
                    'approve'         => !empty($photosMeta[$photosItems[$i]['node']['shortcode']]['approve']) ? 'Yes' : 'No',
                    'related_product' => !empty($photosMeta[$photosItems[$i]['node']['shortcode']]['related_product']) ? $photosMeta[$photosItems[$i]['node']['shortcode']]['related_product'] : 0,
                    'remove'          => !empty($photosMeta[$photosItems[$i]['node']['shortcode']]) ? true : false,
                    'caption'         => $caption
                );
                $data['photos'][$i]['data'] = json_encode($data['photos'][$i]);
            }

            $output = $this->load->view('extension/module/instagramshopgallery/tab_photos_items', $data);

        } else {
            $error  = true;
            $output = $this->data['error_ig_not_respond'];
        }

        return array(
            'error'       => isset($error) ? $error : false,
            // 'photos'      => $data['photos'],
            'output'      => $output,
            'page_info'   => $photosPageInfo, // Contain next fetch info
        );
}

    /**
     * Get photo from database
     *
     * @return  array
     */
    private function fetchDatabase($param)
    {
        $data           = $this->data;
        $this->load->model('extension/module/instagramshopgallery');
        
        $data['photos'] = array();
        $param['page']  = !empty($param['page']) ? (int)$param['page'] : 1;

        $totalPhotos    = $this->model_extension_module_instagramshopgallery ->getPhotosTotal();
        $photosItems    = $this->model_extension_module_instagramshopgallery ->getPhotos($param['page']);

        if (!empty($photosItems)) {
            $photosCount = count($photosItems);

            for ($i=0; $i < $photosCount; $i++) {
                $data['photos'][$i] = $photosItems[$i];
                $data['photos'][$i]['approve']  = !empty($photosItems[$i]['approve']) ? 'Yes' : 'No';
                $data['photos'][$i]['remove']   = true;
                $data['photos'][$i]['data']     = json_encode($data['photos'][$i]);
            }

            $output = $this->load->view($this->module['path'] .'/tab_photos_items', $data);

        } else {
            $error  = true;
            $output = $this->data['error_empty_database'];
        }

        return array(
            'error'       => isset($error) ? $error : false,
            // 'photos'      => $data['photos'],
            'output'      => $output ? $output : $this->data['error_ig_not_respond'],
            'page_info'   => array( // Following IG rules to make it consistent
                'has_next_page' => ($photosCount * $param['page']) < $totalPhotos ? true : false,
                'end_cursor'    => $param['page'] + 1,
            )
        );
    }

    /**
     * Show photo meta info, approval option and related products
     *
     * @return string   Template output used inside modal
     */
    public function modalForm()
    {
        $param      = $this->request->post;
        $this->load->model('extension/module/instagramshopgallery');
        
        $data       = $this->data;
        $output     = '<div style="padding:100px;text-align:center">' . $this->data['error_general'] . '</div>';

        if (isset($param['shortcode'])) {
            $photoFeed  = array();

            $data['token']   = $this->module['token'];
            $data['setting'] = $this->model_extension_module_instagramshopgallery->getPhoto($param['shortcode']);
            $data['photo']   = isset($data['setting']['key_id']) ? $data['setting'] : $param;

            try {
                @$photoFeed = json_decode(file_get_contents('https://www.instagram.com/p/' . $param['shortcode'] . '/?__a=1'), true);
            } catch (Exception $e) {
                // silent
            }

            if (!empty($photoFeed)) {
                $photoData = $photoFeed['graphql']['shortcode_media'];
                $data['photo']['username'] = $photoData['owner']['username'];
                $data['photo']['fullname'] = $photoData['owner']['full_name'];

                // Latest photo data
                $data['photo']['like']    = $photoData['edge_media_preview_like']['count'];
                $data['photo']['comment'] = $photoData['edge_media_to_comment']['count'];
                $data['photo']['caption'] = isset($photoData['edge_media_to_caption']['edges'][0]['node']['text']) ? $photoData['edge_media_to_caption']['edges'][0]['node']['text'] : '';
            }

            $data['photo']['date']    = $this->timeElapsedString('@'. $data['photo']['timestamp']);
            $data['photoData']        = json_encode($data['photo']);
            $data['related_products'] = $this->model_extension_module_instagramshopgallery->getRelatedProduct($param['shortcode']);

            $output = $this->load->view($this->module['path'] .'/tab_photos_modal', $data);

        }

        $this->response->setOutput($output);
    }

    // Saving photo detail
    public function modalSave()
    {
        $param = $this->request->post;
        $this->load->model('extension/module/instagramshopgallery');
        
        $param['data'] = json_decode(htmlspecialchars_decode($param['data']), true);
        $response = array(
            'shortcode' => $param['data']['shortcode'],
            'approve'   => $param['approve'] ? 'Yes' : 'No',
            'related_product' => isset($param['related_product']) ? count($param['related_product']) : 0
        );

        $this->model_extension_module_instagramshopgallery->updatePhoto($param);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
    }

    public function remove()
    {
        $param = $this->request->post;
        $this->load->model('extension/module/instagramshopgallery');
        
        $response = array(
            'shortcode'     => $param['shortcode'],
            'remove_photo'  => $param['source'] == 'database' ? true : false
        );

        $this->model_extension_module_instagramshopgallery->deletePhoto($param['shortcode']);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($response));
    }

    private function validateForm()
    {
        if (!$this->user->hasPermission('modify', $this->module['path'])) {
            $this->error['warning'] = $this->data['error_permission'];
        }
        return !$this->error;
    }

    public function install()
    {
        $this->load->model('extension/module/instagramshopgallery');
        
        $this->model_extension_module_instagramshopgallery->install();
    }

    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->load->model('setting/store');
        $this->load->model('extension/module/instagramshopgallery');
        

        $this->model_setting_setting->deleteSetting($this->module['name'], 0);

        $stores = $this->model_setting_store->getStores();
        foreach ($stores as $store) {
            $this->model_setting_setting->deleteSetting($this->module['name'], $store['store_id']);
        }

        $this->model_extension_module_instagramshopgallery->uninstall();
    }

    /**
     * Convert datetime to elapsed time lang
     *
     * @link https://stackoverflow.com/a/18602474
     */
    private function timeElapsedString($datetime, $level = 2)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        $string = array_slice($string, 0, $level);

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
