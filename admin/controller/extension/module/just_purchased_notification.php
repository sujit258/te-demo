<?php
class ControllerExtensionModuleJustPurchasedNotification extends Controller {
	private $error = array();
	private $version = '2.2';	
	
	public function index() {   
		$this->load->language('extension/module/just_purchased_notification');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->document->addScript('view/javascript/jquery/bootstrap-colorpicker/bootstrap-colorpicker.js');
		$this->document->addStyle('view/stylesheet/bootstrap-colorpicker.css');
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('just_purchased_notification', $this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], true));
		}
				
		$data['heading_title'] = $this->language->get('heading_title') . ' ' . $this->version;
		
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_message'] = $this->language->get('tab_message');
		$data['tab_notification_options'] = $this->language->get('tab_notification_options');
		$data['tab_help'] = $this->language->get('tab_help');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_loading'] = $this->language->get('text_loading');
		
		$data['text_hours'] = $this->language->get('text_hours');
		$data['text_seconds'] = $this->language->get('text_seconds');
		$data['text_top'] = $this->language->get('text_top');
		$data['text_bottom'] = $this->language->get('text_bottom');
		$data['text_left'] = $this->language->get('text_left');
		$data['text_center'] = $this->language->get('text_center');
		$data['text_right'] = $this->language->get('text_right');
		
		$data['text_alert_success'] = $this->language->get('text_alert_success');
		$data['text_alert_info'] = $this->language->get('text_alert_info');
		$data['text_alert_warning'] = $this->language->get('text_alert_warning');
		$data['text_alert_danger'] = $this->language->get('text_alert_danger');
		$data['text_alert_custom'] = $this->language->get('text_alert_custom');
		
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_limit'] = $this->language->get('entry_limit');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_shuffle'] = $this->language->get('entry_shuffle');
		$data['entry_cache'] = $this->language->get('entry_cache');
		
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_time_ago'] = $this->language->get('entry_time_ago');
		$data['entry_time_ago_minute'] = $this->language->get('entry_time_ago_minute');
		$data['entry_time_ago_hour'] = $this->language->get('entry_time_ago_hour');
		$data['entry_time_ago_day'] = $this->language->get('entry_time_ago_day');
		$data['entry_hide_older'] = $this->language->get('entry_hide_older');
		
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');		
		$data['entry_alert_type'] = $this->language->get('entry_alert_type');
		$data['entry_background_color'] = $this->language->get('entry_background_color');
		$data['entry_border_color'] = $this->language->get('entry_border_color');
		$data['entry_text_color'] = $this->language->get('entry_text_color');
		$data['entry_link_color'] = $this->language->get('entry_link_color');
		$data['entry_placement'] = $this->language->get('entry_placement');
		$data['entry_allow_dismiss'] = $this->language->get('entry_allow_dismiss');
		$data['entry_show_progressbar'] = $this->language->get('entry_show_progressbar');
		$data['entry_delay'] = $this->language->get('entry_delay');
		$data['entry_timeout'] = $this->language->get('entry_timeout');
		$data['entry_animate_enter'] = $this->language->get('entry_animate_enter');
		$data['entry_animate_exit'] = $this->language->get('entry_animate_exit');
		$data['entry_zindex'] = $this->language->get('entry_zindex');
		
		$data['help_limit'] = $this->language->get('help_limit');
		$data['help_order_status'] = $this->language->get('help_order_status');
		$data['help_shuffle'] = $this->language->get('help_shuffle');
		$data['help_cache'] = $this->language->get('help_cache');
		
		$data['help_message_title'] = $this->language->get('help_message_title');
		$data['help_message'] = $this->language->get('help_message');
		$data['help_time_ago'] = $this->language->get('help_time_ago');
		$data['help_time_ago_minute'] = $this->language->get('help_time_ago_minute');
		$data['help_time_ago_hour'] = $this->language->get('help_time_ago_hour');
		$data['help_time_ago_day'] = $this->language->get('help_time_ago_day');
		$data['help_hide_older'] = $this->language->get('help_hide_older');
		
		$data['help_background_color'] = $this->language->get('help_background_color');
		$data['help_border_color'] = $this->language->get('help_border_color');
		$data['help_text_color'] = $this->language->get('help_text_color');
		$data['help_link_color'] = $this->language->get('help_link_color');
		
		$data['help_placement_from'] = $this->language->get('help_placement_from');
		$data['help_placement_align'] = $this->language->get('help_placement_align');
		$data['help_allow_dismiss'] = $this->language->get('help_allow_dismiss');
		$data['help_show_progressbar'] = $this->language->get('help_show_progressbar');
		$data['help_delay'] = $this->language->get('help_delay');
		$data['help_timeout'] = $this->language->get('help_timeout');
		$data['help_animate_enter'] = $this->language->get('help_animate_enter');
		$data['help_animate_exit'] = $this->language->get('help_animate_exit');
		$data['help_zindex'] = $this->language->get('help_zindex');		
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_clear_cache'] = $this->language->get('button_clear_cache');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
 		if (isset($this->error['limit'])) {
			$data['error_limit'] = $this->error['limit'];
		} else {
			$data['error_limit'] = '';
		}

 		if (isset($this->error['order_status'])) {
			$data['error_order_status'] = $this->error['order_status'];
		} else {
			$data['error_order_status'] = '';
		}	

 		if (isset($this->error['message'])) {
			$data['error_message'] = $this->error['message'];
		} else {
			$data['error_message'] = array();
		}	
		
 		if (isset($this->error['time_ago_minute'])) {
			$data['error_time_ago_minute'] = $this->error['time_ago_minute'];
		} else {
			$data['error_time_ago_minute'] = array();
		}

 		if (isset($this->error['time_ago_hour'])) {
			$data['error_time_ago_hour'] = $this->error['time_ago_hour'];
		} else {
			$data['error_time_ago_hour'] = array();
		}	

 		if (isset($this->error['time_ago_day'])) {
			$data['error_time_ago_day'] = $this->error['time_ago_day'];
		} else {
			$data['error_time_ago_day'] = array();
		}		

 		if (isset($this->error['image'])) {
			$data['error_image'] = $this->error['image'];
		} else {
			$data['error_image'] = '';
		}	
		
 		if (isset($this->error['background_color'])) {
			$data['error_background_color'] = $this->error['background_color'];
		} else {
			$data['error_background_color'] = '';
		}	

 		if (isset($this->error['border_color'])) {
			$data['error_border_color'] = $this->error['border_color'];
		} else {
			$data['error_border_color'] = '';
		}

 		if (isset($this->error['text_color'])) {
			$data['error_text_color'] = $this->error['text_color'];
		} else {
			$data['error_text_color'] = '';
		}	

 		if (isset($this->error['link_color'])) {
			$data['error_link_color'] = $this->error['link_color'];
		} else {
			$data['error_link_color'] = '';
		}		

 		if (isset($this->error['delay'])) {
			$data['error_delay'] = $this->error['delay'];
		} else {
			$data['error_delay'] = '';
		}	

 		if (isset($this->error['timeout'])) {
			$data['error_timeout'] = $this->error['timeout'];
		} else {
			$data['error_timeout'] = '';
		}		
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true)
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/just_purchased_notification', 'token=' . $this->session->data['token'], true)
   		);
		
		$data['action'] = $this->url->link('extension/module/just_purchased_notification', 'token=' . $this->session->data['token'], true);
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], true);

		$this->update_check();
		
		if (isset($this->error['update'])) {
			$data['update'] = $this->error['update'];
		} else {
			$data['update'] = '';
		}		
		
		if (isset($this->request->post['just_purchased_notification_status'])) {
			$data['just_purchased_notification_status'] = $this->request->post['just_purchased_notification_status'];
		} else {
			$data['just_purchased_notification_status'] = $this->config->get('just_purchased_notification_status');
		}
			
		if (isset($this->request->post['just_purchased_notification_limit'])) {
			$data['just_purchased_notification_limit'] = $this->request->post['just_purchased_notification_limit'];
		} else {
			$data['just_purchased_notification_limit'] = $this->config->get('just_purchased_notification_limit');
		}	

		if (isset($this->request->post['just_purchased_notification_order_status'])) {
			$data['just_purchased_notification_order_status'] = $this->request->post['just_purchased_notification_order_status'];
		} elseif ($this->config->get('just_purchased_notification_order_status')) {
			$data['just_purchased_notification_order_status'] = $this->config->get('just_purchased_notification_order_status');
		} else {
			$data['just_purchased_notification_order_status'] = array();
		}	

		if (isset($this->request->post['just_purchased_notification_shuffle'])) {
			$data['just_purchased_notification_shuffle'] = $this->request->post['just_purchased_notification_shuffle'];
		} else {
			$data['just_purchased_notification_shuffle'] = $this->config->get('just_purchased_notification_shuffle');
		}		

		if (isset($this->request->post['just_purchased_notification_cache'])) {
			$data['just_purchased_notification_cache'] = $this->request->post['just_purchased_notification_cache'];
		} else {
			$data['just_purchased_notification_cache'] = $this->config->get('just_purchased_notification_cache');
		}	
		
		if (isset($this->request->post['just_purchased_notification_localisation'])) {
			$data['just_purchased_notification_localisation'] = $this->request->post['just_purchased_notification_localisation'];
		} elseif ($this->config->get('just_purchased_notification_localisation')) {
			$data['just_purchased_notification_localisation'] = $this->config->get('just_purchased_notification_localisation');
		} else {
			$data['just_purchased_notification_localisation'] = array();
		}		

		if (isset($this->request->post['just_purchased_notification_time_ago'])) {
			$data['just_purchased_notification_time_ago'] = $this->request->post['just_purchased_notification_time_ago'];
		} else {
			$data['just_purchased_notification_time_ago'] = $this->config->get('just_purchased_notification_time_ago');
		}		
		
		if (isset($this->request->post['just_purchased_notification_hide_older'])) {
			$data['just_purchased_notification_hide_older'] = $this->request->post['just_purchased_notification_hide_older'];
		} else {
			$data['just_purchased_notification_hide_older'] = $this->config->get('just_purchased_notification_hide_older');
		}

		if (isset($this->request->post['just_purchased_notification_image_width'])) {
			$data['just_purchased_notification_image_width'] = $this->request->post['just_purchased_notification_image_width'];
		} else {
			$data['just_purchased_notification_image_width'] = $this->config->get('just_purchased_notification_image_width');
		}	

		if (isset($this->request->post['just_purchased_notification_image_height'])) {
			$data['just_purchased_notification_image_height'] = $this->request->post['just_purchased_notification_image_height'];
		} else {
			$data['just_purchased_notification_image_height'] = $this->config->get('just_purchased_notification_image_height');
		}

		if (isset($this->request->post['just_purchased_notification_alert_type'])) {
			$data['just_purchased_notification_alert_type'] = $this->request->post['just_purchased_notification_alert_type'];
		} else {
			$data['just_purchased_notification_alert_type'] = $this->config->get('just_purchased_notification_alert_type');
		}

		if (isset($this->request->post['just_purchased_notification_background_color'])) {
			$data['just_purchased_notification_background_color'] = $this->request->post['just_purchased_notification_background_color'];
		} elseif ($this->config->get('just_purchased_notification_background_color')) {
			$data['just_purchased_notification_background_color'] = $this->config->get('just_purchased_notification_background_color');
		} else {
			$data['just_purchased_notification_background_color'] = '#F1F2F0';
		}

		if (isset($this->request->post['just_purchased_notification_border_color'])) {
			$data['just_purchased_notification_border_color'] = $this->request->post['just_purchased_notification_border_color'];
		} elseif ($this->config->get('just_purchased_notification_border_color')) {
			$data['just_purchased_notification_border_color'] = $this->config->get('just_purchased_notification_border_color');
		} else {
			$data['just_purchased_notification_border_color'] = '#DDDDDD';
		}

		if (isset($this->request->post['just_purchased_notification_text_color'])) {
			$data['just_purchased_notification_text_color'] = $this->request->post['just_purchased_notification_text_color'];
		} elseif ($this->config->get('just_purchased_notification_text_color')) {
			$data['just_purchased_notification_text_color'] = $this->config->get('just_purchased_notification_text_color');
		} else {
			$data['just_purchased_notification_text_color'] = '#666666';
		}	

		if (isset($this->request->post['just_purchased_notification_link_color'])) {
			$data['just_purchased_notification_link_color'] = $this->request->post['just_purchased_notification_link_color'];
		} elseif ($this->config->get('just_purchased_notification_link_color')) {
			$data['just_purchased_notification_link_color'] = $this->config->get('just_purchased_notification_link_color');
		} else {
			$data['just_purchased_notification_link_color'] = '#666666';
		}	

		if (isset($this->request->post['just_purchased_notification_placement_from'])) {
			$data['just_purchased_notification_placement_from'] = $this->request->post['just_purchased_notification_placement_from'];
		} else {
			$data['just_purchased_notification_placement_from'] = $this->config->get('just_purchased_notification_placement_from');
		}	

		if (isset($this->request->post['just_purchased_notification_placement_align'])) {
			$data['just_purchased_notification_placement_align'] = $this->request->post['just_purchased_notification_placement_align'];
		} else {
			$data['just_purchased_notification_placement_align'] = $this->config->get('just_purchased_notification_placement_align');
		}

		if (isset($this->request->post['just_purchased_notification_allow_dismiss'])) {
			$data['just_purchased_notification_allow_dismiss'] = $this->request->post['just_purchased_notification_allow_dismiss'];
		} else {
			$data['just_purchased_notification_allow_dismiss'] = $this->config->get('just_purchased_notification_allow_dismiss');
		}

		if (isset($this->request->post['just_purchased_notification_show_progressbar'])) {
			$data['just_purchased_notification_show_progressbar'] = $this->request->post['just_purchased_notification_show_progressbar'];
		} else {
			$data['just_purchased_notification_show_progressbar'] = $this->config->get('just_purchased_notification_show_progressbar');
		}			
		
		if (isset($this->request->post['just_purchased_notification_delay'])) {
			$data['just_purchased_notification_delay'] = $this->request->post['just_purchased_notification_delay'];
		} else {
			$data['just_purchased_notification_delay'] = $this->config->get('just_purchased_notification_delay');
		}

		if (isset($this->request->post['just_purchased_notification_timeout'])) {
			$data['just_purchased_notification_timeout'] = $this->request->post['just_purchased_notification_timeout'];
		} else {
			$data['just_purchased_notification_timeout'] = $this->config->get('just_purchased_notification_timeout');
		}

		if (isset($this->request->post['just_purchased_notification_animate_enter'])) {
			$data['just_purchased_notification_animate_enter'] = $this->request->post['just_purchased_notification_animate_enter'];
		} elseif ($this->config->get('just_purchased_notification_animate_enter')) {
			$data['just_purchased_notification_animate_enter'] = $this->config->get('just_purchased_notification_animate_enter');
		} else {
			$data['just_purchased_notification_animate_enter'] = 'lightSpeedIn';
		}

		if (isset($this->request->post['just_purchased_notification_animate_exit'])) {
			$data['just_purchased_notification_animate_exit'] = $this->request->post['just_purchased_notification_animate_exit'];
		} elseif($this->config->get('just_purchased_notification_animate_exit')) {
			$data['just_purchased_notification_animate_exit'] = $this->config->get('just_purchased_notification_animate_exit');
		} else {
			$data['just_purchased_notification_animate_exit'] = 'lightSpeedOut';
		}

		if (isset($this->request->post['just_purchased_notification_zindex'])) {
			$data['just_purchased_notification_zindex'] = $this->request->post['just_purchased_notification_zindex'];
		} elseif ($this->config->get('just_purchased_notification_zindex')) {
			$data['just_purchased_notification_zindex'] = $this->config->get('just_purchased_notification_zindex');
		} else {
			$data['just_purchased_notification_zindex'] = 1031;
		}		
		
		$data['animations_enter'] = array(
			'bounce',
			'flash',
			'pulse',
			'rubberBand',
			'shake',
			'swing',
			'tada',
			'wobble',
			'jello',
			'bounceIn',
			'bounceInDown',
			'bounceInLeft',
			'bounceInRight',
			'bounceInUp',
			'fadeIn',
			'fadeInDown',
			'fadeInDownBig',
			'fadeInLeft',
			'fadeInLeftBig',
			'fadeInRight',
			'fadeInRightBig',
			'fadeInUp',
			'fadeInUpBig',
			'flip',
			'flipInX',
			'flipInY',
			'lightSpeedIn',
			'rotateIn',
			'rotateInDownLeft',
			'rotateInDownRight',
			'rotateInUpLeft',
			'rotateInUpRight',
			'slideInUp',
			'slideInDown',
			'slideInLeft',
			'slideInRight',
			'zoomIn',
			'zoomInDown',
			'zoomInLeft',
			'zoomInRight',
			'zoomInUp',
			'hinge',
			'rollIn'
		);
		
		$data['animations_exit'] = array(
			'bounce',
			'flash',
			'pulse',
			'rubberBand',
			'shake',
			'swing',
			'tada',
			'wobble',
			'jello',
			'bounceOut',
			'bounceOutDown',
			'bounceOutLeft',
			'bounceOutRight',
			'bounceOutUp',
			'fadeOut',
			'fadeOutDown',
			'fadeOutDownBig',
			'fadeOutLeft',
			'fadeOutLeftBig',
			'fadeOutRight',
			'fadeOutRightBig',
			'fadeOutUp',
			'fadeOutUpBig',
			'flip',
			'flipInX',
			'flipInY',
			'flipOutX',
			'flipOutY',
			'lightSpeedOut',
			'rotateOut',
			'rotateOutDownLeft',
			'rotateOutDownRight',
			'rotateOutUpLeft',
			'rotateOutUpRight',
			'slideOutUp',
			'slideOutDown',
			'slideOutLeft',
			'slideOutRight',
			'zoomOut',
			'zoomOutDown',
			'zoomOutLeft',
			'zoomOutRight',
			'zoomOutUp',
			'hinge',
			'rollOut'
	    );  

		
		
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();		
		
		$data['token'] = $this->session->data['token'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/just_purchased_notification', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/just_purchased_notification')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (utf8_strlen($this->request->post['just_purchased_notification_limit']) < 1 || !is_numeric($this->request->post['just_purchased_notification_limit'])) {
			$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_general'));			
			$this->error['limit'] = $this->language->get('error_limit');
		}
		
		if (!isset($this->request->post['just_purchased_notification_order_status'])) {
			$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_general'));			
			$this->error['order_status'] = $this->language->get('error_order_status');			
		}
		
		foreach ($this->request->post['just_purchased_notification_localisation'] as $language_id => $value){
			if (utf8_strlen($value['message']) < 1) {
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_message'));
        		$this->error['message'][$language_id] = $this->language->get('error_message');
			}
			
			if ($this->request->post['just_purchased_notification_time_ago'] == 1) {
				if (utf8_strlen($value['time_ago_minute']) < 1 || strpos($value['time_ago_minute'], '{time_counter}') === false) {
					$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_message'));
					$this->error['time_ago_minute'][$language_id] = $this->language->get('error_time_ago_minute');
				}
				
				if (utf8_strlen($value['time_ago_hour']) < 1 || strpos($value['time_ago_hour'], '{time_counter}') === false) {
					$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_message'));
					$this->error['time_ago_hour'][$language_id] = $this->language->get('error_time_ago_hour');
				}

				if (utf8_strlen($value['time_ago_hour']) < 1 || strpos($value['time_ago_day'], '{time_counter}') === false) {
					$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_message'));
					$this->error['time_ago_day'][$language_id] = $this->language->get('error_time_ago_day');
				}				
			}
		}
		
		if (utf8_strlen($this->request->post['just_purchased_notification_image_width']) < 1 || !is_numeric($this->request->post['just_purchased_notification_image_width']) || utf8_strlen($this->request->post['just_purchased_notification_image_height']) < 1 || !is_numeric($this->request->post['just_purchased_notification_image_height'])) {
			$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_notification_options'));
			$this->error['image'] = $this->language->get('error_image');
		}
		
		if ($this->request->post['just_purchased_notification_alert_type'] == 'custom') {
			if (utf8_strlen(trim($this->request->post['just_purchased_notification_background_color'])) != 7) {
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_notification_options'));			
				$this->error['background_color'] = $this->language->get('error_color');
			}
			
			if (utf8_strlen(trim($this->request->post['just_purchased_notification_border_color'])) != 7) {
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_notification_options'));			
				$this->error['border_color'] = $this->language->get('error_color');
			}

			if (utf8_strlen(trim($this->request->post['just_purchased_notification_text_color'])) != 7) {
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_notification_options'));			
				$this->error['text_color'] = $this->language->get('error_color');
			}

			if (utf8_strlen(trim($this->request->post['just_purchased_notification_link_color'])) != 7) {
				$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_notification_options'));			
				$this->error['link_color'] = $this->language->get('error_color');
			}			
		}
		
		if (utf8_strlen($this->request->post['just_purchased_notification_delay']) < 1 || !is_numeric($this->request->post['just_purchased_notification_delay'])) {
			$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_notification_options'));			
			$this->error['delay'] = $this->language->get('error_delay');
		}

		if (utf8_strlen($this->request->post['just_purchased_notification_timeout']) < 1 || !is_numeric($this->request->post['just_purchased_notification_timeout'])) {
			$this->error['warning'] = sprintf($this->language->get('error_in_tab'), $this->language->get('tab_notification_options'));			
			$this->error['timeout'] = $this->language->get('error_timeout');
		}		
				
		return !$this->error;
	}
	
	public function clearcache() {
		$this->language->load('extension/module/just_purchased_notification');
		
		$json = array();
		
		$this->cache->delete('just_purchased_notification');
		
		$json['success'] = $this->language->get('text_cache_success');
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}	
	
	private function update_check() {
		$data = 'v=' . $this->version . '&ex=21&e=' . $this->config->get('config_email') . '&ocv=' . VERSION;
        $curl = false;
        
        if (extension_loaded('curl')) {
			$ch = curl_init();
			
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_URL, 'https://www.oc-extensions.com/api/v1/update_check');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'OCX-Adaptor: curl'));
			curl_setopt($ch, CURLOPT_REFERER, HTTP_CATALOG);
			
			if (function_exists('gzinflate')) {
				curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
			}
            
			$result = curl_exec($ch);
			$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			
			if ($http_code == 200) {
				$result = json_decode($result);
				
                if ($result) {
                    $curl = true;
                }
                
                if ( isset($result->version) && ($result->version > $this->version) ) {
				    $this->error['update'] = 'A new version of this extension is available. <a target="_blank" href="' . $result->url . '">Click here</a> to see the Changelog.';
				}
			}
		}
        
        if (!$curl) {
			if (!$fp = @fsockopen('ssl://www.oc-extensions.com', 443, $errno, $errstr, 20)) {
				return false;
			}

			socket_set_timeout($fp, 20);
			
			$headers = array();
			$headers[] = "POST /api/v1/update_check HTTP/1.0";
			$headers[] = "Host: www.oc-extensions.com";
			$headers[] = "Referer: " . HTTP_CATALOG;
			$headers[] = "OCX-Adaptor: socket";
			if (function_exists('gzinflate')) {
				$headers[] = "Accept-encoding: gzip";
			}	
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			$headers[] = "Accept: application/json";
			$headers[] = 'Content-Length: '.strlen($data);
			$request = implode("\r\n", $headers)."\r\n\r\n".$data;
			fwrite($fp, $request);
			$response = $http_code = null;
			$in_headers = $at_start = true;
			$gzip = false;
			
			while (!feof($fp)) {
				$line = fgets($fp, 4096);
				
				if ($at_start) {
					$at_start = false;
					
					if (!preg_match('/HTTP\/(\\d\\.\\d)\\s*(\\d+)\\s*(.*)/', $line, $m)) {
						return false;
					}
					
					$http_code = $m[2];
					continue;
				}
				
				if ($in_headers) {

					if (trim($line) == '') {
						$in_headers = false;
						continue;
					}

					if (!preg_match('/([^:]+):\\s*(.*)/', $line, $m)) {
						continue;
					}
					
					if ( strtolower(trim($m[1])) == 'content-encoding' && trim($m[2]) == 'gzip') {
						$gzip = true;
					}
					
					continue;
				}
				
                $response .= $line;
			}
					
			fclose($fp);
			
			if ($http_code == 200) {
				if ($gzip && function_exists('gzinflate')) {
					$response = substr($response, 10);
					$response = gzinflate($response);
				}
				
				$result = json_decode($response);
				
                if ( isset($result->version) && ($result->version > $this->version) ) {
				    $this->error['update'] = 'A new version of this extension is available. <a target="_blank" href="' . $result->url . '">Click here</a> to see the Changelog.';
				}
			}
		}
	}
}
?>