<?php
class ControllerExtensionModuleProductcod extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->language->load('extension/module/product_cod');

		$this->document->setTitle($this->language->get('heading_title_1'));
		
		$this->load->model('setting/setting');
						
		$data['heading_title'] = $this->language->get('heading_title');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		//echo "hello";
		//print_r($this->request->post);die;
			$this->model_setting_setting->editSetting('product_cod', $this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_content_top'] = $this->language->get('text_content_top');
		$data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$data['text_column_left'] = $this->language->get('text_column_left');
		$data['text_column_right'] = $this->language->get('text_column_right');

		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_limit'] = $this->language->get('entry_limit');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_position'] = $this->language->get('entry_position');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_add_module'] = $this->language->get('button_add_module');
		$data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_1'),
			'href'      => $this->url->link('extension/module/product_cod', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['action'] = $this->url->link('extension/module/product_cod', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['product_cod'])) {
			$data['product_cod'] = $this->request->post['product_cod'];
		} else {
			$data['product_cod'] = $this->config->get('product_cod');
		}	
				
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		
		if (isset($this->request->post['product_cod_product_category'])) {
			$product_categories = $this->request->post['product_category'];
		} else {		
			$product_categories = $this->config->get('product_cod_product_category');
			
		}
		
		$data['product_categories'] = array();
		if (isset($product_categories)){
		foreach ($product_categories as $category_id) {
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$data['product_categories'][] = array(
					'category_id' => $category_info['category_id'],
					'name'        => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
				);
			}
		}}
				
		if (isset($this->request->post['product_cod'])) {
			$products = explode(',', $this->request->post['product_cod']);
		} else {		
			$products = explode(',', $this->config->get('product_cod'));
		}
		
		$data['products'] = array();
		
		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				$data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'name'       => $product_info['name']
				);
			}
		}

		if($this->config->get('cod_product_error_message')){
			$data['cod_product_error_message'] = $this->config->get('cod_product_error_message');
		}
		else{
			$data['cod_product_error_message'] = "You have Products in your cart on which COD SERVICE Not Available.";
		}
			
		$data['modules'] = array();
		
		if (isset($this->request->post['product_cod_module'])) {
			$data['modules'] = $this->request->post['product_cod_module'];
		} elseif ($this->config->get('product_cod_module')) { 
			$data['modules'] = $this->config->get('product_cod_module');
		}		
				
		$this->load->model('design/layout');
		
		$data['layouts'] = $this->model_design_layout->getLayouts();

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');
			$this->response->setOutput($this->load->view('extension/module/product_cod.tpl', $data));
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/product_cod')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>