<?php
class ControllerCatalogRecipe extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/recipe');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/recipe');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/recipe');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/recipe');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_recipe->addRecipe($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
	
			$this->response->redirect($this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . $url, true));
			
			
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/recipe');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/recipe');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_recipe->editRecipe($this->request->get['recipe_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->response->redirect($this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . $url, true));
			
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/recipe');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/recipe');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $recipe_id) {
				$this->model_catalog_recipe->deleteRecipe($recipe_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->response->redirect($this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . $url, true));
			
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		
		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = null;
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'recipe_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];// i am here
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['insert'] = $this->url->link('catalog/recipe/insert', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/recipe/delete', 'token=' . $this->session->data['token'] . $url, true);
	

		$data['recipies'] = array();

			$filter_data = array(
			
				'filter_name'   => $filter_name,
				'filter_status'   => $filter_status,
				'filter_category'   => $filter_category,
				
				'sort'  => $sort,
				'order' => $order,
				'start' => ($page - 1) * $this->config->get('config_limit_admin'),
				'limit' => $this->config->get('config_limit_admin')
			);

		$recipe_total = $this->model_catalog_recipe->getTotalRecipies($filter_data);

		$results = $this->model_catalog_recipe->getRecipies($filter_data);

		foreach ($results as $result) {
			
			$data['recipies'][] = array(
				'recipe_id' 	 => $result['recipe_id'],
				'title'          => $result['title'],
				'category'       => $this->model_catalog_recipe->getCategoryName($result['category_id']),
				'sort_order'     => $result['sort_order'],
				'status'     => $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'edit'           => $this->url->link('catalog/recipe/update', 'token=' . $this->session->data['token'] . '&recipe_id=' . $result['recipe_id'] . $url, true),
				'delete'      => $this->url->link('catalog/recipe/delete', 'token=' . $this->session->data['token'] . '&recipe_id=' . $result['recipe_id'] . $url, true)
			);
			
		}

		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_list'] = $this->language->get('text_list');

		$data['column_title'] = $this->language->get('column_title');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');		

		$data['button_insert'] = $this->language->get('button_insert');		
		$data['button_delete'] = $this->language->get('button_delete');
		
		// Filter
		$data['filter_name'] = $this->language->get('filter_name');	
		$data['filter_status_name'] = $this->language->get('filter_status');	
		$data['filter_category_name'] = $this->language->get('filter_category');	
		$data['filter_recipe_placeholder'] = $this->language->get('filter_recipe_placeholder');	
		$data['filter_category_placeholder'] = $this->language->get('filter_category_placeholder');	
		// $data['filter_status_placeholder'] = $this->language->get('filter_status_placeholder');	
		$data['button_filter'] = $this->language->get('button_filter');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		
		$data['category_list'] = $this->model_catalog_recipe->getCategories();
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_by_title'] = $this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . '&sort=title' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);
		$data['sort_by_sort_order'] = $this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

		$url = '';
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $recipe_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		
				
		$data['list_results'] = sprintf($this->language->get('text_pagination'), ($recipe_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($recipe_total - $this->config->get('config_limit_admin'))) ? $recipe_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $recipe_total, ceil($recipe_total / $this->config->get('config_limit_admin')));
		
		$data['filter_name'] = $filter_name;
		$data['filter_status'] = $filter_status;
		$data['filter_category'] = $filter_category;
		$temp = $this->model_catalog_recipe->getCategory($filter_category);
		$data['category_name'] = $temp['name'];
		

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/recipe_list', $data));
	}
	


	protected function getForm() {
	    $this->load->model('catalog/recipe');
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_form'] = !isset($this->request->get['recipe_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_date_added'] = $this->language->get('entry_date_added');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_directions'] = $this->language->get('entry_directions');
		$data['entry_ingredients'] = $this->language->get('entry_ingredients');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_author'] = $this->language->get('entry_author');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_related'] = $this->language->get('entry_related');
		$data['entry_related_recipe'] = $this->language->get('entry_related_recipe');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_links'] = $this->language->get('tab_links');
	 
    	$data['help_related'] = $this->language->get('help_related');
		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_related_recipe'] = $this->language->get('help_related_recipe');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = array();
		}

		if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
		} else {
			$data['error_description'] = array();
		}
		
		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}
		
		if (isset($this->error['meta_keyword'])) {
			$data['error_meta_keyword'] = $this->error['meta_keyword'];
		} else {
			$data['error_meta_keyword'] = array();
		}
		
		if (isset($this->error['meta_description'])) {
			$data['error_meta_description'] = $this->error['meta_description'];
		} else {
			$data['error_meta_description'] = array();
		}
		
		if (isset($this->error['contest_name'])) {
			$data['error_contest_name'] = $this->error['contest_name'];
		} else {
			$data['error_contest_name'] = array();
		}
		
		if (isset($this->error['category'])) {
			$data['error_category'] = $this->error['category'];
		} else {
			$data['error_category'] = array();
		}
		
		if (isset($this->error['author'])) {
			$data['error_author'] = $this->error['author'];
		} else {
			$data['error_author'] = array();
		}
		
		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),     		
			);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . $url, true),
			
		);

		if (!isset($this->request->get['recipe_id'])) {
			$data['action'] = $this->url->link('catalog/recipe/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('catalog/recipe/update', 'token=' . $this->session->data['token'] . '&recipe_id=' . $this->request->get['recipe_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/recipe', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['recipe_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$recipe_info = $this->model_catalog_recipe->getRecipe($this->request->get['recipe_id']);
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();
        
		if (isset($this->request->post['recipe_description'])) {
			$data['recipe_description'] = $this->request->post['recipe_description'];
		} elseif (isset($this->request->get['recipe_id'])) {
			$data['recipe_description'] = $this->model_catalog_recipe->getRecipeDescriptions($this->request->get['recipe_id']);
		} else {
			$data['recipe_description'] = array();
		}
		
		
		// Start Categories code

		$data['recipe_categories'] = $this->model_catalog_recipe->getCategories();

		if (isset($this->request->post['category_id'])) {
			$data['category_id'] = $this->request->post['category_id'];
		} elseif (!empty($recipe_info)) {
			$data['category_id'] = $recipe_info['category_id'];
		} else {
			$data['category_id'] = 0;
		}
		
		$data['category_list'] = $this->model_catalog_recipe->getCategories();
		
		
			// Filters
		$this->load->model('catalog/filter');

		if (isset($this->request->post['recipe_filter'])) {
			$filters = $this->request->post['recipe_filter'];
		} elseif (isset($this->request->get['recipe_id'])) {
			$filters = $this->model_catalog_recipe->getRecipeFilters($this->request->get['recipe_id']);
		} else {
			$filters = array();
		}

		$data['recipe_filters'] = array();

		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['recipe_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}
		
		
		// $c_id = $this->model_catalog_recipe->getCategoryIdByRecipeId($this->request->get['recipe_id']);
		
		// $data['to_select'] = array();
		
		// $data['to_select'] =  $this->model_catalog_recipe->getCategory($c_id);
		
		if (isset($this->request->post['category_id'])) {
			$data['category_id'] = $this->request->post['category_id'];
		} elseif (!empty($recipe_info)) {
			$data['category_id'] = $recipe_info['category_id'];
		} else {
			$data['category_id'] = '';
		}

		// End Category Code
		
		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($recipe_info)) {
			$data['keyword'] = $recipe_info['keyword'];
		} else {
			$data['keyword'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($recipe_info)) {
			$data['status'] = $recipe_info['status'];
		} else {
			$data['status'] = '';
		}
		
		
		// Related Product		
		if (isset($this->request->post['recipe_related'])) {
			$products = $this->request->post['recipe_related'];
		} elseif (isset($this->request->get['recipe_id'])) {
			$products = $this->model_catalog_recipe->getRecipeRelatedProducts($this->request->get['recipe_id']);
		} else {
			$products = array();
		}

		$data['recipe_relateds'] = array();

		foreach ($products as $product_id) {
			$related_info = $this->model_catalog_recipe->getProduct($product_id);

			if ($related_info) {
				$data['recipe_relateds'][] = array(
					'product_id' => $related_info['product_id'],
					'name'       => $related_info['name']
				);
			}
		}
		
		// Related Recipes		
		if (isset($this->request->post['related_recipe'])) {
			$data['related_recipes'] = $this->request->post['related_recipe'];
		} elseif (isset($this->request->get['recipe_id'])) {
			$data['related_recipes'] = $this->model_catalog_recipe->getRecipesRelatedRecipe($this->request->get['recipe_id']);
		} else {
			$data['related_recipes'] = array();
		}
		
		// Image code
		
		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($recipe_info)) {
			$data['image'] = $recipe_info['image'];
		} else {
			$data['image'] = '';
		}
		
		if (isset($this->request->post['homepage_image'])) {
			$data['homepage_image'] = $this->request->post['homepage_image'];
		} elseif (!empty($recipe_info)) {
			$data['homepage_image'] = $recipe_info['homepage_image'];
		} else {
			$data['homepage_image'] = '';
		}
		
		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($recipe_info) && is_file(DIR_IMAGE . $recipe_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($recipe_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		
		if (isset($this->request->post['homepage_image']) && is_file(DIR_IMAGE . $this->request->post['homepage_image'])) {
			$data['homethumb'] = $this->model_tool_image->resize($this->request->post['homepage_image'], 100, 100);
		} elseif (!empty($recipe_info) && is_file(DIR_IMAGE . $recipe_info['homepage_image'])) {
			$data['homethumb'] = $this->model_tool_image->resize($recipe_info['homepage_image'], 100, 100);
		} else {
			$data['homethumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		// End Image code
		
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/recipe_form', $data));// may need $filter_data	
	}
	
	//Related Recipe autocomplete 
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/recipe');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name'  => $filter_name,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_catalog_recipe->getRecipies($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'recipe_id' => $result['recipe_id'],
					'title'     => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/recipe')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['recipe_description'] as $language_id => $value) {
			if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 64)) {
				$this->error['title'][$language_id] = $this->language->get('error_title');
			}

			if (utf8_strlen($value['description']) < 3) {
				$this->error['description'][$language_id] = $this->language->get('error_description');
			}
			if (utf8_strlen($value['meta_title']) < 3) {
				$this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
			}
			if ($value['is_winner'] == 1) {
			    if (utf8_strlen($value['contest_name']) < 3) {
			        $this->error['contest_name'][$language_id] = "Please enter contest name";
			    }
			}
		}
		
		if (utf8_strlen($this->request->post['keyword']) > 0) {
			if(count(explode(' ', $this->request->post['keyword'])) > 1) {
			    $this->error['keyword'] = "SEO url should not contain double hyphen or whitespace!";
			} else if(strpos($this->request->post['keyword'], '--')) {
			    $this->error['keyword'] = "SEO url should not contain double hyphen or whitespace!";
			} else {
			    $this->load->model('catalog/url_alias');
    			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);
    
    			if ($url_alias_info && isset($this->request->get['recipe_id']) && $url_alias_info['query'] != 'recipe_id=' . $this->request->get['recipe_id']) {
    				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
    			}
    
    			if ($url_alias_info && !isset($this->request->get['recipe_id'])) {
    				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
    			}
			}
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/recipe')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('setting/store');

		foreach ($this->request->post['selected'] as $recipe_id) {
			if ($this->config->get('config_account_id') == $recipe_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}

			if ($this->config->get('config_checkout_id') == $recipe_id) {
				$this->error['warning'] = $this->language->get('error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $recipe_id) {
				$this->error['warning'] = $this->language->get('error_affiliate');
			}

		
		}

		return !$this->error;
	}
}
?>