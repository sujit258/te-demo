<?php
class ControllerCatalogMediaArticles extends Controller {
	private $error = array();

  	public function index() {
		$this->load->language('catalog/media_articles');
		$this->load->model('catalog/media_articles');

		$this->getList();
  	}

  	public function insert() {
    	$this->load->language('catalog/media_articles');
		$this->load->model('catalog/media_articles');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_media_articles->addMediaArticle($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

            $this->response->redirect($this->url->link('catalog/media_articles', 'token=' . $this->session->data['token'] . $url, true));
    	}

    	 $this->getForm();

  	}

  	public function update() {
    	$this->load->language('catalog/media_articles');
		$this->load->model('catalog/media_articles');

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_media_articles->editMediaArticle($this->request->get['media_articles_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->response->redirect($this->url->link('catalog/media_articles', 'token=' . $this->session->data['token'] . $url, true));
		}

    	$this->getForm();
  	}

  	public function delete() {
    	$this->load->language('catalog/media_articles');
		$this->load->model('catalog/media_articles');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $media_articles_id) {
				$this->model_catalog_media_articles->deleteMediaArticle($media_articles_id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->response->redirect($this->url->link('catalog/media_articles', 'token=' . $this->session->data['token'] . $url, true));
		}

    	$this->getList();
  	}

    protected function getList() {
		$this->document->setTitle($this->language->get('page_title'));

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'media_articles_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

	    $data['media_articles'] = array();
		
          
		$data = array(
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$media_article_total = $this->model_catalog_media_articles->getTotalMediaArticles($data);

		$results = $this->model_catalog_media_articles->getMediaArticles($data);

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], true),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/media_articles', 'token=' . $this->session->data['token'], true),
            'separator' => ' / '
        );

        $data['insert'] = $this->url->link('catalog/media_articles/insert', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/media_articles/delete', 'token=' . $this->session->data['token'] . $url, true);

		foreach ($results as $result) {
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/media_articles/update', 'token=' . $this->session->data['token'] . '&media_articles_id=' . $result['media_articles_id'], true)
			);

			$data['media_articles'][] = array(
				'media_articles_id'         	=> $result['media_articles_id'],
				'media_name' 	=> $result['media_name'],
				'sort_order'    => $result['sort_order'],
				'status'     	=> $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'selected'   	=> isset($this->request->post['selected']) && in_array($result['media_articles_id'], $this->request->post['selected']),
				'action'     	=> $action
			);
      		
    	}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['entry_name'] = $this->language->get('entry_name');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_delete'] = $this->language->get('button_delete');

		$data['token'] = $this->session->data['token'];

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['media_name'])) {
			$url .= '&media_name=' . $this->request->get['media_name'];
		}

		$pagination = new Pagination();
		$pagination->total = $media_article_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/media_articles', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		$data['sort'] = $sort;
		$data['order'] = $order;
        $data['results'] = sprintf($this->language->get('text_pagination'), ($media_article_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($media_article_total - $this->config->get('config_limit_admin'))) ? $media_article_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $media_article_total, ceil($media_article_total / $this->config->get('config_limit_admin')));

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/media_articles_list.tpl', $data));
  	}

  	private function getForm() {
		$this->document->setTitle($this->language->get('page_title'));

    	$data['heading_title'] = $this->language->get('heading_title');

    	$data['text_form'] = $this->language->get('text_form');
    	$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_image'] = $this->language->get('entry_image');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
    	$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_url'] = $this->language->get('entry_url');

    	$data['button_save'] = $this->language->get('button_save');
    	$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_remove'] = $this->language->get('button_remove');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

 		if (isset($this->error['media_name'])) {
			$data['error_name'] = $this->error['media_name'];
		}/* else {
			$data['error_name'] = '';
		}
*/
   		if (isset($this->error['content'])) {
			$data['error_description'] = $this->error['content'];
		} else {
			$data['error_description'] = '';
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], true),
			'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/media_articles', 'token=' . $this->session->data['token'] . $url, true),
      		'separator' => ' / '
   		);


		if (!isset($this->request->get['media_articles_id'])) {
			$data['action'] = $this->url->link('catalog/media_articles/insert', 'token=' . $this->session->data['token'], true);
		} else {
			$data['action'] = $this->url->link('catalog/media_articles/update', 'token=' . $this->session->data['token'] . '&media_articles_id=' . $this->request->get['media_articles_id'], true);
		}
		
		$data['cancel'] = $this->url->link('catalog/media_articles', 'token=' . $this->session->data['token'], true);

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['media_articles_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$media_info = $this->model_catalog_media_articles->getMediaArticle($this->request->get['media_articles_id']);
    	}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();


		if (isset($this->request->post['sort_order'])) {
      		$data['sort_order'] = $this->request->post['sort_order'];
    	} elseif (isset($media_info) && !empty($media_info)) {
      		$data['sort_order'] = $media_info['sort_order'];
    	} else {
			$data['sort_order'] = 1;
		}

    	if (isset($this->request->post['status'])) {
      		$data['status'] = $this->request->post['status'];
    	} else if (isset($media_info) && !empty($media_info)) {
			$data['status'] = $media_info['status'];
		} else {
      		$data['status'] = 1;
    	} 
    	
    	if (isset($this->request->post['publish_date'])) {
			$data['publish_date'] = $this->request->post['publish_date'];
		} elseif (!empty($media_info)) {
			$data['publish_date'] = ($media_info['publish_date'] != '0000-00-00') ? $media_info['publish_date'] : '';
		} else {
			$data['publish_date'] = date('Y-m-d');
		}

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif ( isset($media_info) && !empty($media_info)) {
            $data['image'] = $media_info['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['media_name'])) {
            $data['media_name'] = $this->request->post['media_name'];
        } elseif ( isset($media_info) && !empty($media_info)) {
            $data['media_name'] = $media_info['media_name'];
        } else {
            $data['media_name'] = '';
        }

        if (isset($this->request->post['content'])) {
            $data['content'] = $this->request->post['content'];
        } elseif ( isset($media_info) && !empty($media_info)) {
            $data['content'] = $media_info['content'];
        } else {
            $data['content'] = '';
        }


        if (isset($this->request->post['url'])) {
            $data['url'] = $this->request->post['url'];
        } elseif ( isset($media_info) && !empty($media_info)) {
            $data['url'] = $media_info['url'];
        } else {
            $data['url'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($media_info) && is_file(DIR_IMAGE . $media_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($media_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');
        $data['column_left'] = $this->load->controller('common/column_left');

        $this->response->setOutput($this->load->view('catalog/media_articles_form.tpl', $data));
  	}
  	
  	private function validateForm() {
    	if (!$this->user->hasPermission('modify', 'catalog/media_articles')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

		if ((strlen(utf8_decode($this->request->post['media_name'])) < 3) || (strlen(utf8_decode($this->request->post['media_name'])) > 255)) {
			$this->error['media_name']= $this->language->get('error_name');
		}
		if ((strlen(utf8_decode($this->request->post['content'])) < 3)) {
			$this->error['content'] = $this->language->get('error_description');
		}

    	if (!$this->error) {
			return TRUE;
    	} else {
			if (!isset($this->error['warning'])) {
				$this->error['warning'] = $this->language->get('error_required_data');
			}
      		return FALSE;
    	}
  	}

  	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'catalog/media_articles')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

		if (!$this->error) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
  	}
}