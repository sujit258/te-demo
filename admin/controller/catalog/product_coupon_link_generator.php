<?php
class ControllerCatalogProductCouponLinkGenerator extends Controller {
	private $error = array();

	public function index() {
		$this->document->setTitle("Product Coupon Link Generator");
		$this->load->model('catalog/product');
		$this->getForm();
	}
	
	protected function getForm() {
		$data['heading_title'] = "Product Coupon Link Generator";
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "Product Coupon Link Generator",
			'href' => $this->url->link('catalog/product_coupon_link_generator', 'token=' . $this->session->data['token'] . $url, true)
		);
		
		$data['cancel'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, true);

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/product_coupon_link_generator', $data));
	}
}
?>