<?php
class ControllerCatalogDeals extends Controller {
	private $error = array();

	public function index() {
	    $this->load->language('catalog/deals');
		$this->load->model('catalog/deals');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();
	}
	
	protected function getList() {
	    if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}
		
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = '';
		}
		
		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];
		} else {
			$filter_sku = '';
		}
		
		if (isset($this->request->get['filter_homepage'])) {
			$filter_homepage = $this->request->get['filter_homepage'];
		} else {
			$filter_homepage = '';
		}
		
		if (isset($this->request->get['filter_coupon'])) {
			$filter_coupon = $this->request->get['filter_coupon'];
		} else {
			$filter_coupon = '';
		}
		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}
	    
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$data['filter_name'] = $filter_name;
		$data['filter_price'] = $filter_price;
		$data['filter_sku'] = $filter_sku;
		$data['filter_homepage'] = $filter_homepage;
		$data['filter_coupon'] = $filter_coupon;
		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;

		$url = '';
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		
		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}
		
		if (isset($this->request->get['filter_homepage'])) {
			$url .= '&filter_homepage=' . $this->request->get['filter_homepage'];
		}
		
		if (isset($this->request->get['filter_coupon'])) {
			$url .= '&filter_coupon=' . $this->request->get['filter_coupon'];
		}
		
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/deals/add', 'token=' . $this->session->data['token'] . $url, true);

		$data['deals'] = array();

		$filter_data = array(
		    'filter_name'       => $filter_name,
		    'filter_price'      => $filter_price,
		    'filter_sku'        => $filter_sku,
		    'filter_homepage'   => $filter_homepage,
		    'filter_coupon'     => $filter_coupon,
		    'filter_date_start' => $filter_date_start,
		    'filter_date_end'   => $filter_date_end,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             => $this->config->get('config_limit_admin')
		);
		
		$data['sku_list'] =  $this->model_catalog_deals->getSkuList();

		$deals_total = $this->model_catalog_deals->getTotalDeals($filter_data);

		$results = $this->model_catalog_deals->getDeals($filter_data);
        
        $sku = "";
		foreach ($results as $result) {
		    $sku = $this->model_catalog_deals->getSkuName($result['option_value_id']);
			$data['deals'][] = array(
				'deal_id'       => $result['deal_id'],
				'name'          => $result['name'],
				'discount'      => $result['discount'],
				'product_id'    => $result['product_id'],
				'price'         => $result['price'],
				'sku'           => $sku,
				'homepage'      => $result['homepage'] == 1 ? "Yes" : "No",
				'coupon'        => $result['coupon'],
				'date_start'    => $result['date_start'],
				'date_end'      => $result['date_end'],
				'edit'          => $this->url->link('catalog/deals/edit', 'token=' . $this->session->data['token'] . '&deal_id=' . $result['deal_id'] . $url, true)
			);
			$sku = "";
		}

		$data['heading_title'] = $this->language->get('heading_title');

        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_sku'] = $this->language->get('column_sku');
		$data['column_homepage'] = $this->language->get('column_homepage');
		$data['column_coupon'] = $this->language->get('column_coupon');
		$data['column_date_start'] = $this->language->get('column_date_start');
		$data['column_date_end'] = $this->language->get('column_date_end');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_sku'] = $this->language->get('entry_sku');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_homepage'] = $this->language->get('entry_homepage');
		$data['entry_coupon'] = $this->language->get('entry_coupon');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		} else {
			$data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, true);
		$data['sort_price'] = $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . '&sort=d.price' . $url, true);
		$data['sort_homepage'] = $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . '&sort=d.homepage' . $url, true);
		$data['sort_coupon'] = $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . '&sort=d.coupon' . $url, true);
		$data['sort_date_start'] = $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . '&sort=d.date_start' . $url, true);
		$data['sort_date_end'] = $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . '&sort=d.date_end' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $deals_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($deals_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($deals_total - $this->config->get('config_limit_admin'))) ? $deals_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $deals_total, ceil($deals_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/deals_list', $data));
	}
	
	protected function getForm() {
	    $this->load->language('catalog/deals');
        $this->load->model('catalog/deals');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['deal_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		
		$url = '';
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		
		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}
		
		if (isset($this->request->get['filter_homepage'])) {
			$url .= '&filter_homepage=' . $this->request->get['filter_homepage'];
		}
		
		if (isset($this->request->get['filter_coupon'])) {
			$url .= '&filter_coupon=' . $this->request->get['filter_coupon'];
		}
		
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . $url, true)
		);
		
		$data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['text_help'] = $this->language->get('text_help');
        
        $data['tab_general'] = $this->language->get('tab_general');
        
        $data['entry_name'] = $this->language->get('entry_name');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_discount'] = $this->language->get('entry_discount');
		$data['entry_sku'] = $this->language->get('entry_sku');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_homepage'] = $this->language->get('entry_homepage');
		$data['entry_coupon'] = $this->language->get('entry_coupon');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$data['cancel'] = $this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . $url, true);
		
		if (isset($this->request->get['deal_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$deal_info = $this->model_catalog_deals->getDeal($this->request->get['deal_id']);
		}
		
		if (isset($this->request->post['product_id']) && $this->request->post['product_id'] > 0) {
			$data['product_id'] = $this->request->post['product_id'];
		} elseif (!empty($deal_info)) {
			$data['product_id'] = $deal_info['product_id'];
		} else {
			$data['product_id'] = '';
		}
		
		if($deal_info['product_id']) {
		    $data['options'] = $this->model_catalog_deals->getProductOptions($deal_info['product_id']);
		}
		
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($deal_info)) {
			$data['name'] = $deal_info['name'];
		} else {
			$data['name'] = '';
		}
		
		if (isset($this->request->post['price'])) {
			$data['price'] = $this->request->post['price'];
		} elseif (!empty($deal_info)) {
			$data['price'] = $deal_info['price'];
		} else {
			$data['price'] = '';
		}
		
		if (isset($this->request->post['product_mrp'])) {
			$data['product_mrp'] = $this->request->post['product_mrp'];
		} elseif (!empty($deal_info)) {
			$data['product_mrp'] = round($this->tax->calculate($deal_info['product_price'], $deal_info['tax_class_id'], $this->config->get('config_tax')));
		} else {
			$data['product_mrp'] = '';
		}
		
		if (isset($this->request->post['discount'])) {
			$data['discount'] = $this->request->post['discount'];
		} elseif (!empty($deal_info)) {
			$data['discount'] = $deal_info['discount'];
		} else {
			$data['discount'] = '';
		}
		
		if (isset($this->request->post['option_id'])) {
			$data['option_id'] = $this->request->post['option_id'];
		} elseif (!empty($deal_info)) {
			$data['option_id'] = $deal_info['option_id'];
		} else {
			$data['option_id'] = '';
		}
		
		if (isset($this->request->post['option_value_id'])) {
			$data['option_value_id'] = $this->request->post['option_value_id'];
		} elseif (!empty($deal_info)) {
			$data['option_value_id'] = $deal_info['option_value_id'];
		} else {
			$data['option_value_id'] = '';
		}
		
		if (isset($this->request->post['homepage'])) {
			$data['homepage'] = $this->request->post['homepage'];
		} elseif (!empty($deal_info)) {
			$data['homepage'] = $deal_info['homepage'];
		} else {
			$data['homepage'] = NULL;
		}
		
		if (isset($this->request->post['coupon'])) {
			$data['coupon'] = $this->request->post['coupon'];
		} elseif (!empty($deal_info)) {
			$data['coupon'] = $deal_info['coupon'];
		} else {
			$data['coupon'] = '';
		}
		
		if (!empty($deal_info)) {
		    $this->load->model('marketing/coupon');
		    $coupon_info = $this->model_marketing_coupon->getCouponByCode($deal_info['coupon']);
		    $coupon_id = $coupon_info['coupon_id'];
		} else {
		    $coupon_id = 0;
		}
		
		if (isset($this->request->post['coupon_id'])) {
			$data['coupon_id'] = $this->request->post['coupon_id'];
		} elseif ($coupon_id > 0) {
			$data['coupon_id'] = $coupon_id;
		} else {
			$data['coupon_id'] = 0;
		}
		
		if (isset($this->request->post['date_start'])) {
			$data['date_start'] = $this->request->post['date_start'];
		} elseif (!empty($deal_info)) {
			$data['date_start'] = date("Y-m-d", strtotime($deal_info['date_start']));
		} else {
			$data['date_start'] = '';
		}
		
		if (isset($this->request->post['date_end'])) {
			$data['date_end'] = $this->request->post['date_end'];
		} elseif (!empty($deal_info)) {
			$data['date_end'] = date("Y-m-d", strtotime($deal_info['date_end']));
		} else {
			$data['date_end'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['permission'])) {
			$data['error_permission'] = $this->error['permission'];
		} else {
			$data['error_permission'] = '';
		}
		
		if (isset($this->error['product'])) {
			$data['error_product'] = $this->error['product'];
		} else {
			$data['error_product'] = '';
		}
		
		if (isset($this->error['discount'])) {
			$data['error_discount'] = $this->error['discount'];
		} else {
			$data['error_discount'] = '';
		}
		
		if (isset($this->error['option'])) {
			$data['error_option'] = $this->error['option'];
		} else {
			$data['error_option'] = '';
		}
		
		if (isset($this->error['price'])) {
			$data['error_price'] = $this->error['price'];
		} else {
			$data['error_price'] = '';
		}
		
		if (isset($this->error['coupon'])) {
			$data['error_coupon'] = $this->error['coupon'];
		} else {
			$data['error_coupon'] = '';
		}
		
		if (isset($this->error['homepage'])) {
			$data['error_homepage'] = $this->error['homepage'];
		} else {
			$data['error_homepage'] = '';
		}
		
		if (isset($this->error['date_start'])) {
			$data['error_date_start'] = $this->error['date_start'];
		} else {
			$data['error_date_start'] = '';
		}
		
		if (isset($this->error['date_end'])) {
			$data['error_date_end'] = $this->error['date_end'];
		} else {
			$data['error_date_end'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$data['token'] = $this->session->data['token'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/deals_form', $data));
	}
	
	public function add() {
	    $this->load->language('catalog/deals');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/deals');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_deals->addDeal($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_sku'])) {
				$url .= '&filter_sku=' . urlencode(html_entity_decode($this->request->get['filter_sku'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_homepage'])) {
				$url .= '&filter_homepage=' . $this->request->get['filter_homepage'];
			}
			
			if (isset($this->request->get['filter_coupon'])) {
				$url .= '&filter_coupon=' . $this->request->get['filter_coupon'];
			}
			
			if (isset($this->request->get['filter_date_start'])) {
				$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
			}

			if (isset($this->request->get['filter_date_end'])) {
				$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}
	
	public function edit() {
	    $this->load->language('catalog/deals');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/deals');
		
		$deal_id = $this->request->get['deal_id'];

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_deals->editDeal($deal_id, $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_sku'])) {
				$url .= '&filter_sku=' . urlencode(html_entity_decode($this->request->get['filter_sku'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_homepage'])) {
				$url .= '&filter_homepage=' . $this->request->get['filter_homepage'];
			}
			
			if (isset($this->request->get['filter_coupon'])) {
				$url .= '&filter_coupon=' . $this->request->get['filter_coupon'];
			}
			
			if (isset($this->request->get['filter_date_start'])) {
				$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
			}

			if (isset($this->request->get['filter_date_end'])) {
				$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/deals', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/deals')) {
			$this->error['permission'] = $this->language->get('error_permission');
		}

		if (isset($this->request->post['product_id']) && $this->request->post['product_id'] == 0) {
			$this->error['product'] = $this->language->get('error_product');
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		if (isset($this->request->post['discount']) && $this->request->post['discount'] == 0) {
			$this->error['discount'] = $this->language->get('error_discount');
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		if (isset($this->request->post['has_option']) && $this->request->post['has_option'] == 1) {
		    if($this->request->post['option_value_id'] == '' || $this->request->post['option_value_id'] == 0) {
		        $this->error['option'] = $this->language->get('error_option');
		        $this->error['warning'] = $this->language->get('error_warning');
		    }
		}
		
		if (isset($this->request->post['price']) && $this->request->post['price'] <= 0) {
			$this->error['price'] = $this->language->get('error_price');
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		if (isset($this->request->post['coupon_id']) && $this->request->post['coupon_id'] == 0) {
			$this->error['coupon'] = $this->language->get('error_coupon');
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		if (isset($this->request->post['homepage']) && $this->request->post['homepage'] == '') {
			$this->error['homepage'] = $this->language->get('error_homepage');
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if ((utf8_strlen($this->request->post['date_start']) < 10) || (utf8_strlen($this->request->post['date_start']) > 10)) {
			$this->error['date_start'] = $this->language->get('error_date_start');
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		if ((utf8_strlen($this->request->post['date_end']) < 10) || (utf8_strlen($this->request->post['date_end']) > 10)) {
			$this->error['date_end'] = $this->language->get('error_date_end');
			$this->error['warning'] = $this->language->get('error_warning');
		}

        if($this->error) {
            return false;
        } else {
            return true;
        }
	}
}
?>