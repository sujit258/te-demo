<?php
class ControllerCatalogPartner extends Controller {
	private $error = array();

	public function index() {
	    
	   
        
		$this->load->language('catalog/partner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/partner');

		$this->getList();
	}

	public function info() {
		$this->load->language('catalog/partner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/partner');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/partner', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/partner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/partner');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $partner_id) {
				$this->model_catalog_partner->deletePartner($partner_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/partner', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
	    
		$this->document->addStyle('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
		$this->document->addStyle('https://cdn.datatables.net/1.10.11/css/dataTables.jqueryui.min.css');
		$this->document->addStyle('https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css');
		
		$this->document->addScript('http://code.jquery.com/jquery-1.12.0.min.js');
		$this->document->addScript('http://code.jquery.com/ui/1.11.0/jquery-ui.js');
		$this->document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
		$this->document->addScript('https://cdn.datatables.net/1.10.11/js/dataTables.jqueryui.min.js');
		$this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js');
        
        $this->document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js');
        
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
         $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js');
        $this->document->addScript('https://cdn.datatables.net/plug-ins/1.10.21/sorting/datetime-moment.js');
 
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		} 

		$url = '';	


		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/partner', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['delete'] = $this->url->link('catalog/partner/delete', 'token=' . $this->session->data['token'] . $url, true);

		
		// here
		$data['partner_list'] = array();
		
		$partner_total = $this->model_catalog_partner->getTotalPartners();

		$results = $this->model_catalog_partner->getPartnerList();

		foreach ($results as $result) {
			$data['partner_list'][] = array(
				'partner_id'  => $result['partner_id'],
				'name'       => $result['name'],
				'city'     => $result['city'],
				'scope'     => $result['scope'],
				'email'     => $result['email'],
				'mobile'     => $result['mobile'],
				'request_date' => $result['request_date'], 
				'view'       => $this->url->link('catalog/partner/info', 'token=' . $this->session->data['token'] . '&partner_id=' . $result['partner_id'] . $url, true)
			);
		} 	
		
		//print_r($data['partner_list']);
		// heading
		$data['heading_title'] = $this->language->get('heading_title');
		//column
		$data['column_name'] = $this->language->get('column_name');
		$data['column_city'] = $this->language->get('column_city');
		$data['column_scope'] = $this->language->get('column_scope');
		$data['column_request_date'] = $this->language->get('column_request_date');
		$data['column_action'] = $this->language->get('column_action');
		
		//text
		$data['text_list'] = $this->language->get('text_list');
		$data['text_view'] = $this->language->get('text_view');
		
		
		// Button
		$data['button_delete'] = $this->language->get('button_delete');
		
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}


		$pagination = new Pagination();
		$pagination->total = $partner_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/partner', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		
		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($partner_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($partner_total - $this->config->get('config_limit_admin'))) ? $partner_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $partner_total, ceil($partner_total / $this->config->get('config_limit_admin')));

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');		
		
		$this->response->setOutput($this->load->view('catalog/partner_list', $data));
	}

	protected function getForm() {
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		//Tag
		$data['tag_name']      = $this->language->get('tag_name');
		$data['tag_mobile']    = $this->language->get('tag_mobile');
		$data['tag_email']     = $this->language->get('tag_email');
		$data['tag_city']      = $this->language->get('tag_city');
		$data['tag_scope']     = $this->language->get('tag_scope');
		$data['tag_request_date'] = $this->language->get('tag_request_date');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/partner', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (isset($this->request->get['partner_id']))
		{
			$data['action'] = $this->url->link('catalog/partner/info', 'token=' . $this->session->data['token'] . '&partner_id=' . $this->request->get['partner_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/partner', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['partner_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST'))
		{
			$partner_info = $this->model_catalog_partner->getPartner($this->request->get['partner_id']);
			
			$data['token'] = $this->session->data['token'];		

		$data['partner_id'] = $partner_info['partner_id'];
		
		$data['name'] = $partner_info['name'];
		
		$data['email'] = $partner_info['email'];
		
		$data['mobile'] = $partner_info['mobile'];
		
		$data['scope'] = $partner_info['scope'];
		
		$data['city'] = $partner_info['city'];
		
		$data['request_date'] = ($partner_info['request_date'] != '0000-00-00 00:00' ? $partner_info['request_date'] : '');
		
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/partner_info', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/partner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/partner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}