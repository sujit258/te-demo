<?php
class ControllerCatalogPincode extends Controller {
	private $error = array();

	public function insert() {
		
		$this->document->setTitle('Postcode Service Availability Checker');
		$data['heading_title'] = 'Postcode Service Availability Checker';
		
		$this->load->model('catalog/pincode');
		
		$is_install = $this->model_catalog_pincode->is_install();
		//print_r($is_install);
		if($is_install['total'] <= 0){
			
			$this->response->redirect($this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST'  && $this->validate()){
			//echo "<pre>"; print_r($this->request->post); die;
			
			$this->model_catalog_pincode->addPincode($this->request->post);
			$data['success'] = "Pincodes Successfully Inserted";
		}
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} elseif (isset($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
		} else {
			
		}
		unset($this->session->data['error_warning']);
		//echo "<pre>"; print_r($data);
		//echo "<pre>"; print_r($this->session->data); die;
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => 'Pincode Module',
			'href' => $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Pincode Insert',
			'href' => $this->url->link('catalog/pincode/insert', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['delivery_time'] = $this->model_catalog_pincode->getmydelivery();
		//print_r($data['delivery_time']);die('hh');
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/pincode_insert.tpl', $data));
	}
	
	public function support() {
		
		$this->document->setTitle('Onjection Support');
		$data['heading_title'] = 'Onjection Support';
	
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/pincode_support.tpl', $data));
	}
	
	public function getlist(){
		
		$this->document->setTitle('Pincode List');
		$data['heading_title'] = 'Pincode List';
		
		$this->load->model('catalog/pincode');
		
		$is_install = $this->model_catalog_pincode->is_install();
		if($is_install['total'] <= 0){
			$this->response->redirect($this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$data['delivery_time'] = $this->model_catalog_pincode->getmydelivery();
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Pincode Module',
			'href' => $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => 'Pincode List',
			'href' => $this->url->link('catalog/pincode/getlist', 'token=' . $this->session->data['token'], 'SSL')
		);

		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$filter_data = array(
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		
		$where = array();
		if(isset($this->request->get['filter_pincode'])){
			$where[] = "p.pincode = '".$this->db->escape($this->request->get['filter_pincode'])."'";
			$url .= '&filter_pincode='.$this->request->get['filter_pincode'];
		}
		if(isset($this->request->get['filter_service']) && $this->request->get['filter_service'] != ''){
			$where[] = "p.service_available = '".$this->db->escape($this->request->get['filter_service'])."'";
			$url .= '&filter_service='.$this->request->get['filter_service'];
		}
		if(isset($this->request->get['filter_delivery']) && $this->request->get['filter_delivery'] != ''){
			$where[] = "p.delivery_option = '".$this->db->escape($this->request->get['filter_delivery'])."'";
			$url .= '&filter_delivery='.$this->request->get['filter_delivery'];
		}
		
		$data['csv_link'] = $this->url->link('catalog/pincode/download', 'token=' . $this->session->data['token'], 'SSL');
		$condition = implode(" AND ",$where);
		
		$pincode_total = $this->model_catalog_pincode->getTotalpincode($condition);
		$data['pincodes'] = $this->model_catalog_pincode->getpincode($filter_data,$condition);
		
		
		$pagination = new Pagination();
		$pagination->total = $pincode_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/pincode/getlist', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($pincode_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($pincode_total - $this->config->get('config_limit_admin'))) ? $pincode_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $pincode_total, ceil($pincode_total / $this->config->get('config_limit_admin')));
		
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/pincode_list.tpl', $data));
	}
	
	public function delete(){
		if ($this->request->server['REQUEST_METHOD'] == 'POST'){
			$this->load->model('catalog/pincode');
			$this->model_catalog_pincode->deletePincode($this->request->post);
			$this->session->data['success'] = "Pincode Successfully Delete";
		}
		$this->response->redirect($this->url->link('catalog/pincode/getlist', 'token=' . $this->session->data['token'], true));
	}
	public function deleteallpincodes(){
		$this->load->model('catalog/pincode');
		$this->model_catalog_pincode->deleteallpincodes();
		$this->session->data['success'] = "All Pincode Successfully Delete";
		$this->response->redirect($this->url->link('catalog/pincode/getlist', 'token=' . $this->session->data['token'], true));
	
		
		
	}
	public function edit(){
		
		if(isset($_GET['pincode_id']) && $_GET['pincode_id'] != ''){
			
			$this->document->setTitle('Pincode Edit');
			$data['heading_title'] = 'Pincode Edit';
			
			$this->load->model('catalog/pincode');
			
			$is_install = $this->model_catalog_pincode->is_install();
				if($is_install['total'] <= 0){
				$this->response->redirect($this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], true));
			}
			
			if ($this->request->server['REQUEST_METHOD'] == 'POST'){
				$this->model_catalog_pincode->addPincode($this->request->post);
				$data['success'] = "Pincodes Successfully Inserted";
			}
			
			$data['delivery_time'] = $this->model_catalog_pincode->getmydelivery();
			$data['pincode'] = $this->model_catalog_pincode->get_Pincode($_GET['pincode_id']);
			
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => 'Home',
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			);

			$data['breadcrumbs'][] = array(
				'text' => 'Pincode Module',
				'href' => $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], true)
			);
			
			$data['breadcrumbs'][] = array(
				'text' => 'Pincode List',
				'href' => $this->url->link('catalog/pincode/getlist', 'token=' . $this->session->data['token'], true)
			);
			
			$data['breadcrumbs'][] = array(
				'text' => 'Pincode Edit',
				'href' => $this->url->link('catalog/pincode/edit', 'token=' . $this->session->data['token'], true)
			);

			
			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];
				unset($this->session->data['success']);
			}
			
			$this->load->model('design/layout');

			$data['layouts'] = $this->model_design_layout->getLayouts();

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('catalog/pincode_edit.tpl', $data));
		}
	}
	
	public function update(){
		if ($this->request->server['REQUEST_METHOD'] == 'POST'){
			$this->load->model('catalog/pincode');
			$this->model_catalog_pincode->updatePincode($this->request->post);
			$this->session->data['success'] = "Pincode Successfully Update";
		}
		$this->response->redirect($this->url->link('catalog/pincode/getlist', 'token=' . $this->session->data['token'], true));
	}
	
	public function Insert_delivery() {
		
		$this->document->setTitle('Delivery Option');
		$data['heading_title'] = 'Delivery Option';
		
		$this->load->model('catalog/pincode');
		
		$is_install = $this->model_catalog_pincode->is_install();
		if($is_install['total'] <= 0){
			$this->response->redirect($this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], true));
		}
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST'){
			$this->model_catalog_pincode->adddelivery($this->request->post);
			$data['success'] = "Delivery Option Successfully Inserted";
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		
		$data['breadcrumbs'][] = array(
				'text' => 'Pincode Module',
				'href' => $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => 'Delivery Option Insert',
			'href' => $this->url->link('catalog/pincode/Insert_delivery', 'token=' . $this->session->data['token'], true)
		);

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/pincode_delivery_insert.tpl', $data));
	}
	
	public function delivery_getlist(){
		
		$this->document->setTitle('Delivery Option List');
		$data['heading_title'] = 'Delivery Option List';
		
		$this->load->model('catalog/pincode');
		
		$is_install = $this->model_catalog_pincode->is_install();
		if($is_install['total'] <= 0){
			$this->response->redirect($this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], true));
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		
		$data['breadcrumbs'][] = array(
				'text' => 'Pincode Module',
				'href' => $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => 'Delivery Option List',
			'href' => $this->url->link('catalog/pincode/delivery_getlist', 'token=' . $this->session->data['token'], true)
		);

		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$filter_data = array(
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$delivery_total = $this->model_catalog_pincode->getTotaldelivery();
		$data['deliveries'] = $this->model_catalog_pincode->getdelivery($filter_data);
		
		
		$pagination = new Pagination();
		$pagination->total = $delivery_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($delivery_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($delivery_total - $this->config->get('config_limit_admin'))) ? $delivery_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $delivery_total, ceil($delivery_total / $this->config->get('config_limit_admin')));
		
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/pincode_delivery_list.tpl', $data));
	}
	
	public function delivery_delete(){
		if ($this->request->server['REQUEST_METHOD'] == 'POST'){
			$this->load->model('catalog/pincode');
			$this->model_catalog_pincode->deletedelivery($this->request->post);
			$this->session->data['success'] = "Delivery Options Successfully Delete";
		}
		$this->response->redirect($this->url->link('catalog/pincode/delivery_getlist', 'token=' . $this->session->data['token'], true));
	}
	
	public function delivery_edit(){
		
		if(isset($_GET['delivery_id']) && $_GET['delivery_id'] != ''){
			
			$this->document->setTitle('Delivery Option Edit');
			$data['heading_title'] = 'Delivery Option Edit';
			
			$this->load->model('catalog/pincode');
			
			$is_install = $this->model_catalog_pincode->is_install();
			if($is_install['total'] <= 0){
				$this->response->redirect($this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], true));
			}
		
			if ($this->request->server['REQUEST_METHOD'] == 'POST'){
				$this->model_catalog_pincode->addPincode($this->request->post);
				$data['success'] = "Pincodes Successfully Inserted";
			}
			
			$data['delivery_time'] = $this->model_catalog_pincode->getmydelivery();
			$data['delivery'] = $this->model_catalog_pincode->get_delivery($_GET['delivery_id']);
			
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => 'Home',
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			);

			$data['breadcrumbs'][] = array(
				'text' => 'Pincode Module',
				'href' => $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], true)
			);
			
			$data['breadcrumbs'][] = array(
				'text' => 'Delivery Option',
				'href' => $this->url->link('catalog/pincode/delivery_getlist', 'token=' . $this->session->data['token'], true)
			);
			
			$data['breadcrumbs'][] = array(
				'text' => 'Delivery Option Edit',
				'href' => $this->url->link('catalog/pincode/edit', 'token=' . $this->session->data['token'], true)
			);

			
			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];
				unset($this->session->data['success']);
			}
			
			$this->load->model('design/layout');

			$data['layouts'] = $this->model_design_layout->getLayouts();

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('catalog/pincode_delivery_edit.tpl', $data));
		}
	}
	
	public function delivery_update(){
		if ($this->request->server['REQUEST_METHOD'] == 'POST'){
			$this->load->model('catalog/pincode');
			$this->model_catalog_pincode->updatedelivery($this->request->post);
			$this->session->data['success'] = "Delivery Option Successfully Update";
		}
		$this->response->redirect($this->url->link('catalog/pincode/delivery_getlist', 'token=' . $this->session->data['token'], true));
	}
	
	public function upload(){
		
		if($this->request->server['REQUEST_METHOD'] == 'POST' && !empty($_FILES["file_up"]['name'])){
			//echo "<pre>"; print_r($_FILES["file_up"]); die;
			$pincode = array();
			$type = $_FILES["file_up"]["type"];
			$csv_file = $_FILES["file_up"]["tmp_name"];
			$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
			/* echo "<pre>"; print_r($mimes); 
			echo "<pre>"; print_r($_FILES["file_up"]);
			if(in_array($_FILES["file_up"]["type"],$mimes)){
			echo "<pre>"; print_r($_FILES["file_up"]);} die; */
			if(in_array($_FILES["file_up"]["type"],$mimes)){
				if (($getfile = fopen($csv_file, "r")) !== FALSE) {
					$CSV = fgetcsv($getfile, 1000, ",");
					while (($CSV = fgetcsv($getfile, 1000, ",")) !== FALSE) {
						for ($c=0; $c < 1; $c++) {
							$result = $CSV;
							$str = implode(",", $result);
							$slice = explode(",", $str);
							$col1 = $slice[0];
							$pincode[] = $col1;
						}
					}
					$this->load->model('catalog/pincode');
					if(isset($this->request->post['delivery']))
					{
					$this->model_catalog_pincode->uploadpincode($pincode,$this->request->post['service'],$this->request->post['delivery']);
					}
					else{
						$this->request->post['delivery'] = '';
						$this->model_catalog_pincode->uploadpincode($pincode,$this->request->post['service'],$this->request->post['delivery']);
					}
					$this->session->data['success'] = "Pincode Upload Successfully";
					$this->response->redirect($this->url->link('catalog/pincode/getlist', 'token=' . $this->session->data['token'], true));
				}
			} else{
				$this->session->data['error_warning'] = "File not in proper format";
				$this->response->redirect($this->url->link('catalog/pincode/insert', 'token=' . $this->session->data['token'], true));
			}
			
		} else{
			$this->session->data['error_warning'] = "First upload the file";
			//return $this-error['warning'];
			$this->response->redirect($this->url->link('catalog/pincode/insert', 'token=' . $this->session->data['token'], true));
		}
		
	}
	
	public function setting(){
		
		$this->document->setTitle('Postcode Service Availability Checker');
			$data['heading_title'] = 'Postcode Service Availability Checker';
			
			$this->load->model('catalog/pincode');
			
			$is_install = $this->model_catalog_pincode->is_install();
			if($is_install['total'] <= 0){
				$this->response->redirect($this->url->link('extension/extension/module', 'token=' . $this->session->data['token'], true));
			}
			
			$this->load->model('setting/setting');
			if ($this->request->server['REQUEST_METHOD'] == 'POST') {
				$this->model_setting_setting->editSetting('pincodesetting', $this->request->post); 
				$this->session->data['success'] = "Setting Successfully Save.";
				$this->response->redirect($this->url->link('catalog/pincode/setting', 'token=' . $this->session->data['token'], true));
			}
			
			$data['pincode_checkout_status'] = $this->config->get('pincodesetting_checkout_status');
			$data['pincodesetting_pincodemustbesame'] = $this->config->get('pincodesetting_pincodemustbesame');
			$data['pincode_product_page_status'] = $this->config->get('pincodesetting_product_page_status');
			$data['pincode_no_service_msg'] = $this->config->get('pincodesetting_no_service_msg');
			$data['pincode_cod_msg'] = $this->config->get('pincodesetting_cod_msg');
			$data['pincode_prepaid_msg'] = $this->config->get('pincodesetting_prepaid_msg');
			$data['pincode_cod_error'] = $this->config->get('pincodesetting_cod_error');
			$data['pincode_text_color'] = $this->config->get('pincodesetting_text_color');
			
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => 'Home',
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			);

			$data['breadcrumbs'][] = array(
				'text' => 'Pincode Module',
				'href' => $this->url->link('extension/module/pincode', 'token=' . $this->session->data['token'], true)
			);
			
			$data['breadcrumbs'][] = array(
				'text' => 'Setting',
				'href' => $this->url->link('catalog/pincode/setting', 'token=' . $this->session->data['token'], true)
			);

			
			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];
				unset($this->session->data['success']);
			}
			
			$this->load->model('design/layout');

			$data['layouts'] = $this->model_design_layout->getLayouts();

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('catalog/pincode_setting.tpl', $data));
	}
	
	public function download() {
		
		$where = array();
		if(isset($this->request->get['filter_pincode'])){
			$where[] = "p.pincode = '".$this->request->get['filter_pincode']."'";
		}
		if(isset($this->request->get['filter_service']) && $this->request->get['filter_service'] != ''){
			$where[] = "p.service_available = '".$this->request->get['filter_service']."'";
		}
		if(isset($this->request->get['filter_delivery']) && $this->request->get['filter_delivery'] != ''){
			$where[] = "p.delivery_option = '".$this->request->get['filter_delivery']."'";
		}
		
		$condition = implode(" AND ",$where);
		$this->load->model('catalog/pincode');
		$item = $this->model_catalog_pincode->csvdownload($condition);
		
		header('Content-Type: text/csv; charset=utf-8');
		header("Content-Transfer-Encoding: UTF-8");
		header('Content-Disposition: attachment; filename=Pincode.csv');
		header("Pragma: no-cache");
		header("Expires: 0");
		$output = fopen("php://output", "w");
		fputcsv($output,array('Pincode'));
		foreach ($item as $row) {
			fputcsv($output, $row);
		}
		fclose($output);
		exit();
	}
	protected function validate() {
		//echo "<pre>"; print_r($this->request->post); die;
		for($i=1;$i<=count($this->request->post['data']);$i++){
			if (empty($this->request->post['data'][$i]['pin'])) {
				$this->error['warning'] = "Pincode must not be empty";
			}
		}
		/* if (empty) {
			$this->error['warning'] = $this->language->get('error_permission');
		} */

		return !$this->error;
	}
}