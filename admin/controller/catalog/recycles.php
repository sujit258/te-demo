<?php
class ControllerCatalogRecycles extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/recycles');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/recycles');

		$this->getList();
	}
	
	public function update() {
		$this->language->load('catalog/recycles');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/recycles');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_recycles->editRecycles($this->request->get['recycle_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->response->redirect($this->url->link('catalog/recycles', 'token=' . $this->session->data['token'] . $url, true));
			
		}

		$this->getForm();
	}


	public function delete() {
		$this->load->language('catalog/recycles');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/recycles');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			
			foreach ($this->request->post['selected'] as $recycle_id) {
				$this->model_catalog_recycles->deleteRecycles($recycle_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/recycles', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_mobile'])) {
			$filter_mobile = $this->request->get['filter_mobile'];
		} else {
			$filter_mobile = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		} 

		$url = '';	
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_mobile'])) {
			$url .= '&filter_mobile=' . urlencode(html_entity_decode($this->request->get['filter_mobile'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/recycles', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['delete'] = $this->url->link('catalog/recycles/delete', 'token=' . $this->session->data['token'] . $url, true);

		$filter_data = array(
			
				'filter_name'   => $filter_name,
				'filter_mobile' => $filter_mobile,
				'filter_status'   => $filter_status,				
				'order' => $order,
				'start' => ($page - 1) * $this->config->get('config_limit_admin'),
				'limit' => $this->config->get('config_limit_admin')
			);

		// here
		$data['recycles_list'] = array();
		
		$recycles_total = $this->model_catalog_recycles->getTotalRecycles($filter_data);

		$results = $this->model_catalog_recycles->getRecyclesList($filter_data);

		foreach ($results as $result) {
			$data['recycles_list'][] = array(
				'recycle_id'  => $result['recycle_id'],
				'name'       => $result['name'],
				'mobile'  => $result['mobile'],
				'total_packets'       => $result['total_packets'],
				'status'     => $this->model_catalog_recycles->getStatus($result['status_id']),
				'request_date' => date($this->language->get('date_format_short'), strtotime($result['request_date'])),
				'edit'       => $this->url->link('catalog/recycles/update', 'token=' . $this->session->data['token'] . '&recycle_id=' . $result['recycle_id'] . $url, true)
			);
		}

		// Statuses
		$data['status_list'] = $this->model_catalog_recycles->getRecyclesStatuses();
		
		// heading
		$data['heading_title'] = $this->language->get('heading_title');
		//column
		$data['column_name'] = $this->language->get('column_name');
		$data['column_mobile'] = $this->language->get('column_mobile');
		$data['column_no_of_items'] = $this->language->get('column_no_of_items');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_request_date'] = $this->language->get('column_request_date');
		$data['column_action'] = $this->language->get('column_action');
		
		//text
		$data['text_list'] = $this->language->get('text_list');
		$data['text_view'] = $this->language->get('text_view');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_no_results'] = $this->language->get('text_no_results');
		
		// Filter
		$data['filter_name_placeholder'] = $this->language->get('filter_name');
		$data['filter_status_placeholder'] = $this->language->get('filter_status');
		$data['filter_mobile_placeholder'] = $this->language->get('filter_mobile');
		
		
		// Button
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
		
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$pagination = new Pagination();
		$pagination->total = $recycles_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/recycles', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		
		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($recycles_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($recycles_total - $this->config->get('config_limit_admin'))) ? $recycles_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $recycles_total, ceil($recycles_total / $this->config->get('config_limit_admin')));	
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');		
		
		$this->response->setOutput($this->load->view('catalog/recycles_list', $data));
	}

	protected function getForm() {
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		//Tag
		$data['tag_name']        = $this->language->get('tag_name');
		$data['tag_mobile']      = $this->language->get('tag_mobile');
		$data['tag_email']       = $this->language->get('tag_email');
		$data['tag_address']     = $this->language->get('tag_address');
		$data['tag_no_of_items'] = $this->language->get('tag_no_of_items');
		$data['tag_request_date']= $this->language->get('tag_request_date');
		$data['tag_status'] 	 = $this->language->get('tag_status');
		$data['tag_pickup_date']= $this->language->get('tag_pickup_date');
		$data['tag_pickup_slot'] 	 = $this->language->get('tag_pickup_slot');
		
		// Button		
		$data['button_save']        = $this->language->get('button_save');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/recycles', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (isset($this->request->get['recycle_id']))
		{
			$data['action'] = $this->url->link('catalog/recycles/update', 'token=' . $this->session->data['token'] . '&recycle_id=' . $this->request->get['recycle_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/recycles', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['recycle_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST'))
		{
			$recycles_info = $this->model_catalog_recycles->getRecycle($this->request->get['recycle_id']);
			
			$data['token'] = $this->session->data['token'];	

		$data['recycles'] = array();
		
		if($recycles_info) {
		    if($recycles_info['pickup_slot'] == 1) {
		        $pickup_slot = "9AM - 1PM";
		    } else if($recycles_info['pickup_slot'] == 2) {
		        $pickup_slot = "1PM - 4PM";
		    } else if($recycles_info['pickup_slot'] == 3) {
		        $pickup_slot = "4PM - 7PM";
		    } else {
		        $pickup_slot = "";
		    }
		    
			$data['recycles'][] = array(
			'recycle_id'		=> $recycles_info['recycle_id'],
			'name'				=> $recycles_info['name'],
			'email'				=> $recycles_info['email'],
			'mobile'			=> $recycles_info['mobile'],
			'address'			=> $recycles_info['address'],
			'total_packets'		=> $recycles_info['total_packets'],
			'pickup_date'		=> $recycles_info['pickup_date'],
			'pickup_slot'		=> $pickup_slot,
			'status_id'			=> $recycles_info['status_id'],
			'request_date'		=> date($this->language->get('date_format_short'), strtotime($recycles_info['request_date']))
			
			);
		}
		
		if ($this->request->post) {
			$data['name']		   = $this->request->post['name'];
			$data['email'] 		   = $this->request->post['email'];
			$data['mobile'] 	   = $this->request->post['mobile'];			
			$data['address']	   = $this->request->post['address'];
			$data['total_packets'] = $this->request->post['total_packets'];
			$data['recycle_status']= $this->request->post['recycle_status'];
		}
		
		$data['status_list'] = $this->model_catalog_recycles->getRecyclesStatuses();
		
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/recycles_info', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/recycles')) {
			$this->error['warning'] = $this->language->get('error_permission');			
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/recycles')) {
			
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	// Function autocomplete for filter_data
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']))
		{
			$this->load->model('catalog/recycles');
			
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name'  => $filter_name,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_catalog_recycles->getRecycles($filter_data);

			foreach ($results as $result) {
				
				$json[] = array(
					'recycle_id' => $result['recycle_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					);
			}
		}
		
		if (isset($this->request->get['filter_mobile']))
		{
			$this->load->model('catalog/recycles');
			
			if (isset($this->request->get['filter_mobile'])) {
				$filter_mobile = $this->request->get['filter_mobile'];
			} else {
				$filter_mobile = '';
			}

			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_mobile'  => $filter_mobile,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_catalog_recycles->getRecycles($filter_data);

			foreach ($results as $result) {
				
				$json[] = array(
					'recycle_id' => $result['recycle_id'],
					'mobile'       => strip_tags(html_entity_decode($result['mobile'], ENT_QUOTES, 'UTF-8')),
					);
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}	

}