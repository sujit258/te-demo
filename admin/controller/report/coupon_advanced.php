<?php
class ControllerReportCouponAdvanced extends Controller {
	public function index() {
		$this->load->language('report/product_purchased_advanced');
		
		
		$this->document->addStyle('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css');
		$this->document->addStyle('https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css');
		$this->document->addStyle('https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css');
		

		$this->document->addScript('https://code.jquery.com/jquery-3.3.1.js');
		$this->document->addScript('https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js');
		$this->document->addScript('https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js');
		$this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js');
        
       
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js');
        
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}
		
		if (isset($this->request->get['filter_coupon_id'])) {
			$filter_coupon_id = $this->request->get['filter_coupon_id'];
		} else {
			$filter_coupon_id = "";
		}
		

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}
		
		if (isset($this->request->get['filter_coupon_id'])) {
			$url .= '&filter_coupon_id=' . $this->request->get['filter_coupon_id'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/coupon_advanced', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$this->load->model('report/coupon');

		$data['coupons'] = array();

		$filter_data = array(
			'filter_date_start'	     	=> $filter_date_start,
			'filter_date_end'	     	=> $filter_date_end,
			'filter_order_status_id' 	=> $filter_order_status_id,
			'filter_coupon_id'          => $filter_coupon_id,
			'start'                 	=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  	=> $this->config->get('config_limit_admin')
		);

		$coupon_total = $this->model_report_coupon->getTotalCoupons($filter_data);

		$results = $this->model_report_coupon->getCouponAdvanced($filter_data);
			
			echo "<pre>";
			print_r($results);
			echo "</pre>"; 
			
		foreach ($results as $result) {
			if (!empty($filter_coupon_id)) {
				// single product
				$data['coupons'][] = array(
					'name'       => $result['name'],
					'email'      => $result['email'],
					'order_date' => date($this->language->get('datetime_format'), strtotime($result['order_date'])),
					'order_id' => $result['order_id'],
					'order_status' => $result['order_status'],
					'quantity' => $result['quantity'],
					'total' => $this->currency->format($result['total'], $this->config->get('config_currency')),
					'code'      => $result['code'],
					'customer'   => $result['customer'],
					'coupon_id' => $result['coupon_id']
				);
			} else {
				$data['coupos'][] = array(
					'name'       => $result['name'],
					'code'       => $result['code'],
					'coupon_id'  => $result['coupon_id'],
					'quantity'   => $result['quantity'],
					'total'      => $this->currency->format($result['total'], $this->config->get('config_currency'))
				);
			}
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_customer_name'] = $this->language->get('column_customer_name');
		$data['column_product_name'] = $this->language->get('column_product_name');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_country'] = $this->language->get('column_country');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_order_date'] = $this->language->get('column_order_date');
		$data['column_order_status'] = $this->language->get('column_order_status');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}
		
		if (isset($this->request->get['filter_coupon_id'])) {
			$url .= '&filter_coupon_id=' . $this->request->get['filter_coupon_id'];
		}
		

		$pagination = new Pagination();
		$pagination->total = $coupon_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/coupon_advanced', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($coupon_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($coupon_total - $this->config->get('config_limit_admin'))) ? $coupon_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $coupon_total, ceil($coupon_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_order_status_id'] = $filter_order_status_id;
		$data['filter_coupon_id'] = $filter_coupon_id;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/coupon_advanced.tpl', $data));
	}
}