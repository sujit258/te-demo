<?php
class ControllerAnalyticsProductPerformance extends Controller {
	public function index() {
	    $this->document->addStyle('https://cdn.datatables.net/1.10.11/css/dataTables.jqueryui.min.css');
		$this->document->addStyle('https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css');
		$this->document->addScript('http://code.jquery.com/jquery-1.12.0.min.js');
		$this->document->addScript('http://code.jquery.com/ui/1.11.0/jquery-ui.js');
		$this->document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
		$this->document->addScript('https://cdn.datatables.net/1.10.11/js/dataTables.jqueryui.min.js');
		$this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js');
        
        $this->document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js');
        
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        
		$this->load->language('analytics/product_performance');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('analytics/product_performance', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('analytics/product_performance');

		$data['products'] = array();
		
		for($i = 6; $i >= 0; $i--) {
		    $date_start = date('Y-m-01', strtotime(date('Y-m-01') . "- $i MONTHS"));
		    $date_end = date('Y-m-t', strtotime($date_start));
		    $filter_data[] = array(
		        'date_start'    => $date_start,
		        'date_end'      => $date_end
		    );
		    
		    $data['months_data'][] = strtolower(date('M_y', strtotime($date_start)));
		}
		$data['months_data'] = array_reverse($data['months_data']);
		
		if($this->request->get['category'] && $this->request->get['category'] != '') {
            $category = $data['category'] = $this->request->get['category'];
            
            $data['results'] = $this->model_analytics_product_performance->getCategoryData($this->request->get['category'], $filter_data);
            
            $current_date = date('Y-m-d', strtotime("-1 DAYS"));
            $date_start = date('Y-m-01', strtotime($current_date));
            $data['daily_product_report'] = $this->model_analytics_product_performance->getDailyProductData($this->request->get['category'], $date_start, $current_date);
            
            $i = date('d', strtotime($current_date));
            while($i >= 1) {
                if($i < 10) {
                    $data['report_date_data'][] = date('Y-m-0' . round($i), strtotime($current_date));
                } else {
                    $data['report_date_data'][] = date('Y-m-' . $i, strtotime($current_date));
                }
                $i--;
            }
            
            $data['heading_title'] = $this->language->get('heading_title') . " (" . $category . ")";
            
            $data['breadcrumbs'][] = array(
			    'text' => $category,
			    'href' => $this->url->link('analytics/product_performance', 'category=' . $category . '&token=' . $this->session->data['token'] . $url, true)
		    );
		    
		    $mapped_categories = $this->model_analytics_product_performance->getMappedCategories();
		    
		    foreach($mapped_categories AS $mapped_category) {
		        $data['mapped_categories'][] = array(
		            'name'  => $mapped_category['name'],
		            'href'  => $this->url->link('analytics/product_performance', 'category=' . $mapped_category['name'] . '&token=' . $this->session->data['token'] . $url, 'SSL')
		        );
		    }
	    } else {
	        $data['results'] = $this->model_analytics_product_performance->getProductsData($filter_data);
	        $data['heading_title'] = $this->language->get('heading_title');
		}

		$data['column_category_name'] = $this->language->get('column_category_name');
		$data['column_product_name'] = $this->language->get('column_product_name');
		
		$data['text_weekly_units_summary'] = $this->language->get('text_weekly_units_summary');
		$data['text_monthly_units_summary'] = $this->language->get('text_monthly_units_summary');
		$data['text_monthly_sales_summary'] = $this->language->get('text_monthly_sales_summary');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('analytics/product_performance', $data));
	}
}