<?php
class ControllerAnalyticsClv extends Controller {
	public function index() {
	    $this->document->addStyle('https://cdn.datatables.net/1.10.11/css/dataTables.jqueryui.min.css');
		$this->document->addStyle('https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css');
		$this->document->addScript('http://code.jquery.com/jquery-1.12.0.min.js');
		$this->document->addScript('http://code.jquery.com/ui/1.11.0/jquery-ui.js');
		$this->document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
		$this->document->addScript('https://cdn.datatables.net/1.10.11/js/dataTables.jqueryui.min.js');
		$this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js');
        
        $this->document->addScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js');
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js');
        $this->document->addScript('https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js');
        
		$this->load->language('analytics/clv');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('analytics/clv', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('analytics/clv');
		
		if(isset($this->request->get['filter_range']) && !empty($this->request->get['filter_range'])) {
		    $filter_range = $this->request->get['filter_range'];
		    switch ($filter_range) {
                case "custom":
                    $filter_date_start = $this->request->get['filter_from_date'];
                    $filter_date_end = $this->request->get['filter_to_date'];
                    break;
                case "3m":
                    $filter_date_end = date('Y-m-d');
                    $filter_date_start = date('Y-m-01', strtotime($filter_date_end . " -3 MONTHS"));
                    break;
                case "6m":
                    $filter_date_end = date('Y-m-d');
                    $filter_date_start = date('Y-m-01', strtotime($filter_date_end . " -6 MONTHS"));
                    break;
                case "12m":
                    $filter_date_end = date('Y-m-d');
                    $filter_date_start = date('Y-m-01', strtotime($filter_date_end . " -12 MONTHS"));
                    break;
	        }
		} else {
		    $filter_range = '3m';
		    $filter_date_end = date('Y-m-d');
		    $filter_date_start = date('Y-m-01', strtotime($filter_date_end . " -3 MONTHS"));
		}
        
        $data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_telephone'] = $this->language->get('column_telephone');
		$data['column_latest_order_date'] = $this->language->get('column_latest_order_date');
		$data['column_value'] = $this->language->get('column_value');
		$data['column_units'] = $this->language->get('column_units');
		$data['column_order_count'] = $this->language->get('column_order_count');
		$data['column_ticket_size'] = $this->language->get('column_ticket_size');
		
		$filters = array(
            'filter_date_start' => $filter_date_start,
            'filter_date_end'   => $filter_date_end
		);
		
		$clv_data = $this->model_analytics_clv->getClvData($filters);
		
		foreach($clv_data AS $clv) {
		    $data['clv_data'][] = array(
		        'customer_id'       => $clv['customer_id'],
		        'email'             => $clv['email'],
		        'telephone'         => $clv['telephone'],
		        'name'              => $clv['customer'],
		        'order_value'       => round($clv['order_value']),
		        'order_units'       => round($clv['order_units']),
		        'date_added'        => date('Y-m-d', strtotime($clv['date_added'])),
		        'order_count'       => $clv['order_count'],
		        'latest_order_date' => date('Y-m-d', strtotime($clv['latest_order_date'])),
		        'ticket_size'       => round($clv['order_value'] / $clv['order_count']),
		        'href'              => $this->url->link('customer/customer/edit', 'customer_id=' . $clv['customer_id'] . '&token=' . $this->session->data['token'], true)
		    );
		}
		
		$data['filter_range'] = $filter_range;
		$data['filter_from_date'] = $filter_date_start;
		$data['filter_to_date'] = $filter_date_end;

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('analytics/clv', $data));
	}
}