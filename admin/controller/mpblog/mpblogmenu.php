<?php
class ControllerMpBlogMpBlogMenu extends Controller {
	public function index() {

		$this->load->language('mpblog/mpblogmenu');

		$this->document->addStyle('view/stylesheet/modulepoints/mpblog.css');

		$this->document->addScript('view/javascript/mpblog/mpblog.js');

		// Menu
		$data['menus'][] = array(
			'id'       => 'menu-dashboard',
			'icon'	   => 'fa-dashboard',
			'name'	   => $this->language->get('text_mpblogdashboard'),
			'href'     => $this->url->link('mpblog/mpblogdashboard', 'token=' . $this->session->data['token'], true),
			'children' => array()
		);

		$data['menus'][] = array(
			'id'       => 'menu-mpblogcategory',
			'icon'	   => 'fa-list-ul',
			'name'	   => $this->language->get('text_mpblog_category'),
			'href'     => $this->url->link('mpblog/mpblogcategory', 'token=' . $this->session->data['token'], true),
			'children' => array()
		);

		$data['menus'][] = array(
			'id'       => 'menu-mpblogpost',
			'icon'	   => 'fa-pencil-square-o',
			'name'	   => $this->language->get('text_mpblog_post'),
			'href'     => $this->url->link('mpblog/mpblogpost', 'token=' . $this->session->data['token'], true),
			'children' => array()
		);

		$data['menus'][] = array(
			'id'       => 'menu-mpblogcomment',
			'icon'	   => 'fa-comments',
			'name'	   => $this->language->get('text_mpblog_comment'),
			'href'     => $this->url->link('mpblog/mpblogcomment', 'token=' . $this->session->data['token'], true),
			'children' => array()
		);

		$data['menus'][] = array(
			'id'       => 'menu-mpblograting',
			'icon'	   => 'fa-star',
			'name'	   => $this->language->get('text_mpblog_rating'),
			'href'     => $this->url->link('mpblog/mpblograting', 'token=' . $this->session->data['token'], true),
			'children' => array()
		);
		
		$data['menus'][] = array(
			'id'       => 'menu-mpblogsetting',
			'icon'	   => 'fa-wrench',
			'name'	   => $this->language->get('text_mpblogsetting'),
			'href'     => $this->url->link('mpblog/mpblog', 'token=' . $this->session->data['token'], true),
			'children' => array()
		);


		$data['text_mpblog'] = $this->language->get('text_mpblog');

		

		return $this->load->view('mpblog/mpblogmenu', $data);

	}
}