<?php
class ControllerMpBlogMpBlogDashboard extends Controller {
	private $error = array();

	public function __construct($registry) {
		parent :: __construct($registry);

		$this->load->model('mpblog/mpbloginstall');
		$this->model_mpblog_mpbloginstall->install();

	}

	public function index() {
		$this->load->language('mpblog/mpblogdashboard');

		$this->document->setTitle($this->language->get('heading_title'));


		$data['mpblogcategory'] = $this->url->link('mpblog/mpblogcategory', 'token=' . $this->session->data['token'], true);
		$data['mpblogpost'] = $this->url->link('mpblog/mpblogpost', 'token=' . $this->session->data['token'], true);
		$data['mpblogcomment'] = $this->url->link('mpblog/mpblogcomment', 'token=' . $this->session->data['token'], true);
		$data['mpblogreview'] = $this->url->link('mpblog/mpblogreview', 'token=' . $this->session->data['token'], true);
		$data['mpblog'] = $this->url->link('mpblog/mpblog', 'token=' . $this->session->data['token'], true);

		

		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_total_blogs'] = $this->language->get('text_total_blogs');
		$data['text_total_categories'] = $this->language->get('text_total_categories');
		$data['text_total_comments'] = $this->language->get('text_total_comments');
		$data['text_total_ratings'] = $this->language->get('text_total_ratings');
		$data['text_edit'] = $this->language->get('text_edit');

		$data['text_view_blogs'] = $this->language->get('text_view_blogs');
		$data['text_view_categories'] = $this->language->get('text_view_categories');
		$data['text_view_comments'] = $this->language->get('text_view_comments');
		$data['text_view_ratings'] = $this->language->get('text_view_ratings');
		$data['text_blog_show_search'] = $this->language->get('text_blog_show_search');

		$data['text_latest_blogs'] = $this->language->get('text_latest_blogs');
		$data['text_latest_comments'] = $this->language->get('text_latest_comments');
		$data['text_top_rated_blogs'] = $this->language->get('text_top_rated_blogs');
		$data['text_top_viewed_blogs'] = $this->language->get('text_top_viewed_blogs');
		$data['text_no_blogs'] = $this->language->get('text_no_blogs');
		$data['text_no_comments'] = $this->language->get('text_no_comments');

		$data['column_image'] = $this->language->get('column_image');
		$data['column_blog'] = $this->language->get('column_blog');
		$data['column_date_available'] = $this->language->get('column_date_available');
		$data['column_author'] = $this->language->get('column_author');
		$data['column_published'] = $this->language->get('column_published');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_commentby'] = $this->language->get('column_commentby');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_comment'] = $this->language->get('column_comment');

		$data['button_viewall'] = $this->language->get('button_viewall');

		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}	

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);


		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('mpblog/mpblog', 'token=' . $this->session->data['token'], true)
		);


		$data['token'] = $this->session->data['token'];

		
		// blog post
		$this->load->model('mpblog/mpblogpost');
		$data['total_blogs'] = $this->model_mpblog_mpblogpost->getTotalMpBlogPosts();
		$data['mpblogs'] = $this->url->link('mpblog/mpblogpost', 'token=' . $this->session->data['token'] , true);

		// blog category
		$this->load->model('mpblog/mpblogcategory');
		$data['total_categories'] = $this->model_mpblog_mpblogcategory->getTotalMpBlogCategories();
		$data['mpcategory'] = $this->url->link('mpblog/mpblogcategory', 'token=' . $this->session->data['token'] , true);

		// blog comment
		$this->load->model('mpblog/mpblogcomment');
		$data['total_comments'] = $this->model_mpblog_mpblogcomment->getTotalMpBlogComments();
		$data['mpcomment'] = $this->url->link('mpblog/mpblogcomment', 'token=' . $this->session->data['token'] , true);

		// blog rating
		$this->load->model('mpblog/mpblograting');
		$data['total_ratings'] = $this->model_mpblog_mpblograting->getTotalMpBlogRatings();
		$data['mprating'] = $this->url->link('mpblog/mpblograting', 'token=' . $this->session->data['token'] , true);

		
		$this->load->model('tool/image');

		// Latest Blogs
		$filter_data = array(
			'sort' => 'p.date_available',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 5,
		);

		$latest_blogs = $this->model_mpblog_mpblogpost->getMpBlogPosts($filter_data);

		$data['latest_blogs'] = array();

		foreach($latest_blogs as $latest_blog) {
			if(!empty($latest_blog['image']) && file_exists(DIR_IMAGE . $latest_blog['image'])) {
				$thumb = $this->model_tool_image->resize($latest_blog['image'], 40, 40);
			} else {
				$thumb = $this->model_tool_image->resize('no_image.png', 40, 40);
			}
			$data['latest_blogs'][] = array(
				'thumb' => $thumb,
				'name' => $latest_blog['name'],
				'author' => $latest_blog['author'],
				'likes' => $latest_blog['likes'],
				'viewed' => $latest_blog['viewed'],
				'rating' => round($latest_blog['rating'],2),
				'date_available' => date($this->language->get('date_format_short'), strtotime($latest_blog['date_available'])) ,
				'status' => ($latest_blog['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'href' => $this->url->link('mpblog/mpblogpost/edit', 'token=' .$this->session->data['token'] .'&mpblogpost_id=' . $latest_blog['mpblogpost_id'], true ) ,
			);
		}

		$data['all_blogs'] = $this->url->link('mpblog/mpblogpost', 'token=' .$this->session->data['token'] , true );

		// Latest Comments
		$filter_data = array(
			'sort' => 'r.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 5,
		);

		$latest_comments = $this->model_mpblog_mpblogcomment->getMpBlogComments($filter_data);


		$data['latest_comments'] = array();

		foreach($latest_comments as $latest_comment) {
			
			$data['latest_comments'][] = array(
				'name' => $latest_comment['name'],
				'text' => substr($latest_comment['text'], 0, 100) ,
				'author' => $latest_comment['author'],
				'status' => ($latest_comment['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'date_added' => date($this->language->get('date_format_short'), strtotime($latest_comment['date_added'])) ,
				'href' => $this->url->link('mpblog/mpblogcomment/edit', 'token=' .$this->session->data['token'] .'&mpblogcomment_id=' . $latest_comment['mpblogcomment_id'], true ) ,
			);
		}

		$data['all_comments'] = $this->url->link('mpblog/mpblogcomment', 'token=' .$this->session->data['token'] , true );


		// Top Rated Blogs
		$filter_data = array(
			'sort' => 'rating',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 5,
		);

		$toprated_blogs = $this->model_mpblog_mpblogpost->getMpBlogPosts($filter_data);

		$data['toprated_blogs'] = array();

		foreach($toprated_blogs as $toprated_blog) {
			if(!empty($toprated_blog['image']) && file_exists(DIR_IMAGE . $toprated_blog['image'])) {
				$thumb = $this->model_tool_image->resize($toprated_blog['image'], 40, 40);
			} else {
				$thumb = $this->model_tool_image->resize('no_image.png', 40, 40);
			}
			$data['toprated_blogs'][] = array(
				'thumb' => $thumb,
				'name' => $toprated_blog['name'],
				'author' => $toprated_blog['author'],
				'likes' => $toprated_blog['likes'],
				'viewed' => $toprated_blog['viewed'],
				'rating' => round($toprated_blog['rating'], 2),
				'date_available' => date($this->language->get('date_format_short'), strtotime($toprated_blog['date_available'])) ,
				'status' => ($toprated_blog['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'href' => $this->url->link('mpblog/mpblogpost/edit', 'token=' .$this->session->data['token'] .'&mpblogpost_id=' . $toprated_blog['mpblogpost_id'], true ) ,
			);
		}

		// Top Viewed Blogs
		$filter_data = array(
			'sort' => 'p.viewed',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 5,
		);

		$mostviewed_blogs = $this->model_mpblog_mpblogpost->getMpBlogPosts($filter_data);

		$data['mostviewed_blogs'] = array();

		foreach($mostviewed_blogs as $mostviewed_blog) {
			if(!empty($mostviewed_blog['image']) && file_exists(DIR_IMAGE . $mostviewed_blog['image'])) {
				$thumb = $this->model_tool_image->resize($mostviewed_blog['image'], 40, 40);
			} else {
				$thumb = $this->model_tool_image->resize('no_image.png', 40, 40);
			}
			$data['mostviewed_blogs'][] = array(
				'thumb' => $thumb,
				'name' => $mostviewed_blog['name'],
				'author' => $mostviewed_blog['author'],
				'likes' => $mostviewed_blog['likes'],
				'viewed' => $mostviewed_blog['viewed'],
				'rating' => round($mostviewed_blog['rating'],2),
				'date_available' => date($this->language->get('date_format_short'), strtotime($mostviewed_blog['date_available'])) ,
				'status' => ($mostviewed_blog['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'href' => $this->url->link('mpblog/mpblogpost/edit', 'token=' .$this->session->data['token'] .'&mpblogpost_id=' . $mostviewed_blog['mpblogpost_id'], true ) ,
			);
		}


		$data['mpblogmenu'] = $this->load->controller('mpblog/mpblogmenu');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('mpblog/mpblogdashboard', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'mpblog/mpblogdashboard')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

}