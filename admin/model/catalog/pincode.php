<?php
	class ModelCatalogPincode extends Model {
		public function addPincode($pincodes) {
			foreach($pincodes['data'] as $pincode){
				$count = $this->db->query("SELECT count(*) as total FROM pincheck WHERE pincode = '".$this->db->escape($pincode['pin'])."'")->row;
				if($count['total'] == 0 && $pincode['pin'] != ''){
					if(!empty($pincode['delivery'])){
						$pincode['delivery']=$pincode['delivery'];
					}
					else {
						$pincode['delivery']="";
					}
					 $this->db->query("INSERT INTO pincheck (`id`, `pincode`, `service_available`,delivery_option) VALUES (NULL, '".$this->db->escape($pincode['pin'])."', '".$pincode['service']."', '".$pincode['delivery']."')") ;
				}
			}
		}
		public function getTotalpincode($condition) {
			if($condition != ''){
				$condition = "WHERE ".$condition;
			}
			//echo $condition; die;
			$query = $this->db->query("SELECT count(*) as total FROM pincheck p LEFT JOIN pincode_delivery pd ON (p.delivery_option = pd.id) ".$condition);
			return $query->row['total'];
		}
		public function getpincode($filter,$condition) {
			if($condition != ''){
				$condition = "WHERE ".$condition;
			}
			$query = $this->db->query("SELECT p.id,p.pincode,p.service_available,p.delivery_option,pd.delivery_time FROM pincheck p LEFT JOIN pincode_delivery pd ON (p.delivery_option = pd.id) ".$condition." LIMIT " . (int)$filter['start'] . "," . (int)$filter['limit']."");
			return $query->rows;
		}
		public function deletePincode($data) {
			$id = '';
			foreach($data['selected'] as $delete){
				$id .= $delete.",";
			}
			$this->db->query("DELETE FROM pincheck WHERE id in (".substr($id,0,-1).")");
		}
		public function deleteallpincodes(){
			$this->db->query("DELETE FROM pincheck");
			
		}
		public function get_Pincode($pinid) {
			$query = $this->db->query("SELECT p.id,p.pincode,p.service_available,p.delivery_option,pd.delivery_time FROM pincheck p LEFT JOIN pincode_delivery pd ON (p.delivery_option = pd.id) WHERE p.id = '".$pinid."'");
			return $query->row;
		}
		public function updatePincode($data) {
			$this->db->query("UPDATE pincheck SET pincode = '".$this->db->escape($data['pincode'])."', service_available = '".$data['service']."',delivery_option = '".$data['delivery']."' WHERE id = '".$data['pincode_id']."'");
		}
		public function adddelivery($deliveries) {
			foreach($deliveries['data'] as $delivery){
				$count = $this->db->query("SELECT count(*) as total FROM pincode_delivery WHERE delivery_time = '".$this->db->escape($delivery['delivery'])."'")->row;
				if($count['total'] == 0 && $delivery['delivery'] != ''){
					$this->db->query("INSERT INTO pincode_delivery SET delivery_time = '".$this->db->escape($delivery['delivery'])."'") ;
				}
			}
		}
		public function getTotaldelivery() {
			$query = $this->db->query("SELECT count(*) as total FROM pincode_delivery");
			return $query->row['total'];
		}
		public function getdelivery($filter) {
			$query = $this->db->query("SELECT * FROM pincode_delivery LIMIT " . (int)$filter['start'] . "," . (int)$filter['limit']."");
			return $query->rows;
		}
		public function deletedelivery($data) {
			$id = '';
			foreach($data['selected'] as $delete){
				$id .= $delete.",";
			}
			$this->db->query("DELETE FROM pincode_delivery WHERE id in (".substr($id,0,-1).")");
		}
		public function get_delivery($deliveryid) {
			$query = $this->db->query("SELECT * FROM pincode_delivery WHERE id = '".$deliveryid."'");
			return $query->row;
		}
		public function updatedelivery($data) {
			$this->db->query("UPDATE pincode_delivery SET delivery_time = '".$this->db->escape($data['delivery'])."' WHERE id = '".$data['delivery_id']."'");
		}
		public function getmydelivery() {
			$query = $this->db->query("SELECT * FROM pincode_delivery");
			return $query->rows;
		}
		public function uploadpincode($pincodes,$service,$delivery) {
			foreach($pincodes as $pincode){
				$count = $this->db->query("SELECT count(*) as total FROM pincheck WHERE pincode = '".$this->db->escape($pincode)."'")->row;
				if($count['total'] == 0){
					 $this->db->query("INSERT INTO pincheck (`id`, `pincode`, `service_available`,delivery_option) VALUES (NULL, '".$this->db->escape($pincode)."', '".$service."', '".$this->db->escape($delivery)."')") ;
				}
			}
		}
		public function is_install(){
			$pincode_install_status = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "extension WHERE type = 'module' AND code = 'pincode'")->row;
			return $pincode_install_status;
		}
		public function createTable(){
			
			$this->db->query("CREATE TABLE IF NOT EXISTS `pincheck` (`id` int(255) NOT NULL AUTO_INCREMENT, `pincode` varchar(10) NOT NULL, `service_available` varchar(255) NOT NULL,`delivery_option` int(255) NOT NULL, PRIMARY KEY (`id`) , INDEX (pincode))");
			 $this->db->query("CREATE TABLE IF NOT EXISTS `pincode_delivery` (`id` int(255) NOT NULL AUTO_INCREMENT,`delivery_time` varchar(255) NOT NULL,PRIMARY KEY (`id`))");
			//return $query->rows;
		}
		public function csvdownload($condition) {
			if($condition != ''){
				$condition = "WHERE ".$condition;
			}
			$query = $this->db->query("SELECT p.pincode FROM pincheck p LEFT JOIN pincode_delivery pd ON (p.delivery_option = pd.id) ".$condition);
			return $query->rows;
		}
}