<?php
class ModelCatalogRecipe extends Model {
	public function addRecipe($data) {
		foreach ($data['recipe_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "recipe SET category_id = '". $this->db->escape($value['category_id']) ."', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', tag = '" . $this->db->escape($value['tag']) . "', description = '" . $this->db->escape($value['description']) . "', prep_time = '" . (int)$value['prep_time'] . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', author = '". $this->db->escape($value['author']) ."', youtube_url = '". $this->db->escape($value['youtube_url']) ."', sort_order = '". $this->db->escape($value['sort_order']) ."', status = '". $this->db->escape($value['status']) ."', directions = '". $this->db->escape($value['directions']) ."', ingredients = '". $this->db->escape($value['ingredients']) ."', is_winner = '". $this->db->escape($value['is_winner']) ."', contest_name = '". $this->db->escape($value['contest_name']) ."', image = '". $this->db->escape($data['image']) ."', homepage_image = '". $this->db->escape($data['homepage_image']) ."', date_added = NOW()");
		}
		
		$recipe_id = $this->db->getLastId(); 
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'recipe_id=" . (int)$recipe_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
		}
		
		if (isset($data['recipe_related'])) {
    	    foreach ($data['recipe_related'] as $related_id) {
    	        $this->db->query("INSERT INTO " . DB_PREFIX . "recipe_related_product SET recipe_id = '" . (int)$recipe_id . "', product_id = '" . (int)$related_id . "'");
    	      }
        }
        if (isset($data['related_recipe'])) {
    	    foreach ($data['related_recipe'] as $related_recipe_id) {
    	        $this->db->query("INSERT INTO " . DB_PREFIX . "recipes_related_recipe SET recipe_id = '" . (int)$recipe_id . "', related_recipe_id = '" . (int)$related_recipe_id . "'");
    	      }
        }
        
        if (isset($data['recipe_filter'])) {
			foreach ($data['recipe_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "recipe_filter SET recipe_id = '" . (int)$recipe_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}
		
		$this->cache->delete('recipe');
		
		return $recipe_id;
	}

	public function editRecipe($recipe_id, $data) {
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "recipe WHERE recipe_id = '" . (int)$recipe_id . "'");
	
		foreach ($data['recipe_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "recipe SET recipe_id = '" . (int)$recipe_id . "', category_id = '". $this->db->escape($data['category_id']) ."', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', tag = '" . $this->db->escape($value['tag']) . "', description = '" . $this->db->escape($value['description']) . "', prep_time = '" . (int)$value['prep_time'] . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', author = '". $this->db->escape($value['author']) ."', youtube_url = '". $this->db->escape($value['youtube_url']) ."', sort_order = '". $this->db->escape($value['sort_order']) ."', status = '". $this->db->escape($value['status']) ."', directions = '". $this->db->escape($value['directions']) ."', ingredients = '". $this->db->escape($value['ingredients']) ."', is_winner = '". $this->db->escape($value['is_winner']) ."', contest_name = '". $this->db->escape($value['contest_name']) ."', date_added = '" . $this->db->escape($value['date_added']) . "', date_modified = NOW()"); 
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'recipe_id=" . (int)$recipe_id. "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "recipe SET image = '" . $this->db->escape($data['image']) . "' WHERE recipe_id = '" . (int)$recipe_id . "'");
		}
		
		if (isset($data['homepage_image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "recipe SET homepage_image = '" . $this->db->escape($data['homepage_image']) . "' WHERE recipe_id = '" . (int)$recipe_id . "'");
		}
		
		

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'recipe_id=" . (int)$recipe_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "recipe_related_product WHERE recipe_id = '" . (int)$recipe_id . "'");
	    if (isset($data['recipe_related'])) {
        	foreach ($data['recipe_related'] as $related_id) {
        	    $this->db->query("INSERT INTO " . DB_PREFIX . "recipe_related_product SET recipe_id = '" . (int)$recipe_id . "', product_id = '" . (int)$related_id . "'");
        	}	
	    }
	    
	    $this->db->query("DELETE FROM " . DB_PREFIX . "recipes_related_recipe WHERE recipe_id = '" . (int)$recipe_id . "'");
	    if (isset($data['related_recipe'])) {
    	    foreach ($data['related_recipe'] as $related_recipe_id) {
    	        $this->db->query("INSERT INTO " . DB_PREFIX . "recipes_related_recipe SET recipe_id = '" . (int)$recipe_id . "', related_recipe_id = '" . (int)$related_recipe_id . "'");
    	      }
        }
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "recipe_filter WHERE recipe_id = '" . (int)$recipe_id . "'");

		if (isset($data['recipe_filter'])) {
			foreach ($data['recipe_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "recipe_filter SET recipe_id = '" . (int)$recipe_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}
		$this->cache->delete('recipe');
	}

	public function deleteRecipe($recipe_id) {
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "recipe WHERE recipe_id = '" . (int)$recipe_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'recipe_id=" . (int)$recipe_id . "'");
		
		$this->db->query("DELETE * FROM " . DB_PREFIX . "recipe_related_product WHERE recipe_id= '" . (int)$recipe_id . "' ");
		
		$this->db->query("DELETE * FROM " . DB_PREFIX . "recipes_related_recipe WHERE recipe_id= '" . (int)$recipe_id . "' ");

		$this->cache->delete('recipe');
	}	

	public function getRecipe($recipe_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'recipe_id=" . (int)$recipe_id . "') AS keyword FROM " . DB_PREFIX . "recipe WHERE recipe_id = '" . (int)$recipe_id . "'");
		return $query->row;
	}

	public function getRecipies($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM ". DB_PREFIX ."recipe WHERE recipe_id != 0";
			
			if(isset($data['filter_name'])) {
			    $sql .= " AND title LIKE '%" . $data['filter_name'] . "%'";
			}
			
			if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		   }
		
		   if (isset($data['filter_category']) && !is_null($data['filter_category'])) {
			$sql .= " AND category_id = '" . (int)$data['filter_category'] . "'";
		   }
		
			
			$sql .= " GROUP BY recipe_id";

		$sort_data = array(
		    'recipe_id',
			'name',
			'category_id',
			'status',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$recipe_data = $this->cache->get('recipe.' . (int)$this->config->get('config_language_id'));

			if (!$recipe_data) {
				$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."recipe");

				$recipe_data = $query->rows;

				$this->cache->set('recipe.' . (int)$this->config->get('config_language_id'), $recipe_data);
			}	

			return $recipe_data;			
		}
	}
	
	public function getRecipeDescriptions($recipe_id) {
		$recipe_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recipe WHERE recipe_id = '" . (int)$recipe_id . "'");

		foreach ($query->rows as $result) {
			$recipe_description_data[$result['language_id']] = array(
				'category_id' => $result['category_id'],
				'title'       => $result['title'],
				'date_added'  => $result['date_added'],
				'prep_time'  => $result['prep_time'] > 0 ? $result['prep_time'] : '',
				'description' => $result['description'],				
				'directions'  => $result['directions'],
				'tag'           => $result['tag'],
				'meta_title'  => $result['meta_title'],
				'meta_description'  => $result['meta_description'],
				'meta_keyword'  => $result['meta_keyword'],
				'ingredients' => $result['ingredients'],
				'is_winner'     => $result['is_winner'],
				'contest_name' => $result['contest_name'],
				'author'      => $result['author'],
				'youtube_url'      => $result['youtube_url'],
				'sort_order' => $result['sort_order'],
				'status' => $result['status']
				);
		}

		return $recipe_description_data;
	}

	

	public function getTotalRecipies($data = array()) {
		
			$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "recipe";
			
			$sql .= " WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

			if (!empty($data['filter_name'])) {
				$sql .= " AND title LIKE '" . $this->db->escape($data['filter_name']) . "%'";
			}		
			
			if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
				$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
			}
			
			if (isset($data['filter_category']) && !is_null($data['filter_category'])) {
				$sql .= " AND category_id = '" . (int)$data['filter_category'] . "'";
			}		

			$query = $this->db->query($sql);

			return $query->row['total'];
		
	}	
	
	//Related products
	public function getRecipeRelatedProducts($recipe_id) {
		$recipe_related_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recipe_related_product WHERE recipe_id = '" . (int)$recipe_id . "'");

		foreach ($query->rows as $result) {
			$recipe_related_data[] = $result['product_id'];
		}

		return $recipe_related_data;
	}
	
	//Related Recipes
	public function getRecipesRelatedRecipe($recipe_id) {
		$related_recipe_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recipes_related_recipe WHERE recipe_id = '" . (int)$recipe_id . "'");
		foreach ($query->rows as $result) {
		    $recipe_data_query = $this->db->query("SELECT title FROM " . DB_PREFIX . "recipe WHERE recipe_id = '" . (int)$result['related_recipe_id']. "'");
			$related_recipe_data[] = array(
			    'related_recipe_id'     => $result['related_recipe_id'],
				'title'         => $recipe_data_query->row['title']
			    );
		}
		return $related_recipe_data;
	}
	
	public function getRecipeFilters($recipe_id) {
		$recipe_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recipe_filter WHERE recipe_id = '" . (int)$recipe_id . "'");

		foreach ($query->rows as $result) {
			$recipe_filter_data[] = $result['filter_id'];
		}

		return $recipe_filter_data;
	}
	
	
	public function getProduct($product_id) {
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."product_description WHERE product_id = '".(int)$product_id."' ");

		return $query->row;
	}
	
	public function getProducts($data = array()) {
		
			$sql = "SELECT * FROM ". DB_PREFIX ."product_description WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ";			
			
			if (!empty($data['product_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['product_name']) . "%'";
		}
				
			$sql .= " GROUP BY product_id";

		$sort_data = array(
			'name',
			'product_id'			
		);		

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			
			$query = $this->db->query($sql);

			return $query->rows;		
	}
	
	// category
	
	public function getCategory($c_id) {
		$sql = "SELECT * FROM ". DB_PREFIX ."recipe_category WHERE category_id = '". (int)$c_id ."' ";

		$query = $this->db->query($sql);

		return $query->row;
	}
	
	public function getCategories() {
		$sql = "SELECT * FROM ". DB_PREFIX ."recipe_category";

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getCategoryName($category_id) {
		$sql = "SELECT name FROM ". DB_PREFIX ."recipe_category WHERE category_id = '". (int)$category_id ."'";

		$query = $this->db->query($sql);

		return $query->row['name'];
	}
	
	public function getCategoryIdByRecipeId($recipe_id)	{
		$sql = "SELECT category_id FROM ". DB_PREFIX ."recipe WHERE recipe_id = '". (int)$recipe_id ."' ";

		$query = $this->db->query($sql);
		
		return $query->row['category_id'];

	}
}
?>