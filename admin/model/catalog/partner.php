<?php
class ModelCatalogPartner extends Model {

	public function getTotalPartners() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "partner";
		
		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	
	public function getPartnerList() {
		$query  = $this->db->query("SELECT * FROM ". DB_PREFIX ."partner ORDER BY partner_id DESC");
		return $query->rows;
	}
	
	public function getPartner($partner_id) {
		$query  = $this->db->query("SELECT * FROM ". DB_PREFIX ."partner WHERE partner_id = '". (int)$partner_id ."'");
		return $query->row;
	}
	
	public function deletePartner($partner_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "partner WHERE partner_id = '" . (int)$partner_id . "'");

		$this->cache->delete('partner');
	}
	
}