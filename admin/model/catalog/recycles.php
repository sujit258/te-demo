<?php
class ModelCatalogRecycles extends Model {
	
	
	public function editRecycles($recycle_id, $data) {			
		
		if ($data) {
			$this->db->query("UPDATE " . DB_PREFIX . "recycles SET name = '" . $this->db->escape($data['name']) ."', email = '". $this->db->escape($data['email']) ."', mobile = '". $this->db->escape($data['mobile']) ."', address = '". $this->db->escape($data['address']) ."', total_packets = '". $this->db->escape($data['total_packets']) ."', status_id = '". $this->db->escape($data['recycle_status']) ."' WHERE recycle_id = '". (int)$recycle_id ."' ");
		}		

		
	}

	public function getTotalRecycles($data = array())
	{
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "recycles";
			
			$sql .= " WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

			if (!empty($data['filter_name'])) {
				$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
			}

			if (!empty($data['filter_mobile'])) {
				$sql .= " AND mobile LIKE '" . $this->db->escape($data['filter_mobile']) . "%'";
			}
			
			if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
				$sql .= " AND status_id = '" . (int)$data['filter_status'] . "'";
			}				

			$query = $this->db->query($sql);

			return $query->row['total'];
		/*$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "recycles";
		
		$query = $this->db->query($sql);

		return $query->row['total'];*/
	}
	
	
	public function getRecyclesList($data = array())
	{
		$sql = "SELECT * FROM ". DB_PREFIX ."recycles WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ";			
			
		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_mobile'])) {
			$sql .= " AND mobile LIKE '" . $this->db->escape($data['filter_mobile']) . "%'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND status_id = '" . (int)$data['filter_status'] . "'";
		}
		
		
		$sql .= " GROUP BY recycle_id";

		$sort_data = array(
			'name',			
			'status_id',
			'mobile'
		);		

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			
			$query = $this->db->query($sql);

			return $query->rows;
		/*$query  = $this->db->query("SELECT * FROM ". DB_PREFIX ."recycles");
		return $query->rows;*/
	}
	
	public function getRecycle($recycle_id)
	{
		$query  = $this->db->query("SELECT * FROM ". DB_PREFIX ."recycles WHERE recycle_id = '". (int)$recycle_id ."'");
		return $query->row;
	}

	public function getRecycles($data = array())
	{
		$sql = "SELECT * FROM ". DB_PREFIX ."recycles WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ";
			
		if (isset($data['recycle_id'])) {
			$sql .= " AND recycle_id = '" . (int)$data['recycle_id'] . "'";
		}
			
		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}
	
		if (!empty($data['filter_mobile'])) {
			$sql .= " AND mobile LIKE '" . $this->db->escape($data['filter_mobile']) . "%'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND status_id = '" . (int)$data['filter_status'] . "'";
		}

		$sql .= " GROUP BY recycle_id";

		$sort_data = array(
			'name',
			'mobile',
			'recycles_id',
			'status_id'			
		);		

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

		$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function deleteRecycles($recycle_id)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "recycles WHERE recycle_id = '" . (int)$recycle_id . "'");

		$this->cache->delete('recycle');
	}
	
	public function getRecyclesStatuses()
	{
		$query  = $this->db->query("SELECT * FROM ". DB_PREFIX ."recycles_status");
		return $query->rows;
	}
	
	public function getStatus($status_id)
	{
		$query  = $this->db->query("SELECT name FROM ". DB_PREFIX ."recycles_status WHERE status_id = '". $status_id ."' ");
		return $query->row['name'];
	}
	
	public function updateStatus($recycle_id,$status_id)
	{
		$this->db->query("DELETE status_id FROM " . DB_PREFIX . "recycles WHERE recycle_id = '" . (int)$recycle_id . "'");
		
		$this->db->query("INSERT INTO ". DB_PREFIX ."recycles SET status_id = '". (int)$status_id ."' ");		
	}
	
}