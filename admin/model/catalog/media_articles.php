<?php
class ModelCatalogMediaArticles extends Model {
  
    public function addMediaArticle($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "media_articles SET media_name = '" . $data['media_name'] ."',content = '" . $data['content'] ."', publish_date = '" . $data['publish_date'] . "', url = '" . $data['url'] ."', image = '".$this->db->escape($data['image'])."', sort_order = '" . (int)$data['sort_order'] . "', date_added  = NOW()");
 
    }


    public function editMediaArticle($media_articles_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "media_articles SET media_name = '" . $data['media_name'] ."',content = '" . $data['content'] ."', publish_date = '" . $data['publish_date'] . "',status = '" . (int)$data['status'] . "', url = '" . $data['url'] ."', image = '".$this->db->escape($data['image'])."', sort_order = '" . (int)$data['sort_order'] . "' WHERE media_articles_id = '" . (int)$media_articles_id . "'");
    }

    public function deleteMediaArticle($media_articles_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "media_articles WHERE media_articles_id = '" . (int)$media_articles_id . "'"); 
    }

    public function getMediaArticle($media_articles_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "media_articles WHERE media_articles_id = '" . (int)$media_articles_id . "' LIMIT 1");
        return $query->row;
    }

    public function getMediaArticles($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "media_articles";

            $sort_data = array(
                'media_articles_id',
                'media_name',
                'sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY media_articles_id";
            }

            if (isset($data['order']) && ($data['order'] == 'ASC')) {
                $sql .= " ASC";
            } else {
                $sql .= " DESC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }
                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }
                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }
            $query = $this->db->query($sql);
            return $query->rows;
        } 
    }
    
    public function getTotalMediaArticles() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "media_articles");

        return $query->row['total'];
    }

}
?>