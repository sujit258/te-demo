<?php
class ModelCatalogDeals extends Model {
	public function addDeal($data) {
	    //$date_end = date("Y-m-d", strtotime($data['date_start'] . "+2 DAYS"));
		$this->db->query("INSERT INTO " . DB_PREFIX . "deals SET product_id = '" . (int)$data['product_id'] . "', price = '" . (int)$data['price'] . "', discount = '" . (int)$data['discount'] . "', option_id = '" . (int)$data['option_id'] . "', option_value_id = '" . (int)$data['option_value_id'] . "', homepage = '" . (int)$data['homepage'] . "', coupon = '" . $this->db->escape($data['coupon']) . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "'");
	}

	public function editDeal($deal_id, $data) {
	    //$date_end = date("Y-m-d", strtotime($data['date_start'] . "+2 DAYS"));
		$this->db->query("UPDATE " . DB_PREFIX . "deals SET product_id = '" . (int)$data['product_id'] . "', price = '" . (int)$data['price'] . "', discount = '" . (int)$data['discount'] . "', option_id = '" . (int)$data['option_id'] . "', option_value_id = '" . (int)$data['option_value_id'] . "', homepage = '" . (int)$data['homepage'] . "', coupon = '" . $this->db->escape($data['coupon']) . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "' WHERE deal_id = '" . (int)$deal_id . "'");
	}

	public function getDeal($deal_id) {
		$query = $this->db->query("SELECT d.*, pd.name, p.price AS product_price, p.tax_class_id FROM " . DB_PREFIX . "deals d LEFT JOIN " . DB_PREFIX . "product_description pd ON (d.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pd.product_id = p.product_id) WHERE d.deal_id = '" . (int)$deal_id . "'");
		
		return $query->row;
	}

	public function getDeals($data = array()) {
		$sql = "SELECT d.*, pd.name FROM " . DB_PREFIX . "deals d LEFT JOIN " . DB_PREFIX . "product_description pd ON (d.product_id = pd.product_id) WHERE d.deal_id > 0";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (isset($data['filter_price']) && $data['filter_price'] > 0) {
			$sql .= " AND d.price = '" . $data['filter_price'] . "%'";
		}
		
		if (!empty($data['filter_sku'])) {
			$sql .= " AND d.option_value_id = '" . $data['filter_sku'] . "'";
		}
		
		if (isset($data['filter_homepage']) && $data['filter_homepage'] != '*' && $data['filter_homepage'] != '') {
			$sql .= " AND d.homepage = '" . $data['filter_homepage'] . "'";
		}
		
		if (!empty($data['filter_coupon']) && !is_null($data['filter_coupon'])) {
			$sql .= " AND d.coupon LIKE '%" . $this->db->escape($data['filter_coupon']) . "%'";
		}
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(d.date_start) = DATE('" . $this->db->escape($data['filter_date_start']) . "')";
		}
		
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(d.date_end) = DATE('" . $this->db->escape($data['filter_date_end']) . "')";
		}
		
		$sort_data = array(
			'pd.name',
			'd.price',
			'd.option_value_id',
			'd.homepage',
			'd.coupon',
			'd.date_start',
			'd.date_end'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY d.date_end";
		}

		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getProductOptions($product_id) {
		$product_option_data = array();
		
		$product_price_query = $this->db->query("SELECT price, tax_class_id FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "' LIMIT 1");
		$product_price = $product_price_query->row['price'];
		$tax_class_id = $product_price_query->row['tax_class_id'];

		$product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();

			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' ORDER BY ov.sort_order ASC");

			foreach ($product_option_value_query->rows as $product_option_value) {
			    $price = 0;
			    $price_with_option = 0;
			    if($product_option_value['price_prefix'] == '+') {
			        $price_with_option = $product_price + $product_option_value['price'];
			    } else if($product_option_value['price_prefix'] == '-') {
			        $price_with_option = $product_price - $product_option_value['price'];
			    }
			    
			    $price = $this->currency->format($this->tax->calculate($price_with_option, $tax_class_id, $this->config->get('config_tax')));
			    
				$product_option_value_data[] = array(
					'option_value_id'           => $product_option_value['option_value_id'],
					'name'                      => $product_option_value['name'],
					'price'                     => $price
				);
			}

			$product_option_data[] = array(
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
			);
		}

		return $product_option_data;
	}

	public function getTotalDeals($data = array()) {
		$sql = "SELECT COUNT(d.deal_id) AS total FROM " . DB_PREFIX . "deals d LEFT JOIN " . DB_PREFIX . "product_description pd ON (d.product_id = pd.product_id) WHERE d.deal_id > 0";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (isset($data['filter_price']) && $data['filter_price'] > 0) {
			$sql .= " AND d.price = '" . $data['filter_price'] . "%'";
		}
		
		if (!empty($data['filter_sku'])) {
			$sql .= " AND d.option_value_id = '" . $this->db->escape($data['filter_sku']) . "%'";
		}
		
		if (isset($data['filter_homepage'])) {
			$sql .= " AND d.homepage = '" . $data['filter_homepage'] . "'";
		}
		
		if (!empty($data['filter_coupon']) && !is_null($data['filter_coupon'])) {
			$sql .= " AND d.coupon LIKE '" . $this->db->escape($data['filter_coupon']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getSkuName($option_value_id) {
	    $query = $this->db->query("SELECT name FROM " . DB_PREFIX . "option_value_description WHERE option_value_id = '" . (int)$option_value_id . "' LIMIT 1");
	    return $query->row['name'];
	}
	
	public function getSkuList() {
	    $query = $this->db->query("SELECT option_value_id, name FROM " . DB_PREFIX . "option_value_description WHERE option_id = 1");
	    return $query->rows;
	}
}
?>