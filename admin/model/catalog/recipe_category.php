<?php
class ModelCatalogRecipeCategory extends Model {
	public function addCategory($data) {		

		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "recipe_category SET language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', status = '". (int)$data['status'] ."',image = '" . $this->db->escape($data['image']) . "' ");
		}
		
		$category_id = $this->db->getLastId();
		
		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'recipe_category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
		}

		$this->cache->delete('recipe_category');

		return $category_id;
				
	}

	public function editCategory($category_id, $data) {
		
		foreach ($data['category_description'] as $language_id => $value) {
			
			$this->db->query("UPDATE " . DB_PREFIX . "recipe_category SET language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', status = '". (int)$data['status'] ."',image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '". (int)$category_id ."' ");
		}		
		
		if ($data['keyword']) {
		    $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'recipe_category_id=" . (int)$category_id . "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'recipe_category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
		}

		$this->cache->delete('recipe_category');
	}

	public function deleteCategory($category_id) {
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "recipe_category WHERE category_id = '" . (int)$category_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'recipe_category_id=" . (int)$category_id . "'");
		
		$this->cache->delete('recipe_category');
	}

	public function getCategory($category_id) {
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."recipe_category WHERE category_id = '" . $category_id . "'");
		return $query->row;
	}

	public function getCategories($data = array()) {
		$sql = "SELECT * FROM ". DB_PREFIX ."recipe_category";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY category_id";

		$sort_data = array(
			'name',
			// 'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCategoryDescriptions($category_id) {
		
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recipe_category WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description']
			);
		}

		return $category_description_data;
	}
	
	public function getKeyword($category_id) {
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."url_alias WHERE query = 'recipe_category_id=" . (int)$category_id . "'");
		
		return $query->row['keyword'];
	}

	public function getTotalCategories() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "recipe_category");

		return $query->row['total'];
	}	
}
