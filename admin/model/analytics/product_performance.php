<?php
class ModelAnalyticsProductPerformance extends Model {
	public function getProductsData($data = array()) {
	    $categories = $this->getMappedCategories();
	    foreach($categories AS $category) {
	        $result[] = array(
	            'name'  => $category['name'],
	            'href'  => $this->url->link('analytics/product_performance', 'category=' . $category['name'] . '&token=' . $this->session->data['token'], 'SSL')
	        );
	    }
	    foreach($data AS $date) {
	        if(date('m') == date('m', strtotime($date['date_start']))) {
	            $divider = date('d');
	            $total_days = date('t');
	        } else {
	            $divider = date('t', strtotime($date['date_start']));
	            $total_days = $divider;
	        }
	        
	        $query = $this->db->query("SELECT SUM(op.total + (op.tax * op.quantity)) AS total_sales, SUM(op.quantity) AS total_units, cm.name FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order o USING (order_id) LEFT JOIN " . DB_PREFIX . "product p USING (product_id) LEFT JOIN " . DB_PREFIX . "category_mapping cm USING (product_id) WHERE p.status = 1 AND (o.order_status_id IN (3,5)) AND (DATE(o.date_shipped) BETWEEN '" . $this->db->escape($date['date_start']) . "' AND '" . $this->db->escape($date['date_end']) . "') GROUP BY cm.name");
	        
	        foreach($query->rows AS $monthwise_data) {
	            if($monthwise_data['name'] != '') {
	                $key = array_search($monthwise_data['name'], array_column($result, 'name'));
	                $result[$key][strtolower(date('M_y', strtotime($date['date_start'])))] = array(
	                    'weekly_units_ordered'  => round($monthwise_data['total_units'] / $divider * 7),
                        'monthly_units_ordered' => round($monthwise_data['total_units'] / $divider * $total_days),
                        'monthly_sales'         => round($monthwise_data['total_sales'] / $divider * $total_days)
	                );
	            }
	        }
	    }
		return $result;
	}
	
	public function getDailyProductData($category, $date_start, $date_end) {
	    $products_query = $this->db->query("SELECT DISTINCT(p.product_id), pd.name, ovd.name AS weight, ovd.old_name AS old_weight FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) LEFT JOIN " . DB_PREFIX . "product_option_value pov USING (product_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd USING (option_value_id) LEFT JOIN " . DB_PREFIX . "category_mapping cm USING (product_id) WHERE p.status = 1 AND cm.name LIKE '" . $this->db->escape($category) . "' GROUP BY p.product_id, pd.name, weight");
	    
	    foreach($products_query->rows AS $product) {
	        $result[] = array(
	            'product_id'    => $product['product_id'],
	            'pid_weight'    => $product['product_id'] . strtolower(str_replace([' ', '*', '.'], '' , $product['weight'])) . strtolower(str_replace([' ', '*', '.'], '' , $product['old_weight'])),
	            'name'          => strlen($product['weight']) > 0 ? $product['name'] . " " . $product['weight'] : $product['name'],
	            'href'                  => $this->url->link('catalog/product/edit', 'product_id=' . $product['product_id'] . '&token=' . $this->session->data['token'], 'SSL')
	        );
	    }
	        
	    $query = $this->db->query("SELECT DATE(o.date_shipped) AS date_shipped, p.product_id, pd.name, ovd.name AS weight, ovd.old_name AS old_weight, op.quantity AS total_units, op.total + (op.tax * op.quantity) AS total_sales FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order o USING (order_id) LEFT JOIN " . DB_PREFIX . "order_option oo USING (order_product_id) LEFT JOIN " . DB_PREFIX . "category_mapping cm USING (product_id) LEFT JOIN oc_product p USING (product_id) LEFT JOIN oc_product_description pd USING (product_id) LEFT JOIN oc_product_option_value pov USING (product_id) LEFT JOIN oc_option_value_description ovd USING (option_value_id) WHERE op.product_id = p.product_id AND (oo.value LIKE ovd.name OR oo.value LIKE ovd.old_name) AND o.order_status_id IN (3,5) AND (DATE(o.date_shipped) BETWEEN '" . $this->db->escape($date_start) . "' AND '" . $this->db->escape($date_end) . "')  AND p.status = 1 AND cm.name LIKE '" . $this->db->escape($category) . "' GROUP BY date_shipped, p.product_id, pd.name, weight");
	        
        foreach($query->rows AS $daily_data) {
            if($daily_data['name'] != '') {
                $pid_weight = $daily_data['product_id'] . strtolower(str_replace([' ', '*', '.'], '' , $daily_data['weight'])) . strtolower(str_replace([' ', '*', '.'], '' , $daily_data['old_weight']));
                $key = array_search($pid_weight, array_column($result, 'pid_weight'));
                if(is_array($result[$key][$daily_data['date_shipped']])) {
                    if(array_key_exists('total_units', $result[$key][$daily_data['date_shipped']])) {
                        $result[$key][$daily_data['date_shipped']]['total_units'] += round($daily_data['total_units']);
                    }
                    if(array_key_exists('total_sale', $result[$key][$daily_data['date_shipped']])) {
                        $result[$key][$daily_data['date_shipped']]['total_sale'] += round($daily_data['total_sales']);
                    }
                } else {
                    $result[$key][$daily_data['date_shipped']] = array(
                        'total_sale'    => $daily_data['total_sales'],
                        'total_units'   => $daily_data['total_units']
                    );
                }
            }
        }
	    return $result;
	}
	
	public function getCategoryData($category, $data = array()) {
	    $products_query = $this->db->query("SELECT p.product_id, p.product_type, pd.name, ovd.name AS weight, ovd.old_name AS old_weight, cm.name AS category FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) LEFT JOIN " . DB_PREFIX . "product_option_value pov USING (product_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd USING (option_value_id) LEFT JOIN " . DB_PREFIX . "category_mapping cm USING (product_id) WHERE p.status = 1 AND cm.name LIKE '" . $this->db->escape($category) . "' GROUP BY p.product_id, pd.name, weight");
	    
	    foreach($products_query->rows AS $product) {
	        $result[] = array(
	            'product_id'    => $product['product_id'],
	            'pid_weight'    => $product['product_id'] . strtolower(str_replace([' ', '*', '.'], '' , $product['weight'])) . strtolower(str_replace([' ', '*', '.'], '' , $product['old_weight'])),
	            'name'          => strlen($product['weight']) > 0 ? $product['name'] . " " . $product['weight'] : $product['name'],
	            'href'                  => $this->url->link('catalog/product/edit', 'product_id=' . $product['product_id'] . '&token=' . $this->session->data['token'], 'SSL')
	        );
	    }
	    foreach($data AS $date) {
	        if(date('m') == date('m', strtotime($date['date_start']))) {
	            $divider = date('d');
	            $total_days = date('t');
	        } else {
	            $divider = date('t', strtotime($date['date_start']));
	            $total_days = $divider;
	        }
	        
	        $query = $this->db->query("SELECT p.product_id, p.product_type, pd.name, ovd.name AS weight, ovd.old_name AS old_weight, (SELECT SUM(op.quantity) FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order o USING (order_id) LEFT JOIN " . DB_PREFIX . "order_option oo USING (order_product_id) WHERE op.product_id = p.product_id AND (oo.value LIKE ovd.name OR oo.value LIKE ovd.old_name) AND o.order_status_id IN (3,5) AND (DATE(o.date_shipped) BETWEEN '" . $this->db->escape($date['date_start']) . "' AND '" . $this->db->escape($date['date_end']) . "')) AS total_units, (SELECT SUM(op.total + (op.tax * op.quantity)) FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order o USING (order_id) LEFT JOIN " . DB_PREFIX . "order_option oo USING (order_product_id) WHERE op.product_id = p.product_id AND (oo.value LIKE ovd.name OR oo.value LIKE ovd.old_name) AND o.order_status_id IN (3,5) AND (DATE(o.date_shipped) BETWEEN '" . $this->db->escape($date['date_start']) . "' AND '" . $this->db->escape($date['date_end']) . "')) AS total_sales FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) LEFT JOIN " . DB_PREFIX . "product_option_value pov USING (product_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd USING (option_value_id) LEFT JOIN " . DB_PREFIX . "category_mapping cm USING (product_id) WHERE p.status = 1 AND cm.name LIKE '" . $this->db->escape($category) . "' GROUP BY p.product_id, pd.name, weight, ovd.old_name");
	        
	        foreach($query->rows AS $monthwise_data) {
	            if($monthwise_data['name'] != '') {
	                $pid_weight = $monthwise_data['product_id'] . strtolower(str_replace([' ', '*', '.'], '' , $monthwise_data['weight'])) . strtolower(str_replace([' ', '*', '.'], '' , $monthwise_data['old_weight']));
	                $key = array_search($pid_weight, array_column($result, 'pid_weight'));
	                $result[$key][strtolower(date('M_y', strtotime($date['date_start'])))] = array(
	                    'weekly_units_ordered'  => round($monthwise_data['total_units'] / $divider * 7),
                        'monthly_units_ordered' => round($monthwise_data['total_units'] / $divider * $total_days),
                        'monthly_sales'         => round($monthwise_data['total_sales'] / $divider * $total_days)
	                );
	            }
	        }
	    }
	    return $result;
	    
	    /*$products_query = $this->db->query("SELECT p.product_id, p.product_type, pd.name, ovd.name AS weight, cm.name AS category FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd USING (product_id) LEFT JOIN " . DB_PREFIX . "product_option_value pov USING (product_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd USING (option_value_id) LEFT JOIN " . DB_PREFIX . "category_mapping cm USING (product_id) WHERE p.status = 1 AND cm.name LIKE '" . $this->db->escape($category) . "' GROUP BY p.product_id, pd.name, weight");
	    
	    foreach($products_query->rows AS $product) {
	        $result[] = array(
	            'product_id'    => $product['product_id'],
	            'name'          => strlen($product['weight']) > 0 ? $product['name'] . " " . $product['weight'] : $product['name'],
	            'href'                  => $this->url->link('catalog/product/edit', 'product_id=' . $product['product_id'] . '&token=' . $this->session->data['token'], 'SSL')
	        );
	    }
	    
	    foreach($data AS $date) {
	        if(date('m') == date('m', strtotime($date['date_start']))) {
	            $divider = date('d');
	            $total_days = date('t');
	        } else {
	            $divider = date('t', strtotime($date['date_start']));
	            $total_days = $divider;
	        }
	        
	        foreach($products_query->rows AS $product) {
	            $query = $this->db->query("SELECT SUM(op.total + (op.tax * op.quantity)) AS total_sales, SUM(op.quantity) AS total_units FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order o USING (order_id) LEFT JOIN " . DB_PREFIX . "order_option oo USING (order_product_id) WHERE (op.product_id = '" . (int)$product['product_id'] . "' AND oo.value LIKE '" . $this->db->escape($product['weight']) . "') AND (o.order_status_id IN (3,5)) AND (DATE(o.date_shipped) BETWEEN '" . $this->db->escape($date['date_start']) . "' AND '" . $this->db->escape($date['date_end']) . "')");
	            
	            if(strlen($product['weight']) > 0) 
	            $key = array_search($product['name'], array_column($result, 'name'));
	            if(is_array($app_data[$key][strtolower(date('M_y', strtotime($date['date_start'])))])) {
	                
	            }
	            
	            if(array_key_exists('monthly_sales', $app_data[$key][$table_suffix])) {
	                $result[$key][strtolower(date('M_y', strtotime($date['date_start'])))] = array(
	                'weekly_units_ordered'  => round($query->row['total_units'] / $divider * 7),
                    'monthly_units_ordered' => round($query->row['total_units'] / $divider * $total_days),
                    'monthly_sales'         => round($query->row['total_sales'] / $divider * $total_days)
	            );
	        }
	    }*/
	}
	
	public function getMappedCategories() {
	    $query = $this->db->query("SELECT DISTINCT name FROM " . DB_PREFIX . "category_mapping");
	    
	    return $query->rows;
	}
}
?>