<?php 
class ModelExtensionModulePreOrder extends Model {
	public function __construct($register) {
		if (!defined('IMODULE_ROOT')) define('IMODULE_ROOT', substr(DIR_APPLICATION, 0, strrpos(DIR_APPLICATION, '/', -2)) . '/');
		if (!defined('IMODULE_SERVER_NAME')) define('IMODULE_SERVER_NAME', substr((defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER), 7, strlen((defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER)) - 8));
		parent::__construct($register);
	}
	
	public function install(){

		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "preorder`
	(`preorder_id` INT(11) NOT NULL AUTO_INCREMENT, 
	 `product_id` INT(11) NULL DEFAULT '0',
	 `order_id` INT(11) NOT NULL DEFAULT '0',
	 `date_created` DATETIME  NOT NULL DEFAULT '0000-00-00 00:00:00',
	 `store_id` int(11) DEFAULT NULL,
	 `language_id` INT(11) NOT NULL DEFAULT '".$this->config->get('config_language_id')."',
 	  PRIMARY KEY (`preorder_id`));");
		
		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "preorder_product`
	(`preorder_product_id` INT(11) NOT NULL AUTO_INCREMENT,
	 `product_id` INT(11) NULL DEFAULT '0',	 
	 `preorder_note` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
	 `preorder_date` DATE  NOT NULL DEFAULT '0000-00-00' ,
	 `language_id` INT(11) NOT NULL DEFAULT '".$this->config->get('config_language_id')."',
	  PRIMARY KEY (`preorder_product_id`));");
		
		$this->load->model('design/layout');
	
		$layouts = $this->model_design_layout->getLayouts();
		foreach ($layouts as $layout) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . $layout['layout_id'] . "', code = 'preorder', position = 'content_top', sort_order = '0'");
		}

	  $this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%PreOrder by iSenseLabs%'");
	  $modifications = $this->load->controller('extension/modification/refresh');	
	}
	
	public function uninstall()	{
		  $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "preorder`");
		  $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "preorder_product`");

		  $this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%PreOrder by iSenseLabs%'");
	  	$modifications = $this->load->controller('extension/modification/refresh');	
	}
	
	public function viewcustomers($page=1, $limit=8, $store_id=0) {    
        
        if ($page) {
                $start = ($page - 1) * $limit;
            }
            $query =  $this->db->query("SELECT super.*, o.*, l.name as language, product.name as product_name FROM `" . DB_PREFIX . "preorder` super 
            LEFT JOIN `" . DB_PREFIX . "product_description` product on (super.product_id = product.product_id AND super.language_id = product.language_id)  
            LEFT JOIN `" . DB_PREFIX . "language` l on super.language_id = l.language_id
            LEFT JOIN `" . DB_PREFIX . "order` o on super.order_id = o.order_id
            WHERE super.language_id = " . (int)$this->config->get('config_language_id') . "
			AND super.store_id = " . (int)$store_id . "
			AND o.order_status_id > 0 
            ORDER BY `date_created` DESC
            LIMIT ".$start.", ".$limit);  
        
        return $query->rows; 
    }
    
	
	   public function viewnotifiedcustomers($page=1, $limit=8, $store_id=0) {    
        if ($page) {
                $start = ($page - 1) * $limit;
            }
            $query =  $this->db->query("SELECT super.*, o.*, l.name as language, product.name as product_name FROM `" . DB_PREFIX . "preorder` super 
            LEFT JOIN `" . DB_PREFIX . "product_description` product on (super.product_id = product.product_id AND super.language_id = product.language_id)  
            LEFT JOIN `" . DB_PREFIX . "language` l on super.language_id = l.language_id
            LEFT JOIN `" . DB_PREFIX . "order_history` oh on super.order_id = oh.order_id
            LEFT JOIN `" . DB_PREFIX . "order` o on super.order_id = o.order_id
            WHERE oh.notify=1 and super.language_id = " . (int)$this->config->get('config_language_id') . "
            AND super.store_id = " . (int)$store_id . "
			AND o.order_status_id > 0 
            ORDER BY `date_created` DESC
            LIMIT ".$start.", ".$limit);
            
        return $query->rows; 
    }
	

	public function getTotalCustomers($store_id=0) {
            $query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "preorder` p
			LEFT JOIN `" . DB_PREFIX . "order` o on p.order_id = o.order_id
			WHERE p.language_id = " . (int)$this->config->get('config_language_id') . "
			AND o.order_status_id > 0 
			AND p.store_id=".$store_id);
    
        return $query->row['count']; 
    }
    
    public function getTotalNotifiedCustomers($store_id=0) {
            $query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "preorder` p
			LEFT JOIN `" . DB_PREFIX . "order` o on p.order_id = o.order_id
            LEFT JOIN `" . DB_PREFIX . "order_history` oh on p.order_id = oh.order_id
            WHERE oh.notify=1 
			AND p.language_id= " . (int)$this->config->get('config_language_id') . " 
			AND o.order_status_id > 0 
			AND p.store_id=".$store_id);
        return $query->row['count']; 
    }
	
	  
	public function getStatistics($store_id=0) {
	 	$run_query = $this->db->query("SELECT * FROM `".DB_PREFIX."preorder` p
			LEFT JOIN `" . DB_PREFIX . "order` o on p.order_id = o.order_id
            LEFT JOIN `" . DB_PREFIX . "order_history` oh on p.order_id = oh.order_id
            WHERE p.store_id = ". $store_id ."
            AND p.language_id =  " . (int)$this->config->get('config_language_id') . "
			AND o.order_status_id > 0  ");
		return $run_query; 
    }
	
	public function sendPreOrderEmail($product_id) {	
		$product_info = $this->model_catalog_product->getProduct($product_id);

		$store_id = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` 
			WHERE product_id = ".$product_id."
			LIMIT 1");	
		$store_id = $store_id->row['store_id'];
		
		$this->load->model('tool/image');
		$preorder = $this->config->get('preorder');

		if ($product_info['image']) { 
			if(isset($preorder['ImageWidth']) && isset($preorder['ImageHeight'])) {
				$image = $this->model_tool_image->resize($product_info['image'], $preorder['ImageWidth'], $preorder['ImageHeight']);
			} else {
				$image = $this->model_tool_image->resize($product_info['image'], 200, 200);
			}
		} else { 
			$image = false;
		}
			
		$customers = $this->db->query("SELECT * FROM `" . DB_PREFIX . "preorder` 
			WHERE product_id = ".$product_id." AND customer_notified=0 and store_id = ".$store_id."
			ORDER BY `date_created` DESC");					
								
				
		foreach($customers->rows as $cust) {
			if(!isset($preorder['EmailText'][$cust['language']])){
				$EmailText = '';
				$EmailSubject = '';
			} else {
				$EmailText = $preorder['EmailText'][$cust['language']];
				$EmailSubject = $preorder['EmailSubject'][$cust['language']];
			}
		
			$string = html_entity_decode($EmailText);
			$patterns = array();
			$patterns[0] = '/{c_name}/';
			$patterns[1] = '/{p_name}/';
			$patterns[2] = '/{p_image}/';
			$patterns[3] = '/http:\/\/{p_link}/';
			$replacements = array();
			$replacements[0] = $cust['customer_name'];
			$replacements[1] = "<a href='".HTTP_CATALOG."index.php?route=product/product&product_id=".$product_id."' target='_blank'>".$product_info['name']."</a>";
			$replacements[2] = "<img src='".$image."' />";
			$replacements[3] = HTTP_CATALOG."index.php?route=product/product&product_id=".$product_id;

			$text = preg_replace($patterns, $replacements, $string);
			
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($cust['customer_email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode($EmailSubject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($text);
			$mail->send();
		}
		
		
	}
}
?>