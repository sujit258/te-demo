<?php
class ModelExtensionModuleInstagramShopGallery extends Model
{
    private $module = array();

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->config->load('isenselabs/instagramshopgallery');
        $this->module = $this->config->get('instagramshopgallery');

        //$this->load->model('setting/event');
    }

    /**
     * Get photos with complete data
     */
    public function getPhotos($page = 1, $limit = 48, $select = '*')
    {
        $query = $this->db->query(
            "SELECT " . $select . ", (SELECT COUNT(*) FROM `" . DB_PREFIX . "instagramshopgallery_to_product` isgp WHERE isgp.shortcode = isg.shortcode) as related_product
            FROM `" . DB_PREFIX . "instagramshopgallery` isg
            GROUP BY isg.shortcode
            ORDER BY isg.timestamp DESC
            LIMIT " . (int)(($page-1) * $limit) . ", " . (int)$limit
        );

        return $query->rows;
    }

    /**
     * Summary used to check if photos feed have related data in database
     */
    public function getPhotosSummary()
    {
        $data = array();
        $photos = $this->getPhotos(1, 1000, 'isg.shortcode, isg.approve');

        foreach ($photos as $photo) {
            $data[$photo['shortcode']] = $photo;
        }

        return $data;
    }

    public function getPhotosTotal()
    {
        $query = $this->db->query("SELECT COUNT(shortcode) AS total FROM `" . DB_PREFIX . "instagramshopgallery`");

        return $query->row['total'];
    }

    /**
     * Get photo detailed info
     */
    public function getPhoto($shortcode)
    {
        $data = array();
        $query = $this->db->query(
            "SELECT *
            FROM `" . DB_PREFIX . "instagramshopgallery` isg
            WHERE isg.shortcode = '" . $this->db->escape($shortcode) . "'"
        );

        if ($query->num_rows) {
            $data = $query->row;
            $data['related_product'] = $this->getRelatedProduct($shortcode);
        }

        $data['approve'] = !empty($data['approve']) ? $data['approve'] : 0;

        return $data;
    }

    /**
     * Get photo related product
     */
    public function getRelatedProduct($shortcode)
    {
        $products = array();
        $query = $this->db->query(
            "SELECT isgp.*, pd.name FROM `" . DB_PREFIX . "instagramshopgallery_to_product` isgp LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (pd.product_id = isgp.product_id) WHERE isgp.shortcode = '" . $this->db->escape($shortcode) . "'
            AND pd.language_id = '1' LIMIT 0, 100"
        );

        foreach ($query->rows as $product) {
            $products[] = $product;
        }

        return $products;
    }

    public function updatePhoto($param)
    {
        $this->deletePhoto($param['data']['shortcode']);

        // Remote image URL
        $url = $this->db->escape($param['data']['image_large']);
        
        // Image path
        $img = '/home/trueelementsco/public_html/true-elements.com/image/instagram/'.$this->db->escape($param['data']['shortcode']);
        
        // Save image 
        file_put_contents($img, file_get_contents($url));
        
        // Only save photo that approved or have related product
        $this->db->query(
            "INSERT INTO `" . DB_PREFIX . "instagramshopgallery` SET
            `approve`        = '" . (int)$param['approve'] . "',
            `shortcode`      = '" . $this->db->escape($param['data']['shortcode']) . "',
            `owner`          = '" . $this->db->escape($param['data']['owner']) . "',
            `fullname`       = '" . $this->db->escape($param['data']['fullname']) . "',
            `username`       = '" . $this->db->escape($param['data']['username']) . "',
            `image_thumb`    = '" . $this->db->escape($param['data']['image_thumb']) . "',
            `image_large`    = '" . $this->db->escape($param['data']['image_large']) . "',
            `image_original` = '" . $this->db->escape($param['data']['image_original']) . "',
            `caption`        = '" . $this->db->escape($param['data']['caption']) . "',
            `timestamp`      = '" . $this->db->escape($param['data']['timestamp']) . "',
            `updated`        = NOW()"
        );

        if (!empty($param['related_product'])) {
            foreach ($param['related_product'] as $product_id) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "instagramshopgallery_to_product` SET shortcode = '" . $this->db->escape($param['data']['shortcode']) . "', product_id = '" . (int)$product_id . "'");
            }
        }
    }

    public function deletePhoto($shortcode)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "instagramshopgallery` WHERE shortcode = '" . $this->db->escape($shortcode) . "'");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "instagramshopgallery_to_product` WHERE shortcode = '" . $this->db->escape($shortcode) . "'");
    }

    public function install($drop = false)
    {
        if ($drop) {
            $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "instagramshopgallery`");
            $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "instagramshopgallery_to_product`");
        }

        $this->db->query(
            "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "instagramshopgallery` (
                `key_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `approve` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
                `shortcode` VARCHAR(128) NOT NULL COMMENT 'Unique photo code',
                `owner` VARCHAR(128) NOT NULL COMMENT 'Instagram user id',
                `fullname` VARCHAR(128) NOT NULL,
                `username` VARCHAR(128) NOT NULL,
                `image_thumb` TEXT NOT NULL COMMENT 'URL',
                `image_large` TEXT NOT NULL COMMENT 'URL',
                `image_original` TEXT NOT NULL COMMENT 'URL',
                `caption` TEXT NOT NULL,
                `timestamp` INT(11) UNSIGNED NOT NULL COMMENT 'Photo timestamp',
                `updated` DATETIME NOT NULL,
                PRIMARY KEY (`key_id`),
                INDEX `approve` (`approve`),
                INDEX `shortcode` (`shortcode`)
            )
            ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1"
        );

        $this->db->query(
            "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "instagramshopgallery_to_product` (
                `shortcode` VARCHAR(128) NOT NULL COMMENT 'Unique photo code',
                `product_id` INT(11) UNSIGNED NOT NULL,
                PRIMARY KEY (`product_id`, `shortcode`)
            )
            ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1"
        );

        
    }

    public function uninstall()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "instagramshopgallery`");
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "instagramshopgallery_to_product`");

        
    }

    
}
