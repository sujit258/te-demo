<?php 
class ModelExtensionModuleOrderReviews extends Model {
	
  	public function install() {
		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "orderreviews_log`
		(`log_id` INT(11) NOT NULL AUTO_INCREMENT, 
		 `order_id` INT(11) NOT NULL DEFAULT 0,
		 `date_created` DATETIME  NOT NULL DEFAULT '0000-00-00 00:00:00',
		  PRIMARY KEY (`log_id`));");
		
		$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%OrderReviews by iSenseLabs%'");
		$modifications = $this->load->controller('extension/modification/refresh');
  	} 
  
  	public function uninstall() {
		$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%OrderReviews by iSenseLabs%'");
		$modifications = $this->load->controller('extension/modification/refresh');
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."orderreviews_log");
  	}

	public function checkOrderReviewsVersionCompatibility() {
		$name_exists = $this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "orderreviews_log LIKE 'customer_name'");
		$product_id_exists = $this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "orderreviews_log LIKE 'review_product_id'");
		$product_rating_exists = $this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "orderreviews_log LIKE 'review_rating'");
		$review_coupon_exists = $this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "orderreviews_log LIKE 'review_coupon'");
		$review_store_id_exists = $this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "orderreviews_log LIKE 'store_id'");

		$name = $name_exists->num_rows > 0 ? true : false;
		$product_id = $product_id_exists->num_rows > 0 ? true : false;
		$review_rating = $product_rating_exists->num_rows > 0 ? true :false;
		$review_coupon = $review_coupon_exists->num_rows > 0 ? true :false;
		$store_id = $review_store_id_exists->num_rows > 0 ? true :false;

		if (!$name && !$product_id && !$review_rating && !$review_coupon && !$store_id) {
			$this->db->query("ALTER TABLE  `".DB_PREFIX."orderreviews_log` ADD  `customer_name` VARCHAR( 255 ) NULL DEFAULT NULL AFTER  `order_id` ,
			ADD  `review_product_id` INT( 11 ) NULL DEFAULT NULL AFTER  `customer_name`, ADD  `review_rating` INT( 1 ) NOT NULL AFTER  `review_product_id`, ADD  `review_coupon` VARCHAR( 255 ) NULL DEFAULT NULL AFTER  `review_rating`, ADD  `store_id` INT( 1 ) NOT NULL AFTER  `review_coupon`");
		}
	}

	public function getUsedCouponDetails($coupon_id)
	{
		$used_coupon = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon WHERE coupon_id = ".(int)$coupon_id);
		return $used_coupon->row;
	}

	public function getTotalUsedCoupons($store_id=0) {
		$total_coupons = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon WHERE `name` LIKE '%OrderReviews%'");
		$total_coupons_ids = array();		
		foreach ($total_coupons->rows as $coupon) {
			$total_coupons_ids[] = $coupon['coupon_id'];			
		}

		$comma_separated_ids = implode(', ', $total_coupons_ids);		
		$total_used = $this->db->query("SELECT COUNT(*) as count FROM " . DB_PREFIX . "coupon_history WHERE coupon_id IN (".$this->db->escape($comma_separated_ids).")");
		return $total_used->row['count'];
	}
	
	public function getUsedCoupons($page=1, $limit=1, $store=0, $sort="coupon_id", $order="DESC"){
		
		if ($page) {
			$start = ($page - 1) * $limit;
		}
		$sent_ids = array();
		$sent_coupons = $this->getAllGeneratedCoupons($page, $limit, $store, $sort, $order);
		foreach ($sent_coupons as $coupon) {
			$sent_ids[] = $coupon['coupon_id'];
		}

		$comma_separated_ids = implode(', ', $sent_ids);
		
		$all_used = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_history WHERE coupon_id IN (".$this->db->escape($comma_separated_ids).")");

		return $all_used->rows;
	}

	public function getAllGeneratedCoupons($page=1, $limit=8, $store=0, $sort="coupon_id", $order="DESC") {
		if ($page) {
			$start = ($page - 1) * $limit;
		}
		$query = $this->db->query("SELECT c.*, (SELECT count(*) FROM " . DB_PREFIX . "coupon_history WHERE coupon_id=c.coupon_id) as used FROM " . DB_PREFIX . "coupon c WHERE c.`name` LIKE '%OrderReviews%' ORDER BY c.`coupon_id` DESC LIMIT ".$start.", ".$limit);
		return $query->rows;
	}

	public function getTotalCoupons() {
		$query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "coupon` WHERE `name` LIKE '%OrderReviews%'");
		return $query->row['count']; 
	}

	public function checkForTable() {
		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "orderreviews_log`
		(`log_id` INT(11) NOT NULL AUTO_INCREMENT, 
		 `order_id` INT(11) NOT NULL DEFAULT 0,
		 `date_created` DATETIME  NOT NULL DEFAULT '0000-00-00 00:00:00',
		  PRIMARY KEY (`log_id`));");
	}

	public function getAllReviews($page=1, $limit=8, $store=0, $sort="log_id", $order="DESC") {	
		if ($page) {
			$start = ($page - 1) * $limit;
		}
		$query =  $this->db->query("SELECT * FROM `" . DB_PREFIX . "orderreviews_log`
			WHERE `store_id`='".$store."'
			ORDER BY `log_id` DESC
			LIMIT ".$start.", ".$limit);
		return $query->rows; 
	}

	public function getTotalReviews($store=0){
		$query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "orderreviews_log` WHERE `store_id`=".$store);	

		return $query->row['count']; 
	}
  }