<?php
class ModelExtensionModuleIanalytics extends Model {
  	
    public function install() {
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ianalytics_product_comparisons` (
		  `id` int(11) NOT NULL auto_increment COMMENT 'Primary index',
		  `date` date NOT NULL COMMENT 'Date when data is added',
		  `time` time NOT NULL COMMENT 'Time of day when data is added',
		  `from_ip` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'IP of client that generated the data',
		  `spoken_languages` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'Language of the client that generated the data',
		  `product_ids` varchar(255) collate utf8_unicode_ci NOT NULL COMMENT 'The ids of the compared products, ordered ascending. Used to determine the count of the comparison',
		  `product_names` text collate utf8_unicode_ci NOT NULL COMMENT 'Product names according to the ids',
		  `store_id` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;");

		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_product_comparisons LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_product_comparisons ADD store_id int(11) NOT NULL DEFAULT 0 AFTER product_names");
		} 
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ianalytics_product_opens` (
		  `id` int(11) NOT NULL auto_increment COMMENT 'Primary index',
		  `date` date NOT NULL COMMENT 'Date when data is added',
		  `time` time NOT NULL COMMENT 'Time of day when data is added',
		  `from_ip` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'IP of client that generated the data',
		  `spoken_languages` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'Language of the client that generated the data',
		  `product_id` int(11) NOT NULL COMMENT 'The id of the opened product',
		  `product_name` text collate utf8_unicode_ci NOT NULL COMMENT 'The name of the opened product',
		  `product_model` text collate utf8_unicode_ci NOT NULL COMMENT 'The model of the opened product',
		  `product_price` decimal(15,4) NOT NULL default '0.0000' COMMENT 'The price of the opened product',
		  `product_quantity` int(11) NOT NULL default '0' COMMENT 'The quantity of the opened product',
		  `product_stock_status` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'The stock status of the opened product',
		  `store_id` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;");

		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_product_opens LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_product_opens ADD store_id int(11) NOT NULL DEFAULT 0 AFTER product_stock_status");
		} 
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ianalytics_product_add_to_cart` (
		  `id` int(11) NOT NULL auto_increment COMMENT 'Primary index',
		  `date` date NOT NULL COMMENT 'Date when data is added',
		  `time` time NOT NULL COMMENT 'Time of day when data is added',
		  `from_ip` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'IP of client that generated the data',
		  `spoken_languages` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'Language of the client that generated the data',
		  `product_id` int(11) NOT NULL COMMENT 'The id of the opened product',
		  `product_name` text collate utf8_unicode_ci NOT NULL COMMENT 'The name of the added to cart product',
		  `product_model` text collate utf8_unicode_ci NOT NULL COMMENT 'The model of the added to cart product',
		  `product_price` decimal(15,4) NOT NULL default '0.0000' COMMENT 'The price of the added to cart product',
		  `product_quantity` int(11) NOT NULL default '0' COMMENT 'The quantity of the the added to cart product',
		  `product_stock_status` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'The stock status of the added to cart product',
		  `store_id` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;");
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_product_add_to_cart LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_product_add_to_cart ADD store_id int(11) NOT NULL DEFAULT 0 AFTER product_stock_status");
		} 
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ianalytics_product_add_to_wishlist` (
		  `id` int(11) NOT NULL auto_increment COMMENT 'Primary index',
		  `date` date NOT NULL COMMENT 'Date when data is added',
		  `time` time NOT NULL COMMENT 'Time of day when data is added',
		  `from_ip` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'IP of client that generated the data',
		  `spoken_languages` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'Language of the client that generated the data',
		  `product_id` int(11) NOT NULL COMMENT 'The id of the opened product',
		  `product_name` text collate utf8_unicode_ci NOT NULL COMMENT 'The name of the added to cart product',
		  `product_model` text collate utf8_unicode_ci NOT NULL COMMENT 'The model of the added to cart product',
		  `product_price` decimal(15,4) NOT NULL default '0.0000' COMMENT 'The price of the added to cart product',
		  `product_quantity` int(11) NOT NULL default '0' COMMENT 'The quantity of the the added to cart product',
		  `product_stock_status` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'The stock status of the added to wishlist product',
		  `store_id` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;");
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_product_add_to_wishlist LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_product_add_to_wishlist ADD store_id int(11) NOT NULL DEFAULT 0 AFTER product_stock_status");
		} 
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ianalytics_search_data` (
		  `id` int(11) NOT NULL auto_increment COMMENT 'Primary index',
		  `date` date NOT NULL COMMENT 'Date when data is added',
		  `time` time NOT NULL COMMENT 'Time of day when data is added',
		  `from_ip` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'IP of client that generated the data',
		  `spoken_languages` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'Language of the client that generated the data',
		  `search_value` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'The searched text',
		  `search_results` int(11) NOT NULL default '0' COMMENT 'The number of found search results',
		  `store_id` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;");
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_search_data LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_search_data ADD store_id int(11) NOT NULL DEFAULT 0 AFTER search_results");
		}
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ianalytics_funnel_data` (
		  `id` int(11) NOT NULL auto_increment COMMENT 'Primary index',
		  `date` date NOT NULL COMMENT 'Date when data is added',
		  `time` time NOT NULL COMMENT 'Time of day when data is added',
		  `from_ip` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'IP of client that generated the data',
		  `spoken_languages` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'Language of the client that generated the data',
		  `stage` enum('0','1','2','3','4','5','6') not null default '0' COMMENT 'Stage of the cliend that generated the data',
		  `store_id` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;");
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_funnel_data LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_funnel_data ADD store_id int(11) NOT NULL DEFAULT 0 AFTER stage");
		}
		
		$this->db->query("
		CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ianalytics_visits_data` (
		  `id` int(11) NOT NULL auto_increment COMMENT 'Primary index',
		  `date` date NOT NULL COMMENT 'Date when data is added',
		  `stage` enum('0','1','2','3') not null default '0' COMMENT 'Stage of the cliend that generated the data',
		  `unique_visits` int(11) NOT NULL default '0' COMMENT 'Unique visits',
		  `impressions` int(11) NOT NULL default '0' COMMENT 'Visited Pages',
		  `referers_direct` int(11) NOT NULL default '0' COMMENT 'Direct hits',
		  `referers_social` int(11) NOT NULL default '0' COMMENT 'Referers from social networks',
		  `referers_search` int(11) NOT NULL default '0' COMMENT 'Referers from search engines',
		  `referers_other` int(11) NOT NULL default '0' COMMENT 'Referers from other websites',
		  `store_id` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;");
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_visits_data LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_visits_data ADD store_id int(11) NOT NULL DEFAULT 0 AFTER referers_other");
		}
				 
		//$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%iAnalytics by iSenseLabs%'");
		//$modifications = $this->load->controller('extension/modification/refresh');
  	} 
  
  	public function uninstall() {
		//$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%iAnalytics by iSenseLabs%'");
		//$modifications = $this->load->controller('extension/modification/refresh');
  	}		
	
	public $data;
	
	public function getAnalyticsData($mydata, $store_id = 0) {
		$this->data = $mydata;
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_visits_data LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_visits_data ADD store_id int(11) NOT NULL DEFAULT 0 AFTER referers_other");
		}
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_search_data LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_search_data ADD store_id int(11) NOT NULL DEFAULT 0 AFTER search_results");
		}
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_product_add_to_wishlist LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_product_add_to_wishlist ADD store_id int(11) NOT NULL DEFAULT 0 AFTER product_stock_status");
		} 

		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_product_comparisons LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_product_comparisons ADD store_id int(11) NOT NULL DEFAULT 0 AFTER product_names");
		} 
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_product_add_to_cart LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_product_add_to_cart ADD store_id int(11) NOT NULL DEFAULT 0 AFTER product_stock_status");
		} 
		
		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_product_opens LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_product_opens ADD store_id int(11) NOT NULL DEFAULT 0 AFTER product_stock_status");
		} 

		if ($this->db->query("SHOW COLUMNS FROM " .DB_PREFIX . "ianalytics_funnel_data LIKE 'store_id'")->num_rows==0) {
			$this->db->query("ALTER TABLE " .DB_PREFIX . "ianalytics_funnel_data ADD store_id int(11) NOT NULL DEFAULT 0 AFTER stage");
		}		
			
		//GET DATA
		$this->data['iAnalyticsMinDate'] = $this->_findMinDate($store_id);
		
		//MANAGE DATES
		$fromDate = (empty($_GET['fromDate'])) ? '' : date_parse(preg_replace('/([^0-9-])/m', '', $_GET['fromDate']));
		$toDate = (empty($_GET['toDate'])) ? '' : date_parse(preg_replace('/([^0-9-])/m', '', $_GET['toDate']));
		$now = time();
		
		if (is_array($toDate) && $toDate['warning_count'] == 0 && $toDate['error_count'] == 0) {
			if (!checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
				$toDate = date('Y-m-d', $now);
			} else {
				$toDate = str_pad($toDate['year'], 4, '0', STR_PAD_LEFT) . '-' . str_pad($toDate['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($toDate['day'], 2, '0', STR_PAD_LEFT);
				
				if (strcmp($toDate, date('Y-m-d')) > 0) {
					$toDate = date('Y-m-d', $now);	
				}
			}
		} else {
			$toDate = date('Y-m-d', $now);	
		}
		
		if (is_array($fromDate) && $fromDate['warning_count'] == 0 && $fromDate['error_count'] == 0) {
			if (!checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
				$fromDate = $this->data['iAnalyticsMinDate'];
			} else {
				$fromDate = str_pad($fromDate['year'], 4, '0', STR_PAD_LEFT) . '-' . str_pad($fromDate['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($fromDate['day'], 2, '0', STR_PAD_LEFT);
				
				if (strcmp($fromDate, $this->data['iAnalyticsMinDate']) < 0) {
					$fromDate = $this->data['iAnalyticsMinDate'];
				}
			}
		} else {
			$fromDate = $this->data['iAnalyticsMinDate']; 
		}
		
		$enable = array();
		$interval = NULL;
		
		$interval = abs(ceil((($now - strtotime($this->data['iAnalyticsMinDate']))/86400))) + 1;
		
		switch ($interval) {
			case ($interval < 7) : 						{ $enable = array(1,0,0,0); } break;
			case ($interval >= 7 && $interval < 30) : 	{ $enable = array(1,1,0,0); } break;
			case ($interval >= 30 && $interval < 365) : { $enable = array(1,1,1,0); } break;
			case ($interval >= 365) : 					{ $enable = array(1,1,1,1); } break;
		}
		
		$select = array(1,0,0,0);
		
		if ($toDate == date('Y-m-d', $now)) {
			$interval = abs(ceil(((strtotime($toDate) - strtotime($fromDate))/86400)));
			if (empty($_GET['fromDate']) && empty($_GET['toDate'])) {
				if ($interval > 30) {
					$fromDate = date('Y-m-d', $now - 29*86400);
					$interval = abs(ceil(((strtotime($toDate) - strtotime($fromDate))/86400)));
				}
			}
			
			switch ($interval) {
				case 6 :		{ $select = array(0,1,0,0); } break;
				case 29 : 		{ $select = array(0,0,1,0); } break;
				case 364 : 		{ $select = array(0,0,0,1); } break;
				default : 		{ $select = array(1,0,0,0); } break;
			}
		} 
		
		$this->data['iAnalyticsSelectData'] 					= array('enable' => $enable, 'select' => $select);
		
		$this->data['iAnalyticsFromDate'] 						= $fromDate;
		$this->data['iAnalyticsToDate'] 						= $toDate;
		
		$this->data['iAnalyticsMonthlySearchesGraph'] 		    = $this->getMonthlySearchesGraph($store_id); 
		$this->data['iAnalyticsMonthlySearchesTable'] 		    = $this->getMonthlySearchesTable($store_id);
		$this->data['iAnalyticsKeywordSearchHistory'] 		    = $this->getKeywordSearchHistory(80, $store_id);
		
		$this->data['iAnalyticszeroKeywordSearchHistory'] 		    = $this->getzeroKeywordSearchHistory(80, $store_id);
		
		$this->data['iAnalyticsMostSearchedKeywords'] 		    = $this->getMostSearchedKeywords($store_id);
		$this->data['iAnalyticsMostFoundKeywords'] 			    = $this->getMostFoundKeywords($store_id);
		$this->data['iAnalyticsMostOpenedProducts'] 			= $this->getMostOpenedProducts($store_id);
		$this->data['iAnalyticsMostAddedtoCartProducts'] 		= $this->getMostAddedtoCartProducts($store_id);
		$this->data['iAnalyticsMostAddedtoWishlistProducts'] 	= $this->getMostAddedtoWishlistProducts($store_id);
		$this->data['iAnalyticsMostComparedProducts'] 		    = $this->getMostComparedProducts($store_id);
		$this->data['iAnalyticsMostSearchedKeywordsPie'] 		= $this->getMostSearchedKeywordsPie($store_id);
		$this->data['iAnalyticsMostFoundKeywordsPie'] 		    = $this->getMostFoundKeywordsPie($store_id);
		$this->data['iAnalyticsMostOpenedProductsPie'] 		    = $this->getMostOpenedProductsPie($store_id);
		$this->data['iAnalyticsMostComparedProductsPie'] 		= $this->getMostComparedProductsPie($store_id);
		$this->data['iAnalyticsFunnelData'] 					= $this->getFunnelData($store_id);
		$this->data['iAnalyticsVisitorsData'] 				    = $this->getVisitorsData('stage',$store_id);
		$this->data['iAnalyticsVisitorsDataByDay'] 			    = $this->getVisitorsDataByDay('date', $store_id);
		$this->data['iAnalyticsVisitorsDataByWeek'] 			= $this->getVisitorsDataByWeek('date', $store_id);
		$this->data['iAnalyticsVisitorsDataByMonth'] 			= $this->getVisitorsDataByMonth('date', $store_id);
		$this->data['iAnalyticsVisitorsDataReferers'] 		    = $this->getVisitorsDataReferers('date',$store_id);
		$this->data['iAnalyticsVisitorsDataReferersPie'] 		= $this->getVisitorsDataReferersPie('',$store_id);
		
		return $this->data;
	}
	
	public function deleteSearchKeyword($id, $store_id = 0) {
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_search_data WHERE id = "' . $id . '" AND store_id="'.$store_id.'"');
	}
	
	public function deleteAllSearchKeyword($value, $store_id = 0) {
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_search_data WHERE search_value = "' . $value . '" AND store_id="'.$store_id.'"');
	}
	
	public function deleteAnalyticsData($store_id = 0) {
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_product_comparisons WHERE store_id="'.$store_id.'"');
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_product_opens WHERE store_id="'.$store_id.'"');
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_search_data WHERE store_id="'.$store_id.'"');
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_product_add_to_cart WHERE store_id="'.$store_id.'"');
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_product_add_to_wishlist WHERE store_id="'.$store_id.'"');
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_funnel_data WHERE store_id="'.$store_id.'"');
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'ianalytics_visits_data WHERE store_id="'.$store_id.'"');
	}
	
	function excludedIPs() {
		$configValue = $this->config->get('ianalytics');
		$ips = array();
		if (!empty($configValue['BlacklistedIPs'])) {
			$ips = $configValue['BlacklistedIPs'];
			$ips = str_replace("\n\r", "\n", $ips);
			$ips = explode("\n", $ips);
			foreach ($ips as $i => $val) {
				$ips[$i] = '"'.trim($val).'"';
			}
		}
		
		return $ips;
	}
	
	function getMonthlySearchesGraph($store_id = 0) {
		if (empty($this->data['iAnalyticsVendorId'])) {
			$days = array(array('Day','Successful queries','Zero-results queries'));
			
			for ($i=$this->data['iAnalyticsFromDate']; strcmp($i, $this->data['iAnalyticsToDate']) <= 0; $i = date('Y-m-d', strtotime($i) + 86400 + 43201)) {
				$theday = date("j-n-Y",strtotime($i));
				$days[] = array(date("j-n-Y",strtotime($i)),$this->getNumberSearchesByDay($i,'success', $store_id),$this->getNumberSearchesByDay($i,'fail', $store_id));
			}
			return $days;
		} else {
			
		}
	}
	
	function getKeywordSearchHistory($limit=80, $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		$result = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'ianalytics_search_data WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" ORDER BY `date` DESC, `time` DESC LIMIT 0, '.$limit);
		
		if ($result->num_rows == 0) {
			return array(0 => 'No Data Gathered Yet');	
		} else {
			$k = array(array('Keyword','Date','Time','Results Found','User Language','IP address','ID'));
			foreach($result->rows as $i => $search) {
				array_push($k, array($search['search_value'],$search['date'],$search['time'],$search['search_results'],$search['spoken_languages'],$search['from_ip'],$search['id']));
			}	
		}
		
		return $k;
	}
	
	function getzeroKeywordSearchHistory($limit=80, $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		$result = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'ianalytics_search_data WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND search_results="0" AND store_id="'.$store_id.'" ORDER BY `date` DESC, `time` DESC LIMIT 0, '.$limit);
		
		if ($result->num_rows == 0) {
			return array(0 => 'No Data Gathered Yet');	
		} else {
			$k = array(array('Keyword','Date','Time','Results Found','User Language','IP address','ID'));
			foreach($result->rows as $i => $search) {
				array_push($k, array($search['search_value'],$search['date'],$search['time'],$search['search_results'],$search['spoken_languages'],$search['from_ip'],$search['id']));
			}	
		}
		
		return $k;
	}
	
	function getMostSearchedKeywordsPie($store_id = 0) {
		$keywords = $this->_getMostSearchedKeywordsRaw(7,true,$store_id);
		$keys = array_keys($keywords);
		$values = array_values($keywords);
		$pattern = array();
		foreach (array_combine($keys, $values) as $key => $val) {
			$pattern[$key] = $val;
		}
		return $pattern;
	}
	
	function getMostFoundKeywordsPie($store_id = 0) {
		$keywords = $this->_getMostSearchedKeywordsRaw(15,false, $store_id);
		$keys = array_keys($keywords);
		$values = array_values($keywords);
		
        if (count($values) == 2) {
			return $keys[0].':'.$values[0].':#718b3b;'.$keys[1].':'.$values[1].':#a0c74e;';
		}
		
		if (count($values) == 1) {
			return $keys[0].':'.$values[0].':#718b3b;';
		}
		
		if (count($values) == 0) {
			return 'No Data Gathered Yet:100:#718b3b;';
		}
        
		$pattern = $keys[0].':'.$values[0].':#718b3b;'.$keys[1].':'.$values[1].':#a0c74e;'.$keys[2].':'.$values[2].':#CFE3A6;';
		
        return $pattern;
	}
	

	function getMostOpenedProductsPie($store_id = 0) {
		$opens = $this->_getMostOpenedProductsRaw('product_name', $store_id);
		$keys = array_keys($opens);
		$values = array_values($opens);
		
        if (count($values) == 2) {
			return $keys[0].':'.$values[0].':#003A88;'.$keys[1].':'.$values[1].':#005CD9;';
		}
		
		if (count($values) == 1) {
			return $keys[0].':'.$values[0].':#003A88;';
		}
		
		if (count($values) == 0) {
			return 'No Data Gathered Yet:100:#003A88;';
		}
		
        $pattern = $keys[0].':'.$values[0].':#003A88;'.$keys[1].':'.$values[1].':#005CD9;'.$keys[2].':'.$values[2].':#519BFF;';
		
        return $pattern;
	}
	
	function getMostComparedProductsPie($store_id = 0) {
		$comparisons = $this->_getMostComparedProductsRaw($store_id);
		
		$keys = array_keys($comparisons);
		$values = array_values($comparisons);

		if (count($values) == 2) {
			return $keys[0].':'.$values[0].':#CE3CFF;'.$keys[1].':'.$values[1].':#DD77FF;';
		}
		
		if (count($values) == 1) {
			return $keys[0].':'.$values[0].':#CE3CFF;';
		}
		
		if (count($values) == 0) {
			return 'No Data Gathered Yet:100:#CE3CFF;';
		}
		
		$pattern = $keys[0].':'.$values[0].':#CE3CFF;'.$keys[1].':'.$values[1].':#DD77FF;'.$keys[2].':'.$values[2].':#EFBFFF;';
		
        return $pattern;
	}
	
	
	function _getMostOpenedProductsRaw($param='product_id', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', COUNT(*) as count FROM ' . DB_PREFIX . 'ianalytics_product_opens WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `product_id` ORDER BY count DESC, `date` DESC, `time` DESC');
		
		if ($result->num_rows == 0) {
			return array('No Data Gathered Yet' => 0);
		} else {
			$k = array();
			foreach($result->rows as $i => $search) {
				$k[$search[$param]] = $search['count'];
			}
			arsort($k);
		}
		
		return $k;
	}
	
	function _getMostAddedtoCartProductsRaw($param='product_id', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', COUNT(*) as count FROM ' . DB_PREFIX . 'ianalytics_product_add_to_cart
 WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `product_id` ORDER BY count DESC, `date` DESC, `time` DESC');
		
		if ($result->num_rows == 0) {
			return array('No Data Gathered Yet' => 0);
		} else {
			$k = array();
			foreach($result->rows as $i => $search) {
				$k[$search[$param]] = $search['count'];
			}
			arsort($k);
		}
		
		return $k;
	}
	
	function _getMostAddedtoWishlistProductsRaw($param='product_id', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', COUNT(*) as count FROM ' . DB_PREFIX . 'ianalytics_product_add_to_wishlist
 WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `product_id` ORDER BY count DESC, `date` DESC, `time` DESC');
		
		if ($result->num_rows == 0) {
			return array('No Data Gathered Yet' => 0);
		} else {
			$k = array();
			foreach($result->rows as $i => $search) {
				$k[$search[$param]] = $search['count'];
			}
			arsort($k);
		}
		
		return $k;
	}
	
	function _getFunnelDataRaw($param='stage', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', COUNT(*) as count FROM ' . DB_PREFIX . 'ianalytics_funnel_data
 WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `stage` ORDER BY `stage` ASC, `date` DESC, `time` DESC');

		if ($result->num_rows == 0) {
			return array('No Data Gathered Yet' => 0);
		} else {
			$k = array();
			foreach($result->rows as $i => $search) {
				$k[$search[$param]] = $search['count'];
			}
		}
		return $k;
	}
	
	function _getMostComparedProductsRaw($store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT product_ids as pids, (SELECT product_names FROM ' . DB_PREFIX . 'ianalytics_product_comparisons WHERE product_ids = pids AND store_id="'.$store_id.'" ORDER BY `date` DESC, `time` DESC LIMIT 0,1) as comparison, COUNT(*) as count FROM ' . DB_PREFIX . 'ianalytics_product_comparisons WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY pids ORDER BY count DESC');
		
		if ($result->num_rows == 0) {
			return array('No Data Gathered Yet' => 0);
		} else {
			$k = array();
			foreach($result->rows as $i => $search) {
				$k[$search['comparison']] = $search['count'];
			}
			arsort($k);	
		}
		
		return $k;
	}
	
	function _getMostSearchedKeywordsRaw($limit=80,$returnZeroResultsToo = true, $store_id = 0) {
		$temp = array();
		
		if ($returnZeroResultsToo == false) $condition = ' AND search_results != "0"'; else $condition = '';
		
        $excludedIPs = $this->excludedIPs();

        $result = $this->db->query('SELECT `search_value`, COUNT(*) as count FROM ' . DB_PREFIX . 'ianalytics_search_data WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'"'.$condition.' AND store_id="'.$store_id.'" GROUP BY `search_value` ORDER BY count DESC LIMIT 0, '.$limit);
		
        if ($result->num_rows == 0) {
			return array('No Data Gathered Yet' => 'No Data Gathered Yet');
		} else {
			$res = array();
			foreach($result->rows as $row) {
				$res[$row['search_value']] = $row['count'];	
			}
			arsort($res);
			return $res;
		}
        
		return $results;
	}
	
	function getMostComparedProducts($store_id = 0) {
		$k = array(array('Products','Comparisons'));
		$temp = $this->_getMostComparedProductsRaw($store_id);
		
        if ($temp === array('No Data Gathered Yet' => 0)) {
			return array(0 => 'No Data Gathered Yet');
		}
		
        foreach ($temp as $key => $value) {
			array_push($k, array($key,$value));
		}
		
		return $k;
	}
	
	function getMostOpenedProducts($store_id = 0) {
		$k = array(array('Product','Opens'));
		$temp = $this->_getMostOpenedProductsRaw('product_name', $store_id);
		foreach ($temp as $key => $value) {
			array_push($k, array($key,$value));
		}
		
		return $k;
	}
	
	function getMostAddedtoCartProducts($store_id = 0) {
		$k = array(array('Product','Added to Cart'));
		$temp = $this->_getMostAddedtoCartProductsRaw('product_name', $store_id);
		foreach ($temp as $key => $value) {
			array_push($k, array($key,$value));
		}
		
		return $k;
	}
	
	function getMostAddedtoWishlistProducts($store_id = 0) {
		$k = array(array('Product','Added to Wishlist'));
		$temp = $this->_getMostAddedtoWishlistProductsRaw('product_name', $store_id);
		foreach ($temp as $key => $value) {
			array_push($k, array($key,$value));
		}
		
		return $k;
	}
	
	function getFunnelData($store_id = 0) {
		$k = array(array('Stage','Actions'));
		$temp = $this->_getFunnelDataRaw('stage', $store_id);
		foreach ($temp as $key => $value) {
			array_push($k, array($key,$value));
		}
		
		return $k;
	}
	
	function getVisitorsData($param='stage', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', sum(`unique_visits`) as visits, sum(`impressions`) as page_impressions, sum(referers_social+referers_other+referers_search+referers_direct) as referers FROM ' . DB_PREFIX . 'ianalytics_visits_data
 WHERE `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `stage` ORDER BY `stage` ASC, `date` DESC');
		if ($result->num_rows == 0) {
			return array(0 => 'No Data Gathered Yet');	
		} else {
			$k = array(array('Part of the Day','Unique Visits','Page Impressions','Referers'));
		
			foreach($result->rows as $i => $search) {
				array_push($k, array($search['stage'],$search['visits'],$search['page_impressions'],$search['referers']));
			}	
		}
		return $k;
	}
	
	function getVisitorsDataByDay($param='date', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', sum(`unique_visits`) as visits, sum(`impressions`) as page_impressions, sum(referers_social+referers_other+referers_search+referers_direct) as referers FROM ' . DB_PREFIX . 'ianalytics_visits_data WHERE `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `date` ORDER BY `date` ASC');

		if ($result->num_rows == 0) {
			return array(0 => 'No Data Gathered Yet');	
		} else {
			$k = array(array('Date','Unique Visits','Page Impressions','Referers'));
			if ($result->num_rows == 1) {
				array_push($k, array('#', 0, 0, 0));
			}
			foreach($result->rows as $i => $search) {
				array_push($k, array($search['date'],$search['visits'],$search['page_impressions'],$search['referers']));
			}	
		}
		return $k;
	}
	
	function getVisitorsDataByWeek($param='date', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', sum(`unique_visits`) as visits, sum(`impressions`) as page_impressions, sum(referers_social+referers_other+referers_search+referers_direct) as referers FROM ' . DB_PREFIX . 'ianalytics_visits_data WHERE `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `date` ORDER BY `date` ASC');
        
        $week_array = array();
        $get_week_start = array(
            1 => "-0 DAYS",
            2 => "-1 DAYS",
            3 => "-2 DAYS",
            4 => "-3 DAYS",
            5 => "-4 DAYS",
            6 => "-5 DAYS",
            7 => "-6 DAYS"
        );

        $get_week_end = array(
            1 => "+6 DAYS",
            2 => "+5 DAYS",
            3 => "+4 DAYS",
            4 => "+3 DAYS",
            5 => "+2 DAYS",
            6 => "+1 DAYS",
            7 => "+0 DAYS"
        );
        
        $date_start_day_no = date('N', strtotime($this->data['iAnalyticsFromDate']));
        
        $date_end_day_no = date('N', strtotime($this->data['iAnalyticsToDate']));
        
        if ($result->num_rows == 0) {
	        return array(0 => 'No Data Gathered Yet');	
	    } else {
	    	$k = array(array('Date','Unique Visits','Page Impressions','Referers'));
		   	
		    foreach($result->rows as $i => $search) {
		        $week_start_date = NULL;
		        $date_range_start = NULL;
		        $week_end_date = NULL;
		        $date_range_end = NULL;
		        if($date_start_day_no == 7 && date('Y-m-d', strtotime($search['date'])) >= date('Y-m-d', strtotime($this->data['iAnalyticsFromDate'])) && date('Y-m-d', strtotime($search['date'])) <= date('Y-m-d', strtotime($this->data['iAnalyticsFromDate']))) {
		            $date_range_start = date('d M', strtotime($this->data['iAnalyticsFromDate']));
		            $date_range_end = date('d M', strtotime($this->data['iAnalyticsFromDate']));
		        } else if($date_end_day_no == 1 && date('Y-m-d', strtotime($search['date'])) >= date('Y-m-d', strtotime($this->data['iAnalyticsToDate'])) && date('Y-m-d', strtotime($search['date'])) <= date('Y-m-d', strtotime($this->data['iAnalyticsToDate']))) {
		            $date_range_start = date('d M', strtotime($this->data['iAnalyticsToDate']));
		            $date_range_end = date('d M', strtotime($this->data['iAnalyticsToDate']));
		        } else {
		            $week_start_date = strtotime($get_week_start[date('N', strtotime($search['date']))], strtotime($search['date']));
		            $date_range_start = date('d M', $week_start_date);
		            
		            $week_end_date = strtotime($get_week_end[date('N', strtotime($search['date']))], strtotime($search['date']));
		            $date_range_end = date('d M', $week_end_date);
		            
		            //Date Range Exceeding input range fix
		            if(date('Y-m-d', $week_start_date) < date('Y-m-d', strtotime($this->data['iAnalyticsFromDate']))) {
		                $date_range_start = date('d M', strtotime($this->data['iAnalyticsFromDate']));
		            }
		            if(date('Y-m-d', $week_end_date) > date('Y-m-d', strtotime($this->data['iAnalyticsToDate']))) {
		                $date_range_end = date('d M', strtotime($this->data['iAnalyticsToDate']));
		            }
		        }
		        if(array_key_exists($date_range_start . ' - ' . $date_range_end, $week_array)) {
		            $week_array[$date_range_start . ' - ' . $date_range_end]['visits'] += $search['visits'];
		            $week_array[$date_range_start . ' - ' . $date_range_end]['page_impressions'] += $search['page_impressions'];
		            $week_array[$date_range_start . ' - ' . $date_range_end]['referers'] += $search['referers'];
		        } else {
		            $week_array[$date_range_start . ' - ' . $date_range_end]['date'] = $date_range_start . ' - ' . $date_range_end;
		            $week_array[$date_range_start . ' - ' . $date_range_end]['visits'] = $search['visits'];
		            $week_array[$date_range_start . ' - ' . $date_range_end]['page_impressions'] = $search['page_impressions'];
		            $week_array[$date_range_start . ' - ' . $date_range_end]['referers'] = $search['referers'];
		        }
	    	}
            if(count($week_array) == 1) {
                array_push($k, array('#', 0, 0, 0));
            }
    		foreach ($week_array AS $weekly_data) {
	    	    array_push($k, array($weekly_data['date'],$weekly_data['visits'],$weekly_data['page_impressions'],$weekly_data['referers']));
	    	}
	    }
    	return $k;
	}
	
	function getVisitorsDataByMonth($param='date', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', sum(`unique_visits`) as visits, sum(`impressions`) as page_impressions, sum(referers_social+referers_other+referers_search+referers_direct) as referers FROM ' . DB_PREFIX . 'ianalytics_visits_data WHERE `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `date` ORDER BY `date` ASC');

		if ($result->num_rows == 0) {
			return array(0 => 'No Data Gathered Yet');	
		} else {
			$k = array(array('Date','Unique Visits','Page Impressions','Referers'));
			if ($result->num_rows == 1) {
				array_push($k, array('#', 0, 0, 0));
			}
			$month_array = array();
			foreach($result->rows as $i => $search) {
			    if(array_key_exists(date('M y', strtotime($search['date'])), $month_array)) {
			        $month_array[date('M y', strtotime($search['date']))]['visits'] += $search['visits'];
			        $month_array[date('M y', strtotime($search['date']))]['page_impressions'] += $search['page_impressions'];
			        $month_array[date('M y', strtotime($search['date']))]['referers'] += $search['referers'];
			    } else {
			        $month_array[date('M y', strtotime($search['date']))]['date'] = date('M y', strtotime($search['date']));
			        $month_array[date('M y', strtotime($search['date']))]['visits'] = $search['visits'];
			        $month_array[date('M y', strtotime($search['date']))]['page_impressions'] = $search['page_impressions'];
			        $month_array[date('M y', strtotime($search['date']))]['referers'] = $search['referers'];
			    }
			}
			if(count($month_array) == 1) {
                array_push($k, array('#', 0, 0, 0));
            }
			foreach ($month_array AS $monthly_data) {
			    array_push($k, array($monthly_data['date'],$monthly_data['visits'],$monthly_data['page_impressions'],$monthly_data['referers']));
			}
		}
		return $k;
	}
	
	function getVisitorsDataReferers($param='date', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.', sum(`referers_direct`) as direct, sum(`referers_social`) as social, sum(`referers_other`) as other, sum(`referers_search`) as search FROM ' . DB_PREFIX . 'ianalytics_visits_data
 WHERE `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" GROUP BY `date` ORDER BY `date` DESC');

		if ($result->num_rows == 0) {
			return array(0 => 'No Data Gathered Yet');	
		} else {
			$k = array(array('Date','Direct','Social','Search','Other'));
			foreach($result->rows as $i => $search) {
				array_push($k, array($search['date'],$search['direct'],$search['social'],$search['search'],$search['other']));
			}	
		}
		return $k;
	}
	
	function getVisitorsDataReferersPie($param='', $store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT '.$param.' sum(`referers_direct`) as direct, sum(`referers_social`) as social, sum(`referers_other`) as other, sum(`referers_search`) as search FROM ' . DB_PREFIX . 'ianalytics_visits_data
 WHERE `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND store_id="'.$store_id.'" ORDER BY `date` DESC');

		if ($result->num_rows == 0) {
			return array(0 => 'No Data Gathered Yet');	
		} else {
			$k = array(array('Direct','Social','Search','Other'));
			foreach($result->rows as $i => $search) {
				array_push($k, array($search['direct'],$search['social'],$search['search'],$search['other']));
			}	
		}
		return $k;
	}

	function getMostFoundKeywords($store_id = 0) {
		$k = array(array('Keyword','Queries'));
		$temp = $this->_getMostSearchedKeywordsRaw(80,false, $store_id);
		
        foreach ($temp as $key => $value) {
			array_push($k, array($key,$value));
		}
		
		return $k;
	}
	
	function getMostSearchedKeywords($store_id = 0) {
		$k = array(array('Keyword','Searches'));
		$temp = $this->_getMostSearchedKeywordsRaw(80,true,$store_id);
		
        foreach ($temp as $key => $value) {
			array_push($k, array($key,$value));
		}
		
		return $k;
	}
	
	function getMonthlySearchesTable($store_id = 0) {
		if (empty($this->data['iAnalyticsVendorId'])) {
			$days = array(array('Day','Total Search Queries','Successful Search Queries','Zero-Results Search Queries'));
			
			for ($i=$this->data['iAnalyticsToDate']; strcmp($i, $this->data['iAnalyticsFromDate']) >= 0; $i = date('Y-m-d', strtotime($i) - 43201)) {
				$succeeded = $this->getNumberSearchesByDay($i,'success', $store_id);
				$failed = $this->getNumberSearchesByDay($i,'fail',$store_id);
				$days[] = array(date("j-n-Y",strtotime($i)), $succeeded+$failed, $succeeded, $failed);
			}
			
			return $days;
		} else {
			
		}
	}
	
	function getNumberSearchesByDay(&$day, $type='success', $store_id = 0) {
		$success=0;
		$fail=0;
		
        if ($type == 'success') $condition = 'search_results != "0"'; else $condition = 'search_results = "0"';
		
        $excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('SELECT COUNT(*) as count FROM ' . DB_PREFIX . 'ianalytics_search_data WHERE' . (!empty($excludedIPs) ? ' `from_ip` NOT IN (' . implode(',', $excludedIPs) . ') AND' : '') . ' `date` >= "'.$this->data['iAnalyticsFromDate'].'" AND `date` <= "'.$this->data['iAnalyticsToDate'].'" AND `date`="'.$day.'" AND '.$condition.' AND store_id="'.$store_id.'" GROUP BY `date`');
		$count = empty($result->row['count']) ? 0 : (int)$result->row['count'];
		
		return $count;
	}


	function _findMinDate($store_id = 0) {
		$excludedIPs = $this->excludedIPs();
		
        $result = $this->db->query('
		SELECT LEAST(
			(SELECT MIN(`date`) FROM '.DB_PREFIX.'ianalytics_product_comparisons WHERE store_id="'.$store_id.'"' . (!empty($excludedIPs) ? ' AND `from_ip` NOT IN (' . implode(',', $excludedIPs) . ')' : '') . '),
			(SELECT MIN(`date`) FROM '.DB_PREFIX.'ianalytics_product_opens WHERE store_id="'.$store_id.'"' .(!empty($excludedIPs) ? ' AND `from_ip` NOT IN (' . implode(',', $excludedIPs) . ')' : '') . '),
			(SELECT MIN(`date`) FROM '.DB_PREFIX.'ianalytics_search_data WHERE store_id="'.$store_id.'"' . (!empty($excludedIPs) ? ' AND `from_ip` NOT IN (' . implode(',', $excludedIPs) . ')' : '').')
		) as min
		');
		
		if ($result->num_rows > 0) return $result->row['min'];
		else return '0000-00-00';
	}

	public function getOrders($data = array(), $store_id = 0) {
		$sql = "SELECT MIN(o.date_added) AS date_start, MAX(o.date_added) AS date_end, COUNT(*) AS `orders`, SUM((SELECT SUM(op.quantity) FROM `" . DB_PREFIX . "order_product` op WHERE op.order_id = o.order_id GROUP BY op.order_id)) AS products, SUM((SELECT SUM(ot.value) FROM `" . DB_PREFIX . "order_total` ot WHERE ot.order_id = o.order_id AND ot.code = 'tax' GROUP BY ot.order_id)) AS tax, SUM(o.total) AS `total` FROM `" . DB_PREFIX . "order` o";

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		
		$sql .= " AND o.store_id='" . $store_id . "'";

		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}

		switch($group) {
			case 'day';
				$sql .= " GROUP BY YEAR(o.date_added), MONTH(o.date_added), DAY(o.date_added)";
				break;
			default:
			case 'week':
				$sql .= " GROUP BY YEAR(o.date_added), WEEK(o.date_added)";
				break;
			case 'month':
				$sql .= " GROUP BY YEAR(o.date_added), MONTH(o.date_added)";
				break;
			case 'year':
				$sql .= " GROUP BY YEAR(o.date_added)";
				break;
		}

		$sql .= " ORDER BY o.date_added DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getOrdersCustomer($data = array(), $store_id = 0) {
		$sql = "SELECT c.customer_id, CONCAT(c.firstname, ' ', c.lastname) AS customer, c.email, cgd.name AS customer_group, c.status, COUNT(o.order_id) AS orders, SUM(o.total) AS `total` FROM `" . DB_PREFIX . "order` o 
			LEFT JOIN `" . DB_PREFIX . "customer` c ON (o.customer_id = c.customer_id) 
			LEFT JOIN `" . DB_PREFIX . "customer_group_description` cgd ON (c.customer_group_id = cgd.customer_group_id AND cgd.language_id = '" . (int)$this->config->get('config_language_id') . "') 
			WHERE o.customer_id > 0 AND o.store_id = '".$store_id."'";
	
		if (!empty($data['filter_order_status_id'])) {
			$sql .= " AND o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " AND o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		$sql .= " GROUP BY o.customer_id ORDER BY total DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		
        return $query->rows;
	}
	
	public function getPurchased($data = array()) {
        $sql = "SELECT op.name, op.model, SUM(op.quantity) AS quantity, SUM((op.price + op.tax) * op.quantity) AS total FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		$sql .= " AND o.store_id= '" . $this->db->escape($data['store_id']) . "'";

		$sql .= " GROUP BY op.product_id ORDER BY total DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalPurchased($data) {
		$sql = "SELECT COUNT(DISTINCT op.product_id) AS total FROM `" . DB_PREFIX . "order_product` op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		$sql .= " AND o.store_id= '" . $this->db->escape($data['store_id']) . "'";
		
		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getTrueCashData($data = array()) {
	    /*$query = $this->db->query("SELECT DISTINCT email, customer_id as cid, (SELECT SUM(points) from " . DB_PREFIX . "customer_reward WHERE customer_id = cid AND points > 0) AS earned, (SELECT SUM(points) FROM " . DB_PREFIX . "customer_reward WHERE customer_id = cid AND points < 0) AS used FROM oc_customer WHERE status = 1");

		return $query->rows;*/
		
		$email_sql = "SELECT DISTINCT c.email, c.customer_id FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "order o ON (c.customer_id = o.customer_id) WHERE c.status = 1";
		
		if(isset($data['filter_date_start']) && isset($data['filter_date_end'])) {
		    $email_sql .= " AND (DATE(o.date_modified) BETWEEN '" . $this->db->escape($data['filter_date_start']) . "' AND '" . $this->db->escape($data['filter_date_end']) . "')";
		}
		
		if(isset($data['filter_order_status_id']) && $data['filter_order_status_id'] > 0) {
		    $email_sql .= " AND o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
		    $email_sql .= " AND (o.order_status_id = '3' OR o.order_status_id = '5')";
		}
		
		/*if (isset($data['start']) || isset($data['limit'])) {
		    if ($data['start'] < 0) {
			   	$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
			  	$data['limit'] = 20;
			}

			$email_sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}*/
		
		$email_query = $this->db->query($email_sql);
		$tc_data = array();
		
		if($email_query->num_rows > 0) {
		    foreach($email_query->rows AS $customer) {
		        $tc_earned_sql = "SELECT SUM(points) AS earned FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . $customer['customer_id'] . "' AND points > 0";
		        if(isset($data['filter_date_start']) && isset($data['filter_date_end'])) {
		            $tc_earned_sql .= " AND (DATE(date_added) BETWEEN '" . $this->db->escape($data['filter_date_start']) . "' AND '" . $this->db->escape($data['filter_date_end']) . "')";
		        }
		        
		        $tc_earned_query = $this->db->query($tc_earned_sql);
		        if($tc_earned_query->row['earned'] > 0) {
		            $earned = $tc_earned_query->row['earned'];
		        } else {
		            $earned = 0;
		        }
		        
		        $tc_used_sql = "SELECT SUM(points) AS used FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . $customer['customer_id'] . "' AND points < 0";
		        if(isset($data['filter_date_start']) && isset($data['filter_date_end'])) {
		            $tc_used_sql .= " AND (DATE(date_added) BETWEEN '" . $this->db->escape($data['filter_date_start']) . "' AND '" . $this->db->escape($data['filter_date_end']) . "')";
		        }
		        
		        $tc_used_query = $this->db->query($tc_used_sql);
		        if($tc_used_query->row['used'] < 0) {
		            $used = abs($tc_used_query->row['used']);
		        } else {
		            $used = 0;
		        }
		        
		        
		        if($earned > 0 || $used > 0) {
		            $tc_data[] = array(
		                'customer_id'   => $customer['customer_id'],
		                'email'         => $customer['email'],
		                'earned'        => $earned,
		                'used'          => $used,
		                'href'          => $this->url->link('customer/customer/edit', 'customer_id=' . $customer['customer_id'] . '&token=' . $this->session->data['token'], 'SSL')
		            );
		        }
		    }
		}
		array_multisort(array_column($tc_data, 'earned' ), SORT_DESC, SORT_NUMERIC, $tc_data );
		
		return $tc_data;
	}
	
	public function getCitywiseData($data = array()) {
	    $sql = "SELECT DISTINCT shipping_city, COUNT(order_id) AS total_orders, SUM(total) AS sales FROM " . DB_PREFIX . "order WHERE shipping_city != ''";
	    
	    if(isset($data['filter_date_start']) && isset($data['filter_date_end'])) {
		    $sql .= " AND (DATE(date_modified) BETWEEN '" . $this->db->escape($data['filter_date_start']) . "' AND '" . $this->db->escape($data['filter_date_end']) . "')";
		}
		
		if(isset($data['filter_order_status_id']) && $data['filter_order_status_id'] > 0) {
		    $sql .= " AND order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
		    $sql .= " AND (order_status_id = '3' OR order_status_id = '5')";
		}
		
		$sql .= " AND store_id= '" . $this->db->escape($data['store_id']) . "'";
	    
	    $sql .= " GROUP BY shipping_city ORDER BY sales DESC";
	    
	    if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);
	    return $query->rows;
	}
	
	public function getCitywiseTotalSalesOrders($data = array()) {
	    $sql = "SELECT SUM(total) AS total_sales, COUNT(order_id) AS total_orders FROM " . DB_PREFIX . "order WHERE shipping_city != ''";

		if(isset($data['filter_date_start']) && isset($data['filter_date_end'])) {
		    $sql .= " AND (DATE(date_modified) BETWEEN '" . $this->db->escape($data['filter_date_start']) . "' AND '" . $this->db->escape($data['filter_date_end']) . "')";
		}
		
		if(isset($data['filter_order_status_id']) && $data['filter_order_status_id'] > 0) {
		    $sql .= " AND order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
		    $sql .= " AND (order_status_id = '3' OR order_status_id = '5')";
		}

		$sql .= " AND store_id= '" . $this->db->escape($data['store_id']) . "'";
		$query = $this->db->query($sql);

		return $query->row;
	}
	
	public function getTotalCitywiseData($data = array()) {
	    $sql = "SELECT COUNT(DISTINCT shipping_city) AS total FROM " . DB_PREFIX . "order WHERE shipping_city != ''";

		if(isset($data['filter_date_start']) && isset($data['filter_date_end'])) {
		    $sql .= " AND (DATE(date_modified) BETWEEN '" . $this->db->escape($data['filter_date_start']) . "' AND '" . $this->db->escape($data['filter_date_end']) . "')";
		}
		
		if(isset($data['filter_order_status_id']) && $data['filter_order_status_id'] > 0) {
		    $sql .= " AND order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
		    $sql .= " AND (order_status_id = '3' OR order_status_id = '5')";
		}

		$sql .= " AND store_id= '" . $this->db->escape($data['store_id']) . "'";
		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getNewOldCustomersData($data = array()) {
	    $new_customer_count = 0;
	    $new_order_count = 0;
	    $new_customer_sales = 0;
	    $old_customer_count = 0;
	    $old_order_count = 0;
	    $old_customer_sales = 0;
	    
	    $sql = "SELECT DISTINCT customer_id, COUNT(order_id) AS total_orders, SUM(total) AS total_sales FROM " . DB_PREFIX . "order WHERE order_status_id > 0";
	    
	    if(isset($data['filter_date_start']) && isset($data['filter_date_end'])) {
		    $sql .= " AND (DATE(date_modified) BETWEEN '" . $this->db->escape($data['filter_date_start']) . "' AND '" . $this->db->escape($data['filter_date_end']) . "')";
		}
		
		if(isset($data['filter_order_status_id']) && $data['filter_order_status_id'] > 0) {
		    $sql .= " AND order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
		    $sql .= " AND (order_status_id = '3' OR order_status_id = '5')";
		}
		$sql .= " AND store_id= '" . $this->db->escape($data['store_id']) . "'";
	    
	    $sql .= " GROUP BY customer_id";
	    
	    $query = $this->db->query($sql);
        
        foreach($query->rows AS $new_old) {
            if($new_old['total_orders'] == 1) {
                $new_customer_count += 1;
                $new_order_count += 1;
                $new_customer_sales += $new_old['total_sales'];
                
            } else if($new_old['total_orders'] > 1) {
                $old_customer_count += 1;
                $old_order_count += $new_old['total_orders'];
                $old_customer_sales += $new_old['total_sales'];
            }
        }
        
        $data = array(
            'new_customer_count'    => $new_customer_count,
            'new_order_count'       => $new_order_count,
            'new_customer_sales'    => $new_customer_sales,
            'old_customer_count'    => $old_customer_count,
            'old_order_count'       => $old_order_count,
            'old_customer_sales'    => $old_customer_sales
        );
        
        return $data;
	}

}