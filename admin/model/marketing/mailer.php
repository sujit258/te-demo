<?php 
/*** jitendra kumar sharma (vxinfosystem.com) jks0586@gmail.com, vxinfosystem@gmail.com ****/

class ModelMarketingMailer extends Model {
	public function addreview($data){
			//$this->db->query("INSERT into " .DB_PREFIX. "review SET product_id = '".(int)$data['product_id']."', customer_id = '". (int)$data['customer_id'] ."', author = '".$this->db->escape($data['author'])."', text = '" . $this->db->escape($data['text']) . "', rating = '" . (int)$data['rating'] . "',status = '".(int)$data['status']."', date_added = NOW()");
			//return 2;
		$today = date("Y-m-d");
		
		$check_if_exist = $this->db->query("SELECT email FROM " .DB_PREFIX. "customer_mailer WHERE email = '".$this->db->escape($data['email'])."'AND DATE(date_added) = DATE('" . $this->db->escape($today) . "')");
		
		if (!$check_if_exist->num_rows) {	
		    $this->db->query("INSERT into " .DB_PREFIX. "customer_mailer SET  customer_id = '". (int)$data['customer_id'] ."', email = '".$this->db->escape($data['email'])."', name = '" . $this->db->escape($data['name']) . "', type = '" . $this->db->escape($data['type']) . "', date_added = NOW()");
			
		    return 2;
	    }
	
	}
	
    public function getTypes() {
		$sql = "SELECT DISTINCT(type),DATE(date_added) as date_added FROM " . DB_PREFIX . "customer_mailer";

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getCustomers($data = array()) {
		$sql = "SELECT *  FROM " . DB_PREFIX . "customer_mailer ";

		if (!empty($data['filter_type'])) {
			$implode[] = "type LIKE '" . $this->db->escape($data['filter_type']) . "%'";
		} 
        
        $today = date("Y-m-d");
  
		$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($today) . "')";
		 

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
 

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	
	public function getTotalCustomers($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_mailer";

		$implode = array();
 
		if (!empty($data['filter_type'])) {
			$implode[] = "type LIKE '" . $this->db->escape($data['filter_type']) . "%'";
		} 
        
        $today = date("Y-m-d");
  
		$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($today) . "')";
		 

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
}
?>