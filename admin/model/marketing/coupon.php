<?php
class ModelMarketingCoupon extends Model {
	public function addCoupon($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "coupon SET name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', display_cart = '" . $this->db->escape($data['display_cart']) . "', common_coupon = '" . $this->db->escape($data['common_coupon']) . "', description = '" . $this->db->escape($data['coupon_desc']) . "',success_message = '" . $this->db->escape($data['coupon_success_message']) . "', discount = '" . (float)$data['discount'] . "', min = '" . (float)$data['min'] . "', max = '" . (float)$data['max'] . "', type = '" . $this->db->escape($data['type']) . "', logged = '" . (int)$data['logged'] . "', shipping = '" . (int)$data['shipping'] . "', default1 = '" . (int)$data['default1'] . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "', uses_total = '" . (int)$data['uses_total'] . "', uses_customer = '" . (int)$data['uses_customer'] . "', customer_group = '" . $this->db->escape(isset($data['coupon_customer_group']) ? serialize($data['coupon_customer_group']) : '') . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

		$coupon_id = $this->db->getLastId();

		if (isset($data['coupon_product'])) {
			foreach ($data['coupon_product'] as $product_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "coupon_product SET coupon_id = '" . (int)$coupon_id . "', product_id = '" . (int)$product_id . "'");
			}
		}
		
		//SOF excluded products
		if (isset($data['coupon_excludedproduct'])) {
			foreach ($data['coupon_excludedproduct'] as $product_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "coupon_excludedproduct SET coupon_id = '" . (int)$coupon_id . "', product_id = '" . (int)$product_id . "'");
			}
		}
		//EOF Ecluded Products

		if (isset($data['coupon_category'])) {
			foreach ($data['coupon_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "coupon_category SET coupon_id = '" . (int)$coupon_id . "', category_id = '" . (int)$category_id . "'");
			}
		}

		return $coupon_id;
	}
	
	// S-coupon
	public function checkFieldCoupon() {
		$hasModelDefault = FALSE;
		$result = $this->db->query( "DESCRIBE `".DB_PREFIX."coupon`;" );
		  foreach ($result->rows as $row) {
		   if ($row['Field'] == 'customer_group') {
		    $hasModelDefault = TRUE;
		    break;
		  }
		}
		if (!$hasModelDefault) {
		 $sql = "ALTER TABLE `".DB_PREFIX."coupon` ADD `customer_group` TEXT NOT NULL COLLATE utf8_general_ci";
		 $this->db->query( $sql );
		}
	}
				//	S-coupon
				
				
	public function editCoupon($coupon_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "coupon SET name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', display_cart = '" . $this->db->escape($data['display_cart']) . "', common_coupon = '" . $this->db->escape($data['common_coupon']) . "', description = '" . $this->db->escape($data['coupon_desc']) . "', success_message = '" . $this->db->escape($data['coupon_success_message']) . "', discount = '" . (float)$data['discount'] . "', min = '" . (float)$data['min'] . "', max = '" . (float)$data['max'] . "', type = '" . $this->db->escape($data['type']) . "', logged = '" . (int)$data['logged'] . "', shipping = '" . (int)$data['shipping'] . "', default1 = '" . (int)$data['default1'] . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "', uses_total = '" . (int)$data['uses_total'] . "', uses_customer = '" . (int)$data['uses_customer'] . "', customer_group = '" . $this->db->escape(isset($data['coupon_customer_group']) ? serialize($data['coupon_customer_group']) : '') . "', status = '" . (int)$data['status'] . "' WHERE coupon_id = '" . (int)$coupon_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product WHERE coupon_id = '" . (int)$coupon_id . "'");

		if (isset($data['coupon_product'])) {
			foreach ($data['coupon_product'] as $product_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "coupon_product SET coupon_id = '" . (int)$coupon_id . "', product_id = '" . (int)$product_id . "'");
			}
		}
		
		//SOF excluded products
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_excludedproduct WHERE coupon_id = '" . (int)$coupon_id . "'");
		
		if (isset($data['coupon_excludedproduct'])) {
			foreach ($data['coupon_excludedproduct'] as $product_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "coupon_excludedproduct SET coupon_id = '" . (int)$coupon_id . "', product_id = '" . (int)$product_id . "'");
			}
		}
		//EOF Ecluded Products

		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_category WHERE coupon_id = '" . (int)$coupon_id . "'");

		if (isset($data['coupon_category'])) {
			foreach ($data['coupon_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "coupon_category SET coupon_id = '" . (int)$coupon_id . "', category_id = '" . (int)$category_id . "'");
			}
		}
	}

	public function deleteCoupon($coupon_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon WHERE coupon_id = '" . (int)$coupon_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product WHERE coupon_id = '" . (int)$coupon_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_excludedproduct WHERE coupon_id = '" . (int)$coupon_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_category WHERE coupon_id = '" . (int)$coupon_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_history WHERE coupon_id = '" . (int)$coupon_id . "'");
	}

	public function getCoupon($coupon_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "coupon WHERE coupon_id = '" . (int)$coupon_id . "'");

		return $query->row;
	}
	
	public function getCouponOrderTotal($coupon_id){
	    
	    $sql = "SELECT SUM(ch.order_total) AS ordertotal FROM `" . DB_PREFIX . "coupon_history` ch LEFT JOIN `" . DB_PREFIX . "coupon` c ON (ch.coupon_id = c.coupon_id) WHERE c.coupon_id = $coupon_id";
	    	
	    $query = $this->db->query($sql);

		return $query->row['ordertotal']; 
	}
	
	public function getCouponOrders($coupon_id){
	    
	    $sql = "SELECT COUNT(DISTINCT ch.order_id) AS orders FROM `" . DB_PREFIX . "coupon_history` ch  WHERE ch.coupon_id = $coupon_id";
	    	
	    $query = $this->db->query($sql);

		return $query->row['orders']; 
	}
	
	public function getCouponOrderIds($coupon_id){
	    
	    $sql = "SELECT ch.order_id AS orders FROM `" . DB_PREFIX . "coupon_history` ch  WHERE ch.coupon_id = $coupon_id";
	    	
	    $query = $this->db->query($sql);

		return $query->rows; 
	}
    
	public function getCouponByCode($code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "coupon WHERE code = '" . $this->db->escape($code) . "'");

		return $query->row;
	}

	public function getCoupons($data = array()) {
		$sql = "SELECT coupon_id, name, code, default1,success_message, discount, date_start, date_end, customer_group, status FROM " . DB_PREFIX . "coupon";
		
			if (isset($data['status'])) {
			$implode = array();

			$coupon_statuses = explode(',', $data['status']);

			foreach ($coupon_statuses as $coupon_status_id) {
				$implode[] = "status = '" . (int)$coupon_status_id . "'";
			}

			if ($implode) {
				$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
			} else {

			}
		} else {
			$sql .= " WHERE status > '0'";
		}

		if (!empty($data['name'])) {
			$sql .= " AND name LIKE '%" . $data['name'] . "%'";
		}

		if (!empty($data['code'])) {
			$sql .= " AND code LIKE '%" . $data['code'] . "%'";
		}

		if (!empty($data['date_start'])) {
			$sql .= " AND DATE(date_start) = DATE('" . $this->db->escape($data['date_start']) . "')";
		}

		if (!empty($data['date_end'])) {
			$sql .= " AND DATE(date_end) = DATE('" . $this->db->escape($data['date_end']) . "')";
		}

		if (!empty($data['discount'])) {
			$sql .= " AND discount = '" . (float)$data['discount'] . "'";
		}


		$sort_data = array(
			'name',
			'code',
			'discount',
			'date_start',
			'date_end',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCouponProducts($coupon_id) {
		$coupon_product_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_product WHERE coupon_id = '" . (int)$coupon_id . "'");

		foreach ($query->rows as $result) {
			$coupon_product_data[] = $result['product_id'];
		}

		return $coupon_product_data;
	}
	
	//SOF excluded products
	public function getCouponExcludedProducts($coupon_id) {
		$coupon_excludedproduct_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_excludedproduct WHERE coupon_id = '" . (int)$coupon_id . "'");

		foreach ($query->rows as $result) {
			$coupon_excludedproduct_data[] = $result['product_id'];
		}

		return $coupon_excludedproduct_data;
	}
	//EOF Ecluded Products

	public function getCouponCategories($coupon_id) {
		$coupon_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_category WHERE coupon_id = '" . (int)$coupon_id . "'");

		foreach ($query->rows as $result) {
			$coupon_category_data[] = $result['category_id'];
		}

		return $coupon_category_data;
	}

    public function getTotalCoupons($data = array()) {
 		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "coupon";
			if (isset($data['status'])) {
				$implode = array();

				$coupon_statuses = explode(',', $data['status']);

			foreach ($coupon_statuses as $coupon_status_id) {
					$implode[] = "status = '" . (int)$coupon_status_id . "'";
						}

				if ($implode) {
					$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
				} else {

				}
			} else {
				$sql .= " WHERE status > '0'";
			}

					if (!empty($data['name'])) {
						$sql .= " AND name LIKE '%" . $data['name'] . "%'";
					}

					if (!empty($data['code'])) {
						$sql .= " AND code LIKE '%" . $data['code'] . "%'";
					}

					if (!empty($data['date_start'])) {
						$sql .= " AND DATE(date_start) = DATE('" . $this->db->escape($data['date_start']) . "')";
					}

					if (!empty($data['date_end'])) {
						$sql .= " AND DATE(date_end) = DATE('" . $this->db->escape($data['date_end']) . "')";
					}

					if (!empty($data['discount'])) {
						$sql .= " AND discount = '" . (float)$data['discount'] . "'";
					}
					$query = $this->db->query($sql);
	
		return $query->row['total'];
	}

	public function getCouponHistories($coupon_id, $start = 0, $limit = 10) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 10;
		}

		$query = $this->db->query("SELECT ch.order_id,o.total, CONCAT(c.firstname, ' ', c.lastname) AS customer,(SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status, ch.amount, ch.order_total, ch.date_added FROM " . DB_PREFIX . "coupon_history ch LEFT JOIN " . DB_PREFIX . "order o ON (ch.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "customer c ON (ch.customer_id = c.customer_id) WHERE ch.coupon_id = '" . (int)$coupon_id . "' ORDER BY ch.date_added ASC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalCouponHistories($coupon_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "coupon_history WHERE coupon_id = '" . (int)$coupon_id . "'");

		return $query->row['total'];
	}
}