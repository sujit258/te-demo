<?php
$_['instagramshopgallery'] = array(
    'title'     => 'InstagramShopGallery',
    'name'      => $name = 'instagramshopgallery',
    'version'   => '3.1.1',

    // Internal
    'token'     => '',
    'store_id'  => 0,
    'code'      => 'module_' . $name,
    'path'      => 'extension/module/' . $name,
    'model'     => 'model_extension_module_' . $name,
    'ext_link'  => 'marketplace/extension',
    'ext_type'  => '&type=module',

    // Default setting
    'setting'   => array(
        'status'    => '0',
        'hashtag'   => '',
        'module'    => array(
            'title'         => '',
            'status'        => '0',
            'visibility'    => 'all',
            'limit'         => '12',
            'extra_image'   => '',
            'extra_link'    => '',
            'custom_css'    => '',
        ),
        'page'      => array(
            'title'         => '',
            'status'        => '0',
            'navbar'        => '1',
            'visibility'    => 'all',
            'limit'         => '18',
            'banner'        => '',
            'banner_link'   => '',
            'custom_css'    => '',
            'meta_title'    => '',
            'meta_desc'     => '',
            'meta_keyword'  => '',
            'seo_url'       => ''
        )
    )
);
