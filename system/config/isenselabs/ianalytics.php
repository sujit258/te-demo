<?php
$_['ianalytics_name']           = 'ianalytics';
$_['ianalytics_model']          = 'model_extension_module_ianalytics';
$_['ianalytics_path']           = 'extension/module/ianalytics';
$_['ianalytics_version']        = '3.4.1';

$_['ianalytics_link']           = 'extension/extension';
$_['ianalytics_link_params']    = '&type=module';