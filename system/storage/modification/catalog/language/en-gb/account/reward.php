<?php
// Heading
$_['heading_title']      = 'Your True Cash Transactions';

// Column
$_['column_date_added']  = 'Date Added';
$_['column_description'] = 'Description';
$_['column_points']      = 'True Cash';

// Text

				$_['text_registration']  = 'Registration True Cash';
				$_['text_newsletter']    = 'Newsletter True Cash';
				$_['text_unsubscribe']   = 'Unsubscribe True Cash';
				
$_['text_account']       = 'Account';
$_['text_reward']        = 'True Cash';
$_['text_total']         = 'Your total True Cash is:';
$_['text_empty']         = 'You do not have any True Cash!';