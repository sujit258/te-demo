<?php
class ModelExtensionTotalSalescombo extends Model {
	public function getTotal($total) {
	     
	    $totaldiscount = array(); 
	    
	    $cart_total = $this->cart->getTotal();
	    
	    if($cart_total > 999) {
	    
    	    foreach ($this->cart->getProducts() as $product) {
    		    //print_r("<br>");
    		    
    		    $price += $this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'));
    		    
    		     
    		    $discountdone = $price / 100 * 15;
    		         
        		if ($product['tax_class_id']) {
        			$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discountdone), $product['tax_class_id']);
        			foreach ($tax_rates as $tax_rate) {
        				if ($tax_rate['type'] == 'P') {
        					$total['taxes'][$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
        				}
        			}
        		}
        		
        		$discount_total += $discountdone;
            	     	
    	    } 
	        
	    } 
    	    if ($discountdone > 0 ) {
    				$total['totals'][] = array(
    					'code'       => 'above999off',
    					'title'      => "15% Off Above Rs.999",
    					'value'      => -$discount_total,
    					'sort_order' => $this->config->get('credit_sort_order')
    				);
    
    				$total['total'] -= $discount_total;
    		}	
    		
	    
	    $offerquantity = 0;
	    $discountdone = 0;
	    $price = 0;  
	    $true = 0;
	    
	    $count = 1;
		    foreach ($this->cart->getProducts() as $product) {
		        
		        $freehitqueries = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product['product_id'] . "' AND category_id ='624'");
		    
    		    if ($freehitqueries->row && $count == 1){
    		        $freequantity = $product['quantity'];
    		        
     		        if($this->cart->getTotal() > 500){
     		            
    		            if($freehitqueries->row['product_id'] == $product['product_id']){
    		                
    		                $freehitprice = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
    		                
    		                $freediscountdone = $freehitprice;
    		                
    		                $this->session->data['freehitcart'] = $product['product_id'];
    		            } 
    		            $count++;
    		        }
    		    }
		         
		        
    		    if ($product['tax_class_id']) {
    				$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $freehitprice), $product['tax_class_id']);
    						foreach ($tax_rates as $tax_rate) {
    							if ($tax_rate['type'] == 'P') {
    								$total['taxes'][$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
    							}
    						}
    					}
    				$freediscount_total = $freehitprice;
        	    }	   
        	     
    	
        	    if ($freehitprice > 0) {
        				$total['totals'][] = array(
        					'code'       => 'freehit',
        					'title'      => "TryMe",
        					'value'      => -$freediscount_total,
        					'sort_order' => $this->config->get('credit_sort_order')
        				);
        
        				$total['total'] -= $freediscount_total;
        		}
	}
	

        public function combo1a($product_id) {
    $this->load->language('extension/total/salescombo');
    $data['addqty'] = $this->language->get('addqty');
    $data['getqty'] = $this->language->get('getqty');
    $data['offertag'] = $this->language->get('offertag');
    $data['button_bundle'] = $this->language->get('button_bundle');
    $data['offers'] = array();
    $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "cartbindercombo1a_setting WHERE status = '1' AND showoffer = '1' AND FIND_IN_SET('".$product_id."',`primarypids`)>0 AND displaylocation = 0");
    foreach ($query->rows as $key => $value) {
      if($this->checkCg($value['cids'])) {continue;}
      $data['offers'] = $this->generateOffer($value);
    }
    return $this->load->view('extension/module/salescombo', $data);
  }

  public function combo1atab($product_id) {
    $this->load->language('extension/total/salescombo');
    $data['addqty'] = $this->language->get('addqty');
    $data['getqty'] = $this->language->get('getqty');
    $data['offertag'] = $this->language->get('offertag');
    $data['button_bundle'] = $this->language->get('button_bundle');
    $data['offers'] = array();
    $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "cartbindercombo1a_setting WHERE status = '1' AND showoffer = '1' AND FIND_IN_SET('".$product_id."',`primarypids`)>0 AND displaylocation = 1");
    foreach ($query->rows as $key => $value) {
      if($this->checkCg($value['cids'])) {continue;}
      $data['offers'] = $this->generateOffer($value);
    }
    return $this->load->view('extension/module/salescombo', $data);
  }
        

        public function combo1b($product_id) {
    $this->load->language('extension/total/salescombo');
    $data['addqty'] = $this->language->get('addqty');
    $data['getqty'] = $this->language->get('getqty');
    $data['offertag'] = $this->language->get('offertag');
    $data['foroptions'] = $this->language->get('foroptions');
    $data['button_bundle'] = $this->language->get('button_bundle');
    $data['offers'] = array();
    $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "cartbindercombo1b_setting WHERE status = '1' AND showoffer = '1' AND FIND_IN_SET('".$product_id."',`primarypids`)>0 AND displaylocation = 0");
    foreach ($query->rows as $key => $value) {
      if($this->checkCg($value['cids'])) {continue;}
      $data['offers'] = $this->generateOffer($value);
    }
    return $this->load->view('extension/module/salescombo', $data);
  }

  public function combo1btab($product_id) {
    $this->load->language('extension/total/salescombo');
    $data['addqty'] = $this->language->get('addqty');
    $data['getqty'] = $this->language->get('getqty');
    $data['offertag'] = $this->language->get('offertag');
    $data['foroptions'] = $this->language->get('foroptions');
    $data['button_bundle'] = $this->language->get('button_bundle');
    $data['offers'] = array();
    $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "cartbindercombo1b_setting WHERE status = '1' AND showoffer = '1' AND FIND_IN_SET('".$product_id."',`primarypids`)>0 AND displaylocation = 1");
    foreach ($query->rows as $key => $value) {
      if($this->checkCg($value['cids'])) {continue;}
      $data['offers'] = $this->generateOffer($value);
    }
    return $this->load->view('extension/module/salescombo', $data);
  }
        

        public function combo1c($product_id) {
    $this->load->language('extension/total/salescombo');
    $data['addqty'] = $this->language->get('addqty');
    $data['getqty'] = $this->language->get('getqty');
    $data['offertag'] = $this->language->get('offertag');
    $data['foroptions'] = $this->language->get('foroptions');
    $data['button_bundle'] = $this->language->get('button_bundle');
    $data['offers'] = array();
    $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "cartbindercombo1c_setting WHERE status = '1' AND showoffer = '1' AND FIND_IN_SET('".$product_id."',`primarypids`)>0 AND displaylocation = 0");
    foreach ($query->rows as $key => $value) {
      if($this->checkCg($value['cids'])) {continue;}
      $data['offers'] = $this->generateOffer($value);
    }
    
    return $this->load->view('extension/module/salescombo', $data);
  }

   public function combo1ctab($product_id) {
    $this->load->language('extension/total/salescombo');
    $data['addqty'] = $this->language->get('addqty');
    $data['getqty'] = $this->language->get('getqty');
    $data['offertag'] = $this->language->get('offertag');
    $data['foroptions'] = $this->language->get('foroptions');
    $data['button_bundle'] = $this->language->get('button_bundle');
    $data['offers'] = array();
    $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "cartbindercombo1c_setting WHERE status = '1' AND showoffer = '1' AND FIND_IN_SET('".$product_id."',`primarypids`)>0 AND displaylocation = 1");
    foreach ($query->rows as $key => $value) {
      if($this->checkCg($value['cids'])) {continue;}
      $data['offers'] = $this->generateOffer($value);
    }
    
    return $this->load->view('extension/module/salescombo', $data);
  }
        

        public function combo1($product_id) {
    $this->load->language('extension/total/salescombo');
    $data['addqty'] = $this->language->get('addqty');
    $data['getqty'] = $this->language->get('getqty');
    $data['offertag'] = $this->language->get('offertag');
    $data['button_bundle'] = $this->language->get('button_bundle');
    $data['offers'] = array();
    $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "cartbindercombo1_setting WHERE status = '1' AND showoffer = '1' AND FIND_IN_SET('".$product_id."',`primarypids`)>0 AND displaylocation = 0");
    foreach ($query->rows as $key => $value) {
      if($this->checkCg($value['cids'])) {continue;}
      $data['offers'] = $this->generateOffer($value);
    }
    return $this->load->view('extension/module/salescombo', $data);
  }

  public function combo1tab($product_id) {
    $this->load->language('extension/total/salescombo');
    $data['addqty'] = $this->language->get('addqty');
    $data['getqty'] = $this->language->get('getqty');
    $data['offertag'] = $this->language->get('offertag');
    $data['button_bundle'] = $this->language->get('button_bundle');
    $data['offers'] = array();
    $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "cartbindercombo1_setting WHERE status = '1' AND showoffer = '1' AND FIND_IN_SET('".$product_id."',`primarypids`)>0 AND displaylocation = 1");
    foreach ($query->rows as $key => $value) {
      if($this->checkCg($value['cids'])) {continue;}
      $data['offers'] = $this->generateOffer($value);
    }
    return $this->load->view('extension/module/salescombo', $data);
  }
        
	public function getAvailableOffers($product_id) {
		$availableoffers['notab'] = "";
		$availableoffers['tab'] = "";
$availableoffers['notab'] .= $this->combo1a($product_id);$availableoffers['tab'] .= $this->combo1atab($product_id);
$availableoffers['notab'] .= $this->combo1b($product_id);$availableoffers['tab'] .= $this->combo1btab($product_id);
$availableoffers['notab'] .= $this->combo1c($product_id);$availableoffers['tab'] .= $this->combo1ctab($product_id);
$availableoffers['notab'] .= $this->combo1($product_id);$availableoffers['tab'] .= $this->combo1tab($product_id);
		return $availableoffers;
	}

	public function checkCg($cids = array()) {
  		$cggrup = json_decode($cids,true);
        if(!empty($cggrup)) {
        	$cgid = $this->customer->getGroupId();
         	if(!in_array($cgid, $cggrup)) {
            	return 1;
         	}
        }
        return 0;
    }
	
	public function optionnames($optionids) {
		$optionarray = explode(",", $optionids);
		
		$name = $out = "";
		foreach ($optionarray as $key => $value) {
			$optionname =  $this->getOptionName($value);
			$name .= $out.$optionname;
			$out = "<br>";
		}
		return $name;
		
	}

	public function getOptionName($product_option_value_id) {
		if($product_option_value_id) {
			$product_option_value_query = $this->db->query("SELECT ovd.name as name FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (pov.option_value_id = ovd.option_value_id)  WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND ovd.language_id = '".$this->config->get('config_language_id')."' ");
			if($product_option_value_query->num_rows) {
				return $product_option_value_query->row['name'];
			}
		} 
		return "";
	}

	public function generateOffer($value = array()) {
		  $primaryarray = explode(",",$value['primarypids']);
	      $this->load->model("catalog/product");
	      $this->load->model("catalog/category");
	      $this->load->model("offers/salescombopge");
	      $this->load->model('tool/image');
	      $data['offers'][$value['id']]['id'] = $value['variation']."_".$value['id'];
	      $data['offers'][$value['id']]['name'] = $value['name'];
	      $data['offers'][$value['id']]['bundle'] = isset($value['bundle'])?$value['bundle']:0;
	      if($value['type']) {
	        $discount = $this->currency->format($value['discount'], $this->session->data['currency']);
	        $data['offers'][$value['id']]['offervalue'] = sprintf($this->language->get("fixedoff"),$discount);
	      } else {
	        $data['offers'][$value['id']]['offervalue'] = sprintf($this->language->get("percentageoff"),$value['discount']);
	      }
	      $data['offers'][$value['id']]['offerpage'] = "";
	      if($value['sales_offer_id']) {
	        $salescombopge_info = $this->model_offers_salescombopge->getPage($value['sales_offer_id']);
	        if ($salescombopge_info) {
	          $url = $this->url->link('offers/salescombopge', 'page_id=' .  $value['sales_offer_id'], "SSL");
	          $data['offers'][$value['id']]['offerpage'] = sprintf($this->language->get('readmore_offerpage'), $url, $salescombopge_info['title']);
	        }
	      }

	      $data['offers'][$value['id']]['addproducts'] = array();
	      foreach ($primaryarray as $key => $product_id) {
	        $product_info = $this->model_catalog_product->getProduct($product_id);
	        $product_name = $product_info['name'];
	        if ($product_info['image']) {
	          $thumb = $this->model_tool_image->resize($product_info['image'], 75, 75);
	        } else {
	          $thumb = $this->model_tool_image->resize('placeholder.png', 75, 75);
	        }
	        if($value['variation'] == 6) {
	        	$optionnames = $this->optionnames($value['optionids']);
	        } else {
	        	$optionnames = "";
	        }
	        $data['offers'][$value['id']]['addproducts'][] = array(
	          'name' => $product_name,
	          'product_id' => $product_id,
	          'href' => $this->url->link('product/product', 'product_id=' . $product_id, TRUE),
	          'thumb' => $thumb,
	          'priqty' => $value['primaryquant'],
	          'secqty' => $value['secondaryquant'],
	          'optionnames' => $optionnames,
	        );
	      }

	      if($value['variation'] == 3 || $value['variation'] == 6) {
	      	$data['offers'][$value['id']]['getproducts'] = $data['offers'][$value['id']]['addproducts'];
	      } else if ($value['variation'] == 5) {
	      	$secondaryarray = explode(",",$value['secondarycids']);
		    $data['offers'][$value['id']]['getcategories'] = array();
		    foreach ($secondaryarray as $key => $category_id) {
		        $category_info = $this->model_catalog_category->getCategory($category_id);
		        $category_name = $category_info['name'];
		        if ($category_info['image']) {
		          $thumb = $this->model_tool_image->resize($category_info['image'], 75, 75);
		        } else {
		          $thumb = $this->model_tool_image->resize('placeholder.png', 75, 75);
		        }
		        $data['offers'][$value['id']]['getcategories'][] = array(
		          'name' => $category_name,
		          'href' => $this->url->link('product/category', 'path=' . $category_id, TRUE),
		          'thumb' => $thumb,
		          'secqty' => $value['secondaryquant'],
		        );
		    }
	      } else if($value['variation'] == 1) {
	      	$secondaryarray = explode(",",$value['secondarypids']);
	        $data['offers'][$value['id']]['getproducts'] = array();
	        foreach ($secondaryarray as $key => $product_id) {
		        $product_info = $this->model_catalog_product->getProduct($product_id);
		        $product_name = $product_info['name'];
		        if ($product_info['image']) {
		          $thumb = $this->model_tool_image->resize($product_info['image'], 75, 75);
		        } else {
		          $thumb = $this->model_tool_image->resize('placeholder.png', 75, 75);
		        }
		        $data['offers'][$value['id']]['getproducts'][] = array(
		          'name' => $product_name,
		          'product_id' => $product_id,
		          'href' => $this->url->link('product/product', 'product_id=' . $product_id, TRUE),
		          'thumb' => $thumb,
		          'secqty' => $value['secondaryquant'],
		        );
	        }
	      }
	      return $data['offers'];
	}
}