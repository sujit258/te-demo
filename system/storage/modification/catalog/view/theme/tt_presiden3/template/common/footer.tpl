<?php if(isset($block4)){ echo $block4; } ?>
<footer>
	<div class="footer-static">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-8 col-xs-12">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-5">
							<div class="col col1">
								<div class="footer-title">
									<h3><?php echo $text_information; ?></h3>
								</div>
								<div class="footer-content">
									<ul class="list-unstyled">
										<?php foreach ($informations as $information) { ?>
											<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
							<div class="col col2">
								<div class="footer-title">
									<h3><?php echo $text_service; ?></h3>
								</div>
								<div class="footer-content">
									<ul class="list-unstyled">
										<li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
										<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
										<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
										<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3">
							<div class="col col3">
								<div class="footer-title"><h3><?php echo $text_account; ?></h3></div>
								<div class="footer-content">
									<ul class="list-unstyled">
										 <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
										  <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
										  <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
										  <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-xs-12">
					<div class="col col4">
						<div class="footer-title"><h3><?php echo $text_newsletter; ?></h3></div>
						<div class="footer-content">
							<?php if(isset($block5)){ ?>
								<?php echo $block5; ?>
							<?php } ?>
							<?php if(isset($block6)){ ?>
								<?php echo $block6; ?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="container-inner">
				<div class="link-footer">
					<ul>
						<li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
						<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
						<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
						<li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
					</ul>
				</div>
				<p><?php echo $powered; ?></p>
				<?php if(isset($block7)){ ?>
					<?php echo $block7; ?>
				<?php } ?>
			</div>
		</div>
	</div>
  
  <div id="back-top"></div>
<script type="text/javascript">
	$(document).ready(function(){
		// hide #back-top first
		$("#back-top").hide();
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 300) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});
			// scroll body to 0px on click
			$('#back-top').click(function () {
				$('body,html').animate({scrollTop: 0}, 800);
				return false;
			});
		});
		if (jQuery(window).width() < 992) {   
			jQuery('.footer-static .footer-title').click(function(){     
				jQuery(this).parent('.col').toggleClass('active').siblings().removeClass('active'); 
				jQuery(this).next('.col .footer-content').toggle(300);
				jQuery(this).parent('.col.active').siblings().children('.col .footer-content').slideUp(300); 
			}); 
			 
		}
	});
</script>


        <?php if ($facebook_messenger_enabled_FAE == 'true') { ?>
        <!-- Facebook JSSDK -->
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId            : '',
              autoLogAppEvents : true,
              xfbml            : true,
              version          : '<?= $facebook_jssdk_version_FAE ?>'
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "https://connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
        <div
          class="fb-customerchat"
          attribution="fbe_opencart"
          page_id="<?= $facebook_page_id_FAE ?>"
        />
        <?php } ?>
      
</footer>

        <?php if (isset($smca_status) || isset($smac_status)) { ?>
          <!-- start: OCdevWizard Setting -->
          <script type="text/javascript">     
            var ocdev_modules = [];
                  
            <?php if (isset($smca_status) && $smca_status == 1) { ?>
              ocdev_modules.push({
                src:  'index.php?route=ocdevwizard/smart_cart',
                type:'ajax'
              });
            <?php } ?>
            <?php if (isset($smac_status) && $smac_status == 1 && $smart_abandoned_cart == 1) { ?>
              ocdev_modules.push({
                src:  'index.php?route=ocdevwizard/smart_abandoned_cart',
                type:'ajax'
              });
            <?php } ?>
          </script>
          <!-- end: OCdevWizard Setting -->
        <?php } ?>
      

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->


            	<script type="text/javascript">
                  var ref = (typeof document.referrer != 'undefined') ? document.referrer : "";
                  $.ajax({
                    type: "post",	
                    data: {"ref" : ref},
                    url:"index.php?route=extension/module/ianalytics/register_iAnalyticsVisits",
                  });
                </script>
			

	
			<?php if ($config_whatsappid) { ?>
		   		
			<div class="te-sj">
			<a class="te-sj whatsapp" href="whatsapp://send?phone=<?php echo $config_whatsappid; ?>&text=Hi" />
<img src="image/catalog/xtensions/whatsapp.png" alt="Whatsapp Us" style="width: 45px;border-top-left-radius: 4px;border-top-right-radius: 4px;"></a></div>


			<?php } ?>
			
			
		
		
			
</body></html>