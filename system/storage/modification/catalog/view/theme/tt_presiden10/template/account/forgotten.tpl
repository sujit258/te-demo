<?php echo $header; ?>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
  <br />  <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
  
  <div class="col-md-4 col-xs-1"></div>
     <div class="col-md-4 col-xs-10" style="border: 1px solid rgb(240, 240, 240);margin: 10px 10px 10px 10px;"><br />
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_email; ?></p>
     
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <?php echo $text_your_email; ?> 
          <div class="form-group required">
          
            <div class="col-md-12">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
          </div>
        </fieldset>

				<!--// Add from ocmod -->
		    <div id="continue">
		    <!--//  -->
			
        <div class="buttons clearfix">
          <div class="pull-left"><input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" /></div>
          <div class="pull-right">
            
          </div>
        </div>

				<!--// Add from ocmod -->
		  	</div>
		  	<div class="verify"><?php echo $verify; ?></div>
		  	<!--//  -->
			
      </form><br />
      </div>
       <div class="col-md-4 col-xs-1"></div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>