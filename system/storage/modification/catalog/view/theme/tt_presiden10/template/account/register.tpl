<?php echo $header; ?>


<!--Google Login Script-->
<script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      auth2 = gapi.auth2.init({
        client_id: '688467726932-ftoln2tngeqru6ahv73sqh2rlg1aeq8k.apps.googleusercontent.com'
      });
      attachSignin(document.getElementById('customBtn1'));
    });
  };

  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
              var profile = googleUser.getBasicProfile();
          var id = profile.getId(); // Do not send to your backend! Use an ID token instead.
          var name = profile.getName();
          var first_name = profile.getGivenName();
          var last_name = profile.getFamilyName();
          var imageUrl = profile.getImageUrl();
          var email = profile.getEmail(); // This is null if the 'email' scope is not present.
          document.getElementById('name').innerHTML = 'Please Wait...!';
           googleUser.disconnect();
          $.ajax({
            url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
            data : 'email_address='+email+'&fname='+first_name+'&lname='+last_name+'&login_via=google',
            type : 'post',
            success: function(result) {
                if(result == 'success'){
                    //window.location.reload();
                    location.href = "/index.php?route=account/account";
                } else if(result == 'registered'){
                   location.href = "/index.php?route=account/edit/savedetails";
                } else{
                    $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
                }
                
            }
          });
        }, function(error) {
          $('<div class="alert alert-danger">There was a problem with the authorisation or user may have closed the popup.</div>').insertBefore($('div.error'));
        });
  }
  </script>
<!--Facebook Login Script-->
  <script>

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '587428728308321',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v3.0' // use graph api version 3.1
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.async=true;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
  // Facebook login with JavaScript SDK
function xfbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {		
            getFbUserData();
        } else {
        	autherror();
        }
    }, {scope: 'email'});
}

  function testAPI() { 
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function(response) {
      if(response.email == 'undefined')
          $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please check your email id associated with your facebook account.</div>').insertBefore($('div.customAlert'));
      else{
        document.getElementById('name').innerHTML ='Please Wait...!';

        $.ajax({
            url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
            data : 'email_address='+response.email+'&fname='+response.first_name+'&lname='+response.last_name+'&login_via=facebook',
            type : 'post',
            success: function(result) {
                if(result == 'success'){
                    window.location.reload();
                } else if(result == 'registered'){
                    window.location.reload();
                } else{
                    $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
                }
                
            }
        });
      }
    });
  }
</script>

<!--Linkedin Login Script-->
<script type="text/javascript">
  function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);
        
    }
    
    function getProfileData() {
        IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(onSuccess).error(onError);
    }

function onSuccess(data){
var user = data.values[0];
        var first_name = user.firstName;
        var last_name = user.lastName;
        var email = user.emailAddress;
        document.getElementById('name').innerHTML = 'Please Wait...!';
        $.ajax({
            url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/socialLogin',
            data : 'email_address='+email+'&fname='+first_name+'&lname='+last_name+'&login_via=linkedin',
            type : 'post',
            success: function(result) {
                if(result == 'success'){
                    window.location.reload();
                } else if(result == 'registered'){
                    window.location.reload();
                } else{
                    $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.customAlert'));
                }     
            }
        });

    }

    function onError(error) {
        console.log(error);
    }

</script>
<style>
   .login-button-container {
        margin: 10px auto;
        display: table;
        table-layout: fixed;
        width: 100%;
    }
    .login-button-container .social-container {
        min-width: 133px;
    }
    
    .login-button-container .social-container .social-container-box {
        border: 1px solid #bfc0c6;
        background-color: #fff;
        border-radius: 5px;
        text-align: left;
        padding: 14px 14px 0 0;
        height: 50px;
        cursor: pointer;
        max-width: 150px;
        margin: 0 auto;
    }
    .login-button {
        display: block;
        margin: 0 auto;
        width: 100%;
        max-width: 186px;
        padding-left: 50px;
        position: relative;
        font-weight: 500;
    }
    .login-button-container .login-gplus-logo {
        background-position: -298px 0 !important;
        width: 23px;
        top: -2px;
    }
    .login-button-container .login-fb-logo, .login-button-container .login-gplus-logo, .login-button-container .login-linkedin-logo {
        height: 29px;
        position: absolute;
        left: 15px;
    }
    .login-button-container .header-sprite {
        background: url(https://www.true-elements.com/catalog/view/javascript/xtensions/image/social.png) no-repeat 0 0;
            background-position-x: 0px;
            background-position-y: 0px;
            background-size: auto;
        background-size: 336px 48px;
    }
    .group {
        position: relative;
        margin-bottom: 20px;
    }
    .group .inputMaterial {
        font-size: 13px;
        display: block;
        width: 100%;
        height: 42px;
        box-shadow: none;
        -webkit-box-shadow: none;
        color: #333333;
        padding: 16px 15px 4px;
        border-radius: 4px;
        border: 1px solid  #dddddd;
    }
    
    .group .inputMaterial:focus {
      outline: 0;
      -webkit-box-shadow: none;
      box-shadow: none;
      border-color: #96858d;
    }
    
    .group label {
        color: #cecbe2;
        font-size: 12px;
        font-weight: 600;
        position: absolute;
        pointer-events: none;
        left: 5px;
        top: 12px;
        transition: 0.2s ease all;
        -moz-transition: 0.2s ease all;
        -webkit-transition: 0.2s ease all;
        z-index: 22;
        padding: 0px 10px;
    }
    .group.focus label {
      top: -8px !important;
      background: #ffffff;
    }
    .group.focus label span.required:after {
      display: inline-block;
    }
    .group.focus select.inputMaterial {
      padding-top: 18px;
    }
    .group.focus label {
      color: #96858d;
    }
    *:focus,
    a:focus {
      outline: none;
    }
    
    
    .group.focus label {
      top: -8px !important;
      background: #ffffff !important;
    }
    .group.focus label {
      color: #000 !important;
    }
    
    
     .footer-separator {
        margin: 6% 0;
        padding: 10px 0px 5px;
        border-top: 1px solid #ddd;
        width: 100%
     }
     
    .group.filled label {
        top: -8px !important;
        background: #fff;
        color: #96858d;
    }   
    .group.filled label span.required:after {
      display: inline-block;
    }
    .group.filled select.inputMaterial {
      padding-top: 18px;
    }
    select.inputMaterial.group.filled+labelselect::before {
      top: 10px;
    }
    select.inputMaterial.group.filled+labelselect::after {
      top: 12px;
    }
    .group.filled .is_floating_tooltip {
      visibility: visible;
    }
    .group.filledalways label {
      top: -8px !important;
      background: #fff;
    }
    
</style>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="container">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
     
      <div class="col-md-3"></div>
       <div class="col-md-6" style="box-shadow: 0 5px 6px rgba(0,0,0,0.5);border: 1px solid rgb(240, 240, 240);margin: 10px 10px 10px 10px;"><br />
       <div class="error"> </div>
       <p class="text-center" style="font-size: 16px;"><strong>Sign-up to get complimentary Rs.100/- as TrueCash</strong></p>
            
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset id="account">
          
          <div class="group required" style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
            <label class="col-md-2 control-label"><?php echo $entry_customer_group; ?></label>
            <div class="col-md-10">
              <?php foreach ($customer_groups as $customer_group) { ?>
              <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
              <div class="radio">
                <label>
                  <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
                  <?php echo $customer_group['name']; ?></label>
              </div>
              <?php } else { ?>
              <div class="radio">
                <label>
                  <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" />
                  <?php echo $customer_group['name']; ?></label>
              </div>
              <?php } ?>
              <?php } ?>
            </div>
          </div>
          <div class="group required">
            <input type="text" name="firstname" value="<?php echo $firstname; ?>" id="input-firstname" class="inputMaterial" />
              <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php echo $error_firstname; ?></div>
              <?php } ?>
            <label for="input-firstname">Your Name </label>
          </div>
          
          <div class="group required">
            <input type="email" name="email" value="<?php echo $email; ?>" id="input-email" class="inputMaterial" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            <label for="input-email"><?php echo $entry_email; ?></label>
          </div>
          <div class="group required">
            <input type="tel" name="telephone" value="<?php echo $telephone; ?>" id="input-telephone" class="inputMaterial" maxlength="10" />
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php } ?>
            <label for="input-telephone"><?php echo $entry_telephone; ?></label>
          </div>
        <!--  <div class="group">
            <label for="input-fax"><?php echo $entry_fax; ?></label>
            <div class="col-md-10">
              <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="inputMaterial" />
            </div>
          </div> -->
          
        </fieldset>
        
        
        <fieldset>
          
          <div class="group required">
            <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="inputMaterial" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
            <label for="input-password"><?php echo $entry_password; ?></label>
          </div>
          
          <div class="group required">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="inputMaterial" />
              <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
              <label for="input-confirm"><?php echo $entry_confirm; ?></label>
          </div>
          
        </fieldset>
        
        <?php if ($site_key) { ?>
            <div class="form-group" align="center">
                <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                <?php if ($error_captcha) { ?>
                  <div class="text-danger"><?php echo $error_captcha; ?></div>
                <?php } ?>
            </div>
        <?php } ?>
       
        <?php echo $captcha; ?>
     
		    

				<!--// Add from ocmod -->
		    <div id="continue">
		    <!--//  -->
			
        <?php if ($text_agree) { ?>
        <div class="buttons">
          <div class="pull-right"><?php echo $text_agree; ?>
            <?php if ($agree) { ?>
            <input type="checkbox" name="agree" value="1" checked="checked" />
            <?php } else { ?>
            <input type="checkbox" name="agree" value="1" />
            <?php } ?>
            &nbsp;
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
          </div>
        </div>
        <?php } else { ?>
        <div class="buttons">
          <center>
            <input type="submit" value="Sign Up" class="btn btn-primary" />
          </center>
        </div>
        <?php } ?>
          <script>startApp();</script>

<script>
		(function ($) {
			$(function () {
  				$("#loginFB").on("click", function () {
    				FB.login(function(response) {
      					if (response.authResponse) {
        								testAPI();
        				}
    				});
  				});
			});
		})(jQuery);
</script>

<script type="text/javascript">
		(function ($) {
			$(function () {
  				$("#loginLinkedIn").on("click", function () {
    				IN.User.authorize(function(){
       					onLinkedInLoad();
   					});
				});
			});
		})(jQuery);

    function liAuth(){
      			IN.User.authorize(function(){
           			getProfileData();
       			});
        }
</script>

        <div id="name"></div>
         

				<!--// Add from ocmod -->
		  	</div>
		  	<div class="verify"><?php echo $verify; ?></div>
		  	<!--//  -->
			
      </form>
      <p class="login-info-text text-center hide_guest" style="padding-top: 2%;font-size: 16px;">- OR -</p>
      <div class="login-button-container">
                <div class="social-container">
                <div class="social-container-box" id="customBtn1">
                <span class="login-google login-button"><span class="header-sprite login-gplus-logo"></span>GOOGLE</a>
                </div>
                </div>
            </div>
       <div class="footer-separator">
               <span class="pull-left" style="text-decoration: none;color:#000000;font-weight: 900;">
                    Already have an account, please <a href="https://www.trueelements.co.in/index.php?route=account/login">login </a>.                </span>
        </div>  
        
        
        <p class="login-info-text text-center hide_guest" style="font-size: 16px;"> FOOD THAT DOES NOT LIE TO YOU </p>
          <p class="login-info-text text-center hide_guest" style="font-size: 11px;"> 100% Wholegrains. 0% Chemicals. 0% Added Sugar </span>
          <button onclick="window.location.href = '/our_products';" style="border: 0;margin-top: 10px;font-weight: 600;font-size: 16px;padding: 7px 16px;text-transform: none;color:#000;">See our exciting range of breakfast and snack range</button>
      <?php echo $content_bottom; ?></div>
      <div class="col-md-3"></div>
      
        

      </div>
       
       
       
    <?php echo $column_right; ?>
    
    
    
    </div>
</div>
<script type="text/javascript"><!--
// Sort the custom fields
$('#account .group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .group').length) {
		$('#account .group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .group').length) {
		$('#account .group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#account .group').length) {
		$('#account .group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .group').length) {
		$('#account .group:first').before(this);
	}
});

$('#address .group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .group').length) {
		$('#address .group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .group').length) {
		$('#address .group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#address .group').length) {
		$('#address .group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .group').length) {
		$('#address .group:first').before(this);
	}
});

$('input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('.custom-field').hide();
			$('.custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#custom-field' + custom_field['custom_field_id']).addClass('required');
				}
			}


		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>

<script>
   $('.inputMaterial').focusout(function() {
    $('.group').removeClass('focus');    
    if(false && $(this).val().length < 1 && $(this).closest('.group').children().has('.xerror')){
    	$(this).closest('.group').addClass('has-error');
    	$(this).closest('.group').children('.xerror').removeClass('hidden');
    }    
});
$('.inputMaterial').focus(function() {
    $(this).closest('.group').addClass('focus');
    if(false){
    	$(this).closest('.group').removeClass('has-error');
    	$(this).closest('.group').children('.xerror').addClass('hidden');
    }
});
$('input.inputMaterial, textarea.inputMaterial').bind("change",function(){
	if($(this).val().length > 0){
        $(this).closest('.group').addClass('filled');
    }
    else{
        $(this).closest('.group').removeClass('filled');
    }
});
$('input.inputMaterial, textarea.inputMaterial').bind("change keyup",function(){
	if($(this).val().length > 0){
        $(this).closest('.group').addClass('filled');
    }
    else{
        $(this).closest('.group').removeClass('filled');
    }
});
$('select.inputMaterial').bind("change keyup",function(){
	
   $(this).closest('.group').addClass('filled');
    
});
/// Input Kepress Filled  Focus
$('input.inputMaterial, textarea.inputMaterial').keyup(function() {
    if($(this).val().length > 0){
        $(this).closest('.group').addClass('filled');
    }

    else{
        $(this).closest('.group').removeClass('filled');
    }
});

/// Input Check Filled Focus
var $formControl = $('input.inputMaterial, textarea.inputMaterial');
var values = {};
var validate =    $formControl.each(function() {
    if($(this).val() != null && $(this).val().length > 0){
        $(this).closest('.group').addClass('filled');
    }
    else{
        $(this).closest('.group').removeClass('filled');
    }
});
</script>
<?php echo $footer; ?>
