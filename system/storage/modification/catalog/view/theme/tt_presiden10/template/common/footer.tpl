<p class="footer-breaking"></p><?php if(isset($block4)){echo $block4; } ?>
<style>
    .footer-content-mobile {
        float: left;
        list-style: none;
        margin-right: 10px;
    }
    
    .footer-content-mobile li a {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        color: #c8c8c8;
        border: 1px solid #4d4d4d;
        font-size: 0;
        line-height: 38px;
        padding: 0;
        width: 40px;
        height: 40px;
        text-align: center;
        letter-spacing: 0;
        border: none !important
    }
    
    .footer-content-mobile ul {
        margin: 20px 0 0;
    }
    
    .footer-content-mobile li {
        display: inline-block;
        margin-right: 10px;
    }
    
    .footer-content-mobile .social-title {
        margin: 20px 0 0!important;
    }
    
    .social-title h2 {
        color: #fff!important;
    }

@media(max-width: 850px) and (min-width: 700px){
    .footer-static .col-sm-12{
     width: 100% !important;
    }
    }
</style>
<footer>
	<div class="footer-static">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-8 col-xs-12">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-5">
							<div class="col col1">
								<div class="footer-title">
									<h3><?php echo $text_logo_footer; ?></h3>
								</div>
								<div class="footer-content">
									<div class="logo-footer">
										<!--a href="#"><img src="image/catalog/True-Elements-logo-white.png" alt=""></a-->
									</div>
									<p class="desc-footer" style="color:#FFFFFF;">What you see above is part of our world that is Made of Truth – Not a world full of lies, jargon or False Promises. Where what is written on the pack is what's in it, and vice versa.<br/>Food ranging from ‘Nashta’ to ‘Tiffin’, from Breakfast to Snacks.</p>
									<!--div class="address"><label><?php echo $text_address; ?></label><?php echo $address; ?> </div-->
    								<div class="hidden-sm hidden-xs">
    								    <div class="email" style="text-transform:none"><label><i class="fa fa-envelope"></i>&nbsp;&nbsp;</label><?php echo $email; ?></div>
    									<div class="telephone"><label><i class="fa fa-phone-square"></i>&nbsp;&nbsp;</label><?php echo $telephone; ?></div>
    									<?php if($whatsapp_number) { ?>
    									    <div>For whatsapp Order: <?php echo $whatsapp_number; ?></div>
    									<?php } ?>
    								</div>
    								<div class="hidden-lg hidden-md">
    								    <div class="email" style="text-transform:none"><label><i class="fa fa-envelope"></i>&nbsp;&nbsp;</label><a style="color:#fff;" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
    									<div class="telephone"><label><i class="fa fa-phone-square"></i>&nbsp;&nbsp;</label><a style="color:#fff;" href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></div>
    									<?php if($whatsapp_number) { ?>
        									<div>For whatsapp Order: <a style="color:#fff;" href="whatsapp://send?phone=<?php echo $whatsapp_number; ?>&text=Hi"><?php echo $whatsapp_number; ?></a></div>
        								<?php } ?>
    								</div>
    								<a class="hidden-xs hidden-sm" href="https://play.google.com/store/apps/details?id=com.trueelements.india&hl=en&gl=US" target="_blank" class="store">
    								    <img class="googleplay-original" alt="googleplay-logo" src="/image/data/googleplay1.png" width="171" height="50">
								    </a>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
							<div class="col col2">
								<div class="footer-title">
									<h3>Top Categories</h3>
								</div>
								<div class="footer-content">
									<ul class="list-unstyled">
									    <li style="list-style: none !important;"><a href="/wholegrain-muesli-breakfast">Muesli</a></li>
									    <li style="list-style: none !important;"><a href="/seeds-mixes">Seeds Mixes</a></li>
										<li style="list-style: none !important;"><a href="/wholegrain-oats">Oats</a></li>
										<li style="list-style: none !important;"><a href="/raw-seeds">Healthy Seeds</a></li>
										<li style="list-style: none !important;"><a href="/roasted-seeds">Roasted Seeds</a></li>
										<li style="list-style: none !important;"><a href="/oatmeal-with-real-fruits-breakfast">Oatmeal</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3">
							<div class="col col3">
								<div class="footer-title"><h3>Information</h3></div>
								<div class="footer-content">
									<ul class="list-unstyled">
									    <li style="list-style: none !important;"><a href="/story-of-true">Our Story</a></li>
									    <li style="list-style: none !important;"><a href="/recycle">Let's Recycle</a></li>
										<li style="list-style: none !important;"><a href="/contact-us">Contact Us</a></li>
										<li style="list-style: none !important;"><a href="/certification">Certifications</a></li>
										<li style="list-style: none !important;"><a href="/true-elements-media">Media</a></li>
										<!--li style="list-style: none !important;"><a href="/our-partners">Our Partners</a></li>
										<li style="list-style: none !important;"><a href="/return-refund-policy/">Return Policy</a></li>
										<li style="list-style: none !important;"><a href="/privacy-policy/">Privacy Policy</a></li-->
										<li style="list-style: none !important;"><a href="/our-policies/">Our Policies</a></li>
										<li style="list-style: none !important;"><a href="/terms-conditions/">Terms & Conditions</a></li>
									    <li style="list-style: none !important;"><a href="/careers/">Careers</a></li>
									    <li style="list-style: none !important;"><a class="hidden-xs hidden-sm" href="/index.php?route=information/shipway_track">Track Order</a></li>
									    <li style="list-style: none !important;"><a href="/partner-with-us">Partner with Us</a></li>
									    <li style="list-style: none !important;"><a href="/our-partners">Partner Offers</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 hidden-xs hidden-sm">
					<div class="col col4" style="margin-top: -7px;">
						<div class="footer-title"><h3><?php echo $text_newsletter; ?></h3></div>
						<div class="footer-content">
							<?php if(isset($block5)){ ?>
								<?php echo $block5; ?>
							<?php } ?>
							<?php if(isset($block6)){ ?>
								<?php echo $block6; ?>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 hidden-md hidden-lg hidden-xl">
					<div>
					    <div class="footer-title" style="min-height: 50px;">
					        <a href="/index.php?route=information/shipway_track">
					            <h3>Track Order</h3>
					        </a>
					    </div>
						<div class="footer-title"><h3><?php echo $text_newsletter; ?></h3></div>
						<div class="footer-content-mobile">
							<?php if(isset($block5)){ ?>
								<?php echo $block5; ?>
							<?php } ?>
							<?php if(isset($block6)){ ?>
								<?php echo $block6; ?>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 col-sm-8 col-lg-4">
				    <div class="footer-pincheck">
                        <form method = "POST" id = "footer-pincheck1" <?php if(isset($_SESSION['pincode'])){ echo "style='display:none;'";}?>>
                        
                            <span style="font-size: 15px; color: #fff; font-weight: bold; padding: 0 0 20px;">Check time to deliver to your pincode</span>&nbsp;&nbsp;&nbsp;<input placeholder="Enter Delivery Pincode" type="text" id="footer-pin1" style="padding: 10px; background: transparent; border: 2px solid #fff; color: #fff; width: 50%; margin-top: 20px; font-size: 13px;">
                            <input type = "button"  onClick = "getdata2()" value="Check" id="footer-button-check" class="button" style="background: #f49a25!important; border-radius: 5px !important;padding: 12px 17px 12px 17px; margin-top: 20px;"/>
                            <div id="footer-show_message" style="display:none; color: red;">Enter Delivery Pincode</div>
                        </form>
                        <div id="footer-msg1">
                            <?php if(isset($_SESSION['pincode'])) {
                                echo "<div id='footer-msg' ><span><i class='fa fa-map-marker' style='color: #fff!important;'></i> Pincode : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display:inline;'><input type = 'button'  onclick = 'showform2()' value='Change' id='footer-button-change' class='button'/></form></span><br/>";
                                if(isset($_SESSION['pin_check_delivery']) &&        $_SESSION['pin_check_delivery'] != '') {
                                     echo $_SESSION['pin_check_delivery'];
                                }
                                echo $_SESSION['pin_check_result'];
                                echo"</font></font></div>";
                            } ?>
                        </div>
                    </div>
                    <a class="hidden-md hidden-xl hidden-lg" href="https://play.google.com/store/apps/details?id=com.trueelements.india&hl=en&gl=US" target="_blank" class="store">
    				    <img class="googleplay-original" alt="googleplay-logo" src="/image/data/googleplay1.png" width="171" height="50">
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="container-inner">
				
				<p> © <?php echo date('Y'); ?> HW Wellness Solutions Pvt Ltd. All Rights Reserved. </p>
				<?php if(isset($block7)){ ?>
					<?php echo $block7; ?>
				<?php } ?>
			</div>
		</div>
	</div>
  
  <div id="back-top"></div>
  
  <!--- POSTCODE --->     
  <style>
    .footer-pincheck #footer-msg1 p{margin:0!important;}
    .footer-pincheck #footer-msg1{color:#fff!important;}
    .footer-pincheck #footer-msg1 .fa-2x{font-size:2em!important;}
    .footer-pincheck .msg11 {
    font-size: 15px!important;
    color: #fff!important;
}
.footer-pincheck .msg12 {
    font-size: 15px!important;
    margin-top: 4px!important;
    color: #fff!important;
}
.footer-pincheck {
    font-size: 15px;
    margin-top: 15px;
}

.footer-pincheck #footer-button-change {
    margin-top: 10px!important;
    margin-bottom: 10px!important;
    border-radius: 5px!important;
}

.footer-pincheck b, .footer-pincheck .fa {
    color: #fff!important;
}
#footer-button-change:hover {
    background: #f49a25!important;
}
  </style>
  <script type="text/javascript">
   $('#pincheck1').submit(function (e) {
     e.preventDefault();
     getdata1();
   });
    $('.footer-pincheck #footer-pincheck1').submit(function (e) {
     e.preventDefault();
     getdata2();
   });
   
    function getdata1(){
      var pin = $("#pin1").val();
      if(pin != ''){
        $.ajax({
          type: "POST",
          url: "?route=product/product/pinCheck",
          data: {pincode: pin},
          dataType : "text"
        }).done(function( result )
        {
          $("#msg1").show();
          $("#msg1").html( result );
          $("#pincheck1").hide();
          $("#show_message").hide();
          
        });
      }
      else{
        $("#show_message").show();
      }
    }
    function getdata2(){
      var pin = $(".footer-pincheck #footer-pin1").val();
      if(pin != ''){
        $.ajax({
          type: "POST",
          url: "?route=common/footer/pinCheck",
          data: {pincode: pin},
          dataType : "text"
        }).done(function( result )
        {
          $(".footer-pincheck #footer-msg1").show();
          $(".footer-pincheck #footer-msg1").html( result );
          $(".footer-pincheck #footer-pincheck1").hide();
          $(".footer-pincheck #footer-show_message").hide();
          
        });
      }
      else{
        $(".footer-pincheck #footer-show_message").show();
      }
    }
    
    function showform1(){
      $("#msg1").hide();
      $("#pincheck1").show();
    }
    function showform2(){
      $(".footer-pincheck #footer-msg1").hide();
      $(".footer-pincheck #footer-pincheck1").show();
    }
    
    $('#pincheck-bookmark, #617 a').click(function (e) {
        e.preventDefault();
        if($('#navbar-inner').hasClass('navbar-active')) {
            $('span.close.navbar-toggle').click();
        }
        $("html, body").animate({
            scrollTop: $(".footer-pincheck").offset().top-20
            }, 1000);
    });
  </script>

<script type="text/javascript">
    (function () {
        window.SSG_CONFIG = {
            CART_URL: 'https://www.true-elements.com/index.php?route=checkout/checkout',
            CHECKOUT_URL: 'https://www.true-elements.com/index.php?route=checkout/success',
            INCLUDE_QUERY_PARAMS: true,
            SHOW_DISCOUNT_CODE_STRIP: false,
            COUPON_INPUT_SELECTOR: '#coupon-panel > div > input',
            PURCHASE_VALUE_SELECTOR: '#popup_totals_total > td.text-right',
            SUBMIT_BTN_SELECTOR: '#button-payment',
            CODE_COOKIE_NAME: '',
        };
        var script = document.createElement('script');
        script.src = 'https://cdn.stepsetgo.com/js/brand.t.js';
        document.body.appendChild(script);
    })();
</script>


<script type="text/javascript">
	$(document).ready(function(){
		// hide #back-top first
		$("#back-top").hide();
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 300) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});
			// scroll body to 0px on click
			$('#back-top').click(function () {
				$('body,html').animate({scrollTop: 0}, 800);
				return false;
			});
		});
		if (jQuery(window).width() < 992) {   
			jQuery('.footer-static .footer-title').click(function(){     
				jQuery(this).parent('.col').toggleClass('active').siblings().removeClass('active'); 
				jQuery(this).next('.col .footer-content').toggle(300);
				jQuery(this).parent('.col.active').siblings().children('.col .footer-content').slideUp(300); 
			}); 
			 
		}
		
	    var body_class = $('body').attr('class'); 
	    var category_id = body_class.replace(/\D/g,'');
	    
    	if(category_id > 0) {
    	    $('#pt_menu' + category_id).addClass('act');
    	}
    	if(body_class == 'recipe-recipe' || body_class == 'recipe-recipe-category' || body_class == 'recipe-recipe-info') {
    	    $('#pt_menu537').addClass('act');
    	}
    	if(body_class == 'product-category-494') {
    	    $('#pt_menu613').addClass('act');
    	}
    	if(body_class == 'product-gift_hampers') {
    	    $('#pt_menu616').addClass('act');
    	}
    	if(body_class == 'product-our_products') {
    	    $('#pt_menu533').addClass('act');
    	}
    	if(body_class == 'mpblog-blogs' || body_class == 'mpblog-blogcategory' || body_class == 'mpblog-blog') {
    	    $('#pt_menu551').addClass('act');
    	}
	});
</script>
 
<!--Start of Connecto Script-->
<script type="text/javascript">
var _TConnecto = _TConnecto || {};
_TConnecto.licenseKey = 'BNOA2WTMZG4VELM4';

(function() {
var con = document.createElement('script'); con.type = 'text/javascript';
var host = (document.location.protocol === 'http:') ? 'http://cdn' : 'https://server';
con.src = host + '.connecto.io/javascripts/connect.prod.min.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(con, s);
})();
</script>
<!--End of Connecto Script-->

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/15d2ed4c38fa70e92de132155/3199368802bc1a3a4d2c4ffe6.js");</script>


<!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset opt_in_tracking opt_out_tracking has_opted_in_tracking has_opted_out_tracking clear_opt_in_out_tracking people.set people.set_once people.unset people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn4.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn4.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn4.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("70e72d15d29418a0d596030504766425");</script><!-- end Mixpanel -->

<script type="text/javascript">
        window._pt_lt = new Date().getTime();
        window._pt_sp_2 = [];
        _pt_sp_2.push('setAccount,62084c67');
        var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
        (function() {
            var atag = document.createElement('script'); atag.type = 'text/javascript'; atag.async = true;
            atag.src = _protocol + 'cjs.ptengine.com/pta_en.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(atag, s);
        })();
</script>
            
<!--script>
(function(h,e,a,t,m,p) {
m=e.createElement(a);m.async=!0;m.src=t;
p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
})(window,document,'script','https://u.heatmap.it/log.js');
</script-->



<!-- Hotjar Tracking Code for https://www.true-elements.com/ -->
<script> (function(h,o,t,j,a,r){ h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:1446735,hjsv:6}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');</script>
<script type="text/javascript">
    $(".tc-slider").owlCarousel({
	    autoPlay: true,
	    items : 1,
	    slideSpeed : 1000,
	    navigation : true,
	    paginationNumbers : true,
	    pagination : false,
	    stopOnHover : false,
	    itemsDesktop : [1199,1],
	    itemsDesktopSmall : [991,1],
	    itemsTablet: [700,1],
	    itemsMobile : [480,1]
    });
</script>
<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/f2a297baccac31d4574488d02/ff913f2c0134cb1ea7bb665df.js");</script>


        <?php if ($facebook_messenger_enabled_FAE == 'true') { ?>
        <!-- Facebook JSSDK -->
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId            : '',
              autoLogAppEvents : true,
              xfbml            : true,
              version          : '<?= $facebook_jssdk_version_FAE ?>'
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "https://connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
        <div
          class="fb-customerchat"
          attribution="fbe_opencart"
          page_id="<?= $facebook_page_id_FAE ?>"
        />
        <?php } ?>
      
</footer>

        <?php if (isset($smca_status) || isset($smac_status)) { ?>
          <!-- start: OCdevWizard Setting -->
          <script type="text/javascript">     
            var ocdev_modules = [];
                  
            <?php if (isset($smca_status) && $smca_status == 1) { ?>
              ocdev_modules.push({
                src:  'index.php?route=ocdevwizard/smart_cart',
                type:'ajax'
              });
            <?php } ?>
            <?php if (isset($smac_status) && $smac_status == 1 && $smart_abandoned_cart == 1) { ?>
              ocdev_modules.push({
                src:  'index.php?route=ocdevwizard/smart_abandoned_cart',
                type:'ajax'
              });
            <?php } ?>
          </script>
          <!-- end: OCdevWizard Setting -->
        <?php } ?>
      



            	<script type="text/javascript">
                  var ref = (typeof document.referrer != 'undefined') ? document.referrer : "";
                  $.ajax({
                    type: "post",	
                    data: {"ref" : ref},
                    url:"index.php?route=extension/module/ianalytics/register_iAnalyticsVisits",
                  });
                </script>
			

	
			<?php if ($config_whatsappid) { ?>
		   		
			<div class="te-sj">
			<a class="te-sj whatsapp" href="whatsapp://send?phone=<?php echo $config_whatsappid; ?>&text=Hi" />
<img src="image/catalog/xtensions/whatsapp.png" alt="Whatsapp Us" style="width: 45px;border-top-left-radius: 4px;border-top-right-radius: 4px;"></a></div>


			<?php } ?>
			
			
		
		
			
</body></html>