<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel='stylesheet' href='https://dgs.straightarrowdev.com/app/utilities/css/'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel='stylesheet' href='https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css'>
<link rel='stylesheet' href='https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.min.css" integrity="sha512-nIm/JGUwrzblLex/meoxJSPdAKQOe2bLhnrZ81g5Jbh519z8GFJIWu87WAhBH+RAyGbM4+U3S2h+kL5JoV6/wA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!--graph css-->
<style type="text/css">
 @media screen and (min-width: 300px) and (max-width: 800px){

 /* .type-image img{
  height: 350px !important;
  margin: auto;
 }*/
  #chart .bar{
  width: 43px !important;
 }
 #chart li{
  padding-right: 3.5em !important;
 }
 .chart-span2{
    font-size: 12px !important;
    margin-left: 15px;
 }
.chart-span{
   font-size: 11px !important;
   margin-left: 15px;
 }
}
 @media screen and (min-width: 481px) and (max-width: 800px){
  .forshadowbox-mob{
    min-height: 169px !important;
    margin-left: 5% !important;
    margin-bottom: 10rem !important;
  }
  .forshadowboximg, .forshadowboximg2, .forshadowboximg3 {
    padding: 3% 26% 0 26% !important;
    margin-top: -4.7rem !important;
 }

}
#bars .bar{
  background-color:#f49a25;
}
#chart .chart-span{
  position: absolute;
  bottom: -3.5em;
}
.chart-span2{
  font-weight: 700;
}
#chart {
  height: 200px;
  margin: 8% auto 0;;
  display: block;
 } 
  #chart #numbers {
    width: 50px; 
    height: 100%;
    margin: 0;
    padding: 0;
    display: inline-block;
    float: left;
  }  
#chart li {
  text-align: right;
  padding-right: 1em;
  list-style: none;
  height: 29px;
  position: relative;
  bottom: 30px;
  
  &:last-child {
    height: 30px;
  }
  #chart span {
    color: #000;
    position: absolute;
    bottom: 0;
    right: 10px;
  }
}

#bars {
display: flex;
height: 200px;
padding: 0;
margin: 0;
justify-content: center;
}
#chart li {
  display: table-cell;
  width: 87px;
  height: 152px;
  margin: 0;
  text-align: center;
  position: relative;
}   
#chart .bar {
  display: block;
  width: 70px;
  margin-left: 15px;
  position: absolute;
  bottom: 0;
  border-top-right-radius:5px;
  border-top-left-radius:5px;
  
  &:hover {
    background: #5AE;
    cursor: pointer;
    
    &:before {
      color: white;
      content: attr(data-percentage) '%';
      position:relative;
      bottom: 20px;
    }
  }
}

#chart span {
  color: #000;
  width: 100%;
  position: absolute;
  bottom: -2em;
  left: 0;
  text-align: center;
}
}
}
}
</style>
<style type="text/css">

@media screen and (min-width: 900px){
  .slick-list, .slider-wrapper{
    overflow: unset !important;
  }
  
  #content{
    overflow: hidden;
  }

  #gallery_01 .slick-list{
    overflow: hidden !important;
  }

.product-details, .product-details .radio, .product-details .col-lg-12,
.product-details .col-md-12, .product-details .col-sm-12
.product-details .col-lg-6, .product-details .col-md-6,
.product-details .col-sm-6, .product-details .col-lg-4,
.product-details .col-md-4, .product-details .col-sm-4,
.product-details .col-md-9, .product-details .col-xs-1,
.product-details .col-xs-11, .product-details .col-lg-3,
.product-details .col-lg-3, .product-details .col-lg-1,
.product-details .col-lg-8
{
  position: initial !important;
}

.slick-track::before, .slick-track::after{
  display: inline-table;
}

}
</style>

<style>

* {box-sizing: border-box;}

.img-zoom-container {
  position: relative;
}

#myresult{
  background: #fff;
}
.img-zoom-lens {
  position: relative;
  border: 1px solid #d4d4d4;
  /*set the size of the lens:*/
  width: 40px;
  height: 40px;
}

.img-zoom-result {
border: 5px solid #f49a45;
width: 500px;
height: 300px;
float: right;
margin-left: 15px;
position: absolute;
z-index: 999;
left: 500;
}
</style>


<?php echo $header; ?>
<ul class="breadcrumb" style="background-image: none;">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  <?php } ?>
</ul>

<?php if(strlen($svd) > 5) { ?>
    <div class="col-md-12 col-xs-12 col-sm-12 feature-banner-container">
        <div class="col-md-offset-4 col-md-4 feature-banner">
            <h4><?php echo $svd; ?></h4>
        </div>
    </div>
<?php } ?>

<div class="container">
  <div class=""><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> p0">
			<?php echo $content_top; ?>
			 <?php if ($hb_snippets_bc_enable == '1'){ ?>
			<?php 
				$count_breadcrumb = 0; 
				$bc = '';
				foreach ($breadcrumbs as $breadcrumb) { 
				$count_breadcrumb = $count_breadcrumb + 1; 
				$bc .= '{
    "@type": "ListItem",
    "position": '.$count_breadcrumb.',
    "item": {
      "@id": "'.$breadcrumb['href'].'",
      "name": "'.$breadcrumb['text'].'"
    }},';
	}
	$bc = str_replace('<i class="fa fa-home"></i>','Home',$bc);
	$bc = rtrim($bc,',');
	?>
			<script type="application/ld+json">
			{
		  "@context": "http://schema.org",
		  "@type": "BreadcrumbList",
		  "itemListElement": [<?php echo $bc; ?>]}
		  </script>
		  <?php } ?>
		  
		  <?php if ($hb_snippets_prod_enable == '1'){ ?>
		  <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "name": "<?php echo $heading_title; ?>",
  <?php if ($thumb) { ?>
	"image": "<?php echo $thumb; ?>"
  <?php } ?>
  ,"description": "<?php $desc = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "", htmlentities(strip_tags($description))); echo preg_replace('/\s{2,}/', ' ', trim($desc));?>"
  <?php if ($manufacturer) { ?>
  	,"brand": {
    "@type": "Thing",
    "name": "<?php echo $manufacturer; ?>"
  }
  <?php } ?>
  <?php if (($review_status) and ($rating)) { ?>
,"aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "<?php echo $rating; ?>",
    "reviewCount": "<?php echo $review_count; ?>"
  }
 <?php } ?> 
 <?php if ($price) { ?>
   ,"offers": {
    "@type": "Offer",
    "priceCurrency": "<?php echo $currencycode; ?>",
	<?php if (!$special) { 
	if ($language_decimal_point == ','){
		$hbprice = str_replace('.','',$price);
		$hbprice = str_replace(',','.',$hbprice);
		$hbprice = preg_replace("/[^0-9.]/", "", $hbprice);
		$hbprice = ltrim($hbprice,'.');
	}else{
		$hbprice = $price;
		$hbprice = preg_replace("/[^0-9.]/", "", $hbprice);
		$hbprice = ltrim($hbprice,'.');
	}
	?>
		"price": "<?php echo $hbprice; ?>"
	<?php } else { 
		if ($language_decimal_point == ','){
		$hbspecial = str_replace('.','',$special);
		$hbspecial = str_replace(',','.',$hbspecial);
		$hbspecial =  preg_replace("/[^0-9.]/", "", $hbspecial);
		$hbspecial = ltrim($hbspecial,'.');
		}else{
		$hbspecial = $special;
		$hbspecial =  preg_replace("/[^0-9.]/", "", $hbspecial);
		$hbspecial = ltrim($hbspecial,'.');
		}
	?>
		"price": "<?php echo $hbspecial; ?>"
	<?php } ?>
	<?php if ($stockqty > 0) { ?>
    ,"availability": "http://schema.org/InStock"
	<?php } ?>
  }
 <?php } ?>
}
</script>
<?php } ?>
			
      <div class="col-xs-12 hidden" style="border-bottom: 1px solid #eee;">
          <h1 class="product-name" style="text-align: left; font-size: 20px; margin: 0 0 4px; color: #000000; font-weight: 700 !important; text-transform: none; padding-bottom: 10px; min-height: 30px !important; margin-top: 10px;"><?php echo $heading_title; ?></h1>
          <?php if ($review_status) { ?>
        <!--  <a onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">Review </a> -->
        <!-- <div class="ratings" itemprop="reviewRating" style="margin: 0 !important;">
          <div class="rating-box" style="min-height: 20px;">
              <?php for ($i = 0; $i <= 5; $i++) { ?>
                <?php if ($rating == $i) {
                    $class_r= "rating".$i;
                    if($rating > 0) {
                        echo '<div class="'.$class_r.' rating-img col-md-2 col-xs-3 col-sm-2">rating</div><span class="rating-text col-md-10 col-xs-9 col-sm-3">' . $rating_decimal . ' / 5 by ' . $total_reviews . ' Customers</span>';
                    } else {
                        echo '<div class="'.$class_r.' rating-img">rating</div>';
                    }
                } 
              }  ?>
      </div>
    </div> -->
      <?php if ($review_status) { ?>
        <div class="col-lg-12 p0 product-rating">
            <?php for ($i = 0; $i <= 5; $i++) { ?>
                <?php if ($rating == $i) {
                    $class_r= "rating".$i;
                    if($rating > 0) {
                      echo '<span class="fa fa-star checked"></span>
                      <span><strong>' . $rating_decimal .  ' </strong> ( By ' . $total_reviews . ' Customers ) </span>';
                   } else {
                      echo '<div class="'.$class_r.' rating-img">rating</div>';
                    }
                  } 
                }  ?>
        </div>

      <?php } ?>
  </div>

   <!--  <hr class="hidden-xl hidden-lg" style="margin: 8px 0 15px 0 !important;"> -->
    
    <?php } ?>
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } ?>
        
    

  <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 p0">
   <section class="module-gallery module-12345" id="position-fixed">
  <div class="maxWidth900 padLR15">
    <div class="padTB20">

      <div class="slider-wrapper">
        <?php if ($thumb || $images) { ?>
         <ul id="gallery_01" class="slider-thumb noPad noMar hidden-xs">
          <?php if ($thumb) { ?>
          <li class="type-image">
          <img class="img-responsive" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" data-zoom-image="<?php echo $thumb; ?>">
          </li>
          <?php } ?>

          <?php if ($images) { ?>
          <?php foreach ($images as $image) { ?>
          <li class="type-image">
          <img class="img-responsive" src="<?php echo $image['thumb']; ?>" alt="" data-zoom-image="<?php echo $image['thumb']; ?>">
          </li>
          <?php } ?>
          <?php } ?>
           <!--  <li class="type-image"><img class="<?php echo $image['thumb']; ?>" src="https://harippa.co.in/demo/image/catalog/1-new-branding/front-strip-images/muesli/fruit-nut-muesli-video-image.jpg" alt=""></li> -->
        </ul>
        <ul class="slider-preview noPad noMar thumbnails">
          <?php if ($thumb) { ?>
          <li class="type-image">
          <img class="img-responsive" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
          </li>
          <?php } ?>

          <?php if ($images) { ?>
          <?php foreach ($images as $image) { ?>
          <li class="type-image"><img class="img-responsive" src="<?php echo $image['thumb']; ?>" 
            alt=""></li>
           <?php } ?>
          <?php } ?>
          <!-- <li class="type-image embed-responsive embed-responsive-16by9"  style="padding-bottom: 14% !important"><iframe class="responsive-iframe" src="https://www.youtube.com/embed/e7b7imfU1sE">
          </iframe></li> -->
        </ul>
        <?php } ?>
      </div>

    </div>
  </div>
</section>
</div>

 <!-- <style type="text/css">
  @media screen and (max-width: 500px) and (min-width: 300px){
   .image-additional-container .owl-wrapper .thumbnail img{
    width: 63px !important;
    border: 1px solid #f49a25;
    padding: 5px;
    opacity: 0.5;
   }

   .image-additional-container .owl-wrapper .owl-item.active2 .thumbnail img{
    opacity: 1;
   }
 }
 </style> -->



 <!--div class="col-xs-12 hidden-lg hidden-xl" style="z-index: 99999;">

<div id="myCarousel" class="carousel carousel-fade" data-ride="carousel">
  
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img class="hidden-xl hidden-lg hidden-sm hidden-md" src="https://www.harippa.co.in/demo/image/catalog/Banner/mobile/demo4.jpg" alt="image">
       <img class="hidden-xs" src="https://harippa.co.in/demo/image/catalog/Banner/desktop/chocolate-granola-home-slider-desktop.jpg" alt="image">
    </div>

    <div class="item">
      <img class="hidden-xl hidden-lg hidden-sm hidden-md" src="https://www.harippa.co.in/demo/image/catalog/Banner/mobile/demo4.jpg" alt="image">
       <img class="hidden-xs" src="https://harippa.co.in/demo/image/catalog/Banner/desktop/daily-dose-home-slider-desktop.jpg" alt="image">
    </div>
    <div class="item">
       <img class="hidden-xl hidden-lg hidden-sm hidden-md" src="https://www.harippa.co.in/demo/image/catalog/Banner/mobile/demo4.jpg" alt="image">
       <img class="hidden-xs" src="https://harippa.co.in/demo/image/catalog/Banner/desktop/mmm-home-slider-desktop.jpg" alt="image">
    </div>
  </div>

  
  <a class="left carousel-control hidden-xs" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control hidden-xs" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div-->
       <!-- <div class="col-xs-12 hidden-xl hidden-lg p0" style="margin-bottom: 20px;">
          <?php if ($thumb || $images) { ?>
        <div class="col-xs-12 p0">
          <div class="thumbnails">
            <?php if ($thumb && $product_id != 6463) { ?>
            <a class="thumbnail" title="<?php echo $heading_title; ?>">
             <img class="pro_img" src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
           </a>
           <?php } ?>
         </div>
        </div>

        <div class="col-xs-12 p0">
         <div class="image-additional-container owl-style2">
        <?php if($product_id != 6463) { ?>
            <div class="image-additional" id="gallery_01">
              <?php if ($thumb) { ?>
              <a class="thumbnail" style="display: none" href="#" data-image="<?php echo $thumb; ?>" data-zoom-image="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
              <?php } ?>
              <?php if ($images) { ?>
              <?php foreach ($images as $image) { ?>
              <a style="display: none" class="thumbnail" href="#" data-image="<?php echo $image['thumb']; ?>" data-zoom-image="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
              <?php } ?>
              <?php } ?>
            </div>
            <?php } else { ?>
           <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
           <?php } ?>
        </div>
        <?php } ?>
      </div>
    </div>
 -->
      <?php if ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } ?>
      <div class="product-details col-2 col-lg-6 col-md-12 col-sm-12 col-xs-12 p0" style="display: inline-block;">
        <?php if($old_order_date) { ?>
            <div class="col-md-12 old-order-date">
                <h4>You Purchased this on <?php echo date('jS M Y', strtotime($old_order_date)); ?></h4>
        </div>`
        <?php } ?>
        <h1 class="product-name"><?php echo $heading_title; ?></h1> 

       <?php if ($review_status) { ?>
        <div class="col-lg-12 p0 product-rating">
            <?php for ($i = 0; $i <= 5; $i++) { ?>
                <?php if ($rating == $i) {
                    $class_r= "rating".$i;
                    if($rating > 0) {
                      echo '<span class="fa fa-star checked"></span>
                      <span><strong>' . $rating_decimal .  ' </strong> ( By ' . $total_reviews . ' Customers ) </span>';
                   } else {
                      echo '<div class="'.$class_r.' rating-img">rating</div>';
                    }
                  } 
                }  ?>
        </div>

      <?php } ?>
  
       <!--  
        <?php if ($review_status) { ?>
        <div class="ratings hidden-xs hidden-sm" itemprop="reviewRating">
          <div class="rating-box" style="min-height: 20px;">
              <?php for ($i = 0; $i <= 5; $i++) { ?>
                <?php if ($rating == $i) {
                    $class_r= "rating".$i;
                    if($rating > 0) {
                        echo '<div class="'.$class_r.' rating-img col-md-2 col-xs-3 col-sm-3">rating</div><span class="rating-text col-md-10 col-xs-9 col-sm-9">' . $rating_decimal . ' / 5 by ' . $total_reviews . ' Customers.</span>';
                    } else {
                        echo '<div class="'.$class_r.' rating-img">rating</div>';
                    }
                } 
              }  ?>
        
      </div>
    </div>
   
    
    <?php } ?> -->


       <?php if ($salescombopgeoffers) {  foreach($salescombopgeoffers as $offer) { echo html_entity_decode($offer['html']); } } ?>
        
    <!-- <?php if ($price) { ?>
    <ul class="list-unstyled price-product">
      <?php if (!$special) { ?>
      <li>
        Price: <span>
            <small class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></small>
            </span>
      </li>
      <?php } else { ?>
      <li>Price: <span class="new-price">
            <span class="<?php echo $live_options['live_options_special_container']; ?>"><?php echo $special; ?></span>
            </span><br />M.R.P.:<span class="old-price">
            <small class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></small>
            </span></li>
      <span style="color: green; font-size:12px;"><?php echo $saving; ?></span>
      <?php } ?>
      
      <?php if (!$is_logged && $te_lab_product == 1) { ?>
      <li style="text-transform: none!important;"><span style="font-size: 16px;"> Login to get this at </span><span style="font-size: 17px;"><b> Rs.<?php echo round($discounted_price); ?> </b></span></li>
      <?php } ?>
    </ul>
    <?php } ?> -->
   <!--  <hr style="margin: 0;"> -->
    <!-----Pricing--->
     

       <?php if ($salescombopgeoffers) {  foreach($salescombopgeoffers as $offer) { echo html_entity_decode($offer['html']); } } ?>
        
    <?php if ($price) { ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product_pricing product-pricing">
      <div class="col-lg-3 col-md-6 col-sm-2 col-xs-5 product_pricing product-mrp">
       <h3>MRP: <span style="font-size: 18px !important;">
            <small class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></small>
            </span></h3>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-3 col-xs-6 product_pricing product-price">
        <h3>Price: <span>
            <span class="<?php echo $live_options['live_options_special_container']; ?>"><?php echo $special; ?></span>
            </span></h3>
      </div>
    </div>
    <?php } ?>
    
    <div id="product" style="width: 100%;">
      <?php if ($options) { ?>
      <?php foreach ($options as $option) { ?>
      <?php if ($option['type'] == 'select') { ?>
      <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
        <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($option['product_option_value'] as $option_value) { ?>
          <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </option>
          <?php } ?>
        </select>
      </div>
      <?php } ?>
      <?php if ($option['type'] == 'radio') { ?>
      <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
        <label class="control-label"><?php echo $option['name']; ?></label>
        <div id="input-option<?php echo $option['product_option_id']; ?>">
            
          <?php foreach ($option['product_option_value'] as $option_value) { ?>
          <?php $countoption =count($option['product_option_value'] ); ?>
          <div class="radio">
             <?php if ($countoption == 1) { ?>
            <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
            <?php } else { ?> 
            <label>
            <?php } ?>
              <?php $price_tooltip_radio = 1; ?>
              
              <?php if ($countoption == 1) { ?>
                    <input type="radio" data-pid="<?php echo $option_value['hw_pid']; ?>" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> checked/>
            <?php } else { ?> 
                    <input type="radio" data-pid="<?php echo $option_value['hw_pid']; ?>" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
            <?php } ?>
            

              <?php if ($price_tooltip_radio && $option_value['price']) { ?>
              <?php if ($option_value['image']) { ?>
              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" data-toggle="tooltip" title="<?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)" class="img-thumbnail" /> 
              <?php } else { ?> 
              <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>"><?php echo $option_value['name']; ?></span>
              <?php } ?>
              <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
              <?php if ($option_value['image']) { ?>
              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
              <?php } ?>
              <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)</span>
              <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
              <?php if ($option_value['image']) { ?>
              <img data-toggle="tooltip" title="<?php echo $option_value['name']; ?>" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" class="img-thumbnail" />
              <?php } else { ?> 
              <span><?php echo $option_value['name']; ?></span>
              <?php } ?>
              <?php } ?>
            </label>
          </div>
          
          <?php } ?>
        </div>
      </div>
      <?php } ?>
      <?php if ($option['type'] == 'checkbox') { ?>
      <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
        <label class="control-label"><?php echo $option['name']; ?></label>
        <div id="input-option<?php echo $option['product_option_id']; ?>">
          <?php foreach ($option['product_option_value'] as $option_value) { ?>
          <div class="checkbox">
            <label>
              <?php $price_tooltip_checkbox = 1; ?>
              <?php if ($price_tooltip_checkbox) { ?>
              <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
              <?php if ($option_value['image']) { ?>
              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
              <?php } ?>
              <span<?php if ($option_value['price']) { ?> data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>"<?php } ?>><?php echo $option_value['name']; ?></span>
              <?php } else { ?>
              <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
              <?php if ($option_value['image']) { ?>
              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
              <?php } ?>
              <span><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?></span>
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'image') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label"><?php echo $option['name']; ?></label>
          <div id="input-option<?php echo $option['product_option_id']; ?>">
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <div class="radio">
              <label>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'text') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'textarea') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'file') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label"><?php echo $option['name']; ?></label>
          <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'date') { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
          <div class="input-group date">
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span></div>
          </div>
          <?php } ?>
          <?php if ($option['type'] == 'datetime') { ?>
          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
            <div class="input-group datetime">
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
              </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
              </div>
              <?php } ?>
              <?php } ?>
              <?php } ?>
              <?php if ($recurrings) { ?>
              <h3><?php echo $text_payment_recurring ?></h3>
              <div class="form-group required">
                <select name="recurring_id" class="form-control">
                  <option value=""><?php echo $text_select; ?></option>
                  <?php foreach ($recurrings as $recurring) { ?>
                  <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                  <?php } ?>
                </select>
                <div class="help-block" id="recurring-description"></div>
              </div>
              <?php } ?>  
              <div class="form-group">
               <?php if ($quantity <= 0) { ?>          
               <button class="oos button btn-block" type="button" disabled>
                Stock Over!
              </button>
              <?php } else { ?>
               
              <section id="cart_section" class="fixed_cart hidden-md hidden-xl hidden-lg" style="display: none;">
                    <?php if ($options) { ?>
                    <div class="cart_details col-xs-7">
                          <?php foreach ($options as $option) { ?> 
                              <?php if ($option['type'] == 'radio') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    
                                  <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                  <?php $countoption =count($option['product_option_value'] ); ?>
                                  <div class="radio">
                                     <?php if ($countoption == 1) { ?>
                                    <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                    <?php } else { ?> 
                                    <label>
                                    <?php } ?>
                                      <?php $price_tooltip_radio = 1; ?>
                                      
                                      <?php if ($countoption == 1) { ?>
                                            <input type="radio" data-pid="<?php echo $option_value['hw_pid']; ?>" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> checked/>
                                    <?php } else { ?> 
                                            <input type="radio" data-pid="<?php echo $option_value['hw_pid']; ?>" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                    <?php } ?>
                                    
                        
                                      <?php if ($price_tooltip_radio && $option_value['price']) { ?>
                                      <?php if ($option_value['optionimage']) { ?>
                                      <img src="<?php echo $option_value['optionimage']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" data-toggle="tooltip" title="<?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)" class="img-thumbnail" /> 
                                      <?php } else { ?> 
                                      <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>"><?php echo $option_value['name']; ?></span>
                                      <?php } ?>
                                      <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                      <?php if ($option_value['optionimage']) { ?>
                                      <img src="<?php echo $option_value['optionimage']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                                      <?php } ?>
                                      <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)</span>
                                      <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                      <?php if ($option_value['optionimage']) { ?>
                                      <img data-toggle="tooltip" title="<?php echo $option_value['name']; ?>" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" class="img-thumbnail" />
                                      <?php } else { ?> 
                                      <span><?php echo $option_value['name']; ?></span>
                                      <?php } ?>
                                      <?php } ?>
                                    </label>
                                  </div>
                                  
                                  <?php } ?>
                                </div>
                              </div>
                        <?php } ?>
                      <?php if ($option['type'] == 'checkbox') { ?>
                      <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"><?php echo $option['name']; ?></label>
                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                          <div class="checkbox">
                            <label>
                              <?php $price_tooltip_checkbox = 1; ?>
                              <?php if ($price_tooltip_checkbox) { ?>
                              <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                              <?php if ($option_value['optionimage']) { ?>
                              <img src="<?php echo $option_value['optionimage']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                              <?php } ?>
                              <span <?php if ($option_value['price']) { ?> data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>"<?php } ?>><?php echo $option_value['name']; ?></span>
                              <?php } else { ?>
                              <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                              <?php if ($option_value['optionimage']) { ?>
                              <img src="<?php echo $option_value['optionimage']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                              <?php } ?>
                              <span><?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>
                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                <?php } ?></span>
                                <?php } ?>
                              </label>
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                        <?php } ?>
                          <?php } ?>
                    </div>
                   <div class="col-xs-5" style="padding: 0;">
                        <div class="actions" style="margin-top: 20px;">
                            <div class="qty-product">
                                <input type="button" id="minus-bottom" value="-" class="form-control" style="background: #000 !important;color:#fff !important;"/>
                                <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity-bottom" class="form-control" style="width: 65px; height: 40px;float: left; text-align:center;" />
                                <input type="button" id="plus-bottom" value="&#43;" class="form-control" style="background: #000 !important;color:#fff !important;"/></div>
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                        </div><br>
                        <button type="button" id="button-cart-bottom" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
                    </div>
                    <div class="col-xs-12">
                        <?php if (!$special) { ?>
                            <span style="color: #f49a25; font-weight: bold; font-size: 18px;">
            <small class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></small>
            </span>
                        <?php } else { ?>
                            <span class="new-price" style="color: #f49a25; font-weight: bold; font-size: 18px;">
            <span class="<?php echo $live_options['live_options_special_container']; ?>"><?php echo $special; ?></span>
            </span><span class="old-price" style="text-decoration: line-through; font-size: 14px;">
            <small class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></small>
            </span>
                        <?php } ?>
                    </div>
                    <?php } else { ?>
                        <div class="col-xs-12" style="text-align: center; padding-bottom: 5px;">
                            <?php if (!$special) { ?>
                                <span style="color: #f49a25; font-weight: bold; font-size: 18px;">
            <small class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></small>
            </span>
                            <?php } else { ?>
                                <span class="new-price" style="color: #f49a25; font-weight: bold; font-size: 18px;">
            <span class="<?php echo $live_options['live_options_special_container']; ?>"><?php echo $special; ?></span>
            </span><span class="old-price" style="text-decoration: line-through; font-size: 14px;">
            <small class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></small>
            </span>
                            <?php } ?>
                        </div>
                       <div class="col-xs-6" style="padding: 0;">
                            <div class="actions" style="float: left!important;">
                                <div class="qty-product">
                                    <input type="button" id="minus-bottom" value="-" class="form-control" style="background: #000 !important;color:#fff !important;"/>
                                    <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity-bottom" class="form-control" style="width: 65px; height: 40px;float: left; text-align:center;" />
                                    <input type="button" id="plus-bottom" value="&#43;" class="form-control" style="background: #000 !important;color:#fff !important;"/>
                                </div>
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                            </div>
                        </div>
                        <div class="col-xs-6" style="padding: 0;">
                            <button type="button" id="button-cart-bottom" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block" style="margin-top: 0 !important; padding: 10px !important;"><?php echo $button_cart; ?></button>
                        </div>
                    <?php } ?>
              </section>
              

                                <?php } ?>
                <?php if($whatsapp_number) { ?>
                    <a id="button-wa" class="waorder" href="whatsapp://send?phone=<?php echo $whatsapp_number; ?>&text=I'd%20like%20to%20order%20<?php echo $heading_title; ?>"/>
                        <img alt="True Elements" src="image/catalog/images-08-19/svg-icons/whatsapp-product-page.svg" style="height: 38px;">
                    </a>
                <?php } ?>
               </div>

                <div class="col-lg-12 col-md-9 col-xs-12 p0">
                    <div class="highlight-panel panel panel-warning">
                        <div class="panel-heading">Highlights</div>
                        <div class="panel-body">
                            <?php echo $short; ?>
                        </div>
                    </div>
                </div>
                
                <?php if ($product_type != 4) { ?>
                    <div class="coupon col-lg-12 col-sm-12 col-md-12 col-xs-12 hidden">
                        <strong style="font-size: 15px; font-weight: 500 !important;">Offers:</strong>
                        <ul>
                            <li><img alt="True Elements" src="https://www.true-elements.com/image/catalog/Banner/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png">Get Extra 10% OFF on orders above Rs 499/-, Use Code: <strong>TRUE10</strong></li>
                            <li><i class="fa fa-truck" style="font-size: 15px !important; color: #000; -ms-transform: scaleX(-1); -webkit-transform: scaleX(-1); transform: scaleX(-1);
                              margin: 3px;"></i> Get Free Shipping across India using code: <strong>FREESHIPPING</strong></li>
                            <?php if($deal_coupon && $discount) { ?>
                                <li><img alt="True Elements" src="https://www.true-elements.com/image/catalog/Banner/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png">Get <?php echo $discount; ?>% OFF, Use Code:
                                  <strong><?php echo strtoupper($deal_coupon); ?></strong></li>
                            <?php } ?>
                        </ul>
                    </div>
                  <?php } ?>

                   <?php if ($product_type != 4) { ?>
                    <div class="col-lg-12 col-xs-12 p0" style="font-size: 15px; font-weight: 600; margin-top: 20px;">Offers</div>
                    <div class="coupon col-lg-12 col-xs-12">

                      <div class="col-xs-1 p0"><img alt="True Elements" src="https://www.true-elements.com/image/catalog/Banner/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png" style="margin-top: 4px;"></div>
                      <div class="col-xs-11 p0">
                      <p><span>Get Extra 10% OFF on orders above Rs 499/- Use Code: <strong>TRUE10</strong></span>
                      </p></div>
                      <div class="col-xs-1 p0"><i class="fa fa-truck" style="font-size: 15px !important; color: #000; -ms-transform: scaleX(-1); -webkit-transform: scaleX(-1); transform: scaleX(-1); margin: 5px; "></i></div>
                      <div class="col-xs-11 p0">
                      <p><span>Get Free Shipping across India using code: <strong>FREESHIPPING</strong></span></p></div>
                      <?php if($deal_coupon && $discount) { ?>
                               <div><img alt="True Elements" src="https://www.true-elements.com/image/catalog/Banner/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png">Get <?php echo $discount; ?>% OFF, Use Code: <strong><?php echo strtoupper($deal_coupon); ?></strong></div>
                            <?php } ?>
                   </div>
                  <?php } ?>
 

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 product_qty">
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 p0"><label class="control-label qty-label" for="input-quantity">Qty:</label>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 p0">
                <!-- <div class="quantity" style="border: none !important;">
                  <input type="number" min="1" max="9" step="1" value="1">
                </div> -->
                <div class="qty-product" style="margin-bottom: 0;">
                <input type="button" id="minus" value="-" class="form-control" />
                <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                <input type="button" id="plus" value="&#43;" class="form-control"/>
                </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 p0">
                  <button type="button" id="button-cart" onclick="scrolltooption();" data-loading-text="<?php echo $text_loading; ?>" class="qtybtn btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
                </div>
                </div>

                <script type="text/javascript">
                  function scrolltooption(){
                 window.scrollTo(0,100);
                  }
                </script>
            
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 traceproduct">
                <div class="traceability col-lg-4 col-sm-4 col-xs-6 p0" style="display: inline-block;">
                    <!-- <span style="font-weight: 500">Journey from farm to kitchen:&nbsp;&nbsp;</span> --><span><a onclick="traceability();" type="button" class="btn btn-default btn-trace-journey" style="padding: 7px 3px!important;">Trace Your Product</a></span>
                </div>
                <div class="col-lg-6 col-xs-6 p0 hidden-lg hidden-sm hidden-md">
                  <a href="" type="button" class="btn btn-default revwbutton" data-toggle="modal"  data-target="#rvwForm" style="width: 100%;">Write us a Review</a>
                </div>

                <?php 
                if($pincode_install_status['total'] > 0){
                if(isset($product_page_status) && $product_page_status == '1'){ 
                ?>
                <div class="col-lg-8 col-sm-8 col-xs-12 p0">
                  <form method = "POST" id = "pincheck1" class="form-inline"  <?php if(isset($_SESSION['pincode'])){ echo "style='display:none;'";}?>>
                   <div class="col-lg-8 col-sm-5 col-xs-8 p0">
                    <div class="input-group" style="margin-right: 6px; width: 99%;">
                      <input id="pin1" type="text" class="form-control pin_input" name="msg" placeholder="Enter Delivery Pincode">
                      </div>
                     </div>
                      <div class="col-lg-4 col-sm-2 col-xs-4 p0">
                      <button type="submit" onClick = "getdata1()" id="button-check" class="btn pin-btn">Check</button>
                      </div>
                      <div id="show_message" style="display:none; color: red;">Enter Delivery Pincode</div>
                    </form>
                      <div id="msg1" >
                <?php
                if(isset($_SESSION['pincode'])){
                echo "<div id='msg' ><span><i class='fa fa-map-marker'></i> Pincode : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display:inline;'><input type = 'button'  onclick = 'showform1()' value='Change' id='button-change' class='button'/></form></span><br/>";
                    if(isset($_SESSION['pin_check_delivery']) &&        $_SESSION['pin_check_delivery'] != ''){
                      echo $_SESSION['pin_check_delivery'];
                    }
                    echo $_SESSION['pin_check_result'];
                    echo"</font></font></div>";
                }
            ?>
          </div>
                  </div>
                   <?php } }?>
                  
                </div>
              </div>
                <div class="col-lg-12 col-xs-12 p0">
                  <div class="col-lg-5 col-sm-4 col-xs-12 p0">
                    <div style="padding: 12px 0 12px 0 !important;" class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count" style="padding: 0 !important; width: 82px !important;"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                  </div>
                   <div class="col-lg-6 col-xs-5 p0 hidden-xs">
                  <a href="" type="button" class="btn btn-default revwbutton" data-toggle="modal"  data-target="#rvwForm">Write us a Review</a>
                </div>
                </div>

    <!---rvw form--->
  <!--   <style type="text/css">
      .modal-content{
        border-radius: 0 !important;
      }

      .modal-header{
        border-bottom: none !important;
      }

      .modal-header hr{
        width: 15%;
        float: left;
        border-top: 3px solid #f49a25 !important;
        margin: 0 !important;
      }

    
        .modal-body input {
          border: 0 !important;
          border-bottom: 2px solid #9e9e9e !important;
          outline: none !important;
          -webkit-box-shadow: none !important;
          transition: .2s ease-in-out !important;
          box-sizing: border-box !important;
        }

        .modal-body label {
          top: 0 !important;
          left: 0; right: 0;
          color: #616161 !important;
          display: flex;
          align-items: center !important;
          position: absolute !important;
          font-size: 1rem !important;
          cursor: text !important; 
          transition: .2s ease-in-out !important;
          box-sizing: border-box !important;
        }

        .modal-body input,
        label {
          width: 100% !important;
          height: 3rem !important;
          font-size: 12px !important;
          text-align: left !important;
          color: #616161 !important;
        }

     
      .modal-body input:focus + label {
        color: #f49a25 !important;
        font-size: .8rem !important;
        pointer-events: none;
      }

      .modal-body input:valid,
       {
       
        width: 100% !important;  
        transition: 0.5s;
      }

      .modal-body input:focus {
         outline-color: black !important;
         border: 0 !important;
         box-shadow:none !important;
         border-bottom: 2px solid #f49a25 !important;
      }
    </style> -->

   <?php if ($review_status) { ?>
    <div class="modal fade" id="rvwForm" role="dialog">
    <div class="modal-dialog" style="top:100;">
    
      <!-- Modal content-->
      <div class="modal-content" style="display: inline-block;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3>Write us a Review</h3>
        </div>
        <div class="modal-body" style="display: inline-block;">
           <form id="form-review" style="display: inline-block;">
            <div class="col-lg-12">
            <div class="col-lg-6">
            <?php if ($review_guest) { ?>
              <div class="form-group">
                <input type="text" class="form-control" name="name" id="input-name" value="<?php echo $customer_name; ?>" placeholder="<?php echo $entry_name; ?>">
              </div>
            </div>
            <div class="col-lg-6">
               <div class="form-group">
                <input type="text" name="" value="" class="form-control" id="usr" placeholder="Enter Last Name">
              </div>
            </div>
            <div class="col-lg-12">
               <div class="form-group">
                <input type="text" id="input-email" name="email" value="<?php echo $customer_email; ?>" class="form-control" placeholder="Enter Email Address">
              </div>
            </div>
            <div class="col-lg-12">
               <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                <div class="help-block"><?php echo $text_note; ?></div>
            </div>
          
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12" style="margin-top: 10px;">
                
                <!-- star-rating start -->
                <div class="form-rating">
                  <label class="control-label" style="margin-top: 7px;"><?php echo $entry_rating; ?></label>
                  <div class="form-rating-container">
                    <input id="rating-5" type="radio" name="rating" value="5">
                    <label class="fa fa-stack" for="rating-5">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-4" type="radio" name="rating" value="4">
                    <label class="fa fa-stack" for="rating-4">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-3" type="radio" name="rating" value="3">
                    <label class="fa fa-stack" for="rating-3">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-2" type="radio" name="rating" value="2">
                    <label class="fa fa-stack" for="rating-2">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-1" type="radio" name="rating" value="1">
                    <label class="fa fa-stack" for="rating-1">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                  </div>
                </div>
                <!-- star-rating end -->
              </div>
              <div class="col-lg-6 col-sm-4 col-xs-12" style="margin-top: 10px;">
                <a id="button-review" class="btn btn-default">Add Comment</a>
              </div>
              <?php }else { ?>
            <?php echo $text_login; ?>
            <?php } ?>
               </div>
              </form>
        </div>
      </div>
      
    </div>
  </div>
   <?php } ?>                      <!---rvw form--->
  
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
            
               
               <?php if ($minimum > 1) { ?>
               <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
               <?php } ?>
             </div>


       <?php if ($salescombopgeoffers) {  foreach($salescombopgeoffers as $offer) { echo html_entity_decode($offer['html']); } } ?>
        
             <?php if ($price) { ?>
             <ul class="list-unstyled">
              <?php if ($tax) { ?>
              <!--<li><?php echo $text_tax; ?><span class="ex-text"><?php echo $tax; ?></span></li> -->
              <?php } ?>
              <?php if ($points) { ?>
              <!--<li><?php echo $text_points; ?><span class="ex-text">
            <span class="<?php echo $live_options['live_options_points_container']; ?>"><?php echo $points; ?></span>
            </span></li> -->
              <?php } ?>
              <?php if ($discounts) { ?>
              <?php foreach ($discounts as $discount) { ?>
              <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><span class="ex-text"><?php echo $discount['price']; ?></span></li>
              <?php } ?>
              <?php } ?> 
            </ul>
            <?php } ?>

            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
            
            <!--- POSTCODE --->
            <?php 
            if($pincode_install_status['total'] > 0){
            if(isset($product_page_status) && $product_page_status == '1'){ 
            ?>
          <!--   <div style="width:100%; float:left; overflow: none;">
              <form method = "POST" id = "pincheck1" <?php if(isset($_SESSION['pincode'])){ echo "style='display:none;'";}?>>
                <input placeholder="Enter Delivery Pincode" type="text" id="pin1" style="padding: 5px;">
                <input type = "button"  onClick = "getdata1()" value="Check" id="button-check" class="button" style="background: #000 !important;border-radius: 0px !important;padding: 7px 12px 7px 12px;"/>
                <div id="show_message" style="display:none; color: red;">Enter Delivery Pincode</div>
              </form>
              <div id="msg1" >
                <?php
                if(isset($_SESSION['pincode'])){
                echo "<div id='msg' ><span><i class='fa fa-map-marker'></i> Pincode : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display:inline;'><input type = 'button'  onclick = 'showform1()' value='Change' id='button-change' class='button'/></form></span><br/>";
                    if(isset($_SESSION['pin_check_delivery']) &&        $_SESSION['pin_check_delivery'] != ''){
                      echo $_SESSION['pin_check_delivery'];
                    }
                    echo $_SESSION['pin_check_result'];
                    echo"</font></font></div>";
                }
            ?>
          </div>
        </div>          --> 
        <?php } }?>
        <!--- POSTCODE --->
        

        
        
        <!-- AddThis Button BEGIN -->
        
        <!-- AddThis Button END -->
      </div>
    </div>

    <div class="col-3">


 
 <!--review--->



 <!--review end-->

      <?php if ($product_type != 4 && $attribute_groups) { ?> 

       <!-- <div class="module-title" style="margin: 25px 0 5px 0 !important; border-bottom: 1px solid #ebebeb; font-weight: 700;"><span><?php echo $tab_attribute; ?> </span><br /></div> --><?php } ?>    
        <?php if ($attribute_groups) { ?>
        <div class="tab-pane">
          <!-- <table class="table table-bordered">
            <?php foreach ($attribute_groups as $attribute_group) { ?>
            <thead>
              <tr>
                <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
              <tr>
                <td><?php echo $attribute['name']; ?></td>
                <td><?php echo $attribute['text']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
            <?php } ?>
          </table> -->
        </div>
        <?php } ?>
        
           <!-- <div class="module-title" style="margin: 5px 0 5px 0 !important; border-bottom: 1px solid #ebebeb; font-weight: 700;"><span></span><br /></div> -->
        <div class="tab-pane active product-description">
            
            <?php if ($description) { ?>  
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 product-info">
                    <div class="productdesc well"> 
                        <h3 class="whytetitle"><strong>Why <?php echo $heading_title; ?> is better than the rest?</strong></h3>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0">
                        <p class="">
                            <?php echo $description; ?>
                        </p>
                      </div>
                    </div>
                   </div>
            <?php } ?>
 
            <?php if ($feature_image_1) { ?> 
                <div id="hb" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center hb-sect">
                        <img class="hb-img img-circle" src="<?php echo $feature_image_1; ?>">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center hb-sect">
                        <img class="hb-img img-circle" src="<?php echo $feature_image_2; ?>">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
                        <img class="img-circle hb-img" src="<?php echo $feature_image_3; ?>">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">
                        <img class="img-circle hb-img" src="<?php echo $feature_image_4; ?>">
                    </div>
                </div>
            <?php } ?>

            <?php if ($ingredients_image_desktop) { ?>
                <div class="col-lg-12 col-xs-12 p0 ingredient-banner">
                    <img class="img-responsive hidden-xs" src="<?php echo $ingredients_image_desktop; ?>">
                    <img class="img-responsive hidden-lg hidden-md hidden-sm" src="<?php echo $ingredients_image_mobile; ?>">
                </div>
            <?php } ?>
            <!--graph-->
            <div id="chart" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <ul id="bars">
                <li><div data-percentage="95" class="bar"></div><span class="chart-span">Sodium</span><span class="chart-span2">210 MG</span></li>
                <li><div data-percentage="75" class="bar"></div><span class="chart-span">Potassium</span><span class="chart-span2">65 MG</span></li>
                <li><div data-percentage="45" class="bar"></div><span class="chart-span">Sodium</span><span class="chart-span2">1.20 MG</span></li>
                <li><div data-percentage="15" class="bar"></div><span class="chart-span">Potassium</span><span class="chart-span2">15 MG</span></li>
                <li><div data-percentage="10" class="bar"></div><span class="chart-span">Sodium</span><span class="chart-span2">10 MG</span></li>
                <li><div data-percentage="0" class="bar"></div><span class="chart-span">Potassium</span><span class="chart-span2">0 MG</span></li>
              </ul>
            </div>
            <?php if ($htu_image_1) { ?> 
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 product-htu">
                  <h3 class="usetitle"><strong>How To Use <?php echo $heading_title; ?>:</strong></h3>
                  <div class="forshadowbox hidden-xs">
                    <div class="col-md-12 col-xs-12 col-sm-12 usebox">
                      
                        <?php if ($htu_image_1) { ?> 
                            <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3 imgalign shadow">
                                <img src="<?php echo $htu_image_1; ?>" class="img-responsive useimg forshadowboximg">
                                <h4 class="product-description h4"><b><?php echo $htu_text_1; ?></b></h4>
                            </div>
                        <?php } ?>
                        <?php if ($htu_image_2) { ?> 
                            <div class="col-md-3 col-sm-3 col-xs-12 col-lg-3 imgalign shadow">
                                <img src="<?php echo $htu_image_2; ?>" class="img-responsive useimg forshadowboximg">
                                <h4 class="product-description h4"><strong><?php echo $htu_text_2; ?></strong></h4>
                            </div>
                        <?php } ?>
                        <?php if ($htu_image_3) { ?> 
                            <div class="col-md-3 col-sm-3 col-xs-12 col-lg-3 imgalign shadow">
                                <img src="<?php echo $htu_image_3; ?>" class="img-responsive useimg forshadowboximg">
                                <h4 class="product-description h4"><strong class="uset"><?php echo $htu_text_3; ?></strong></h4>
                            </div>
                        <?php } ?>
                        <?php if ($htu_image_4) { ?> 
                            <div class="col-md-3 col-sm-3 col-xs-12 col-lg-3 imgalign shadow marginnbottom">
                                <img src="<?php echo $htu_image_4; ?>" class="img-responsive useimg forshadowboximg">
                                <h4 class="product-description h4"><strong class=" lineht"><?php echo $htu_text_4; ?></strong></h4>
                            </div>
                        <?php } ?>
                    </div>
                  </div>
                
                  <div class="forshadowbox hidden-sm hidden-md hidden-xl hidden-lg p0"> 
                    <div class="col-md-12 col-xs-12 col-sm-12 usebox p0">
                        
                        <?php if ($htu_image_1) { ?>
                            <div class="col-md-12 col-xs-5 col-sm-6 forshadowbox-mob s02">
                                <img src="<?php echo $htu_image_1; ?>" class="img-responsive useimg forshadowboximg">
                                <h4><strong><?php echo $htu_text_1; ?></strong></h4>
                            </div>
                        <?php } ?>
                        <?php if ($htu_image_2) { ?>
                            <div class="col-md-12 col-xs-5 col-sm-6 forshadowbox-mob s02">
                                <img src="<?php echo $htu_image_2; ?>" class="img-responsive useimg forshadowboximg2">
                                <h4><strong><?php echo $htu_text_2; ?></strong></h4>
                            </div>
                        <?php } ?>
                        <?php if ($htu_image_3) { ?>
                            <div class="col-md-12 col-xs-5 col-sm-6 forshadowbox-mob s02 mr-bt">
                                <img src="<?php echo $htu_image_3; ?>" class="img-responsive useimg forshadowboximg3">
                                <h4><strong class="uset"><?php echo $htu_text_3; ?></strong></h4>
                            </div>
                        <?php } ?>
                        <?php if ($htu_image_4) { ?>
                            <div class="col-md-12 col-xs-5 col-sm-6 forshadowbox-mob s02 phnviewmar mr-bt">
                                <img src="<?php echo $htu_image_4; ?>" class="img-responsive useimg forshadowboximg">
                                <h4><strong><?php echo $htu_text_4; ?></strong></h4>
                            </div>
                        <?php } ?> 
                    </div>
                  </div>
                </div>
                  
            <?php } ?>
            
             
            
            <?php if ($storage) { ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 product-content-info">
                    <div class="well col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px;">
                        <h4 style="font-size: 16px !important;">Storage Information</h4>
                        <?php echo $storage; ?> 
                    </div>
                </div>
            <?php } ?>
            
            <?php if ($storage) { ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 product-content-info">
                  <div class="well col-md-12 col-sm-12 col-xs-12">
                        <h4 style="font-size: 16px !important;">Allergen Information</h4>
                        <?php echo $allergen; ?>
                    </div>
                </div>
            <?php } ?>
            
            

<style>
    .item-inner .name-wishlist {
        min-height: 59px;
    }
    
    .boughtoptions {
        min-height: 20px;
    }
</style>
<?php if ($boughtproducts) { ?>
<div class="related-product-container quickview-product container" id="bought-product-container">
  <div class="module-title"><span>Frequently bought together</span>
 
        <div class="pull-right add-products">
            <?php if($in_stock_products) { ?>
                <div class="buttons hidden-sm hidden-xs" style="text-align: center;"> 
                    
                    <button id="checkall_button" onclick="submittocart('<?php echo $in_stock_products; ?>','bought-product-container')" type="button" class="hidden-sm hidden-xs btn btn-primary btn-lg btn-block" style="width: 100%;display:inline-block;" disabled>Add bundle to Cart</button>
                </div>
                <!--a id="checkall_button"  onclick="submittocart('<?php echo $in_stock_products; ?>','bought-product-container')" type="button" class="hidden-md hidden-lg hidden-xlg btn button button-cart-mobile"><i class="fa fa-shopping-cart" style="color:#fff;"></i></a-->
                Rs.<span id="proids">0</span>
            <?php } else { ?>
                <button class="oos btn button hidden-xs hidden-sm" type="button" data-toggle="tooltip" title="Stock Over!" disabled>Stock Over!</button>
                <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
            <?php } ?>
        </div>
    </div>    
  <div class="row">
    <div class="related-product quickview-added qv-wtext">
      <?php foreach ($boughtproducts as $boughtproduct) { ?>
      <div class="row_items" style="margin-bottom: 5px;">
        <div class="product-layout product-grid">
          <div class="product-thumb item-inner">
            <?php if($boughtproduct['coupon_discount']){ ?> 
        <div class="deals-discount"><h5><?php echo round($boughtproduct['discount']); ?> %<br>Off</h5></div>
      <?php } ?>
            <div class="images-container">
              <a href="<?php echo $boughtproduct['href']; ?>">
                <?php if ($boughtproduct['thumb']) { ?>
                
                <?php if($boughtproduct['rotator_image']){ ?>
                <img class="image2" src="<?php echo $boughtproduct['rotator_image']; ?>" alt="<?php echo $boughtproduct['name']; ?>" style="max-width: 70% !important;"/>
                <?php } ?>
                <?php } else { ?>
                <img src="image/cache/no_image-100x100.png" alt="<?php echo $boughtproduct['name']; ?>" />
                <?php } ?>
                <img src="<?php echo $boughtproduct['thumb']; ?>" alt="<?php echo $boughtproduct['name']; ?>" title="<?php echo $boughtproduct['name']; ?>" class="image1" style="max-width: 70% !important;"/>
              </a>
          
          <?php if (isset($boughtproduct['rating'])) { ?>
          <center><div class="ratings">
            <div class="rating-box">
              <?php for ($i = 0; $i <= 5; $i++) { ?>
              <?php if ($boughtproduct['rating'] == $i) {
              $class_r= "rating".$i;
              echo '<div class="'.$class_r.'">rating</div>';
            } 
          }  ?>
        </div>
      </div> </center>
      <?php } ?>
      
    </div>
    <div class="des-container">
                  <div class="name-wishlist">
                    <h2 class="product-name hidden-xs hidden-sm" ><a href="<?php echo $boughtproduct['href']; ?>"><?php echo $boughtproduct['name']; ?></a></h2>
                    <h2 class="product-name hidden-md hidden-xl hidden-lg" ><a href="<?php echo $boughtproduct['href']; ?>"><?php echo $boughtproduct['name_mobile']; ?></a></h2>
                  </div>
                    <?php $is_optionqty = 1; ?>
                    <?php if($boughtproduct['options']) { ?>
                        <div class="options boughtoptions options-<?php echo $boughtproduct['product_id']; ?>">
                            <?php foreach($boughtproduct['options'] AS $option) { ?>
                                <?php $countoption = count($option['product_option_value']); ?>
                                <?php $price_tooltip_radio = 1; ?>
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                    <div class="radio">
                                        <?php if($option_value['product_option_value_id'] == $boughtproduct['base_option_id']) { ?>
                                            <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                <input type="radio" class="option-<?php echo $boughtproduct['product_id']; ?>" onchange="price_with_options_ajax_call1(<?php echo $boughtproduct['product_id']; ?>,'related-product-container')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/> 
                                        <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                            <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                        <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                        <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?></span>
                                        <?php } ?>
                                        </label>
                                        
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="options"></div>
                        <?php $is_optionqty = 0; ?>
                    <?php } ?>
                    
                  <div class="price-rating">
                    <div class="price-label">
                      <div class="box-price">
                          <?php if ($boughtproduct['price']) { ?>
                            <?php if (!$boughtproduct['special']) { ?>
                              <div class="price-box box-regular">
                                <span class="regular-price">
                                  <span class="price-new-<?php echo $boughtproduct['product_id']; ?> price"><?php echo $boughtproduct['price']; ?></span>
                                </span>
                              </div>
                            <?php } else { ?>
                              <div class="price-box box-special">
                                <p class="special-price"><span class="price-new-<?php echo $boughtproduct['product_id']; ?> price"><?php echo $boughtproduct['special']; ?></span></p>
                                <p class="price-old-<?php echo $boughtproduct['product_id']; ?> old-price"><span class="price"><?php echo $boughtproduct['price']; ?></span></p>
                              </div>
                            <?php } ?>
                             <?php if ($boughtproduct['coupon_discount']) { ?>
                        <span class="price-coupon price-coupon-<?php echo $boughtproduct['product_id']; ?>"><b> <?php echo $boughtproduct['coupon_discount']; ?> </b></span><span> <?php echo $boughtproduct['coupon'] ? 'Using code: <b>' . $boughtproduct['coupon'] . '</b>' : ''; ?></span>
                    <?php } ?>
                          <?php } ?>
                          <input type="checkbox" id="<?php echo $boughtproduct['buy_price']; ?>"  class="checkbox_check" value = "<?php echo $boughtproduct['product_id']; ?>" />
                      </div>
                    </div>
                  
                  </div>
                </div>
    
  </div>
</div>
</div>
    
<!--div>
    <label for="<?php echo $boughtproduct['product_id']; ?>"><?php echo $boughtproduct['name']; ?></label>
    <input type="checkbox" id="<?php echo $boughtproduct['product_id']; ?>" class = "chk" value = "<?php echo $boughtproduct['product_id']; ?>" />
</div-->
<?php } ?>
</div>
</div>
</div>

<?php } ?>            

  <?php if ($recipes) { ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0">
    <div class="related-recipe-container quickview-product">
        <div class="module-title">
            <span>Recommended Recipes for you</span>
        </div>
        <div class="">
            <div class="related-product1 quickview-added qv-wtext">
                <?php foreach ($recipes as $recipe) { ?>
                    <div class="recipe_row_items" style="margin-bottom: 5px; border-bottom: 1px solid #ebebeb;border-right: 1px solid #ebebeb;border-left: 1px solid #ebebeb;border-top: 1px solid #ebebeb;height: 195px;">
                        <div class="product-layout product-grid">
                            <div class="product-thumb item-inner">
                                <div class="images-container">
                                    <a href="<?php echo $recipe['href']; ?>">
                                        <img src="<?php echo $recipe['thumb']; ?>" alt="<?php echo $recipe['name']; ?>" title="<?php echo $recipe['name']; ?>" class="image1"/>
                                    </a>
                                </div>
                                <div class="des-container">
                                    <div class="name-wishlist">
                                        <h2 class="product-name"><a href="<?php echo $recipe['href']; ?>"><?php echo $recipe['name']; ?></a></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
  </div>
<?php } ?>

<!--blog-->

 <!-- <?php if ($blogs) { ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p0">
    <div class="related-recipe-container quickview-product">
        <div class="module-title">
            <span>Recommend Blogs for you</span>
        </div>
        <div class="">
            <div class="related-product1 quickview-added qv-wtext">
                <?php foreach ($blogs as $blog) { ?>
                    <div class="recipe_row_items" style="margin-bottom: 5px; border-bottom: 1px solid #ebebeb;border-right: 1px solid #ebebeb;border-left: 1px solid #ebebeb;border-top: 1px solid #ebebeb;height: 195px;">
                        <div class="product-layout product-grid">
                            <div class="product-thumb item-inner">
                                <div class="images-container">
                                    <a href="<?php echo $blog['href']; ?>">
                                        <img src="image/<?php echo $blog['image']; ?>" alt="<?php echo $blog['name']; ?>" title="<?php echo $blog['name']; ?>" class="image1"/>
                                    </a>
                                </div>
                                <div class="des-container">
                                    <div class="name-wishlist">
                                        <h2 class="product-name"><a href="<?php echo $blog['href']; ?>"><?php echo $blog['name']; ?></a></h2>
                                    </div>
                                    <?php if (strlen($blog['description']) > 100) { ?>
                                    <p><?php echo substr($blog['description'],0,100), "..." ?></p>
                                     <?php } else { ?>
                                    <p><?php echo $blog['description']; ?></p>
                                     <?php } ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
  </div>
<?php } ?> -->
 
<!-- <?php if ($blogs) { ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-padding" style="display:inline-block;">
  <div class="blog-section">
  <h3>Recommended blogs for you</h3>
  <div id="related-blogs" class="owl-carousel owl-theme">

    <?php foreach ($blogs as $blog) { ?>
    <div class="item">
        <div class="blog-card blog-card-primary blog-card-hover blog-card-body">
          <div class="col-lg-12 blog-padding">
          <div class="col-lg-6 blog-padding"><a href="<?php echo $blog['href']; ?>"><img class="blog-padding" src="image/<?php echo $blog['image']; ?>" alt="<?php echo $blog['name']; ?>" title="<?php echo $blog['name']; ?>" class="image1"/></a></div>
            <div class="col-lg-6 blog-desc"><a href="<?php echo $blog['href']; ?>"><h4 class="blog-card-title"><?php echo $blog['name']; ?></h4></a>
              
              <?php if (strlen($blog['description']) > 100) { ?>
              <p><?php echo substr($blog['description'],0,100), "..." ?></p>
              <?php } else { ?>
              <p><?php echo $blog['description']; ?></p>
               <?php } ?> 

              <a class="btn warning" href="<?php echo $blog['href']; ?>">Read More</a>
            </div>
        </div>
      </div>
  </div>
  <?php } ?>
</div>
</div>
</div>
<?php } ?> -->

<?php if ($blogs) { ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-padding related-blogs">
  <span>Recommended Blogs for you</span>
  <?php foreach ($blogs as $blog) { ?>
  <div class="blog-section">
    <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12 blog-padding blog-img">
    <a href="<?php echo $blog['href']; ?>"><img src="image/<?php echo $blog['image']; ?>" alt="<?php echo $blog['name']; ?>" title="<?php echo $blog['name']; ?>"></a>
  </div>
  <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12 blog-content">
    <h3><a href="<?php echo $blog['href']; ?>"><?php echo $blog['name']; ?></a></h3>
     <?php if (strlen($blog['description']) > 250) { ?>
         <p><?php echo substr($blog['description'],0,250), "..." ?></p>
        <?php } else { ?>
          <p><?php echo $blog['description']; ?></p>
        <?php } ?>
       <a class="blog-link" href="<?php echo $blog['href']; ?>">Read More</a>
  </div>
</div>
 <?php } ?>
</div>
<?php } ?>
        <!--product page video-->
        <?php if ($video_link) { ?>
            <div class="videoWrap module-title">
              <span style="display: block;">Delicious Recipe with a TRUE Twist</span>
              <hr style="margin-top: 0;"/>
                <div class="vidBox text-center"> 
                    <a rel="https://www.youtube.com/embed/<?php echo $video_link; ?>?rel=0&amp;showinfo=0&amp;autoplay=1" class="videoLink">
                     <img src="/image/catalog/recipe-video.jpg" style="cursor: pointer;" alt="True Elements"></a> 
                </div> 
            </div>
        <?php } ?>
        <!--product page video-->
        <!--div class="module-title" style="margin: 25px 0 5px 0 !important;    border-bottom: 1px solid #ebebeb;"><span><?php echo $tab_review; ?> </span><br /></div-->


       
<div id="review"></div>
<?php if ($total_reviews > 0) { ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 customer-review p0">
<div class="swiper-container">
  <div class="swiper-wrapper">
    <?php foreach ($topreviews as $review) { ?>
    <div class="swiper-slide">
      <div class="panel" style="border: none !important;">
        <div class="panel profile-card-2 text-center">
              <!-- <img src="https://randomuser.me/api/portraits/men/64.jpg" alt="profile-image" class="profile"/> -->
              <div class="review-icon">
                <span><i class="fa fa-quote-left"></i></span>
              </div> 
               <div class="panel-heading" style="margin-top: 6%;">
                <h4 class=""><?php echo $review['author']; ?></h4>
                 <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($review['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
              <?php } ?>
                </div>
                <div class="pt-5 panel-body">
               
                <?php if (strlen($review['text']) > 100) { ?>
                 <p class="card-text"><?php echo substr($review['text'],0,100),"..."; ?></p>
                  <?php } else { ?>
                  <p class="card-text"><?php echo substr($review['text'],0,100); ?></p>
                  <?php } ?> 
                  </div>
                </div>
        </div>
    </div>
     <?php } ?>

     <!-- <div class="swiper-slide">
      <div class="panel" style="border: none !important;">
        <div class="panel profile-card-2 text-center">
              <div class="review-icon">
                <span><i class="fa fa-quote-left"></i></span>
              </div> 
               <div class="panel-heading" style="margin-top: 15px;">
                <h4 class="">Rahul</h4>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                </div>
                <div class="pt-5 panel-body">
                <p class="card-text">Healthy.</p>
                  </div>
                </div>
        </div>
    </div> -->
    <!--   <div class="swiper-slide">
      <div class="panel" style="border: none !important;">
        <div class="panel profile-card-2 text-center">
          
              <div class="review-icon">
                <span><i class="fa fa-quote-left"></i></span>
              </div> 
               <div class="panel-heading" style="margin-top: 15px;">
                <h4 class="">Archie</h4>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                </div>
                <div class="pt-5 panel-body">
                <p class="card-text">Genuinely i liked this product.</p>
                  </div>
                </div>
        </div>
    </div>
      <div class="swiper-slide">
      <div class="panel" style="border: none !important;">
        <div class="panel profile-card-2 text-center">
        
              <div class="review-icon">
                <span><i class="fa fa-quote-left"></i></span>
              </div> 
               <div class="panel-heading" style="margin-top: 15px;">
                <h4 class="">Jofi</h4>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                </div>
                <div class="pt-5 panel-body">
                <p class="card-text">Its my first time and I found it to be simple and affordable.</p>
                  </div>
                </div>
        </div>
    </div>
      <div class="swiper-slide">
      <div class="panel" style="border: none !important;">
        <div class="panel profile-card-2 text-center">
        
              <div class="review-icon">
                <span><i class="fa fa-quote-left"></i></span>
              </div> 
               <div class="panel-heading" style="margin-top: 15px;">
                <h4 class="">Rohit kumar</h4>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                </div>
                <div class="pt-5 panel-body">
                <p class="card-text">Good in taste good for diet.</p>
                  </div>
                </div>
        </div>
    </div>
      <div class="swiper-slide">
      <div class="panel" style="border: none !important;">
        <div class="panel profile-card-2 text-center">
            
              <div class="review-icon">
                <span><i class="fa fa-quote-left"></i></span>
              </div> 
               <div class="panel-heading" style="margin-top: 15px;">
                <h4 class=""> Rahul Bali</h4>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                <span class="fa fa-star rating1"></span>
                </div>
                <div class="pt-5 panel-body">
                <p class="card-text">Value for Money Good amount of Crunchy Dry Fruits.</p>
                  </div>
                </div>
        </div>
    </div> -->
  </div>
  <div class="swiper-pagination"></div>
  <div class="swiper-button-prev hidden-xs"></div>
  <div class="swiper-button-next hidden-xs"></div>
</div>
<div class="col-md-12 col-xs-12 col-sm-12 text-center">
    <a href="index.php?route=product/product/product_reviews&product_id=<?php echo $product_id; ?>" target="_blank" class="btn button" style="margin-bottom:15px; color: #f49a25!important; background-color: #fff !important; border: 1px solid #f49a25 !important;">See All Reviews</a>
</div>
</div> 
 <?php } ?>
    </div>

<!-- start shop gallery -->
<!-- <link href="catalog/view/javascript/instagramshopgallery/swiper/css/swiper.min.css" type="text/css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/instagramshopgallery/swiper/css/opencart.css" type="text/css" rel="stylesheet" media="screen" />
<link href="catalog/view/theme/default/stylesheet/instagramshopgallery.css?v=3.1.1" type="text/css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/instagramshopgallery/swiper/js/swiper.jquery.js" type="text/javascript"></script>
<div class="container isl-instagramphotos islip-page th-isl" id="container">

  <div class="row">

    <div id="content" style="min-height: 300px;">
      

     <div class="new-title" style="margin-bottom: 28px;">
            <center><h2><p class="te">What others say about Millet Granola</p></h2>
            (Actual images posted by Customers on Instagram)</center>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4"></div>
            <div class="title-line col-sm-4 col-md-4" style="margin-top: -10px;margin-bottom: 20px;">
                <div class="tl-1" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
                <div class="tl-2" style="display: inline-block;height: 13px;margin: 0 5px;position: relative;top: 5px;width: 13px;border: 1px solid #ebc131;border-radius: 50px;"></div>
                <div class="tl-3" style="width: 45%;height: 1px;display: inline-block;background: #eee;"></div>
            </div>
            <div class="col-sm-4 col-md-4"></div>
        </div>

      <div class="clearfix">
    

        <div class="islip-gutter-5px clearfix js-islip-page-container"></div>

        <div class="js-photos-notification"></div>
        <div class="text-center">
          <a class="btn btn-default btn-info btn-sm js-fetch-more" data-isl-fetch>Load more</a>
        </div>
      </div>
  </div></div>
</div>

end shop gallery -->
    <?php echo $content_bottom; ?>
  </div>
  <?php echo $column_right; ?>
</div>

<!-- SOF Alsobought -->

<?php if ($alsoproducts) { ?>
<div class="alsoproduct-product-container quickview-product" id="alsoproduct-product-container">
  <div class="module-title"><span>Customers Who Bought <?php echo $heading_title; ?> Also Bought</span></div>
  <div class="row">
    <div class="related-product quickview-added qv-wtext">
      <?php foreach ($alsoproducts as $alsoproduct) { ?>
      <div class="row_items" style="margin-bottom: 5px;">
        <div class="product-layout product-grid">
          <div class="product-thumb item-inner">
               <div class="label-product l-new">
                    <span><?php echo $alsoproduct['percentage']; ?>% Bought this  </span>
                  </div>
            <span style="display:none"><?php echo $product['no_sells']; ?> <br><?php echo $product['totalorder']; ?></span>      
            <?php if($alsoproduct['coupon_discount']){ ?> 
                  <div class="deals-discount"><h5><?php echo round($alsoproduct['discount']); ?> %<br>Off</h5></div>
      <?php } ?>
        <div class="images-container">
              <a href="<?php echo $alsoproduct['href']; ?>">
                <?php if ($alsoproduct['thumb']) { ?>
                
                <?php if($alsoproduct['rotator_image']){ ?>
                <img class="image2" src="<?php echo $alsoproduct['rotator_image']; ?>" alt="<?php echo $alsoproduct['name']; ?>" style="max-width: 70% !important;"/>
                <?php } ?>
                <?php } else { ?>
                <img src="image/cache/no_image-100x100.png" alt="<?php echo $alsoproduct['name']; ?>" />
                <?php } ?>
                <img src="<?php echo $alsoproduct['thumb']; ?>" alt="<?php echo $alsoproduct['name']; ?>" title="<?php echo $alsoproduct['name']; ?>" class="image1" style="max-width: 70% !important;"/>
              </a>
          
          <?php if (isset($alsoproduct['rating'])) { ?>
          <center><div class="ratings">
            <div class="rating-box">
              <?php for ($i = 0; $i <= 5; $i++) { ?>
              <?php if ($alsoproduct['rating'] == $i) {
              $class_r= "rating".$i;
              echo '<div class="'.$class_r.'">rating</div>';
            } 
          }  ?>
        </div>
      </div> </center>
      <?php } ?>
      
    </div>
    <div class="des-container">
                  <div class="name-wishlist">
                    <h2 class="product-name hidden-xs hidden-sm" ><a href="<?php echo $alsoproduct['href']; ?>"><?php echo $alsoproduct['name']; ?></a></h2>
                    <h2 class="product-name hidden-md hidden-xl hidden-lg" ><a href="<?php echo $alsoproduct['href']; ?>"><?php echo $alsoproduct['name_mobile']; ?></a></h2>
                  </div>
                    <?php $is_optionqty = 1; ?>
                    <?php if($alsoproduct['options']) { ?>
                        <div class="options options-<?php echo $alsoproduct['product_id']; ?>">
                            <?php foreach($alsoproduct['options'] AS $option) { ?>
                                <?php $countoption = count($option['product_option_value']); ?>
                                <?php $price_tooltip_radio = 1; ?>
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                    <div class="radio">
                                        <?php if($countoption == 1) { ?>
                                            <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                <input type="radio" class="option-<?php echo $alsoproduct['product_id']; ?>" onchange="price_with_options_ajax_call1(<?php echo $alsoproduct['product_id']; ?>,'alsoproduct-product-container')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                        <?php } else { ?>
                                            <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                <input type="radio" class="option-<?php echo $alsoproduct['product_id']; ?>" onchange="price_with_options_ajax_call1(<?php echo $alsoproduct['product_id']; ?>,'alsoproduct-product-container')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                        <?php } ?>
                                        <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                            <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                        <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                        <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?></span>
                                        <?php } ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="options"></div>
                        <?php $is_optionqty = 0; ?>
                    <?php } ?>
                    
                  <div class="price-rating">
                    <div class="price-label">
                      <div class="box-price">
                          <?php if ($alsoproduct['price']) { ?>
                            <?php if (!$alsoproduct['special']) { ?>
                              <div class="price-box box-regular">
                                <span class="regular-price">
                                  <span class="price-new-<?php echo $alsoproduct['product_id']; ?> price"><?php echo $alsoproduct['price']; ?></span>
                                </span>
                              </div>
                            <?php } else { ?>
                              <div class="price-box box-special">
                                <p class="special-price"><span class="price-new-<?php echo $alsoproduct['product_id']; ?> price"><?php echo $alsoproduct['special']; ?></span></p>
                                <p class="price-old-<?php echo $alsoproduct['product_id']; ?> old-price"><span class="price"><?php echo $alsoproduct['price']; ?></span></p>
                              </div>
                            <?php } ?>
                            <?php if ($alsoproduct['coupon_discount']) { ?>
                        <span class="price-coupon price-coupon-<?php echo $alsoproduct['product_id']; ?>"><b> <?php echo $alsoproduct['coupon_discount']; ?> </b></span><span> <?php echo $alsoproduct['coupon'] ? 'Using code: <b>' . $alsoproduct['coupon'] . '</b>' : ''; ?></span>
                    <?php } ?>
                          <?php } ?>
                      </div>
                    </div>
                  
                  </div>
                    <div class="add-to-cart" style="padding-top: 7px;">
                     <?php if($alsoproduct['product_type'] != 5) { ?>
                            <?php if($is_optionqty != 1 && $alsoproduct['quantity'] > 0) { ?>
                                <div style="width: 50%;">
                                    <input type="button" value="-" class="minus-button" onclick="minuss(<?php echo $alsoproduct['product_id']; ?>,'alsoproduct-product-container')" />
                                    <input type="text" class="quantity-<?php echo $alsoproduct['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $alsoproduct['product_id']; ?>,'alsoproduct-product-container')" />
                                    <input type="button" value="&#43;" class="plus-button" onclick="pluss(<?php echo $alsoproduct['product_id']; ?>,'alsoproduct-product-container')" />
                                </div>
                                <button type="button" class="hidden-md hidden-lg hidden-xl btn button button-cart cart-<?php echo $alsoproduct['product_id']; ?>" onclick ="addtocart(<?php echo $alsoproduct['product_id']; ?>,'alsoproduct-product-container')"><i class="fa fa-shopping-cart"></i></button>
                                <button type="button" class="hidden-xs hidden-sm btn button button-cart cart-<?php echo $alsoproduct['product_id']; ?>" onclick ="addtocart(<?php echo $alsoproduct['product_id']; ?>,'alsoproduct-product-container')"><?php echo $button_cart; ?></button>
                            <?php } else { ?>
                                <button class="hidden-md hidden-lg hidden-xl oos btn button" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                <button class="hidden-xs hidden-sm oos btn button" type="button" disabled>Stock Over!</button>
                            <?php } ?>
                        <?php } else { ?>
                            <?php if($alsoproduct['quantity'] > 0) { ?>
                    <a href="<?php echo $alsoproduct['href']; ?>" class="btn button button-personalize">Personalize Now</a>
                  <?php } else { ?>
                    <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
                    <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                 <?php } ?>
                        <?php } ?>
                    </div>
                </div>
    
  </div>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
<?php } ?>


<!-- EOF Alsobought -->

<!-- videos -->

    <?php if ($videos) { ?>
        <div class="product-video-container container" id="product-video-container">
            <div class="module-title" style="margin-bottom: 0px;"><span>Videos</span></div>
            <div class="col-md-12 col-xs-12 col-sm-12" style="background: #fff;margin-top: 15px;padding: 15px;">
                <div class="videos-carousel">
                    <?php foreach ($videos as $video) { ?>
                        <div class="col-md-12 col-xs-12 col-sm-12 video-container">
                            <iframe src="https://www.youtube.com/embed/<?php echo $video['url']; ?>?rel=0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="100%" height="225" frameborder="0"></iframe>
                        </div>  
                    <?php } ?> 
                </div>
            </div>
        </div>
    <?php } ?>

<!-- videos -->

<?php if ($products) { ?>
<div class="related-product-container quickview-product container" id="related-product-container">
  <div class="module-title"><span>You could also try</span></div>
  <div class="row">
    <div class="related-product quickview-added qv-wtext">
      <?php foreach ($products as $product) { ?>
      <div class="row_items" style="margin-bottom: 5px;">
        <div class="product-layout product-grid">
          <div class="product-thumb item-inner">
            <?php if($product['coupon_discount']){ ?> 
        <div class="deals-discount"><h5><?php echo round($product['discount']); ?> %<br>Off</h5></div>
      <?php } ?>
            <div class="images-container">
              <a href="<?php echo $product['href']; ?>">
                <?php if ($product['thumb']) { ?>
                
                <?php if($product['rotator_image']){ ?>
                <img class="image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" style="max-width: 70% !important;"/>
                <?php } ?>
                <?php } else { ?>
                <img src="image/cache/no_image-100x100.png" alt="<?php echo $product['name']; ?>" />
                <?php } ?>
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="image1" style="max-width: 70% !important;"/>
              </a>
          
          <?php if (isset($product['rating'])) { ?>
          <center><div class="ratings">
            <div class="rating-box">
              <?php for ($i = 0; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] == $i) {
              $class_r= "rating".$i;
              echo '<div class="'.$class_r.'">rating</div>';
            } 
          }  ?>
        </div>
      </div> </center>
      <?php } ?>
      
    </div>
    <div class="des-container">
                  <div class="name-wishlist">
                    <h2 class="product-name hidden-xs hidden-sm" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                    <h2 class="product-name hidden-md hidden-xl hidden-lg" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name_mobile']; ?></a></h2>
                  </div>
                    <?php $is_optionqty = 1; ?>
                    <?php if($product['options']) { ?>
                        <div class="options options-<?php echo $product['product_id']; ?>">
                            <?php foreach($product['options'] AS $option) { ?>
                                <?php $countoption = count($option['product_option_value']); ?>
                                <?php $price_tooltip_radio = 1; ?>
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                    <div class="radio">
                                        <?php if($countoption == 1) { ?>
                                            <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call1(<?php echo $product['product_id']; ?>,'related-product-container')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                        <?php } else { ?>
                                            <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call1(<?php echo $product['product_id']; ?>,'related-product-container')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                        <?php } ?>
                                        <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                            <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                        <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                        <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?></span>
                                        <?php } ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="options"></div>
                        <?php $is_optionqty = 0; ?>
                    <?php } ?>
                    
                  <div class="price-rating">
                    <div class="price-label">
                      <div class="box-price">
                          <?php if ($product['price']) { ?>
                            <?php if (!$product['special']) { ?>
                              <div class="price-box box-regular">
                                <span class="regular-price">
                                  <span class="price-new-<?php echo $product['product_id']; ?> price"><?php echo $product['price']; ?></span>
                                </span>
                              </div>
                            <?php } else { ?>
                              <div class="price-box box-special">
                                <p class="special-price"><span class="price-new-<?php echo $product['product_id']; ?> price"><?php echo $product['special']; ?></span></p>
                                <p class="price-old-<?php echo $product['product_id']; ?> old-price"><span class="price"><?php echo $product['price']; ?></span></p>
                              </div>
                            <?php } ?>
                             <?php if ($product['coupon_discount']) { ?>
                        <span class="price-coupon price-coupon-<?php echo $product['product_id']; ?>"><b> <?php echo $product['coupon_discount']; ?> </b></span><span> <?php echo $product['coupon'] ? 'Using code: <b>' . $product['coupon'] . '</b>' : ''; ?></span>
                    <?php } ?>
                          <?php } ?>
                      </div>
                    </div>
                  
                  </div>
                    <div class="add-to-cart" style="padding-top: 7px;">
                     <?php if($product['product_type'] != 5) { ?>
                            <?php if($is_optionqty != 1 && $product['quantity'] > 0) { ?>
                                <div style="width: 50%;">
                                    <input type="button" value="-" class="minus-button" onclick="minuss(<?php echo $product['product_id']; ?>,'related-product-container')" />
                                    <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>,'related-product-container')" />
                                    <input type="button" value="&#43;" class="plus-button" onclick="pluss(<?php echo $product['product_id']; ?>,'related-product-container')" />
                                </div>
                                <button type="button" class="hidden-md hidden-lg hidden-xl btn button button-cart cart-<?php echo $product['product_id']; ?>" onclick ="addtocart(<?php echo $product['product_id']; ?>,'related-product-container')"><i class="fa fa-shopping-cart"></i></button>
                                <button type="button" class="hidden-xs hidden-sm btn button button-cart cart-<?php echo $product['product_id']; ?>" onclick ="addtocart(<?php echo $product['product_id']; ?>,'related-product-container')"><?php echo $button_cart; ?></button>
                            <?php } else { ?>
                                <button class="hidden-md hidden-lg hidden-xl oos btn button" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                <button class="hidden-xs hidden-sm oos btn button" type="button" disabled>Stock Over!</button>
                            <?php } ?>
                        <?php } else { ?>
                            <?php if($product['quantity'] > 0) { ?>
                    <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
                  <?php } else { ?>
                    <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
                    <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                 <?php } ?>
                        <?php } ?>
                    </div>
                </div>
    
  </div>
</div>
</div>
<?php } ?>
</div>
</div>
</div>

<?php } ?>
<!--SOF Related Recipe-->

<!--EOF Related Recipe-->


        <!--reviews end--->
    <!-- <div class="container">

  <button id="" type="button" class="btn btn-default addrvw" data-toggle="collapse" data-target="#demo">Add Review</button></div> -->
  <!-- <div id="demo" class="collapse" style="width: 600px;">
    <form class="" id="">
          
          <div class="module-title" style="margin: 25px 0 5px 0 !important;">
            <?php if ($review_guest) { ?>
            <div class="form-group required">
            
              <div class="col-md-8 col-xs-12">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
              </div>
            </div>
            <div class="form-group required">
              <div class="col-md-8 col-xs-12">
                <label class="control-label" for="input-email">E-mail:</label>
                <input type="text" name="email" value="<?php echo $customer_email; ?>" id="input-email" class="form-control" />
              </div>
            </div>
            <div class="form-group required">
              <div class="col-md-8 col-xs-12">
                <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                <div class="help-block"><?php echo $text_note; ?></div>
              </div>
            </div>
            <div class="form-group required">
              <div class="col-md-8 col-xs-12">
               
         
                <div class="form-rating">
                  <label class="control-label"><?php echo $entry_rating; ?></label>
                  <div class="form-rating-container">
                    <input id="rating-5" type="radio" name="rating" value="5" />
                    <label class="fa fa-stack" for="rating-5">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-4" type="radio" name="rating" value="4" />
                    <label class="fa fa-stack" for="rating-4">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-3" type="radio" name="rating" value="3" />
                    <label class="fa fa-stack" for="rating-3">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-2" type="radio" name="rating" value="2" />
                    <label class="fa fa-stack" for="rating-2">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-1" type="radio" name="rating" value="1" />
                    <label class="fa fa-stack" for="rating-1">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                  </div>
                </div>
              
              </div>
            </div>
            <?php echo $captcha; ?>
            <div class="buttons clearfix">
              <div class="pull-left">
                <button type="button" id="button-review" class="btn btn-primary">Add Comment</button>
              </div>
            </div>
            <?php } else { ?>
            <?php echo $text_login; ?>
            <?php } ?>
          </form>
  </div> -->
</div>
        <!-- <?php if ($review_status) { ?>
        <div class="tab-pane"  style="margin-top: 20px;">
         <div class="col-lg-12 p0">
          <button id="addrvw" type="button" class="btn btn-default addrvw">Add Review</button></div>
          <form class="form-horizontal hidden" id="addrvwform">
           
          <div class="module-title" style="margin: 25px 0 5px 0 !important;">
            <?php if ($review_guest) { ?>
            <div class="form-group required">
            
              <div class="col-md-6 col-xs-12">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
              </div>
            </div>
            <div class="form-group required">
              <div class="col-md-6 col-xs-12">
                <label class="control-label" for="input-email">E-mail:</label>
                <input type="text" name="email" value="<?php echo $customer_email; ?>" id="input-email" class="form-control" />
              </div>
            </div>
            <div class="form-group required">
              <div class="col-md-6 col-xs-12">
                <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                <div class="help-block"><?php echo $text_note; ?></div>
              </div>
            </div>
            <div class="form-group required">
              <div class="col-md-6 col-xs-12">
               
         
                <div class="form-rating">
                  <label class="control-label"><?php echo $entry_rating; ?></label>
                  <div class="form-rating-container">
                    <input id="rating-5" type="radio" name="rating" value="5" />
                    <label class="fa fa-stack" for="rating-5">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-4" type="radio" name="rating" value="4" />
                    <label class="fa fa-stack" for="rating-4">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-3" type="radio" name="rating" value="3" />
                    <label class="fa fa-stack" for="rating-3">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-2" type="radio" name="rating" value="2" />
                    <label class="fa fa-stack" for="rating-2">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                    <input id="rating-1" type="radio" name="rating" value="1" />
                    <label class="fa fa-stack" for="rating-1">
                      <i class="fa fa-star fa-stack-1x"></i>
                      <i class="fa fa-star-o fa-stack-1x"></i>
                    </label>
                  </div>
                </div>
              
              </div>
            </div>
            <?php echo $captcha; ?>
            <div class="buttons clearfix">
              <div class="pull-left">
                <button type="button" id="button-review" class="btn btn-primary">Add Comment</button>
              </div>
            </div>
            <?php } else { ?>
            <?php echo $text_login; ?>
            <?php } ?>
          </form>
        </div>
        <?php } ?> -->

</div>

</div>

</div>





<!-- popup -->
           <?php if($related_products_count > 0) { ?>  
    <div class="modal fade" id="popup-product" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button> 
<?php if ($products) { ?>
<div class="product-popup quickview-product" id="product-popup">
  <div class="module-title"><span>You might be interested in</span></div>
  <div class="row">
    <div class="product-popup quickview-added qv-wtext">
        <?php $count = 0; ?>
      <?php foreach ($products as $product) { ?>
      <?php if($count < 2){ ?> 
      <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6" style="margin-bottom: 5px;">
        <div class="product-layout product-grid">
          <div class="product-thumb item-inner">
            <?php if($product['coupon_discount']){ ?> 
        <div class="deals-discount"><h5><?php echo round($product['discount']); ?> %<br>Off</h5></div>
      <?php } ?>
            <div class="images-container">
              <a href="<?php echo $product['href']; ?>">
                <?php if ($product['thumb']) { ?>
                
                <?php if($product['rotator_image']){ ?>
                <img class="image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" style="max-width: 70% !important;"/>
                <?php } ?>
                <?php } else { ?>
                <img src="image/cache/no_image-100x100.png" alt="<?php echo $product['name']; ?>" />
                <?php } ?>
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="image1" style="max-width: 70% !important;"/>
              </a>
          
          <?php if (isset($product['rating'])) { ?>
          <center>    <div class="ratings">
            <div class="rating-box">
              <?php for ($i = 0; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] == $i) {
              $class_r= "rating".$i;
              echo '<div class="'.$class_r.'">rating</div>';
            } 
          }  ?>
        </div>
      </div> </center>
      <?php } ?>
      
    </div>
    <div class="des-container">
                  <div class="name-wishlist">
                    <h2 class="product-name hidden-xs hidden-sm" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                    <h2 class="product-name hidden-md hidden-xl hidden-lg" ><a href="<?php echo $product['href']; ?>"><?php echo $product['name_mobile']; ?></a></h2>
                  </div>
                    <?php $is_optionqty = 1; ?>
                    <?php if($product['options']) { ?>
                        <div class="options options-<?php echo $product['product_id']; ?>">
                            <?php foreach($product['options'] AS $option) { ?>
                                <?php $countoption = count($option['product_option_value']); ?>
                                <?php $price_tooltip_radio = 1; ?>
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <?php if($option_value['quantity'] > 0) { $is_optionqty = 0; } ?>
                                    <div class="radio">
                                        <?php if($countoption == 1) { ?>
                                            <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call1(<?php echo $product['product_id']; ?>,'product-popup')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                        <?php } else { ?>
                                            <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call1(<?php echo $product['product_id']; ?>,'product-popup')" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                        <?php } ?>
                                        <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                            <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                        <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                        <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                            <span><?php echo $option_value['name']; ?></span>
                                        <?php } ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="options"></div>
                        <?php $is_optionqty = 0; ?>
                    <?php } ?>
                    
                  <div class="price-rating">
                    <div class="price-label">
                      <div class="box-price">
                          <?php if ($product['price']) { ?>
                            <?php if (!$product['special']) { ?>
                              <div class="price-box box-regular">
                                <span class="regular-price">
                                  <span class="price-new-<?php echo $product['product_id']; ?> price"><?php echo $product['price']; ?></span>
                                </span>
                              </div>
                            <?php } else { ?>
                              <div class="price-box box-special">
                                <p class="special-price"><span class="price-new-<?php echo $product['product_id']; ?> price"><?php echo $product['special']; ?></span></p>
                                <p class="price-old-<?php echo $product['product_id']; ?> old-price"><span class="price"><?php echo $product['price']; ?></span></p>
                              </div>
                            <?php } ?>
                             <?php if ($product['coupon_discount']) { ?>
                        <span class="price-coupon price-coupon-<?php echo $product['product_id']; ?>"><b> <?php echo $product['coupon_discount']; ?> </b></span><span> <?php echo $product['coupon'] ? 'Using code: <b>' . $product['coupon'] . '</b>' : ''; ?></span>
                    <?php } ?>
                          <?php } ?>
                      </div>
                    </div>
                  
                  </div>
                    <div class="add-to-cart" style="padding-top: 7px;">
                     <?php if($product['product_type'] != 0) { ?>
                            <?php if($is_optionqty != 1 && $product['quantity'] > 0) { ?>
                                <div style="width: 50%;">
                                    <input type="button" value="-" class="minus-button" onclick="minuss(<?php echo $product['product_id']; ?>,'product-popup')" />
                                    <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>,'product-popup')" />
                                    <input type="button" value="&#43;" class="plus-button" onclick="pluss(<?php echo $product['product_id']; ?>,'product-popup')" />
                                </div>
                                <button type="button" class="hidden-md hidden-lg hidden-xl btn button button-cart cart-<?php echo $product['product_id']; ?>" onclick ="addtocart(<?php echo $product['product_id']; ?>,'product-popup')"><i class="fa fa-shopping-cart"></i></button>
                                <button type="button" class="hidden-xs hidden-sm btn button button-cart cart-<?php echo $product['product_id']; ?>" onclick ="addtocart(<?php echo $product['product_id']; ?>,'product-popup')"><?php echo $button_cart; ?></button>
                            <?php } else { ?>
                                <button class="hidden-md hidden-lg hidden-xl oos btn button" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                                <button class="hidden-xs hidden-sm oos btn button" type="button" disabled>Stock Over!</button>
                            <?php } ?>
                        <?php } else { ?>
                            <?php if($product['quantity'] > 0) { ?>
                    <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
                  <?php } else { ?>
                    <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
                    <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
                 <?php } ?>
                        <?php } ?>
                    </div>
                </div>
    
  </div>
</div>
</div>
<?php } ?>
 <?php $count++; ?>
<?php } ?>
</div>
</div>
</div>
<?php } ?> 
                        
                    </div>
                    </div>
                </div>
            </div>
 <?php } ?>                
<!-- popup -->

<!-- popup video-->

    <div class="videopopup">
        <iframe width="560" height="315" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <a class="closeBtn"><span>Close</span></a> 
  </div>


<script>
    $(document).ready(function () {
      var selected = [];
      var price = 0
 
        
        $("input[type=checkbox]").change(function () {
              var total = 0;
              $('input:checkbox:checked').each(function(){ // iterate through each checked element.
                total += isNaN(parseInt($(this).attr('id'))) ? 0 : Math.round($(this).attr('id'));
              });     
              $("#proids").text(total);
              $('#checkall_button').removeAttr("disabled");
        
        });

    }); 
</script>

  <script type="text/javascript">
    function submittocart(pids,id) {
        var productss = pids.split(',');
        var option_not_checked = 0;
        
        var products = [];
        
        $("input:checkbox[class=checkbox_check]:checked").each(function(){
            products.push($(this).val());
        });
        
        //alert(productss);

        for (i = 0; i < products.length; i++) {
            addtocart(products[i],id);
        }/*
        setTimeout(function(){
            if($("#prodcarousel4 div").hasClass('text-danger')) {
                alert("Please select weight to proceed.");
            }
        }, 1000);*/
    }
</script>  
 <!-- popup video--> 
    <?php if ($videos) { ?>
        <script type="text/javascript">
            $(".videos-carousel").owlCarousel({
                autoPlay: true,
                items : 3, 
                slideSpeed : 1000, 
                navigation: true,
                paginationNumbers : false,
                pagination: false, 
                goToFirstSpeed : 1500,
                itemsDesktop : [1199,3],
                itemsDesktopSmall : [991,3],
                itemsTablet: [700,2],
                itemsMobile : [480,1]
            });
        </script>
    <?php } ?>

    <script type="text/javascript">
        function traceability() {
            var url = 'https://www.true-elements.in/scratchpad/dashboard';
            var hw_pid = $('.checked-option input[type=\'radio\']:checked').data('pid');
            if(parseInt(hw_pid) > 0) {
                url += '?pid=' + hw_pid;
            }
            window.open(url,'_blank');
        }
    </script>

    <script type="text/javascript"><!--
        $('.des-container .options .radio input[type="radio"]').click(function() {
            $('.des-container .options .radio input[type="radio"]').each(function() {
                $(this).parents("label").toggleClass('checked-option', this.checked);
            });
        });
    
        $(document).ready(function() {
            var bottom_cart_height = $('#cart_section').height();
            $('footer').css('margin-bottom', bottom_cart_height + 'px');
        });
    </script>

    <script type="text/javascript">
        function addtocart(product_id, id) {
            $('input[name=\'product_id\']').val(product_id);
            $.ajax({
                url: 'index.php?route=checkout/cart/add',
                type: 'post',
                data: $('input[name=\'product_id\'], #' + id +' .quantity-' + product_id + ', #' + id +' .checked-option .option-' + product_id),
                dataType: 'json',
                success: function(json) {
                    if (json['error']) {
                        if (json['error']['option']) {
                            if($(".text-danger." + product_id).length == 0) {
                                $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                            }
                        }
                    }
                    if (json['success']) {
                        $('.close').click();
                        // Added to cart
                        var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';
    
                        var n = new Noty({
                            type: 'success',
                            layout: json['notice_add_layout'],
                            text:  success_text ,
                            textAlign:"right",
                            animateOpen:{"height":"toggle"},
                            timeout: json['notice_add_timeout'],
                            progressBar: false,
                            closeWith: 'button',
                        }).show();
                        // Added to cart
                        $('#cart-total').html(json['total']);
                        $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        
        function minuss(product_id, id) {
            var currentval = parseInt($(".quantity-" + product_id).val());
            $("#" + id + " .quantity-" + product_id).val(currentval-1);
            if($("#" + id + " .quantity-" + product_id).val() <= 0) {
                $("#" + id + " .quantity-" + product_id).val(1);
            }
        }
  
        function pluss(product_id, id) {
            var currentval = parseInt($("#" + id + " .quantity-" + product_id).val());
            $("#" + id + " .quantity-" + product_id).val(currentval+1);
        }
    
        function quantityVerify(product_id, id) {
            var currentval = parseInt($("#" + id + " .quantity-" + product_id).val());
            if(currentval <= 0) {
               $("#" + id + " .quantity-" + product_id).val(1); 
            }
        }
    </script>
    
    <script type="text/javascript">
        function price_with_options_ajax_call1(product_id, id) {
            $(".text-danger." + product_id).remove();
            $.ajax({
                type: 'POST',
                url: 'index.php?route=product/live_options/index&product_id=' + product_id,
                data: $('#'+id+' .checked-option .option-' + product_id),
                dataType: 'json',
                success: function(json) {
                    if (json.success) {
                        $('#'+id+' .price-new-' + product_id).html(json.new_price.special);
                        $('#'+id+' .price-old-' + product_id).html('<span class="price">' + json.new_price.price + '</span>');
          
                          if(json.new_price.coupon) {
                               $('#'+id+' .price-new-' + product_id).html(json.new_price.coupon);
                          }
          
                          if(json.new_price.special) {
                              $('#'+id+' .price-new-' + product_id).html(json.new_price.special);
                              $('#'+id+' .price-old-' + product_id).html('<span class="price">' + json.new_price.price + '</span>');
                          } else {
                              $('#'+id+' .price-new-' + product_id).html(json.new_price.price);
                          }
                    }
                },
                error: function(error) {
                    console.log('error: '+error);
                }
            });
        }
  
        $(document).ready(function() {
            $(".product-options").each(function () {
                if ($(this)[0].length == 0) {
                    $(this).hide();
                }
            });
      });
      
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("input[type=\'radio\']:disabled").parent().css({"background" : "#ddd", "cursor" : "not-allowed"});
            $("input[type=\'radio\']:disabled").next("span").css({"user-select" : "none"});
            });
    </script>
    
    <script type="text/javascript">
        $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
            $.ajax({
                url: 'index.php?route=product/product/getRecurringDescription',
                type: 'post',
                data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
                dataType: 'json',
                beforeSend: function() {
                    $('#recurring-description').html('');
                },
                success: function(json) {
                    $('.alert, .text-danger').remove();

                    if (json['success']) {
                      $('#recurring-description').html(json['success']);
                    }
                }
            });
        });
  </script>
  
    <script type="text/javascript"><!--
        $('#button-cart').on('click', function() {
            $.ajax({
                url: 'index.php?route=checkout/cart/add',
                    type: 'post',
                    data: $('input[name=\'product_id\'], #input-quantity, #product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                    dataType: 'json',
                        beforeSend: function() {
                        $('#button-cart').button('loading');
                    },
                    complete: function() {
                      $('#button-cart').button('reset');
                    },
                    success: function(json) {
                        console.log(json);
                        $('.alert').remove();
                        $('.form-group').removeClass('has-error');
                        $('#product .text-danger').removeClass('text-danger');

                        if (json['error']) {
                            if (json['error']['option']) {
                                for (i in json['error']['option']) {
                                    var element = $('#product #input-option' + i.replace('_', '-'));

                                    if (element.parent().hasClass('input-group')) {
                                      element.parent().find('label.control-label').html('<span class="text-danger">' + json['error']['option'][i] + '</span>');
                                    } else {
                                      element.parent().find('label.control-label').html('<span class="text-danger">' + json['error']['option'][i] + '</span>');
                                    }
                                }
                            }

                            if (json['error']['recurring']) {
                              $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                            }

                            // Highlight any found errors
                            $('.text-danger').parent().addClass('has-error');
                    }

                    if (json['success']) {
                        var related_products_count = '<?php echo $related_products_count; ?>';
                        if(related_products_count > 0) {
                            $("#popup-product").modal();
                        }
                        //$('body').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        // Added to cart
                        var success_text = '<div style="margin: 0 0 10px 0">\
                            <div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div>\
                            <div  style="text-align: center" >' + json['success'] + '</div>\
                        </div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);

                     //$('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    </script>
    
    <script type="text/javascript">
        $('#button-cart-bottom').on('click', function() {
            $.ajax({
                 url: 'index.php?route=checkout/cart/add',
                    type: 'post',
                    data: $('input[name=\'product_id\'], #input-quantity-bottom, .cart_details input[type=\'text\'], .cart_details input[type=\'hidden\'], .cart_details input[type=\'radio\']:checked, .cart_details input[type=\'checkbox\']:checked, .cart_details select, .cart_details textarea'),
                    dataType: 'json',
                    beforeSend: function() {
                      $('#button-cart-bottom').button('loading');
                    },
                    complete: function() {
                      $('#button-cart-bottom').button('reset');
                    },
                    success: function(json) {
                      $('.alert').remove();
                      $('.form-group').removeClass('has-error');
                      $('.cart_details .text-danger').removeClass('text-danger');

                    if (json['error']) {
                        if (json['error']['option']) {
                            for (i in json['error']['option']) {
                                var element = $('.cart_details #input-option' + i.replace('_', '-'));

                                if (element.parent().hasClass('input-group')) {
                                  element.parent().find('label.control-label').html('<span class="text-danger">' + json['error']['option'][i] + '</span>');
                                } else {
                                  element.parent().find('label.control-label').html('<span class="text-danger">' + json['error']['option'][i] + '</span>');
                                }
                            }
                        }

                        if (json['error']['recurring']) {
                          $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    }

                    if (json['success']) {
                        var related_products_count = '<?php echo $related_products_count; ?>';
                        if(related_products_count > 0) {
                            $("#popup-product").modal();
                        }
                        //$('body').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        // Added to cart
                        var success_text = '<div style="margin: 0 0 10px 0">\
                            <div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div>\
                            <div  style="text-align: center" >' + json['success'] + '</div>\
                        </div>';

                        var n = new Noty({
                            type: 'success',
                            layout: json['notice_add_layout'],
                            text:  success_text ,
                            textAlign:"right",
                            animateOpen:{"height":"toggle"},
                            timeout: json['notice_add_timeout'],
                            progressBar: false,
                            closeWith: 'button',
                        }).show();
                        // Added to cart
                        $('#cart-total').html(json['total']);

                        //$('html, body').animate({ scrollTop: 0 }, 'slow');

                        $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                     alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    </script>
    
    <script type="text/javascript"><!--
        $('.date').datetimepicker({
            pickTime: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });

        $('.time').datetimepicker({
            pickDate: false
        });

        $('button[id^=\'button-upload\']').on('click', function() {
            var node = this;

            $('#form-upload').remove();

            $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

            $('#form-upload input[name=\'file\']').trigger('click');

            if (typeof timer != 'undefined') {
            clearInterval(timer);
            }

            timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(node).button('loading');
                    },
                    complete: function() {
                        $(node).button('reset');
                    },
                    success: function(json) {
                        $('.text-danger').remove();

                    if (json['error']) {
                        $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        alert(json['success']);
                        $(node).parent().find('input').attr('value', json['code']);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
          }
        }, 500);
      });
      //--></script>
    <script type="text/javascript">
        $('#review').load('index.php?route=product/product/reviewsSummary&product_id=<?php echo $product_id; ?>');

        $('#button-review').on('click', function() {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),

             complete: function() {
              $('#button-review').button('reset');
            },

            success: function(json) {
              $('.alert-success, .alert-danger').remove();

              if (json['error']) {
                $('.help-block').after('<div class="alert alert-danger reviewerror"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
              }

              if (json['success']) {
                $('.help-block').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                $('input[name=\'name\']').val('');
                $('input[name=\'email\']').val('');
                $('textarea[name=\'text\']').val('');
                $('input[name=\'rating\']:checked').prop('checked', false);

              }
            }
        });
        });

        var minimum = <?php echo $minimum; ?>;
        $("#input-quantity, #input-quantity-bottom").change(function(){
          if ($(this).val() < minimum) {
            $(this).val(minimum);
          }
        });
  
        // increase number of product
        function minus(minimum){
            var currentval = parseInt($("#input-quantity").val());
            $("#input-quantity").val(currentval-1);
            if($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum){
              alert("Minimum Quantity: "+minimum);
              $("#input-quantity").val(minimum);
            }
        };
    
         // decrease of product
        function plus(){
            var currentval = parseInt($("#input-quantity").val());
            $("#input-quantity").val(currentval+1);
        };
        
        $('#minus').click(function(){
            minus(minimum);
        });
        
        $('#plus').click(function(){
            plus();
        });
  
        // Bottom Section
        // increase number of product
        function minusBottom(minimum){
            var currentval = parseInt($("#input-quantity-bottom").val());
            $("#input-quantity-bottom").val(currentval-1);
            if($("#input-quantity-bottom").val() <= 0 || $("#input-quantity-bottom").val() < minimum){
                $("#input-quantity-bottom").val(minimum);
            }
        };
    
        // decrease of product
        function plusBottom(){
            var currentval = parseInt($("#input-quantity-bottom").val());
            $("#input-quantity-bottom").val(currentval+1);
        };
        $('#minus-bottom').click(function(){
            minusBottom(minimum);
        });
        $('#plus-bottom').click(function(){
            plusBottom();
        });
  
  
        // zoom
        $(".thumbnails img").elevateZoom({
            zoomType : "window",
            cursor: "crosshair",
            gallery:'gallery_01', 
            galleryActiveClass: "active", 
            imageCrossfade: true,
            responsive: true,
            zoomWindowOffetx: 0,
            zoomWindowOffety: 0,
        });
        
      // slider  
        $(".image-additional").owlCarousel({
            loop: true,
            navigation: true,
            pagination: false,
            slideSpeed : 500,
            goToFirstSpeed : 1500,
            addClassActive: true,
            autoHeight: true,
            items : 3, 
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [767,2],
            itemsMobile : [480,4],
            addClassActive: true,
        
            afterInit: function(){
                $('#gallery_01 .owl-wrapper .owl-item:first-child').addClass('active');
            },
            beforeInit: function(){
                $(".image-additional .thumbnail").show();
            }
        }); 
    
        $('#gallery_01 .owl-item .thumbnail').each(function(){
            $(this).click(function(){
                $('#gallery_01 .owl-item').removeClass('active2');
                $(this).parent().addClass('active2');
            });
        });
        
        // related products  
        $(".related-product").owlCarousel({
            navigation: true,
            pagination: false,
            slideSpeed : 500,
            goToFirstSpeed : 1500,
            items : 4, 
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [767,2],
            itemsTablet: [640,2],
            itemsMobile : [480,2],
            addClassActive: true,
            afterAction: function(el){
                this.$owlItems.removeClass('before-active')
                this.$owlItems.removeClass('last-active')
                this.$owlItems .eq(this.currentItem).addClass('before-active')  
                this.$owlItems .eq(this.currentItem + (this.owl.visibleItems.length - 1)).addClass('last-active') 
            }
        });

        $(".related-product1").owlCarousel({
            navigation: true,
            pagination: false,
            slideSpeed : 500,
            goToFirstSpeed : 1500,
            items : 4, 
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [767,2],
            itemsTablet: [640,2],
            itemsMobile : [480,1],
            addClassActive: true,
            afterAction: function(el){
                this.$owlItems.removeClass('before-active')
                this.$owlItems.removeClass('last-active')
                this.$owlItems .eq(this.currentItem).addClass('before-active')  
                this.$owlItems .eq(this.currentItem + (this.owl.visibleItems.length - 1)).addClass('last-active') 
            }
        });
 //--></script>

    <script type="text/javascript">
        $(".itemcarousel").owlCarousel({
            navigation: true,
            pagination: false,
            slideSpeed : 500,
            goToFirstSpeed : 1500,
            items : 4, 
            itemsDesktop : [1199,4],
        });
    </script>

    <script type="text/javascript">
        $(".thumbcarousel").owlCarousel({
            navigation: true,
            pagination: false,
            slideSpeed : 500,
            goToFirstSpeed : 1500,
            items : 1, 
            itemsDesktop : [1199,1],
        });
 </script>

    <script type="text/javascript"><!--
        $('#product input[type="radio"], #product input[type="checkbox"]').click(function() {
                $('#product input[type="radio"], #product input[type="checkbox"]').each(function() {
                $(this).parents("label").toggleClass('checked-option', this.checked);
            });
         });
  //--></script>
  
  <!--- POSTCODE --->     
    <style>
        #msg1 p{margin:0;}
        #msg1{color:#000}
        #msg1 .fa-2x{font-size:2em!important;}
    </style>
    
    <script type="text/javascript">
        $('#pincheck1').submit(function (e) {
            e.preventDefault();
            getdata1();
        });
        
        function getdata1(){
            var pin = $("#pin1").val();
            if(pin != ''){
                $.ajax({
                type: "POST",
                url: "?route=product/product/pinCheck",
                data: {pincode: pin},
                dataType : "text"
                }).done(function( result ) 
                {
                    $("#msg1").show();
                    $("#msg1").html( result );
                    $("#pincheck1").hide();
                    $("#show_message").hide();
                });
            } else{
                $("#show_message").show();
            }
        }
        
        function showform1(){
            $("#msg1").hide();
            $("#pincheck1").show();
            }
    </script>
    <!--- POSTCODE --->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '355421528440199');
        fbq('track', 'AddToCart');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=355421528440199&ev=AddToCart&noscript=1"
      /></noscript>
      <!-- End Facebook Pixel Code -->
     
    <script type="text/javascript">
        $('.rating-box').click(function() {
            $("html, body").animate({
                scrollTop: $("#form-review").offset().top - 250
            }, 1000);
        });
    </script>
    
<!--video popup-->
    <script type="text/javascript">
        $('a.videoLink').click(function(){
          var thsRel = $(this).attr('rel');
          $('.videopopup iframe').attr('src',thsRel);
          $('.videopopup').fadeIn();
        });
        
        $('.videopopup a.closeBtn').click(function(){
          $('.videopopup iframe').attr('src','');
          $('.videopopup').fadeOut();
        });
          
        $('.rating-box').click(function() {
            $("html, body").animate({
                scrollTop: $("#form-review").offset().top - 250
            }, 1000);
        });
    </script>
   
<!---reviews--->
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>


<script type="text/javascript">
  const swiper = new Swiper('.swiper-container', {
  effect: 'coverflow',
  centeredSlides: true,
  slidesPerView: 1,
  loop: true,
  speed: 600,
  
  autoplay: {
    delay: 3000,
  },
  
  coverflowEffect: {
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows: true,
  },
  
  breakpoints: {
    320: {
      slidesPerView: 1,
    },
    560: {
      slidesPerView: 3,
    },
    990: {
      slidesPerView: 3,
    }
  },

  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },

  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js" integrity="sha512-+m6t3R87+6LdtYiCzRhC5+E0l4VQ9qIT1H9+t1wmHkMJvvUQNI5MKKb7b08WL4Kgp9K0IBgHDSLCRJk05cFUYg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src='https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js'></script>


 
<script type="text/javascript">
  $(".carousel").swipe({
    excludedElements: "input, select, textarea, .noSwipe",
    swipeLeft: function() {
      $(this).carousel('next');
    },
    swipeRight: function() {
      $(this).carousel('prev');
    },
    allowPageScroll: 'vertical'
  });
</script>
<script>
    $(document).ready(function() {
        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
    
            fixedContentPos: false
        });
    });
</script>

<script type="text/javascript">

  $(function(){
  $('.slider-thumb').slick({
    vertical: true,
    infinite: true,
    adaptiveHeight: false, 
    slidesPerRow: 5,
    slidesToShow: 5,
    asNavFor: '.slider-preview',
    focusOnSelect: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-up"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-down"></i></button>',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesPerRow: 3,
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 479,
        settings: {
          slidesPerRow: 3,
          slidesToShow: 3,
        }
      },
    ]
  });
  $('.slider-preview').slick({
    autoplay: true,
    infinite: true,
    centerMode: true,
    adaptiveHeight: false,
    slidesPerRow: 1,
    slidesToShow: 1,
    asNavFor: '.slider-thumb',
    arrows: false,
    draggable: false,
    fade: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          vertical: false,
          fade: true,
        }
      },
        {
        breakpoint: 480,
         settings: {
          slidesToShow: 1,
          dots: true,
          //centerPadding: '20px',
        }
      }
    ]
  });
});

</script>
<script>
function imageZoom(imgID, resultID) {
  var img, lens, result, cx, cy;
  img = document.getElementsByClassName(imgID)[0];
  result = document.getElementsByClassName(resultID)[0];
  /*create lens:*/
  lens = document.createElement("DIV");
  lens.setAttribute("class", "img-zoom-lens");
  
  /*insert lens:*/
  img.parentElement.insertBefore(lens, img);
  /*calculate the ratio between result DIV and lens:*/
  cx = result.offsetWidth / lens.offsetWidth;
  cy = result.offsetHeight / lens.offsetHeight;
  /*set background properties for the result DIV:*/
  result.style.backgroundImage = "url('" + img.src + "')";
  result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
  /*execute a function when someone moves the cursor over the image, or the lens:*/
  lens.addEventListener("mousemove", moveLens);
  img.addEventListener("mousemove", moveLens);
  /*and also for touch screens:*/
  lens.addEventListener("touchmove", moveLens);
  img.addEventListener("touchmove", moveLens);
  function moveLens(e) {
    var pos, x, y;
    /*prevent any other actions that may occur when moving over the image:*/
    e.preventDefault();
    /*get the cursor's x and y positions:*/
    pos = getCursorPos(e);
    /*calculate the position of the lens:*/
    x = pos.x - (lens.offsetWidth / 2);
    y = pos.y - (lens.offsetHeight / 2);
    /*prevent the lens from being positioned outside the image:*/
    if (x > img.width - lens.offsetWidth) {x = img.width - lens.offsetWidth;}
    if (x < 0) {x = 0;}
    if (y > img.height - lens.offsetHeight) {y = img.height - lens.offsetHeight;}
    if (y < 0) {y = 0;}
    /*set the position of the lens:*/
    lens.style.left = x + "px";
    lens.style.top = y + "px";
    /*display what the lens "sees":*/
    result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
  }
  function getCursorPos(e) {
    var a, x = 0, y = 0;
    e = e || window.event;
    /*get the x and y positions of the image:*/
    a = img.getBoundingClientRect();
    /*calculate the cursor's x and y coordinates, relative to the image:*/
    x = e.pageX - a.left;
    y = e.pageY - a.top;
    /*consider any page scrolling:*/
    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return {x : x, y : y};
  }
}
</script>
<script>
// Initiate zoom effect:
imageZoom("myimage", "myresult");
</script>

<script type="text/javascript">
 /*$(function() {
  $("#bars li .bar").each( function( key, bar ) {
    var percentage = $(this).data('percentage');
    
    $(this).animate({
      'height' : percentage + '%'
    }, 1000);
  });


});*/
$(document).ready(function(){
  
  $(window).scroll(function(){
    if ($(this).scrollTop() > 1600) 
    {
    console.log('hello');
     $("#bars li .bar").each( function( key, bar ) {
    var percentage = $(this).data('percentage');
    
    $(this).animate({
      'height' : percentage + '%'
    }, 1000);
  });
    } 
    else 
    {
     console.log('bie');
    }
  });
});
</script>
<script type="text/javascript">
 
$(document).ready(function(){
  
  $(window).scroll(function(){
    if ($(this).scrollTop() < 500) 
    {
    console.log('dhanashri');
     document.getElementById("cart_section").style.display = 'none';
    } 
    else 
    {
     console.log('wagh');
     document.getElementById("cart_section").style.display = 'block';
    }
  });
});
</script>




            <script type="text/javascript" src="index.php?route=product/live_options/js&product_id=<?php echo $product_id; ?>"></script>
            

         <div id="offerPopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo $offerclose; ?></button>
      </div>
    </div>

  </div>
</div>
         

         <script type="text/javascript">
  function openOfferPopup(id) {
      $.ajax({
      url: 'index.php?route=offers/salescombopge/popp',
      type: 'post',
      dataType: 'json',
      data: {"page_id" : id} ,
      success: function(json) {
        if(json.html != undefined) {
          if(json.html.title != undefined) {
            $('#offerPopup .modal-title').html(json.html.title);
          }
          if(json.html.description != undefined) {
            $('#offerPopup .modal-body').html(json.html.description);
          }
          $('#offerPopup').modal('show'); 
        } 
      }
    });
  }
</script>
         
<?php echo $footer; ?>

        <input type='hidden' id='fbProductID' value='<?= $product_id ?>' />
      
