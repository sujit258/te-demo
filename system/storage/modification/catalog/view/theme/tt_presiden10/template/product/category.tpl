<style>
body {
    font-size: 15px;
}



@media screen and (max-width: 500px) {
     .pro_img {width: 58%;display: inline;}
     .sub-cat{ font-size:11px!important;font-weight: bold; }
     .w3-btn {
        margin-bottom: 5px;
        margin-top: 5px;
    }
}

@media screen and (min-width: 501px) {
    .sub-cat{ font-size:12px!important;font-weight: bold; }
}


.w3-btn {margin-bottom:10px;margin-top: 10px;}
.w3-border-red,.w3-hover-border-red:hover{border-color:#c6c6c6!important}
.w3-btn,.w3-button{border:none;display:inline-block;padding:8px 8px;vertical-align:middle;overflow:hidden;text-decoration:none;color:inherit;background-color:inherit;text-align:center;cursor:pointer;white-space:nowrap}
.w3-btn:hover{box-shadow:0 8px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)}
.w3-btn,.w3-button{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}  
.w3-white,.w3-hover-white:hover{color:#000!important;background-color:#fff!important}
.w3-border-0{border:0!important}.w3-border{border:1px solid #c6c6c6!important}
.w3-border-top{border-top:1px solid #c6c6c6!important}.w3-border-bottom{border-bottom:1px solid #ccc!important}
.w3-border-left{border-left:1px solid #c6c6c6!important}.w3-border-right{border-right:1px solid #ccc!important}
.w3-border-red,.w3-hover-border-red:hover{border-color:#c6c6c6!important;}
.w3-round-large{border-radius:0px}
.refine-search a {
    color: #555;
    display: block;
}

 @import url(https://fonts.googleapis.com/css?family=Open+Sans:300,800); .content { width: 100%; padding: 20px; margin: 0 auto; } .centerplease { margin: 0 auto; max-width: 270px; font-size: 40px; } .question { color:#000; position: relative; background:#fcfcfc; margin: 0; padding: 10px 10px 10px 50px; display: block; width:100%; cursor: pointer; border: 2px solid #fcfcfc; } .answers { background: #fcfcfc; font-color:#000; font-size:15px; padding: 0px 15px; margin: 5px 0; height: 0; overflow: hidden; z-index: -1; position: relative; opacity: 0; -webkit-transition: .7s ease; -moz-transition: .7s ease; -o-transition: .7s ease; transition: .7s ease; } .questions:checked ~ .answers{ height: auto; opacity: 1; padding: 15px; } .plus { position: absolute; margin-top: 10px; margin-left: 10px; z-index: 5; font-size: 2em; line-height: 100%; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none; -webkit-transition: .3s ease; -moz-transition: .3s ease; -o-transition: .3s ease; transition: .3s ease; } .questions:checked ~ .plus { -webkit-transform: rotate(45deg); -moz-transform: rotate(45deg); -o-transform: rotate(45deg); transform: rotate(45deg); } .questions { display: none; } 
.answers p {
     text-align: justify!important;
 }
    .options {
        min-height: 55px;
    }
    .product-name {
        min-height: 52px;
        padding-right: 0!important;
        padding-top: 10px!important;
    }
    .product-options {
        margin-top: 10px;
        text-align: center;
        float: left;
        background: #fff!important;
    }
    .button-cart {
        float: right!important;
        font-size: 13px!important;
        padding: 8px 10px!important;
    }
    .button-personalize {
        font-size: 13px!important;
        padding: 8px 10px!important;
        float: right!important;
    }
    .price-label {
        padding: 8px 0px!important;
        width: 100%!important;
        min-height: 40px!important;
    }
    .quantity {
        height: 35px!important;
        width: 60%!important;
        text-align: center;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    .minus-button, .plus-button {
        font-weight: 700;
        text-align: center;
        height: 35px!important;
        width: 20%!important;
        font-size: 14px!important;
        padding: 0px!important;
        background: #fff!important;
        border: 1px solid #f2f2f2!important;
        float: left;
    }
    
    .minus-button:hover, .plus-button:hover {
        background: #f49a25!important;
        color: #fff!important;
        border: 1px solid #f2f2f2!important;
    }
    
    .minus-button {
        border-top-left-radius: 4px!important;
        border-bottom-left-radius: 4px!important;
    }
    
    .plus-button {
        border-top-right-radius: 4px!important;
        border-bottom-right-radius: 4px!important;
    }
    .personalized-product {
        min-height: 330px!important;
    }
    @media(max-width: 1000px) {
        .button-cart, .button-personalize, .oos {
            padding: 7px 10px!important;
            font-size: 14px!important;
            margin-bottom: 15px!important;
        }
        .personalized-product {
            min-height: 230px!important;
        }
        .quantity-container {
            width: 60%!important;
        }
        .radio label span {
            font-size: 12px!important;
        }
        .des-container .options .radio {
            margin: 0px 0px 5px -2px!important;
        }
        .options {
            min-height: 55px!important;
        }
        .category-products {
            padding: 0!important;
        }
        .product-container {
            padding: 7px!important;
        }
        .mobile {
            min-height: 505px!important;
        }
    }
    .des-container {
        padding: 0!important;
    }
    @media(min-width: 1000px) {
        .des-container {
            min-height: 200px;
        }
    }
    .oos {
        background: #777!important;
        border-radius: 4px!important;
        float: right!important;
        font-size: 13px!important;
        padding: 8px 10px!important;
    }
    .oos:hover {
        cursor: not-allowed!important;
    }
    .button-cart:focus, .button-personalize:focus {
        color: #fff!important;
    }
    .deals-overlay {
        right: 0px;
        top: 10px;
        position: absolute;
        background:#f49a25;
        color:#fff;
        padding: 4px;
        font-weight: 600;
        width: 25%;
        text-align: center;
        z-index: 1;
    }
    .button-cart-mobile, .btn-oos-mobile {
        font-size: 23px!important;
        padding: 9px 10px!important;
        margin-top: 0;
    }
    .button-cart-mobile:focus, .btn-oos-mobile:focus {
        outline: none;
    }
    .mobile {
        margin-left: 0px;
        margin-bottom: 10px;
        min-height: 340px;
    }
    .personalized {
        min-height: 1px!important;
    }
    .des-container .options .radio input {
        display: none !important;
    }
    .des-container .options .radio {
        display: inline-block;
        margin: 0px 5px 5px 0px;
    }
    .des-container .options label {
        display: block;
        background-color: #fff;
        border: 0.5px solid #000;
        border-radius: 3px;
        padding: 0;
        text-align: center;
        min-width: 52px;
    }
    .des-container .options .checked-option {
        background-color: #000 !important;
        color: #fff !important;
        border: 1px solid #000!important;
    }
    .radio label span {
        display: block;
        font-size: 13px;
    }
    label.disabled {
        background: #ddd!important;
        cursor: not-allowed!important;
    }
    .price, .coupon {
        padding-left: 0px!important;
        padding-right: 0px!important;
    }
    .coupon {
        min-height: 20px!important;
        text-align: left;
        font-size: 12px!important;
        margin-bottom: 0!important;
        color: #000!important;
    }
    .breadcrumb {
        margin-bottom: 15px!important;
    }
    .image1, .image2 {
        padding: 5%!important;
    }
     @media(min-width:741px){ 
         #bf-brainyfilter-anchor{
            margin-top: 35%!important;
        }  
        .options {
            min-height: 55px !important;
        }
   }
   
   @media(max-width:740px){ 
        .bf-btn-show{
            margin-top: 106px!important;
        }
        .options {
            min-height: 90px !important;
        }
   } 
   
   .des-container {
         min-height: 250px !important;
    }
    .product-name {
        min-height: 73px !important;
    }
    
    .deals-discount {
        border-radius: 50%;
        right: 0px;
        top: 5%;
        position: absolute;
        background: #f49a25;
        text-align: center;
        z-index: 5;
        width: 50px;
    }
    
    .deals-discount h5 {
        color: #fff;
        font-weight: 600;
        padding: 1px;
    }
    
    .price-coupon{
        font-weight: bold;
    }
  .col-sm-12{
  width: 100% !important;
  }
</style>
<!-- <style type="text/css">
      .jumbotron{
        padding-top: 10px !important;
        padding-bottom: 10px !important;
        background-color: #fff !important;
      }
      
      .img-container{
        width: 170px;
        padding: 15px;
        display: inline-block;
      }

      .img-container:hover{

         transform: scale(1.1, 1.1);
         transition: all 0.6s ease;
      }
      .img-container img{
        width: 150px;
        border-radius: 15px;
        border: 1px solid #404040;
      }

      .img-container img:hover{
        border: 4px solid #f49a25;
      }

      .img-container h5{
        font-size: 15px !important;
      }

      .img-container h5:hover{
        color: #f49a25;
      }
    </style> -->
<?php echo $header; ?>
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>

<?php if($category_id == '621') { ?>
    <style>
        .nav > li > a:focus, .nav > li > a:hover {
                color:#000;
        }
        .nav-pills li {
            text-align: center;
            margin-top: 10px;
        }
        .nav-pills > li + li {
            margin-left: 0!important;
        }
        .nav a {
            font-weight: 600;
        }
        .nav a span {
            vertical-align: middle;
        }
        .nav li a {
            padding: 15px 10px;
            font-size: 18px;
        }
        .option-banner {
            margin-top: 15px;
        }
        .nav-pills li.active a {
            color: #fff!important;
            background-color: #f49a25!important;
        }
        .nav-pills li a {
            color: #fff;
            border: 1px solid #f49a25;
            background: #000;
        }
        .select-types {
            margin-top: 5px;
            margin-bottom: 15px;
        }
    </style>
     
    
    
            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 2%;padding-top: 2%;">
                <img class="p0 img-responsive hidden-xs hidden-sm" src="image/catalog/Banner/mms-mmm-desktop.jpg">
                <img class="p0 img-responsive hidden-md hidden-lg hidden-xl" src="image/catalog/Banner/mms-mmm-mobile.jpg">
            </div>
        <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 select-types" role="tabpanel">
                <ul class="nav nav-pills nav-fill" role="tablist">
                    <li class="nav col-md-4 col-sm-1 col-xs-1"></li>
                    <li role="presentation" class="nav col-md-2 col-sm-5 col-xs-5">
                        <a href="/index.php?route=product/make_my_muesli" ><span>Make My Muesli</span></a>
                    </li>
                    <li role="presentation" class="nav col-md-2 col-sm-5 col-xs-5">
                        <a href="/index.php?route=product/make_my_snack" ><span>Make My Snack</span></a>
                    <li class="nav col-md-4 col-sm-1 col-xs-1"></li> 
                </ul>
            </div>
        </div>
    </div>    
<?php } ?>

<!-- <div class="jumbotron text-center">
  <h2>Option 1</h2>
<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/caramel-oats-square-1080x1080.webp"></a>
  <h5>Oats</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chocolate-muesli-parfait-1080x1080.webp"></a>
  <h5>Muesli</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/berries-mix-smoothie-a-1080x1080.webp"></a>
  <h5>Granola</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chia-mango-parfait-1080x1080.webp"></a>
  <h5>Oatmeal</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/oatmeal-muffins-a-1080x1080.webp"></a>
  <h5>Seeds and Berries</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/tangy-cranberry-cooler-1080x1080.webp"></a>
  <h5>Roasted Snacks</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/seeds-and-berry-oatmeal-1080x1080.webp"></a>
  <h5>Raw Seeds</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/tasty-chia-drink-1080x1080.webp"></a>
  <h5>Roasted Seeds</h5>
</div>

</div> -->

<!--option 2-->

<!-- <div class="jumbotron text-center">
  <h2>Option 2</h2>
<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/caramel-oats-square-1080x1080.webp" style="border-radius: 70px;"></a>
  <h5>Oats</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chocolate-muesli-parfait-1080x1080.webp" style="border-radius: 70px;"></a>
  <h5>Muesli</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/berries-mix-smoothie-a-1080x1080.webp" style="border-radius: 70px;"></a>
  <h5>Granola</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chia-mango-parfait-1080x1080.webp" style="border-radius: 70px;"></a>
  <h5>Oatmeal</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/oatmeal-muffins-a-1080x1080.webp" style="border-radius: 70px;"></a>
  <h5>Seeds and Berries</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/tangy-cranberry-cooler-1080x1080.webp" style="border-radius: 70px;"></a>
  <h5>Roasted Snacks</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/seeds-and-berry-oatmeal-1080x1080.webp" style="border-radius: 70px;"></a>
  <h5>Raw Seeds</h5>
</div>

<div class="img-container">
  <a><img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/tasty-chia-drink-1080x1080.webp" style="border-radius: 70px;"></a>
  <h5>Roasted Seeds</h5>
</div>

</div>

<style type="text/css">
  div.image-gallery {
  margin: 5px;
  border: 1px solid #ccc;
  width: 150px;
  box-shadow: 0 8px 6px -6px rgba(0,0,0,0.1);
}

div.image-gallery:hover {
  border: 2px solid #ccc;
}

div.image-gallery img {
  width: 150px;
  height: auto;
  border-bottom: 3px solid #f49a25;
}

div.desc {
  padding: 2px;
  text-align: center;
}

.imgcontainer{
  display: inline-block;
  width: 160px;
  }

  .desc h5{
    font-size: 15px;
  }
}
</style>

<div class="jumbotron text-center">
<h2>Option 3</h2>
 <div class="imgcontainer">
  <div class="image-gallery">
  <a target="_blank" href="">
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/caramel-oats-square-1080x1080.webp">
  </a>
  <div class="desc"><h5>Oats<h5></div>
 </div>
</div>

<div class="imgcontainer">
  <div class="image-gallery">
  <a target="_blank" href="">
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chocolate-muesli-parfait-1080x1080.webp">
  </a>
  <div class="desc"><h5>Muesli<h5></div>
 </div>
</div>

<div class="imgcontainer">
  <div class="image-gallery">
  <a target="_blank" href="">
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/oatmeal-muffins-a-1080x1080.webp">
  </a>
  <div class="desc"><h5>Granola<h5></div>
 </div>
</div>

<div class="imgcontainer">
  <div class="image-gallery">
  <a target="_blank" href="">
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chia-mango-parfait-1080x1080.webp">
  </a>
  <div class="desc"><h5>Oatmeal<h5></div>
 </div>
</div>

<div class="imgcontainer">
  <div class="image-gallery">
  <a target="_blank" href="">
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/caramel-oats-square-1080x1080.webp">
  </a>
  <div class="desc"><h5>Seeds and Berries<h5></div>
 </div>
</div>

<div class="imgcontainer">
  <div class="image-gallery">
  <a target="_blank" href="">
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/tangy-cranberry-cooler-1080x1080.webp">
  </a>
  <div class="desc"><h5>Roasted Snacks<h5></div>
 </div>
</div>

<div class="imgcontainer">
  <div class="image-gallery">
  <a target="_blank" href="">
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chocolate-muesli-parfait-1080x1080.webp">
  </a>
  <div class="desc"><h5>Muesli<h5></div>
 </div>
</div>

<div class="imgcontainer">
  <div class="image-gallery">
  <a target="_blank" href="">
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chia-mango-parfait-1080x1080.webp">
  </a>
  <div class="desc"><h5>Oatmeal<h5></div>
 </div>
</div>
</div>
 -->
<!---option 4--->

<!-- <style>
div.gallery1 {
  margin: 6px 15px 6px 15px;
  border: 1px solid #ccc;
  float: left;
  width: 148px;
  border-radius: 15px;
}

div.gallery1:hover {
  border: 1px solid #777;
  transform: scale(1.1, 1.1);
  transition: all 0.6s ease;
}

div.gallery1 img {
  width: 100%;
  height: auto;
  border-top-right-radius: 15px;
  border-top-left-radius: 15px;
}

div.desc1 {
  text-align: center;
  border-bottom-right-radius: 15px;
  border-bottom-left-radius: 15px;
  background-color: #F9F9F9;
}

.button5:hover {
  background-color: #555555;
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button5 {
  background-color: white;
  color: black;
  border: 1px solid #555555;
  width: 100%;
  border-bottom-right-radius: 15px;
  border-bottom-left-radius: 15px;
}

.desc1 p{
  font-size: 15px;
  font-weight: bold;
}

.container-fluid{
  margin-bottom: 10px;
}

@media (max-width: 500px) and (min-width: 300px){
  div.gallery1 {
  margin: 8px !important;
}
</style>
 -->
<!-- <div class="container-fluid text-center">
<h2>Option 4</h2>
<div class="gallery1">
  <a>
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/blueberry-blast-1080x1080.webp" width="600" height="400">
  </a>
  <div class="desc1">

      <button class="button button5">Oats</button>
  </div>
  
</div>

<div class="gallery1">
  <a>
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chocolate-overnight-oats-1080x1080.webp" width="600" height="400">
  </a>
 <div class="desc1">
  
      <button class="button button5">Berries</button>
  </div>

</div>

<div class="gallery1">
  <a>
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/mango-oatmeal-parfait-1080x1080.webp" width="600" height="400">
  </a>
  <div class="desc1">
      <button class="button button5">Snacks</button>
  </div>
</div>

<div class="gallery1">
  <a>
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chia-smoothie-1080x1080.webp"width="600" height="400">
  </a>
  <div class="desc1">
  
      <button class="button button5">Granola</button>
  </div>
</div>

<div class="gallery1">
  <a>
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chia-mango-parfait-1080x1080.webp"width="600" height="400">
  </a>
  <div class="desc1">

      <button class="button button5">Muesli</button>
  </div>
</div>

<div class="gallery1">
  <a>
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/caramel-oats-square-1080x1080.webp" width="600" height="400">
  </a>
  <div class="desc1">
 
      <button class="button button5">Raw seeds</button>
  </div>
</div>

<div class="gallery1">
  <a>
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chia-mango-parfait-1080x1080.webp" width="600" height="400">
  </a>
  <div class="desc1">
   
      <button class="button button5">Roasted Seeds</button>
  </div>
</div>

<div class="gallery1">
  <a>
    <img src="https://www.true-elements.com/image/cachewebp/catalog/1-1/chia-mango-parfait-1080x1080.webp"width="600" height="400">
  </a>
  <div class="desc1">

      <button class="button button5">Muesli</button>
  </div>
</div>
</div> -->





<?php if($category_id != 586 && $category_id != '621') { ?>
    <a href="/our-products"><img alt="True Elements" src="<?php echo $category_strip_desktop; ?>" style="margin-bottom:20px;" class="img-responsive hidden-xs hidden-sm"></a>
    <a href="/our-products"><img alt="True Elements" src="<?php echo $category_strip_mobile; ?>" style="margin-bottom:20px;" class="img-responsive hidden-md hidden-xl hidden-lg"></a>
<?php } ?>
<?php if($category_id != '621') { ?>
<div class="">
  <div class="container-fluid">
      
      	<?php if ($thumb || $description) { ?>
		<div class="category-info">
			<?php if ($thumb) { ?>
				<div class="category-img"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
			<?php } ?>

		</div>

    <?php } ?>
    <?php if($category_id != 586) { ?>
    	<h3><?php echo $heading_title; ?></h3><br />
    <?php } ?>
    				<?php if ($description) { ?>
			<div class="category-des"><?php echo $description; ?></div>
			<?php } ?><br />
     

    
      <div class="container">
	<?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
			<?php echo $content_top; ?>
			 <?php if ($hb_snippets_bc_enable == '1'){ ?>
			<?php 
				$count_breadcrumb = 0; 
				$bc = '';
				foreach ($breadcrumbs as $breadcrumb) { 
				$count_breadcrumb = $count_breadcrumb + 1; 
				$bc .= '{
    "@type": "ListItem",
    "position": '.$count_breadcrumb.',
    "item": {
      "@id": "'.$breadcrumb['href'].'",
      "name": "'.$breadcrumb['text'].'"
    }},';
	}
	$bc = str_replace('<i class="fa fa-home"></i>','Home',$bc);
	$bc = rtrim($bc,',');
	?>
			<script type="application/ld+json">
			{
		  "@context": "http://schema.org",
		  "@type": "BreadcrumbList",
		  "itemListElement": [<?php echo $bc; ?>]}
		  </script>
		  <?php } ?>
		


       <?php if ($salescombopgeoffers) {  foreach($salescombopgeoffers as $offer) { echo html_entity_decode($offer['html']); } } ?>
        
    <?php if ($categories) { ?>
    <div class="col-md-1"></div>
    <div class="refine-search col-md-10 col-sm-12 col-xs-12">
	
		
		<div class="row">
		    <?php foreach ($categories as $category) { ?>
		<center><div class="col-lg-15 col-md-3 col-sm-4 col-xs-6">
				<a class="w3-btn w3-white w3-border w3-border-red w3-round-large" href="<?php echo $category['href']; ?>">
				    <span class="sub-cat"><?php echo $category['name']; ?></span></a>
				</div>
			</center>
			<?php } ?>
		</div>
		</div>
		    <div class="col-md-1"></div>

	<?php } ?>
	<!--style="<?php if($category_id == 586) { echo 'display: none'; } ?>"-->
	<div class="custom-category">
	<?php if ($products) { ?>
	    <?php if($category_id == "496") {
	        $array_key = array_column($products, 'stock_status');
	        $array_key2 = array_column($products, 'sort_order');
            array_multisort($array_key, SORT_ASC, $array_key2, SORT_ASC, $products);
        } ?>
	<input type="hidden" name="product_id" value="" />
	<br />
		<div class="toolbar"> 
			<div class="toolbar1">
				<div class="btn-group">
					<button type="button" id="list-view" class="btn btn-default"></button>
					<button type="button" id="grid-view" class="btn btn-default"></button>
				</div>
			</div>
			<div class="toolbar2">
				<select id="input-sort" class="form-control" onchange="location = this.value;">
					<?php foreach ($sorts as $sorts) { ?>
					<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
					<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="toolbar3">
				<select id="input-limit" class="form-control" onchange="location = this.value;">
				  <?php foreach ($limits as $limits) { ?>
				  <?php if ($limits['value'] == $limit) { ?>
				  <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
				  <?php } ?>
				  <?php } ?>
				</select>
			</div>
		</div>
			<div class="col-md-12 col-xs-12 col-sm-12 category-products">
			<?php foreach ($products as $product) { ?>
			<div class="col-md-4 col-xs-6 col-sm-4 product-container <?php if($category_id == 586) { echo 'personalized-product'; } else { echo 'mobile'; } ?>">
				<div class="product-thumb item-inner">
					<?php if($product['coupon_discount']){ ?> 
        				<div class="deals-discount"><h5><?php echo round($product['discount']); ?> %<br>Off</h5></div>
        			 <?php } ?>
        			 <div class="images-container">
				 
								 <a href="<?php echo $product['href']; ?>">
								    <div class="pro_img">
									<?php if ($product['thumb']) { ?>
									<?php if($product['rotator_image']){ ?>
									<img class="image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>"/><?php } ?>
									<?php } else { ?>
									<img alt="True Elements"  src="image/cache/no_image-100x100.png" alt="<?php echo $product['name']; ?>" />
									<?php } ?>
									<img  src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="image1"/></div>
								</a>
						
					</div>
					<div class="des-container <?php if($category_id == 586) { echo 'personalized'; } ?>">
						<h2 class="product-name hidden-xs hidden-sm"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
						<h2 class="product-name hidden-md hidden-xl hidden-lg"><a href="<?php echo $product['href']; ?>"><?php echo $product['name_mobile']; ?></a></h2>
						<?php $is_optionqty = 1; ?>
						<?php if($product['options']) { ?>
						    <div class="options options-<?php echo $product['product_id']; ?>">
						        <?php foreach($product['options'] AS $option) { ?>
                                    <?php $countoption = count($option['product_option_value']); ?>
                                            <?php $price_tooltip_radio = 1; ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <?php if($option_value['quantity'] >= 0) { $is_optionqty = 0; } ?>
                                                        <div class="radio">
                                                            <?php if($countoption == 1) { ?>
                                                                <label class="<?php if($option_value['quantity'] > 0) { echo 'checked-option'; } else { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } else { echo 'checked'; } ?>/>
                                                            <?php } else { ?>
                                                                <label class="<?php if($option_value['quantity'] < 1) { echo 'disabled'; } ?>">
                                                                    <input type="radio" class="option-<?php echo $product['product_id']; ?>" onchange="price_with_options_ajax_call(<?php echo $product['product_id']; ?>)" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($option_value['quantity'] < 1) { echo 'disabled'; } ?> />
                                                            <?php } ?>
                                                            <?php if($price_tooltip_radio && $option_value['price']) { ?>
                                                                <span data-toggle="tooltip" title="<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>"><?php echo $option_value['name']; ?></span>
                                                            <?php } elseif (!$price_tooltip_radio && $option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?> (<?php echo $option_value['price_prefix']; ?><?php echo round($option_value['price']); ?>)</span>
                                                            <?php } elseif ($price_tooltip_radio && !$option_value['price']) { ?>
                                                                <span><?php echo $option_value['name']; ?></span>
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                            <?php } ?>
						        <?php } ?>
						    </div>
						<?php } else { ?>
						    <div class="options <?php if($category_id == 586) { echo 'hidden-md hidden-xl hidden-lg hidden-xs hidden-sm';} ?>"></div>
						    <?php $is_optionqty = 0; ?>
						<?php } ?>

						<div class="price-label <?php if($category_id == 586) { echo 'hidden-xs hidden-sm hidden-md hidden-xl hidden-lg'; } else { echo 'col-md-12 col-xs-12 col-sm-12'; } ?>">
							<div class="box-price">
								<?php if ($product['price']) { ?>
								<p class="price col-md-12 col-xs-12 col-sm-12">
								  <?php if (!$product['special']) { ?>
                                        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } else { ?>
								        <span class="price-new price-new-<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
						                <span class="price-old price-old-<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
								  <?php } ?>
								</p>
								<?php if ($product['coupon_discount']) { ?>
            			    	            <span class="price-coupon price-coupon-<?php echo $product['product_id']; ?>"><b> <?php echo $product['coupon_discount']; ?> </b></span><span> <?php echo $product['coupon'] ? 'Using code: <b>' . $product['coupon'] . '</b>' : ''; ?></span>
            			    	    <?php } ?>	
								<?php } ?>
							</div>
						</div>
						
						<?php if($product['product_type'] != 5) { ?>
						    <?php if($is_optionqty != 1 && $product['quantity'] > 0) { ?>
						        <div class="quantity-container" style="width: 50%;">
						            <input type="button" value="-" class="minus-button" onclick="minus(<?php echo $product['product_id']; ?>)" />
						            <input type="text" class="quantity-<?php echo $product['product_id']; ?> quantity" name="quantity" value="1" onchange="quantityVerify(<?php echo $product['product_id']; ?>)" />
						            <input type="button" value="&#43;" class="plus-button" onclick="plus(<?php echo $product['product_id']; ?>)" />
						        </div>
						        <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-sm hidden-xs" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><?php echo $button_cart; ?></button>
						        <button type="button" class="btn button button-cart cart-<?php echo $product['product_id']; ?> hidden-xl hidden-lg hidden-md button-cart-mobile" onclick ="addtocart(<?php echo $product['product_id']; ?>)"><i class="fa fa-shopping-cart"></i></button>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
						<?php } else { ?>
						    <?php if($product['quantity'] > 0) { ?>
						        <a href="<?php echo $product['href']; ?>" class="btn button button-personalize">Personalize Now</a>
						    <?php } else { ?>
						        <button class="oos btn button hidden-xs hidden-sm" type="button" disabled>Stock Over!</button>
						        <button class="oos btn button btn-oos-mobile hidden-xl hidden-lg hidden-md" type="button" disabled><i class="fa fa-shopping-cart"></i></button>
						    <?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
			</div>
		<div class="row toolbar4">
	<div class="col-md-12 col-sm-12 col-xs-12"><center><?php echo $pagination; ?></center></div>
		 
		</div>
    <?php } ?>
	</div>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php } ?>
<script type="text/javascript">
    function addtocart(product_id) {
        $('input[name=\'product_id\']').val(product_id);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('input[name=\'product_id\'], .quantity-' + product_id + ', .checked-option .option-' + product_id),
            dataType: 'json',
            success: function(json) {
                if (json['error']) {
                    if (json['error']['option']) {
                        if($(".text-danger." + product_id).length == 0) {
                            $('.options-' + product_id).before('<div class="text-danger ' + product_id + '">Please Select Weight</div>');
                        }
                    }
                }
                 if (json['success']) {
                    // Added to cart
                    var success_text = '<div style="margin: 0 0 10px 0"><div  style="float: left" ><img  style="margin: 0 5px 10px 0" class="img-thumbnail" src="'+json['href']+'" alt="True Elements"></div><div  style="text-align: center" >' + json['success'] + '</div></div>';

                    var n = new Noty({
                        type: 'success',
                        layout: json['notice_add_layout'],
                        text:  success_text ,
                        textAlign:"right",
                        animateOpen:{"height":"toggle"},
                        timeout: json['notice_add_timeout'],
                        progressBar: false,
                        closeWith: 'button',
                    }).show();
                    // Added to cart
                    $('#cart-total').html(json['total']);
                    $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    function minus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval-1);
        if($(".quantity-" + product_id).val() <= 0) {
            $(".quantity-" + product_id).val(1);
        }
    }
  
    function plus(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        $(".quantity-" + product_id).val(currentval+1);
    }
    
    function quantityVerify(product_id) {
        var currentval = parseInt($(".quantity-" + product_id).val());
        if(currentval <= 0) {
           $(".quantity-" + product_id).val(1); 
        }
    }
</script>
<script type="text/javascript">
    function price_with_options_ajax_call(product_id) {
        $(".text-danger." + product_id).remove();
	    $.ajax({
	    	type: 'POST',
	    	url: 'index.php?route=product/live_options/index&product_id=' + product_id,
	    	data: $('.checked-option .option-' + product_id),
	    	dataType: 'json',
			success: function(json) {
				if (json.success) {
					$('.price-new-' + product_id).html(json.new_price.special);
					$('.price-old-' + product_id).html(json.new_price.price);
					
					if(json.new_price.coupon) {
					    $('.price-coupon-' + product_id).html(json.new_price.coupon); 
					}
					
					if(json.new_price.special) {
					    $('.price-new-' + product_id).html(json.new_price.special);
					    $('.price-old-' + product_id).html(json.new_price.price);
					} else {
					    $('.price-new-' + product_id).html(json.new_price.price);
					}
				}
			},
			error: function(error) {
				console.log('error: '+error);
			}
		});
	}
	
	$(document).ready(function() {
	    $(".product-options").each(function () {
            if ($(this)[0].length == 0) {
                $(this).hide();
            }
        });
	});
</script>
<script type="text/javascript"><!--
    $('.des-container .options .radio input[type="radio"]').click(function() {
        $('.des-container .options .radio input[type="radio"]').each(function() {
            $(this).parents("label").toggleClass('checked-option', this.checked);
        });
    });
</script>

         <div id="offerPopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo $offerclose; ?></button>
      </div>
    </div>

  </div>
</div>
         

         <script type="text/javascript">
  function openOfferPopup(id) {
      $.ajax({
      url: 'index.php?route=offers/salescombopge/popp',
      type: 'post',
      dataType: 'json',
      data: {"page_id" : id} ,
      success: function(json) {
        if(json.html != undefined) {
          if(json.html.title != undefined) {
            $('#offerPopup .modal-title').html(json.html.title);
          }
          if(json.html.description != undefined) {
            $('#offerPopup .modal-body').html(json.html.description);
          }
          $('#offerPopup').modal('show'); 
        } 
      }
    });
  }
</script>
         
<?php echo $footer; ?>