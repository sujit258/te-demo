<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>

				<?php if (is_array($opengraphs)) { ?>
                <?php foreach ($opengraphs as $opengraph) { ?>
                <meta property="<?php echo $opengraph['meta'] ?>" content="<?php echo $opengraph['content']; ?>" />
                <?php } ?>
                <?php } ?>
				
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>

			<?php if (isset($mpmetas)) { ?>
			<?php foreach ($mpmetas as $mpmeta) { ?>
			<meta <?php echo $mpmeta['attribute']; ?>="<?php echo $mpmeta['attribute_value']; ?>" content="<?php echo $mpmeta['content']; ?>" />
			<?php } ?>
			<?php } ?>
			
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery-ui.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/css/jquery-ui.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/javascript/material-design-iconic-font/css/material-design-iconic-font.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700,700i" rel="stylesheet" type="text/css">
<link href="catalog/view/theme/tt_presiden1/stylesheet/stylesheet.css" rel="stylesheet">
<script src="catalog/view/javascript/opentheme/ocslideshow/jquery.nivo.slider.js" type="text/javascript"></script>
<link href="catalog/view/theme/tt_presiden1/stylesheet/opentheme/ocslideshow/ocslideshow.css" rel="stylesheet" />
<link href="catalog/view/theme/tt_presiden1/stylesheet/opentheme/css/animate.css" rel="stylesheet" />
<script src="catalog/view/javascript/opentheme/hozmegamenu/custommenu.js" type="text/javascript"></script>
<script src="catalog/view/javascript/opentheme/hozmegamenu/mobile_menu.js" type="text/javascript"></script>
<link href="catalog/view/theme/tt_presiden1/stylesheet/opentheme/hozmegamenu/css/custommenu.css" rel="stylesheet" />
<link href="catalog/view/theme/tt_presiden1/stylesheet/opentheme/css/owl.carousel.css" rel="stylesheet" />
<script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.js" type="text/javascript"></script>
<script src="catalog/view/javascript/opentheme/ocquickview/ocquickview.js" type="text/javascript"></script>
<link href="catalog/view/theme/tt_presiden1/stylesheet/opentheme/ocquickview/css/ocquickview.css" rel="stylesheet">
<link href="catalog/view/theme/tt_presiden1/stylesheet/opentheme/oclayerednavigation/css/oclayerednavigation.css" rel="stylesheet">
<script src="catalog/view/javascript/opentheme/oclayerednavigation/oclayerednavigation.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/elevatezoom/jquery.elevatezoom.js" type="text/javascript"></script>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

        <script type="text/javascript">
          function facebook_loadScript(url, callback) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            if(script.readyState) {  // only required for IE <9
              script.onreadystatechange = function() {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                  script.onreadystatechange = null;
                  if (callback) {
                    callback();
                  }
                }
              };
            } else {  //Others
              if (callback) {
                script.onload = callback;
              }
            }

            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
          }
        </script>

        <script type="text/javascript">
          (function() {
            var enableCookieBar = '<?= $facebook_enable_cookie_bar ?>';
            if (enableCookieBar === 'true') {
              facebook_loadScript("catalog/view/javascript/facebook/cookieconsent.min.js");

              // loading the css file
              var css = document.createElement("link");
              css.setAttribute("rel", "stylesheet");
              css.setAttribute("type", "text/css");
              css.setAttribute(
                "href",
                "catalog/view/theme/css/facebook/cookieconsent.min.css");
              document.getElementsByTagName("head")[0].appendChild(css);

              window.addEventListener("load", function(){
                function setConsent() {
                  fbq(
                    'consent',
                    this.hasConsented() ? 'grant' : 'revoke'
                  );
                }
                window.cookieconsent.initialise({
                  palette: {
                    popup: {
                      background: '#237afc'
                    },
                    button: {
                      background: '#fff',
                      text: '#237afc'
                    }
                  },
                  cookie: {
                    name: fbq.consentCookieName
                  },
                  type: 'opt-out',
                  showLink: false,
                  content: {
                    dismiss: 'Agree',
                    deny: 'Opt Out',
                    header: 'Our Site Uses Cookies',
                    message: 'By clicking Agree, you agree to our <a class="cc-link" href="https://www.facebook.com/legal/terms/update" target="_blank">terms of service</a>, <a class="cc-link" href="https://www.facebook.com/policies/" target="_blank">privacy policy</a> and <a class="cc-link" href="https://www.facebook.com/policies/cookies/" target="_blank">cookies policy</a>.'
                  },
                  layout: 'basic-header',
                  location: true,
                  revokable: true,
                  onInitialise: setConsent,
                  onStatusChange: setConsent,
                  onRevokeChoice: setConsent
                }, function (popup) {
                  // If this isn't open, we know that we can use cookies.
                  if (!popup.getStatus() && !popup.options.enabled) {
                    popup.setStatus(cookieconsent.status.dismiss);
                  }
                });
              });
            }
          })();
        </script>

        <script type="text/javascript">
          (function() {
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

            var enableCookieBar = '<?= $facebook_enable_cookie_bar ?>';
            if (enableCookieBar === 'true') {
              fbq.consentCookieName = 'fb_cookieconsent_status';

              (function() {
                function getCookie(t){var i=("; "+document.cookie).split("; "+t+"=");if(2==i.length)return i.pop().split(";").shift()}
                var consentValue = getCookie(fbq.consentCookieName);
                fbq('consent', consentValue === 'dismiss' ? 'grant' : 'revoke');
              })();
            }

            <?php if ($facebook_pixel_id_FAE) { ?>
              facebook_loadScript(
                "catalog/view/javascript/facebook/facebook_pixel.js",
                function() {
                  var params = <?= $facebook_pixel_params_FAE ?>;
                  _facebookAdsExtension.facebookPixel.init(
                    '<?= $facebook_pixel_id_FAE ?>',
                    <?= $facebook_pixel_pii_FAE ?>,
                    params);
                  <?php if ($facebook_pixel_event_params_FAE) { ?>
                    _facebookAdsExtension.facebookPixel.firePixel(
                      JSON.parse('<?= $facebook_pixel_event_params_FAE ?>'));
                  <?php } ?>
                });
            <?php } ?>
          })();
        </script>
      

			<style type="text/css">
.te-sj{
margin-top:-15px;
position: fixed;  
bottom: -2px; 
right: 5px;
}

 .te-sj { 
display:none; 
}
    
    @media screen and (max-width: 500px) {
.te-sj { 
display:block; }
}

.te-sj a{-webkit-transition: all 0.3s ease-in-out; -moz-transition: all 0.3s ease-in-out; -o-transition: all 0.3s ease-in-out; -ms-transition: all 0.3s ease-in-out; transition: all 0.3s ease-in-out;}
.te-sj a.whatsapp:hover{background:#39599f;}
.te-sj .fa {
    width: 23px;
    height: 21px;
    text-align: center;
    margin: 0 2px;
    display: inline-block;
    font-size: 20px;
    color: #fff;
    border-radius: 2px;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}
.botao-wpp {
  text-decoration: none;
  color: #eee;
  display: inline-block;
  background-color: #25d366;
  font-weight: bold;
  padding: 1rem 2rem;
  border-radius: 3px;
}

.botao-wpp:hover {
  background-color: darken(#25d366, 5%);
}

.botao-wpp:focus {
  background-color: darken(#25d366, 15%);
}


</style>
  

            <?php echo isset($_xshipping_estimator) ? $_xshipping_estimator : ''; ?>
        
</head>
<body class="<?php echo $class; ?>">
<header>
<nav id="top">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-12 col-sms-12">
				<div id="logo">
				  <?php if ($logo) { ?>
				  <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
				  <?php } else { ?>
				  <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
				  <?php } ?>
				</div>
			</div>
			<div class="col-md-9 col-sm-12 col-sms-12">
				<div class="header-container">
					<?php if(isset($block1)){ echo $block1; }?>
					<div class="box-top">
						<div class="top-cart">
							<?php echo $cart; ?>
						</div>
						<div class="header-link">
							<div class="header-content">
								<div class="icon-link"></div>
								<div class="box-content">
									<div id="top-links">
										<div class="box-link">
											<ul class="list-inline links">
												<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>"><span><?php echo $text_account; ?></span></a></li>
												<li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><span><?php echo $text_shopping_cart; ?></span></a></li>
												<li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><span ><?php echo $text_checkout; ?></span></a></li>
											</ul>
										</div>
									</div>
									<div class="language-currency">
										<div class="header-currency"><?php echo $currency; ?></div>
										<div class="header-laguage"><?php echo $language; ?></div>
									</div>
								</div>
							</div>
						</div>
						<div class="header-search">
							<?php echo $search; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</nav>
</header>
<?php if(isset($block2)){ echo $block2; }?>
<script type="text/javascript">
$(document).ready(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() > 47) {
		$('header').addClass("fix-nav");
		} else {
		$('header').removeClass("fix-nav");
		}
	});
	$(function dropDown()
	{
		elementClick = '.header-link .icon-link,#search-by-category .icon-search,#cart .btn-cart';
		elementSlide =  '.dropdown-menu,.search-box,.box-content';
		activeClass = 'active';

		$(elementClick).on('click', function(e){
		e.stopPropagation();
		var subUl = $(this).next(elementSlide);
		if(subUl.is(':hidden'))
		{
		subUl.slideDown();
		$(this).addClass(activeClass);
		}
		else
		{
		subUl.slideUp();
		$(this).removeClass(activeClass);
		}
		$(elementClick).not(this).next(elementSlide).slideUp();
		$(elementClick).not(this).removeClass(activeClass);
		e.preventDefault();
		});

		$(elementSlide).on('click', function(e){
		e.stopPropagation();
		});

		$(document).on('click', function(e){
		e.stopPropagation();
		var elementHide = $(elementClick).next(elementSlide);
		$(elementHide).slideUp();
		$(elementClick).removeClass('active');
		});
		
	});
});
</script>