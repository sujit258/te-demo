<?php
class ControllerCommonHeader extends Controller {
	public function index() {

        $data['facebook_pixel_id_FAE'] =
          $this->fbevents['facebook_pixel_id_FAE'];
        $data['facebook_pixel_pii_FAE'] =
          $this->fbevents['facebook_pixel_pii_FAE'];
        $data['facebook_pixel_params_FAE'] =
          $this->fbevents['facebook_pixel_params_FAE'];
        $data['facebook_pixel_params_FAE'] =
          $this->fbevents['facebook_pixel_params_FAE'];
        $data['facebook_pixel_event_params_FAE'] =
          $this->fbevents['facebook_pixel_event_params_FAE'];
        $data['facebook_enable_cookie_bar'] = false;

        // remove away the facebook_pixel_event_params_FAE in session data
        // to avoid duplicate firing after the 1st fire
        unset($this->session->data['facebook_pixel_event_params_FAE']);
      
$this->document->addStyle('catalog/view/theme/default/stylesheet/joseanmatias_preco_cupom.css');
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');
       	
		$this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/main.css');
        $this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/util.css');
        //$this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/showcase.css');

       //$this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/slick.min.js');
       
       
       		
		//$this->document->addStyle('catalog/view/javascript/wow.css');
		
		$this->document->addStyle('catalog/view/theme/tt_presiden10/stylesheet/myaccount.css');
        
        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        //$this->document->addStyle('catalog/view/javascript/font-awesome.min.css');
        
        //$this->document->addScript(HTTPS_SERVER . 'catalog/view/javascript/wow.js');

        
        
		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		/*if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}*/
		$this->document->addLink($server . 'image/catalog/brands/te-favicon.svg', 'icon');

		$data['title'] = $this->document->getTitle();

					if (is_array($this->document->getOpengraph())) { 
	$data['opengraphs'] = $this->document->getOpengraph();
	}
				

		$data['base'] = $server;
$this->document->addStyle('catalog/view/theme/default/stylesheet/slsoffr.css');
		$data['description'] = $this->document->getDescription();

			$data['mpmetas'] = $this->document->getMpMeta();
			
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}
		
		$data['whatsapp_number'] = $this->config->get('config_whatsappid');
		$data['store_telephone'] = $this->config->get('config_telephone');
		$data['store_email'] = $this->config->get('config_email');


                  $this->load->model('extension/shipping/xshippingpro');
                  $data['_xshipping_estimator'] = $this->model_extension_shipping_xshippingpro->getEstimatorMeta();
            
		$this->load->language('common/header');
$data['store_url'] = HTTPS_SERVER;

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');

				$data['block1'] = $this->load->controller('common/block1');
				$data['block2'] = $this->load->controller('common/block2');
				$data['block3'] = $this->load->controller('common/block3');
				if ($this->customer->isLogged()) {
					$data['text_message'] = sprintf($this->language->get('text_message2'),$this->customer->getFirstName(),$this->customer->getLastName());
				} else{
					$data['text_message'] = $this->language->get('text_message');
				}
				$data['text_store'] = $this->language->get('text_store');
				$data['text_guarantee'] = $this->language->get('text_guarantee');
				$data['text_compare'] = $this->language->get('text_compare');
				$data['text_block'] = $this->language->get('text_block');
				$data['compare'] = $this->url->link('product/compare', '', true);
				
			

		// Menu
		$this->load->model('catalog/category');


				$this->load->controller('extension/module/ianalytics');
			
		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}


			/* mps */
			$this->load->language('mpblog/blogmenu');
			$mblog_link = $this->url->link('mpblog/blogs','',true);
			$text_mblog = $this->language->get('text_mblog');
			if($this->config->get('mpblog_home_category')) {
			$this->load->model('mpblog/mpblogcategory');
			$mpblog_catinfo = $this->model_mpblog_mpblogcategory->getMpBlogCategory($this->config->get('mpblog_home_category'));
			if($mpblog_catinfo) {
				$mblog_link = $this->url->link('mpblog/blogcategory','mpcpath='.$mpblog_catinfo['mpblogcategory_id'] ,true);
				$text_mblog = $mpblog_catinfo['name'];
			}
			}
			$mpblogmenu = array(
				'name'     => $text_mblog,
				'children' => array(),
				'column'   => 1,
				'href'     => $mblog_link
			);
			if($this->config->get('mpblog_status')) {
			//array_unshift($data['categories'], $mpblogmenu);
			array_push($data['categories'], $mpblogmenu);
			}
			/* mpe */
			

      	$this->load->model('newsblog/category');
        $this->load->model('newsblog/article');

		$data['newsblog_categories'] = array();

		$categories = $this->model_newsblog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['settings']) {
				$settings=unserialize($category['settings']);
				if ($settings['show_in_top']==0) continue;
			}

			$articles = array();

			if ($category['settings'] && $settings['show_in_top_articles']) {
				$filter=array('filter_category_id'=>$category['category_id'],'filter_sub_category'=>true);
				$results = $this->model_newsblog_article->getArticles($filter);

				foreach ($results as $result) {
					$articles[] = array(
						'name'        => $result['name'],
						'href'        => $this->url->link('newsblog/article', 'newsblog_path=' . $category['category_id'] . '&newsblog_article_id=' . $result['article_id'])
					);
				}
            }
			$data['categories'][] = array(
				'name'     => $category['name'],
				'children' => $articles,
				'column'   => 1,
				'href'     => $this->url->link('newsblog/category', 'newsblog_path=' . $category['category_id'])
			);
		}
		
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		
				/* Edit for Search Category Module by OCMod */
				$module_status = $this->config->get('ocsearchcategory_status');
				if($module_status) {
					$data['search'] = $this->load->controller('extension/module/ocsearchcategory');
				} else {
					$data['search'] = $this->load->controller('common/search');
				}
				/* End Code */
			
		$data['cart'] = $this->load->controller('common/cart');

		// For page specific css

        $this->load->language('offers/salescombopge');
        $data['text_salescombopge_heading'] = $this->language->get('text_salescombopge_heading');
        $data['salescombopge_info'] = array();
        $this->load->model('offers/salescombopge');
        $salescombopge_info = $this->model_offers_salescombopge->getPages();
        foreach ($salescombopge_info as $key => $value) {
          $data['salescombopge_info'][] = array(
            'name'=> $value['title'],
            'href' => $this->url->link('offers/salescombopge', 'page_id=' .  $value['salescombopge_id']),
            'id' => "",
            'children_level2' => array()
          );
        }
        if(!empty($data['salescombopge_info'])) {
          $data['categories'][] = array(
            'name'     => $data['text_salescombopge_heading'],
            'children' => $data['salescombopge_info'],
            'column'   => 1,
            'href'     => $this->url->link("offers/alloffers")
          );
        }

        
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}


				if($this->config->get('ocajaxlogin_status')){
					$data['use_ajax_login'] = true;
				}else{
					$data['use_ajax_login'] = false;
				}
			
		return $this->load->view('common/header', $data);
	}
}
