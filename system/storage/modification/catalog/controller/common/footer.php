<?php
class ControllerCommonFooter extends Controller {
			
/* AbandonedCarts - Begin */
protected function register_abandonedCarts() {

    $ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '*HiddenIP*';
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $ip = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    
    if (isset($this->session->data['abandonedCart_ID']) & !empty($this->session->data['abandonedCart_ID'])) {
        $id = $this->session->data['abandonedCart_ID'];
    } else if ($this->customer->isLogged()) {
        $id = (!empty($this->session->data['abandonedCart_ID'])) ? $this->session->data['abandonedCart_ID'] : $this->customer->getEmail();
    } else {
        $id = (!empty($this->session->data['abandonedCart_ID'])) ? $this->session->data['abandonedCart_ID'] : session_id();
    }

    $exists = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcarts` WHERE `restore_id` = '$id' AND `ordered`=0");
    $cart = $this->cart->getProducts();
    $store_id = (int)$this->config->get('config_store_id');
    $cart = (!empty($cart)) ? $cart : '';
    
    $lastpage = "$_SERVER[REQUEST_URI]";
    
    $checker = $this->customer->getId();
    if (!empty($checker)) {
        $customer = array(
            'id'        => $this->customer->getId(), 
            'email'     => $this->customer->getEmail(),		
            'telephone' => $this->customer->getTelephone(),
            'firstname' => $this->customer->getFirstName(),
            'lastname'  => $this->customer->getLastName(),
            'language'  => $this->session->data['language']
        );
    } 

    $route = isset($this->request->get['route']) ? $this->request->get['route'] : '';
    if ($route!='checkout/success') {
      if (empty($exists->row)) {
          if (!empty($cart)) {
              if (!isset($customer)) {
                  $customer = array(
                      'language' => $this->session->data['language']
                  );
              }
              $cart = json_encode($cart);
              $customer = (!empty($customer)) ? json_encode($customer) : '';
              $this->db->query("INSERT INTO `" . DB_PREFIX . "abandonedcarts` SET `cart`='".$this->db->escape($cart)."', `customer_info`='".$this->db->escape($customer)."', `last_page`='$lastpage', `ip`='$ip', `date_created`=NOW(), `date_modified`=NOW(), `restore_id`='".$id."', `store_id`='".$store_id."'");
              $this->session->data['abandonedCart_ID'] = $id;
          } 
      } else {
          if (!empty($cart)) {
              $cart = json_encode($cart);
              $this->db->query("UPDATE `" . DB_PREFIX . "abandonedcarts` SET `cart` = '".$this->db->escape($cart)."', `last_page`='".$this->db->escape($lastpage)."', `date_modified`=NOW() WHERE `restore_id`='$id'");
          }
          if (isset($customer)) {
              $customer = json_encode($customer);
              $this->db->query("UPDATE `" . DB_PREFIX . "abandonedcarts` SET `customer_info` = '".$this->db->escape($customer)."', `last_page`='".$this->db->escape($lastpage)."', `date_modified`=NOW() WHERE `restore_id`='$id'");
          }
      }
    }
}
/* AbandonedCarts - End */
			
	public function index() {

        $data['facebook_page_id_FAE'] = $this->config->get('facebook_page_id');
        $data['facebook_jssdk_version_FAE'] = $this->config->get('facebook_jssdk_version');
        $data['facebook_messenger_enabled_FAE'] = $this->config->get('facebook_messenger_activated');
      
		$this->load->language('common/footer');

/* AbandonedCarts - Begin */
$this->load->model('setting/setting');

$abandonedCartsSettings = $this->model_setting_setting->getSetting('abandonedcarts', $this->config->get('store_id'));

if (isset($abandonedCartsSettings['abandonedcarts']['Enabled']) && $abandonedCartsSettings['abandonedcarts']['Enabled']=='yes') { 
    $this->register_abandonedCarts();
}
/* AbandonedCarts - End */
			

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		
				$data['text_logo_footer'] = $this->language->get('text_logo_footer');
				$data['text_desc_footer'] = $this->language->get('text_desc_footer');
				$data['text_address'] = $this->language->get('text_address');
				$data['text_email'] = $this->language->get('text_email');
				$data['text_telephone'] = $this->language->get('text_telephone');
				$data['text_instagram'] = $this->language->get('text_instagram');
				$data['address'] = $this->config->get('config_address');			
				$data['telephone'] = $this->config->get('config_telephone');			
				$data['email'] = $this->config->get('config_email');			
				$data['opentime'] = $this->config->get('config_open');			
				$data['fax'] = $this->config->get('config_fax');
				$data['block7'] = $this->load->controller('common/block7');
				$data['block8'] = $this->load->controller('common/block8');
				$data['block6'] = $this->load->controller('common/block6');
				$data['block4'] = $this->load->controller('common/block4');
				$data['block5'] = $this->load->controller('common/block5');
				if ($this->request->server['HTTPS']) {
					$server = $this->config->get('config_ssl');
				} else {
					$server = $this->config->get('config_url');
				}
				if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
					$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
				} else {
					$data['logo'] = '';
				}
				$data['name'] = $this->config->get('config_name');
				$data['home'] = $this->url->link('common/home');
			
		
		$data['whatsapp_number'] = $this->config->get('config_whatsappid');
		$data['store_telephone'] = $this->config->get('config_telephone');
		$data['store_email'] = $this->config->get('config_email');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
		
				'short-des' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 300),
			
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact');

			
			$data['config_whatsapptl'] = $this->config->get('config_whatsapptl');
			$data['config_whatsappid'] = $this->config->get('config_whatsappid');
			
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

 // BOF - Betaout Opencart mod
        $this->load->model('tool/betaout');
        $data['betaout_footer_text'] = $this->model_tool_betaout->getFooterText();
        // EOF - Betaout Opencart mod

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		return $this->load->view('common/footer', $data);
	}
	
	public function pinCheck() {
					$this->load->model('catalog/pincode');
					$pin = array();
					if(isset($_POST['pincode'])){
						$_SESSION['pin_check_status'] = "1";
						$pincode = addslashes($_POST['pincode']);
						$_SESSION['pincode'] = "<b>".addslashes($_POST['pincode'])."</b>";
						$pin = $this->model_catalog_pincode->getPin($pincode);
						$message_cod = $message_pre = $message_not = '';
						$message_cod = html_entity_decode($this->config->get('pincodesetting_cod_msg'), ENT_QUOTES, 'UTF-8');
						$message_pre = html_entity_decode($this->config->get('pincodesetting_prepaid_msg'), ENT_QUOTES, 'UTF-8'); 
						$message_not = html_entity_decode($this->config->get('pincodesetting_no_service_msg'), ENT_QUOTES, 'UTF-8'); 
						$text_color = $this->config->get('pincodesetting_text_color'); 
						
						if(!isset($message_cod) || $message_cod == ''){
							$message_cod  = "COD and Prepaid Service is Available At Your Location";
						}
						if(!isset($message_pre) || $message_pre == ''){
							$message_pre  = "Only Prepaid Service is Available At Your Location";
						}
						if(!isset($message_not) || $message_not == ''){
							$message_not  = "Service is not Available at your location yet";
						}
			
				if(isset($pin['id'])){
				$service = $pin['service_available'];
				$delivery_days = (int)filter_var($pin['delivery_time'], FILTER_SANITIZE_NUMBER_INT);
				$check_time = date("H");
				$day = "today";
				if($check_time >= 12) {
					$delivery_days += 1;
					$day = "tomorrow";
				}
				if($service == '1'){                    
                    echo "<span class='msg11'><i class='fa fa-map-marker'></i> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform2()' value='CHANGE' id='footer-button-change' class='button'  /></form></span><br/>";
                    echo "<span class='msg12'><b><i class='fa fa-check-square'></i> Get this by " . date("d M",strtotime('+' . $delivery_days . ' DAYS')) . " if you order by 12 PM " . $day . "</b></span>";
                    echo "<p class='codcheck'><i class='fa fa-check-square'></i> <b>Status :</b> <font color = '32CD32'>".$message_cod."</font></p><br/>";                   
                        
                    $_SESSION['pin_check_delivery'] ="<span class='msg12'><b><i class='fa fa-check-square'></i> Get this by " . date("d M",strtotime('+' . $delivery_days . ' DAYS')) . " if you order by 12 PM " . $day . "</b></span>";
                    $_SESSION['pin_check_result'] = "<p class='codcheck'><i class='fa fa-check-square'></i> <b>Status : </b> <font color = '32CD32'>".$message_cod."</font></p><br/>";                   
                }
                else if($service == '0'){
                    echo "<span class='msg11'><i class='fa fa-map-marker'></i> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform2()' value='CHANGE' id='footer-button-change' class='button'/></form></span><br/>";
                    echo "<span  class='msg12'><b><i class='fa fa-check-square'></i> Get this by " . date("d M",strtotime('+' . $delivery_days . ' DAYS')) . " if you order by 12 PM " . $day . "</b></span>";
                    echo "<p><i class='fa fa-check-square'></i> <b>Status :</b> <font color = '5EA4FF'>".$message_pre."</font></p><br/>";                   
                    
                    $_SESSION['pin_check_delivery'] = "<span  class='msg12'><b><i class='fa fa-check-square'></i> Get this by " . date("d M",strtotime('+' . $delivery_days . ' DAYS')) . " if you order by 12 PM " . $day . "</b></span>";
                    $_SESSION['pin_check_result'] = "<p><i class='fa fa-check-square'></i> <b>Status : </b> <font color = '5EA4FF'>".$message_pre."</font></p><br/>";                   
                }
                else{
                    echo "<span class='msg11'><i class='fa fa-map-marker'></i> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform2()' value='CHANGE' id='footer-button-change' class='button' /></form></span><br/>";
                    echo "<p><i class='fa fa-times'></i> <b>Status :</b></b><font color = 'FF1605'>".$message_not."</font></p><br/>";
                        
                    $_SESSION['pin_check_result'] = "<p><i class='fa fa-times'></i> <b>Status :</b></b> <font color = 'FF1605'>".$message_not."</font></p><br/>";
                    $_SESSION['pin_check_delivery'] = '';
                }
            }
            else{
                echo "<span class='msg11'><i class='fa fa-map-marker'></i> DELIVERY OPTION FOR : <b>".$_SESSION['pincode']."</b>&nbsp;&nbsp;<form style='display: inline;'><input type = 'button'  onClick = 'showform2()' value='CHANGE' id='footer-button-change' class='button'/></form></span><br/>";
                echo "<p><i class='fa fa-times'></i> <b>Status :</b> <font color = '".$text_color."'>".$message_not."</font></p><br/>";
                
                $_SESSION['pin_check_result'] = "<p><i class='fa fa-times'></i> <b>Status :</b></b><font color = '".$text_color."'>".$message_not."</font></p><br/>";
                $_SESSION['pin_check_delivery'] = '';
            }
                    }
    }
}
