<?php
class ControllerAccountWishList extends Controller {
	public function index() {

        // start: OCdevWizard SMWWL
        $this->load->model('ocdevwizard/ocdevwizard_setting');
        $smwwl_form_data = $this->model_ocdevwizard_ocdevwizard_setting->getSettingData('smart_wishlist_without_login_form_data');  
       
        if (isset($smwwl_form_data['activate']) && $smwwl_form_data['activate']) {
          $smwwl_status = 1;
        } else {
          $smwwl_status = 0;
        }
        // end: OCdevWizard SMWWL
      
		
        // start: OCdevWizard SMWWL
        if (!$this->customer->isLogged() && $smwwl_status == 0) {
        // end: OCdevWizard SMWWL
      
			$this->session->data['redirect'] = $this->url->link('account/wishlist', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
        // Added to cart
	    $this->document->addStyle('catalog/view/javascript/addtocart/noty.css');
		$this->document->addScript('catalog/view/javascript/addtocart/notice_add.min.js'); 
		// Added to cart
        
		$this->load->language('account/wishlist');

		$this->load->model('account/wishlist');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['remove'])) {
			// Remove Wishlist
			$this->model_account_wishlist->deleteWishlist($this->request->get['remove']);

			$this->session->data['success'] = $this->language->get('text_remove');

			$this->response->redirect($this->url->link('account/wishlist'));
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/wishlist')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_stock'] = $this->language->get('column_stock');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['products'] = array();

		$results = $this->model_account_wishlist->getWishlist();

		foreach ($results as $result) {
			$product_info = $this->model_catalog_product->getProduct($result['product_id']);

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_wishlist_width'), $this->config->get($this->config->get('config_theme') . '_image_wishlist_height'));
				} else {
					$image = false;
				}

				if ($product_info['quantity'] <= 0) {
					$stock = $product_info['stock_status'];
				} elseif ($this->config->get('config_stock_display')) {
					$stock = $product_info['quantity'];
				} else {
					$stock = $this->language->get('text_instock');
				}
				
				if($result['options']) {
	    		    $options_data = json_decode($result['options']);
	    		    $custom_name = $result['custom_name'];
	    		    $options = NULL;
	    		    $option_price = 0;
	    		    foreach ($options_data AS $product_option_id => $product_option_value_id) {
	    		        $product_option_data = NULL;
                        if(is_array($product_option_value_id)) {
                            foreach ($product_option_value_id AS $key => $product_option_value_id1) {
                                $product_option_data = NULL;
                                $product_option_data = $this->model_catalog_product->getProductOptionsData($product_info['product_id'], $product_option_id, $product_option_value_id1);
                                
                                $options[] = array(
                                    'option_name'  => $product_option_data['option_name'],
                                    'option_value' => $product_option_data['option_value_name']
                                );
                                $option_value_query = $this->db->query("SELECT price, price_prefix, option_special_price, option_special_price_prefix FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . (int)$product_option_value_id1 . "' AND product_option_id = '" . (int)$product_option_id . "' AND product_id = '" . (int)$result['product_id'] . "'");

					            if ($option_value_query->num_rows) {
						            if($option_value_query->row['option_special_price'] > 0) {
						                if ($option_value_query->row['option_special_price_prefix'] == '+') {
								            $option_price += $option_value_query->row['option_special_price'];
							            } elseif ($option_value_query->row['option_special_price_prefix'] == '-') {
								            $option_price -= $option_value_query->row['option_special_price'];
							            }
						            } else {
						                if ($option_value_query->row['price_prefix'] == '+') {
								            $option_price += $option_value_query->row['price'];
							            } elseif ($option_value_query->row['price_prefix'] == '-') {
								            $option_price -= $option_value_query->row['price'];
							            }
						            }
						        }
                            }
                        } else {
                            $product_option_data = $this->model_catalog_product->getProductOptionsData($product_info['product_id'], $product_option_id, $product_option_value_id);
                            $options[] = array(
                                'option_name'  => $product_option_data['option_name'],
                                'option_value' => $product_option_data['option_value_name']
                            );
                            $option_value_query = $this->db->query("SELECT price, price_prefix, option_special_price, option_special_price_prefix FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . (int)$product_option_value_id . "' AND product_option_id = '" . (int)$product_option_id . "' AND product_id = '" . (int)$result['product_id'] . "'");

					        if ($option_value_query->num_rows) {
						        if($option_value_query->row['option_special_price'] > 0) {
						            if ($option_value_query->row['option_special_price_prefix'] == '+') {
								        $option_price += $option_value_query->row['option_special_price'];
							        } elseif ($option_value_query->row['option_special_price_prefix'] == '-') {
								        $option_price -= $option_value_query->row['option_special_price'];
							        }
						        } else {
						            if ($option_value_query->row['price_prefix'] == '+') {
								        $option_price += $option_value_query->row['price'];
							        } elseif ($option_value_query->row['price_prefix'] == '-') {
								        $option_price -= $option_value_query->row['price'];
							        }
						        }
						    }
                        }
                    }
	    		} else {
	    		    $options_data = array();
	    		}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'] + $option_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'] + $option_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				$data['products'][] = array(
				    'wishlist_id'=> $result['id'],
					'product_id' => $product_info['product_id'],
					'thumb'      => $image,
					'name'       => $product_info['name'],
					'custom_name'=> $custom_name,
					'options'    => $options,
					'model'      => $product_info['model'],
					'stock'      => $stock,
					'price'      => $price,
					'special'    => $special,
					'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
					'remove'     => $this->url->link('account/wishlist', 'remove=' . $result['id'])
				);
			} else {
				$this->model_account_wishlist->deleteWishlist($result['product_id']);
			}
		}

		$data['continue'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/wishlist', $data));
	}

	public function add() {

        // start: OCdevWizard SMWWL
        $this->load->model('ocdevwizard/ocdevwizard_setting');
        $smwwl_form_data = $this->model_ocdevwizard_ocdevwizard_setting->getSettingData('smart_wishlist_without_login_form_data');  
       
        if (isset($smwwl_form_data['activate']) && $smwwl_form_data['activate']) {
          $smwwl_status = 1;
        } else {
          $smwwl_status = 0;
        }
        // end: OCdevWizard SMWWL
      
		$this->load->language('account/wishlist');

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		if (isset($this->request->post['option'])) {
			$option = array_filter($this->request->post['option']);
		} else {
			$option = NULL;
		}
		
		if (isset($this->request->post['my-creation'])) {
			$custom_name = $this->request->post['my-creation'];
		} else {
			$custom_name = NULL;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			
        // start: OCdevWizard SMWWL
        if ($this->customer->isLogged() || $smwwl_status == 1) {
        // end: OCdevWizard SMWWL
      
				// Edit customers cart
				$this->load->model('account/wishlist');

				$this->model_account_wishlist->addWishlist($this->request->post['product_id'], $option, $custom_name);

				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

				$json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
			} else {
				if (!isset($this->session->data['wishlist'])) {
					$this->session->data['wishlist'] = array();
				}

				$this->session->data['wishlist'][] = $this->request->post['product_id'];

				//iAnalytics code start
                if ($this->request->post['product_id']!=0) {
                  $iAproductID = $this->request->post['product_id'];
                  $iAproductInfo = $this->model_catalog_product->getProduct($iAproductID);
                  $iAtoday = date("Y-m-d"); 
                  $iAtime = date("H:i:s"); 
                  $iAFromIP = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '';
                  $iASpokenLanguages = (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '';

                  $iAdata = array(
                   'ianalytics_product_add_to_wishlist' => array (
                    'date' => $iAtoday,
                    'time' => $iAtime,
                    'from_ip' => $iAFromIP,
                    'spoken_languages' => $iASpokenLanguages,
                    'product_id' => $iAproductID,
                    'product_name' => $iAproductInfo['name'],
                    'product_model' => $iAproductInfo['model'],
                    'product_price' => $iAproductInfo['price'],
                    'product_quantity' => $iAproductInfo['quantity'],
                    'product_stock_status' => $iAproductInfo['stock_status'],
				  	'store_id' => $this->config->get('config_store_id'),
                   )
                  );			

                  foreach ($iAdata as $table => $tableData) {
                   $insertFields = array();
                   $insertData = array();

                   foreach ($tableData as $fieldName => $fieldValue) {
                    $insertFields[] = $fieldName;
                    $insertData[] = '"'.$this->db->escape($fieldValue).'"';
                   }

                   $this->db->query('INSERT INTO ' . DB_PREFIX . $table . ' ('. implode(',', $insertFields) .') VALUES ('.implode(',', $insertData).')');
                  }
				}
				//iAnalytics code end
			

				$this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);

				$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

				$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
			}
			// Added to cart	
				$json['notice_add_layout'] = $this->config->get('notice_add_layout');
				$json['notice_add_timeout'] = $this->config->get('notice_add_timeout');
				$json['notice_add_status'] = $this->config->get('notice_add_status');
				$this->load->model('tool/image');				
				if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
				} else {
						$image = '';
				}					
				$json['href'] = $image;		
			// Added to cart
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function addtocart() {
	    $this->load->model('account/wishlist');
	    $this->load->model('catalog/product');
	    $this->load->language('account/wishlist');
	    $json = array();
		$json['notice_add_layout'] = $this->config->get('notice_add_layout');
		$json['notice_add_timeout'] = $this->config->get('notice_add_timeout');
		$json['notice_add_status'] = $this->config->get('notice_add_status');
		
	    if (isset($this->request->get['wishlist_id']) && $this->request->get['wishlist_id'] > 0) {
	        $result = $this->model_account_wishlist->getWishlistById($this->request->get['wishlist_id']);
	        if($result) {
	            if($result['options']) {
	                $options_data = json_decode($result['options']);
	            } else {
	                $options_data = NULL;
	            }
	            $product_id = $result['product_id'];
	            $product_info = $this->model_catalog_product->getProduct($product_id);
	        }
	        
	        $this->cart->add($product_id, 1, $options_data);
	        
	        $this->load->model('tool/image');
			if ($product_info['image']) {
				$image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
			} else {
				$image = '';
			}
			$json['href'] = $image;
			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
			$json['success'] = sprintf($this->language->get('text_success_cart'), $this->url->link('product/product', 'product_id=' . $product_id), $product_info['name'], $this->url->link('checkout/cart'));
	    } else {
	        $json['error'] = "Error";
	    }
	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
