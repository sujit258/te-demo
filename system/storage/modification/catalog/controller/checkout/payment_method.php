<?php
class ControllerCheckoutPaymentMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		if (isset($this->session->data['payment_address'])) {
			// Totals
			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array.
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			$this->load->model('extension/extension');

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);
					
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			// Payment Methods
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('payment');

			$recurring = $this->cart->hasRecurringProducts();

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/payment/' . $result['code']);

					$method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

					if ($method) {
						if ($recurring) {
							if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
								$method_data[$result['code']] = $method;
							}
						} else {
							$method_data[$result['code']] = $method;
						}
					}
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			$this->session->data['payment_methods'] = $method_data;
		}

		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');

		if (empty($this->session->data['payment_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

 
				$product_cod_install_status = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "extension WHERE type = 'module' AND code = 'product_cod'")->row;
				
				$cod_status = $this->config->get('cod_status');
				
				if($product_cod_install_status['total'] > 0 && isset($cod_status) && $cod_status == 1){
				
					if(!isset($this->session->data['payment_methods']['cod'])){
						if (empty($this->session->data['payment_methods'])) {
					
							$data['error_warning'] .= "<br />";
						}
						if($this->config->get('cod_product_error_message')){
				
							$data['error_warning'] .= $this->config->get('cod_product_error_message');
						}
						else{
						
							$data['error_warning'] .= "You have Products in your cart on which COD SERVICE Not Available.";
						}
					}
				}
			
 
				$this->load->model('catalog/pincode');
				$pincode_install_status = $this->model_catalog_pincode->is_install();
				$cod_status = $this->config->get('cod_status');
				if($pincode_install_status['total'] > 0 && isset($cod_status) && $cod_status == 1){
					if(!isset($this->session->data['payment_methods']['cod'])){
						if (empty($this->session->data['payment_methods'])) {
							$data['error_warning'] .= "<br />";
						}
						if($this->config->get('pincode_cod_error')){
							$data['error_warning'] .= html_entity_decode($this->config->get('pincodesetting_cod_error'), ENT_QUOTES, 'UTF-8');
						}
					}
				}
			
		if (isset($this->session->data['payment_methods'])) {
			$data['payment_methods'] = $this->session->data['payment_methods'];
		} else {
			$data['payment_methods'] = array();
		}

		if (isset($this->session->data['payment_method']['code'])) {
			$data['code'] = $this->session->data['payment_method']['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		$data['scripts'] = $this->document->getScripts();

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_checkout_id'), true), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->session->data['agree'])) {
			$data['agree'] = $this->session->data['agree'];
		} else {
			$data['agree'] = '';
		}

		$this->response->setOutput($this->load->view('checkout/payment_method', $data));
	}


				/* iAnalytics - Begin */
				protected function register_iAnalytics() {
					$iAstage = (!empty($this->session->data['iAnalyticsStage'])) ? $this->session->data['iAnalyticsStage'] : '0';
					$iAtoday = date("Y-m-d"); 
					$iAtime = date("H:i:s"); 
					$iAFromIP = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '*HiddenIP*';
					$iASpokenLanguages = (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '';
					
					$iAdata = array(
						'ianalytics_funnel_data' => array (
							'date' => $iAtoday,
							'time' => $iAtime,
							'from_ip' => $iAFromIP,
							'spoken_languages' => $iASpokenLanguages,
							'stage' => '4',
				  			'store_id' => $this->config->get('config_store_id'),
						)
					);	
					
					if (isset($this->session->data['iAnalyticsStage']) && ((int)$this->session->data['iAnalyticsStage']<4)) {
						foreach ($iAdata as $table => $tableData) {
							$insertFields = array();
							$insertData = array();
							
							foreach ($tableData as $fieldName => $fieldValue) {
								$insertFields[] = $fieldName;
								$insertData[] = '"'.$this->db->escape($fieldValue).'"';
							}
							$this->db->query('INSERT INTO ' . DB_PREFIX . $table . ' ('. implode(',', $insertFields) .') VALUES ('.implode(',', $insertData).')');
						}
						$this->session->data['iAnalyticsStage'] = '4';
					}
				}
				/* iAnalytics - End */
			
	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if payment address has been set.
		if (!isset($this->session->data['payment_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!isset($this->request->post['payment_method'])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		} elseif (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		}

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		if (!$json) {
			$this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];

			$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		}

		$this->response->addHeader('Content-Type: application/json');

				/* iAnalytics - Begin */
				$this->load->model('setting/setting');

                $ianalytics_settings = $this->model_setting_setting->getSetting('iAnalytics', $this->config->get('config_store_id'));
                $iAnalyticsSettings = $ianalytics_settings['ianalytics'];

				if ($iAnalyticsSettings['Enabled']=='yes' && $iAnalyticsSettings['AfterSaleData']=='yes') {
					$this->register_iAnalytics();
				}
				/* iAnalytics - End */
			
		$this->response->setOutput(json_encode($json));
	}
}
