<?php
class ModelReportProduct extends Model {
	public function getProductsViewed($data = array()) {
		$sql = "SELECT pd.name, p.model, p.viewed FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.viewed > 0 ORDER BY p.viewed DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalProductViews() {
		$query = $this->db->query("SELECT SUM(viewed) AS total FROM " . DB_PREFIX . "product");

		return $query->row['total'];
	}

	public function getTotalProductsViewed() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE viewed > 0");

		return $query->row['total'];
	}

	public function reset() {
		$this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = '0'");
	}


					public function getPurchasedAdvanced($data = array()) {
		if (empty($data['filter_product_id'])) {
			$sql = "SELECT op.product_id as product_id, CONCAT(o.payment_firstname, ' ', o.payment_lastname) AS customer, op.name, op.model, 
			SUM(op.quantity) AS quantity, SUM((op.total + op.tax) * op.quantity) AS total FROM " . DB_PREFIX . "order_product op 
			LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";
		} else {
			// single product
			$sql = "SELECT op.quantity as quantity, ((op.total + op.tax) * op.quantity) AS total, os.name as order_status, o.order_id as order_id, op.product_id as product_id, CONCAT(o.payment_firstname, ' ', o.payment_lastname) AS customer, 
			o.email as email, o.date_added as order_date, o.payment_country as country, op.name, op.model FROM " . DB_PREFIX . "order_product op 
			LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) LEFT JOIN `" . DB_PREFIX . "order_status` os ON (os.order_status_id = o.order_status_id)";
		}
		
		if (empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id > '0'";
		} else {
			// single product
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		}

		if (!empty($data['filter_product_id'])) {
			$sql .= " AND op.product_id='" . $data['filter_product_id'] . "'";
		} 

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_date_shipped_start'])) {
			$sql .= " AND DATE(o.date_shipped) >= '" . $this->db->escape($data['filter_date_shipped_start']) . "'";
		}

		if (!empty($data['filter_date_shipped_end'])) {
			$sql .= " AND DATE(o.date_shipped) <= '" . $this->db->escape($data['filter_date_shipped_end']) . "'";
		}
		
		if (empty($data['filter_product_id'])) {
			$sql .= " GROUP BY op.product_id ORDER BY total DESC";
		} else {
			$sql .= " ORDER BY order_date DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
				
	public function getPurchased($data = array()) {
		$sql = "SELECT op.order_product_id, op.name, op.model, SUM(op.quantity) AS quantity, SUM(op.price + (op.tax * op.quantity)) AS total FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		$sql .= " GROUP BY op.product_id ORDER BY total DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		
		/*foreach($query->rows AS $order_product) {
		    $options_query = $this->db->query("SELECT value FROM " . DB_PREFIX . "order_option WHERE order_product_id = '" . $order_product['order_product_id'] . "'");
		    if($options_query->num_rows == 1) {
		        $option = $options_query->row['value'];
		    } else {
		        $option = '';
		    }
		    $order_products_data[] = array(
		        'name'      => $order_product['name'],
		        'model'     => $order_product['model'],
		        'option'    => $option,
		        'quantity'  => $order_product['quantity'],
		        'total'     => $order_product['total']
		    );
		}

		return $order_products_data;*/
		return $query->rows;
	}

	public function getTotalPurchased($data) {
		$sql = "SELECT COUNT(DISTINCT op.product_id) AS total FROM `" . DB_PREFIX . "order_product` op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}
