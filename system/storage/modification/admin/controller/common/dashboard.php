<?php
class ControllerCommonDashboard extends Controller {
	public function index() {

        $this->load->language('common/dashboard');
        try {
          $operation = ', dashboard module';

          // validates the plugin
          $all_error_messages = $this->load->controller(
            'extension/facebookadsextension/validate');
          if (sizeof($all_error_messages) > 0) {
            throw new Exception($this->language->get(
              'text_warning_facebook_fae_install_problem'));
          }

          $this->load->controller(
            'extension/facebookproduct/validateFAEAndCatalogSetupAndProductUploadComplete',
            $operation);

          $hasNewUpgradeAvailable = $this->load->controller(
            'extension/facebookadsextension/hasNewUpgradeAvailable',
            $operation);
          $data['plugin_upgrade_message'] = ($hasNewUpgradeAvailable)
            ? FacebookCommonUtils::PLUGIN_UPGRADE_MESSAGE
            : '';
        } catch (Exception $e) {
          $data['error_facebook_sync'] =
            ($e->getCode() === FacebookGraphAPIError::ACCESS_TOKEN_EXCEPTION_CODE)
            ? $this->language->get('text_warning_facebook_access_token_problem')
            : $e->getMessage();
        }
      
		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		// Check install directory exists
		if (is_dir(dirname(DIR_APPLICATION) . '/install')) {
			$data['error_install'] = $this->language->get('error_install');
		} else {
			$data['error_install'] = '';
		}

		// Dashboard Extensions
		$dashboards = array();

		$this->load->model('extension/extension');

		// Get a list of installed modules
		$extensions = $this->model_extension_extension->getInstalled('dashboard');
		
		// Add all the modules which have multiple settings for each module
		$i = 0;
		foreach ($extensions as $code) {
			if ($this->config->get('dashboard_' . $code . '_status') && $this->user->hasPermission('access', 'extension/dashboard/' . $code)) {
				$output = $this->load->controller('extension/dashboard/' . $code . '/dashboard');
				
				if ($output) {
					$dashboards[] = array(
						'code'       => $code,
						'width'      => $this->config->get('dashboard_' . $code . '_width'),
						'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
						'output'     => $output
					);
				}
				$i++;
			}
		}
		
		// For MTD Sale
		$date_start = date('Y-m-01');
	    $date_end = date('Y-m-d');
	    
		$this->load->model('report/sale');
		$total_mtd = $this->model_report_sale->getMtdSales($date_start, $date_end);

		if ($total_mtd > 1000000000000) {
			$total_mtd = round($total_mtd / 1000000000000, 1) . 'T';
		} elseif ($total_mtd > 1000000000) {
			$total_mtd = round($total_mtd / 1000000000, 1) . 'B';
		} elseif ($total_mtd > 1000000) {
			$total_mtd = round($total_mtd / 1000000, 1) . 'M';
		} elseif ($total_mtd > 1000) {
			$total_mtd = round($total_mtd / 1000, 1) . 'K';
		} else {
			$total_mtd = round($total_mtd);
		}
		$sale_url = $this->url->link('sale/order', 'token=' . $this->session->data['token'], true);
		
		$mtd_sale_output = '<div class="tile"><div class="tile-heading">' . $this->language->get('heading_mtd') . '</div><div class="tile-body"><i class="fa fa-credit-card"></i><h2 class="pull-right">' . $total_mtd . '</h2></div><div class="tile-footer"><a href="' . $sale_url . '">' . $this->language->get('text_view') . '</a></div></div>';
        $code = 'sale';
        if ($this->user->hasPermission('access', 'extension/dashboard/' . $code)) {
            $dashboards[$i] = array(
		    	'code'       => $code,
		    	'width'      => $this->config->get('dashboard_' . $code . '_width'),
		    	'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
		    	'output'     => $mtd_sale_output
		    );
        }
        $i++;
        
        // For Product MTD Count
        $total_mtd_products_count = $this->model_report_sale->getProductsMtdSalesCount($date_start, $date_end);

		if ($total_mtd_products_count > 1000000000000) {
			$total_mtd_products_count = round($total_mtd_products_count / 1000000000000, 1) . 'T';
		} elseif ($total_mtd_products_count > 1000000000) {
			$total_mtd_products_count = round($total_mtd_products_count / 1000000000, 1) . 'B';
		} elseif ($total_mtd_products_count > 1000000) {
			$total_mtd_products_count = round($total_mtd_products_count / 1000000, 1) . 'M';
		} elseif ($total_mtd_products_count > 1000) {
			$total_mtd_products_count = round($total_mtd_products_count / 1000, 1) . 'K';
		} else {
			$total_mtd_products_count = round($total_mtd_products_count);
		}
		
		$filters = '&filter_date_shipped_start=' . $date_start . '&filter_date_shipped_end=' . $date_end . '&filter_order_status_id=3';
		$product_sale_url = $this->url->link('report/product_purchased_advanced', 'token=' . $this->session->data['token'] . $filters, true);
		
		$mtd_product_output = '<div class="tile"><div class="tile-heading">' . $this->language->get('heading_products_mtd') . '</div><div class="tile-body"><i class="fa fa-credit-card"></i><h2 class="pull-right">' . $total_mtd_products_count . '</h2></div><div class="tile-footer"><a href="' . $product_sale_url . '">' . $this->language->get('text_view') . '</a></div></div>';
        $code = 'sale';
        if ($this->user->hasPermission('access', 'extension/dashboard/' . $code)) {
            $dashboards[$i] = array(
		    	'code'       => $code,
		    	'width'      => $this->config->get('dashboard_' . $code . '_width'),
		    	'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
		    	'output'     => $mtd_product_output
		    );
        }
        $i++;

		$sort_order = array();

		foreach ($dashboards as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $dashboards);
		
		// Split the array so the columns width is not more than 12 on each row.
		$width = 0;
		$column = array();
		$data['rows'] = array();
		
		foreach ($dashboards as $dashboard) {
			$column[] = $dashboard;
			
			$width = ($width + $dashboard['width']);
			
			if ($width >= 12) {
				$data['rows'][] = $column;
				
				$width = 0;
				$column = array();
			}
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// Run currency update
		if ($this->config->get('config_currency_auto')) {
			$this->load->model('localisation/currency');

			$this->model_localisation_currency->refresh();
		}

		$this->response->setOutput($this->load->view('common/dashboard', $data));
	}
}