<?php
class ControllerCommonColumnLeft extends Controller {
	public function index() {

        $token = (isset($this->session->data['user_token']))
          ? $this->session->data['user_token']
          : $this->session->data['token'];
        $tokenString = 'token=' . $token . '&user_token=' . $token;
        $facebookAdsExtension = array();
        $facebookAdsExtension[] = array(
          'name'     => 'Facebook Ads Extension',
          'href'     => $this->url->link('extension/facebookadsextension', $tokenString, true),
          'children' => array()
        );

        $data['menus'][] = array(
          'id'       => 'menu-facebook-ads-extension',
          'icon'     => 'fa-facebook-square',
          'name'     => 'Facebook Ads Extension',
          'href'     => '',
          'children' => $facebookAdsExtension
        );
      
		if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->load->language('common/column_left');
	
			$this->load->model('user/user');
	
			$this->load->model('tool/image');
	
			$user_info = $this->model_user_user->getUser($this->user->getId());
	
			if ($user_info) {
				$data['firstname'] = $user_info['firstname'];
				$data['lastname'] = $user_info['lastname'];
	
				$data['user_group'] = $user_info['user_group'];
	
				if (is_file(DIR_IMAGE . $user_info['image'])) {
					$data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
				} else {
					$data['image'] = '';
				}
			} else {
				$data['firstname'] = '';
				$data['lastname'] = '';
				$data['user_group'] = '';
				$data['image'] = '';
			}			
		
			// Create a 3 level menu array
			// Level 2 can not have children
			
			// Menu
			$data['menus'][] = array(
				'id'       => 'menu-dashboard',
				'icon'	   => 'fa-dashboard',
				'name'	   => $this->language->get('text_dashboard'),
				'href'     => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
			

      		$newsblog = array();

			if ($this->user->hasPermission('access', 'newsblog/category')) {
				$newsblog[] = array(
					'name'	   => $this->language->get('text_newsblog_category'),
					'href'     => $this->url->link('newsblog/category', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'newsblog/article')) {
				$newsblog[] = array(
					'name'	   => $this->language->get('text_newsblog_article'),
					'href'     => $this->url->link('newsblog/article', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}

			if ($newsblog) {
				$data['menus'][] = array(
					'id'       => 'menu-newsblog',
					'icon'	   => 'fa-newspaper-o',
					'name'	   => $this->language->get('text_newsblog'),
					'href'     => '',
					'children' => $newsblog
				);
			}
      
			// Catalog

			/* mps */
			if ($this->user->hasPermission('access', 'mpblog/mpblogdashboard')) {
				$data['menus'][] = array(
					'id'       => 'menu-mpblog',
					'icon'	   => 'fa fa-file-text-o fw', 
					'name'	   => $this->language->get('text_mpblog'),
					'href'     => $this->url->link('mpblog/mpblogdashboard', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);		
			}
			/* mpe */
			
			$catalog = array();
			

				// Categories

				$categories = array();
			
			if ($this->user->hasPermission('access', 'catalog/category')) {

				$categories[] = array(
					'name'	   => $this->language->get('text_category'),
					'href'     => $this->url->link('catalog/category', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);

			}

			$this->load->language('catalog/occategorythumbnail');

			if($this->user->hasPermission('access', 'catalog/occategorythumbnail')) {
				$categories[] = array(
					'name'	   => $this->language->get('text_thumbnail'),
					'href'     => $this->url->link('catalog/occategorythumbnail', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}

			/*		
			
				$catalog[] = array(
					'name'	   => $this->language->get('text_category'),
					'href'     => $this->url->link('catalog/category', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			

				*/

				if($categories) {
					$catalog[] = array(
						'name'	   => $this->language->get('text_category'),
						'href'     => '',
						'children' => $categories
					);
				}
			
			if ($this->user->hasPermission('access', 'catalog/product')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_product'),
					'href'     => $this->url->link('catalog/product', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/deals')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_deals'),
					'href'     => $this->url->link('catalog/deals', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/media_articles')) {
				$catalog[] = array(
					'name'	   => "Media Atricles",
					'href'     => $this->url->link('catalog/media_articles', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/product_coupon_link_generator')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_product_coupon_link'),
					'href'     => $this->url->link('catalog/product_coupon_link_generator', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			

				$this->load->language('catalog/octestimonial');

				if ($this->user->hasPermission('access', 'catalog/octestimonial')) {
					$catalog[] = array(
						'name'	   => $this->language->get('text_testimonial'),
						'href'     => $this->url->link('catalog/octestimonial', 'token=' . $this->session->data['token'], true),
						'children' => array()
					);
				}
			
			if ($this->user->hasPermission('access', 'catalog/recurring')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_recurring'),
					'href'     => $this->url->link('catalog/recurring', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/filter')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_filter'),
					'href'     => $this->url->link('catalog/filter', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			// Attributes
			$attribute = array();
			
			if ($this->user->hasPermission('access', 'catalog/attribute')) {
				$attribute[] = array(
					'name'     => $this->language->get('text_attribute'),
					'href'     => $this->url->link('catalog/attribute', 'token=' . $this->session->data['token'], true),
					'children' => array()	
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/attribute_group')) {
				$attribute[] = array(
					'name'	   => $this->language->get('text_attribute_group'),
					'href'     => $this->url->link('catalog/attribute_group', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($attribute) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_attribute'),
					'href'     => '',
					'children' => $attribute
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/option')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_option'),
					'href'     => $this->url->link('catalog/option', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/manufacturer')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_manufacturer'),
					'href'     => $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/download')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_download'),
					'href'     => $this->url->link('catalog/download', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/review')) {		
				$catalog[] = array(
					'name'	   => $this->language->get('text_review'),
					'href'     => $this->url->link('catalog/review', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);		
			}
			
			if ($this->user->hasPermission('access', 'catalog/recycles')) {		
				$catalog[] = array(
					'name'	   => $this->language->get('Recycles'),
					'href'     => $this->url->link('catalog/recycles', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);		
			}
			
			if ($this->user->hasPermission('access', 'catalog/information')) {		
				$catalog[] = array(
					'name'	   => $this->language->get('text_information'),
					'href'     => $this->url->link('catalog/information', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);					
			}
			
				$recipe = array();
			
			if ($this->user->hasPermission('access', 'catalog/recipe')) {
				$recipe[] = array(
					'name'     => $this->language->get('text_recipe'),
					'href'     => $this->url->link('catalog/recipe', 'token=' . $this->session->data['token'], true),
					'children' => array()	
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/recipe_review')) {
				$recipe[] = array(
					'name'	   => $this->language->get('text_recipe_review'),
					'href'     => $this->url->link('catalog/recipe_review', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/recipe_category')) {
				$recipe[] = array(
					'name'	   => $this->language->get('text_recipe_category'),
					'href'     => $this->url->link('catalog/recipe_category', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($recipe) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_recipes'),
					'href'     => '',
					'children' => $recipe
				);
			}
			
			
			


        if ($this->user->hasPermission('access', 'catalog/salescombopge')) {
          $catalog[] = array(
            'name'     => $this->language->get('text_salescombopge'),
            'href'     => $this->url->link('catalog/salescombopge', 'token=' . $this->session->data['token'], true),
            'children' => array()   
          );
        }
        
        
			if ($catalog) {
				$data['menus'][] = array(
					'id'       => 'menu-catalog',
					'icon'	   => 'fa-tags', 
					'name'	   => $this->language->get('text_catalog'),
					'href'     => '',
					'children' => $catalog
				);		
			}
			
	
			// Extension
			$extension = array();
			/*
			if ($this->user->hasPermission('access', 'extension/store')) {		
				$extension[] = array(
					'name'	   => $this->language->get('text_store'),
					'href'     => $this->url->link('extension/store', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);					
			}
			*/
			
			if ($this->user->hasPermission('access', 'catalog/headermenu')) {		
				$extension[] = array(
					'name'	   => "Menu",
					'href'     => $this->url->link('catalog/headermenu', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);					
			}
			
			
			if ($this->user->hasPermission('access', 'extension/installer')) {		
				$extension[] = array(
					'name'	   => $this->language->get('text_installer'),
					'href'     => $this->url->link('extension/installer', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);					
			}	
			
			if ($this->user->hasPermission('access', 'extension/extension')) {		
				$extension[] = array(
					'name'	   => $this->language->get('text_extension'),
					'href'     => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
					
			if ($this->user->hasPermission('access', 'extension/modification')) {
				$extension[] = array(
					'name'	   => $this->language->get('text_modification'),
					'href'     => $this->url->link('extension/modification', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'extension/event')) {
				$extension[] = array(
					'name'	   => $this->language->get('text_event'),
					'href'     => $this->url->link('extension/event', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
					
			if ($extension) {					
				$data['menus'][] = array(
					'id'       => 'menu-extension',
					'icon'	   => 'fa-puzzle-piece', 
					'name'	   => $this->language->get('text_extension'),
					'href'     => '',
					'children' => $extension
				);		
			}
			
			// Design

        // start: OCdevWizard
        $ocdevwizard = array();

        // start: OCdevWizard SMWWL
        $smwwl = array();

        $smwwl[] = array(
          'name'     => $this->language->get('text_smart_wishlist_without_login_setting_left_menu'),
          'href'     => $this->url->link('ocdevwizard/smart_wishlist_without_login', 'token=' . $this->session->data['token'], true),
          'children' => array()
        );    

        $ocdevwizard[] = array(
          'name'     => $this->language->get('text_smart_wishlist_without_login_left_menu'),
          'href'     => '',
          'children' => $smwwl
        );
        // end: OCdevWizard SMWWL
      
        
        $data['menus'][] = array(
          'id'       => 'menu-ocdevwizard',
          'icon'     => 'fa fa-lightbulb-o fa-fw', 
          'name'     => $this->language->get('text_ocdevwizard'),
          'href'     => '',
          'children' => $ocdevwizard
        );
        // end: OCdevWizard
      
			$design = array();
			
			if ($this->user->hasPermission('access', 'design/layout')) {
				$design[] = array(
					'name'	   => $this->language->get('text_layout'),
					'href'     => $this->url->link('design/layout', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			/*
			if ($this->user->hasPermission('access', 'design/menu')) {
				$design[] = array(
					'name'	   => $this->language->get('text_menu'),
					'href'     => $this->url->link('design/menu', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			*/	
			/*	
			if ($this->user->hasPermission('access', 'design/theme')) {	
				$design[] = array(
					'name'	   => $this->language->get('text_theme'),
					'href'     => $this->url->link('design/theme', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'design/language')) {
				$design[] = array(
					'name'	   => $this->language->get('text_translation'),
					'href'     => $this->url->link('design/language', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			*/	
			if ($this->user->hasPermission('access', 'design/banner')) {
				$design[] = array(
					'name'	   => $this->language->get('text_banner'),
					'href'     => $this->url->link('design/banner', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'tltslideshow/tltslideshow')) {
				$design[] = array(
					'name'     => $this->language->get('Home Slider'),
					'href'     => $this->url->link('tltslideshow/tltslideshow', 'token=' . $this->session->data['token'], true),
					'children' => array()	
				);
			}
			
			if ($design) {
				$data['menus'][] = array(
					'id'       => 'menu-design',
					'icon'	   => 'fa-television', 
					'name'	   => $this->language->get('text_design'),
					'href'     => '',
					'children' => $design
				);	
			}
			
			// Sales
			$sale = array();
			
			if ($this->user->hasPermission('access', 'sale/order')) {
				$sale[] = array(
					'name'	   => $this->language->get('text_order'),
					'href'     => $this->url->link('sale/order', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'sale/recurring')) {	
				$sale[] = array(
					'name'	   => $this->language->get('text_recurring'),
					'href'     => $this->url->link('sale/recurring', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'sale/return')) {
				$sale[] = array(
					'name'	   => $this->language->get('text_return'),
					'href'     => $this->url->link('sale/return', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			// Voucher
			$voucher = array();
			
			if ($this->user->hasPermission('access', 'sale/voucher')) {
				$voucher[] = array(
					'name'	   => $this->language->get('text_voucher'),
					'href'     => $this->url->link('sale/voucher', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'sale/voucher_theme')) {
				$voucher[] = array(
					'name'	   => $this->language->get('text_voucher_theme'),
					'href'     => $this->url->link('sale/voucher_theme', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($voucher) {
				$sale[] = array(
					'name'	   => $this->language->get('text_voucher'),
					'href'     => '',
					'children' => $voucher		
				);		
			}
			
			if ($sale) {
				$data['menus'][] = array(
					'id'       => 'menu-sale',
					'icon'	   => 'fa-shopping-cart', 
					'name'	   => $this->language->get('text_sale'),
					'href'     => '',
					'children' => $sale
				);
			}
			
			// Customer

            /* mps $this->user->hasPermission('access', 'extension/module/manageawb') and  */
            if ($this->config->get('delhivery_lastmile_status')) {
                $data['menus'][] = array(
                    'id'       => 'menu-delhivery_lastmile',
                    'icon'     => 'fa fa-file-text-o fw', 
                    'name'     => $this->language->get('text_delhivery'),
                    'children' => array(
                                    array(
                                        'name'     => 'Manage AWB',
                                        'href'     => $this->url->link('extension/module/manageawb', 'token=' . $this->session->data['token'], true)
                                    ),
                                    array(
                                        'name'     => 'Manage Pincode',
                                        'href'     => $this->url->link('extension/module/managepincode', 'token=' . $this->session->data['token'], true)
                                    ),
                                    array(
                                        'name'     => 'Manage Location',
                                        'href'     => $this->url->link('extension/module/managelocation', 'token=' . $this->session->data['token'], true)
                                    )
                                )
                );      
            }
            /* mpe */
            
			$customer = array();
			
			if ($this->user->hasPermission('access', 'customer/customer')) {
				$customer[] = array(
					'name'	   => $this->language->get('text_customer'),
					'href'     => $this->url->link('customer/customer', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'customer/te_labs_customer')) {
				$customer[] = array(
					'name'	   => "TE Labs Customers",
					'href'     => $this->url->link('customer/te_labs_customer', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'customer/customer_group')) {
				$customer[] = array(
					'name'	   => $this->language->get('text_customer_group'),
					'href'     => $this->url->link('customer/customer_group', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'catalog/partner')) {		
				$customer[] = array(
					'name'	   => $this->language->get('Partner With us'),
					'href'     => $this->url->link('catalog/partner', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);					
			}
			
			if ($this->user->hasPermission('access', 'customer/customer')) {
				$customer[] = array(
					'name'	   => $this->language->get('text_customer_export'),
					'href'     => $this->url->link('extension/module/customer_export', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			
			if ($this->user->hasPermission('access', 'customer/custom_field')) {		
				$customer[] = array(
					'name'	   => $this->language->get('text_custom_field'),
					'href'     => $this->url->link('customer/custom_field', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			
			if ($customer) {
				$data['menus'][] = array(
					'id'       => 'menu-customer',
					'icon'	   => 'fa-user', 
					'name'	   => $this->language->get('text_customer'),
					'href'     => '',
					'children' => $customer
				);	
			}
			
			//instashop
			
				// Marketing
			$instagram = array();
			
			if ($this->user->hasPermission('access', 'extension/module/instagramshopgallery')) {
				$instagram[] = array(
					'name'	   => $this->language->get('InstaGram Shop'),
					'href'     => $this->url->link('extension/module/instagramshopgallery', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($instagram) {
				$data['menus'][] = array(
					'id'       => 'menu-insta',
					'icon'	   => 'fa-tags', 
					'name'	   => $this->language->get('Instashop'),
					'href'     => '',
					'children' => $instagram
				);	
			}
			
			
			// Marketing
			$marketing = array();
			

                $this->load->language('extension/module/ocblog');

                $blog_menu = array();

                if ($this->user->hasPermission('access', 'blog/article')) {
                    $blog_menu[] = array(
                        'name' => $this->language->get('text_blog_article'),
                        'href' => $this->url->link('blog/article', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }

                if ($this->user->hasPermission('access', 'blog/articlelist')) {
                    $blog_menu[] = array(
                        'name' => $this->language->get('text_blog_article_list'),
                        'href' => $this->url->link('blog/articlelist', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }

                if($blog_menu) {
                    $data['menus'][] = array(
                        'id'       => 'menu-blog',
                        'icon'     => 'fa-pencil-square-o', 
                        'name'     => $this->language->get('text_blog'),
                        'href'     => '',
                        'children' => $blog_menu
                    );
                }
            
			if ($this->user->hasPermission('access', 'marketing/marketing')) {
				$marketing[] = array(
					'name'	   => $this->language->get('text_marketing'),
					'href'     => $this->url->link('marketing/marketing', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'marketing/affiliate')) {
				$marketing[] = array(
					'name'	   => $this->language->get('text_affiliate'),
					'href'     => $this->url->link('marketing/affiliate', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'marketing/coupon')) {	
				$marketing[] = array(
					'name'	   => $this->language->get('text_coupon'),
					'href'     => $this->url->link('marketing/coupon', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'marketing/contact')) {
				$marketing[] = array(
					'name'	   => $this->language->get('text_contact'),
					'href'     => $this->url->link('marketing/contact', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($marketing) {
				$data['menus'][] = array(
					'id'       => 'menu-marketing',
					'icon'	   => 'fa-share-alt', 
					'name'	   => $this->language->get('text_marketing'),
					'href'     => '',
					'children' => $marketing
				);	
			}
			
			// System
			$system = array();
			
			if ($this->user->hasPermission('access', 'setting/setting')) {
				$system[] = array(
					'name'	   => $this->language->get('text_setting'),
					'href'     => $this->url->link('setting/store', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
		
			// Users
			$user = array();
			
			
			if ($this->user->hasPermission('access', 'extension/hb_snippets')) {
				$system[] = array(
					'name'	   => $this->language->get('text_seo_snippet'),
					'href'     =>  $this->url->link('extension/hb_snippets', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
				
			if ($this->user->hasPermission('access', 'user/user')) {
				$user[] = array(
					'name'	   => $this->language->get('text_users'),
					'href'     => $this->url->link('user/user', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'user/user_permission')) {	
				$user[] = array(
					'name'	   => $this->language->get('text_user_group'),
					'href'     => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'user/api')) {		
				$user[] = array(
					'name'	   => $this->language->get('text_api'),
					'href'     => $this->url->link('user/api', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($user) {
				$system[] = array(
					'name'	   => $this->language->get('text_users'),
					'href'     => '',
					'children' => $user		
				);
			}
			
			// Localisation
			$localisation = array();
			
			if ($this->user->hasPermission('access', 'localisation/location')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_location'),
					'href'     => $this->url->link('localisation/location', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'localisation/language')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_language'),
					'href'     => $this->url->link('localisation/language', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/currency')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_currency'),
					'href'     => $this->url->link('localisation/currency', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/stock_status')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_stock_status'),
					'href'     => $this->url->link('localisation/stock_status', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/order_status')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_order_status'),
					'href'     => $this->url->link('localisation/order_status', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			// Returns
			$return = array();
			
			if ($this->user->hasPermission('access', 'localisation/return_status')) {
				$return[] = array(
					'name'	   => $this->language->get('text_return_status'),
					'href'     => $this->url->link('localisation/return_status', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/return_action')) {
				$return[] = array(
					'name'	   => $this->language->get('text_return_action'),
					'href'     => $this->url->link('localisation/return_action', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);		
			}
			
			if ($this->user->hasPermission('access', 'localisation/return_reason')) {
				$return[] = array(
					'name'	   => $this->language->get('text_return_reason'),
					'href'     => $this->url->link('localisation/return_reason', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($return) {	
				$localisation[] = array(
					'name'	   => $this->language->get('text_return'),
					'href'     => '',
					'children' => $return		
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/country')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_country'),
					'href'     => $this->url->link('localisation/country', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/zone')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_zone'),
					'href'     => $this->url->link('localisation/zone', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/geo_zone')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_geo_zone'),
					'href'     => $this->url->link('localisation/geo_zone', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			// Tax		
			$tax = array();
			
			if ($this->user->hasPermission('access', 'localisation/tax_class')) {
				$tax[] = array(
					'name'	   => $this->language->get('text_tax_class'),
					'href'     => $this->url->link('localisation/tax_class', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/tax_rate')) {
				$tax[] = array(
					'name'	   => $this->language->get('text_tax_rate'),
					'href'     => $this->url->link('localisation/tax_rate', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($tax) {	
				$localisation[] = array(
					'name'	   => $this->language->get('text_tax'),
					'href'     => '',
					'children' => $tax		
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/length_class')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_length_class'),
					'href'     => $this->url->link('localisation/length_class', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($this->user->hasPermission('access', 'localisation/weight_class')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_weight_class'),
					'href'     => $this->url->link('localisation/weight_class', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($localisation) {																
				$system[] = array(
					'name'	   => $this->language->get('text_localisation'),
					'href'     => '',
					'children' => $localisation	
				);
			}
			
			// Tools	
			$tool = array();
			
			if ($this->user->hasPermission('access', 'tool/upload')) {
				$tool[] = array(
					'name'	   => $this->language->get('text_upload'),
					'href'     => $this->url->link('tool/upload', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			

			/* mps */
			if ($this->user->hasPermission('access', 'importer/product')) {
				$tool[] = array(
					'name'	   => $this->language->get('text_product_importer'),
					'href'     => $this->url->link('importer/product', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			
			if ($this->user->hasPermission('access', 'exporter/product')) {
				$tool[] = array(
					'name'	   => $this->language->get('text_product_exporter'),
					'href'     => $this->url->link('exporter/product', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);	
			}
			/* mpe */
			
			if ($this->user->hasPermission('access', 'tool/backup')) {
				$tool[] = array(
					'name'	   => $this->language->get('text_backup'),
					'href'     => $this->url->link('tool/backup', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'tool/log')) {
				$tool[] = array(
					'name'	   => $this->language->get('text_log'),
					'href'     => $this->url->link('tool/log', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($tool) {
				$system[] = array(
					'name'	   => $this->language->get('text_tools'),
					'href'     => '',
					'children' => $tool	
				);
			}
			
			
            /* AbandonedCarts - Begin */
            $this->load->model('setting/setting');
            $abandonedCartsSettings = $this->model_setting_setting->getSetting('abandonedcarts', $this->config->get('store_id'));
            
            if (isset($abandonedCartsSettings['abandonedcarts']['Enabled']) && $abandonedCartsSettings['abandonedcarts']['Enabled']=='yes' && isset($abandonedCartsSettings['abandonedcarts']['MenuWidget']) && $abandonedCartsSettings['abandonedcarts']['MenuWidget']=='yes') { 
                $AB_count = $this->db->query("SELECT count(*) as total FROM `" . DB_PREFIX . "abandonedcarts` WHERE `notified`=0");
                
                $data['menus'][] = array(
                    'id'       => 'menu-abandonedcarts',
                    'icon'	   => 'fa fa-shopping-cart fa-fw', 
                    'name'	   => 'Abandoned Carts <span class="label label-danger">'. $AB_count->row['total'] . '</span>',
                    'href'     => $this->url->link('extension/module/abandonedcarts', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );	
            }
            /* AbandonedCarts - End */
			
			if ($system) {
				$data['menus'][] = array(
					'id'       => 'menu-system',
					'icon'	   => 'fa-cog', 
					'name'	   => $this->language->get('text_system'),
					'href'     => '',
					'children' => $system
				);
			}
			
			// Analytics
			if ($this->user->hasPermission('access', 'analytics/product_performance')) {
				$analytics[] = array(
					'name'	   => $this->language->get('text_product_performance'),
					'href'     => $this->url->link('analytics/product_performance', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'extension/module/ianalytics')) {
				$analytics[] = array(
					'name'	   => $this->language->get('text_ianalytics'),
					'href'     => $this->url->link('extension/module/ianalytics', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($this->user->hasPermission('access', 'analytics/clv')) {
				$analytics[] = array(
					'name'	   => $this->language->get('text_clv'),
					'href'     => $this->url->link('analytics/clv', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}
			
			if ($analytics) {
				$data['menus'][] = array(
					'id'       => 'menu-analytics',
					'icon'	   => 'fa-pie-chart', 
					'name'	   => $this->language->get('text_analytics'),
					'href'     => '',
					'children' => $analytics
				);
			}
			
			
			// Report
			$report = array();
			
			// Report Sales
			$report_sale = array();	
			
			if ($this->user->hasPermission('access', 'report/sale_order')) {
				$report_sale[] = array(
					'name'	   => $this->language->get('text_report_sale_order'),
					'href'     => $this->url->link('report/sale_order', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($this->user->hasPermission('access', 'report/sale_tax')) {
				$report_sale[] = array(
					'name'	   => $this->language->get('text_report_sale_tax'),
					'href'     => $this->url->link('report/sale_tax', 'token=' . $this->session->data['token'], true),
					'children' => array()	
				);
			}
			
			if ($this->user->hasPermission('access', 'report/sale_shipping')) {
				$report_sale[] = array(
					'name'	   => $this->language->get('text_report_sale_shipping'),
					'href'     => $this->url->link('report/sale_shipping', 'token=' . $this->session->data['token'], true),
					'children' => array()	
				);
			}
			
			if ($this->user->hasPermission('access', 'report/sale_return')) {	
				$report_sale[] = array(
					'name'	   => $this->language->get('text_report_sale_return'),
					'href'     => $this->url->link('report/sale_return', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);	
			}
			
			if ($this->user->hasPermission('access', 'report/sale_coupon')) {		
				$report_sale[] = array(
					'name'	   => $this->language->get('text_report_sale_coupon'),
					'href'     => $this->url->link('report/sale_coupon', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($report_sale) {
				$report[] = array(
					'name'	   => $this->language->get('text_report_sale'),
					'href'     => '',
					'children' => $report_sale
				);			
			}
			
			// Report Products			
			$report_product = array();	

			if ($this->user->hasPermission('access', 'report/adv_products')) {
				$report_product[] = array(
					'name'	   => $this->language->get('text_report_adv_products'),
					'href'     => $this->url->link('report/adv_products', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}				
            
			
			if ($this->user->hasPermission('access', 'report/product_viewed')) {
				$report_product[] = array(
					'name'	   => $this->language->get('text_report_product_viewed'),
					'href'     => $this->url->link('report/product_viewed', 'token=' . $this->session->data['token'], true),
					'children' => array()	
				);
			}
			

					if ($this->user->hasPermission('access', 'report/product_purchased_advanced')) {
						$report_product[] = array(
							'name'	   => $this->language->get('text_report_product_purchased_advanced'),
							'href'     => $this->url->link('report/product_purchased_advanced', 'token=' . $this->session->data['token'], true),
							'children' => array()	
						);
					}

				
			if ($this->user->hasPermission('access', 'report/product_purchased')) {
				$report_product[] = array(
					'name'	   => $this->language->get('text_report_product_purchased'),
					'href'     => $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'], true),
					'children' => array()	
				);
			}
			
			
			if ($report_product) {	
				$report[] = array(
					'name'	   => $this->language->get('text_report_product'),
					'href'     => '',
					'children' => $report_product	
				);		
			}
			
			// Report Customers				
			$report_customer = array();
			
			if ($this->user->hasPermission('access', 'report/customer_online')) {	
				$report_customer[] = array(
					'name'	   => $this->language->get('text_report_customer_online'),
					'href'     => $this->url->link('report/customer_online', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($this->user->hasPermission('access', 'report/customer_activity')) {
				$report_customer[] = array(
					'name'	   => $this->language->get('text_report_customer_activity'),
					'href'     => $this->url->link('report/customer_activity', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'report/customer_search')) {
				$report_customer[] = array(
					'name'	   => $this->language->get('text_report_customer_search'),
					'href'     => $this->url->link('report/customer_search', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'report/customer_order')) {	
				$report_customer[] = array(
					'name'	   => $this->language->get('text_report_customer_order'),
					'href'     => $this->url->link('report/customer_order', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($this->user->hasPermission('access', 'report/customer_reward')) {
				$report_customer[] = array(
					'name'	   => $this->language->get('text_report_customer_reward'),
					'href'     => $this->url->link('report/customer_reward', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($this->user->hasPermission('access', 'report/customer_credit')) {
				$report_customer[] = array(
					'name'	   => $this->language->get('text_report_customer_credit'),
					'href'     => $this->url->link('report/customer_credit', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($report_customer) {	
				$report[] = array(
					'name'	   => $this->language->get('text_report_customer'),
					'href'     => '',
					'children' => $report_customer	
				);
			}
			
			// Report Marketing			
			$report_marketing = array();			
			
			if ($this->user->hasPermission('access', 'report/marketing')) {
				$report_marketing[] = array(
					'name'	   => $this->language->get('text_report_marketing'),
					'href'     => $this->url->link('report/marketing', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			
			if ($this->user->hasPermission('access', 'report/affiliate')) {
				$report_marketing[] = array(
					'name'	   => $this->language->get('text_report_affiliate'),
					'href'     => $this->url->link('report/affiliate', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);		
			}
			
			if ($this->user->hasPermission('access', 'report/affiliate_activity')) {
				$report_marketing[] = array(
					'name'	   => $this->language->get('text_report_affiliate_activity'),
					'href'     => $this->url->link('report/affiliate_activity', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);		
			}
			
			if ($report_marketing) {	
				$report[] = array(
					'name'	   => $this->language->get('text_report_marketing'),
					'href'     => '',
					'children' => $report_marketing	
				);		
			}
			
			
            /* iAnalytics - Begin */
			/*$data['menus'][] = array(
				'id'       => 'menu-ianalytics',
				'icon'	   => 'fa fa-pie-chart fa-fw', 
				'name'	   => 'iAnalytics',
				'href'     => $this->url->link('extension/module/ianalytics', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);*/
            /* iAnalytics - End */
			
			if ($report) {	
				$data['menus'][] = array(
					'id'       => 'menu-report',
					'icon'	   => 'fa-bar-chart-o', 
					'name'	   => $this->language->get('text_reports'),
					'href'     => '',
					'children' => $report
				);	
			}		
			
			// Stats
			$data['text_complete_status'] = $this->language->get('text_complete_status');
			$data['text_processing_status'] = $this->language->get('text_processing_status');
			$data['text_other_status'] = $this->language->get('text_other_status');
	
			$this->load->model('sale/order');
	
			$order_total = $this->model_sale_order->getTotalOrders();
	
			$complete_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_complete_status'))));
			
			if ($complete_total) {
				$data['complete_status'] = round(($complete_total / $order_total) * 100);
			} else {
				$data['complete_status'] = 0;
			}
	
			$processing_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_processing_status'))));
	
			if ($processing_total) {
				$data['processing_status'] = round(($processing_total / $order_total) * 100);
			} else {
				$data['processing_status'] = 0;
			}
	
			$this->load->model('localisation/order_status');
	
			$order_status_data = array();
	
			$results = $this->model_localisation_order_status->getOrderStatuses();
	
			foreach ($results as $result) {
				if (!in_array($result['order_status_id'], array_merge($this->config->get('config_complete_status'), $this->config->get('config_processing_status')))) {
					$order_status_data[] = $result['order_status_id'];
				}
			}
	
			$other_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $order_status_data)));
	
			if ($other_total) {
				$data['other_status'] = round(($other_total / $order_total) * 100);
			} else {
				$data['other_status'] = 0;
			}
			
			return $this->load->view('common/column_left', $data);
		}
	}
}