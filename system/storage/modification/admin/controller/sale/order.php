<?php
class ControllerSaleOrder extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		$this->getList();
	}

	public function add() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		$this->getForm();
	}

	public function edit() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		$this->getForm();
	}
	
	public function delete() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		if (isset($this->request->post['selected']) && $this->validate()) {
			foreach ($this->request->post['selected'] as $order_id) {
				$this->model_sale_order->deleteOrder($order_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_order_id'])) {
				$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
			}
	
			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}
	
	        if (isset($this->request->get['filter_phn_no'])) {
				$url .= '&filter_phn_no=' . $this->request->get['filter_phn_no'];
			}
			
			if (isset($this->request->get['filter_order_status'])) {
				$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
			}
	
			if (isset($this->request->get['filter_total'])) {
				$url .= '&filter_total=' . $this->request->get['filter_total'];
			}
	
			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}
	
			if (isset($this->request->get['filter_date_modified'])) {
				$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
			}

			$this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	
	protected function getList() {
		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = null;
		}
		
		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}
		
        
       if (isset($this->request->get['filter_telephone'])) {
			$filter_telephone = $this->request->get['filter_telephone'];
		} else {
			$filter_telephone = null;
		}

		
		if (isset($this->request->get['filter_order_status'])) {
			$filter_order_status = $this->request->get['filter_order_status'];
		} else {
			$filter_order_status = null;
		}

		if (isset($this->request->get['filter_total'])) {
			$filter_total = $this->request->get['filter_total'];
		} else {
			$filter_total = null;
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$filter_date_modified = $this->request->get['filter_date_modified'];
		} else {
			$filter_date_modified = null;
		}
        
        if (isset($this->request->get['filter_telephone'])) {
			$url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'o.order_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

     	if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

     	if (isset($this->request->get['filter_telephone'])) {
			$filter_telephone = $this->request->get['filter_telephone'];
		} else {
			$filter_telephone = null;
		}

		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

        if (isset($this->request->get['filter_telephone'])) {
			$url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['invoice'] = $this->url->link('sale/order/invoice', 'token=' . $this->session->data['token'], true);
		$data['delhivery_label'] = $this->url->link('sale/order/delhivery_label', 'token=' . $this->session->data['token'], true);
		
		$data['barcode_invoice'] = $this->url->link('sale/order/barcode_invoice', 'token=' . $this->session->data['token'], true);
		$data['picklist'] = $this->url->link('sale/order/picklist', 'token=' . $this->session->data['token'], true);
		$data['shipping'] = $this->url->link('sale/order/shipping', 'token=' . $this->session->data['token'], true);
		$data['add'] = $this->url->link('sale/order/add', 'token=' . $this->session->data['token'], true);
		$data['delete'] = $this->url->link('sale/order/delete', 'token=' . $this->session->data['token'], true);
		$data['tracking'] = $this->url->link('sale/order/ExportTracking', 'token=' . $this->session->data['token'], true);
		$data['mms_label'] = $this->url->link('sale/order/mms_label', 'token=' . $this->session->data['token'], true);
		$data['mmm_label'] = $this->url->link('sale/order/mmm_label', 'token=' . $this->session->data['token'], true);

		$data['orders'] = array();

		$filter_data = array(
			'filter_order_id'      => $filter_order_id,
			'filter_customer'	   => $filter_customer,
			'filter_email'	       => $filter_email,
			'filter_telephone'     => $filter_telephone,
			'filter_order_status'  => $filter_order_status,
			'filter_total'         => $filter_total,
			'filter_date_added'    => $filter_date_added,
			'filter_date_modified' => $filter_date_modified,
			'sort'                 => $sort,
			'order'                => $order,
			'start'                => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                => $this->config->get('config_limit_admin')
		);

		$order_total = $this->model_sale_order->getTotalOrders($filter_data);

		$results = $this->model_sale_order->getOrders($filter_data);

			if($this->config->get('shipway_login')){
				$this->load->model('shipway/courier_tracking');
				$data['couriers'] = $this->model_shipway_courier_tracking->getCourierList(true);
				$data['all_couriers'] = $this->model_shipway_courier_tracking->getCourierList();
				$data['hasShipWayAccount'] = true;
				$results = $this->model_shipway_courier_tracking->getOrderShipwayFields($results);
			}
			

		foreach ($results as $result) {
		    
		      $order_customer_id = 0;
              $order_customer_id = $this->model_sale_order->getOrderCustomerId($result['order_id']);

              $total_customer_orders = 0;
              if ($order_customer_id > 0) {
                $total_customer_orders = $this->model_sale_order->getTotalShippedCompleteOrdersByCustomerId($order_customer_id);
              } 
               
			$data['orders'][] = array(
				'order_id'      => $result['order_id'],
				'is_gift'       => $result['is_gift'],
				'telephone'     => $result['telephone'],
				'customer'      => $result['customer'],
			    'customer_id'                => $order_customer_id,
                'total_customer_orders'      => $total_customer_orders,
            	'dawb'          => $result['awb'],

				'courier_id'      => ($this->config->get('shipway_login'))?$result['courier_id']:'',
				'awbno'  		  => ($this->config->get('shipway_login'))?$result['awbno']:'',			
			
				'order_status'  => $result['order_status'] ? $result['order_status'] : $this->language->get('text_missing'),
				'order_status_id' => $result['order_status_id'],
				'total'         => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
				'date_added'    => date($this->language->get('datetime_format'), strtotime($result['date_added'])),
				'date_modified' => date($this->language->get('datetime_format'), strtotime($result['date_modified'])),
				'date_shipped' => date($this->language->get('datetime_format'), strtotime($result['date_shipped'])),
				'shipping_code' => $result['shipping_code'],
				'view'          => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
				'edit'          => $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_missing'] = $this->language->get('text_missing');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_date_modified'] = $this->language->get('column_date_modified');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_order_id'] = $this->language->get('entry_order_id');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_date_added'] = $this->language->get('entry_date_added');
		$data['entry_date_modified'] = $this->language->get('entry_date_modified');
		$data['entry_telephone'] = $this->language->get('entry_telephone');

		$data['button_invoice_print'] = $this->language->get('button_invoice_print');
		$data['button_picklist_print'] = $this->language->get('button_picklist_print');
		$data['button_shipping_print'] = $this->language->get('button_shipping_print');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_view'] = $this->language->get('button_view');
		$data['button_ip_add'] = $this->language->get('button_ip_add');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

        if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

        if (isset($this->request->get['filter_telephone'])) {
			$url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
		}
		

		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_order'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.order_id' . $url, true);
		$data['sort_customer'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, true);
		$data['sort_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=order_status' . $url, true);
		$data['sort_total'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.total' . $url, true);
		$data['sort_date_added'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, true);
		$data['sort_telephone'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.telephone' . $url, true);
		$data['sort_date_modified'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.date_modified' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

	    if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

	    if (isset($this->request->get['filter_telephone'])) {
			$filter_telephone = $this->request->get['filter_telephone'];
		} else {
			$filter_telephone = null;
		}
		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}
		if (isset($this->request->get['filter_telephone'])) {
			$url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

		$data['filter_order_id'] = $filter_order_id;
		$data['filter_customer'] = $filter_customer;
	    $data['filter_email']    = $filter_email;
	    $data['filter_telephone'] = $filter_telephone;
		$data['filter_order_status'] = $filter_order_status;
		$data['filter_total'] = $filter_total;
		$data['filter_date_added'] = $filter_date_added;
		$data['filter_date_modified'] = $filter_date_modified;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sale/order_list', $data));
	}

	public function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
		$data['text_product'] = $this->language->get('text_product');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_order_detail'] = $this->language->get('text_order_detail');

		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_affiliate'] = $this->language->get('entry_affiliate');
		$data['entry_address'] = $this->language->get('entry_address');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_zone_code'] = $this->language->get('entry_zone_code');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_option'] = $this->language->get('entry_option');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_to_name'] = $this->language->get('entry_to_name');
		$data['entry_to_email'] = $this->language->get('entry_to_email');
		$data['entry_from_name'] = $this->language->get('entry_from_name');
		$data['entry_from_email'] = $this->language->get('entry_from_email');
		$data['entry_theme'] = $this->language->get('entry_theme');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_amount'] = $this->language->get('entry_amount');
		$data['entry_currency'] = $this->language->get('entry_currency');
		$data['entry_shipping_method'] = $this->language->get('entry_shipping_method');
		$data['entry_payment_method'] = $this->language->get('entry_payment_method');
		$data['entry_coupon'] = $this->language->get('entry_coupon');
		$data['entry_voucher'] = $this->language->get('entry_voucher');
		$data['entry_reward'] = $this->language->get('entry_reward');
		$data['entry_order_status'] = $this->language->get('entry_order_status');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_refresh'] = $this->language->get('button_refresh');
		$data['button_product_add'] = $this->language->get('button_product_add');
		$data['button_voucher_add'] = $this->language->get('button_voucher_add');
		$data['button_apply'] = $this->language->get('button_apply');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_ip_add'] = $this->language->get('button_ip_add');

		$data['tab_order'] = $this->language->get('tab_order');
		$data['tab_customer'] = $this->language->get('tab_customer');
		$data['tab_payment'] = $this->language->get('tab_payment');
		$data['tab_shipping'] = $this->language->get('tab_shipping');
		$data['tab_product'] = $this->language->get('tab_product');
		$data['tab_voucher'] = $this->language->get('tab_voucher');
		$data['tab_total'] = $this->language->get('tab_total');

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_telephone'])) {
			$filter_telephone = $this->request->get['filter_telephone'];
		} else {
			$filter_telephone = null;
		}

		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}
		
		if (isset($this->request->get['filter_telephone'])) {
			$url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true);

		$data['token'] = $this->session->data['token'];
		$data['user'] = $this->user->getUserName();

		if (isset($this->request->get['order_id'])) {
			$order_info = $this->model_sale_order->getOrder($this->request->get['order_id']);
		}

		if (!empty($order_info)) {
			$data['order_id'] = $this->request->get['order_id'];
			$data['store_id'] = $order_info['store_id'];
			$data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

			$data['customer'] = $order_info['customer'];
			$data['customer_id'] = $order_info['customer_id'];
			$data['customer_group_id'] = $order_info['customer_group_id'];
			$data['firstname'] = $order_info['firstname'];
			$data['lastname'] = $order_info['lastname'];
			$data['email'] = $order_info['email'];
			$data['telephone'] = $order_info['telephone'];
			$data['fax'] = $order_info['fax'];
			$data['account_custom_field'] = $order_info['custom_field'];

			$this->load->model('customer/customer');

			$data['addresses'] = $this->model_customer_customer->getAddresses($order_info['customer_id']);

			$data['payment_firstname'] = $order_info['payment_firstname'];
			$data['payment_lastname'] = $order_info['payment_lastname'];
			$data['payment_company'] = $order_info['payment_company'];
			$data['payment_address_1'] = $order_info['payment_address_1'];
			$data['payment_address_2'] = $order_info['payment_address_2'];
			$data['payment_city'] = $order_info['payment_city'];
			$data['payment_postcode'] = $order_info['payment_postcode'];
			$data['payment_country_id'] = $order_info['payment_country_id'];
			$data['payment_zone_id'] = $order_info['payment_zone_id'];
			$data['payment_custom_field'] = $order_info['payment_custom_field'];
			$data['payment_method'] = $order_info['payment_method'];
			$data['payment_code'] = $order_info['payment_code'];

			$data['shipping_firstname'] = $order_info['shipping_firstname'];
			$data['shipping_lastname'] = $order_info['shipping_lastname'];
			$data['shipping_company'] = $order_info['shipping_company'];
			$data['shipping_address_1'] = $order_info['shipping_address_1'];
			$data['shipping_address_2'] = $order_info['shipping_address_2'];
			$data['shipping_city'] = $order_info['shipping_city'];
			$data['shipping_postcode'] = $order_info['shipping_postcode'];
			$data['shipping_country_id'] = $order_info['shipping_country_id'];
			$data['shipping_zone_id'] = $order_info['shipping_zone_id'];
			$data['shipping_custom_field'] = $order_info['shipping_custom_field'];
			$data['shipping_method'] = $order_info['shipping_method'];
			$data['shipping_code'] = $order_info['shipping_code'];
			
			$data['old_total'] = round($order_info['total'],2);

			// Products
			$data['order_products'] = array();

			$products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);

			foreach ($products as $product) {
				$data['order_products'][] = array(
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']),
					'quantity'   => $product['quantity'],
					'price'      => $product['price'],
					'total'      => $product['total'],
					'reward'     => $product['reward']
				);
			}

			// Vouchers
			$data['order_vouchers'] = $this->model_sale_order->getOrderVouchers($this->request->get['order_id']);

			$data['coupon'] = '';
			$data['voucher'] = '';
			$data['reward'] = '';

			$data['order_totals'] = array();

			$order_totals = $this->model_sale_order->getOrderTotals($this->request->get['order_id']);

			foreach ($order_totals as $order_total) {
				// If coupon, voucher or reward points
				$start = strpos($order_total['title'], '(') + 1;
				$end = strrpos($order_total['title'], ')');

				if ($start && $end) {
					$data[$order_total['code']] = substr($order_total['title'], $start, $end - $start);
				}
			}

			$data['order_status_id'] = $order_info['order_status_id'];
			$data['comment'] = $order_info['comment'];
			$data['affiliate_id'] = $order_info['affiliate_id'];
			$data['affiliate'] = $order_info['affiliate_firstname'] . ' ' . $order_info['affiliate_lastname'];
			$data['currency_code'] = $order_info['currency_code'];
		} else {
			$data['order_id'] = 0;
			$data['store_id'] = 0;
			$data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;
			
			$data['customer'] = '';
			$data['customer_id'] = '';
			$data['customer_group_id'] = $this->config->get('config_customer_group_id');
			$data['firstname'] = '';
			$data['lastname'] = '';
			$data['email'] = '';
			$data['telephone'] = '';
			$data['fax'] = '';
			$data['customer_custom_field'] = array();

			$data['addresses'] = array();

			$data['payment_firstname'] = '';
			$data['payment_lastname'] = '';
			$data['payment_company'] = '';
			$data['payment_address_1'] = '';
			$data['payment_address_2'] = '';
			$data['payment_city'] = '';
			$data['payment_postcode'] = '';
			$data['payment_country_id'] = '';
			$data['payment_zone_id'] = '';
			$data['payment_custom_field'] = array();
			$data['payment_method'] = '';
			$data['payment_code'] = '';

			$data['shipping_firstname'] = '';
			$data['shipping_lastname'] = '';
			$data['shipping_company'] = '';
			$data['shipping_address_1'] = '';
			$data['shipping_address_2'] = '';
			$data['shipping_city'] = '';
			$data['shipping_postcode'] = '';
			$data['shipping_country_id'] = '';
			$data['shipping_zone_id'] = '';
			$data['shipping_custom_field'] = array();
			$data['shipping_method'] = '';
			$data['shipping_code'] = '';

			$data['order_products'] = array();
			$data['order_vouchers'] = array();
			$data['order_totals'] = array();

			$data['order_status_id'] = $this->config->get('config_order_status_id');
			$data['comment'] = '';
			$data['affiliate_id'] = '';
			$data['affiliate'] = '';
			$data['currency_code'] = $this->config->get('config_currency');

			$data['coupon'] = '';
			$data['voucher'] = '';
			$data['reward'] = '';
		}

		// Stores
		$this->load->model('setting/store');

		$data['stores'] = array();

		$data['stores'][] = array(
			'store_id' => 0,
			'name'     => $this->language->get('text_default')
		);

		$results = $this->model_setting_store->getStores();

		foreach ($results as $result) {
			$data['stores'][] = array(
				'store_id' => $result['store_id'],
				'name'     => $result['name']
			);
		}

		// Customer Groups
		$this->load->model('customer/customer_group');

		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		// Custom Fields
		$this->load->model('customer/custom_field');

		$data['custom_fields'] = array();

		$filter_data = array(
			'sort'  => 'cf.sort_order',
			'order' => 'ASC'
		);

		$custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

		foreach ($custom_fields as $custom_field) {
			$data['custom_fields'][] = array(
				'custom_field_id'    => $custom_field['custom_field_id'],
				'custom_field_value' => $this->model_customer_custom_field->getCustomFieldValues($custom_field['custom_field_id']),
				'name'               => $custom_field['name'],
				'value'              => $custom_field['value'],
				'type'               => $custom_field['type'],
				'location'           => $custom_field['location'],
				'sort_order'         => $custom_field['sort_order']
			);
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();

		$this->load->model('localisation/currency');

		$data['currencies'] = $this->model_localisation_currency->getCurrencies();

		$data['voucher_min'] = $this->config->get('config_voucher_min');

		$this->load->model('sale/voucher_theme');

		$data['voucher_themes'] = $this->model_sale_voucher_theme->getVoucherThemes();

		// API login
		$data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;
		
		$this->load->model('user/api');

		$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

		if ($api_info) {
			
			$data['api_id'] = $api_info['api_id'];
			$data['api_key'] = $api_info['key'];
			$data['api_ip'] = $this->request->server['REMOTE_ADDR'];
		} else {
			$data['api_id'] = '';
			$data['api_key'] = '';
			$data['api_ip'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sale/order_form', $data));
	}

	public function info() {
		$this->load->model('sale/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
        
        
		$order_info = $this->model_sale_order->getOrder($order_id);
         
        
		if ($order_info) {
			$this->load->language('sale/order');

			$this->document->setTitle($this->language->get('heading_title'));

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_customer_detail'] = $this->language->get('text_customer_detail');
			$data['text_option'] = $this->language->get('text_option');
			$data['text_store'] = $this->language->get('text_store');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			$data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$data['text_customer'] = $this->language->get('text_customer');
			$data['text_customer_group'] = $this->language->get('text_customer_group');
			$data['text_email'] = $this->language->get('text_email');
			$data['text_telephone'] = $this->language->get('text_telephone');
			$data['text_invoice'] = $this->language->get('text_invoice');
			$data['text_reward'] = $this->language->get('text_reward');
			$data['text_affiliate'] = $this->language->get('text_affiliate');
			$data['text_order'] = sprintf($this->language->get('text_order'), $this->request->get['order_id']);
			
			$data['is_gift'] = $order_info['is_gift'];
			
			$data['text_payment_address'] = $this->language->get('text_payment_address');
			$data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$data['text_comment'] = $this->language->get('text_comment');
			$data['text_account_custom_field'] = $this->language->get('text_account_custom_field');
			$data['text_payment_custom_field'] = $this->language->get('text_payment_custom_field');
			$data['text_shipping_custom_field'] = $this->language->get('text_shipping_custom_field');
			$data['text_browser'] = $this->language->get('text_browser');
			$data['text_ip'] = $this->language->get('text_ip');
			$data['text_forwarded_ip'] = $this->language->get('text_forwarded_ip');
			$data['text_user_agent'] = $this->language->get('text_user_agent');
			$data['text_accept_language'] = $this->language->get('text_accept_language');
			$data['text_history'] = $this->language->get('text_history');
			$data['text_history_add'] = $this->language->get('text_history_add');
			$data['text_loading'] = $this->language->get('text_loading');

			$data['column_product'] = $this->language->get('column_product');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');

			$data['entry_order_status'] = $this->language->get('entry_order_status');
			$data['entry_notify'] = $this->language->get('entry_notify');
			$data['entry_override'] = $this->language->get('entry_override');
			$data['entry_comment'] = $this->language->get('entry_comment');

			$data['help_override'] = $this->language->get('help_override');

			$data['button_invoice_print'] = $this->language->get('button_invoice_print');
			$data['button_picklist_print'] = $this->language->get('button_picklist_print');
			$data['button_shipping_print'] = $this->language->get('button_shipping_print');
			$data['button_edit'] = $this->language->get('button_edit');
			$data['button_cancel'] = $this->language->get('button_cancel');
			$data['button_generate'] = $this->language->get('button_generate');
			$data['button_reward_add'] = $this->language->get('button_reward_add');
			$data['button_reward_remove'] = $this->language->get('button_reward_remove');
			$data['button_commission_add'] = $this->language->get('button_commission_add');
			$data['button_commission_remove'] = $this->language->get('button_commission_remove');
			$data['button_history_add'] = $this->language->get('button_history_add');
			$data['button_ip_add'] = $this->language->get('button_ip_add');

			$data['tab_history'] = $this->language->get('tab_history');
			$data['tab_additional'] = $this->language->get('tab_additional');

			$url = '';

			if (isset($this->request->get['filter_order_id'])) {
				$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
			}

			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}

    		if (isset($this->request->get['filter_telephone'])) {
    			$filter_telephone = $this->request->get['filter_telephone'];
    		} else {
    			$filter_telephone = null;
    		}
    		
			if (isset($this->request->get['filter_order_status'])) {
				$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
			}

			if (isset($this->request->get['filter_total'])) {
				$url .= '&filter_total=' . $this->request->get['filter_total'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['filter_date_modified'])) {
				$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
			}
			
			if (isset($this->request->get['filter_telephone'])) {
			    $url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
		    }

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true)
			);

			$data['shipping'] = $this->url->link('sale/order/shipping', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
			$data['invoice'] = $this->url->link('sale/order/invoice', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
			$data['picklist'] = $this->url->link('sale/order/picklist', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
			
			$data['delhivery_label'] = $this->url->link('sale/order/delhivery_label', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
			
			$data['barcode_invoice'] = $this->url->link('sale/order/barcode_invoice', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
			
			$data['edit'] = $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
			$data['cancel'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true);

			$data['token'] = $this->session->data['token'];

			$data['order_id'] = $this->request->get['order_id'];
			$data['is_gift'] = $this->request->get['is_gift'];

			$data['store_id'] = $order_info['store_id'];
			$data['store_name'] = $order_info['store_name'];
			
			$data['user'] = $this->user->getUserName();
			
			if ($order_info['store_id'] == 0) {
				$data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;
			} else {
				$data['store_url'] = $order_info['store_url'];
			}

			if ($order_info['invoice_no']) {
				$data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$data['invoice_no'] = '';
			}

			$data['date_added'] = date($this->language->get('datetime_format'), strtotime($order_info['date_added']));

			$data['firstname'] = $order_info['firstname'];
			$data['lastname'] = $order_info['lastname'];

			if ($order_info['customer_id']) {
				$data['customer'] = $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $order_info['customer_id'], true);
			} else {
				$data['customer'] = '';
			}

			$this->load->model('customer/customer_group');

			$customer_group_info = $this->model_customer_customer_group->getCustomerGroup($order_info['customer_group_id']);

			if ($customer_group_info) {
				$data['customer_group'] = $customer_group_info['name'];
			} else {
				$data['customer_group'] = '';
			}

			$data['email'] = $order_info['email'];
			$data['telephone'] = $order_info['telephone'];

			$data['shipping_method'] = $order_info['shipping_method'];
			$data['payment_method'] = $order_info['payment_method'];

			// Payment Address
			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			// Shipping Address
			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			// Uploaded files
			$this->load->model('tool/upload');


        //xtensions- Custom Fields
  			if($this->xtensions_checkout->isActive('xtensions_best_checkout',$order_info['store_id'])){
  				$formatted_address = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getFormattedAddressForOrder($order_info);
  				$data['payment_address'] = $formatted_address['payment_address_formatted'];
  				$data['shipping_address'] = $formatted_address['shipping_address_formatted'];
  			}
  			//xtensions- Custom Fields
			
			$data['products'] = array();

			$products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);
            
            $product_option_weight = 0;
			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
				    
				     $option_weight = substr($option['value'], 0, -1);
				    
				    $product_option_weight += $option_weight * $product['quantity'];
				    
					if ($option['type'] != 'file') {
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $option['value'],
							'type'  => $option['type']
						);
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$option_data[] = array(
								'name'  => $option['name'],
								'value' => $upload_info['name'],
								'type'  => $option['type'],
								'href'  => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], true)
							);
						}
					}
				}

				$data['products'][] = array(
					'order_product_id' => $product['order_product_id'],
					'product_id'       => $product['product_id'],
					'name'    	 	   => $product['name'],
					'model'    		   => $product['model'],
					'option'   		   => $option_data,
					'quantity'		   => $product['quantity'],
					'price'    		   => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'sub-total'        => $this->currency->format($product['quantity']*$product['price'], $order_info['currency_code'], $order_info['currency_value']),
					'total'    		   => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'href'     		   => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], true)
				);
			}
 
			$data['vouchers'] = array();

			$vouchers = $this->model_sale_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
					'href'        => $this->url->link('sale/voucher/edit', 'token=' . $this->session->data['token'] . '&voucher_id=' . $voucher['voucher_id'], true)
				);
			}

			$data['totals'] = array();

			$totals = $this->model_sale_order->getOrderTotals($this->request->get['order_id']);

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			$data['comment'] = nl2br($order_info['comment']);

			$this->load->model('customer/customer');

			$data['reward'] = $order_info['reward'];

			$data['reward_total'] = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($this->request->get['order_id']);

			$data['affiliate_firstname'] = $order_info['affiliate_firstname'];
			$data['affiliate_lastname'] = $order_info['affiliate_lastname'];

			if ($order_info['affiliate_id']) {
				$data['affiliate'] = $this->url->link('marketing/affiliate/edit', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $order_info['affiliate_id'], true);
			} else {
				$data['affiliate'] = '';
			}

			$data['commission'] = $this->currency->format($order_info['commission'], $order_info['currency_code'], $order_info['currency_value']);

			$this->load->model('marketing/affiliate');

			$data['commission_total'] = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($this->request->get['order_id']);

			$this->load->model('localisation/order_status');

			$order_status_info = $this->model_localisation_order_status->getOrderStatus($order_info['order_status_id']);

			if ($order_status_info) {
				$data['order_status'] = $order_status_info['name'];
			} else {
				$data['order_status'] = '';
			}

			$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

			$data['order_status_id'] = $order_info['order_status_id'];

			$data['account_custom_field'] = $order_info['custom_field'];

			// Custom Fields
			$this->load->model('customer/custom_field');

			$data['account_custom_fields'] = array();

			$filter_data = array(
				'sort'  => 'cf.sort_order',
				'order' => 'ASC'
			);

			$custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

			foreach ($custom_fields as $custom_field) {
				if ($custom_field['location'] == 'account' && isset($order_info['custom_field'][$custom_field['custom_field_id']])) {
					if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
						$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['custom_field'][$custom_field['custom_field_id']]);

						if ($custom_field_value_info) {
							$data['account_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $custom_field_value_info['name']
							);
						}
					}

					if ($custom_field['type'] == 'checkbox' && is_array($order_info['custom_field'][$custom_field['custom_field_id']])) {
						foreach ($order_info['custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
							$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

							if ($custom_field_value_info) {
								$data['account_custom_fields'][] = array(
									'name'  => $custom_field['name'],
									'value' => $custom_field_value_info['name']
								);
							}
						}
					}

					if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
						$data['account_custom_fields'][] = array(
							'name'  => $custom_field['name'],
							'value' => $order_info['custom_field'][$custom_field['custom_field_id']]
						);
					}

					if ($custom_field['type'] == 'file') {
						$upload_info = $this->model_tool_upload->getUploadByCode($order_info['custom_field'][$custom_field['custom_field_id']]);

						if ($upload_info) {
							$data['account_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $upload_info['name']
							);
						}
					}
				}
			}

			// Custom fields
			$data['payment_custom_fields'] = array();

			foreach ($custom_fields as $custom_field) {
				if ($custom_field['location'] == 'address' && isset($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
					if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
						$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

						if ($custom_field_value_info) {
							$data['payment_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $custom_field_value_info['name'],
								'sort_order' => $custom_field['sort_order']
							);
						}
					}

					if ($custom_field['type'] == 'checkbox' && is_array($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
						foreach ($order_info['payment_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
							$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

							if ($custom_field_value_info) {
								$data['payment_custom_fields'][] = array(
									'name'  => $custom_field['name'],
									'value' => $custom_field_value_info['name'],
									'sort_order' => $custom_field['sort_order']
								);
							}
						}
					}

					if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
						$data['payment_custom_fields'][] = array(
							'name'  => $custom_field['name'],
							'value' => $order_info['payment_custom_field'][$custom_field['custom_field_id']],
							'sort_order' => $custom_field['sort_order']
						);
					}

					if ($custom_field['type'] == 'file') {
						$upload_info = $this->model_tool_upload->getUploadByCode($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

						if ($upload_info) {
							$data['payment_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $upload_info['name'],
								'sort_order' => $custom_field['sort_order']
							);
						}
					}
				}
			}

			// Shipping
			$data['shipping_custom_fields'] = array();

			foreach ($custom_fields as $custom_field) {
				if ($custom_field['location'] == 'address' && isset($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
					if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
						$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

						if ($custom_field_value_info) {
							$data['shipping_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $custom_field_value_info['name'],
								'sort_order' => $custom_field['sort_order']
							);
						}
					}

					if ($custom_field['type'] == 'checkbox' && is_array($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
						foreach ($order_info['shipping_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
							$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

							if ($custom_field_value_info) {
								$data['shipping_custom_fields'][] = array(
									'name'  => $custom_field['name'],
									'value' => $custom_field_value_info['name'],
									'sort_order' => $custom_field['sort_order']
								);
							}
						}
					}

					if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
						$data['shipping_custom_fields'][] = array(
							'name'  => $custom_field['name'],
							'value' => $order_info['shipping_custom_field'][$custom_field['custom_field_id']],
							'sort_order' => $custom_field['sort_order']
						);
					}

					if ($custom_field['type'] == 'file') {
						$upload_info = $this->model_tool_upload->getUploadByCode($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

						if ($upload_info) {
							$data['shipping_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $upload_info['name'],
								'sort_order' => $custom_field['sort_order']
							);
						}
					}
				}
			}


			if($this->config->get('shipway_login')){
				$this->load->model('shipway/courier_tracking');
				$data['couriers'] = $this->model_shipway_courier_tracking->getCourierList(true);
			}
			
			$data['ip'] = $order_info['ip'];
			$data['forwarded_ip'] = $order_info['forwarded_ip'];
			$data['user_agent'] = $order_info['user_agent'];
			$data['accept_language'] = $order_info['accept_language'];

			// Additional Tabs
			$data['tabs'] = array();

			if ($this->user->hasPermission('access', 'extension/payment/' . $order_info['payment_code'])) {
				if (is_file(DIR_CATALOG . 'controller/extension/payment/' . $order_info['payment_code'] . '.php')) {
					$content = $this->load->controller('extension/payment/' . $order_info['payment_code'] . '/order');
				} else {
					$content = null;
				}

				if ($content) {
					$this->load->language('extension/payment/' . $order_info['payment_code']);

					$data['tabs'][] = array(
						'code'    => $order_info['payment_code'],
						'title'   => $this->language->get('heading_title'),
						'content' => $content
					);
				}
			}

			$this->load->model('extension/extension');

			$extensions = $this->model_extension_extension->getInstalled('fraud');

			foreach ($extensions as $extension) {
				if ($this->config->get($extension . '_status')) {
					$this->load->language('extension/fraud/' . $extension);

					$content = $this->load->controller('extension/fraud/' . $extension . '/order');

					if ($content) {
						$data['tabs'][] = array(
							'code'    => $extension,
							'title'   => $this->language->get('heading_title'),
							'content' => $content
						);
					}
				}
			}
			
			// The URL we send API requests to
			$data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;
			
			// API login
			$this->load->model('user/api');

			$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

			if ($api_info) {
				$data['api_id'] = $api_info['api_id'];
				$data['api_key'] = $api_info['key'];
				$data['api_ip'] = $this->request->server['REMOTE_ADDR'];
			} else {
				$data['api_id'] = '';
				$data['api_key'] = '';
				$data['api_ip'] = '';
			}

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');


        // check weather customer invoice print module is enable or not
      $successMsg = '';
      $errorMsg = '';
      if($this->session->data['success_msg']) {
        $successMsg = $this->session->data['success_msg'];
        unset($this->session->data['success_msg']);
      }
      if($this->session->data['success']){
        $successMsg.= "<br>".$this->session->data['success'];
        unset($this->session->data['success']);
      }
      if($this->session->data['error']){
        $errorMsg = $this->session->data['error'];
        unset($this->session->data['error']);
      }
      $data['success_msg'] = $successMsg;
      $data['error_msg'] = $errorMsg;
      
      if($this->config->get('delhivery_lastmile_auto_manifest')){
      $this->load->model('extension/module/manageawb');
      $getLocationData = $this->model_extension_module_manageawb->getDelhiveryLastmileAllLocations();
      $locationOptions = '';
      foreach($getLocationData as $_locationData){
        $locationOptions.= '<option value="'.$_locationData['values'].'">'.$_locationData['label'].'</option>';
      }
      
      $locationData = '<div class="form-group">
                  <label class="col-sm-2 control-label" for="input-delhivery_lastmile_location">Select Location</label>
                  <div class="col-sm-4">
                    <select name="delhivery_lastmile_location"  required="true" id="input-delhivery_lastmile_location" class="form-control required">
            <option value=""></option>
            '.$locationOptions.'
                    </select>
                  </div>
                </div>';
      }else{
        $locationData = '';
      }
      
      if($this->config->get('delhivery_lastmile_status') and $product_option_weight <= 5000 and $this->config->get("delhivery_lastmile_carrier_title")!='')  {
        $data['awbGenerateUrl'] = $this->url->link('extension/module/manageawb/generateAwbs', 'order_id=' . $this->request->get['order_id'], true);
       
        $data['tabs'][] = array(
              'code'    => 'delhivery-lastmiles',
              'title'   => 'Delhivery Booking',
              'content' => '<fieldset>
            <legend>Assign Delhivery Lastmile Tracking</legend>
            <form method="post" action="'.$this->url->link('extension/module/manageawb/saveTrackingInformation').'&token='.$this->session->data['token'].'" class="form-horizontal">
            <input type="hidden" name="order_id" value="'.$this->request->get['order_id'].'" />
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-delhivery-lastmile">Select Carrier</label>
              <div class="col-sm-4">
              <select required="true" name="order_delhivery_lastmile" id="input-delhivery-lastmile" class="form-control required">
                <option value=""></option>
                <option value="delhivery_lastmile">'.$this->config->get("delhivery_lastmile_carrier_title").'</option>
              </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-awbdata">AWB</label>
              <div class="col-sm-4">
              <input type="text" name="awbs" class="form-control required" readonly="readonly" required="true" value="" id="input-awbdata">
              </div>
            </div>
            '.$locationData.'
            <div class="text-right">
              <button type="submit" id="button-dhlawb" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Assign Tracking</button>
              
              <button type="button" id="button-popupCalculation" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-truck"></i> Calculate Shipping Charges</button>
            </div>
            </form>
          </fieldset>
          <div class="shipping_calculation_data" id="popupForCalculation" style="display:none;">
            <div class="popup-content-area">
            <a class="close closeCalculationPopup" href="javascript:;void(0)">&times;</a>
              <h3>Shipping Charges</h3>
              <div class="shipping_calculate_form">
                <fieldset>
                  <form id="ship_calculation" class="form-horizontal">
                    <div class="form-group custom_form_field_section">
                      <label class="col-sm-7 control-label" for="ship_md">Billing mode of shipment(md)</label>
                      <div class="col-sm-4">
                        <select  name="md" id="ship_md" class="form-control">
                          <option value="">Select</option>
                          <option value="E">E</option>
                          <option value="S">S</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group custom_form_field_section">
                      <label class="col-sm-8 control-label" for="ship_pt">Payment type of shipment(pt)</label>
                      <div class="col-sm-4">
                        <select name="pt" id="ship_pt" class="form-control">
                          <option value="">Select</option>
                          <option value="COD">COD</option>
                          <option value="Pre-paid">Pre-paid</option>
                        </select>
                      </div>
                    </div>
                    <div style="width: 100%;border-bottom: 1px solid #f3eded;float: left;"></div>
                    <div class="form-group custom_form_field_section">
                      <label class="col-sm-7 control-label" for="ship_ss">Delivery status of shipment(ss)</label>
                      <div class="col-sm-4">
                        <select name="ss" id="ship_ss" class="form-control">
                          <option value="">Select</option>
                          <option value="Delivered">Delivered</option>
                          <option value="RTO">RTO</option>
                          <option value="DTO">DTO</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group custom_form_field_section">
                      <label style="padding-top:0px;" class="col-sm-8 control-label" for="ship_cgm">Chargeable weight of shipment (cgm)</label>
                      <div class="col-sm-4">
                        <input type="text" name="cgm" id="ship_cgm" class="form-control" value="" />
                      </div>
                    </div>
                    <div style="width: 100%;border-bottom: 1px solid #f3eded;float: left;"></div>
                    <div class="form-group custom_form_field_section">
                      <label class="col-sm-7 control-label" for="ship_cgm">Postcode of Origin center(oc)</label>
                      <div class="col-sm-4">
                        <input type="text" name="oc" id="ship_oc" class="form-control" value="" />
                      </div>
                    </div>
                
                    <button type="button" id="button-calculate" data-loading-text="Loading..." class="btn btn-primary" style="margin: 7px 10%;"><i class="fa fa-truck"></i> Calculate Shipping Charges</button>
                    <div style="width: 100%;border-bottom: 1px solid #f3eded;float: left;"></div>
                  </form>
                </fieldset>
              </div>
              <div class="shipping_calculate_response_data">
              </div>
            </div>
          </div>
          
          <script type="text/javascript">
          
          $("#button-calculate").on("click", function() {
            var urlsS ="'.$this->url->link('extension/module/manageawb/shippingCalculation', 'token=' . $this->request->get['order_id'], true).'&token='.$this->session->data['token'].'";
            var orderId = "'.$this->request->get['order_id'].'";
            $.ajax({
              url: urlsS,
              type: "post",
              data: {order_id:orderId,md:$("#ship_md").val(),pt: $("#ship_pt").val(),ss: $("#ship_ss").val(),cgm:$("#ship_cgm").val(),oc:$("#ship_oc").val()},
              beforeSend: function() {
                $("#button-calculate").button("loading");
              },
              complete: function() {
                $("#button-calculate").button("reset");
                //alert("Complete");
              },
              success: function(json) {
                $(".shipping_calculate_response_data").html(json);
              },
              error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
            });
          });
          $("#button-popupCalculation").on("click", function() {
            $("#popupForCalculation").show(200);
          });
          
          $(".closeCalculationPopup").on("click", function() {
            $(".shipping_calculate_response_data").html("");
            $("#popupForCalculation").hide(200);
          });
          
          $("#input-delhivery-lastmile").on("change", function() {
            if($(this).val()=="delhivery_lastmile"){
              var urls ="'.$this->url->link('extension/module/manageawb/generateAwbs', 'token=' . $this->request->get['order_id'], true).'&token='.$this->session->data['token'].'";
              var orderId = "'.$this->request->get['order_id'].'";
              $.ajax({
                url: urls,
                type: "post",
                //dataType: "json",
                data: "order_id=" + orderId,
                beforeSend: function() {
                  $("#button-dhlawb").button("loading");
                },
                complete: function() {
                  $("#button-dhlawb").button("reset");
                  //alert("Complete");
                },
                success: function(json) {
                  var splitData = json.split("@");
                  if(splitData[0]=="success"){
                    $("#input-awbdata").val(splitData[1]);
                  }else{
                    $("#input-awbdata").val("");
                    alert(splitData[1]);
                  }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });
            }else{$("#input-awbdata").val("");}
          });
          </script>
          <style>
            .shipping_calculation_data{
              position: fixed;
              top: 0;
              bottom: 0;
              left: 0;
              right: 0;
              background: rgba(0, 0, 0, 0.7);
              transition: opacity 500ms;
              z-index: 99999;
              overflow-y:scroll;
            }
            .popup-content-area {
              margin: 40px auto 0px;
              padding: 10px 20px;
              background: #fff;
              border-radius: 5px;
              width: 60%;
              position: relative;
              transition: all 5s ease-in-out;
            }
            .popup-content-area h3{
              border-bottom: 2px solid #05659a;
              padding-bottom: 10px;
            }
            .popup-content-area .close {
              position: absolute;
              top: 7px;
              right: 20px;
              transition: all 200ms;
              font-size: 30px;
              font-weight: bold;
              text-decoration: none;
              color: #333;
            }
            .form-group.custom_form_field_section {
              width: 47%;
              float: left;
            }
            .form-group.custom_form_field_section .col-sm-4 {
              padding-left:0px;
              padding-right: 0px;
            }
            .form-group.custom_form_field_section .col-sm-7.control-label,.form-group.custom_form_field_section .col-sm-8.control-label {
              padding-right:5px;
              padding-left:0px;
            }
            .form-group + .form-group {
              border-top: none;
            }
            .shipping_calculate_form .form-horizontal .form-group {
              margin-left: 0px;
              margin-right: 0px;
            }
            .shipping_calculate_form .form-group {
              padding-top: 7px;
              padding-bottom: 7px;
              margin-bottom: 0;
            }
            @media screen and (max-width: 700px){
              .popup-content-area{
                width: 100%;
                padding:20px 5px;
              }
            }
          </style>
        ');
      
      }
if($this->config->get('delhivery_lastmile_status') and $product_option_weight > 5000 and $this->config->get("delhivery_lastmile_carrier_title")!='')  {
        $data['awbGenerateUrl'] = $this->url->link('extension/module/manageawb/generateAwbs', 'order_id=' . $this->request->get['order_id'], true);
       
        $data['tabs'][] = array(
              'code'    => 'delhivery-lastmiles',
              'title'   => 'Delhivery Booking',
              'content' => '<fieldset>
            <legend>Booking is disabled because the order weight is greater than 5 kg</legend>	 
 ');
}
       

			if($this->config->get('shipway_login')){
				$this->load->model('shipway/courier_tracking');
				$data['couriers'] = $this->model_shipway_courier_tracking->getCourierList(true);
				//$this->data['trackingshipment_orderid'] = $order_id;
			}			
			
			$this->response->setOutput($this->load->view('sale/order_info', $data));
		} else {
			return new Action('error/not_found');
		}
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	public function createInvoiceNo() {
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} elseif (isset($this->request->get['order_id'])) {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$invoice_no = $this->model_sale_order->createInvoiceNo($order_id);

			if ($invoice_no) {
				$json['invoice_no'] = $invoice_no;
			} else {
				$json['error'] = $this->language->get('error_action');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function ExportTracking() {

        $this->load->model('sale/order');
        $orders= array();
        $data = array();
        $results = $this->model_sale_order->getOrderToExport($data);
    
        foreach ($results as $result) {
          $orders[] = array(
            'order_id'         => $result['order_id'],
            'tracking_id'      => $result['tracking_id'],
            'customer'         => $result['customer'],
            'date_added'       => date($this->language->get('date_format_short'), strtotime($result['date_added']))
          );
        }   
    
        $orders_data = array();
        $orders_column=array();
        $orders_column = array('Order ID','Tracking Id','Customer Name','Date Added');
        $orders_data[0]=   $orders_column;  
    
        foreach($orders as $orders_row)
        {
          $orders_data[]=   $orders_row;            
        }
        require_once(DIR_SYSTEM . 'library/PHPExcel.php');
        $filename='orders_tracking_list_'.date('Y-m-d _ H:i:s');
        $filename = preg_replace('/[^aA-zZ0-9\_\-]/', '', $filename);
                            // Create new PHPExcel object
    
        $objPHPExcel = new PHPExcel();
    
        $objPHPExcel->getActiveSheet()->fromArray($orders_data, null, 'A1');
                            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
    
                            // Save Excel 95 file
    
        $callStartTime = microtime(true);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    
                            //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment; filename=\"" . $filename . ".xls\"");
    
        header('Cache-Control: max-age=0');
    
        $objWriter->save('php://output');

    }


	public function addReward() {
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info && $order_info['customer_id'] && ($order_info['reward'] > 0)) {
				$this->load->model('customer/customer');

				$reward_total = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($order_id);

				if (!$reward_total) {
					$this->model_customer_customer->addReward($order_info['customer_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['reward'], $order_id);
				}
			}

			$json['success'] = $this->language->get('text_reward_added');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function removeReward() {
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {
				$this->load->model('customer/customer');

				$this->model_customer_customer->deleteReward($order_id);
			}

			$json['success'] = $this->language->get('text_reward_removed');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addCommission() {
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {
				$this->load->model('marketing/affiliate');

				$affiliate_total = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($order_id);

				if (!$affiliate_total) {
					$this->model_marketing_affiliate->addTransaction($order_info['affiliate_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['commission'], $order_id);
				}
			}

			$json['success'] = $this->language->get('text_commission_added');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function removeCommission() {
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {
				$this->load->model('marketing/affiliate');

				$this->model_marketing_affiliate->deleteTransaction($order_id);
			}

			$json['success'] = $this->language->get('text_commission_removed');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function history() {
		
		$this->load->language('sale/order');

		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_notify'] = $this->language->get('column_notify');
		$data['column_comment'] = $this->language->get('column_comment');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['histories'] = array();

		$this->load->model('sale/order');

		$results = $this->model_sale_order->getOrderHistories($this->request->get['order_id'], ($page - 1) * 10, 10);

		foreach ($results as $result) {
			$data['histories'][] = array(
				'notify'     => $result['notify'] ? $this->language->get('text_yes') : $this->language->get('text_no'),
				'status'     => $result['status'],
				'comment'    => nl2br($result['comment']),
				'user'       => $result['user'],
				'date_added' => date($this->language->get('datetime_format'), strtotime($result['date_added']))
			);
		}

		$history_total = $this->model_sale_order->getTotalOrderHistories($this->request->get['order_id']);

		$pagination = new Pagination();
		$pagination->total = $history_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('sale/order/history', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($history_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($history_total - 10)) ? $history_total : ((($page - 1) * 10) + 10), $history_total, ceil($history_total / 10));

		$this->response->setOutput($this->load->view('sale/order_history', $data));
	}
	
	public function delhivery_label(){
	   
    	 $this->load->language('delhivery_lastmile/manageawb');
    	 $this->load->model('extension/module/manageawb');
    	
         
    	 if (isset($this->request->post['selected'])) {
			$awbs = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$awbs[] = $this->request->get['order_id'];
		}
		
      if($awbs){
          
        $modeIs = $this->config->get("delhivery_lastmile_is_production");
        if($modeIs){
          $apiurl='https://track.delhivery.com/api/p/packing_slip';
        }else{
          $apiurl='http://test.delhivery.com/api/p/packing_slip';
        }
        $cl = $this->config->get("delhivery_lastmile_client_id");
        $token = $this->config->get("delhivery_lastmile_license_key");
        $this->load->model('extension/module/manageawb');
        $this->load->model('extension/module/managelocation');
        $this->load->model('sale/order');
        if($cl && $token){
          $awbArray=array();
          foreach($awbs as $awb)
          {
            $awbmodel=$this->model_extension_module_manageawb->getAwbno($awb);
            
            if($awbmodel['state'] == 1)
            {
              array_push($awbArray,$awbmodel['awb']);
            }
          }
          if($awbArray){
            $apiurl =  $apiurl.'?wbns='.implode(',',$awbArray);
            $resultData=$this->model_extension_module_manageawb->Executecurl2($apiurl,$token);
            $resultData = json_decode($resultData);
            $objArray=get_object_vars($resultData);
            
            $labeldata=array();
            $label=array();
            $page_break=false;
            if(array_key_exists("packages_found",$objArray) && $objArray['packages_found']){
              foreach($resultData->packages as $codes){
                $subtotal=0;
                $awbModel=$this->model_extension_module_manageawb->getAwbdataByawb($codes->wbn);
                $LocationModel =$this->model_extension_module_managelocation->getLocation($awbModel['pickup_location_id']);
                $orderModel=$this->model_sale_order->getOrder($codes->oid);
                $methodcode = (strtolower($this->config->get("delhivery_lastmile_cod_method")) == strtolower($orderModel['payment_code']) || $order['payment_code']=="cod") ?"COD":"Pre-Paid";
                $products = $this->model_sale_order->getOrderProducts($codes->oid);
                $r= $this->currency->getSymbolRight($orderModel['currency_code']);
                $l= $this->currency->getSymbolLeft($orderModel['currency_code']);
                $productOrdered=array();
                $shipping_address=$orderModel['shipping_firstname'];
                $shipping_address.=($orderModel['shipping_lastname'])?" ".$orderModel['shipping_lastname']:"";
                $shipping_address.="<br>".$orderModel['shipping_address_1'];
                $shipping_address.=($orderModel['shipping_address_2'])?", ".$orderModel['shipping_address_2']:"";
                $shipping_address.=($orderModel['shipping_postcode'])?", ".$orderModel['shipping_postcode']:"";
                $shipping_address.=($orderModel['shipping_city'])?", ".$orderModel['shipping_city']:"";
                $shipping_address.=($orderModel['shipping_zone'])?", ".$orderModel['shipping_zone']:"";
                $shipping_address.=($orderModel['shipping_country'])?", ".$orderModel['shipping_country']:"";
                $shipping_address.=($orderModel['telephone'])?"<br>T: ".$orderModel['telephone']:"";
                foreach($products as $product){
                  $subtotal=$subtotal + floatval($product['total']);
                  array_push($productOrdered,array("name"=>$product['name'],"qty"=>$product['quantity'],"total"=>$l.number_format($product['total'],2).$r,"price"=>$l.number_format($product['price'],2).$r));
                }
                $totals = $this->model_sale_order->getOrderTotals($codes->oid);
                $Ototals=array();
                foreach($totals as $total){
                  array_push($Ototals,array("title"=>$total['title'],"value"=>$l.number_format($total['value'],2).$r));
                }
                //echo "<pre>";
                //print_r($orderModel);die;
                $labeldata['items']=$productOrdered;
                $labeldata['page_break']=$page_break;
                $labeldata['delhivery_logo']=$codes->delhivery_logo;
                $labeldata['barcode']=$codes->barcode;
                $labeldata['order_id']=$codes->oid;
                $labeldata['order']=$orderModel;
                $labeldata['shipping_address']=$shipping_address;
                $labeldata['grandTotle']=$l.number_format($orderModel['total'],2).$r;
                $labeldata['methodcode']=$methodcode;
                $labeldata['subtotal']=$l.$subtotal.$r;
                $labeldata['shippingAmount']=$l.number_format($orderModel['total']-$subtotal,2).$r;
                $labeldata['gst']=$this->config->get("delhivery_lastmile_gst_no");
                $labeldata['cst']=$this->config->get("delhivery_lastmile_cst_no");
                $labeldata['tin']=$this->config->get("delhivery_lastmile_consignee_tin");
                $labeldata['store_name']=$this->config->get("delhivery_lastmile_store_name");
                $labeldata['store_address']=$this->config->get("delhivery_lastmile_store_address");
                $labeldata['return_address']=$this->config->get("delhivery_lastmile_return_address");
                $labeldata['O_totals']=$Ototals;
                $label["delhiveryLastmile_manageLabel"][]=$labeldata;
                $page_break=true;
              }
              $flag=false;
              $this->response->setOutput($this->load->view('sale/delhivery_label', $label));
            }else{
              $this->session->data['error'] = "No Shipping label available at this moment.";
              $this->response->redirect($this->url->link('extension/module/manageawb', 'token=' . $this->session->data['token'], 'SSL'));
            }
          }else{
            $this->session->data['error'] = "Sorry! Something Went wrong. Please select used Awb Records.";
            $this->response->redirect($this->url->link('extension/module/manageawb', 'token=' . $this->session->data['token'], 'SSL'));
          }
        }else{
          $this->session->data['error'] = $this->session->data['error']."#".$awbmodel['awb']." Please add valid License Key and Gateway URL in plugin configuration";
          $this->response->redirect($this->url->link('extension/module/manageawb', 'token=' . $this->session->data['token'], 'SSL'));
        }
      }else{
        $this->session->data['error'] = "Sorry! Something Went wrong. Please select Awb Records.";
        $this->response->redirect($this->url->link('extension/module/manageawb', 'token=' . $this->session->data['token'], 'SSL'));
      }

	}

	public function invoice() {
		$this->load->language('sale/order');

		$data['title'] = $this->language->get('text_invoice');

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = $this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = $this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');

		$this->load->model('sale/order');

		$this->load->model('setting/setting');

		$data['orders'] = array();

		$orders = array();

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$orders = array(
		        0 => $this->request->get['order_id']
		    );
		}

		foreach ($orders as $order_id) {
			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {
				$store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

				if ($store_info) {
					$store_address = $store_info['config_address'];
					$store_email = $store_info['config_email'];
					$store_telephone = $store_info['config_telephone'];
					$store_fax = $store_info['config_fax'];
				} else {
					$store_address = $this->config->get('config_address');
					$store_email = $this->config->get('config_email');
					$store_telephone = $this->config->get('config_telephone');
					$store_fax = $this->config->get('config_fax');
				}

				if ($order_info['invoice_no']) {
					$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
				} else {
					$invoice_no = '';
				}

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['payment_firstname'],
					'lastname'  => $order_info['payment_lastname'],
					'company'   => $order_info['payment_company'],
					'address_1' => $order_info['payment_address_1'],
					'address_2' => $order_info['payment_address_2'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);

				$payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);

				$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');


        //xtensions- Custom Fields
				$customer_fields =array();
				if($this->xtensions_checkout->isActive('xtensions_best_checkout',$order_info['store_id'])){
					$customer_fields =array();
					$fieldData = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getCustomFieldByIdentifier($order_info['custom_field'],'account');
					foreach($this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getCustomFieldsAll($order_info['customer_group_id']) as $field){
						if($field['location']=='account' && $field['invoice']){
							if(isset($fieldData[$field['identifier']])){
								$customer_fields[] = $fieldData[$field['identifier']];
							}
						}
					}
					$formatted_address = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getFormattedAddressForOrder($order_info);
					$payment_address = $formatted_address['payment_address_formatted'];
					$shipping_address = $formatted_address['shipping_address_formatted'];
				}
				//xtensions- Custom Fields
			
				$product_data = array();

				$products = $this->model_sale_order->getOrderProducts($order_id);

				foreach ($products as $product) {
					$option_data = array();

					$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

					foreach ($options as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $value
						);
					}

					$product_data[] = array(
						'name'     => $product['name'],
						'model'    => $product['model'],
						'sku'       => $product['sku'],
						'option'   => $option_data,
						'quantity' => $product['quantity'],
						'weight'   => $product['weight'],
						'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				$voucher_data = array();

				$vouchers = $this->model_sale_order->getOrderVouchers($order_id);

				foreach ($vouchers as $voucher) {
					$voucher_data[] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				$total_data = array();

				$totals = $this->model_sale_order->getOrderTotals($order_id);

				foreach ($totals as $total) {
					$total_data[] = array(
						'title' => $total['title'],
					    'code' => $total['code'],
						'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				$data['orders'][] = array(

        //xtensions- Custom Fields
        'customer_fields'	 => $customer_fields,
        //xtensions- Custom Fields
			
					'order_id'	       => $order_id,
					'is_gift'           => $order_info['is_gift'],
					'invoice_no'       => $invoice_no,
					'date_added'       => date($this->language->get('datetime_format'), strtotime($order_info['date_added'])),
					'store_name'       => $order_info['store_name'],
					'store_url'        => rtrim($order_info['store_url'], '/'),
					'store_address'    => nl2br($store_address),
					'store_email'      => $store_email,
					'store_telephone'  => $store_telephone,
					'store_fax'        => $store_fax,
					'email'            => $order_info['email'],
					'telephone'        => $order_info['telephone'],
					'shipping_address' => $shipping_address,
					'shipping_method'  => $order_info['shipping_method'],
					'payment_address'  => $payment_address,
					'payment_method'   => $order_info['payment_method'],
					'product'          => $product_data,
					'voucher'          => $voucher_data,
					'total'            => $total_data,
					'comment'          => nl2br($order_info['comment'])
				);
			}
		}

		$this->response->setOutput($this->load->view('sale/order_invoice', $data));
	}
	
	public function picklist() {
		$this->load->language('sale/order');

		$data['title'] = $this->language->get('text_picklist');

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_picklist'] = $this->language->get('text_picklist');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_quantity'] = $this->language->get('column_quantity');
		
		$this->load->model('sale/order');
		
		$data['orders'] = array();

		$orders = array();
		$merged_products = array();

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} else if($this->request->get['order_id']) {
		    $orders = array(
		        0 => $this->request->get['order_id']
		    );
		}

		foreach ($orders as $order_id) {
			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {

        //xtensions- Custom Fields
				$customer_fields =array();
				if($this->xtensions_checkout->isActive('xtensions_best_checkout',$order_info['store_id'])){
					$customer_fields =array();
					$fieldData = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getCustomFieldByIdentifier($order_info['custom_field'],'account');
					foreach($this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getCustomFieldsAll($order_info['customer_group_id']) as $field){
						if($field['location']=='account' && $field['invoice']){
							if(isset($fieldData[$field['identifier']])){
								$customer_fields[] = $fieldData[$field['identifier']];
							}
						}
					}
					$formatted_address = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getFormattedAddressForOrder($order_info);
					$payment_address = $formatted_address['payment_address_formatted'];
					$shipping_address = $formatted_address['shipping_address_formatted'];
				}
				//xtensions- Custom Fields
			
				$product_data = array();
				$products = $this->model_sale_order->getOrderProducts($order_id);

				foreach ($products as $product) {
				    $plastic_free_packaging = 0;
					$option_data = array();

					$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

					foreach ($options as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}
						$options_weight = 0;
						if($product['product_id'] == 6438 || $product['product_id'] == 6449) {
						    $options_weight_data = $this->model_sale_order->getOrderOptionsWeight($option['product_option_value_id'], $option['product_option_id']);
						    if($options_weight_data['weight_prefix'] == '+') {
						        $options_weight = $product['weight'] + $options_weight_data['weight'];
						    } else if($options_weight_data['weight_prefix'] == '-') {
						        $options_weight = $product['weight'] - $options_weight_data['weight'];
						    }
						}
						if($option['name'] == "Packaging Material") {
						    $plastic_free_packaging = 1;
					    }
						
					    if($options_weight > 0) {
					        $option_data[] = array(
							    'name'  => $option['name'],
							    'value' => $value,
							    'weight'=> $options_weight
						    );
					    } else {
					        $option_data[] = array(
							    'name'  => $option['name'],
							    'value' => $value
						    );
					    }
					    if ($option['name'] == "Crispy Munchies") {
					        $pack = " (Pack of 7) - White Matt Pouch";
					    } else if ($option['name'] == "Exotic Indulgence") {
					        $pack = " (Pack of 5) - Glass Buffer Bottle";
					    } else if ($option['name'] == "Indulgence Pack") {
					        $pack = " (Pack of 3) - PET Bottle";
					    } else {
					        $pack = "";
					    }
					}
					
					if($plastic_free_packaging == 0 && ($product['product_id'] == 6438 || $product['product_id'] == 6449)) {
					    $regular_packaging =  array(
						    'name'  => "Packaging Material",
						    'value' => "Regular Packaging"
					    );
					    array_push($option_data, $regular_packaging);
					}

					$product_data[] = array(
					    'product_id'    => $product['product_id'],
						'name'          => $product['name'],
						'pack'          => $pack,
						'option'        => $option_data,
						'quantity'      => $product['quantity'],
						'weight'        => $product['weight']
					);
				}
				
				foreach($product_data AS $order_product) {
				    $found = 0;
				    foreach($merged_products AS &$merged_product) {
				        if($order_product['name'] == $merged_product['name'] && $order_product['option'] == $merged_product['option']) {
				            $merged_product['quantity'] += $order_product['quantity'];
				            $found = 1;
				            break;
				        }
				    }
				    if($found == 0) {
				        $merged_products[] = array(
				            'name'      => $order_product['name'],
				            'pack'      => $order_product['pack'],
						    'option'    => $order_product['option'],
						    'quantity'  => $order_product['quantity'],
						    'weight'    => $order_product['weight']
				        );
				    }
				}
			}
		}
		$data['merged_products'] = $merged_products;
		

		$this->response->setOutput($this->load->view('sale/order_picklist', $data));
	}
	
	public function barcode_invoice() {
		$this->load->language('sale/order');

		$data['title'] = $this->language->get('text_invoice');

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = $this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = $this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');

		$this->load->model('sale/order');

		$this->load->model('setting/setting');

		$data['orders'] = array();

		$orders = array();

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$orders[] = $this->request->get['order_id'];
		}

		foreach ($orders as $order_id) {
			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {
				$store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

				if ($store_info) {
					$store_address = $store_info['config_address'];
					$store_email = $store_info['config_email'];
					$store_telephone = $store_info['config_telephone'];
					$store_fax = $store_info['config_fax'];
				} else {
					$store_address = $this->config->get('config_address');
					$store_email = $this->config->get('config_email');
					$store_telephone = $this->config->get('config_telephone');
					$store_fax = $this->config->get('config_fax');
				}

				if ($order_info['invoice_no']) {
					$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
				} else {
					$invoice_no = '';
				}

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['payment_firstname'],
					'lastname'  => $order_info['payment_lastname'],
					'company'   => $order_info['payment_company'],
					'address_1' => $order_info['payment_address_1'],
					'address_2' => $order_info['payment_address_2'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);

				$payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);

				$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');


        //xtensions- Custom Fields
				$customer_fields =array();
				if($this->xtensions_checkout->isActive('xtensions_best_checkout',$order_info['store_id'])){
					$customer_fields =array();
					$fieldData = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getCustomFieldByIdentifier($order_info['custom_field'],'account');
					foreach($this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getCustomFieldsAll($order_info['customer_group_id']) as $field){
						if($field['location']=='account' && $field['invoice']){
							if(isset($fieldData[$field['identifier']])){
								$customer_fields[] = $fieldData[$field['identifier']];
							}
						}
					}
					$formatted_address = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getFormattedAddressForOrder($order_info);
					$payment_address = $formatted_address['payment_address_formatted'];
					$shipping_address = $formatted_address['shipping_address_formatted'];
				}
				//xtensions- Custom Fields
			
				$product_data = array();

				$products = $this->model_sale_order->getOrderProducts($order_id);

				foreach ($products as $product) {
					$option_data = array();

					$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

					foreach ($options as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $value
						);
					}

					$product_data[] = array(
						'name'     => $product['name'],
						'model'    => $product['model'],
						'sku'       => $product['sku'],
						'option'   => $option_data,
						'quantity' => $product['quantity'],
						'weight'   => $product['weight'],
						'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				$voucher_data = array();

				$vouchers = $this->model_sale_order->getOrderVouchers($order_id);

				foreach ($vouchers as $voucher) {
					$voucher_data[] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				$total_data = array();

				$totals = $this->model_sale_order->getOrderTotals($order_id);

				foreach ($totals as $total) {
					$total_data[] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
					);
				}

				$data['orders'][] = array(

        //xtensions- Custom Fields
        'customer_fields'	 => $customer_fields,
        //xtensions- Custom Fields
			
					'order_id'	       => $order_id,
					'invoice_no'       => $invoice_no,
					'date_added'       => date($this->language->get('datetime_format'), strtotime($order_info['date_added'])),
					'store_name'       => $order_info['store_name'],
					'store_url'        => rtrim($order_info['store_url'], '/'),
					'store_address'    => nl2br($store_address),
					'store_email'      => $store_email,
					'store_telephone'  => $store_telephone,
					'store_fax'        => $store_fax,
					'email'            => $order_info['email'],
					'telephone'        => $order_info['telephone'],
					'shipping_address' => $shipping_address,
					'shipping_method'  => $order_info['shipping_method'],
					'payment_address'  => $payment_address,
					'payment_method'   => $order_info['payment_method'],
					'product'          => $product_data,
					'voucher'          => $voucher_data,
					'total'            => $total_data,
					'comment'          => nl2br($order_info['comment'])
				);
			}
		}

		$this->response->setOutput($this->load->view('sale/barcode_invoice', $data));
	}
	

	public function shipping() {
		$this->load->language('sale/order');

		$data['title'] = $this->language->get('text_shipping');

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_shipping'] = $this->language->get('text_shipping');
		$data['text_picklist'] = $this->language->get('text_picklist');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = $this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_sku'] = $this->language->get('text_sku');
		$data['text_upc'] = $this->language->get('text_upc');
		$data['text_ean'] = $this->language->get('text_ean');
		$data['text_jan'] = $this->language->get('text_jan');
		$data['text_isbn'] = $this->language->get('text_isbn');
		$data['text_mpn'] = $this->language->get('text_mpn');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_location'] = $this->language->get('column_location');
		$data['column_reference'] = $this->language->get('column_reference');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_weight'] = $this->language->get('column_weight');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');

		$this->load->model('sale/order');

		$this->load->model('catalog/product');

		$this->load->model('setting/setting');

		$data['orders'] = array();

		$orders = array();

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$orders[] = $this->request->get['order_id'];
		}

		foreach ($orders as $order_id) {
			$order_info = $this->model_sale_order->getOrder($order_id);

			// Make sure there is a shipping method
			if ($order_info && $order_info['shipping_code']) {
				$store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

				if ($store_info) {
					$store_address = $store_info['config_address'];
					$store_email = $store_info['config_email'];
					$store_telephone = $store_info['config_telephone'];
					$store_fax = $store_info['config_fax'];
				} else {
					$store_address = $this->config->get('config_address');
					$store_email = $this->config->get('config_email');
					$store_telephone = $this->config->get('config_telephone');
					$store_fax = $this->config->get('config_fax');
				}

				if ($order_info['invoice_no']) {
					$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
				} else {
					$invoice_no = '';
				}

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);

				$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');


        //xtensions- Custom Fields
				$customer_fields =array();
				if($this->xtensions_checkout->isActive('xtensions_best_checkout',$order_info['store_id'])){
					$customer_fields =array();
					$fieldData = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getCustomFieldByIdentifier($order_info['custom_field'],'account');
					foreach($this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getCustomFieldsAll($order_info['customer_group_id']) as $field){
						if($field['location']=='account' && $field['invoice']){
							if(isset($fieldData[$field['identifier']])){
								$customer_fields[] = $fieldData[$field['identifier']];
							}
						}
					}
					$formatted_address = $this->xtensions_checkout->getXtensionsModel($this->config->get('xtensions_custom_fields_path'))->getFormattedAddressForOrder($order_info);
					$payment_address = $formatted_address['payment_address_formatted'];
					$shipping_address = $formatted_address['shipping_address_formatted'];
				}
				//xtensions- Custom Fields
			
				$product_data = array();

				$products = $this->model_sale_order->getOrderProducts($order_id);

				foreach ($products as $product) {
					$option_weight = '';

					$product_info = $this->model_catalog_product->getProduct($product['product_id']);

					if ($product_info) {
						$option_data = array();

						$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

						foreach ($options as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

								if ($upload_info) {
									$value = $upload_info['name'];
								} else {
									$value = '';
								}
							}

							$option_data[] = array(
								'name'  => $option['name'],
								'value' => $value
							);

							$product_option_value_info = $this->model_catalog_product->getProductOptionValue($product['product_id'], $option['product_option_value_id']);

							if ($product_option_value_info) {
								if ($product_option_value_info['weight_prefix'] == '+') {
									$option_weight += $product_option_value_info['weight'];
								} elseif ($product_option_value_info['weight_prefix'] == '-') {
									$option_weight -= $product_option_value_info['weight'];
								}
							}
						}

						$product_data[] = array(
							'name'     => $product_info['name'],
							'model'    => $product_info['model'],
							'option'   => $option_data,
							'quantity' => $product['quantity'],
							'location' => $product_info['location'],
							'sku'      => $product_info['sku'],
							'upc'      => $product_info['upc'],
							'ean'      => $product_info['ean'],
							'jan'      => $product_info['jan'],
							'isbn'     => $product_info['isbn'],
							'mpn'      => $product_info['mpn'],
							'weight'   => $this->weight->format(($product_info['weight'] + $option_weight) * $product['quantity'], $product_info['weight_class_id'], $this->language->get('decimal_point'), $this->language->get('thousand_point'))
						);
					}
				}

				$data['orders'][] = array(

        //xtensions- Custom Fields
        'customer_fields'	 => $customer_fields,
        //xtensions- Custom Fields
			
					'order_id'	       => $order_id,
					'invoice_no'       => $invoice_no,
					'date_added'       => date($this->language->get('datetime_format'), strtotime($order_info['date_added'])),
					'store_name'       => $order_info['store_name'],
					'store_url'        => rtrim($order_info['store_url'], '/'),
					'store_address'    => nl2br($store_address),
					'store_email'      => $store_email,
					'store_telephone'  => $store_telephone,
					'store_fax'        => $store_fax,
					'email'            => $order_info['email'],
					'telephone'        => $order_info['telephone'],
					'shipping_address' => $shipping_address,
					'shipping_method'  => $order_info['shipping_method'],
					'product'          => $product_data,
					'comment'          => nl2br($order_info['comment'])
				);
			}
		}

		$this->response->setOutput($this->load->view('sale/order_shipping', $data));
	}
	
	public function mms_label() {
	    $this->load->language('sale/order');
	    $this->load->model('sale/order');

		$data['title'] = "Make My Snack Label";

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$orders[] = $this->request->get['order_id'];
		}
		
		foreach ($orders as $order_id) {
			$order_info = $this->model_sale_order->getOrder($order_id);
			
			if ($order_info) {
				$label_data = array();
				$products = $this->model_sale_order->getOrderProducts($order_id);
				
				foreach ($products as $product) {
				    if($product['product_id'] == 6449) {
				        $ingredients = "";
				        $energy = 0;
				        $protein = 0;
				        $carbohydrates = 0;
				        $dietary_fibres = 0;
				        $fats = 0;
				        $sodium = 0;
				        $weight = 0;
				        
					    $option_data = array();
					    $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
                        $regular_packaging = 1; // Default Packaging
                        $allergy_almonds = 0;
                        
				    	foreach ($options as $option) {
				    	    $nutritional_info = $this->model_sale_order->getNutririonalInfo($option['product_option_value_id'], $product['product_id']);
				    	    
				    	    if($nutritional_info) {
				    	        $energy += $nutritional_info['energy'];
				    	        $protein += $nutritional_info['protein'];
				    	        $carbohydrates += $nutritional_info['carbohydrates'];
				    	        $dietary_fibres += $nutritional_info['dietary_fibres'];
				    	        $fats += $nutritional_info['fats'];
				    	        $sodium += $nutritional_info['sodium'];
				    	        $weight += $nutritional_info['weight'];
				    	    }
				    	    
					    	if ($option['type'] != 'file' && $option['name'] != "Packaging Material") {
					    		$value = $option['value'];
					    	} else {
					    	    $value = '';
					    	}
					    	
					    	if($option['name'] == "Packaging Material") {
					    	    $regular_packaging = 0; //Plastic free packaging
					    	}
					    	
						    if($value != '') {
					    	    $option_data[] = array(
					    	    	'name'  => $option['name'],
					    	    	'value' => $value
					    	    );
						    }
					    	if($option['name'] != "Seasoned Variety (Coated with Oil)" && $option['name'] != "Packaging Material") {
					    	    $option_name = " - " . $option['name'];
					    	}
					    }
					    
					    $day_of_year = date('z') + 1;
		                if($day_of_year < 10) {
                            $batch_prefix = '00';
                        } else if ($day_of_year < 100) {
                            $batch_prefix = '0';
                        } else {
                            $batch_prefix = '';
                        }
                        $data['todays_batch'] = date('y') . 'TE' . $batch_prefix . $day_of_year;
        
					    foreach($option_data AS $option_value) {
					        if($option_value['value'] != "Plain") {
					            $ingredients .= $option_value['value'];
					            $ingredients .= ", ";
					        }
					        
					        if($option_value['value'] == "Almonds") {
					            $allergy_almonds = 1;
					        }
					    }
					    
					    $ingredients = substr($ingredients, 0, strlen($ingredients) - 2);
					    
					    //$value is ingredients
					    if($weight-3 < 250) {
					        $weight = 250;
					    } else {
					        $weight -= 3;
					    }
					    //Adjustment
					    $weight = 250;
					    
					    $divider = $weight / 100;

					    $label_data[] = array(
					    	'name'              => $product['name'] . "<span class='bold'>" . $option_name . "</span>",
					    	'price'             => round($product['price'] + $product['tax'], 0),
					    	'ingredients'       => $ingredients,
					    	'energy'            => round($energy / $divider,0),
					    	'protein'           => round($protein / $divider,1),
					    	'carbohydrates'     => round($carbohydrates / $divider,1),
					    	'dietary_fibres'    => round($dietary_fibres / $divider,1),
					    	'fats'              => round($fats / $divider,1),
					    	'sodium'            => round($sodium / $divider,1),
					    	'weight'            => $weight,
					    	'regular_packaging' => $regular_packaging
					    );
				    }
				}
				
				$data['allergy_almonds'] = $allergy_almonds;
				
				$data['labels'][] = array(
					'label'     => $label_data
				);
			}
		}
		$this->response->setOutput($this->load->view('sale/mms_label', $data));
	}
	
	public function mmm_label() {
	    $this->load->language('sale/order');
	    $this->load->model('sale/order');

		$data['title'] = "Make My Muesli Label";

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$orders[] = $this->request->get['order_id'];
		}
		
		foreach ($orders as $order_id) {
			$order_info = $this->model_sale_order->getOrder($order_id);
			
			if ($order_info) {
				$label_data = array();
				$products = $this->model_sale_order->getOrderProducts($order_id);
				
				foreach ($products as $product) {
				    if($product['product_id'] == 6345) {
				        $ingredients = "";
				        $energy = 0;
				        $protein = 0;
				        $carbohydrates = 0;
				        $sugar = 0;
				        $added_sugar = 0;
				        $dietary_fibres = 0;
				        $fats = 0;
				        $weight = 0;
				        
					    $option_data = array();
					    $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
                        $allergy_almonds = 0;
                        
				    	foreach ($options as $option) {
				    	    $nutritional_info = $this->model_sale_order->getNutririonalInfo($option['product_option_value_id'], $product['product_id']);
				    	    
				    	    if($nutritional_info) {
				    	        $energy += $nutritional_info['energy'];
				    	        $protein += $nutritional_info['protein'];
				    	        $carbohydrates += $nutritional_info['carbohydrates'];
				    	        $sugar += $nutritional_info['sugar'];
				    	        $added_sugar += $nutritional_info['added_sugar'];
				    	        $dietary_fibres += $nutritional_info['dietary_fibres'];
				    	        $fats += $nutritional_info['fats'];
				    	        $weight += $nutritional_info['weight'];
				    	    }
				    	    
					    	if ($option['type'] != 'file' && $option['name'] != "Packaging Material") {
					    		$value = $option['value'];
					    	} else {
					    	    $value = '';
					    	}
					    	
						    if($value != '') {
					    	    $option_data[] = array(
					    	    	'name'  => $option['name'],
					    	    	'value' => $value
					    	    );
						    }
						    
						    if($option['name'] == 'Base') {
						        $base_ingredients = $this->model_sale_order->getSelectedBaseIngredients($option['product_option_value_id']);
						    }
					    }
					    
					    $day_of_year = date('z') + 1;
		                if($day_of_year < 10) {
                            $batch_prefix = '00';
                        } else if ($day_of_year < 100) {
                            $batch_prefix = '0';
                        } else {
                            $batch_prefix = '';
                        }
                        $data['todays_batch'] = date('y') . 'TE' . $batch_prefix . $day_of_year;
        
					    foreach($option_data AS $option_value) {
					        if($option_value['value'] != "Plain") {
					            $ingredients .= $option_value['value'];
					            if($option_value['name'] == "Base") {
					                $ingredients .= " " . $base_ingredients;
					            }
					            $ingredients .= ", ";
					        }
					        
					        if($option_value['value'] == "Almonds") {
					            $allergy_almonds = 1;
					        }
					    }
					    
					    $ingredients = substr($ingredients, 0, strlen($ingredients) - 2);
					    
					    $divider = $weight / 100;

					    $label_data[] = array(
					    	'name'              => $product['name'],
					    	'price'             => round($product['price'] + $product['tax'], 0),
					    	'ingredients'       => $ingredients,
					    	'energy'            => round($energy / $divider,0),
					    	'protein'           => round($protein / $divider,1),
					    	'carbohydrates'     => round($carbohydrates / $divider,1),
					    	'sugar'             => round($sugar / $divider,1),
					    	'added_sugar'       => round($added_sugar / $divider,1),
					    	'dietary_fibres'    => round($dietary_fibres / $divider,1),
					    	'fats'              => round($fats / $divider,1),
					    	'weight'            => $weight
					    );
				    }
				}
				
				$data['allergy_almonds'] = $allergy_almonds;
				
				$data['labels'][] = array(
					'label'     => $label_data
				);
			}
		}
		$this->response->setOutput($this->load->view('sale/mmm_label', $data));
	}
}
