<?php

$_['adv_text_ext_name']					= 'Extension name:';
$_['adv_ext_name']						= 'ADV Products Report';
$_['adv_text_instal_version']			= 'Installed version:';
$_['adv_text_latest_version']			= 'Latest version:';
$_['adv_ext_version']					= '4.1';
$_['adv_ext_type']						= 'OCMod';
$_['adv_text_ext_compatibility']		= 'Extension compatibility:';
$_['adv_ext_compatibility']				= 'OpenCart v2.3.x.x';
$_['adv_text_ext_url']					= 'Extension URL:';
$_['adv_text_ext_support']				= 'Extension support:';
$_['adv_text_reg_status']				= 'License status:';
$_['adv_text_reg_info']					= 'License data:';
$_['adv_ext_support']					= 'opencart.reports@gmail.com';
$_['adv_ext_subject']      				= '%s support needed';
$_['adv_text_ext_legal']				= 'Extension legal notice:';
$_['adv_text_copyright']				= 'ADV Reports &amp; Statistics &copy; 2011-2016';
            
// Heading
$_['heading_title']							= '<img src="view/image/adv_reports/adv_icon_small.png"> <span style="color:#4170bd; font-weight:bold">ADV Products Report</span>';
$_['heading_title_main']					= 'ADV Products Report';

// Text
$_['text_edit']        						= 'Edit ADV Products Report';
$_['text_extension']   						= 'Extensions';
$_['text_asking_help']						= 'Read before requesting for support';
$_['text_help_request']						= 'Requesting for support';
$_['text_terms']							= 'Terms & Conditions';
$_['text_success']							= 'Success: You have modified module ADV Products Report!';

//Tab
$_['tab_about']            					= 'About';

// Button
$_['button_documentation']            		= 'Documentation';

// Error
$_['error_permission']    					= 'Warning: You do not have permission to modify module ADV Products Report!';