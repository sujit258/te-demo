<?php
class DB {
	private $adaptor;

	public function __construct($adaptor, $hostname, $username, $password, $database, $port = NULL) {
		$class = 'DB\\' . $adaptor;

		if (class_exists($class)) {
			$this->adaptor = new $class($hostname, $username, $password, $database, $port);
		} else {
			throw new \Exception('Error: Could not load database adaptor ' . $adaptor . '!');
		}
	}

	public function query($sql, $params = array()) {

//tri mod start
if (version_compare(VERSION, '2.3') >= 0) {
  $query = $this->adaptor->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` = 'timezone'");
} else {
  $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` = 'timezone'");
}

if ($query->num_rows) {
  foreach ($query->rows as $row) {
    if ($row['key'] == 'timezone_timezone') $timezone['timezone'] = $row['value'];
    if ($row['key'] == 'timezone_status') $timezone['status'] = $row['value'];
  }
  
  if (!empty($timezone['status']) && !empty($timezone['timezone'])) {
    date_default_timezone_set($timezone['timezone']);
    
    $now = new DateTime();
    $mins = $now->getOffset() / 60;
    $sgn = ($mins < 0 ? -1 : 1);
    $mins = abs($mins);
    $hrs = floor($mins / 60);
    $mins -= $hrs * 60;
    $offset = sprintf('%+d:%02d', $hrs * $sgn, $mins);
    if (version_compare(VERSION, '2.3') >= 0) {
      $this->adaptor->query("SET time_zone='" . $offset . "'");
    } else {
      $this->db->query("SET time_zone='" . $offset . "'");
    }
  }
}
//tri mod end
      
		return $this->adaptor->query($sql, $params);
	}

	public function escape($value) {
		return $this->adaptor->escape($value);
	}

	public function countAffected() {
		return $this->adaptor->countAffected();
	}

	public function getLastId() {
		return $this->adaptor->getLastId();
	}
	
	public function connected() {
		return $this->adaptor->connected();
	}
}